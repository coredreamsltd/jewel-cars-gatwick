<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

if (!function_exists('email_help')) {

    function email_help($recipients, $subject, $msg, $from, $attachments = null)
    {

        if (in_array(ENVIRONMENT, ['testing', 'production'])) {
            $mail = new PHPMailer(true);
            try {
                // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
                $mail->isSMTP();                                            //Send using SMTP
                $mail->Host       = 'mail.jewelcars.co.uk';                 //Set the SMTP server to send through
                $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                $mail->Username   = SWIFT_EMAIL;                            //SMTP username
                $mail->Password   = SWIFT_PASSWORD;                             //SMTP password
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

                //Recipients
                if (is_array($from)) {
                    $mail->setFrom(key($from), $from[key($from)]);
                } else {
                    $mail->setFrom($from);
                }

                if (is_array($recipients)) {
                    foreach ($recipients as $recipient) {
                        $mail->addAddress($recipient);
                    }
                } else {
                    $mail->addAddress($recipients);
                }

                //Attachments
                if ($attachments) {
                    foreach ($attachments as $k => $v) {
                        $mail->addAttachment($v, $k . '.pdf');
                        // $mail->attach(Swift_Attachment::fromPath($v));
                    }
                }
                //Content
                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body    = $msg;
                // debug($mail);
                $mail->send();
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }
        return true;
    }
}
