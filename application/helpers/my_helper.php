<?php

function segment($segment)
{
    $ci = &get_instance();
    return $ci->uri->segment($segment);
}

function flash()
{
    $ci = &get_instance();
    if ($ci->session->flashdata('msg')) {
        echo
        '<div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
            . $ci->session->flashdata('msg') .
            '</div>';
    }
    if ($ci->session->flashdata('dmsg')) {
        echo
        '<div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
            . $ci->session->flashdata('dmsg') .
            '</div>';
    }
}

function set_flash($index, $msg)
{
    $ci = &get_instance();
    $ci->session->set_flashdata($index, $msg);
}

function check_user($check_index = 'current_user')
{
    $ci = &get_instance();
    return ($ci->session->userdata($check_index)) ? $ci->session->userdata($check_index) : false;
}

function check_front_user()
{
    $ci = &get_instance();
    return ($ci->session->userdata('front_user')) ? $ci->session->userdata('front_user') : false;
}

function get_userdata($index)
{
    $ci = &get_instance();
    return $ci->session->userdata($index) ? $ci->session->userdata($index) : false;
}

function debug(...$data)
{

    foreach ($data as $d) {
        echo "<pre>";
        print_r($d);
        echo "<br>";
        echo "</pre>";
    }
    die;
}

require APPPATH . '/vendor/autoload.php';

use Pelago\Emogrifier\CssInliner;

function common_emogrifier($emailer)
{
    $css = file_get_contents(FCPATH . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'emailer.css');
    $visualHtml = CssInliner::fromHtml($emailer)->inlineCss($css)->render();
    return $visualHtml;
}

function get_page_by_slug($slug)
{
    if ($slug) {
        $ci = &get_instance();
        $data = array('slug' => $slug);
        $result = $ci->db->get_where('tbl_page', $data)->row();
        return $result;
    }
    return null;
}

function jsonOutput($status, $msg, $data = false)
{

    $response = new stdClass();
    $response->status = $status;
    $response->msg = $msg;

    if ($data)
        $response->data = $data;

    die(json_encode($response));
}

// function calDistance($postcode_from, $postcode_to, $distance_unit = 'M')
// {
//     // debug($postcode_from, $postcode_to, $distance_unit = 'M');
//     $data = array(
//         "postcodes" => [strtoupper($postcode_to)],
//         "key" => FECHIFY_ACCESS_TOKEN,
//         "distance" => [
//             "base_postcode" => strtoupper($postcode_from)
//         ]
//     );
//     $data_string = json_encode($data);

//     $ch = curl_init('http://pcls1.craftyclicks.co.uk/json/geocode');
//     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//     curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     curl_setopt(
//         $ch,
//         CURLOPT_HTTPHEADER,
//         array(
//             'Content-Type: application/json',
//             'Content-Length: ' . strlen($data_string)
//         )
//     );

//     $jsonResponse = curl_exec($ch);

//     $dataset = json_decode($jsonResponse, true);
// // debug($dataset);
//     if (!$dataset)
//         return 0;

//     if (!isset($dataset[$postcode_to]))
//         return 0;

//     $distance = !empty($dataset[$postcode_to]['distance'])?$dataset[$postcode_to]['distance']:0;


//     if ($distance_unit == "K") {
//         $distance = round(($distance / 1000), 2); // kilometer
//     } else if ($distance_unit == "M") {
//         $distance = round(($distance * 0.000621371), 2); // miles
//     }
//     return ['distance' => $distance];
// }
// function calDistance($postcode_from, $postcode_to, $distance_unit = 'M')
// {

//     $url = "https://api.getAddress.io/distance/{$postcode_from}/{$postcode_to}?api-key=-ZTt5_mWHEG1srrzErhyBg24212";
//     $c = curl_init();
//     curl_setopt($c, CURLOPT_URL, $url);
//     curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
//     curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
//     curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//     $jsonResponse = curl_exec($c);

//     $dataset = json_decode($jsonResponse);

//     if (!$dataset)
//         return 0;

//     if (!isset($dataset->metres))
//         return 0;

//     $distance = $dataset->metres;

//     if ($distance_unit == "K") {
//         $distance = round(($distance / 1000), 2); // kilometer
//     } else if ($distance_unit == "M") {
//         $distance = round(($distance * 0.000621371), 2); // miles
//     }

//     return ['distance' => $distance];
// }

function calDistance($inLatitude, $inLongitude, $outLatitude, $outLongitude, $way_latlng = null, $distance_unit = 'M', $duration_unit = 'm')
{

    $url = "https://maps.googleapis.com/maps/api/directions/json?origin=$inLatitude,$inLongitude&destination=$outLatitude,$outLongitude&waypoints=$way_latlng&key=" . MAP_API_KEY;
    $c = curl_init($url);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $jsonResponse = curl_exec($c);
    if (curl_errno($c)) {
        log_message('error', 'Curl error: ' . curl_error($c));
    }

    curl_close($c);

    $dataset = json_decode($jsonResponse);
    $routes = $dataset->routes;

    if (!$dataset)
        return 0;
    if (!isset($dataset->routes[0]->legs[0]->distance->value))
        return 0;

    $distance = 0;
    $duration = 0;
    //Since multiple routes are generated, we add the distance
    if ($way_latlng != '') {

        $start_address = $routes[0]->legs[0]->start_address;
        $start_location = $routes[0]->legs[0]->start_location;
        $waypoint_address = $routes[0]->legs[0]->end_address;
        $waypoint_location = $routes[0]->legs[0]->end_location;
        $end_address = $routes[0]->legs[1]->end_address;
        $end_location = $routes[0]->legs[1]->end_location;

        foreach ($routes[0]->legs as $leg) {
            $distance += $leg->distance->value;
            $duration += $leg->duration->value;
        }
    } else {
        $start_address = $routes[0]->legs[0]->start_address;
        $start_location = $routes[0]->legs[0]->start_location;
        $waypoint_address = array();
        $waypoint_location = array();
        $end_address = $routes[0]->legs[0]->end_address;
        $end_location = $routes[0]->legs[0]->end_location;

        $distance = $routes[0]->legs[0]->distance->value;
        $duration = $routes[0]->legs[0]->duration->value;
    }

    $data = array(
        'distance' => $distance, // meters
        'duration' => $duration, // seconds
        'duration_seconds' => $duration, // seconds
        'polyline_map_data' => array(
            'start_address' => $start_address,
            'start_location' => $start_location,
            'end_address' => $end_address,
            'end_location' => $end_location,
            'bounds' => $routes[0]->bounds,
            'overview_polyline' => $routes[0]->overview_polyline,
            'waypoint_address' => $waypoint_address,
            'waypoint_location' => $waypoint_location,
        )
    );

    if ($distance_unit == "K") {
        $data['distance'] = round(($data['distance'] / 1000), 2); // kilometer
    } else if ($distance_unit == "M") {
        $data['distance'] = round(($data['distance'] * 0.000621371), 2); // miles
    }
    $data['duration'] = secondsToTime($data['duration']);

    //    if ($duration_unit == "m") {
    //        $data['duration'] = round($data['duration'] / 60, 0); // minutes
    //    } else if ($duration_unit == "h") {
    //        $data['duration'] = round($data['duration'] / (60 * 60), 0); // hours
    //    }

    return $data;
}

function secondsToTime($seconds)
{
    //To convert number of seconds into days hours and minutes
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    if ($dtF->diff($dtT)->format('%a') > 0) {
        return $dtF->diff($dtT)->format('%a days, %h hrs, %i mins');
    } else if ($dtF->diff($dtT)->format('%h') > 0) {
        return $dtF->diff($dtT)->format('%h hrs, %i mins');
    } else if ($dtF->diff($dtT)->format('%i') > 0) {
        return $dtF->diff($dtT)->format('%i mins');
    }
}


function meetingInstruction($id=''){
	$ci = &get_instance();
	$specific_location=$ci->db->select('id,instructions')->get_where('specific_locations',['id'=>$id])->row();
	return $specific_location->instructions??'';

}

function isAirport($location)
{
    $ci = &get_instance();
    if (!is_array($location)) {
        $location = (array)$location;
    }

    if (!empty($location['start_lng']) && !empty($location['start_lat'])) {
        $sql = "SELECT *  FROM `specific_locations` as spl "
            . "WHERE ST_WITHIN(Point({$location['start_lng']},{$location['start_lat']}), spl.coordinates) AND spl.type = 'airport' ";

        $data = $ci->db->query($sql);
        $num = '';
        $is_airport = '';
        if ($data === FALSE) {
        } else {
            $num = $data->num_rows();
            $is_airport = $num ? $data->row() : '';
        }

        if ($is_airport) {
            return $is_airport->id;
        }
    }
    !empty($location['start']) ?: $location['start'] = $location['pickup_address'];
    
    if ((stripos($location['start'], 'airport')) !== false) {
        return true;
    }
}

function isEndAirport($location)
{
    $ci = &get_instance();
    if (!is_array($location)) {
        $location = (array)$location;
    }

    if (!empty($location['end_lng']) && !empty($location['end_lat'])) {
        $sql = "SELECT *  FROM `specific_locations` as spl "
            . "WHERE ST_WITHIN(Point({$location['end_lng']},{$location['end_lat']}), spl.coordinates) AND spl.type = 'airport' ";

        $data = $ci->db->query($sql);
        $num = '';
        $is_airport = '';
        if ($data === FALSE) {
        } else {
            $num = $data->num_rows();
            $is_airport = $num ? $data->row() : '';
        }

        if ($is_airport) {
            return $is_airport->id;
        }
    }

    !empty($location['end']) ?: $location['end'] = $location['dropoff_address'];

    if ((stripos($location['end'], 'airport')) !== false) {
        return true;
    }
}

function isSeaport($location)
{
    $ci = &get_instance();

    if (!empty($location['start_lng']) && !empty($location['start_lat'])) {
        $sql = "SELECT *  FROM `zones` as z "
            . "WHERE ST_WITHIN(Point({$location['start_lng']},{$location['start_lat']}), z.coordinates) AND type = 'seaport' ";
        // $num = $ci->db->query($sql)->num_rows();
        // $is_port = $num ? $ci->db->query($sql)->row() : '';

        $data = $ci->db->query($sql);
        $num = '';
        $is_port = '';
        if ($data === FALSE) {
        } else {
            $num = $data->num_rows();
            $is_port = $num ? $data->row() : '';
        }


        if ($is_port) {
            return $is_port->id;
        }
    }
    return number_format(0);
    //    !empty($location['start']) ?: $location['start'] = $location['pickup_address'];
    //
    //    if ((stripos($location['start'], 'airport')) !== false) {
    //        return false;
    //    }
    //    if ((stripos($location['start'], 'cruise') !== false) || (stripos($location['start'], 'port')) !== false) {
    //        return true;
    //    }
    //
    //    $location = $ci->common_model->get_where('tbl_locations', array('id' => $location['start_id']));
    //
    //    if ($location && $location[0]['location_type_id'] == 2) {
    //        return true;
    //    }
}

function isEndSeaport($location)
{
    $ci = &get_instance();
    if (!empty($location['end_lng']) && !empty($location['end_lat'])) {
        $sql = "SELECT *  FROM `zones` as z "
            . "WHERE ST_WITHIN(Point({$location['end_lng']},{$location['end_lat']}), z.coordinates) AND type = 'seaport' ";
        $num = $ci->db->query($sql)->num_rows();
        $is_port = $num ? $ci->db->query($sql)->row() : '';


        if ($is_port) {
            return $is_port->id;
        }
    }
    return number_format(0);
    //    !empty($location['end']) ?: $location['end'] = $location['dropoff_address'];
    //
    //    if ((stripos($location['end'], 'airport')) !== false) {
    //        return false;
    //    }
    //
    //
    //    if ((stripos($location['end'], 'cruise') !== false)) {
    //        return true;
    //    }
    //
    //    $location = $ci->common_model->get_where('tbl_locations', array('id' => $location['end_id']));
    //
    //    if ($location && $location[0]['location_type_id'] == 2) {
    //        return true;
    //    }
}

function isUniversities($location)
{
    !empty($location['start']) ?: $location['start'] = $location['pickup_address'];

    if ((stripos($location['start'], 'university') > -1)) {
        return true;
    }

    $ci = &get_instance();
    $location = $ci->common_model->get_where('tbl_locations', array('id' => $location['start_id']));

    if ($location && $location[0]['location_type_id'] == 9) {
        return true;
    }
}

function isEndUniversities($location)
{
    !empty($location['end']) ?: $location['end'] = $location['dropoff_address'];

    if ((stripos($location['end'], 'university') > -1)) {
        return true;
    }

    $ci = &get_instance();
    $location = $ci->common_model->get_where('tbl_locations', array('id' => $location['end_id']));

    if ($location && $location[0]['location_type_id'] == 9) {
        return true;
    }
}

function isStation($location)
{
    $ci = &get_instance();

    if (!empty($location['start_lng']) && !empty($location['start_lat'])) {
        $sql = "SELECT *  FROM `zones` as z "
            . "WHERE ST_WITHIN(Point({$location['start_lng']},{$location['start_lat']}), z.coordinates) AND type = 'station' ";

        $data = $ci->db->query($sql);
        $num = '';
        $is_port = '';
        if ($data === FALSE) {
        } else {
            $num = $data->num_rows();
            $is_port = $num ? $data->row() : '';
        }


        if ($is_port) {
            return $is_port->id;
        }
    }
    return number_format(0);
    // !empty($location['start']) ?: $location['start'] = $location['pickup_address'];

    // if ((stripos($location['start'], 'station') !== false)) {
    //     return true;
    // }

    // $ci = &get_instance();
    // $location = $ci->common_model->get_where('tbl_locations', array('id' => $location['start_id']));

    // if ($location && $location[0]['location_type_id'] == 3) {
    //     return true;
    // }
}

function isEndStation($location)
{
    $ci = &get_instance();
    if (!empty($location['end_lng']) && !empty($location['end_lat'])) {
        $sql = "SELECT *  FROM `zones` as z "
            . "WHERE ST_WITHIN(Point({$location['end_lng']},{$location['end_lat']}), z.coordinates) AND type = 'station' ";
        $num = $ci->db->query($sql)->num_rows();
        $is_port = $num ? $ci->db->query($sql)->row() : '';


        if ($is_port) {
            return $is_port->id;
        }
    }
    return number_format(0);
    // !empty($location['end']) ?: $location['end'] = $location['dropoff_address'];

    // if ((stripos($location['end'], 'station') !== false)) {
    //     return true;
    // }

    // $ci = &get_instance();
    // $location = $ci->common_model->get_where('tbl_locations', array('id' => $location['end_id']));

    // if ($location && $location[0]['location_type_id'] == 3) {
    //     return true;
    // }
}

function isTourist($location)
{
    $ci = &get_instance();
    $location = $ci->common_model->get_where('tbl_locations', array('id' => $location['start_id']));

    if ($location && $location[0]['location_type_id'] == 8) {
        return true;
    }
}

function isEndTourist($location)
{
    $ci = &get_instance();
    $location = $ci->common_model->get_where('tbl_locations', array('id' => $location['end_id']));

    if ($location && $location[0]['location_type_id'] == 8) {
        return true;
    }
}

function triptype_text($triptype)
{

    if ($triptype == 'one_way') {
        return 'One Way';
    } else if ($triptype == 'two_way') {
        return 'Round Trip';
    }
}

function airportTerminal($airport)
{
    if ((stripos($airport, 'heathrow airport') !== false)) {
        return true;
    } elseif ((stripos($airport, 'gatwick airport') !== false)) {
        return true;
    }
}

function SMD_dateformat($date)
{
    if ($date) {
        $date = DateTime::createFromFormat('Y-m-d', $date)->format('d/m/Y');
        return $date;
    }
}

function SMDCrateDateTimeFormat($date, $from, $to)
{
    if ($date) {
        $date = DateTime::createFromFormat($from, $date)->format($to);
        return $date;
    }
}

function generateBookingRefId($booking_id)
{
    if (strlen($booking_id) < 3) {
        $zerosRequired = 3 - strlen($booking_id);

        $zeros = '';
        while ($zerosRequired > 0) {
            $zeros .= '0';
            $zerosRequired--;
        }
        $bookingId8digit = $zeros . $booking_id;
    }
    return $bookingId8digit;
}

function setSession($name, $data)
{
    $ci = &get_instance();
    $ci->session->set_userdata($name, $data);
}

function getSession($name)
{
    $ci = &get_instance();
    return $ci->session->userdata($name);
}
function unSetSession($name)
{
    $ci = &get_instance();
    $ci->session->unset_userdata($name);
}

function uniqueid($prefix, $length = 4, $search_table, $search_column, $auto_incr = 0)
{
    $ci = &get_instance();

    if ($length > 0) {
        $id = $prefix . substr(rand(1111111111, 9999999999), 0, $length);
    } else {
        if ($auto_incr > 0) {
            $id = $prefix . $auto_incr;
        } else {
            $id = $prefix;
        }
    }

    $checks = $ci->db->get_where($search_table, array($search_column => $id));
    if ($checks->num_rows() > 0) {
        return uniqueid($prefix, $length, $search_table, $search_column, ++$auto_incr);
    } else {
        return $id;
    }
}

// Function to convert NTP string to an array
function NVPToArray($NVPString)
{
    $proArray = array();
    while (strlen($NVPString)) {
        // name
        $keypos = strpos($NVPString, '=');
        $keyval = substr($NVPString, 0, $keypos);
        // value
        $valuepos = strpos($NVPString, '&') ? strpos($NVPString, '&') : strlen($NVPString);
        $valval = substr($NVPString, $keypos + 1, $valuepos - $keypos - 1);
        // decoding the respose
        $proArray[$keyval] = urldecode($valval);
        $NVPString = substr($NVPString, $valuepos + 1, strlen($NVPString));
    }
    return $proArray;
}

function isStartHeathrowAirport($data)
{

    if ((stripos($data['start'], 'heathrow airport') !== false) || (stripos($data['start'], 'tw6') !== false)) {
        return true;
    }
}

function isStartGatwickAirport($data)
{
    if ((stripos($data['start'], 'gatwick') !== false)) {
        return true;
    }
}

function isEndHeathrowAirport($data)
{
    if ((stripos($data['end'], 'heathrow airport') !== false) || (stripos($data['end'], 'tw6') !== false)) {
        return true;
    }
}

function isEndGatwickAirport($data)
{
    if ((stripos($data['end'], 'gatwick') !== false)) {
        return true;
    }
}

function getLocationType($typeId)
{
    $ci = &get_instance();
    $locationType = $ci->common_model->get_where('tbl_location_type', array('id' => $typeId));
    return (!empty($locationType[0]['type']) ? $locationType[0]['type'] : '');
}

function getAllLocationTypes()
{
    $ci = &get_instance();
    $locationTypes = $ci->common_model->get_all('tbl_location_type', '', 'type');
    return $locationTypes;
}

function getAllLocations()
{

    $ci = &get_instance();
    $locationTypes = $ci->common_model->get_all('tbl_location_type', '', 'type');
    $locationsGrpArr = array();

    foreach ($locationTypes as $lt) {

        $type = $lt['type'];
        $locations = $ci->common_model->get_where('tbl_locations', array('location_type_id' => $lt['id']));
        $locationsGrpArr = array_merge($locationsGrpArr, array($type => $locations));
    }

    return $locationsGrpArr;
}

function getFAByLocationTypeId($location_type_id)
{
    $ci = &get_instance();
    $lt = $ci->common_model->get_where('tbl_location_type', array('id' => $location_type_id));
    return !empty($lt[0]['fa_icon']) ? $lt[0]['fa_icon'] : 'fa-map-marker';
}
function getLocationNameById($location_id = null)
{
    $ci = &get_instance();
    $lt = $ci->common_model->get_where('tbl_locations', array('id' => $location_id));
    return !empty($lt[0]['name']) ? $lt[0]['name'] : '';
}

function getLocationTypeByLocationId($location_id = null)
{
    $ci = &get_instance();
    $lt = $ci->common_model->get_where('tbl_locations', array('id' => $location_id));
    if (!empty($lt[0]['location_type_id'])) {
        return getLocationType($lt[0]['location_type_id']);
    }
}

function TM_order_special_string($key, $term)
{
    $string = "CASE";
    $string .= " WHEN $key = '$term' THEN 0";
    $string .= " WHEN $key LIKE '$term%' THEN 1";
    $string .= " WHEN $key LIKE '%$term%' THEN 2";
    $string .= " WHEN $key LIKE '%$term' THEN 3";
    $string .= " ELSE 4";
    $string .= " END";

    return $string;
}

function getLatLng($address)
{
    $url = "https://maps.googleapis.com/maps/api/geocode/json?key=" . MAP_API_KEY . "&address=" . urlencode($address);

    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

    try {
        $jsonResponse = curl_exec($c);
        $data = json_decode($jsonResponse);

        if (isset($data->results) && $data->results) {
            $lat_lng = ['lat' => $data->results[0]->geometry->location->lat, 'lng' => $data->results[0]->geometry->location->lng];
            return $lat_lng;
        }
    } catch (Exception $e) {
        die($e->getMessage());
    }
}

function getManualRateById($from_id, $to_id)
{
    $ci = &get_instance();
    $rate = $ci->db->select('*')->from('tbl_routes_rate')->where('from_id', $from_id)->where('to_id', $to_id)->get()->row();
    return ($rate ? $rate->rate : '0');
}

function is_admin_menu_accessible($menu_index)
{
    $ci = &get_instance();
    $admin = $ci->common_model->get_where('tbl_admin', array('username' => $ci->session->userdata('username')));

    if (in_array($menu_index, explode(',', $admin[0]['menu_access']))) {
        return true;
    }
}
function math_captcha_generator()
{
    $min_no = 1;
    $max_no = 20;

    $random_number1 = mt_rand($min_no, $max_no);
    $random_number2 = mt_rand($min_no, $max_no);
    setSession('random_number1', $random_number1);
    setSession('random_number2', $random_number2);
}

function check_math_captcha($sum)
{
    if ($sum == (getSession('random_number1') + getSession('random_number2'))) {
        return true;
    }
    return false;
}

function checkGoogleCaptcha()
{
    if (ENVIRONMENT == 'development') {
        return true;
    }
    $recaptcha_secret = CAPTCHA_SECRET_KEY;
    $captcha = $_POST['g-recaptcha-response'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $url = "https://www.google.com/recaptcha/api/siteverify?secret=$recaptcha_secret&response=$captcha&remoteip=$ip";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $jasonResponse = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($jasonResponse);

    if ($response->success != 1) {
        return false;
    }
    unset($_POST['g-recaptcha-response']);
    return true;
}
function getCountryFromIP($ip)
{
    $url = "http://ip-api.com/json/" . $ip;
    //    $url = "http://ip-api.com/json/103.242.22.237";

    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $jsonResponse = curl_exec($c);
    curl_close($c);
    $data = json_decode($jsonResponse);
    return ($data);
}
function viaPointFormat($get)
{
    //Way point formatting
    $way_latlng = '';
    if (!empty($get['way_point_lat']) && !empty($get['way_point_lng'])) {
        foreach ($get['way_point_lat'] as $key => $way_lat) :
            $way_point_lat[$key] = $way_lat;
        endforeach;
        foreach ($get['way_point_lng'] as $key => $way_lng) :
            $way_point_lng[$key] = $way_lng;
        endforeach;

        $way_points_concate = "";
        for ($i = 0; $i < count($get['stop_point']); $i++) :
            $way_points_concate = $way_points_concate . "|" . $way_point_lat[$i] . "," . $way_point_lng[$i];
        endfor;

        $way_latlng = substr($way_points_concate, 1);
    }

    return $way_latlng;
}
function convertToEndDate($datetime = null, $from = 'Y-m-d H:i:s', $to = 'Y-m-d H:i:s', $append_seconds = 0)
{
    $date = SMDCrateDateTimeFormat($datetime, $from, 'Y-m-d H:i:s');
    $new_date_time = date("Y-m-d H:i:s", (strtotime(date($date)) + $append_seconds));
    if (!empty($new_date_time)) {
        return (SMDCrateDateTimeFormat($new_date_time, 'Y-m-d H:i:s', $to));
    }
    return;
}

function getCountryByCountryCode($code = null)
{
    $countryArray = array(
        'AD' => array('name' => 'ANDORRA', 'code' => '376'),
        'AE' => array('name' => 'UNITED ARAB EMIRATES', 'code' => '971'),
        'AF' => array('name' => 'AFGHANISTAN', 'code' => '93'),
        'AG' => array('name' => 'ANTIGUA AND BARBUDA', 'code' => '1268'),
        'AI' => array('name' => 'ANGUILLA', 'code' => '1264'),
        'AL' => array('name' => 'ALBANIA', 'code' => '355'),
        'AM' => array('name' => 'ARMENIA', 'code' => '374'),
        'AN' => array('name' => 'NETHERLANDS ANTILLES', 'code' => '599'),
        'AO' => array('name' => 'ANGOLA', 'code' => '244'),
        'AQ' => array('name' => 'ANTARCTICA', 'code' => '672'),
        'AR' => array('name' => 'ARGENTINA', 'code' => '54'),
        'AS' => array('name' => 'AMERICAN SAMOA', 'code' => '1684'),
        'AT' => array('name' => 'AUSTRIA', 'code' => '43'),
        'AU' => array('name' => 'AUSTRALIA', 'code' => '61'),
        'AW' => array('name' => 'ARUBA', 'code' => '297'),
        'AZ' => array('name' => 'AZERBAIJAN', 'code' => '994'),
        'BA' => array('name' => 'BOSNIA AND HERZEGOVINA', 'code' => '387'),
        'BB' => array('name' => 'BARBADOS', 'code' => '1246'),
        'BD' => array('name' => 'BANGLADESH', 'code' => '880'),
        'BE' => array('name' => 'BELGIUM', 'code' => '32'),
        'BF' => array('name' => 'BURKINA FASO', 'code' => '226'),
        'BG' => array('name' => 'BULGARIA', 'code' => '359'),
        'BH' => array('name' => 'BAHRAIN', 'code' => '973'),
        'BI' => array('name' => 'BURUNDI', 'code' => '257'),
        'BJ' => array('name' => 'BENIN', 'code' => '229'),
        'BL' => array('name' => 'SAINT BARTHELEMY', 'code' => '590'),
        'BM' => array('name' => 'BERMUDA', 'code' => '1441'),
        'BN' => array('name' => 'BRUNEI DARUSSALAM', 'code' => '673'),
        'BO' => array('name' => 'BOLIVIA', 'code' => '591'),
        'BR' => array('name' => 'BRAZIL', 'code' => '55'),
        'BS' => array('name' => 'BAHAMAS', 'code' => '1242'),
        'BT' => array('name' => 'BHUTAN', 'code' => '975'),
        'BW' => array('name' => 'BOTSWANA', 'code' => '267'),
        'BY' => array('name' => 'BELARUS', 'code' => '375'),
        'BZ' => array('name' => 'BELIZE', 'code' => '501'),
        'CA' => array('name' => 'CANADA', 'code' => '1'),
        'CC' => array('name' => 'COCOS (KEELING) ISLANDS', 'code' => '61'),
        'CD' => array('name' => 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'code' => '243'),
        'CF' => array('name' => 'CENTRAL AFRICAN REPUBLIC', 'code' => '236'),
        'CG' => array('name' => 'CONGO', 'code' => '242'),
        'CH' => array('name' => 'SWITZERLAND', 'code' => '41'),
        'CI' => array('name' => 'COTE D IVOIRE', 'code' => '225'),
        'CK' => array('name' => 'COOK ISLANDS', 'code' => '682'),
        'CL' => array('name' => 'CHILE', 'code' => '56'),
        'CM' => array('name' => 'CAMEROON', 'code' => '237'),
        'CN' => array('name' => 'CHINA', 'code' => '86'),
        'CO' => array('name' => 'COLOMBIA', 'code' => '57'),
        'CR' => array('name' => 'COSTA RICA', 'code' => '506'),
        'CU' => array('name' => 'CUBA', 'code' => '53'),
        'CV' => array('name' => 'CAPE VERDE', 'code' => '238'),
        'CX' => array('name' => 'CHRISTMAS ISLAND', 'code' => '61'),
        'CY' => array('name' => 'CYPRUS', 'code' => '357'),
        'CZ' => array('name' => 'CZECH REPUBLIC', 'code' => '420'),
        'DE' => array('name' => 'GERMANY', 'code' => '49'),
        'DJ' => array('name' => 'DJIBOUTI', 'code' => '253'),
        'DK' => array('name' => 'DENMARK', 'code' => '45'),
        'DM' => array('name' => 'DOMINICA', 'code' => '1767'),
        'DO' => array('name' => 'DOMINICAN REPUBLIC', 'code' => '1809'),
        'DZ' => array('name' => 'ALGERIA', 'code' => '213'),
        'EC' => array('name' => 'ECUADOR', 'code' => '593'),
        'EE' => array('name' => 'ESTONIA', 'code' => '372'),
        'EG' => array('name' => 'EGYPT', 'code' => '20'),
        'ER' => array('name' => 'ERITREA', 'code' => '291'),
        'ES' => array('name' => 'SPAIN', 'code' => '34'),
        'ET' => array('name' => 'ETHIOPIA', 'code' => '251'),
        'FI' => array('name' => 'FINLAND', 'code' => '358'),
        'FJ' => array('name' => 'FIJI', 'code' => '679'),
        'FK' => array('name' => 'FALKLAND ISLANDS (MALVINAS)', 'code' => '500'),
        'FM' => array('name' => 'MICRONESIA, FEDERATED STATES OF', 'code' => '691'),
        'FO' => array('name' => 'FAROE ISLANDS', 'code' => '298'),
        'FR' => array('name' => 'FRANCE', 'code' => '33'),
        'GA' => array('name' => 'GABON', 'code' => '241'),
        'GB' => array('name' => 'UNITED KINGDOM', 'code' => '44'),
        'GD' => array('name' => 'GRENADA', 'code' => '1473'),
        'GE' => array('name' => 'GEORGIA', 'code' => '995'),
        'GH' => array('name' => 'GHANA', 'code' => '233'),
        'GI' => array('name' => 'GIBRALTAR', 'code' => '350'),
        'GL' => array('name' => 'GREENLAND', 'code' => '299'),
        'GM' => array('name' => 'GAMBIA', 'code' => '220'),
        'GN' => array('name' => 'GUINEA', 'code' => '224'),
        'GQ' => array('name' => 'EQUATORIAL GUINEA', 'code' => '240'),
        'GR' => array('name' => 'GREECE', 'code' => '30'),
        'GT' => array('name' => 'GUATEMALA', 'code' => '502'),
        'GU' => array('name' => 'GUAM', 'code' => '1671'),
        'GW' => array('name' => 'GUINEA-BISSAU', 'code' => '245'),
        'GY' => array('name' => 'GUYANA', 'code' => '592'),
        'HK' => array('name' => 'HONG KONG', 'code' => '852'),
        'HN' => array('name' => 'HONDURAS', 'code' => '504'),
        'HR' => array('name' => 'CROATIA', 'code' => '385'),
        'HT' => array('name' => 'HAITI', 'code' => '509'),
        'HU' => array('name' => 'HUNGARY', 'code' => '36'),
        'ID' => array('name' => 'INDONESIA', 'code' => '62'),
        'IE' => array('name' => 'IRELAND', 'code' => '353'),
        'IL' => array('name' => 'ISRAEL', 'code' => '972'),
        'IM' => array('name' => 'ISLE OF MAN', 'code' => '44'),
        'IN' => array('name' => 'INDIA', 'code' => '91'),
        'IQ' => array('name' => 'IRAQ', 'code' => '964'),
        'IR' => array('name' => 'IRAN, ISLAMIC REPUBLIC OF', 'code' => '98'),
        'IS' => array('name' => 'ICELAND', 'code' => '354'),
        'IT' => array('name' => 'ITALY', 'code' => '39'),
        'JM' => array('name' => 'JAMAICA', 'code' => '1876'),
        'JO' => array('name' => 'JORDAN', 'code' => '962'),
        'JP' => array('name' => 'JAPAN', 'code' => '81'),
        'KE' => array('name' => 'KENYA', 'code' => '254'),
        'KG' => array('name' => 'KYRGYZSTAN', 'code' => '996'),
        'KH' => array('name' => 'CAMBODIA', 'code' => '855'),
        'KI' => array('name' => 'KIRIBATI', 'code' => '686'),
        'KM' => array('name' => 'COMOROS', 'code' => '269'),
        'KN' => array('name' => 'SAINT KITTS AND NEVIS', 'code' => '1869'),
        'KP' => array('name' => 'KOREA DEMOCRATIC PEOPLES REPUBLIC OF', 'code' => '850'),
        'KR' => array('name' => 'KOREA REPUBLIC OF', 'code' => '82'),
        'KW' => array('name' => 'KUWAIT', 'code' => '965'),
        'KY' => array('name' => 'CAYMAN ISLANDS', 'code' => '1345'),
        'KZ' => array('name' => 'KAZAKSTAN', 'code' => '7'),
        'LA' => array('name' => 'LAO PEOPLES DEMOCRATIC REPUBLIC', 'code' => '856'),
        'LB' => array('name' => 'LEBANON', 'code' => '961'),
        'LC' => array('name' => 'SAINT LUCIA', 'code' => '1758'),
        'LI' => array('name' => 'LIECHTENSTEIN', 'code' => '423'),
        'LK' => array('name' => 'SRI LANKA', 'code' => '94'),
        'LR' => array('name' => 'LIBERIA', 'code' => '231'),
        'LS' => array('name' => 'LESOTHO', 'code' => '266'),
        'LT' => array('name' => 'LITHUANIA', 'code' => '370'),
        'LU' => array('name' => 'LUXEMBOURG', 'code' => '352'),
        'LV' => array('name' => 'LATVIA', 'code' => '371'),
        'LY' => array('name' => 'LIBYAN ARAB JAMAHIRIYA', 'code' => '218'),
        'MA' => array('name' => 'MOROCCO', 'code' => '212'),
        'MC' => array('name' => 'MONACO', 'code' => '377'),
        'MD' => array('name' => 'MOLDOVA, REPUBLIC OF', 'code' => '373'),
        'ME' => array('name' => 'MONTENEGRO', 'code' => '382'),
        'MF' => array('name' => 'SAINT MARTIN', 'code' => '1599'),
        'MG' => array('name' => 'MADAGASCAR', 'code' => '261'),
        'MH' => array('name' => 'MARSHALL ISLANDS', 'code' => '692'),
        'MK' => array('name' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'code' => '389'),
        'ML' => array('name' => 'MALI', 'code' => '223'),
        'MM' => array('name' => 'MYANMAR', 'code' => '95'),
        'MN' => array('name' => 'MONGOLIA', 'code' => '976'),
        'MO' => array('name' => 'MACAU', 'code' => '853'),
        'MP' => array('name' => 'NORTHERN MARIANA ISLANDS', 'code' => '1670'),
        'MR' => array('name' => 'MAURITANIA', 'code' => '222'),
        'MS' => array('name' => 'MONTSERRAT', 'code' => '1664'),
        'MT' => array('name' => 'MALTA', 'code' => '356'),
        'MU' => array('name' => 'MAURITIUS', 'code' => '230'),
        'MV' => array('name' => 'MALDIVES', 'code' => '960'),
        'MW' => array('name' => 'MALAWI', 'code' => '265'),
        'MX' => array('name' => 'MEXICO', 'code' => '52'),
        'MY' => array('name' => 'MALAYSIA', 'code' => '60'),
        'MZ' => array('name' => 'MOZAMBIQUE', 'code' => '258'),
        'NA' => array('name' => 'NAMIBIA', 'code' => '264'),
        'NC' => array('name' => 'NEW CALEDONIA', 'code' => '687'),
        'NE' => array('name' => 'NIGER', 'code' => '227'),
        'NG' => array('name' => 'NIGERIA', 'code' => '234'),
        'NI' => array('name' => 'NICARAGUA', 'code' => '505'),
        'NL' => array('name' => 'NETHERLANDS', 'code' => '31'),
        'NO' => array('name' => 'NORWAY', 'code' => '47'),
        'NP' => array('name' => 'NEPAL', 'code' => '977'),
        'NR' => array('name' => 'NAURU', 'code' => '674'),
        'NU' => array('name' => 'NIUE', 'code' => '683'),
        'NZ' => array('name' => 'NEW ZEALAND', 'code' => '64'),
        'OM' => array('name' => 'OMAN', 'code' => '968'),
        'PA' => array('name' => 'PANAMA', 'code' => '507'),
        'PE' => array('name' => 'PERU', 'code' => '51'),
        'PF' => array('name' => 'FRENCH POLYNESIA', 'code' => '689'),
        'PG' => array('name' => 'PAPUA NEW GUINEA', 'code' => '675'),
        'PH' => array('name' => 'PHILIPPINES', 'code' => '63'),
        'PK' => array('name' => 'PAKISTAN', 'code' => '92'),
        'PL' => array('name' => 'POLAND', 'code' => '48'),
        'PM' => array('name' => 'SAINT PIERRE AND MIQUELON', 'code' => '508'),
        'PN' => array('name' => 'PITCAIRN', 'code' => '870'),
        'PR' => array('name' => 'PUERTO RICO', 'code' => '1'),
        'PT' => array('name' => 'PORTUGAL', 'code' => '351'),
        'PW' => array('name' => 'PALAU', 'code' => '680'),
        'PY' => array('name' => 'PARAGUAY', 'code' => '595'),
        'QA' => array('name' => 'QATAR', 'code' => '974'),
        'RO' => array('name' => 'ROMANIA', 'code' => '40'),
        'RS' => array('name' => 'SERBIA', 'code' => '381'),
        'RU' => array('name' => 'RUSSIAN FEDERATION', 'code' => '7'),
        'RW' => array('name' => 'RWANDA', 'code' => '250'),
        'SA' => array('name' => 'SAUDI ARABIA', 'code' => '966'),
        'SB' => array('name' => 'SOLOMON ISLANDS', 'code' => '677'),
        'SC' => array('name' => 'SEYCHELLES', 'code' => '248'),
        'SD' => array('name' => 'SUDAN', 'code' => '249'),
        'SE' => array('name' => 'SWEDEN', 'code' => '46'),
        'SG' => array('name' => 'SINGAPORE', 'code' => '65'),
        'SH' => array('name' => 'SAINT HELENA', 'code' => '290'),
        'SI' => array('name' => 'SLOVENIA', 'code' => '386'),
        'SK' => array('name' => 'SLOVAKIA', 'code' => '421'),
        'SL' => array('name' => 'SIERRA LEONE', 'code' => '232'),
        'SM' => array('name' => 'SAN MARINO', 'code' => '378'),
        'SN' => array('name' => 'SENEGAL', 'code' => '221'),
        'SO' => array('name' => 'SOMALIA', 'code' => '252'),
        'SR' => array('name' => 'SURINAME', 'code' => '597'),
        'ST' => array('name' => 'SAO TOME AND PRINCIPE', 'code' => '239'),
        'SV' => array('name' => 'EL SALVADOR', 'code' => '503'),
        'SY' => array('name' => 'SYRIAN ARAB REPUBLIC', 'code' => '963'),
        'SZ' => array('name' => 'SWAZILAND', 'code' => '268'),
        'TC' => array('name' => 'TURKS AND CAICOS ISLANDS', 'code' => '1649'),
        'TD' => array('name' => 'CHAD', 'code' => '235'),
        'TG' => array('name' => 'TOGO', 'code' => '228'),
        'TH' => array('name' => 'THAILAND', 'code' => '66'),
        'TJ' => array('name' => 'TAJIKISTAN', 'code' => '992'),
        'TK' => array('name' => 'TOKELAU', 'code' => '690'),
        'TL' => array('name' => 'TIMOR-LESTE', 'code' => '670'),
        'TM' => array('name' => 'TURKMENISTAN', 'code' => '993'),
        'TN' => array('name' => 'TUNISIA', 'code' => '216'),
        'TO' => array('name' => 'TONGA', 'code' => '676'),
        'TR' => array('name' => 'TURKEY', 'code' => '90'),
        'TT' => array('name' => 'TRINIDAD AND TOBAGO', 'code' => '1868'),
        'TV' => array('name' => 'TUVALU', 'code' => '688'),
        'TW' => array('name' => 'TAIWAN, PROVINCE OF CHINA', 'code' => '886'),
        'TZ' => array('name' => 'TANZANIA, UNITED REPUBLIC OF', 'code' => '255'),
        'UA' => array('name' => 'UKRAINE', 'code' => '380'),
        'UG' => array('name' => 'UGANDA', 'code' => '256'),
        'US' => array('name' => 'UNITED STATES', 'code' => '1'),
        'UY' => array('name' => 'URUGUAY', 'code' => '598'),
        'UZ' => array('name' => 'UZBEKISTAN', 'code' => '998'),
        'VA' => array('name' => 'HOLY SEE (VATICAN CITY STATE)', 'code' => '39'),
        'VC' => array('name' => 'SAINT VINCENT AND THE GRENADINES', 'code' => '1784'),
        'VE' => array('name' => 'VENEZUELA', 'code' => '58'),
        'VG' => array('name' => 'VIRGIN ISLANDS, BRITISH', 'code' => '1284'),
        'VI' => array('name' => 'VIRGIN ISLANDS, U.S.', 'code' => '1340'),
        'VN' => array('name' => 'VIET NAM', 'code' => '84'),
        'VU' => array('name' => 'VANUATU', 'code' => '678'),
        'WF' => array('name' => 'WALLIS AND FUTUNA', 'code' => '681'),
        'WS' => array('name' => 'SAMOA', 'code' => '685'),
        'XK' => array('name' => 'KOSOVO', 'code' => '381'),
        'YE' => array('name' => 'YEMEN', 'code' => '967'),
        'YT' => array('name' => 'MAYOTTE', 'code' => '262'),
        'ZA' => array('name' => 'SOUTH AFRICA', 'code' => '27'),
        'ZM' => array('name' => 'ZAMBIA', 'code' => '260'),
        'ZW' => array('name' => 'ZIMBABWE', 'code' => '263')
    );

    $new_cc = [];
    foreach ($countryArray as $index => $cc) {
        if (((string) ('+' . $cc['code'])) === $code) {
            return $index;
        }
        // $new_cc['+' . $cc['code']] = ['name' => $cc['name'], 'short_name' => $index];
    }
}
function TM_number_format($number)
{
    return number_format((float)$number, 2, '.', '');
}

function response_body($status, $message, $data = null, $mode = null, $custom_msg = false)
{
    $response = ['status' => $status, 'message' => $message];

    if (!is_null($data)) {
        $response['data'] = $data;
    }

    if (!is_null($mode)) {
        $response['mode'] = $mode;
    }

    if ($custom_msg) {
        $response['custom_msg'] = $custom_msg;
    }

    return $response;
}
function only_numbers($string = null)
{
    $string = preg_replace('/[^0-9.]+/', '', $string);
    return $string;
}
