<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Dompdf\Dompdf;

class Passenger extends Public_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('booking_info_model', 'booking');
        $this->load->model('passenger_model');

        if (!$this->session->userdata('logged_in_passenger')) {
            redirect(base_url('passenger/login'));
        }
    }


    public function dashboard()
    {
        $this->data['sub_content'] = 'accounts/passenger/_dashboard';
        $this->load->view(PASSENGER, $this->data);
    }

    public function profile()
    {

        if ($_POST) {
            $post = $this->input->post();
            $passenger_id = $this->session->userdata('logged_in_passenger');
            unset($post['old_password']);
            unset($post['password2']);
            $user_image = $_FILES['image'];
            if (!empty($user_image['name'])) {
                $user = $this->passenger_model->get($passenger_id);
                if ($user->image) {
                    $url = 'uploads/passenger/' . $user->image;
                    if (file_exists($url))
                        unlink($url);
                }
                $files_data = $this->common_library->upload_image('image', 'uploads/passenger/', 'passenger_' . time());
                $image_name = $files_data['filename'];
                $post['image'] = $image_name;
            }

            if ($this->passenger_model->update($post, array('id' => $passenger_id))) {
                $this->session->set_flashdata('msg', 'Successfully updated.');
            } else {
                $this->session->set_flashdata('dmsg', 'Nothing was updated.');
            }


            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->data['sub_content'] = 'accounts/passenger/_profile';
            $this->load->view(PASSENGER, $this->data);
        }
    }

    public
    function booking()
    {
        $passenger_id = $this->session->userdata('logged_in_passenger');
        $this->data['sub_content'] = 'accounts/passenger/_booking_history';
        $this->data['bookings'] = $this->booking->order_by('id', 'desc')->get_all(array('passenger_id' => $passenger_id,'job_status <>'=>'completed'));
        $this->data['passenger'] = $this->passenger_model->get($passenger_id);
        $this->load->view(PASSENGER, $this->data);
    }

    public
    function booking_history()
    {
        $passenger_id = $this->session->userdata('logged_in_passenger');
        $this->data['sub_content'] = 'accounts/passenger/_booking_history';
        $this->data['bookings'] = $this->booking->order_by('id', 'desc')->get_all(array('passenger_id' => $passenger_id,'job_status'=>'completed'));
        $this->data['passenger'] = $this->passenger_model->get($passenger_id);
        $this->load->view(PASSENGER, $this->data);
    }

    public
    function booking_details($booking_id)
    {
        $passenger_id = $this->session->userdata('logged_in_passenger');
        $this->data['sub_content'] = 'accounts/passenger/_booking_details';

        $this->data['booking'] = $this->booking->get(array('booking_ref_id' => $booking_id, 'passenger_id' => $passenger_id));
        //        debug($this->data['booking']);
        $this->data['passenger'] = $this->passenger_model->get($passenger_id);

        $this->load->view(PASSENGER, $this->data);
    }

    public
    function logout()
    {
        $this->session->unset_userdata('logged_in_passenger');
        $this->session->unset_userdata('passenger');
        $this->session->set_flashdata('msg', 'Logged out successfully.');
        redirect(site_url('passenger/login'));
    }


    public
    function ajax_checkPassword()
    {
        $post = $this->input->post();
        $email = $post['email'];
        $password = $post['password'];
        $count = $this->passenger_model->get(['email' => $email, 'password' => $password]);
        if ($count) {
            $response = array('isMatch' => '1');
        } else if (empty($count)) {
            $response = array('isMatch' => '0');
        }
        echo json_encode($response);
    }
}
