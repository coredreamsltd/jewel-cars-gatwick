<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Driver extends Public_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('driver_model', 'driver');
        $this->load->model('fleet_model');
        $this->load->model('drive_fleets_model');
        $this->load->model('file_model');
        $this->load->model('job_assign_model');
        $this->load->model('booking_info_model');
        if (!$this->session->userdata('logged_in_driver') && $this->uri->segment(2) !== 'register') {
            redirect(base_url('driver/register'));
        }
    }

    public function index()
    {
        redirect(site_url('driver/profile'));
    }
    public function register()
    {
        if ($_POST) {
            $post   = $this->input->post();
            $driver = $this->driver->get(array('email' => $post['email']));
            if (!$driver) {
                $driver_image = $_FILES['image'];
                if (!empty($driver_image['name'])) {
                    $files_data       = $this->common_library->upload_image('image', 'uploads/driver/', 'profile_driver' . time());
                    $post['image_id'] = $this->file_model->insert(['file_name' => $files_data['filename'], 'file_data' => json_encode($_FILES)]);
                }


                if ($this->driver->insert($post)) {
                    $mergeHTML = common_emogrifier($this->load->view('emailer/emailer_driver_registration', array('data' => $post), true));
                    $this->load->helper('email_helper');
                    if (email_help($post['email'], 'Driver Registration', $mergeHTML, [SITE_EMAIL => SITE_NAME])) {
                        $this->session->set_flashdata('msg', 'Account registered successfully. Please use your login credentials.');
                    } else {
                        $this->session->set_flashdata('dmsg', 'Account registered but email not sent. Please use your new credentials to login.');
                    }
                    redirect(base_url('driver/login'));
                } else {
                    show_error('Driver register failed.');
                }
            } else {
                $this->session->set_flashdata('dmsg', "Email ({$post['email']}) already exists.");
                redirect(base_url('driver/register'));
            }
        } else {
            $this->load->view('accounts/driver/_register', $this->data);
        }
    }

    public function profile()
    {

        $post      = $this->input->post();
        $driver_id = $this->session->userdata('logged_in_driver');

        if ($post) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->data['driver'] = $this->db->select('d.*,f.file_name as image')
                ->from('driver d')
                ->join('file f', 'f.id=d.image_id', 'left')
                ->where('d.id', $driver_id)
                ->get()->row();

            // Driver
            $this->data['sub_content'] = 'accounts/driver/_profile';
            $this->load->view(DRIVER, $this->data);
        }
    }

    function updateProfile($id = null)
    {
        $post = $this->input->post();
        //        debug($_FILES);
        if ($post && $id) {
            if (empty($post['password'])) {
                unset($post['password']);
            }
            if ($_FILES['driver_image']['name']) {
                $driver = $this->driver->get(['id' => $id]);
                $file   = $this->file_model->get(['id' => $driver->image_id]);

                if ($file) {
                    $url = 'uploads/driver/' . $file->file_name;
                    if (file_exists($url)) {
                        unlink($url);
                    }
                }
                $files_data       = $this->common_library->upload_image('driver_image', 'uploads/driver/', 'profile_' . $id . "_" . time());
                $post['image_id'] = $this->file_model->insert(['file_name' => $files_data['filename'], 'file_data' => json_encode($_FILES)]);
            }

            $this->driver->update($post, ['id' => $id]);
            set_flash('msg', 'Successfully Updated.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function vehicleAndDocument()
    {
        $driver_id            = $this->session->userdata('logged_in_driver');
        $this->data['driver'] = $this->db->select(
            'd.*,
        f.file_name as image,f1.file_name as file_pco_driver_batch,
        f2.file_name as file_hire_agreement,f3.file_name as file_driver_licence_photo_front,
        f4.file_name as file_driver_licence_photo_back,f5.file_name as file_driver_taxi_licence,
        f6.file_name as file_other_docs,f7.file_name as file_tc_signed_copy,
         f8.file_name as file_pco_licence,f9.file_name as file_pco_badge
        '
        )
            ->from('driver d')
            ->join('file f', 'f.id=d.image_id', 'left')
            ->join('file f1', 'f1.id=d.file_pco_driver_batch_id', 'left')
            ->join('file f2', 'f2.id=d.file_hire_agreement_id', 'left')
            ->join('file f3', 'f3.id=d.file_driver_licence_photo_front_id', 'left')
            ->join('file f4', 'f4.id=d.file_driver_licence_photo_back_id', 'left')
            ->join('file f5', 'f5.id=d.file_driver_taxi_licence_id', 'left')
            ->join('file f6', 'f6.id=d.file_other_docs_id', 'left')
            ->join('file f7', 'f7.id=d.file_tc_signed_copy_id', 'left')
            ->join('file f8', 'f8.id=d.file_pco_licence_id', 'left')
            ->join('file f9', 'f9.id=d.file_pco_badge_id', 'left')
            ->where('d.id', $driver_id)
            ->get()->row();

        $this->data['driver_fleets'] = $this->db->select(
            'df.*,v.title as title,
        f.file_name as file_phv_licence,f1.file_name as file_mot,
        f2.file_name as file_taxi_private_hire_insurance,f3.file_name as file_vehicle_log,
        f4.file_name as file_road_tax
        '
        )
            ->from('driver_fleets df')
            ->join('tbl_fleets v', 'v.id=df.fleet_id', 'left')
            ->join('file f', 'f.id=df.file_phv_licence_id', 'left')
            ->join('file f1', 'f1.id=df.file_mot_id', 'left')
            ->join('file f2', 'f2.id=df.file_taxi_private_hire_insurance_id', 'left')
            ->join('file f3', 'f3.id=df.file_vehicle_log_id', 'left')
            ->join('file f4', 'f4.id=df.file_road_tax_id', 'left')
            ->where('df.driver_id', $driver_id)
            ->get()->result();

        $this->data['fleets']      = $this->fleet_model->get_all();
        $this->data['sub_content'] = 'accounts/driver/_vehicle_and_document';
        $this->load->view(DRIVER, $this->data);
    }

    function updateDriverDocuments($id = null)
    {
        $post = $this->input->post();
        foreach ($_FILES as $index => $doc_file) {

            if ($doc_file['name']) {
                $driver = $this->driver->get(['id' => $id]);
                $file   = $this->file_model->get(['id' => $driver->{$index . '_id'}]);

                if ($file) {
                    $url = 'uploads/documents/driver/' . $id . '/' . $file->file_name;
                    if (file_exists($url)) {
                        unlink($url);
                    }
                }
                $files_data           = $this->common_library->upload_image($index, 'uploads/documents/driver/' . $id . '/', $index . "_" . $id . "_" . time());
                $post[$index . '_id'] = $this->file_model->insert(['file_name' => $files_data['filename'], 'file_data' => json_encode($doc_file)]);
            }
            //                debug($post);
        }

        if (!empty($post['dvla_licence_expiry_date']))
            $post['dvla_licence_expiry_date'] = DateTime::createFromFormat('m/d/Y', $post['dvla_licence_expiry_date'])->format('Y-m-d');

        if (!empty($post['insurance_expiry_date']))
            $post['insurance_expiry_date']    = DateTime::createFromFormat('m/d/Y', $post['insurance_expiry_date'])->format('Y-m-d');
        if (!empty($post['pco_driver_expiry_date']))
            $post['pco_driver_expiry_date']   = DateTime::createFromFormat('m/d/Y', $post['pco_driver_expiry_date'])->format('Y-m-d');
        $this->driver->update($post, ['id' => $id]);
        set_flash('msg', 'Driver Documents Updated.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function addVehicle($fleet_id = null)
    {
        $post = $this->input->post();
        //        debug($post);
        if ($post) {
            $this->drive_fleets_model->insert($post);
            set_flash('msg', 'Vehicle Added.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function uploadDocument($driver_id = null)
    {
        $post = $this->input->post();
        //        debug($post);

        if ($_FILES['document']['name']) {
            $driver_fleet = $this->drive_fleets_model->get(['id' => $post['df_id']]);
            $file         = $this->file_model->get(['id' => $driver_fleet->$post['field_name']]);

            if ($file) {
                $url = 'uploads/documents/driver/' . $driver_id . '/' . $file->file_name;
                if (file_exists($url)) {
                    unlink($url);
                }
            }
            $files_data                   = $this->common_library->upload_image(
                'document',
                'uploads/documents/driver/' . $driver_id . '/',
                $post['document_name'] . "_" . $driver_id . "_" . $post['df_id'] . "_" . time()
            );
            $db_data[$post['field_name']] = $this->file_model->insert(['file_name' => $files_data['filename'], 'file_data' => json_encode($_FILES)]);
        }

        (isset($post['exp_date'])) ? $db_data[$post['field_name_expiry']] = DateTime::createFromFormat('d/m/Y', $post['exp_date'])->format('Y-m-d') : '';
        $this->drive_fleets_model->update($db_data, ['id' => $post['df_id']]);
        //        debug($db_data);
        set_flash('msg', 'Documents Saved.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function updateExpDate($driver_id = null)
    {
        $post = $this->input->post();
        //        debug($post);
        $db_data[$post['new_exp_filed_name']] = DateTime::createFromFormat('d/m/Y', $post['new_exp_date'])->format('Y-m-d');
        $this->drive_fleets_model->update($db_data, ['id' => $post['df_id']]);
        //        debug($db_data);
        set_flash('msg', 'Expiry Date Saved.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function changeColor($df_id = null)
    {
        $post = $this->input->post();
        //        debug($post);
        if (!$post) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->drive_fleets_model->update($post, ['id' => $df_id]);
        set_flash('msg', 'Color Saved.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function change_fleet_make_model_registration($df_id = null)
    {
        $post = $this->input->post();
        //        debug($post);
        if (!$post) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->drive_fleets_model->update($post, ['id' => $df_id]);
        set_flash('msg', 'Data Saved.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function deleteVehicle($df_id = null)
    {
        $this->drive_fleets_model->delete(['id' => $df_id]);
        set_flash('msg', 'Vehicle Deleted.');
        redirect($_SERVER['HTTP_REFERER']);
    }


    public function logout()
    {
        $this->session->unset_userdata('logged_in_driver');
        $this->session->set_flashdata('msg_success', 'Logged out successfully.');
        redirect(site_url('driver/login'));
    }

    function jobs()
    {
        $driver_id = $this->session->userdata('logged_in_driver');
        //        $this->data['jobs'] = $this->driver->allJobs($driver_id);
        $this->data['jobs'] = $this->booking_info_model->order_by('id', 'desc')->get_all(['driver_id' => $driver_id]);
        $this->data['sub_content'] = 'accounts/driver/_jobs';
        $this->load->view(DRIVER, $this->data);
    }

    public function jobDetails($booking_id)
    {
        $this->data['sub_content'] = 'accounts/driver/_job_details';
        $this->data['booking']     = $this->booking_info_model->get(['id' => $booking_id]);
        if ($this->data['booking']) {
            $this->load->view(DRIVER, $this->data);
        } else {
            show_error('Sorry, the booking is not found', 400);
        }
    }

    public function rejectJob()
    {
        $post    = $this->input->post();
        $booking = $this->booking_info_model->get(['id' => $post['booking_id']]);
        $driver  = $this->driver->get(['id' => $post['driver_id']]);

        $this->booking_info_model->update(['job_status' => 'rejected'], ['id' => $post['booking_id']]);
        //        $is_update  = $this->job_assign_model->update(['status' => 'reject'], ['booking_id' => $post['booking_id'], 'driver_id' => $post['driver_id']]);
        $email_data = ['booking' => $booking, 'driver' => $driver,];

        $mergeHTML = common_emogrifier($this->load->view('emailer/emailer_driver_reject_assign_job', $email_data, true));
        $this->load->helper('email_helper');

        $is_email = email_help([ADMIN_EMAIL], 'Job Rejected  - [BK.ID' . $booking->booking_ref_id . ']', $mergeHTML, [SITE_EMAIL => SITE_NAME]);
        if (!$is_email) {
            set_flash('msg', 'Error occur while sending email.');
        }

        set_flash('msg', 'Job Rejected');
        redirect(site_url('accounts/driver/jobs'));
    }

    public function acceptJob()
    {
        $post = $this->input->post();

        $booking    = $this->booking_info_model->get(['id' => $post['booking_id']]);
        $driver     = $this->driver->get(['id' => $post['driver_id']]);
        $this->booking_info_model->update(['job_status' => 'accepted'], ['id' => $post['booking_id']]);
        //        $is_update  = $this->job_assign_model->update(['status' => 'completed'], ['booking_id' => $post['booking_id'], 'driver_id' => $post['driver_id']]);
        $email_data = ['booking' => $booking, 'driver' => $driver,];

        $mergeHTML = common_emogrifier($this->load->view('emailer/emailer_driver_accept_assign_job', $email_data, true));

        $this->load->helper('email_helper');
        $is_email = email_help($booking->client_email, 'Booking Confirm - [BK.ID' . $booking->booking_ref_id . ']', $mergeHTML, [SITE_EMAIL => SITE_NAME]);
        set_flash('msg', 'Job accepted successfully and email has been sent to passenger. Thank you!');
        if (!$is_email) {
            set_flash('msg', 'Error occur while sending email.');
        }


        redirect(site_url('accounts/driver/jobs'));
    }
}
