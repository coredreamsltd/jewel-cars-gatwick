<?php

class Mail extends Public_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('testimonials_model');
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->load->helper('form');
        $this->load->helper('email_helper');
    }

    function contact()
    {
       
        $post = $this->input->post();
        if (!$post) {
            set_flash('dmsg', 'Warning! Invalid method call.');
            redirect($_SERVER['HTTP_REFERER']);
        }

        if (!checkGoogleCaptcha()) {
            set_flash('dmsg', "Captcha verification not passed! Please try again.");
            redirect($_SERVER['HTTP_REFERER']);
        }
        unset($post['g-recaptcha-response']);
        $emailer = $this->load->view('emailer/_emailer_contact', $post, true);
        email_help([ADMIN_EMAIL, ADMIN_EMAIL_1], "Contact Query - " . SITE_NAME, $emailer, [SITE_EMAIL => SITE_NAME]);
        set_flash('msg', 'Thank you for your query. We will get back to you shortly.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function review()
    {
        $post = $this->input->post();
        if (!$post) {
            set_flash('dmsg', 'Warning! Invalid method call.');
            redirect($_SERVER['HTTP_REFERER']);
        }

        // if (!checkGoogleCaptcha()) {
        //     set_flash('dmsg', "Captcha verification not passed! Please try again.");
        //     redirect($_SERVER['HTTP_REFERER']);
        // }
        // unset($post['g-recaptcha-response']);

        $post['user_agent_data'] = json_encode(array(
            'Browser' => $this->agent->browser(),
            'Version' => $this->agent->version(),
            'OS' => $this->agent->platform(),
            'Mobile Device' => $this->agent->mobile(),
            'IP Address' => $this->input->ip_address()
        ));
        $this->testimonials_model->insert($post);
        set_flash('msg', 'Thank you for your valuable review.');
        redirect($_SERVER['HTTP_REFERER']);
    }
}
