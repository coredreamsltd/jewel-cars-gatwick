      <?php

        defined('BASEPATH') or exit('No direct script access allowed');

        class Page extends Public_Controller
        {

            public function __construct()
            {
                parent::__construct();
                $this->load->model('fleet_model');
                $this->load->model('pages_model');
                $this->load->model('testimonials_model');
                //   debug('sdsd');
            }

            public function index()
            {

                $slug = $this->uri->segment(1);
                $result = $this->pages_model->get(array('slug' => $slug, 'template' => 'business_page'));

                if ($result) {
                    $this->data['template_data'] = $result;
                    $this->data['main_content'] = 'frontend/pages/templates/business';
                    $this->load->view(FRONTEND, $this->data);
                    return;
                }

                switch ($slug) {
                    case "school-transport":
                        $this->data['main_content'] = 'frontend/pages/school-transport';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "business":
                        $this->data['main_content'] = 'frontend/pages/business';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "about":
                        $this->data['main_content'] = 'frontend/pages/about';
                        $this->load->view(FRONTEND, $this->data);
                        break;
                    case "faq":
                        $this->data['main_content'] = 'frontend/pages/faq';
                        $this->data['faqs'] = $this->db->get('tbl_faqs', '', 'order ASC')->result();
                        $this->load->view(FRONTEND, $this->data);
                        break;
                    case "work-with-us":
                        $this->data['main_content'] = 'frontend/pages/work-with-us';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "coronavirus":
                        $this->data['main_content'] = 'frontend/pages/coronavirus';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "gdpr":
                        $this->data['main_content'] = 'frontend/pages/gdpr';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "privacy-policy":
                        $this->data['main_content'] = 'frontend/pages/privacy-policy';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "terms-and-conditions":
                        $this->data['main_content'] = 'frontend/pages/terms-and-conditions';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "cookies-policy":
                        $this->data['main_content'] = 'frontend/pages/cookies-policy';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "contact":
                        $this->data['main_content'] = 'frontend/pages/contact';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "thank-you":
                        $this->data['main_content'] = 'frontend/pages/thank-you';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    case "reviews":
                        $this->data['testimonials'] = $this->testimonials_model->order_by('post_date', 'desc')->get_all(['status' => 1]);
                        $this->data['main_content'] = 'frontend/pages/review';
                        $this->load->view(FRONTEND, $this->data);
                        break;

                    default:
                        $this->data['main_content'] = 'frontend/pages/404';
                        $this->load->view(FRONTEND, $this->data);
                        break;
                }
            }
        }
