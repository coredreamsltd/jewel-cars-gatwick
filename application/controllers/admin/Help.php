<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Help extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('help_model');
        $this->load->model('help_detail_model');
    }
    public function index()
    {
        $this->data['helps'] = $this->help_model->get_all();
        $this->data['main_content'] = 'admin/help/index';
        $this->data['sub_content'] = 'admin/help/_help';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update()
    {
        $id = segment(4);
        $post = $this->input->post();
        if ($post) {

            if (!empty($_FILES['image']['name'])) {
                $page = $this->help_model->get(array('id' => segment(4)));
                if (!empty($page->image)) {
                    $url = 'uploads/help/' . $page->image;
                    if (file_exists($url))
                        unlink($url);
                }

                $files_data = $this->common_library->upload_image('image', FCPATH . implode(DIRECTORY_SEPARATOR, ['uploads', 'help']), 'help_' . time());
                $post['image'] = $files_data['filename'];
            }

            if ($id == '') {
                $this->help_model->insert($post);
                set_flash('msg', 'Query saved');
            } else {
                $this->help_model->update($post, array('id' => $id));
                set_flash('msg', 'Query updated');
            }
            redirect('admin/help');
        } else {
            $this->data['main_content'] = 'admin/help/index';
            $this->data['sub_content'] = 'admin/help/_help_form';
            if ($id != '') {
                $this->data['help'] = $this->help_model->get($id);
            }
            $this->load->view(BACKEND, $this->data);
        }
    }

    function delete($id = null)
    {
        $this->help_model->delete(['id' => $id]);
        $this->help_detail_model->delete(array('help_id' => $id));
        set_flash('msg', 'Query Deleted Successfully');
        redirect('admin/help');
    }

    function query($help_id = null)
    {
        $this->data['qas'] = $this->help_detail_model->get_all(['help_id' => $help_id]);
        $this->data['main_content'] = 'admin/help/index';
        $this->data['sub_content'] = 'admin/help/_help_qa';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update_qa()
    {
        $id = segment(4);
        $help_id = $_GET['help_id'];
        $post = $this->input->post();
        if ($post) {

            if (!empty($_FILES['image']['name'])) {
                $page = $this->help_detail_model->get(array('id' => segment(4)));
                if (!empty($page->image)) {
                    $url = 'uploads/help/' . $page->image;
                    if (file_exists($url))
                        unlink($url);
                }

                $files_data = $this->common_library->upload_image('image', FCPATH . implode(DIRECTORY_SEPARATOR, ['uploads', 'help']), 'help_qa_' . time());
                $post['image'] = $files_data['filename'];
            }

            if ($id == '') {
                $this->help_detail_model->insert($post);
                set_flash('msg', 'Query & answer saved');
            } else {
                $this->help_detail_model->update($post, array('id' => $id));
                set_flash('msg', 'Query & answer updated');
            }
            redirect('admin/help/query/'.$help_id);
        }

        $this->data['main_content'] = 'admin/help/index';
        $this->data['sub_content'] = 'admin/help/_help_qa_form';
        if ($id != '') {
            $this->data['qa'] = $this->help_detail_model->get($id);
        }
        $this->load->view(BACKEND, $this->data);
    }

    function delete_qa($id = null)
    {
        $help_id = $_GET['help_id'];
        $this->help_detail_model->delete(array('id' => $id));
        set_flash('msg', 'Query Deleted Successfully');
        redirect('admin/help/query/'.$help_id);
    }
}
