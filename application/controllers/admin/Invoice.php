<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        if (!is_admin_menu_accessible(10)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        $this->load->model('passenger_model');
        $this->load->model('invoice_model');
    }

//    function index()
//    {
//
//        if (isset($_GET['account_id'])) {
//            $account_id = $_GET['account_id'];
//
//            $data['account'] = $this->account_model->get_by_id($account_id);
//            $invoices_booking = $this->db
//                ->select('i.*')
//                ->from('tbl_invoice as i')
//                ->join('tbl_bookings as b', 'i.booking_id = b.booking_id')
//                ->where(array('b.account_id' => $account_id))
//                ->order_by('i.id', 'DESC')
//                ->get()->result_array();
//
//            $invoices_booking_cart = $this->db
//                ->select('i.*')
//                ->from('tbl_invoice as i')
//                ->join('tbl_bookings_cart as bc', 'i.cart_id = bc.cart_id')
//                ->where(array('bc.account_id' => $account_id))
//                ->order_by('i.id', 'DESC')
//                ->get()->result_array();
//
//            $invoices_booking_bulk = $this->db
//                ->select('i.*')
//                ->from('tbl_invoice as i')
//                ->join('tbl_invoice_bulk as ib', 'i.invoice_id = ib.invoice_id')
//                ->join('tbl_bookings as b', 'b.booking_id = ib.booking_id')
//                ->where(array('b.account_id' => $account_id))
//                ->order_by('i.id', 'DESC')
//                ->group_by('i.invoice_id')
//                ->get()->result_array();
//
//            $data['invoices'] = array_merge($invoices_booking, $invoices_booking_cart, $invoices_booking_bulk);
//        } else {
//            $data['invoices'] = $this->db->order_by('id', 'DESC')->get('tbl_invoice')->result_array();
//        }
//
//        $data['main_content'] = 'admin/invoice/index';
//        $data['sub_content'] = 'admin/invoice/_invoices';
//        $this->load->view(BACKEND, $data);
//    }

    function index() {

        $per_page_limit = $_GET['per_page_limit'] = 10;

        if (isset($_GET['account_id'])) {
            $account_id = $_GET['account_id'];
            $this->data['account'] = $this->passenger_model->get($account_id);
            $this->data['invoices'] = $this->invoice_model->search($_GET);
        } else {
            $this->data['invoices'] = $this->invoice_model->search($_GET);
        }
        unset($_GET['per_page_limit']);
        $this->data['total_booking_rows'] = $this->invoice_model->search($_GET, true);

        // Pagination
        $this->load->library('Pagination');
        $config['base_url'] = site_url('admin/invoice?search='.$this->input->get('search', TRUE).'&account_id='.$this->input->get('account_id', TRUE));
        $config['total_rows'] = $this->data['total_booking_rows'];
        $config['page_query_string'] = true;
        $config['per_page'] = $per_page_limit;
        $this->pagination->initialize($config);

        $this->data['main_content'] = 'admin/invoice/index';
        $this->data['sub_content'] = 'admin/invoice/_invoices';
        $this->load->view(BACKEND, $this->data);
    }

    function details($invoice_id) {
        $this->data['invoice'] =$this->invoice_model->get(array('invoice_id' => $invoice_id));

        if ($this->data['invoice']) {
            $this->data['main_content'] = 'admin/invoice/index';
            $this->data['sub_content'] = 'admin/invoice/_details';
            $this->load->view(BACKEND, $this->data);
        } else {
            die('Invalid invoice id: ' . $invoice_id);
        }
    }

    function receipt_bulk() {

        $post = $this->input->post();

        if (isset($post['checked_item'])) {
            $this->data['invoice_ids'] = array();

            $this->data['invoice_total'] = 0;
            foreach ($post['checked_item'] as $index => $ci) {
                $invoice = $this->common_model->get_where('tbl_invoice', array('id' => $ci));

                if ($invoice[0]['payment_status'] && $invoice[0]['receipt_mailed_datetime'] > 0) {
                    show_error("Invoice id: {$invoice[0]['invoice_id']} receipt has been generated & emailed already. Please remove it from bulk receipt list.");
                } else if ($invoice[0]['cart_id']) {
                    show_error("Invoice id: {$invoice[0]['invoice_id']} is a cart invoice. It can not be included in bulk receipt. Please remove it from bulk receipt list.");
                } else if (!$invoice[0]['booking_id'] && !$invoice[0]['cart_id']) {
                    show_error("Invoice id: {$invoice[0]['invoice_id']} is already a bulk invoice. Please send its receipt from its own send receipt button from its invoice details.");
                } else {
                    array_push($this->data['invoice_ids'], $invoice[0]['invoice_id']);
                    // calculate total invoice amount
                    $this->data['invoice_total'] += $invoice[0]['invoice_amount'];
                }
            }
            $this->data['main_content'] = 'admin/invoice/index';
            $this->data['sub_content'] = 'admin/invoice/_send_receipt_bulk';
            $this->load->view(BACKEND, $this->data);
        } elseif (isset($post['i_invoice_ids'])) {
            $newArr = array();
            foreach ($post as $k => $v) {
                if ($k == 'i_invoice_ids')
                    continue;
                $newArr[substr($k, 2)] = $v;
            }

            $recipients = explode(',', $newArr['customer_email']);

            foreach ($recipients as $e) {
                if (!filter_var($e, FILTER_VALIDATE_EMAIL)) {
                    die('Recipients Emails (' . $newArr['customer_email'] . ') has an invalid email.');
                }
            }

            unset($newArr['customer_name']);

            $invoice_ids = explode(',', $post['i_invoice_ids']);
            foreach ($invoice_ids as $i_id) {
                $this->db->update('tbl_invoice', array(
                    'payment_status' => 1, 
                    'mail_subject_receipt' => $newArr['mail_subject'],
                    'mail_template_receipt' => $newArr['mail_template'],
                    'receipt_mailed_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s')
                    ), array('invoice_id' => $i_id));
            }

            $param = array(
                'from' => array(getSiteOptions('site_email') => getSiteOptions('site_name')),
                'reply' => array(getSiteOptions('site_email') => getSiteOptions('site_name')),
                'recipients' => $recipients,
                'subject' => $newArr['mail_subject'],
                'msg' => $newArr['mail_template']
            );
            swiftsend($param);

            set_flash('msg', 'All invoice marked as paid and receipt emailed to: ' . $newArr['customer_email']);
            redirect(base_url('admin/invoice'));
        }
    }
    
    function ajax_preview_receipt_bulk() {

        $post = NVPToArray($_POST['formdata']);

        $post['invoice_ids'] = explode(',', $post['invoice_ids']);

        $receipt_mail_template = $this->load->view('emailer/_emailer_receipt_bulk', array('data' => $post), true);

        $data = array(
            'invoice_ids' => $post['invoice_ids'],
            'customer_name' => $post['customer_name'],
            'customer_email' => $post['customer_email'],
            'receipt_amount' => $post['receipt_amount'],
            'mail_subject' => $post['mail_subject'],
            'mail_template' => $receipt_mail_template
        );
        jsonOutput(1, 'Email template preview', $data);
    }


}
