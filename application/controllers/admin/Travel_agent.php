<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/2/2017
 * Time: 11:30 AM
 */

class Travel_agent extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!is_admin_menu_accessible(6)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        $this->load->model('passenger_model');
    }

    function index()
    {
        $this->data['count']['today'] = $this->db->select("DATE_FORMAT(created_at, '%Y-%m-%d'),COUNT(*) as count")
                            ->from('passenger')->where("date(created_at) = CURDATE() AND type = 'travel_agent'")
                            ->get()->row()->count;

        $this->data['count']['month'] =  $this->db->select("MONTH(created_at),COUNT(*) as count")
                            ->from('passenger')->where("MONTH(created_at) = MONTH(CURDATE()) AND type = 'travel_agent'")
                            ->get()->row()->count;

        $this->data['count']['annual'] =  $this->db->select("YEAR(created_at),COUNT(*) as count")
                            ->from('passenger')->where("YEAR(created_at) = YEAR(CURDATE()) AND type = 'travel_agent'")
                            ->get()->row()->count;

        
        $this->data['travel_agents'] = $this->passenger_model->get_all(['type'=>'travel_agent']);
        $this->data['main_content'] = 'admin/travel_agent/index';
        $this->data['sub_content'] = 'admin/travel_agent/travel_agent';
        $this->load->view(BACKEND, $this->data);
    }

    function update($id = null)
    {
        $post = $this->input->post();
        if ($post) {
            $passenger = $_FILES['image'];

            $image_name = 'default.png';

            if (!empty($passenger['name'])) {
                $passenger = $this->passenger_model->get(array('id' => $id));

                if ($passenger) {
                    $url = 'uploads/passenger/' . $passenger->image;
                    if (file_exists($url))
                        unlink($url);
                }
                $files_data = $this->common_library->upload_image('image', 'uploads/passenger/', 'passenger_' . time());
                $image_name = $files_data['filename'];
                $post['image'] = $image_name;
            }

            if ($id == '') {
                $post['image'] = $image_name;
                $post['type'] = 'corporate';
                $this->passenger_model->insert($post);
            } else {
                $this->passenger_model->update($post, array('id' => $id));
            }
            $this->session->set_flashdata('msg', "Travel Agent Details Saved.");

            redirect('admin/travel_agent');
        } else {
            $this->data['main_content'] = 'admin/travel_agent/index';
            $this->data['sub_content'] = 'admin/travel_agent/form';

            $this->data['isNew'] = true;
            if ($id != '') {
                $this->data['isNew'] = false;
                $this->data['passenger'] = $this->passenger_model->get(['id' => $id]);
            }
            $this->load->view(BACKEND, $this->data);
        }
    }
    function delete($id=null) {
        $passenger = $this->passenger_model->get($id);
        $url = 'uploads/passenger/' . $passenger->image;
        if (file_exists($url))
            unlink($url);
        $this->passenger_model->delete(['id'=>$id]);
        $this->session->set_flashdata('msg', 'Successfully! Travel Agent deleted');
        redirect('admin/travel_agent');
    }
}