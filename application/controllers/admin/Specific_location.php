<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 10:26 AM
 */

class Specific_location extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('specific_location_model');
    }

    function index()
    {
        $this->data['areas'] = $this->specific_location_model->getAllArea();
        $this->data['main_content'] = 'admin/specific_location/index';
        $this->data['sub_content'] = 'admin/specific_location/location';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update($id = null)
    {
        if ($this->input->post()) {
            $zone_data = array(
                'title' => $this->input->post('title'),
                'type' => $this->input->post('type'),
                'lat' => $this->input->post('lat'),
                'lng' => $this->input->post('lng'),
                'instructions' => $this->input->post('instructions'),
                'coordinates' => $this->input->post('coordinates')
            );

            if ($id) {
                $this->specific_location_model->updateArea($id, $zone_data);
                set_flash('msg', 'Update success.');
                redirect(site_url('admin/specific-location/add-update/' . $id));
            }

            $this->specific_location_model->insertArea($zone_data);
            set_flash('msg', 'Insert success.');
            redirect(site_url('admin/specific-location'));
        }

        $this->data['isEdit'] = false;
        if ($id) {
            $this->data['area'] = $this->specific_location_model->getArea($id);;
            $this->data['isEdit'] = true;
        }

        $this->data['main_content'] = 'admin/specific_location/index';
        $this->data['sub_content'] = 'admin/specific_location/form';
        $this->load->view(BACKEND, $this->data);
    }
    function delete($id = null)
    {
        $this->specific_location_model->delete($id);
        set_flash('msg', 'Successfully! deleted');
        redirect('admin/specific-location');
    }
}
