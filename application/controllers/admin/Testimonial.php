<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/2/2017
 * Time: 9:10 AM
 */

class Testimonial extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('testimonials_model');
    }

    function index()
    {
        $this->data['testimonials'] = $this->testimonials_model->get_all();
        $this->data['main_content'] = 'admin/testimonial/index';
        $this->data['sub_content'] = 'admin/testimonial/testimonial';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update($id = null)
    {
        $post = $this->input->post();
        if ($post) {
            if ($id == '') {
                $this->testimonials_model->insert($post);
            } else {
                $this->testimonials_model->update($post, array('id' => $id));
            }
            $this->session->set_flashdata('msg', "Testimonial Saved.");

            redirect('admin/testimonial');
        } else {
            $this->data['main_content'] = 'admin/testimonial/index';
            $this->data['sub_content'] = 'admin/testimonial/form';

            $this->data['isNew'] = true;
            if ($id != '') {
                $this->data['isNew'] = false;
                $this->data['testimonial'] = $this->testimonials_model->get(['id' => $id]);
            }
            $this->load->view(BACKEND, $this->data);

        }
    }

    function delete($id = null)
    {
        $this->testimonials_model->delete(['id'=>$id]);
        $this->session->set_flashdata('msg', 'Successfully! Testimonial deleted');
        redirect('admin/testimonial');
    }
}