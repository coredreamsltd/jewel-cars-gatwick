<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rate extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('location_model');
        $this->load->model('location_type_model');
        $this->load->model('rate_model');
        $this->load->model('fleet_model');
    }

    function index()
    {
        $this->data['rates'] = $this->rate_model->get_all();
        $this->data['main_content'] = 'admin/rate/index';
        $this->data['sub_content'] = 'admin/rate/rate_list';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update()
    {
        $id = segment(4);
        $post = $this->input->post();
        $this->data['fleets'] =  $this->fleet_model->order_by('sort')->get_all(['status' => 1]);
        if ($post) {
            $post['from'] = json_encode($post['from']);
            $post['to'] = json_encode($post['to']);
            $post['from_id'] = json_encode($post['from_id']);
            $post['to_id'] = json_encode($post['to_id']);
            $post['fleet_rates'] = json_encode($post['fleet_rates']);

            if ($id) {
                $update =  $this->rate_model->update($post, ['id' => $id]);
                set_flash('msg', "Routes Rate updated.");
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $insert = $this->rate_model->insert($post);
                set_flash('msg', "Routes Rate saved.");
                redirect(site_url('admin/rate'));
            }
        }
        $this->data['rate'] =  $this->rate_model->get(['id' => $id]);
        // debug($this->data['fleets']);
        $this->data['main_content'] = 'admin/rate/index';
        $this->data['sub_content'] = 'admin/rate/rate';
        $this->load->view(BACKEND, $this->data);
    }

    function delete()
    {
        $this->common_model->delete_data('rate', array('id' => segment(4)));
        set_flash('msg', 'Rate successfully deleted.');
        redirect('admin/rate');
    }

    function ajax_validate_location()
    {
        $post = $this->input->post();
        if ($post['type'] == 'from') {
            $to_ids =  array_unique(explode(",", $post['t_id']));
            foreach ($to_ids as $to_id) {
                $this->db->select("*");
                $this->db->like('from_id', '"'.$post['f_id'].'"');
                $this->db->like('to_id', '"'.$to_id.'"');
                $result = $this->db->get("rate")->row();
                if (!empty($result)) {
                    echo json_encode(['status' => false, 'msg' => 'location already exists!', 'data' => '']);
                    return;
                }
            }
            echo json_encode(['status' => true, 'msg' => '', 'data' => '']);
        } else {
            $from_ids =  array_unique(explode(",", $post['f_id']));
            foreach ($from_ids as $from_id) {
                $this->db->select("*");
                $this->db->like('from_id', $from_id);
                $this->db->like('to_id', $post['t_id']);
                $result = $this->db->get("rate")->row();
               if (!empty($result)) {
                    echo json_encode(['status' => false, 'msg' => 'location already exists!', 'data' => '']);
                    return;
                }
            }
            echo json_encode(['status' => true, 'msg' => '', 'data' => '']);
        }
    }



    function ajax_auto_complete()
    {
        $post = $_GET["query"];
        $results = $this->location_model->location_search($post);
        foreach ($results as $result) {
            $response_data[] = ['label' => $result->name, 'id' => $result->id];
        }
        echo json_encode($response_data);
    }

    function update_single_route_rate()
    {
        if ($_POST) {
            $post = $_POST;

            $data['rate'] = $post['rate'];

            $this->common_model->update('routes_rate', $data, array('id' => $post['routes_rate_id']));

            set_flash('msg', 'Routes Rate saved');
            redirect('admin/rate/routes-rate');
        }

        $routes_rate_id = segment(4);

        if ($routes_rate_id == '')
            redirect('admin/rate/routes-rate');

        $routes_rates = $this->common_model->get_where('routes_rate', array('id' => $routes_rate_id));

        $from = $this->common_model->get_where('locations', array('id' => $routes_rates[0]['from_id']));
        $routes_rates[0]['from_name'] = $from[0]['name'];

        $to = $this->common_model->get_where('locations', array('id' => $routes_rates[0]['to_id']));
        $routes_rates[0]['to_name'] = $to[0]['name'];

        $this->data['routes_rate'] = $routes_rates[0];
        $this->data['main_content'] = 'admin/rate/index';
        $this->data['sub_content'] = 'admin/rate/_single_route_rate_form';

        $this->load->view(BACKEND, $this->data);
    }
}
