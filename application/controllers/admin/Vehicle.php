<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 10:26 AM
 */

class Vehicle extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('vehicle_model');
        $this->load->model('driver_model');
        $this->load->model('fleet_model');
        $this->load->model('manufacturer_model');
        $this->load->model('manufacturer_model_model');
    }

    function index()
    {
        $this->data['vehicles'] = $this->vehicle_model->get_all();
        $this->data['main_content'] = 'admin/vehicle/index';
        $this->data['sub_content'] = 'admin/vehicle/vehicle';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update($id = null)
    {
        $this->data['fleets'] = $this->fleet_model->get_all();
        $this->data['manufacturers'] = $this->manufacturer_model->get_all();
        $post = $this->input->post();
        if ($post) {
            if ($id == '') {
                $this->vehicle_model->insert($post);
            } else {
                $this->vehicle_model->update($post, array('id' => $id));
            }
            set_flash('msg', "Vehicle saved.");
            redirect('admin/vehicle');
        } else {
            $this->data['main_content'] = 'admin/vehicle/index';
            $this->data['sub_content'] = 'admin/vehicle/form';
            $this->data['isNew'] = true;
            if ($id != '') {
                $this->data['isNew'] = false;
                $this->data['vehicle'] = $this->vehicle_model->get(['id' => $id]);
            }
            $this->load->view(BACKEND, $this->data);
        }
    }
    function delete($id = null)
    {
        $this->vehicle_model->delete($id);
        set_flash('msg', 'Successfully! Vehicle deleted');
        redirect('admin/vehicle');
    }

    function ajaxCheckManufacture($make = null)
    {
        $manufacturer = $this->manufacturer_model->get(['slug' => $make]);
        if (empty($manufacturer)) {
            jsonOutput(false, 'Not added.');
        }
        jsonOutput(true, 'Already added.');
    }

    function ajaxCheckModel($model = null)
    {
        $manufacturer_model = $this->manufacturer_model_model->get(['slug' => $model]);
        if (empty($manufacturer_model)) {
            jsonOutput(false, 'Not added.');
        }
        jsonOutput(true, 'Already added.');
    }

    function ajaxAddManufacture($make = null)
    {
        $data = [
            'name' => $make,
            'slug' => strtolower(str_replace(' ', '-', trim($make))),
        ];
        $manufacturer = $this->manufacturer_model->insert($data);
        if (!empty($manufacturer)) {
            jsonOutput(true, 'Added.', $manufacturer);
        }
        jsonOutput(false, 'Something went wrong. Please try again.');
    }

    function ajaxAddModel()
    {
        $data = [
            'manufacturer_id'=>$_GET['manufacturer_id'],
            'model' => $_GET['model'],
            'slug' => strtolower(str_replace(' ', '-', trim($_GET['model']))),
        ];
        $manufacturer = $this->manufacturer_model_model->insert($data);
        if (!empty($manufacturer)) {
            jsonOutput(true, 'Added.', $manufacturer);
        }
        jsonOutput(false, 'Something went wrong. Please try again.');
    }

    function ajaxGetModel($make = null)
    {
        $manufacturer = $this->manufacturer_model->get(['slug' => $make]);
        if (empty($manufacturer)) {
            jsonOutput(false, 'Car manufacture not found.',['manufacturer_id'=>$manufacturer->id]);
        }

        $models = $this->manufacturer_model_model->get_all(['manufacturer_id' => $manufacturer->id]);
        if (empty($models)) {
            jsonOutput(false, 'Model not found.',['manufacturer_id'=>$manufacturer->id]);
        }
        jsonOutput(true, 'Model found.', ['manufacturer_id'=>$manufacturer->id,'models'=>$models]);
    }
}
