<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 4:51 PM
 */

class Vacancy extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('vacancy_model');
    }

    function index()
    {
        $this->data['vacancies'] = $this->vacancy_model->get_all();
        $this->data['main_content'] = 'admin/vacancy/index';
        $this->data['sub_content'] = 'admin/vacancy/vacancy';
        $this->load->view(BACKEND, $this->data);
    }

    function delete($id=null) {

        $vacancy = $this->vacancy_model->get($id);
        $url = 'uploads/vacancy/' . $vacancy->mot_file;
        if (file_exists($url))
            unlink($url);
        $url = 'uploads/vacancy/' . $vacancy->insurance_file;
        if (file_exists($url))
            unlink($url);
        $this->vacancy_model->delete(['id'=>$id]);
        $this->session->set_flashdata('msg', 'Successfully! Vacancy deleted');
        redirect('admin/vacancy');
    }

}