<?php

class Discounts extends Admin_Controller {

    function __construct() {
        parent::__construct();
        if (!is_admin_menu_accessible(9)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        $this->load->model('discount_coupon_model');
        $this->load->model('discount_coupon_list_model');
        $this->load->model('discount_account_based_model');
        $this->load->model('passenger_model');
    }

    function coupons() {

        $account_id = $this->input->get('account_id');

        if ($account_id) {
            $this->data['account'] = $this->passenger_model->get($account_id);

            if (!$this->data['account']) {
                show_error('Invalid account id.');
            }

            $conditions = array('account_id' => $account_id);
        } else {
            $conditions = array('account_id' => null);
        }

        $this->data['coupons'] = $this->discount_coupon_model->order_by('id', 'desc')->get_all($conditions);

        $this->data['main_content'] = 'admin/discounts/index';
        $this->data['sub_content'] = 'admin/discounts/_coupons';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update_coupons() {

        if ($_POST) {
            $post = $_POST;

            if (!isset($post['id'])) {

                $dc_id = $this->discount_coupon_model->insert($post);
                for ($i = 1; $i <= $post['no_of_coupons_issued']; $i++) {
                    $this->discount_coupon_list_model->insert(['coupon_no' => uniqueid('', 10, 'tbl_discount_coupon_list', 'coupon_no'), 'discount_coupon_id' => $dc_id]);
                }
                set_flash('msg', 'Coupons has been generated. You need to activate it to work.');
            } else {
                $this->discount_coupon_model->update($post,['id' => $post['id']]);
                set_flash('msg', 'Coupon details edited.');
            }

            if (isset($post['account_id'])) {
                $redirect_url = 'admin/discounts/coupons?account_id=' . $post['account_id'];
            } else {
                $redirect_url = 'admin/discounts/coupons';
            }
            
            redirect(base_url($redirect_url));
        }

        if (segment(4) != '') {
            $this->data['coupon'] = $this->discount_coupon_model->get(['id' => segment(4)]);
        }

        $account_id = $this->input->get('account_id');
        if ($account_id) {
            $this->data['account'] = $this->passenger_model->get($account_id);
            if (!$this->data['account']) {
                show_error('Invalid account id.');
            }
        }

        $this->data['main_content'] = 'admin/discounts/index';
        $this->data['sub_content'] = 'admin/discounts/_form_coupons';
        $this->load->view(BACKEND, $this->data);
    }

    function coupons_view($coupon_id) {

        $coupon = $this->discount_coupon_model->get(array('id' => $coupon_id));
        if (!$coupon)
            die('invalid coupon');

        $this->data['coupon'] = $coupon;
        $this->data['coupons'] = $this->discount_coupon_list_model->get_all(array('discount_coupon_id' => $coupon_id));

        $this->data['main_content'] = 'admin/discounts/index';
        $this->data['sub_content'] = 'admin/discounts/_coupons_view';
        $this->load->view(BACKEND, $this->data);
    }
    
    function change_is_emailed($discount_coupon_list_item_id = null, $boolean_action = null){
        
        if($discount_coupon_list_item_id == null || $boolean_action == null || !is_bool((boolval($boolean_action)))){
            show_404();
        }
        
        $discount_coupon_list_item = $this->discount_coupon_list_model->get($discount_coupon_list_item_id);
        if(!$discount_coupon_list_item){
            show_error('Invalid discount coupon list item id.');
        }
        
        $discount_coupon_list_item_data = array(
            'is_emailed' => $boolean_action
        );
        $update_result = $this->discount_coupon_list_model->update($discount_coupon_list_item->id, $discount_coupon_list_item_data);
        if(!$update_result){
            show_error('Error while updating.');
        }
        
        set_flash('msg', "Emailed status updated for coupon [$discount_coupon_list_item->coupon_no].");
        redirect($this->agent->referrer());
    }

    function change_coupon_status($coupon_id) {
        $this->discount_coupon_model->update(array('is_active' => $_GET['action']), array('id' => $coupon_id));
        set_flash('msg', 'Coupon status changed successfully.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_coupons($coupon_id) {
        $this->discount_coupon_list_model->delete(array('discount_coupon_id' => $coupon_id));
        $this->discount_coupon_model->delete(array('id' => $coupon_id));
        set_flash('msg', 'Coupons deleted!');
        redirect($_SERVER['HTTP_REFERER']);
    }

//    function account_based() {
//
//        if ($_POST) {
//            $post = $_POST;
//            $this->discount_account_based_model->update($post, array('id' => 1));
//            set_flash('msg', 'Discount Rates Updated');
//            redirect($_SERVER['HTTP_REFERER']);
//        }
//
//        $this->data['ab_discount'] = $this->discount_account_based_model->get();
//        $this->data['main_content'] = 'admin/discounts/index';
//        $this->data['sub_content'] = 'admin/discounts/_account_based';
//        $this->load->view(BACKEND, $this->data);
//    }

}
