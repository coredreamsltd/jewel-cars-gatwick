<?php

class System_user extends Admin_Controller {
    public function __construct()
    {
        parent::__construct();
        if (!is_admin_menu_accessible(13)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        $this->load->model('admin_model');
    }

    function index() {
        $this->data['admins'] = $this->admin_model->get_all('tbl_admin', '', 'id DESC');
        // debug(data['admins']);
        $this->data['main_content'] = 'admin/system_users/index';
        $this->data['sub_content'] = 'admin/system_users/_users';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update($admin_id = '') {
        if ($_POST) {
            $post = $_POST;
            // debug($post);

            if($_SESSION['current_user'] == 1){
                $post['menu_access'] = implode(',', $post['menu_access']);
            }
            if ($admin_id == '') {
                unset($post['is_active']);
                $post['expired_at'] = date('Y-m-d H:i:s', strtotime('+7 days'));
                $post['extend_at'] = date('Y-m-d H:i:s');
                $this->db->insert('tbl_admin', $post);
            } else {
                $current = $this->common_model->get_where('tbl_admin', array('id' => $admin_id,'type' => 3));
                // debug(!empty($current));
                if(!empty($current))
                {
                    if(date('Y-m-d H:i:s') > $current[0]['expired_at'] && $post['is_active']) {
                        $post['expired_at'] = date('Y-m-d H:i:s', strtotime('+7 days'));
                        $post['extend_at'] = date('Y-m-d H:i:s');
                    }
                    unset($post['is_active']);
                }
                // debug($post);
                $this->db->update('tbl_admin', $post, array('id' => $admin_id));
            }
            
            set_flash('msg', 'System User saved');
            redirect('admin/system-user');
        } else {
            if ($admin_id != '') {
                $this->data['admin'] = $this->common_model->get_where('tbl_admin', array('id' => $admin_id));
            }
            
            $this->data['main_content'] = 'admin/system_users/index';
            $this->data['sub_content'] = 'admin/system_users/_form';
            $this->load->view(BACKEND, $this->data);
        }
    }
}