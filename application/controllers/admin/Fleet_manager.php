<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Fleet_manager
 *
 * @author Sujendra
 */
class Fleet_manager extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!is_admin_menu_accessible(4)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        $this->load->model('fleet_model');
        $this->load->model('zone_model');
        $this->load->model('route_model');
        $this->load->model('zone_rate_model');
        $this->load->model('additional_rate_model');
        $this->load->model('fare_breakdown_model');
        $this->load->model('specific_location_model');
        $this->load->model('rush_hour_model');
        $this->load->model('holidays_model');
        $this->load->model('location_type_model');
        $this->load->model('location_group_model');
    }

    public function index()
    {
        $fleets = $this->fleet_model->order_by('sort')->get_all();
        $this->data['fleets'] = $fleets;
        $this->data['main_content'] = 'admin/fleet/index';
        $this->data['sub_content'] = 'admin/fleet/_fleets';
        $this->load->view(BACKEND, $this->data);
    }

    public function add_update()
    {
        $fleet_id = segment(4);
        $post = $this->input->post();
        if ($post) {
            $db_fleet = [
                'title' => $post['title'],
                'class' => $post['class'],
                'passengers' => $post['passengers'],
                'suitcases' => $post['suitcases'],
                'luggage' => $post['luggage']??0,
                'baby_seats' => $post['baby_seats'],
                'infant_seats' => $post['infant_seats'],
                'child_booster_seats' => $post['child_booster_seats'],
                'booster_seats' => $post['booster_seats'],
                'status' => $post['status'],
                'desc' => $post['desc'],
                'desc_1' => $post['desc_1'],
            ];
            $db_add_rate = [
                'card_fee' => $post['card_fee'],
                'card_fee_type' => $post['card_fee_type'],
                'baby_seater' => $post['baby_seater'],
                'airport_pickup_fee' => !empty($post['airport_pickup_fee']) ? $post['airport_pickup_fee'] : 0,
                'round_trip' => !empty($post['round_trip']) ? $post['round_trip'] : 0,
                'meet_and_greet' => $post['meet_and_greet'],
                'via_point' => $post['via_point'],
                'fleet_id' => $fleet_id,
                'raise_by' => $post['raise_by'],
                'raise_by_type' => $post['raise_by_type'],
                'waiting_time' => $post['waiting_time'],
                'pickup_charge' => $post['pickup_charge'],
            ];
            if (!empty($_FILES['img_name']['name'])) {
                if ($fleet_id) {
                    $fleetdata = $this->fleet_model->get($fleet_id);
                    if ($fleetdata) {
                        $url = 'uploads/fleet/' . $fleetdata->img_name;
                        if (file_exists($url))
                            unlink($url);
                    }
                }
                $files_data = $this->common_library->upload_image('img_name', 'uploads/fleet/', 'fleet' . time());
                $db_fleet['img_name'] = $files_data['filename'];
            }

            if ($fleet_id == '') {
                $id = $this->fleet_model->insert($db_fleet);
                $db_add_rate['fleet_id'] = $id;
                $this->additional_rate_model->insert($db_add_rate);
                if (!empty($post['from_fleet_id'])) {
                    // $this->mapAllRate(['from_fleet_id' => $post['from_fleet_id'], 'to_fleet_id' => $id]);
                }
                $this->session->set_flashdata('msg', "Data Saved.");
                redirect('admin/fleet_manager');
            } else {
                $db_add_rate['multi_airport'] = json_encode($post['multi_airport']);
                // $db_add_rate['multi_seaport'] = json_encode($post['multi_seaport']);
                // $db_add_rate['multi_station'] = json_encode($post['multi_station']);
                $db_add_rate['multi_airport_dropoff'] = json_encode($post['multi_airport_dropoff']);
                // $db_add_rate['multi_seaport_dropoff'] = json_encode($post['multi_seaport_dropoff']);
                // $db_add_rate['multi_station_dropoff'] = json_encode($post['multi_station_dropoff']);

                $this->fleet_model->update($db_fleet, array('id' => $fleet_id));
                $this->additional_rate_model->update($db_add_rate, array('fleet_id' => $fleet_id));
                $this->session->set_flashdata('msg', "Data Updated.");
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        $this->data['main_content'] = 'admin/fleet/index';
        $this->data['sub_content'] = 'admin/fleet/_form';
        $this->data['is_edit'] = FALSE;
        $this->data['fleet'] = '';
        $this->data['additional_rate'] = '';

        if ($fleet_id != '') {
            $this->data['fleet'] = $this->fleet_model->get(['id' => $fleet_id]);
            $this->data['additional_rate'] = $this->additional_rate_model->get(array('fleet_id' => $fleet_id));
            $this->data['is_edit'] = TRUE;
        }
        $this->data['specific_locations'] = $this->specific_location_model->getAllArea();
        $this->load->view(BACKEND, $this->data);
    }

    function add_update_routes_rate()
    {

        $post = $this->input->post();
        if ($post) {
            if (isset($post['datatable-rr_length'])) {
                unset($post['datatable-rr_length']);
            }
            $no_of_rate_changed = 0;
            // New Entry
            $ids_for_discount = array();
            $loop_ran = 0;
            foreach ($post as $k => $v) {
                if ($k == 'from_location_id' || $k == 'location_type_id' || $k == 'fleet_id')
                    continue;

                if (stripos($k, 'discount_') !== false) {
                    $discount_key_name = explode('_', $k);

                    $from_to = explode('-', $discount_key_name[1]);
                    $routePrevRate = $this->db->get_where('tbl_routes_rate', array('from_id' => $from_to[0], 'to_id' => $from_to[1], 'fleet_id' => $post['fleet_id']))->row();
                    if ($routePrevRate && $routePrevRate->discount != $v) {
                        $this->common_model->update(
                            'tbl_routes_rate',
                            array('discount' => $v, 'updated_at' => date('Y-m-d H:i:s')),
                            array('from_id' => $from_to[0], 'to_id' => $from_to[1], 'fleet_id' => $post['fleet_id'])
                        );
                        $no_of_rate_changed++;
                    }

                    $to_from = explode('-', $discount_key_name[2]);
                    $routePrevRate = $this->db->get_where('tbl_routes_rate', array('from_id' => $to_from[0], 'to_id' => $to_from[1], 'fleet_id' => $post['fleet_id']))->row();
                    if ($routePrevRate && $routePrevRate->discount != $v) {
                        $this->common_model->update(
                            'tbl_routes_rate',
                            array('discount' => $v, 'updated_at' => date('Y-m-d H:i:s')),
                            array('from_id' => $to_from[0], 'to_id' => $to_from[1], 'fleet_id' => $post['fleet_id'])
                        );
                        $no_of_rate_changed++;
                    }
                } else {
                    $key = explode("-", $k);
                    $location_from_id = $key[0];
                    $location_to_id = $key[1];

                    $routePrevRate = $this->db->get_where('tbl_routes_rate', array('from_id' => $location_from_id, 'to_id' => $location_to_id, 'fleet_id' => $post['fleet_id']))->row();

                    $data['id'] = ($routePrevRate ? $routePrevRate->id : '');
                    $data['fleet_id'] = $post['fleet_id'];
                    $data['from_id'] = $location_from_id;
                    $data['to_id'] = $location_to_id;
                    $data['rate'] = $v;

                    if ($data['from_id'] && $data['to_id'] && $data['fleet_id']) {

                        if ($routePrevRate && $routePrevRate->rate != $data['rate']) {
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            $this->common_model->update('tbl_routes_rate', $data, array('id' => $routePrevRate->id));
                            $no_of_rate_changed++;
                        } else if ($data['rate'] > 0 && !$data['id']) {
                            $data['created_at'] = date('Y-m-d H:i:s');
                            $this->common_model->insert('tbl_routes_rate', $data);
                            $no_of_rate_changed++;
                        }
                    }
                }
                $loop_ran++;
            }

            jsonOutput(true, "$no_of_rate_changed Routes Rate saved.");
        }
    }

    public function delete()
    {
        $fleet_id = segment(4);
        $fleet = $this->fleet_model->get($fleet_id);

        $url = 'uploads/fleet/' . $fleet->img_name;
        if (file_exists($url))
            unlink($url);
        $this->fleet_model->delete(['id' => $fleet_id]);
        $this->additional_rate_model->delete(['fleet_id' => $fleet_id]);
        $this->zone_model->DeleteZoneRateWhere(['fleet_id' => $fleet_id]);
        $this->fare_breakdown_model->delete(['fleet_id' => $fleet_id]);
        $this->rush_hour_model->delete(['fleet_id' => $fleet_id]);
        $this->holidays_model->delete(['fleet_id' => $fleet_id]);

        $this->session->set_flashdata('msg', 'Successfully! Fleet Deleted');
        redirect('admin/fleet_manager');
    }
    public function deleteFleetLocationRate($id = null)
    {
        $this->zone_model->delete(['id' => $id]);
        set_flash('msg', 'Successfully! Location Deleted');
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function deleteFleetRouteRate($id = null)
    {
        $this->route_model->delete(['id' => $id]);
        set_flash('msg', 'Successfully! Route Deleted');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function ajaxZoneRate()
    {
        $from_id = $this->input->post('from_id');
        $fleet_id = $this->input->post('fleet_id');
        $sql = "SELECT *,ST_AsText(coordinates) as coordinates FROM `zones`";
        $zones = $this->db->query($sql)->result();

        if (!$from_id) {
            echo json_encode($response = array('msg' => "Please select the Zone", 'data' => '', 'status' => false));
            return;
        }
        if ($zones) {
            foreach ($zones as $index => $zone) {
                $sql = "SELECT * FROM `zones_rate` WHERE `from_id`='{$from_id}' AND `to_id`='{$zone->id}' AND `fleet_id`='{$fleet_id}'";
                $rate = $this->db->query($sql)->row();
                if ($rate) {
                    $zones[$index]->rate = $rate->rate;
                    $zones[$index]->rate_type = $rate->rate_type;
                    if ($rate->minimum_rate == 0) {
                        $zones[$index]->minimum_rate = 0;
                    } else {
                        $zones[$index]->minimum_rate = $rate->minimum_rate;
                    }
                    $zones[$index]->pick_rate = $rate->pick_rate;
                } else {
                    $zones[$index]->rate = 0;
                    $zones[$index]->rate_type = "per_mile";
                    $zones[$index]->minimum_rate = 0;
                    $zones[$index]->pick_rate = 0;
                }
                $sql = "SELECT * FROM `zones_rate` WHERE `from_id`='{$zone->id}' AND `to_id`='{$from_id}' AND `fleet_id`='{$fleet_id}'";
                $reverse_rate = $this->db->query($sql)->row();
                if ($reverse_rate) {
                    $zones[$index]->reverse_rate = $reverse_rate->rate;
                    $zones[$index]->reverse_rate_type = $reverse_rate->rate_type;
                } else {
                    $zones[$index]->reverse_rate = 0;
                    $zones[$index]->reverse_rate_type = 'per_mile';
                }
            }
            echo json_encode($response = array('data' => $zones, 'status' => true));
        } else {
            echo json_encode($response = array('data' => "empty", 'status' => false));
        }
    }

    public function ajaxPostZoneRate()
    {
        $post = $this->input->post();
        $cond = ['from_id' => $post['from_id'], 'to_id' => $post['to_id'], 'fleet_id' => $post['fleet_id']];
        $zone_from_to_rate = $this->zone_model->get_where_zone_rate($cond);

        if ($post['rate'] != 0) {

            if ($zone_from_to_rate) {
                $zone_from_to_rate->from_id = $post['from_id'];
                $zone_from_to_rate->fleet_id = $post['fleet_id'];
                $zone_from_to_rate->to_id = $post['to_id'];
                $zone_from_to_rate->rate = $post['rate'];
                $zone_from_to_rate->rate_type = $post['rate_type'];
                if (isset($post['minimum_rate'])) {
                    $zone_from_to_rate->minimum_rate = $post['minimum_rate'];
                }
                if (isset($post['pick_rate'])) {
                    $zone_from_to_rate->pick_rate = $post['pick_rate'];
                }
                $this->zone_model->update_zone_rate($zone_from_to_rate->id, $zone_from_to_rate);
            } else {

                $zone_from_to_rate->from_id = $post['from_id'];
                $zone_from_to_rate->fleet_id = $post['fleet_id'];
                $zone_from_to_rate->to_id = $post['to_id'];
                $zone_from_to_rate->rate_type = $post['rate_type'];
                $zone_from_to_rate->rate = $post['rate'];
                $zone_from_to_rate->minimum_rate = $post['minimum_rate'];
                $zone_from_to_rate->pick_rate = $post['pick_rate'];

                $this->zone_model->insert_zone_rate($zone_from_to_rate);
            }
        } else {
            $this->zone_model->delete_zone_rate($zone_from_to_rate->id);
        }

        echo json_encode(['status' => true, 'message' => 'Successfully Saved', 'data' => '']);
    }

    public function ajax_google_miles_rate($fleet_id = null)
    {

        $fleet = $this->fleet_model->get(['id' => $fleet_id]);
        if (!$fleet) {
            echo json_encode(['status' => false, 'message' => 'Invalid fleet id', 'data' => '']);
            exit;
        }

        $html_data = [
            'fleet' => $fleet,
            'base_rate' => $this->fare_breakdown_model->get(['fleet_id' => $fleet->id, 'is_min' => 1]),
            'rates' => $this->fare_breakdown_model->order_by('start', 'asc')->get_all(['fleet_id' => $fleet->id, 'is_min' => 0])
        ];

        $html = $this->load->view('admin/fleet/navtab-parts/ajax_google_miles_rate', $html_data, true);
        //        debug($html);

        $response_data = [
            'html' => $html
        ];
        echo json_encode(['status' => true, 'message' => 'success', 'data' => $response_data]);
    }

    public function ajax_save_google_rate()
    {

        if (!$this->input->is_ajax_request()) {
            echo json_encode(['status' => false, 'message' => 'Invalid request type', 'data' => '']);
            return;
        }


        $rate_data = [
            'start' => $this->input->post('start'),
            'end' => $this->input->post('end'),
            'rate' => $this->input->post('rate'),
            'is_min' => $this->input->post('is_min'),
            'fleet_id' => $this->input->post('fleet_id')
        ];

        $rate_id = $this->input->post('rate_id');
        if ($rate_id) {
            $this->fare_breakdown_model->update($rate_data, ['id' => $rate_id]);
            set_flash('msg', 'Update success.');
            echo json_encode(['status' => true, 'message' => 'Update success.', 'data' => '']);
            return;
        }

        if ($rate_data['is_min'] == 1) {
            $rate = $this->fare_breakdown_model->get(['is_min' => 1, 'fleet_id' => $rate_data['fleet_id']]);
            if ($rate) {
                $this->fare_breakdown_model->update($rate_data, ['id' => $rate->id]);
                echo json_encode(['status' => true, 'message' => 'Update success.', 'data' => '']);
                return;
            }
        }

        $this->fare_breakdown_model->insert($rate_data);
        set_flash('msg', 'Insert success.');
        echo json_encode(['status' => true, 'message' => 'Insert success.', 'data' => '']);
    }

    public function ajax_delete_google_rate($rate_id = null)
    {

        if (!$this->input->is_ajax_request()) {
            echo json_encode(['status' => false, 'message' => 'Invalid request type', 'data' => '']);
            return;
        }

        $this->fare_breakdown_model->delete(['id' => $rate_id]);

        set_flash('msg', 'Delete success.');
        echo json_encode(['status' => true, 'message' => 'Delete success.', 'data' => '']);
    }

    public function ajax_google_rate($rate_id = null)
    {

        if (!$this->input->is_ajax_request()) {
            echo json_encode(['status' => false, 'message' => 'Invalid request type', 'data' => '']);
            return;
        }

        $rate = $this->fare_breakdown_model->get($rate_id);
        if (!$rate) {
            echo json_encode(['status' => false, 'message' => 'rate not found.', 'data' => '']);
            return;
        }

        echo json_encode(['status' => true, 'message' => 'rate.', 'data' => $rate]);
    }

    public function ajax_rush_hour_rate_view($fleet_id = null)
    {
        $rush_hour = $this->rush_hour_model->get_all(['fleet_id' => $fleet_id,'shift'=>'night']);

        if (!$rush_hour) {
            echo json_encode(['status' => false, 'message' => 'Invalid fleet id', 'data' => '']);
            exit;
        }
        $html_data = [
            'rush_hour' => $rush_hour,
        ];

        $html = $this->load->view('admin/fleet/navtab-parts/ajax_rush_hour_rate', $html_data, true);
        //        debug($html);

        $response_data = [
            'html' => $html
        ];
        echo json_encode(['status' => true, 'message' => 'success', 'data' => $response_data]);
    }

    public function ajaxPostRushHoursCharge()
    {
        $post = $this->input->post();
        $post['start_time'] = DateTime::createFromFormat('h:i a', $post['start_time'])->format('H:i:s');
        $post['end_time'] = DateTime::createFromFormat('h:i a', $post['end_time'])->format('H:i:s');
        //        debug($post);

        $cond = ['fleet_id' => $post['fleet_id'], 'id' => $post['rush_hrs_id']];
        $rush_hour_charge = $this->rush_hour_model->get($cond);
        if ($rush_hour_charge) {
            set_flash('msg', 'Update success.');
            $this->rush_hour_model->update($post, $cond);
        } else {
            unset($post['rush_hrs_id']);
            set_flash('msg', 'Insert success.');
            $this->rush_hour_model->insert($post);
        }
        echo json_encode(['status' => true, 'message' => 'Data Saved!', 'data' => '']);
    }

    public function ajax_holiday_rate_view($fleet_id = null)
    {
        $holiday_rates = $this->holidays_model->get_all(['fleet_id' => $fleet_id]);

        $html_data = [
            'holiday_rates' => $holiday_rates,
        ];

        $html = $this->load->view('admin/fleet/navtab-parts/ajax_holiday_rate', $html_data, true);

        $response_data = [
            'html' => $html
        ];
        echo json_encode(['status' => true, 'message' => 'success', 'data' => $response_data]);
    }
    public function AjaxSaveHolidayRate()
    {
        if (!$this->input->is_ajax_request()) {
            echo json_encode(['status' => false, 'message' => 'Invalid request type', 'data' => '']);
            return;
        }

        $holiday_rate_data = [
            'starting_date' => DateTime::createFromFormat('d/m/Y', $this->input->post('start_date'))->format('Y-m-d'),
            'starting_time' => DateTime::createFromFormat('h:i a', $this->input->post('start_time'))->format('H:i:s'),
            'ending_date' => DateTime::createFromFormat('d/m/Y', $this->input->post('end_date'))->format('Y-m-d'),
            'ending_time' => DateTime::createFromFormat('h:i a', $this->input->post('end_time'))->format('H:i:s'),
            'charge' => $this->input->post('charge'),
            'charge_type' => $this->input->post('charge_type'),
            'is_active' => $this->input->post('is_active'),
            'fleet_id' => $this->input->post('fleet_id')
        ];

        $holiday_rate_id = $this->input->post('holiday_rate_id');
        //        debug($_POST);
        if ($holiday_rate_id) {
            $this->holidays_model->update($holiday_rate_data, ['id' => $holiday_rate_id]);

            set_flash('msg', 'Update success.');
            echo json_encode(['status' => true, 'message' => 'Update success.', 'data' => '']);
            return;
        }

        $this->holidays_model->insert($holiday_rate_data);
        set_flash('msg', 'Insert success.');
        echo json_encode(['status' => true, 'message' => 'Insert success.', 'data' => '']);
    }

    function AjaxHolidayRate($rate_id = null)
    {

        if (!$this->input->is_ajax_request()) {
            echo json_encode(['status' => false, 'message' => 'Invalid request type', 'data' => '']);
            return;
        }

        $rate = $this->holidays_model->get($rate_id);
        $rate = [
            'starting_date' => DateTime::createFromFormat('Y-m-d', $rate->starting_date)->format('d/m/Y'),
            'starting_time' => DateTime::createFromFormat('H:i:s', $rate->starting_time)->format('h:i a'),
            'ending_date' => DateTime::createFromFormat('Y-m-d', $rate->ending_date)->format('d/m/Y'),
            'ending_time' => DateTime::createFromFormat('H:i:s', $rate->ending_time)->format('h:i a'),
            'is_active' => $rate->is_active,
            'fleet_id' => $rate->fleet_id,
            'id' => $rate->id,
            'charge' => $rate->charge,
            'charge_type' => $rate->charge_type,
            'is_active' => $rate->is_active,
        ];
        //        debug($rate);

        if (!$rate) {
            echo json_encode(['status' => false, 'message' => 'Holiday Rate not found.', 'data' => '']);
            return;
        }

        echo json_encode(['status' => true, 'message' => 'rate.', 'data' => $rate]);
    }

    function AjaxDeleteHolidayRate($rate_id = null)
    {

        if (!$this->input->is_ajax_request()) {
            echo json_encode(['status' => false, 'message' => 'Invalid request type', 'data' => '']);
            return;
        }

        $this->holidays_model->delete(['id' => $rate_id]);

        set_flash('msg', 'Delete success.');
        echo json_encode(['status' => true, 'message' => 'Delete success.', 'data' => '']);
    }

    public function ajaxGetIncrementFleet()
    {
        $post = $this->input->post();
        $data['fleets'] = $this->fleet_model->get_all(['id <>' => $post['fleet_id']]);
        $html = $this->load->view('admin/fleet/_ajax_fleet_increment_fleets', $data, true);

        echo json_encode(['status' => true, 'message' => 'success', 'data' => ['html' => $html]]);
    }
    public function ajaxBulkRateIncrement()
    {
        $post = $this->input->post();

        for ($i = 0; $i < count($post['value']); $i++) {
            $fleetsToIncrease[$post['fleetsToIncrease'][$i]] = $post['value'][$i];
        }


        $fleets = $this->fleet_model->get_all(['id <>' => $post['fleet_id']]);
        $zoneRates = $this->zone_rate_model->get_all(['fleet_id' => $post['fleet_id']]);
        $breakDownFares = $this->fare_breakdown_model->get_all(['fleet_id' => $post['fleet_id']]);

        foreach ($fleets as $fleet) {
            foreach ($zoneRates as $zoneRate) {
                $increment_rate = ($post['increment_type'] == 'amount') ? $fleetsToIncrease[$fleet->id] : ($zoneRate->rate * $fleetsToIncrease[$fleet->id] * 0.01);
                $newZoneRate[] = [
                    'fleet_id' => $fleet->id,
                    'from_id' => $zoneRate->from_id,
                    'to_id' => $zoneRate->to_id,
                    'rate_type' => $zoneRate->rate_type,
                    'minimum_rate' => $zoneRate->minimum_rate,
                    'rate' => round($zoneRate->rate + $increment_rate, 2),
                ];
            }
        }

        foreach ($fleets as $fleet) {
            foreach ($breakDownFares as $breakDownFare) {
                $increment_rate = ($post['increment_type'] == 'amount') ? $fleetsToIncrease[$fleet->id] : ($breakDownFare->rate * $fleetsToIncrease[$fleet->id] * 0.01);
                $newbreakDownFare[] = [
                    'start' => $breakDownFare->start,
                    'end' => $breakDownFare->end,
                    'is_min' => $breakDownFare->is_min,
                    'rate' => round($breakDownFare->rate + $increment_rate, 2),
                    'fleet_id' => $fleet->id,
                ];
            }
        }

        foreach ($fleets as $fleet) {
            $this->zone_rate_model->delete(['fleet_id' => $fleet->id]);
        }
        foreach ($fleets as $fleet) {
            $this->fare_breakdown_model->delete(['fleet_id' => $fleet->id]);
        }
        $this->db->insert_batch('zones_rate', $newZoneRate);
        $this->db->insert_batch('tbl_fare_breakdown', $newbreakDownFare);
        echo json_encode(['status' => true, 'message' => 'Rate successfully saved', 'data' => '']);
    }

    private function mapAllRate($fleet_ids = null)
    {
        $from_fleet = $this->fleet_model->get(['id' => $fleet_ids['from_fleet_id']]);
        $to_fleet = $this->fleet_model->get(['id' => $fleet_ids['to_fleet_id']]);
        // Additional rate
        $from_additional_rate = (array) $this->additional_rate_model->get(['fleet_id' => $fleet_ids['from_fleet_id']]);
        unset($from_additional_rate['id'], $from_additional_rate['fleet_id'], $from_additional_rate['created_at'], $from_additional_rate['updated_at']);
        $this->additional_rate_model->update($from_additional_rate, ['fleet_id' => $fleet_ids['to_fleet_id']]);
        // Route rates
        $route_rates =  $this->rush_hour_model->get_all(['fleet_id' => $fleet_ids['from_fleet_id']]);
        foreach ($route_rates as $route_rate) {
            $db_data = [
                'start_time' => $route_rate->start_time,
                'end_time' => $route_rate->end_time,
                'charge' => $route_rate->charge,
            ];
            $this->rush_hour_model->update($db_data, ['fleet_id' => $fleet_ids['to_fleet_id']]);
        }
        // Holiday Rates
        $holiday_rates =  $this->holidays_model->get_all(['fleet_id' => $fleet_ids['from_fleet_id']]);
        foreach ($holiday_rates as $holiday_rate) {
            $db_data = [
                'starting_date' => $holiday_rate->starting_date,
                'starting_time' => $holiday_rate->starting_time,
                'ending_date' => $holiday_rate->ending_date,
                'ending_time' => $holiday_rate->ending_time,
                'charge' => $holiday_rate->charge,
                'charge_type' => $holiday_rate->charge_type,
                'is_active' => $holiday_rate->is_active,
            ];
            $this->holidays_model->update($db_data, ['fleet_id' => $fleet_ids['to_fleet_id']]);
        }

        // Location rates
        $from_location_rates = $this->zone_model->getWhere(['fleet_id' => $fleet_ids['from_fleet_id']]);
        foreach ($from_location_rates as $rate) {
            $db_data = [
                'title' => $rate->title,
                'coordinates' => $rate->coordinates,
                'lat' => $rate->lat,
                'lng' => $rate->lng,
                'type' => $rate->type,
                'rate' => $rate->rate,
                'rate_distance' => $rate->rate_distance,
                'radius' => $rate->radius,
                'fleet_id' => $fleet_ids['to_fleet_id'],
                'service_area_id' => $rate->service_area_id,
            ];
            $this->zone_model->insertZone($db_data);
        }
        // Route rates
        $from_route_rates = $this->route_model->getWhere(['fleet_id' => $fleet_ids['from_fleet_id']]);
        foreach ($from_route_rates as $rate) {
            $db_data = [
                'title_1' => $rate->title_1,
                'title_2' => $rate->title_2,
                'coordinates_1' => $rate->coordinates_1,
                'coordinates_2' => $rate->coordinates_2,
                'lat_1' => $rate->lat_1,
                'lat_2' => $rate->lat_2,
                'lng_1' => $rate->lng_1,
                'lng_2' => $rate->lng_2,
                'type' => $rate->type,
                'rate' => $rate->rate,
                'rate_distance' => $rate->rate_distance,
                'radius_1' => $rate->radius_1,
                'radius_2' => $rate->radius_2,
                'fleet_id' => $fleet_ids['to_fleet_id'],
                'service_area_id' => $rate->service_area_id,
            ];
            $this->route_model->insertZone($db_data);
        }
        // Fare Break down
        $from_break_down_rates = $this->fare_breakdown_model->get_all(['fleet_id' => $fleet_ids['from_fleet_id']]);
        foreach ($from_break_down_rates as $rate) {
            $db_data = [
                'start' => $rate->start,
                'end' => $rate->end,
                'rate' => $rate->rate,
                'is_min' => $rate->is_min,
                'fleet_id' => $fleet_ids['to_fleet_id'],
            ];
            $this->fare_breakdown_model->insert($db_data);
        }
    }
}
