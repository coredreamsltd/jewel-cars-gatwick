<?php

class Route_manager extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('route_model');
    }

    function index()
    {
        redirect(site_url('admin/fleet_manager'));
        // $sql = "SELECT *,ST_AsText(coordinates) as coordinates FROM `routes`";
        // $this->data['zones'] = $this->db->query($sql)->result();
        // $this->data['main_content'] = 'admin/route/index';
        // $this->data['sub_content'] = 'admin/route/zones';
        // $this->load->view(BACKEND, $this->data);
    }
    public function add_edit($zone_id = null)
    {
        $fleet_id = !empty($this->input->get('fleet_id')) ? $this->input->get('fleet_id') : '';
        $service_area_id = $this->data['service_area_id'];
        $this->data['zone'] = $this->route_model->getZone($zone_id);


        if ($this->input->post()) {

            $zone_data = array(
                'title_1' => $this->input->post('title_1'),
                'lat_1' => $this->input->post('lat_1'),
                'lng_1' => $this->input->post('lng_1'),
                'radius_1' => $this->input->post('radius_1'),
                'title_2' => $this->input->post('title_2'),
                'lat_2' => $this->input->post('lat_2'),
                'lng_2' => $this->input->post('lng_2'),
                'radius_2' => $this->input->post('radius_2'),
                'rate' => $this->input->post('rate'),
                'rate_distance' => $this->input->post('rate_distance'),
                'fleet_id' => $fleet_id,
                'service_area_id' => $service_area_id,
                'coordinates_1' => $this->input->post('coordinates_1'),
                'coordinates_2' => $this->input->post('coordinates_2'),
                'type' => !empty($this->input->post('type')) ? $this->input->post('type') : 'one_way',
            );
            
            if ($zone_id) {
                $this->route_model->updateZone($zone_id, $zone_data);
                set_flash('msg', 'Location updated.');
                redirect(site_url('admin/route_manager/add_edit/' . $zone_id . '?fleet_id=' . $fleet_id));
            }

            $zone_id = $this->route_model->insertZone($zone_data);
            set_flash('msg', 'Location added.');
            redirect(site_url('admin/fleet_manager/add_update/' . $fleet_id.'#tab8'));
        }

        $this->data['isEdit'] = false;
        if ($zone_id) {
            $this->data['isEdit'] = true;
        }
        
        $this->data['main_content'] = 'admin/route/index';
        $this->data['sub_content'] = 'admin/route/add_edit';
        $this->load->view(BACKEND, $this->data);
    }

    function delete($id)
    {
        if ($id) {
            $this->route_model->delete(['id' => $id]);
            set_flash('msg', 'Zone Deleted');
            redirect(site_url('admin/route_manager'));
        }
    }
}
