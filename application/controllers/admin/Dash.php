<?php

class Dash extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!is_admin_menu_accessible(1)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        
    }

    public function index()
    {

        $this->data['count']=[
            'driver'=>$this->db->select("*")->from('driver')->get()->num_rows(),
            'passenger'=>$this->db->select("*")->where('type', 'personal')->from('passenger')->get()->num_rows(),
            'corporate'=>$this->db->select("*")->where('type', 'corporate')->from('passenger')->get()->num_rows(),
            'travel_agent'=>$this->db->select("*")->where('type', 'travel_agent')->from('passenger')->get()->num_rows(),
            'total_booking'=>$this->db->select("*")->from('tbl_booking_infos')->get()->num_rows(),
            'total_earn'=>$this->db->select_sum("total_fare")->from('tbl_booking_infos')->get()->row()->total_fare?:0,
            ];
        
        $this->data['main_content'] = 'admin/dashboard';
        $this->load->view(BACKEND, $this->data);
    }

    

    function setSessionServiceId()
    {
        $post = $this->input->post();
        setSession('service_area_id', $post['service_area_id']);
        redirect($_SERVER['HTTP_REFERER']);
    }
}
