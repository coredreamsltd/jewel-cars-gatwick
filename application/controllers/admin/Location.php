<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!is_admin_menu_accessible(8)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        $this->load->model('location_model');
        $this->load->model('terminal_model');
        $this->load->model('location_group_model');
        $this->load->model('location_group_reln_model');
    }

    function index($offset = '')
    {

        if (isset($_GET['type'])) {
            $this->data['locations'] = $this->common_model->get_where('tbl_locations', array('location_type_id' => $_GET['type']), 'name ASC');
            //    debug($this->data['locations']);

        } else {
            $this->data['locations'] = $this->common_model->get_all('tbl_locations', '', 'name ASC');
        }

        if ($this->data['locations']) {
            foreach ($this->data['locations'] as $k => $l) {
                $location_type = $this->common_model->get_where('tbl_location_type', array('id' => $l['location_type_id']));
                $newArray = array('location_type' => $location_type[0]['type']);
                $this->data['locations'][$k] = array_merge($l, $newArray);
            }
        }

        $this->data['main_content'] = 'admin/location/index';
        $this->data['sub_content'] = 'admin/location/_locations';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update($location_id = null)
    {

        if ($_POST) {
            $post = $_POST;
            $config = array(
                'field' => 'slug',
                'title' => 'name',
                'table' => 'tbl_locations',
                'id' => 'id',
            );
            $this->load->library('Slug', $config);

            if (!empty($_FILES['img_featured']['name'])) {
                if ($location_id != '') {
                    $locationdata = $this->common_model->get_where('tbl_locations', array('id' => $location_id));
                    $url = 'uploads/images/location/' . $locationdata[0]['img_featured'];
                    if (file_exists($url))
                        unlink($url);
                }
                $files_data = $this->common_library->upload_image('img_featured', 'uploads/images/location/', 'location' . time());
                $post['img_featured'] = $files_data['filename'];
            }

            if ($location_id == '') {
                $post['slug'] = $this->slug->create_uri($post);
                $this->common_model->insert('tbl_locations', $post, true);
                set_flash('msg', 'Location added.');
                redirect(base_url('admin/location?type=' . $post['location_type_id']));
            } else {
                $post['slug'] = $this->slug->create_uri($post, $location_id);
                $this->common_model->update('tbl_locations', $post, array('id' => $location_id));
                set_flash('msg', 'Location updated.');
                redirect($this->agent->referrer());
            }
        }

        $this->data['main_content'] =  'admin/location/index';
        $this->data['sub_content'] =  'admin/location/_form';
        if ($location_id) {
            $this->data['isEdit'] = true;

            $this->data['location'] = $this->common_model->get_where('tbl_locations', array('id' => $location_id));
            // $this->data['terminals'] = $this->terminal_model->order_by('created_at', 'desc')->get_all(['location_id' => $location_id]);
            $this->data['terminals'] = $this->db->order_by('created_at', 'desc')->get_where(' tbl_terminals', ['location_id' => $location_id])->result();
        } else {
            $this->data['isEdit'] = false;
        }

        $this->data['location_types'] = $this->common_model->get_all('tbl_location_type', '', 'id');
        // debug($this->data);

        $this->load->view(BACKEND, $this->data);
    }

    function delete_location()
    {
        $location_id = segment(4);


        $rates_count = $this->common_model->get_count_string_condition('tbl_routes_rate', "from_id = '$location_id' OR to_id = '$location_id'");

        $this->common_model->delete_data('tbl_locations', array('id' => segment(4)));
        if ($rates_count > 0) {
            set_flash('msg', 'Location deleted along with its corresponding rates');
        } else {
            set_flash('msg', 'Location deleted.');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    function categories()
    {

        $locationTypes = $this->common_model->get_all('tbl_location_type', '', '`id` ASC');
        $this->data['location_types'] = $locationTypes;
        $this->data['main_content'] = 'admin/location/index';
        $this->data['sub_content'] = 'admin/location/_location_categories';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update_location_category()
    {

        $location_type_id = segment(4);
        if ($_POST) {
            $post = $_POST;
            if ($location_type_id == '') {
                $this->common_model->insert('tbl_location_type', $post);
            } else {
                $this->common_model->update('tbl_location_type', $post, array('id' => $location_type_id));
            }
            set_flash('msg', 'Location Type saved');
            redirect('admin/location/categories');
        } else {
            $this->data['main_content'] = 'admin/location/index';
            $this->data['sub_content'] = 'admin/location/_location_categories_form';
            if ($location_type_id != '') {
                $this->data['location_type'] = $this->common_model->get_where('tbl_location_type', array('id' => $location_type_id));
            }
            $this->load->view(BACKEND, $this->data);
        }
    }

    function delete_location_category()
    {
        $locations = $this->common_model->get_where('locations', array('location_type_id' => segment(4)));
        if (!empty($locations))
            foreach ($locations as $l) {
                $bookings_count = $this->common_model->get_count_string_condition('booking_details', "pickup_id = '{$l['id']}' OR dropoff_id = '{$l['id']}'");

                if ($bookings_count > 0) {
                    set_flash('msg_danger', 'Bookings exists for corresponding location category. Please delete them first!');
                    redirect('admin/location/categories');
                    return;
                }
            }

        $locationsCount = $this->common_model->get_count('locations', array('location_type_id' => segment(4)));
        $this->common_model->delete_data('location_type', array('id' => segment(4)));


        if ($locationsCount > 0) {
            set_flash('msg', 'Location Type along with its locations and corresponding rates deleted');
        } else {
            set_flash('msg', 'Location Category Deleted.');
        }

        redirect('admin/location/categories');
    }

    function groups()
    {
        $location_groups = $this->location_group_model->order_by('id', 'desc')->get_all();
        $this->data['location_groups'] = $location_groups;
        $this->data['main_content'] = 'admin/location/index';
        $this->data['sub_content'] = 'admin/location/groups';
        $this->load->view(BACKEND, $this->data);
    }

    function add_edit_group($location_group_id = null)
    {

        if ($_POST) {
            $post = $this->input->post();
            $data_location_group = array(
                'group_name' => $post['group_name']
            );

            if ($location_group_id) {
                $this->location_group_model->update($data_location_group, $location_group_id);
            } else {
                $location_group_id = $this->location_group_model->insert($data_location_group);
                if (!$location_group_id) {
                    show_error('Something went wrong.');
                }
            }

            $new_location_ids = explode(',', $post['location_ids']);

            // if edit
            $group_locations = $this->location_model->get_locations_by_group_id($location_group_id);
            $old_location_ids = array();
            if ($group_locations) {
                foreach ($group_locations as $location) {
                    $old_location_ids[] = $location->id;
                }
            }

            $to_add_location_ids = array_diff($new_location_ids, $old_location_ids);
            if ($to_add_location_ids) {
                $batch_data_location_group_reln = array();
                foreach ($to_add_location_ids as $location_id) {
                    $batch_data_location_group_reln[] = array(
                        'location_id' => $location_id,
                        'location_group_id' => $location_group_id
                    );
                }

                $this->location_group_reln_model->insert_batch($batch_data_location_group_reln);
            }

            $to_delete_location_ids = array_diff($old_location_ids, $new_location_ids);
            if ($to_delete_location_ids) {
                $this->location_group_reln_model->delete_by(array('location_id' => $to_delete_location_ids, 'location_group_id' => $location_group_id));
            }


            set_flash('msg', 'Data saved.');
            redirect(site_url('admin/location/add_edit_group/' . $location_group_id));
        }


        if ($location_group_id) {
            $location_group = $this->location_group_model->get($location_group_id);
            if (!$location_group) {
                show_404();
            }

            $this->data['location_group'] = $location_group;
            $this->data['group_locations'] = $this->location_model->get_locations_by_group_id($location_group_id);

            $location_ids = array();
            if ($this->data['group_locations']) {
                foreach ($this->data['group_locations'] as $location) {
                    $location_ids[] = $location->id;
                }
            }
            $this->data['location_ids'] = implode(',', $location_ids);

            $this->data['isEdit'] = true;
        } else {
            $this->data['isEdit'] = false;
        }

        $this->data['locations'] = $this->location_model->get_all_with_group();
        $this->data['main_content'] = 'admin/location/index';
        $this->data['sub_content'] = 'admin/location/add_edit_group';
        $this->load->view(BACKEND, $this->data);
    }

    function delete_group($location_group_id = null)
    {

        if (!$location_group_id) {
            show_404();
        }

        $this->location_group_reln_model->delete_by(array('location_group_id' => $location_group_id));
        $this->location_group_model->delete($location_group_id);

        set_flash('msg', 'Data deleted.');
        redirect($this->agent->referrer());
    }

    public function add_edit_terminal()
    {

        if (!$this->input->post()) {
            show_error('Invalid request.');
        }

        $terminal_data = array(
            'location_id' => $this->input->post('location_id'),
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description')
        );

        // update if terminal id is provided
        $terminal_id = $this->input->post('terminal_id');
        if ($terminal_id) {
            $this->terminal_model->update($terminal_data, $terminal_id);
            set_flash('msg', 'Terminal updated.');
            redirect($this->agent->referrer());
        }

        $this->terminal_model->insert($terminal_data);
        set_flash('msg', 'Terminal added.');
        redirect($this->agent->referrer());
    }

    public function ajax_get_terminal_details($terminal_id = null)
    {

        if (!$terminal_id) {
            jsonOutput(0, 'Terminal id required.');
        }

        $terminal = $this->terminal_model->get($terminal_id);
        if (!$terminal) {
            jsonOutput(0, 'Terminal not found.');
        }

        jsonOutput(1, 'Terminal data.', $terminal);
    }

    public function delete_terminal($terminal_id)
    {

        $this->terminal_model->delete($terminal_id);
        set_flash('msg', 'Terminal deleted.');
        redirect($this->agent->referrer());
    }
}
