<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 10:26 AM
 */

class Service_area extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('service_area_model');
    }

    function index()
    {
        $this->data['areas'] = $this->service_area_model->getAllArea();
        $this->data['main_content'] = 'admin/service_area/index';
        $this->data['sub_content'] = 'admin/service_area/area';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update($id = null)
    {
        if ($this->input->post()) {
            $zone_data = array(
                'area_name' => $this->input->post('area_name'),
                'custom_area_name' => $this->input->post('custom_area_name'),
                'lat' => $this->input->post('lat'),
                'lng' => $this->input->post('lng'),
                'coordinates' => $this->input->post('coordinates')
            );

            if ($id) {
                $this->service_area_model->updateArea($id, $zone_data);
                set_flash('msg', 'Update success.');
                redirect(site_url('admin/service_area/add_update/' . $id));
            }

            $this->service_area_model->insertArea($zone_data);
            set_flash('msg', 'Insert success.');
            redirect(site_url('admin/service_area'));
        }

        $this->data['isEdit'] = false;
        if ($id) {
            $this->data['area'] = $this->service_area_model->getArea($id);;
            $this->data['isEdit'] = true;
        }

        $this->data['main_content'] = 'admin/service_area/index';
        $this->data['sub_content'] = 'admin/service_area/form';
        $this->load->view(BACKEND, $this->data);
    }
    function delete($id = null)
    {
        $this->service_area_model->delete($id);
        set_flash('msg', 'Successfully! deleted');
        redirect('admin/service_area');
    }
}
