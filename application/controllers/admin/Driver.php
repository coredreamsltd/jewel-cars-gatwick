<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 10:45 AM
 */

class Driver extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('driver_model');
        $this->load->model('drive_fleets_model');
        $this->load->model('file_model');
        $this->load->model('fleet_model');
        $this->load->model('vehicle_model');

        if (!is_admin_menu_accessible(7)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
    }

    function index()
    {
        $this->data['count']['today'] = $this->db->select("DATE_FORMAT(created_at, '%Y-%m-%d'),COUNT(*) as count")
            ->from('driver')->where("date(created_at) = CURDATE()")
            ->get()->row()->count;

        $this->data['count']['month'] =  $this->db->select("MONTH(created_at),COUNT(*) as count")
            ->from('driver')->where("MONTH(created_at) = MONTH(CURDATE())")
            ->get()->row()->count;

        $this->data['count']['annual'] =  $this->db->select("YEAR(created_at),COUNT(*) as count")
            ->from('driver')->where("YEAR(created_at) = YEAR(CURDATE())")
            ->get()->row()->count;


        $this->data['drivers'] = $this->db->select('d.*,f.file_name as image')
            ->from('driver d')
            ->join('file f', 'f.id=d.image_id', 'left')
            ->get()->result();
        //        debug($this->data['drivers']);
        $this->data['vehicles'] = $this->vehicle_model->get_all();

        $this->data['main_content'] = 'admin/driver/index';
        $this->data['sub_content'] = 'admin/driver/driver';
        $this->load->view(BACKEND, $this->data);
    }
    function add()
    {
        $post = $this->input->post();
        if ($post) {
            $this->driver_model->insert($post);
            set_flash('msg', 'Driver added.');
            redirect(site_url('admin/driver'));
        }
        $this->data['main_content'] = 'admin/driver/index';
        $this->data['sub_content'] = 'admin/driver/add';
        $this->load->view(BACKEND, $this->data);
    }

    function delete($driver_id = null)
    {
        $this->driver_model->delete(['id' => $driver_id]);
        set_flash('msg', 'Driver Deleted.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function details($id = null)
    {
        $this->data['driver'] = $this->db->select('d.*,
        f.file_name as image,f1.file_name as file_pco_driver_batch,
        f2.file_name as file_hire_agreement,f3.file_name as file_driver_licence_photo_front,
        f4.file_name as file_driver_licence_photo_back,f5.file_name as file_driver_taxi_licence,
        f6.file_name as file_other_docs,f7.file_name as file_tc_signed_copy,
        f8.file_name as file_pco_licence,f9.file_name as file_pco_badge
        ')
            ->from('driver d')
            ->join('file f', 'f.id=d.image_id', 'left')
            ->join('file f1', 'f1.id=d.file_pco_driver_batch_id', 'left')
            ->join('file f2', 'f2.id=d.file_hire_agreement_id', 'left')
            ->join('file f3', 'f3.id=d.file_driver_licence_photo_front_id', 'left')
            ->join('file f4', 'f4.id=d.file_driver_licence_photo_back_id', 'left')
            ->join('file f5', 'f5.id=d.file_driver_taxi_licence_id', 'left')
            ->join('file f6', 'f6.id=d.file_other_docs_id', 'left')
            ->join('file f7', 'f7.id=d.file_tc_signed_copy_id', 'left')
            ->join('file f8', 'f8.id=d.file_pco_licence_id', 'left')
            ->join('file f9', 'f9.id=d.file_pco_badge_id', 'left')
            ->where('d.id', $id)
            ->get()->row();

        $this->data['driver_fleets'] = $this->db->select('df.*,v.title as title,
        f.file_name as file_phv_licence,f1.file_name as file_mot,
        f2.file_name as file_taxi_private_hire_insurance,f3.file_name as file_vehicle_log,
        f4.file_name as file_road_tax
        ')
            ->from('driver_fleets df')
            ->join('tbl_fleets v', 'v.id=df.fleet_id', 'left')
            ->join('file f', 'f.id=df.file_phv_licence_id', 'left')
            ->join('file f1', 'f1.id=df.file_mot_id', 'left')
            ->join('file f2', 'f2.id=df.file_taxi_private_hire_insurance_id', 'left')
            ->join('file f3', 'f3.id=df.file_vehicle_log_id', 'left')
            ->join('file f4', 'f4.id=df.file_road_tax_id', 'left')
            ->where('df.driver_id', $id)
            ->get()->result();

        $this->data['fleets'] = $this->fleet_model->get_all();
        $this->data['main_content'] = 'admin/driver/index';
        $this->data['sub_content'] = 'admin/driver/details';
        $this->load->view(BACKEND, $this->data);
    }

    function updateProfile($id = null)
    {
        $post = $this->input->post();

        if ($post && $id) {
            if ($_FILES['driver_image']['name']) {
                $driver = $this->driver_model->get(['id' => $id]);
                $file = $this->file_model->get(['id' => $driver->image_id]);

                if ($file) {
                    $url = 'uploads/driver/' . $file->file_name;
                    if (file_exists($url))
                        unlink($url);
                }
                $files_data = $this->common_library->upload_image('driver_image', 'uploads/driver/', 'profile_' . $id . "_" . time());
                $post['image_id'] = $this->file_model->insert(['file_name' => $files_data['filename'], 'file_data' => json_encode($_FILES)]);
            }

            $this->driver_model->update($post, ['id' => $id]);
            set_flash('msg', 'Driver Personal Details Updated.');
            redirect(site_url('admin/driver/details/' . $id));
        }
    }

    function updateDriverDocuments($id = null)
    {
        $post = $this->input->post();
        foreach ($_FILES as $index => $doc_file) {

            if ($doc_file['name']) {
                $driver = $this->driver_model->get(['id' => $id]);
                $file = $this->file_model->get(['id' => $driver->{$index . '_id'}]);

                if ($file) {
                    $url = 'uploads/documents/driver/' . $id . '/' . $file->file_name;
                    if (file_exists($url))
                        unlink($url);
                }
                $files_data = $this->common_library->upload_image($index, 'uploads/documents/driver/' . $id . '/', $index . "_" . $id . "_" . time());
                $post[$index . '_id'] = $this->file_model->insert(['file_name' => $files_data['filename'], 'file_data' => json_encode($doc_file)]);
            }
            //                debug($post);
        }

        if (!empty($post['dvla_licence_expiry_date']))
            $post['dvla_licence_expiry_date'] = DateTime::createFromFormat('d/m/Y', $post['dvla_licence_expiry_date'])->format('Y-m-d');
        if (!empty($post['insurance_expiry_date']))
            $post['insurance_expiry_date'] = DateTime::createFromFormat('d/m/Y', $post['insurance_expiry_date'])->format('Y-m-d');
        if (!empty($post['pco_driver_expiry_date']))
            $post['pco_driver_expiry_date'] = DateTime::createFromFormat('d/m/Y', $post['pco_driver_expiry_date'])->format('Y-m-d');
        $this->driver_model->update($post, ['id' => $id]);
        set_flash('msg', 'Driver Documents Updated.');
        redirect(site_url('admin/driver/details/' . $id));
    }

    function addVehicle($fleet_id = null)
    {
        $post = $this->input->post();
        if ($post) {
            $this->drive_fleets_model->insert($post);
            set_flash('msg', 'Vehicle Added.');
            redirect(site_url('admin/driver/details/' . $post['driver_id']));
        }
    }


    function assignVehicle()
    {
        $post = $this->input->post();
        if ($post) {
            $this->driver_model->update(['vehicle_id' => $post['vehicle_id']], ['id' => $post['driver_id']]);
            set_flash('msg', 'Vehicle Added.');
        }
        redirect(site_url('admin/driver'));
    }

    function uploadDocument($driver_id = null)
    {
        $post = $this->input->post();
        
        
        if ($_FILES['document']['name']) {
            $driver_fleet = $this->drive_fleets_model->get(['id' => $post['df_id']]);
            $file = $this->file_model->get(['id' => $driver_fleet->{$post['field_name']}]);

            if ($file) {
                $url = 'uploads/documents/driver/' . $driver_id . '/' . $file->file_name;
                if (file_exists($url))
                    unlink($url);
            }
            $files_data = $this->common_library->upload_image('document', 'uploads/documents/driver/' . $driver_id . '/', $post['document_name'] . "-" . $post['df_id'] . "-" . time());
            $db_data[$post['field_name']] = $this->file_model->insert(['file_name' => $files_data['filename'], 'file_data' => json_encode($_FILES)]);
        }

        (isset($post['exp_date'])) ? $db_data[$post['field_name_expiry']] = DateTime::createFromFormat('d/m/Y', $post['exp_date'])->format('Y-m-d') : '';
        $this->drive_fleets_model->update($db_data, ['id' => $post['df_id']]);
        //        debug($db_data);
        set_flash('msg', 'Documents Saved.');
        redirect(site_url('admin/driver/details/' . $driver_id));
    }

    function updateExpDate($driver_id = null)
    {
        $post = $this->input->post();
        //        debug($post);
        $db_data[$post['new_exp_filed_name']] = DateTime::createFromFormat('d/m/Y', $post['new_exp_date'])->format('Y-m-d');
        $this->drive_fleets_model->update($db_data, ['id' => $post['df_id']]);
        //        debug($db_data);
        set_flash('msg', 'Expiry Date Saved.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function changeColor($df_id = null)
    {
        $post = $this->input->post();
        //        debug($post);
        if (!$post) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->drive_fleets_model->update($post, ['id' => $df_id]);
        set_flash('msg', 'Color Saved.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function change_fleet_make_model_registration($df_id = null)
    {
        $post = $this->input->post();
        //        debug($post);
        if (!$post) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->drive_fleets_model->update($post, ['id' => $df_id]);
        set_flash('msg', 'Data Saved.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function deleteVehicle($df_id = null)
    {
        $this->drive_fleets_model->delete(['id' => $df_id]);
        set_flash('msg', 'Vehicle Deleted.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function statements($driver_id)
    {
        $this->data['driver'] = $this->driver_model->get(['id' => $driver_id]);
        $this->data['statements'] = $this->driver_model->get_statements($driver_id);

        $this->data['main_content'] = 'admin/driver/index';
        $this->data['sub_content'] = 'admin/driver/_statements';
        $this->load->view(BACKEND, $this->data);
    }

    function preview_statement($statement_ref_id)
    {
        $statement = $this->db->get_where('tbl_statements', array('statement_ref_id' => $statement_ref_id))->row();
        if ($statement) {
            die($statement->mail_template);
        }
    }

    function send_statement($driver_id)
    {

        if ($this->input->post()) {
            $post = $this->input->post();

            $newArr = array();
            foreach ($post as $k => $v) {
                $newArr[substr($k, 2)] = $v;
            }

            $driver = $this->driver_model->get(['id' => $newArr['driver_id']]);
            $this->common_model->insert('tbl_statements', $newArr);
            email_help($driver->email, $newArr['mail_subject'], $newArr['mail_template'], [SITE_EMAIL => SITE_NAME]);
            set_flash('msg', 'Work statement sent.');
            redirect(base_url('admin/driver/statements/' . $driver->id));
        } else {
            $this->data['driver'] = $this->driver_model->get(['id' => $driver_id]);
            $this->data['statement_ref_id'] = uniqueid('', 5, 'tbl_statements', 'statement_ref_id');

            $this->data['main_content'] = 'admin/driver/index';
            $this->data['sub_content'] = 'admin/driver/_send_statement';
            $this->load->view(BACKEND, $this->data);
        }
    }

    function cancel_statement()
    {

        $statement_ref_id = $this->input->post('statement_ref_id');
        $statement = $this->db->get_where('tbl_statements', array('statement_ref_id' => $statement_ref_id))->row();

        if (!$statement) {
            show_error('Invalid statement ref id:' . $statement_ref_id);
        }

        if (!$_POST) {
            show_error('Invalid request.');
        }

        $driver = $this->driver_model->get(['id' => $statement->driver_id]);
        $emailer_data = array(
            'message' => $this->input->post('message'),
            'driver' => $driver,
            'statement' => $statement
        );
        $emailer_cancel = common_emogrifier($this->load->view('emailer/_emailer_driver_statement_cancelled', $emailer_data, true));

        $this->db->update('tbl_statements', array('is_cancelled' => 1, 'cancel_email_template' => $emailer_cancel), array('statement_ref_id' => $statement_ref_id));

        email_help($driver->email, "[RefID:$statement_ref_id] Statement Cancelled", $emailer_cancel, [SITE_EMAIL => SITE_NAME]);

        set_flash('msg', "[RefID:$statement_ref_id] Statement Cancelled & email sent to driver.");
        redirect(base_url('admin/driver/statements/' . $driver->id));
    }

    function ajax_preview_statement()
    {
        $post = NVPToArray($_POST['formdata']);

        $from_date = $post['from_date'];
        $to_date = $post['to_date'];
        $driver = $this->driver_model->get(['id' => $post['driver_id']]);


        $is_statement_raised = $this->driver_model->isStatementRaised($from_date, $driver->id);
        if ($is_statement_raised) {
            jsonOutput(0, 'Statement already raised for ' . $from_date);
            exit;
        }

        $post['jobs'] = $this->driver_model->getBookings($driver->id, $from_date, $to_date);

        // driver payments list (job statement start - today)
        $post['payments'] = $this->driver_model->getPayments($driver->id, $from_date, date('d/m/Y'));

        // sum of all driver payments
        $total_payment = $this->driver_model->getTotalPayment($driver->id, $from_date, date('d/m/Y'));

        $post['driver_name'] = $driver->name;

        $statement['last_amount'] = $this->driver_model->statement_amount(
            date('d/m/Y', strtotime($driver->created_at)),
            DateTime::createFromFormat('d/m/Y', $from_date)->modify('-1 day')->format('d/m/Y'),
            date('d/m/Y', strtotime($driver->created_at)),
            DateTime::createFromFormat('d/m/Y', $from_date)->modify('-1 day')->format('d/m/Y'),
            $driver->id,
            1
        );

        $statement['current_amount'] = $this->driver_model->statement_amount($from_date, $to_date, $from_date, date('d/m/Y'), $driver->id, 0);
        $statement['total_payable'] = round($statement['current_amount'] - $total_payment, 2);

        $replace_data = array(
            '{total_final_fare}' => 0,
            '{total_cash_collected_by_driver}' => 0,
            '{total_company_earning}' => 0,
            '{total_driver_earning}' => 0,
            '{start_date}' => $from_date,
            '{end_date}' => $to_date,
            '{total_driver_payable}' => $statement['total_payable']
        );

        if ($post['jobs']) {
            foreach ($post['jobs'] as $index => $j) {
                $company_commission = $post['jobs'][$index]['company_commission'] = round($j['driver_fare'] * 0.01 * COMPANY_COMMISSION, 2);
                $replace_data['{total_final_fare}'] += $j['driver_fare'];
                $cash_to_driver = $this->driver_model->calculateCashToDriver($j['booking_ref_id']);
                $post['jobs'][$index]['cash_collected_by_driver'] = intval($cash_to_driver);
                $replace_data['{total_cash_collected_by_driver}'] += $post['jobs'][$index]['cash_collected_by_driver'];
                $replace_data['{total_company_earning}'] += $company_commission;
                $replace_data['{total_driver_earning}'] += ($j['driver_fare'] - $company_commission);
            }
        }

        $statement_mail_template = common_emogrifier($this->load->view('emailer/_emailer_driver_statement', array('data' => $post, 'statement' => $statement, 'replace_data' => $replace_data), true));

        $data = array(
            'payable_amount_due' => $statement['total_payable'],
            'from_date' => $from_date,
            'to_date' => $to_date,
            'mail_subject' => $post['mail_subject'],
            'mail_template' => $statement_mail_template
        );
        jsonOutput(1, 'Email template preview', $data);
    }

    function payments($driver_id)
    {

        if ($_POST) {

            $post = $this->input->post();
            if ($this->driver_model->add_payment($post, $driver_id)) {
                set_flash('msg', 'Payment added.');
            } else {
                set_flash('dmsg', 'Something went wrong. Please try again.');
            }

            redirect($this->agent->referrer());
        } else {
            $this->data['driver'] = $this->driver_model->get(['id' => $driver_id]);
            $this->data['payments'] = $this->driver_model->get_payments($driver_id);

            $this->data['main_content'] = 'admin/driver/index';
            $this->data['sub_content'] = 'admin/driver/_payments';
            $this->load->view(BACKEND, $this->data);
        }
    }

    function delete_payment($payment_id)
    {

        if ($this->driver_model->delete_payment($payment_id)) {
            set_flash('msg', 'Payment deleted.');
        } else {
            set_flash('dmsg', 'Something went wrong. Please try again.');
        }

        redirect($this->agent->referrer());
    }

    function job_sheets($driver_id)
    {

        $this->data['driver'] = $this->driver_model->get(['id' => $driver_id]);
        $this->data['job_sheets'] = $this->driver_model->get_job_sheets($driver_id);
        $this->data['main_content'] = 'admin/driver/index';
        $this->data['sub_content'] = 'admin/driver/_job_sheets';
        $this->load->view(BACKEND, $this->data);
    }

    function preview_job_sheet_email($job_sheet_id)
    {
        $this->load->model('job_sheet_model');
        $job_sheet = $this->job_sheet_model->get_by_id($job_sheet_id);
        if ($job_sheet) {
            die($job_sheet->mail_template);
        } else {
            show_error('Invalid job sheet id: ' . $job_sheet_id);
        }
    }

    function delete_job_sheet($id = null)
    {
        $job_sheet = $this->job_sheet_model->get(['id' => $id]);
        if ($job_sheet) {
            $this->job_sheet_model->delete(['id' => $id]);
        } else {
            show_error('Invalid job sheet id: ' . $id);
        }
        set_flash('msg', 'Job sheet deleted.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function send_job_sheet($driver_id)
    {

        if ($this->input->post()) {
            $post = $this->input->post();

            $newArr = array();
            foreach ($post as $k => $v) {
                $newArr[substr($k, 2)] = $v;
            }
            $mail_admin_template = $newArr['mail_admin_template'];
            unset($newArr['mail_admin_template']);
            $driver = $this->driver_model->get(['id' => $newArr['driver_id']]);
            $this->common_model->insert('tbl_job_sheet', $newArr);

            email_help($driver->email, $newArr['mail_subject'], $newArr['mail_template'], [SITE_EMAIL => SITE_NAME]);
            email_help(ADMIN_EMAIL, $newArr['mail_subject'], $mail_admin_template, [SITE_EMAIL => SITE_NAME]);
            set_flash('msg', 'Job sheet sent.');
            redirect(base_url('admin/driver/job_sheets/' . $driver_id));
        } else {
            $this->data['driver'] = $this->driver_model->get(['id' => $driver_id]);
            $this->data['main_content'] = 'admin/driver/index';
            $this->data['sub_content'] = 'admin/driver/_send_job_sheet';
            $this->load->view(BACKEND, $this->data);
        }
    }

    function ajax_preview_job_sheet()
    {
        $post = NVPToArray($_POST['formdata']);

        $from_date = $post['from_date'];
        $to_date = $post['to_date'];

        $sql = "SELECT * FROM tbl_booking_infos WHERE (STR_TO_DATE(pickup_date, '%m/%d/%Y') BETWEEN STR_TO_DATE('$from_date', '%m/%d/%Y') AND STR_TO_DATE('$to_date', '%m/%d/%Y')) AND driver_id = '{$post['driver_id']}' AND job_status <> 'completed' AND is_cancelled = 0 ORDER BY pickup_date ASC";
        $post['jobs'] = $this->db->query($sql)->result_array();

        $booking_ids = array();
        if ($post['jobs'])
            foreach ($post['jobs'] as $j)
                array_push($booking_ids, $j['booking_ref_id']);

        $post['driver'] = $this->driver_model->get(['id' => $post['driver_id']]);
        $jobsheet_mail_template = common_emogrifier($this->load->view('emailer/_emailer_job_sheet', array('data' => $post), true));
        $jobsheet_mail_admin_template = common_emogrifier($this->load->view('emailer/_emailer_job_sheet', array('data' => $post, 'email_to_admin' => 1), true));


        $data = array(
            'from_date' => $from_date,
            'to_date' => $to_date,
            'booking_ids' => implode(',', $booking_ids),
            'mail_subject' => $post['mail_subject'],
            'mail_template' => $jobsheet_mail_template,
            'mail_admin_template' => $jobsheet_mail_admin_template,
        );

        jsonOutput(1, 'Email template preview', $data);
    }
}
