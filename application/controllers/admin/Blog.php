<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 10:26 AM
 */

class Blog extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('blog_model');
    }

    function index()
    {
        $this->data['blogs'] = $this->blog_model->get_all();

        $this->data['main_content'] = 'admin/blog/index';
        $this->data['sub_content'] = 'admin/blog/blog';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update($id = null)
    {

        $post = $this->input->post();
        if ($post) {
            $blog = $_FILES['blog'];
//            debug($blog);
            $image_name = '';

            if (!empty($blog['name'])) {
                $blog = $this->blog_model->get(array('id' => $id));

                if ($blog) {
                    $url = 'uploads/blog/' . $blog->featured_image;
                    if (file_exists($url))
                        unlink($url);
                }
                $files_data = $this->common_library->upload_image('blog', 'uploads/blog/', 'blog_' . time());
                $image_name = $files_data['filename'];
                $post['featured_image'] = $image_name;
            }

            if ($id == '') {
                $post['featured_image'] = $image_name;
                $this->blog_model->insert($post);
            } else {
                $this->blog_model->update($post, array('id' => $id));
            }
            $this->session->set_flashdata('msg', "Blog Saved.");

            redirect('admin/blog');
        } else {
            $this->data['main_content'] = 'admin/blog/index';
            $this->data['sub_content'] = 'admin/blog/blog_form';

            $this->data['isNew'] = true;
            if ($id != '') {
                $this->data['isNew'] = false;
                $this->data['blog'] = $this->blog_model->get(['id' => $id]);
            }
            $this->load->view(BACKEND, $this->data);
        }
    }
    function delete($id=null) {

        $blog = $this->blog_model->get($id);
        $url = 'uploads/blog/' . $blog->featured_image;
        if (file_exists($url))
            unlink($url);
        $this->blog_model->delete($id);
        $this->session->set_flashdata('msg', 'Successfully! Blog deleted');
        redirect('admin/blog');
    }

}