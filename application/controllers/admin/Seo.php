<?php

class Seo extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        if (!is_admin_menu_accessible(11)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        $this->load->model('seo_model');
    }

    function index($offset = '') {
        $this->data['seo'] = $this->db->get('tbl_seo')->result_array();
        $this->data['main_content'] = 'admin/seo/index';
        $this->data['sub_content'] = 'admin/seo/_seo';
        $this->load->view(BACKEND, $this->data);
    }

    function seo_add_update($seo_id = '') {

        if ($_POST) {
            $post = $_POST;
            if ($seo_id == '') {
                $this->db->insert('tbl_seo', $post);
                $this->session->set_flashdata('msg', 'New SEO Link added.');
            } else {
                $this->db->update('tbl_seo', $post, array('id' => $seo_id));
                $this->session->set_flashdata('msg', 'SEO Link updated.');
            }
            redirect('admin/seo');
        } else {
            if ($seo_id != '') {
                $this->data['seo'] = $this->db->get_where('tbl_seo', array('id' => $seo_id))->row();
            }
            $this->data['main_content'] = 'admin/seo/index';
            $this->data['sub_content'] = 'admin/seo/_form';
            $this->load->view(BACKEND, $this->data);
        }
    }

    function seo_delete($seo_id) {

        if ($this->db->delete('tbl_seo', array('id' => $seo_id))) {
            $this->session->set_flashdata('msg', 'SEO Link deleted.');
        } else {
            $this->session->set_flashdata('dmsg', 'Nothing was deleted.');
        }
        redirect('admin/seo');
    }

    function seo_common() {

        if ($_POST) {
            $post = $_POST;
            if ($this->db->update('tbl_seo_common', $post, array('id' => 1))) {
                $this->session->set_flashdata('msg', 'Common SEO details saved.');
            } else {
                $this->session->set_flashdata('dmsg', 'Nothing new to save.');
            }
            redirect('admin/seo/seo_common');
        } else {
            $this->data['seo_common'] = $this->db->get_where('tbl_seo_common', array('id' => 1))->row_array();
            $this->load->view(BACKEND, $this->data);
        }
    }

}
