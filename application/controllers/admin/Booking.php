<?php

class Booking extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!is_admin_menu_accessible(2)) {
			set_flash('msg', 'Unauthorized Access.');
			redirect(base_url('admin/login'));
		}
		$this->load->model('fleet_model');
		$this->load->model('driver_model');
		$this->load->model('invoice_model');
		$this->load->model('job_assign_model');
		$this->load->model('booking_info_model');
		$this->load->model('invoice_bulk_model');
		$this->load->model('location_model');
		$this->load->model('rush_hour_model');
		$this->load->model('holidays_model');
		$this->load->model('additional_rate_model');
		$this->load->model('fare_breakdown_model');
		$this->load->helper('email_helper');
		$this->load->library('TwilioSms');
		$this->load->model('passenger_model');
		$this->load->model('passenger_model');
		$this->load->model('yearly_booking_model');
		$this->load->model('yearly_time_schedule_model');
		$this->data['passenger'] = '';
	}

	function index()
	{
		$this->db->select("b.*,d.name as driver_name,d.email as driver_email,d.phone_cc as driver_phone_cc,d.mobile as driver_mobile,v.plate_number");
		$this->db->from('tbl_booking_infos b');
		$this->db->join('driver d', 'b.driver_id=d.id', 'left');
		$this->db->join('vehicles v', 'd.vehicle_id=v.id', 'left');

		// SATRT
		if (isset($_GET['action']) && $_GET['action'] == 'search') {
			$get = $this->input->get();

			if (isset($get['booking_ref_ids']) && $get['booking_ref_ids']) {
				$this->db->where_in('booking_ref_id', array_map('trim', explode(',', $get['booking_ref_ids'])));
			}

			if (isset($get['journey_type']) && $get['journey_type']) {
				$this->db->where('journey_type', $get['journey_type']);
			}

			if (isset($get['status'])) {
				if ($get['status'] == 'new') {
					$this->db->where('booking_status', 'not confirmed');
				} else if ($get['status'] == 'confirmed') {
					$this->db->where("(booking_status = 'confirmed')");
					$this->db->where("(job_status = '' OR job_status = 'rejected')");
				} else if ($get['status'] == 'job_assigned') {
					$this->db->where("(job_status = 'assigned')");
				} else if ($get['status'] == 'job_accepted') {
					$this->db->where("(job_status = 'accepted')");
				} else if ($get['status'] == 'job_rejected') {
					$this->db->where("(job_status = 'rejected')");
				} else if ($get['status'] == 'completed') {
					$this->db->where('job_status', 'completed');
				} else if ($get['status'] == 'cancelled') {
					$this->db->where('is_cancelled', 1);
				}
			}

			if (isset($get['booked_date_from']) && $get['booked_date_from']) {
				$this->db->where('b.created_at >=', date('Y-m-d', strtotime(str_replace('/', '-', $get['booked_date_from']))));
			}

			if (isset($get['booked_date_to']) && $get['booked_date_to']) {
				$this->db->where('b.created_at <=', date('Y-m-d', strtotime("+1 day", strtotime(str_replace('/', '-', $get['booked_date_to'])))));
			}

			if (isset($get['pickup_date_from']) && $get['pickup_date_from']) {
				$this->db->where("pickup_date >=", date('Y-m-d', strtotime(str_replace('/', '-', $get['pickup_date_from']))));
			}

			if (isset($get['pickup_date_to']) && $get['pickup_date_to']) {
				$this->db->where("pickup_date <=", date('Y-m-d', strtotime(str_replace('/', '-', $get['pickup_date_to']))));
			}

			if (isset($get['client_name']) && $get['client_name']) {
				$this->db->like('client_name', $get['client_name']);
			}

			if (isset($get['booked_by_name']) && $get['booked_by_name']) {
				$this->db->join('passenger p', 'b.passenger_id = p.id', 'LEFT');
				$this->db->like('p.full_name', $get['booked_by_name']);
			}

			if (isset($get['client_email']) && $get['client_email']) {
				$this->db->where('client_email', $get['client_email']);
			}

			if (isset($get['driver_id']) && $get['driver_id']) {
				$this->db->where('driver_id', $get['driver_id']);
			}


			if (isset($get['passenger_id']) && $get['passenger_id']) {
				$this->db->where('passenger_id', $get['passenger_id']);
			}
		} else {
			$condDate = date('Y-m-d', strtotime('- 2 days'));
			$this->db->where("pickup_date >=", $condDate);
			$this->db->where(array('is_cancelled' => 0, 'job_status <>' => 'completed'));
		}

		// END


		$order = " CASE"
			. " WHEN STR_TO_DATE(b.pickup_date, '%Y-%m-%d') = '" . date('Y-m-d') . "' THEN 0 "
			. " WHEN STR_TO_DATE(b.pickup_date, '%Y-%m-%d') > '" . date('Y-m-d') . "' THEN 1 "
			. " WHEN STR_TO_DATE(b.pickup_date, '%Y-%m-%d') < '" . date('Y-m-d') . "' THEN 2 END ASC, "
			. " CASE WHEN STR_TO_DATE(b.pickup_date, '%Y-%m-%d') >= '" . date('Y-m-d') . "' THEN STR_TO_DATE(b.pickup_date, '%Y-%m-%d') END ASC, "
			. " CASE WHEN STR_TO_DATE(b.pickup_date, '%Y-%m-%d') < '" . date('Y-m-d') . "' THEN STR_TO_DATE(b.pickup_date, '%Y-%m-%d') END DESC, STR_TO_DATE(b.pickup_time, '%h:%i A') ASC";
		$query = $this->db->order_by($order)->get();
		// $query = $this->db->get();
		$bookings = $query ? $query->result() : null;

		if (!empty($_GET['invoice_status'])) {
			$new_bookings = [];
			if ($bookings) {
				if ($_GET['invoice_status'] == 'raise') {
					foreach ($bookings as $b) {
						$invoices = $this->invoice_model->get_all_by_booking_id($b->booking_ref_id);
						if ($invoices) {
							$new_bookings[] = $b;
						}
					}
				} elseif ($_GET['invoice_status'] == 'not_raise') {
					foreach ($bookings as $b) {
						$invoices = $this->invoice_model->get_all_by_booking_id($b->booking_ref_id);
						if (!$invoices) {
							$new_bookings[] = $b;
						}
					}
				}
			}
			$this->data['booking_infos']  = $new_bookings;
		} else {
			$this->data['booking_infos']  = $bookings;
		}
		// debug($bookings);
		$sub_query = array(
			'new_jobs' => "(SELECT count(*) FROM (`tbl_booking_infos`) WHERE `booking_status` = 'not confirmed' AND `is_cancelled` = 0 AND job_status <> 'completed')",
			'confirmed_jobs' => "(SELECT count(*) FROM (`tbl_booking_infos`) WHERE (booking_status = 'confirmed') AND (job_status = '' OR job_status = 'rejected') AND `is_cancelled` = 0)",
			'completed_jobs' => "(SELECT count(*) FROM (`tbl_booking_infos`) WHERE  (job_status = 'completed') AND `is_cancelled` = 0)",
			'driver_assigned_jobs' => "(SELECT count(*) FROM (`tbl_booking_infos`) WHERE job_status = 'assigned' AND `is_cancelled` = 0)",
			'driver_accepted_jobs' => "(SELECT count(*) FROM (`tbl_booking_infos`) WHERE job_status = 'accepted' AND `is_cancelled` = 0)",
			'driver_rejected_jobs' => "(SELECT count(*) FROM (`tbl_booking_infos`) WHERE job_status = 'rejected' AND `is_cancelled` = 0)",
			'cancelled_jobs' => "(SELECT count(*) FROM (`tbl_booking_infos`) WHERE `is_cancelled` = 1)",
		);
		$this->db->select("{$sub_query['new_jobs']} as new_jobs");
		$this->db->select("{$sub_query['confirmed_jobs']} as confirmed_jobs");
		$this->db->select("{$sub_query['completed_jobs']} as completed_jobs");
		$this->db->select("{$sub_query['cancelled_jobs']} as cancelled_jobs");
		$this->db->select("{$sub_query['driver_assigned_jobs']} as driver_assigned_jobs");
		$this->db->select("{$sub_query['driver_accepted_jobs']} as driver_accepted_jobs");
		$this->db->select("{$sub_query['driver_rejected_jobs']} as driver_rejected_jobs");
		$count = $this->db->get()->row();

		$this->data['counts'] = array(
			'new_jobs' => $count->new_jobs,
			'confirmed_jobs' => $count->confirmed_jobs,
			'completed_jobs' => $count->completed_jobs,
			'cancelled_jobs' => $count->cancelled_jobs,
			'driver_assigned_jobs' => $count->driver_assigned_jobs,
			'driver_accepted_jobs' => $count->driver_accepted_jobs,
			'driver_rejected_jobs' => $count->driver_rejected_jobs
		);

		$this->data['drivers'] = $this->driver_model->get_all();
		$this->data['main_content'] = 'admin/booking_info/index';
		$this->data['sub_content'] = 'admin/booking_info/client';
		$this->load->view(BACKEND, $this->data);
	}


	function view($id)
	{
		$this->data['booking'] = $this->booking_info_model->get(array('id' => $id));
		if (empty($this->data['booking'])) {
			show_404('Booking not found.');
		}
		$this->data['driver'] = $this->driver_model->get(['id' => $this->data['booking']->driver_id]);
		$this->data['fleet'] = $this->fleet_model->get(array('title' => $this->data['booking']->vehicle_name));
		$this->data['invoices'] = $this->invoice_model->get_all_by_booking_id($this->data['booking']->booking_ref_id);
		$this->data['amount_due'] = $this->invoice_model->get_due_amount($this->data['booking']->booking_ref_id, 0);
		$this->data['drivers'] = $this->driver_model->get_all();
		$this->data['main_content'] = 'admin/booking_info/index';
		$this->data['sub_content'] = 'admin/booking_info/_client_more_info';
		$this->load->view(BACKEND, $this->data);
	}

	function update($id = null)
	{
		$post = $this->input->post();
		if ($post) {
			$post['pickup_date'] = DateTime::createFromFormat('d/m/Y', $post['pickup_date'])->format('Y-m-d');
			if (!empty($post['via_point'])) {
				$post['via_point'] = json_encode($post['via_point']);
			}
			$fleet_id = explode('-|-', $post['fleet_id'], 2);
			$post['selected_fleet_id'] = $fleet_id[0];
			$post['vehicle_name'] = $fleet_id[1];
			$this->booking_info_model->update($post, ['id' => $id]);

			$booking = $this->booking_info_model->get(array('id' => $id));
			$mail_message =  "Booking detail has been updated. Please verify. Thank you!";
			$admin_emailer_template = $this->load->view('emailer/booking_emailler', array('data' => (array)$booking, 'emailer_to' => 'admin_booking_update', 'mail_message' => $mail_message), true);
			$client_emailer_template = $this->load->view('emailer/booking_emailler', array('data' => (array)$booking, 'emailer_to' => 'client_booking_update', 'mail_message' => $mail_message), true);

			$admin_mergedHtml = common_emogrifier($admin_emailer_template);
			$client_mergedHtml = common_emogrifier($client_emailer_template);

			email_help($booking->client_email, "Booking Updated - " . SITE_NAME . " [{$booking->booking_ref_id}]", $client_mergedHtml, [SITE_EMAIL => SITE_NAME]);
			email_help(ADMIN_EMAIL, "Booking Updated - " . SITE_NAME . " [{$booking->booking_ref_id}]", $admin_mergedHtml, [SITE_EMAIL => SITE_NAME]);

			set_flash('msg', 'Details Updated Successfully!');
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->data['booking_infos'] = $this->booking_info_model->get(array('id' => $id));
			$this->data['fleets'] = $this->fleet_model->get_all();
			$this->data['main_content'] = 'admin/booking_info/index';
			$this->data['sub_content'] = 'admin/booking_info/edit';
			$this->load->view(BACKEND, $this->data);
		}
	}

	function reBook()
	{
		$post = $this->input->post();
		$booking = $this->booking_info_model->get(array('id' => $post['booking_id']));

		if (empty($booking)) {
			set_flash('dmsg', 'Booking not found.');
			redirect('admin/booking');
		}
		$booking_ref_id = uniqueid('', 7, 'tbl_booking_infos', 'booking_ref_id');
		unset($booking->id, $booking->is_paid, $booking->booking_status, $booking->job_status, $booking->is_cancelled, $booking->is_bulk_invoice_raised, $booking->discount_coupon_no);
		$booking->booking_ref_id = $booking_ref_id;
		$booking->driver_id = 0;
		$booking->pickup_date = $post['pickup_date'];
		$booking->pickup_time = $post['pickup_time'];
		$booking_id = $this->booking_info_model->insert($booking);
		if (empty($booking_id)) {
			set_flash('dmsg', 'Sorry for the inconvenience. Error occur while generating booking. Please try again.');
			redirect('admin/booking');
		}
		set_flash('msg', 'Re-Booking successfully done. [BK.ID: ' . $booking_ref_id . ']');
		redirect('admin/booking');
	}
	function returnBook()
	{
		$post = $this->input->post();
		$booking = $old_booking = $this->booking_info_model->get(array('id' => $post['booking_id']));

		if (empty($booking)) {
			set_flash('dmsg', 'Booking not found.');
			redirect('admin/booking');
		}

		$booking_ref_id = uniqueid('', 7, 'tbl_booking_infos', 'booking_ref_id');
		unset($booking->id, $booking->is_paid, $booking->booking_status, $booking->job_status, $booking->is_cancelled, $booking->is_bulk_invoice_raised, $booking->discount_coupon_no);
		$booking->booking_ref_id = $booking_ref_id;
		$booking->driver_id = 0;
		$booking->journey_type = 'two_way';
		$booking->pickup_address = $old_booking->dropoff_address;
		$booking->dropoff_address = $old_booking->pickup_address;
		$booking->start_lat = $old_booking->end_lat;
		$booking->start_lng = $old_booking->end_lng;
		$booking->end_lat = $old_booking->start_lat;
		$booking->end_lng = $old_booking->end_lat;
		$booking->pickup_date = $post['pickup_date'];
		$booking->pickup_time = $post['pickup_time'];

		$booking_id = $this->booking_info_model->insert($booking);
		if (empty($booking_id)) {
			set_flash('dmsg', 'Sorry for the inconvenience. Error occur while generating booking. Please try again.');
			redirect('admin/booking');
		}
		set_flash('msg', 'Return booking successfully done. [BK.ID: ' . $booking_ref_id . ']');
		redirect('admin/booking');
	}

	function mark_paid($invoice_id)
	{
		if ($this->invoice_model->update(array('payment_status' => 1), array('invoice_id' => $invoice_id))) {
			set_flash('msg', "Payment status for invoice id: $invoice_id has been marked as paid. Please continue sending the payment receipt.");
		} else {
			set_flash('msg_danger', 'Something went wrong while marking invoice as paid. Please try again.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function mark_completed($booking_id)
	{
		if ($this->booking_info_model->update(array('job_status' => 'completed'), array('id' => $booking_id))) {
			$booking = (array) $this->booking_info_model->get(array('id' => $booking_id));
			$booking['mail_message'] = 'Your booking has been completed. ';
			$booking_mail_template = ($this->load->view('emailer/booking_emailler', array('data' => $booking, 'emailer_to' => 'confirm_client'), true));
			email_help($booking['client_email'], "[{$booking['booking_ref_id']}] Booking Completed " . SITE_NAME, $booking_mail_template, [SITE_EMAIL => SITE_NAME]);

			$booking['mail_message'] = 'Booking has been completed. ';
			$booking_mail_template = ($this->load->view('emailer/booking_emailler', array('data' => $booking, 'emailer_to' => 'confirm_admin'), true));
			email_help(ADMIN_EMAIL, "[{$booking['booking_ref_id']}] Booking Completed " . SITE_NAME, $booking_mail_template, [SITE_EMAIL => SITE_NAME]);

			set_flash('msg', "Booking id [<a href='" . site_url('admin/booking/view/' . $booking_id) . "'>" . $booking['booking_ref_id'] . "</a>] marked as completed.");
		} else {
			set_flash('msg_danger', 'Nothing done. Try again.');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	function cancel_invoice($invoice_id)
	{
		$this->invoice_model->update(array('is_cancelled' => 1), array('invoice_id' => $invoice_id));

		// if bulk invoice unset flag of bookings table
		//        $bulk_bookings_invoice = $this->common_model->get_where('tbl_invoice_bulk', array('invoice_id' => $invoice_id));
		//        if ($bulk_bookings_invoice) {
		//            foreach ($bulk_bookings_invoice as $bbi) {
		//                $this->common_model->update('tbl_bookings', array('is_bulk_invoice_raised' => 0), array('booking_id' => $bbi['booking_id']));
		//            }
		//        }

		set_flash('msg', "Invoice Id: $invoice_id cancelled successfully.");
		redirect($_SERVER['HTTP_REFERER']);
	}

	function cancel($booking_id)
	{
		if ($this->booking_info_model->update(array('is_cancelled' => 1), array('id' => $booking_id))) {
			$booking = (array) $this->booking_info_model->get(array('id' => $booking_id));
			$booking['mail_message'] = 'Your booking has been cancelled. ';
			$booking_mail_template = ($this->load->view('emailer/booking_emailler', array('data' => $booking, 'emailer_to' => 'confirm_client'), true));
			email_help($booking['client_email'], "[{$booking['booking_ref_id']}] Booking Cancelled " . SITE_NAME, $booking_mail_template, [SITE_EMAIL => SITE_NAME]);
			set_flash('msg', "Booking id [<a href='" . site_url('admin/booking/view/' . $booking_id) . "'>" . $booking['booking_ref_id'] . "</a>] is cancelled.");
		} else {
			set_flash('msg_danger', 'Nothing done. Try again.');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	function delete($id)
	{
		$this->booking_info_model->delete($id);
		$this->session->set_flashdata('msg', 'Successfully! Booking Information Deleted');
		redirect('admin/booking');
	}

	public function assignDriver()
	{
		$post = $this->input->post();
		$booking_id = $post['booking_id'];
		$driver = $this->driver_model->get(['id' => $post['driver_id']]);
		$db_assign = [
			'job_status' => 'assigned',
			'driver_id' => $post['driver_id']
		];
		!empty($post['driver_fare']) ? $db_assign['driver_fare'] = $post['driver_fare'] : '';
		$this->booking_info_model->update($db_assign, ['id' => $booking_id]);
		$booking = $this->booking_info_model->get(['id' => $booking_id]);
		$email_data = ['driver' => $driver, 'booking' => $booking];
		$mergeHTML = $this->load->view('emailer/emailer_driver_job_assign', $email_data, true);
		set_flash('msg', 'Driver Assigned but email not sent.');
		if (email_help($driver->email, 'Job Assign ', $mergeHTML, [SITE_EMAIL => SITE_NAME])) {
			set_flash('msg', 'Driver assigned. Email sent to driver [' . $driver->name . ']');
		}

		// $msisdn = implode(',', array($driver->phone_cc . $driver->mobile));
		// $sms_message = "Dear {$driver->name}, [BK.ID:-{$booking->booking_ref_id}] Job has been assigned.";
		// $post_body = $this->twiliosms->seven_bit_sms($sms_message, $msisdn);
		// $result = $this->twiliosms->send_message($post_body);

		if ($this->input->is_ajax_request()) {
			jsonOutput(true, "Driver assigned. Email sent to driver [{$driver->name}]");
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function edit_final_fare()
	{
		$post = $_POST;
		$this->booking_info_model->update($post, array('id' => $post['booking_id']));
		redirect($_SERVER['HTTP_REFERER']);
	}

	function invoice()
	{
		$invoice = $this->invoice_model->get(array('invoice_id' => segment(4)));
		if ($invoice) {
			echo $invoice->mail_template;
		} else {
			echo 'No invoice found for invoice id: ' . segment(4);
		}
	}

	function raise_invoice()
	{

		if ($_POST) {

			$newArr = array();
			foreach ($_POST as $k => $v) {
				$newArr[substr($k, 2)] = $v;
			}
			$is_send_email = $newArr['is_send_email'];
			unset($newArr['is_send_email']);

			$booking = $this->booking_info_model->get(array('booking_ref_id' => $newArr['booking_id']));
			$newArr['invoice_payment_method'] = $booking->pay_method;
			$recipients = explode(',', $newArr['customer_email']);

			foreach ($recipients as $e) {
				if (!filter_var($e, FILTER_VALIDATE_EMAIL)) {
					die('Recipients Emails (' . $newArr['customer_email'] . ') has an invalid email.');
				}
			}

			if ($is_send_email) {
				$newArr['invoice_mailed_datetime'] = date('Y-m-d H:i:s');
			}

			/**
			 * Author: Manish Shrestha <manishrestha529@gmail.com>
			 * Here bulk invoice is raised because later system was modified &
			 * every invoice's amount was stored in partial inside bulk invoice list
			 * this made bulk invoice a lot easier.
			 * In booking bulk flag is set 1 because that affects in various modules, so didn't want to take risk on it.
			 */
			$this->invoice_model->insert($newArr);
			//            $this->invoice_bulk_model->insert(array(
			//                    'booking_id' => $newArr['booking_id'],
			//                    'invoice_id' => $newArr['invoice_id'],
			//                    'partial_amount' => $newArr['invoice_amount']
			//                )
			//            );
			//            $this->booking_info_model->update(array('is_bulk_invoice_raised' => 1), array('booking_ref_id' => $newArr['booking_id']));

			if ($is_send_email) {
				email_help($recipients, $newArr['mail_subject'], $newArr['mail_template'], [SITE_EMAIL => SITE_NAME]);
				set_flash('msg', 'Invoice has been successfully generated and emailed to: ' . $newArr['customer_email']);
			} else {
				set_flash('msg', 'Invoice has been successfully generated, but not emailed.');
			}

			redirect(site_url('admin/booking/view/' . $booking->id));
		} else {
			$this->data['booking'] = $this->booking_info_model->get(array('booking_ref_id' => $_GET['booking_id']));
			$this->data['main_content'] = 'admin/booking_info/index';
			$this->data['sub_content'] = 'admin/booking_info/_raise_invoice';

			$this->load->view(BACKEND, $this->data);
		}
	}

	function ajax_preview_invoice()
	{

		$post = NVPToArray($_POST['formdata']);
		$post['invoice_id'] = uniqueid('INV', 5, 'tbl_invoice', 'invoice_id');
		$post['payment_url'] = base_url('quote/payment?invoice_id=' . $post['invoice_id']);

		$booking = (array) $this->booking_info_model->get(array('booking_ref_id' => $post['booking_id']));
		$post = $post + $booking;

		$invoice_data = array(
			'mail_subject' => $post['mail_subject'],
			'mail_message' => $post['mail_message']
		);
		$invoice_mail_template = $this->invoice_model->create_invoice($post, $booking, $booking['total_fare'], $booking['booking_ref_id']);


		$data = array(
			'is_send_email' => $post['is_send_email'],
			'invoice_id' => $post['invoice_id'],
			'booking_id' => $post['booking_id'],
			'customer_email' => $post['customer_email'],
			'invoice_amount' => $post['invoice_amount'],
			'mail_subject' => $post['mail_subject'],
			'mail_template' => $invoice_mail_template
		);

		jsonOutput(1, 'Email template preview', $data);
	}

	function resend_invoice()
	{
		if ($_POST) {
			$post = $this->input->post();

			$this->db->update('tbl_invoice', array('mail_template' => $post['mail_template']), array('invoice_id' => $post['invoice_id']));

			$invoice = $this->invoice_model->get_by_invoice_id($post['invoice_id']);

			$recipients = explode(',', $invoice->customer_email);

			$this->load->helper('email_helper');

			email_help($recipients, $invoice->mail_subject, $post['mail_template'], [SITE_EMAIL => SITE_NAME]);
			$this->db->update('tbl_invoice', array('invoice_mailed_datetime' => date('Y-m-d H:i:s')), array('invoice_id' => $invoice->invoice_id));

			set_flash('msg', "Invoice ID: $invoice->invoice_id has been resent to email: $invoice->customer_email");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function ajax_preview_resend_invoice()
	{
		$post = $this->input->post();

		$invoice = $this->invoice_model->get_by_invoice_id($post['invoice_id']);
		jsonOutput(1, 'Invoice details.', $invoice);
	}

	function ajax_preview_receipt()
	{

		$post = NVPToArray($_POST['formdata']);

		$invoice = $this->db->get_where('tbl_invoice', array('invoice_id' => $post['invoice_id']))->result_array();
		if (!$invoice[0]['booking_id'] && !$invoice[0]['cart_id']) {
			$bulk_invoice_bids = $this->db->get_where('tbl_invoice_bulk', array('invoice_id' => $post['invoice_id']))->result_array();
			$booking_id = $bulk_invoice_bids[0]['booking_id'];
		} else {
			$booking_id = $post['booking_id'];
		}

		$booking = $this->db->get_where('tbl_booking_infos', array('booking_ref_id' => $booking_id))->result_array();


		if (!isset($booking[0]) || !isset($invoice[0])) {
			jsonOutput(0, 'Invalid Booking/Invoice Id.');
		}

		if (!$invoice[0]['booking_id'] && !$invoice[0]['cart_id']) {
			$booking_ids = array();
			foreach ($bulk_invoice_bids as $b_ids) {
				array_push($booking_ids, $b_ids['booking_id']);
			}
			$post['booking_ids'] = implode(',', $booking_ids);
		}

		$post = $post + $booking[0] + $invoice[0];

		$post['prev_receipts'] = $this->invoice_model->getAllPaidReceipts(isset($post['booking_ids']) ? explode(',', $post['booking_ids']) : array($post['booking_id']));

		$invoice_mail_template = $this->load->view('emailer/_emailer_receipt', array('data' => $post, 'is_manual_receipt' => false), true);

		$data = array(
			'is_send_email' => $post['is_send_email'],
			'customer_email' => $post['customer_email'],
			'date_of_payment' => $post['date_of_payment'],
			'invoice_payment_method' => $post['invoice_payment_method'],
			'mail_subject' => $post['mail_subject'],
			'mail_template' => $invoice_mail_template
		);
		jsonOutput(1, 'Email template preview', $data);
	}

	function receipt()
	{
		$invoice = $this->invoice_model->get(array('invoice_id' => segment(4)));
		if ($invoice) {
			echo $invoice->mail_template_receipt;
		} else {
			echo 'No receipt found for invoice id: ' . segment(4);
		}
	}

	function resend_receipt($invoice_id)
	{
		$invoice = $this->invoice_model->get(array('invoice_id' => $invoice_id, 'is_cancelled' => 0));
		if ($invoice) {

			$recipients = explode(',', $invoice->customer_email);
			$this->load->helper('email_helper');
			email_help($recipients, $invoice->mail_subject_receipt, $invoice->mail_template_receipt, [SITE_EMAIL => SITE_NAME]);

			$this->db->update('tbl_invoice', array('receipt_mailed_datetime' => date('Y-m-d H:i:s')), array('invoice_id' => $invoice_id));

			set_flash('msg', "Receipt for Invoice ID: $invoice_id has been resent to email: {$invoice->customer_email}");
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			die('Invalid Request.');
		}
	}

	function send_receipt($invoice_id)
	{

		if ($this->input->post()) {

			$post = $this->input->post();

			$newArr = array();
			foreach ($post as $k => $v) {
				$newArr[substr($k, 2)] = $v;
			}

			$newArr['payment_status'] = 1;

			$recipients = explode(',', $newArr['customer_email']);

			foreach ($recipients as $e) {
				if (!filter_var($e, FILTER_VALIDATE_EMAIL)) {
					die('Recipients Emails (' . $newArr['customer_email'] . ') has an invalid email.');
				}
			}

			$is_send_email = $newArr['is_send_email'];
			unset($newArr['is_send_email']);

			if ($is_send_email) {
				$newArr['receipt_mailed_datetime'] = date('Y-m-d H:i:s');
			}

			$this->invoice_model->update($newArr, array('invoice_id' => segment(4)));

			if ($is_send_email) {

				email_help($recipients, $newArr['mail_subject_receipt'], $newArr['mail_template_receipt'], [SITE_EMAIL => SITE_NAME]);

				set_flash('msg', 'Payment confirmed for invoice id: ' . $newArr['invoice_id'] . ' & receipt emailed to: ' . $newArr['customer_email']);
			} else {
				set_flash('msg', 'Payment confirmed for invoice id: ' . $newArr['invoice_id'] . ', but receipt not emailed.');
			}
			redirect($this->agent->referrer());
		} else {
			$this->data['invoice'] = $this->db->get_where('tbl_invoice', array('invoice_id' => $invoice_id))->row_array();
			$this->data['booking'] = $this->booking_info_model->get(array('booking_ref_id' => $this->data['invoice']['booking_id']));

			$this->data['main_content'] = 'admin/booking_info/index';
			$this->data['sub_content'] = 'admin/booking_info/_send_receipt';

			$this->load->view(BACKEND, $this->data);
		}
	}

	function confirm_booking()
	{
		if ($_POST) {
			$post = $_POST;

			$recipients = explode(',', $post['i_customer_email']);
			foreach ($recipients as $e) {
				if (!filter_var($e, FILTER_VALIDATE_EMAIL)) {
					die('Recipients Emails (' . $post['i_customer_email'] . ') has an invalid email.');
				}
			}
			$recipients[] = ADMIN_EMAIL_1;

			$confirm_data = array(
				'booking_status' => 'confirmed',
				'mail_subject_confirm' => $post['i_mail_subject'],
				'mail_template_confirm' => $post['i_mail_template']
			);

			$this->booking_info_model->update($confirm_data, array('booking_ref_id' => $post['i_booking_id']));
			$booking = $this->booking_info_model->get(array('booking_ref_id' => $post['i_booking_id']));

			email_help($recipients, $post['i_mail_subject'], $post['i_mail_template'], [SITE_EMAIL => SITE_NAME]);

			// $msisdn = implode(',', array($booking->phone_cc . $booking->client_phone));
			// $sms_message = "Dear {$booking->client_name}, Booking [{$booking->booking_ref_id}]  has been confirmed.";
			// $post_body = $this->twiliosms->seven_bit_sms($sms_message, $msisdn);
			// $result = $this->twiliosms->send_message($post_body);

			set_flash('msg', 'Booking marked as confirmed and emailed send to: ' . $booking->phone_cc . $booking->client_phone . ' and ' . $post['i_customer_email'] . ' respectively.');
			redirect(site_url('admin/booking/view/' . $booking->id));
		}

		$this->data['booking'] = $this->booking_info_model->get(array('id' => $_GET['booking_id']));
		$this->data['main_content'] = 'admin/booking_info/index';
		$this->data['sub_content'] = 'admin/booking_info/_confirm_booking';

		$this->load->view(BACKEND, $this->data);
	}
	

	function ajax_preview_confirm_booking()
	{
		$post = NVPToArray($_POST['formdata']);
		$booking = (array) $this->booking_info_model->get(array('booking_ref_id' => $post['booking_id']));
		$post_for_emailer = $post;
		unset($post_for_emailer['customer_email']);
		$emailer_data = array_merge($booking, $post_for_emailer);
		$booking_mail_template = ($this->load->view('emailer/booking_emailler', array('data' => $emailer_data, 'emailer_to' => 'confirm_client'), true));
		$data = array(
			'booking_id' => $post['booking_id'],
			'customer_email' => $post['customer_email'],
			'mail_subject' => $post['mail_subject'],
			'mail_template' => $booking_mail_template
		);

		jsonOutput(1, 'Email template preview', $data);
	}

	function preview_confirm($booking_id = null)
	{
		$booking = $this->booking_info_model->get(array('id' => $booking_id));
		if ($booking) {
			echo $booking->mail_template_confirm;
		} else {
			echo 'No confirmed email for this booking id: ';
		}
	}
	function resend_confirm_booking($booking_id = null)
	{
		$booking = $this->booking_info_model->get(array('id' => $booking_id));
		if ($booking) {
			$passenger = $this->passenger_model->get(array('id' => $booking->passenger_id));
			email_help($passenger->email ?? $booking->client_email, $booking->mail_subject_confirm, $booking->mail_template_confirm, [SITE_EMAIL => SITE_NAME]);
			set_flash('msg', 'Conformation email has been send successfully.');
		} else {
			set_flash('msg', 'No confirmed email for this booking');
		}
		redirect(site_url('admin/booking/view/' . $booking->id));
	}

	function ajax_get_invoice_details()
	{

		$invoice_id = $this->input->get('invoice_id');

		$invoice = $this->invoice_model->get_by_invoice_id($invoice_id);
		if (!$invoice) {
			jsonOutput(0, 'Invalid invoice_id');
		}

		jsonOutput(1, 'Invoice details.', $invoice);
	}

	function invoice_edit()
	{

		$invoice_id = $this->input->post('invoice_id');
		$invoice_payment_method = $this->input->post('invoice_payment_method');
		$invoice = $this->invoice_model->get_by_invoice_id($invoice_id);
		if (!$invoice) {
			show_error('Invalid invoice id.');
		}

		$invoice_data = array(
			'invoice_payment_method' => $invoice_payment_method
		);

		$is_updated = $this->invoice_model->update($invoice_data, ['id' => $invoice->id]);
		if (!$is_updated) {
			show_error($this->db->_error_message());
		}

		set_flash('msg', 'Invoice updated successfully.');
		redirect($this->agent->referrer());
	}

	function invoice_bulk()
	{

		$post = $this->input->post();

		if (isset($post['checked_item'])) {
			$this->data['booking_ids'] = array();

			$this->data['invoice_total'] = 0;
			foreach ($post['checked_item'] as $index => $ci) {
				$booking = $this->common_model->get_where('tbl_booking_infos', array('id' => $ci));

				if ($booking[0]['passenger_id']) {
					$account = $this->common_model->get_where('passenger', array('id' => $booking[0]['passenger_id']));
					$this->data['account'] = $account[0];
				}

				$this->data['bookings'][$index] = $booking[0];
				array_push($this->data['booking_ids'], $booking[0]['booking_ref_id']);
			}

			$this->data['invoice_total'] = $this->invoice_model->get_due_amount_bulk($this->data['booking_ids']);
			$this->data['invoice_id'] = uniqueid('INV', 5, 'tbl_invoice', 'invoice_id');

			$this->data['main_content'] = 'admin/booking_info/index';
			$this->data['sub_content'] = 'admin/booking_info/_raise_invoice_bulk';
			$this->load->view(BACKEND, $this->data);
		} elseif (isset($post['i_invoice_id'])) {
			$newArr = array();
			foreach ($post as $k => $v) {
				if ($k == 'i_booking_ids')
					continue;
				$newArr[substr($k, 2)] = $v;
			}

			$recipients = explode(',', $newArr['customer_email']);

			foreach ($recipients as $e) {
				if (!filter_var($e, FILTER_VALIDATE_EMAIL)) {
					die('Recipients Emails (' . $newArr['customer_email'] . ') has an invalid email.');
				}
			}

			unset($newArr['customer_name']);

			$newArr['invoice_mailed_datetime'] = date('Y-m-d H:i:s');
			$this->invoice_model->insert($newArr);

			$booking_ids = explode(',', $post['i_booking_ids']);
			foreach ($booking_ids as $b_id) {
				$b_detail = $this->booking_info_model->get(['booking_ref_id' => $b_id]);
				$invoice_bulk_data = array(
					'booking_id' => $b_id,
					'invoice_id' => $post['i_invoice_id'],
					'partial_amount' => $this->invoice_model->get_due_amount($b_id) //$b_detail->final_fare
				);
				$this->invoice_bulk_model->insert($invoice_bulk_data);
				$this->common_model->update('tbl_booking_infos', array('is_bulk_invoice_raised' => 1), array('booking_ref_id' => $b_id));
			}


			email_help($recipients, $newArr['mail_subject'], $newArr['mail_template'], [SITE_EMAIL => SITE_NAME]);
			set_flash('msg', 'Invoice has been successfully generated and emailed to: ' . $newArr['customer_email']);
			redirect(site_url('admin/booking'));
		}
	}

	function ajax_preview_invoice_bulk()
	{

		$post = NVPToArray($_POST['formdata']);

		$post['booking_ids'] = explode(',', $post['booking_ids']);
		$post['payment_url'] = base_url('quote/payment?invoice_id=' . $post['invoice_id']);

		if (isset($post['account_id']) && $post['account_id']) {
			$account = $this->common_model->get_where('passenger', array('id' => $post['account_id']));
		}

		$emailer_data = array(
			'data' => $post
		);

		if (isset($account) && $account) {
			$emailer_data['account'] = $account[0];
		}

		$invoice_mail_template = common_emogrifier($this->load->view('emailer/_emailer_invoice_bulk', $emailer_data, true));

		$data = array(
			'invoice_id' => $post['invoice_id'],
			'booking_ids' => $post['booking_ids'],
			'customer_email' => $post['customer_email'],
			'customer_name' => $post['customer_name'],
			'invoice_amount' => $post['invoice_amount'],
			'mail_subject' => $post['mail_subject'],
			'mail_template' => $invoice_mail_template
		);
		jsonOutput(1, 'Email template preview', $data);
	}

	public function make_booking()
	{
		$this->data['fleets'] = $this->fleet_model->get_all(['status' => 1]);
		$this->data['main_content'] = 'admin/booking_info/index';
		$this->data['sub_content'] = 'admin/booking_info/make_booking';
		$this->load->view(BACKEND, $this->data);
	}

	function airport_list()
	{
		$locations = $this->location_model->get_by_location_type_id(1);
		if ($locations) {
			jsonOutput(true, 'airports', $locations);
		} else {
			jsonOutput(false, 'No airports in database.');
		}
	}

	public function location_details($location_id)
	{
		$location = $this->location_model->get(['id' => $location_id]);
		if ($location) {
			jsonOutput(true, 'airport', $location);
		} else {
			jsonOutput(false, 'Location not found.');
		}
	}
	function calculate_fare()
	{

		$start = $this->input->get('start');
		$start_id = $this->input->get('start_id');
		$end = $this->input->get('end');
		$end_id = $this->input->get('end_id');
		$pickup_datetime = $this->input->get('pickup_datetime');
		$return_datetime = !empty($this->input->get('return_datetime')) ? $this->input->get('return_datetime') : '';
		$service_type = $this->input->get('service_type');
		$journey_type = $this->input->get('journey_type');
		$duration_hourly = !empty($this->input->get('duration')) ? $this->input->get('duration') : '';
		$from_dispatch = !empty($this->input->get('from_dispatch')) ? 0 : 1;
		if (!$from_dispatch) {
			$way_lat_lng = viaPointFormat($this->input->get());
			$stop_point = (!empty($this->input->get('stop_point'))) ? json_encode($this->input->get('stop_point')) : '';
		} else {
			$way_lat_lng = (!empty($this->input->get('way_lat_lng'))) ? $this->input->get('way_lat_lng') : '';
			$stop_point = (!empty($this->input->get('stop_point'))) ? $this->input->get('stop_point') : '';
		}
		// check if both location filled.
		if (!$start || !$end) {
			jsonOutput(false, 'Please fill both location.');
		}
		$response_data = [];
		$quote = array(
			'start' => $start,
			'start_id' => $start_id,
			'end' => $end,
			'end_id' => $end_id,
			'booking_type' => $service_type,
			'journey_type' => $journey_type,
			'duration_hourly' => $duration_hourly,
			'date' => DateTime::createFromFormat('d/m/Y h:ia', $pickup_datetime)->format('d/m/Y'),
			'time' => DateTime::createFromFormat('d/m/Y h:ia', $pickup_datetime)->format('H:i')
		);
		$quote['return_date'] = $quote['return_time'] = '';
		if ($journey_type == 'two_way') {
			$quote['return_date'] = DateTime::createFromFormat('d/m/Y h:ia', $return_datetime)->format('d/m/Y');
			$quote['return_time'] = DateTime::createFromFormat('d/m/Y h:ia', $return_datetime)->format('H:i');
		}
		$fleets = $this->fleet_model->get_all(['status' => 1]);
		$fleets = $this->computeRateAndStoreInSession($quote, $fleets);

		if (empty($fleets)) {
			jsonOutput(false, 'No fleets were found for this journey.');
		}
		$response_data['fleets'] = $fleets;
		echo json_encode($response_data);
	}

	private function computeRateAndStoreInSession($quote, $fleets)
	{

		foreach ($fleets as $key => $fleet) {
			$additional_rate = $this->additional_rate_model->get(['fleet_id' => $fleet->id]);
			$fleet->charge_details = [];
			$one_way_base_fare = $two_way_base_fare = $fleet->fare = 0;

			$route_fare = $this->calculate_route_fare($quote['start_id'], $quote['end_id'], $fleet->id);

			// if (empty($route_fare)) {
			//     unset($fleets[$key]);
			// }
			$one_way_base_fare = round($route_fare, 2);
			$two_way_base_fare = round($one_way_base_fare * 2, 2);

			$fleet->fare = $one_way_base_fare;
			$fleet->round_trip_fare = $two_way_base_fare;

			$fleet->charge_details['seed_fare'] = ($quote['journey_type'] == 'two_way') ? $two_way_base_fare : $one_way_base_fare;
			if (($quote['journey_type'] == 'two_way')) {
				$fleet->charge_details['round_trip_discount'] = round($two_way_base_fare * $additional_rate->round_trip * 0.01, 2);
				$fleet->charge_details['round_trip_discount_percentage'] = $additional_rate->round_trip;
			}

			$additional_via_point = (!empty($quote['stop_point']) ? count($quote['stop_point']) * $additional_rate->via_point : 0);
			$fleet->charge_details['additional_via_point'] = $additional_via_point;
			$fleet->fare += $additional_via_point;
			$fleet->round_trip_fare += $additional_via_point;

			$fleet->charge_details['raise_by'] = $additional_rate->raise_by;
			if ($additional_rate->raise_by_type == 'percentage') {
				$raise_by_one_amount = round($one_way_base_fare * 0.01 * $additional_rate->raise_by, 2);
				$raise_by_two_amount = round($two_way_base_fare * 0.01 * $additional_rate->raise_by, 2);
			} else {
				$raise_by_one_amount = $raise_by_two_amount = round($additional_rate->raise_by, 2);
			}
			$fleet->charge_details['raise_by_amount'] = round(($quote['journey_type'] == 'two_way' ? $raise_by_two_amount : $raise_by_one_amount), 2);

			$fleet->fare += $raise_by_one_amount;
			$fleet->round_trip_fare += $raise_by_two_amount;


			$fleet->fare = round($fleet->fare, 2);
			$fleet->round_trip_fare = round(isset($fleet->round_trip_fare) ? $fleet->round_trip_fare : 0, 2);
			$fleet->total = ($quote['journey_type'] == 'two_way') ? $fleet->round_trip_fare : $fleet->fare;
			unset($fleet->charge_details['round_trip_discount'], $fleet->charge_details['round_trip_discount_percentage'], $fleet->charge_details['raise_by']);
		}
		return ($fleets) ? array_values($fleets) : '';
	}

	private function calculate_route_fare($start_id, $end_id, $fleet_id)
	{
		$route_rate = 0;
		$result = $this->db->get_where('tbl_routes_rate', ['from_id' => $start_id, 'to_id' => $end_id, 'rate >' => '0', 'fleet_id' => $fleet_id])->row();
		if (!empty($result->rate)) {
			$route_rate = round($result->rate, 2);
		}
		return $route_rate;
	}

	public function preview_emailer()
	{

		$post = $this->input->post();
		if (isset($post['baby_seat_details'])) {
			$post['baby_seat_details'] = implode(',', $post['baby_seat_details']);
		}


		$user_agent_data = array(
			'Browser' => $this->agent->browser(),
			'Version' => $this->agent->version(),
			'OS' => $this->agent->platform(),
			'Mobile Device' => $this->agent->mobile(),
			'IP Address' => $this->input->ip_address()
		);
		$booking_info = array(
			'booking_ref_id' => '',
			'passenger_id' => $post['passenger_id'],
			'distance' => '',
			'duration' => '',
			'pickup_address' => $post['start'],
			'dropoff_address' => $post['end'],
			'start_post_code' => '',
			'end_post_code' => '',
			'start_id' => $post['start_id'],
			'end_id' => $post['end_id'],

			'start_lat' => '',
			'start_lng' =>  '',
			'end_lat' => '',
			'end_lng' => '',

			'pickup_date' => SMDCrateDateTimeFormat($post['pickup_datetime'], 'Y-m-d H:i:s', 'd/m/Y'),
			'pickup_time' => SMDCrateDateTimeFormat($post['pickup_datetime'], 'Y-m-d H:i:s', 'H:i'),
			'journey_type' => $post['journey_type'],
			'via_point' => !empty($post['stop_point']) ? json_encode($post['stop_point']) : NULL,
			'total_fare' => $post['final_fare'],
			'discount_fare' => 0,
			'vehicle_name' => $post['fare_details']['selected_fleet']['title'],
			'selected_fleet_id' => $post['fare_details']['selected_fleet']['id'],

			'flight_number' => !empty($post['flight_no']) ? $post['flight_no'] : '',
			'flight_name' => !empty($post['flight_name']) ? $post['flight_name'] : '',
			'display_name' => '',
			'landing_time' => '',
			'after_landing_time' => '',
			'flight_arrive_from' => '',
			'cruise_name' => '',
			'cruise_landing_time' => '',
			'cruise_arrive_from' => '',
			'cruise_after_landing_time' => '',

			'client_name' => $post['passenger_name'],
			'client_email' => $post['passenger_email'],
			'phone_cc' => '',
			'client_phone' => $post['passenger_cell_no'],

			'class_type' => '',
			'password_type' => '',
			'return_class_type' => '',
			'return_password_type' => '',
			'client_address' => '',
			'lead_passenger_name' => '',
			'lead_phone_cc' => '',
			'lead_passenger_phone' => '',

			'client_passanger_no' => $post['passengers'],
			'client_baby_no' => $post['baby_seat'],
			'baby_seat_details' => !empty($post['baby_seat_details']) ? $post['baby_seat_details'] : '',
			'client_luggage' => $post['luggage'],
			'client_hand_luggage' => 0,

			'pickup_address_line' => '',
			'pickup_address_door_no' => '',
			'pickup_address_post_code' => '',
			'pickup_address_town_city' => '',
			'dropoff_address_line' => '',
			'dropoff_address_door_no' => '',
			'dropoff_address_postcode' => '',
			'dropoff_address_town_city' => '',

			'client_return_date' => !empty($post['return_datetime']) ? SMDCrateDateTimeFormat($post['return_datetime'], 'Y-m-d H:i:s', 'd/m/Y') : '',
			'client_return_time' => !empty($post['return_datetime']) ? SMDCrateDateTimeFormat($post['return_datetime'], 'Y-m-d H:i:s', 'H:i') : '',

			'return_cruise_name' => '',
			'return_cruise_arrive_from' => '',
			'return_cruise_after_landing_time' => '',
			'return_flight_no' => '',
			'return_flight_arrive_from' => '',
			'return_after_landing_time' => '',
			'return_display_name' => '',

			'message' => $post['special_instruction'],
			'pay_method' => $post['payment_method'],
			'user_agent_data' => json_encode($user_agent_data),
		);

		$emailer_data = ['data' => $booking_info, 'emailer_to' => 'client'];
		$emailer_booking = ($this->load->view('emailer/booking_emailler', $emailer_data, true));
		echo json_encode($emailer_booking);
	}
	public function create_booking()
	{
		$post = $this->input->post();
		if (isset($post['baby_seat_details'])) {
			$post['baby_seat_details'] = implode(',', $post['baby_seat_details']);
		}


		$user_agent_data = array(
			'Browser' => $this->agent->browser(),
			'Version' => $this->agent->version(),
			'OS' => $this->agent->platform(),
			'Mobile Device' => $this->agent->mobile(),
			'IP Address' => $this->input->ip_address()
		);
		$booking_ref_id = uniqueid('', 7, 'tbl_booking_infos', 'booking_ref_id');
		$booking_info = array(
			'booking_ref_id' => $booking_ref_id,
			'passenger_id' => $post['passenger_id'],
			'distance' => '',
			'duration' => '',
			'pickup_address' => $post['start'],
			'dropoff_address' => $post['end'],
			'start_post_code' => '',
			'end_post_code' => '',
			'start_id' => $post['start_id'],
			'end_id' => $post['end_id'],

			'start_lat' => '',
			'start_lng' => '',
			'end_lat' => '',
			'end_lng' =>  '',

			'pickup_date' => SMDCrateDateTimeFormat($post['pickup_datetime'], 'Y-m-d H:i:s', 'd/m/Y'),
			'pickup_time' => SMDCrateDateTimeFormat($post['pickup_datetime'], 'Y-m-d H:i:s', 'H:i'),
			'journey_type' => $post['journey_type'],
			'via_point' => !empty($post['stop_point']) ? json_encode($post['stop_point']) : NULL,
			'total_fare' => $post['final_fare'],
			'discount_fare' => 0,
			'vehicle_name' => $post['fare_details']['selected_fleet']['title'],
			'selected_fleet_id' => $post['fare_details']['selected_fleet']['id'],

			'flight_number' => !empty($post['flight_no']) ? $post['flight_no'] : '',
			'flight_name' => !empty($post['flight_name']) ? $post['flight_name'] : '',
			'display_name' => '',
			'landing_time' => '',
			'after_landing_time' => '',
			'flight_arrive_from' => '',
			'cruise_name' => '',
			'cruise_landing_time' => '',
			'cruise_arrive_from' => '',
			'cruise_after_landing_time' => '',

			'client_name' => $post['passenger_name'],
			'client_email' => $post['passenger_email'],
			'phone_cc' => '',
			'client_phone' => $post['passenger_cell_no'],

			'class_type' => '',
			'password_type' => '',
			'return_class_type' => '',
			'return_password_type' => '',
			'client_address' => '',
			'lead_passenger_name' => '',
			'lead_phone_cc' => '',
			'lead_passenger_phone' => '',

			'client_passanger_no' => $post['passengers'],
			'client_baby_no' => $post['baby_seat'],
			'baby_seat_details' => !empty($post['baby_seat_details']) ? $post['baby_seat_details'] : '',
			'client_luggage' => $post['luggage'],
			'client_hand_luggage' => 0,

			'pickup_address_line' => '',
			'pickup_address_door_no' => '',
			'pickup_address_post_code' => '',
			'pickup_address_town_city' => '',
			'dropoff_address_line' => '',
			'dropoff_address_door_no' => '',
			'dropoff_address_postcode' => '',
			'dropoff_address_town_city' => '',

			'client_return_date' => !empty($post['return_datetime']) ? SMDCrateDateTimeFormat($post['return_datetime'], 'Y-m-d H:i:s', 'd/m/Y') : '',
			'client_return_time' => !empty($post['return_datetime']) ? SMDCrateDateTimeFormat($post['return_datetime'], 'Y-m-d H:i:s', 'H:i') : '',

			'return_cruise_name' => '',
			'return_cruise_arrive_from' => '',
			'return_cruise_after_landing_time' => '',
			'return_flight_no' => '',
			'return_flight_arrive_from' => '',
			'return_after_landing_time' => '',
			'return_display_name' => '',

			'message' => $post['special_instruction'],
			'pay_method' => $post['payment_method'],
			'user_agent_data' => json_encode($user_agent_data),
		);

		$booking_id = $this->booking_info_model->insert($booking_info);

		if (!$booking_id) {
			jsonOutput(false, "Sorry for the inconvenience. Error occur while generating booking. Please try again");
		}
		jsonOutput(true, "created.", ['id' => $booking_id]);
	}
	function ajaxViewBooking($id)
	{
		$booking = $this->booking_info_model->get(array('id' => $id));
		$drivers = $this->driver_model->get_all();
		$data = ['booking' => $booking, 'drivers' => $drivers];
		$html = $this->load->view('admin/booking_info/ajax/details', $data, true);
		jsonOutput(true, 'Booking details', ['html' => $html]);
	}

	public function diary()
	{
		$this->data['main_content'] = 'admin/booking_info/index';
		$this->data['sub_content'] = 'admin/booking_info/_booking_calendar';
		$this->load->view(BACKEND, $this->data);
	}

	function ajax_get_resources()
	{
		$get = $this->input->get();

		$start = new DateTime($get['start']);
		$end = new DateTime($get['end']);
		$start = $start->format('Y-m-d');
		$end = $end->modify('-1 day')->format('Y-m-d');
		$resources = $this->db->query("SELECT * FROM tbl_booking_infos WHERE STR_TO_DATE(pickup_date,'%d/%m/%Y') BETWEEN '{$start}' AND '{$end}'")->result();
		$data_resources = [];
		if ($resources)
			foreach ($resources as $r) {

				$data_resources[] = array(
					"id" => $r->id,
					"booking_ref_id" => $r->booking_ref_id,
					"start" => DateTime::createFromFormat('d/m/Y H:i', $r->pickup_date . ' ' . $r->pickup_time)->format('Y-m-d H:i:s'),
					"end" => convertToEndDate($r->pickup_date . ' ' . $r->pickup_time, 'd/m/Y H:i', 'Y-m-d H:i:s', $r->duration_seconds),
					"pickup_address" => $r->pickup_address,
					"dropoff_address" => $r->dropoff_address,
					"pickup_date" => $r->pickup_date,
					"pickup_time" => SMDCrateDateTimeFormat($r->pickup_time, 'H:i', 'h:ia'),
					"payment_method" => $r->pay_method,
					"payment_status" => $r->is_paid,
					"booked_by" => $r->client_name,
					"email" => $r->client_email,
					"job_status" => $r->job_status,
					"is_cancelled" => $r->is_cancelled,
					"booking_type" => $r->booking_type,
					"driver_name" => '',
					"booked_datetime" => DateTime::createFromFormat('Y-m-d H:i:s', $r->created_at)->format('d M, Y'),
				);
			}

		if (!$data_resources) {
			$data_resources = [['id' => 'none', 'title' => 'Booking not found']];
		}
		echo json_encode($data_resources);
	}

	function ajax_get_events()
	{
		$get = $this->input->get();

		$start = new DateTime($get['start']);
		$end = new DateTime($get['end']);
		$start = $start->format('Y-m-d');
		$end = $end->modify('-1 day')->format('Y-m-d');
		$events = $this->db->query("SELECT * FROM tbl_booking_infos WHERE STR_TO_DATE(pickup_date,'%d/%m/%Y') BETWEEN '{$start}' AND '{$end}' ")->result();

		if ($events)
			foreach ($events as $r) {
				$data_events[] = array(
					"id" => $r->id,
					"resourceId" => $r->id,
					"booking_ref_id" => $r->booking_ref_id,
					"start" => DateTime::createFromFormat('d/m/Y H:i', $r->pickup_date . ' ' . $r->pickup_time)->format('Y-m-d H:i:s'),
					"end" => convertToEndDate($r->pickup_date . ' ' . $r->pickup_time, 'd/m/Y H:i', 'Y-m-d H:i:s', $r->duration_seconds),
					"pickup_address" => $r->pickup_address,
					"dropoff_address" => $r->dropoff_address,
					"pickup_date" => $r->pickup_date,
					"pickup_time" => SMDCrateDateTimeFormat($r->pickup_time, 'H:i', 'h:ia'),
					"payment_method" => $r->pay_method,
					"payment_status" => $r->is_paid,
					"booked_by" => $r->client_name,
					"email" => $r->client_email,
					"job_status" => $r->job_status,
					"is_cancelled" => $r->is_cancelled,
					"booking_type" => $r->booking_type,
					"driver_name" => '',
					"booked_datetime" => DateTime::createFromFormat('Y-m-d H:i:s', $r->created_at)->format('d M, Y'),
				);
			}

		if (empty($data_events)) {
			$data_events = '';
		}
		echo json_encode($data_events);
	}


	function ajax_getLocations()
	{

		if (!$this->input->is_ajax_request()) {
			show_404();
		}

		$keywords = $_GET['q'];

		$result = $this->location_model->search($keywords);

		if ($result) {
			$data = array();
			foreach ($result as $r) {
				array_push($data, array(
					'id' => $r->id,
					'label' => $r->name,
					'logo' => getFAByLocationTypeId($r->location_type_id),
					'type_id' => $r->location_type_id,
				));
			}
			jsonOutput(1, 'query: ' . $keywords, $data);
		} else {
			jsonOutput(0, 'something went wrong (Keywords):' . $keywords);
		}
	}

	function quote()
	{
		$this->data['passengers'] = $this->passenger_model->get_all(['is_active' => 1]);
		$this->data['passenger'] = $this->passenger_model->get(array('id' => $this->session->userdata('logged_in_passenger')));
		// debug($this->data['passengers']);

		if (empty(getSession('journey'))) {
			setSession('journey', 0);
		}
		if (empty(getSession('journey_type'))) {
			setSession('journey_type', 'one_way');
		}
		if (empty(getSession('screen'))) {
			setSession('screen', 'quote-form');
		}
		if (empty(getSession('discount'))) {
			setSession('discount', ['code' => '', 'amount' => 0]);
		}

		if (!empty($_GET['action'])) {
			if ($_GET['action'] == 'add_new_journey') {
				if (!empty($_SESSION['journeys'][getSession('journey')]['one_way']['quote'])) {
					setSession('journey', count($_SESSION['journeys']));
				}
				setSession('screen', 'quote-form');
			} elseif ($_GET['action'] == 'add_return_journey' && isset($_GET['journey'])) {
				$_SESSION['journeys'][getSession('journey')]['journey_type'] = 'two_way';
				setSession('journey_type', 'two_way');
				setSession('journey', $_GET['journey']);
				setSession('screen', 'booking-form-two');
				redirect(site_url('#booking'));
			} elseif ($_GET['action'] == 'remove_journey' && isset($_GET['journey']) && isset($_GET['type'])) {
				$_SESSION['journeys'][getSession('journey')]['journey_type'] = 'one_way';
				setSession('journey_type', 'one_way');
				setSession('journey', getSession('journey'));
				setSession('screen', 'confirm');

				if ($_GET['type'] == 'one_way') {
					$one_way = $_SESSION['journeys'][getSession('journey')]['one_way'];
					$two_way = $_SESSION['journeys'][getSession('journey')]['two_way'];
					$_SESSION['journeys'][getSession('journey')]['one_way'] = $two_way;
					$_SESSION['journeys'][getSession('journey')]['two_way'] = $one_way;
				}

				if ($_SESSION['journeys'][getSession('journey')]['journey_type'] == 'one_way') {
					unset($_SESSION['journeys'][getSession('journey')]);
				}
			}
			redirect(site_url());
		}

		$journey = getSession('journey');
		$journeys = getSession('journeys');

		if (empty($journeys[$journey]['one_way'])) {
			$_SESSION['journeys'][$journey]['one_way'] = ['quote' => '', 'fleets' => '', 'selected_fleet' => '', 'booking_details' => '', 'grand_total_charge' => ''];
			setSession('discount', ['code' => '', 'amount' => 0]);
		}
		// if (empty($journeys[$journey]['two_way'])) {
		//     $_SESSION['journeys'][$journey]['two_way']=['quote'=>'','fleets'=>'','selected_fleet'=>'','booking_details'=>'','grand_total_charge'=>''];
		// }

		$this->data['fleets'] = $this->fleet_model->order_by('sort')->get_all(['status' => 1]);
		$this->data['additional_rate'] = ['meet_and_greet' => 0, 'baby_seater' => 0];
		if (!empty($_SESSION['journeys'][getSession('journey')][getSession('journey_type')]['selected_fleet']->id)) {
			$conditions = ['fleet_id' => $_SESSION['journeys'][getSession('journey')][getSession('journey_type')]['selected_fleet']->id];
			$this->data['additional_rate'] =  $this->db->select('*')->get('tbl_additional_rate', $conditions)->row();
			if ($this->data['additional_rate']) {
				$this->data['additional_rate']->multi_airport = json_decode($this->data['additional_rate']->multi_airport);
				$this->data['additional_rate']->multi_seaport = json_decode($this->data['additional_rate']->multi_seaport);
				$this->data['additional_rate']->multi_station = json_decode($this->data['additional_rate']->multi_station);
				$this->data['additional_rate']->multi_airport_dropoff = json_decode($this->data['additional_rate']->multi_airport_dropoff);
				$this->data['additional_rate']->multi_seaport_dropoff = json_decode($this->data['additional_rate']->multi_seaport_dropoff);
				$this->data['additional_rate']->multi_station_dropoff = json_decode($this->data['additional_rate']->multi_station_dropoff);
			}
		}

		$this->data['main_content'] = 'admin/booking_info/create_booking/index';
		$this->data['sub_content'] = 'admin/booking_info/create_booking/quote';
		// $this->data['main_content'] = 'admin/booking_info/index';
		// $this->data['sub_content'] = 'admin/booking_info/make_booking';
		$this->load->view(BACKEND, $this->data);
	}

	function basket()
	{

		if (empty(getSession('journey'))) {
			setSession('journey', 0);
		}
		if (empty(getSession('journey_type'))) {
			setSession('journey_type', 'one_way');
		}
		if (empty(getSession('screen'))) {
			setSession('screen', 'quote-form');
		}
		if (empty(getSession('discount'))) {
			setSession('discount', ['code' => '', 'amount' => 0]);
		}

		$quote = $this->input->post();
		if ($quote['type'] == 'add_new_journey') {
			if (!empty($_SESSION['journeys'][getSession('journey')]['one_way']['quote'])) {
				setSession('journey', count($_SESSION['journeys']));
			}
			setSession('screen', 'quote-form');
		} elseif ($quote['type'] == 'add_return_journey' && isset($_GET['journey'])) {
			$_SESSION['journeys'][getSession('journey')]['journey_type'] = 'two_way';
			setSession('journey_type', 'two_way');
			setSession('journey', $_GET['journey']);
			setSession('screen', 'booking-form-two');
		} elseif ($quote['type'] == 'remove_journey' && isset($_GET['journey']) && isset($_GET['type'])) {
			$_SESSION['journeys'][getSession('journey')]['journey_type'] = 'one_way';
			setSession('journey_type', 'one_way');
			setSession('journey', getSession('journey'));
			setSession('screen', 'confirm');

			if ($_GET['type'] == 'one_way') {
				$one_way = $_SESSION['journeys'][getSession('journey')]['one_way'];
				$two_way = $_SESSION['journeys'][getSession('journey')]['two_way'];
				$_SESSION['journeys'][getSession('journey')]['one_way'] = $two_way;
				$_SESSION['journeys'][getSession('journey')]['two_way'] = $one_way;
			}

			if ($_SESSION['journeys'][getSession('journey')]['journey_type'] == 'one_way') {
				unset($_SESSION['journeys'][getSession('journey')]);
			}
		}
		jsonOutput(true, 'New Journey');
	}

	function yearly_booking()
	{
		$this->data['yearly_bookings'] = $this->yearly_booking_model->with('driver')->with('passenger')->with('fleet')->get_all();
		// debug($this->data['yearly_bookings']);
		$this->data['main_content'] = 'admin/booking_info/yearly_booking/index';
		$this->data['sub_content'] = 'admin/booking_info/yearly_booking/_booking_info';
		$this->load->view(BACKEND, $this->data);
	}

	function add_update()
	{
		$yearly_booking_id = segment(4);
		$post = $this->input->post();
		if ($post) {
			$post['home_pickup_time'] = date('H:i:s', strtotime($post['home_pickup_time']));
			$post['school_pickup_time'] = date('H:i:s', strtotime($post['school_pickup_time']));
			// debug($yearly_booking_id);
			if ($yearly_booking_id == '') {
				$this->yearly_booking_model->insert($post);
				$this->session->set_flashdata('msg', "Data Saved.");
			} else {
				$this->yearly_booking_model->update($post, array('id' => $yearly_booking_id));
				$this->session->set_flashdata('msg', "Data Updated.");
			}
			redirect('admin/booking/yearly_booking');
		}

		$this->data['is_edit'] = FALSE;

		if ($yearly_booking_id != '') {
			$this->data['yearly_booking'] = $this->yearly_booking_model->with('driver')->with('passenger')->with('fleet')->get(['id' => $yearly_booking_id]);
			$this->data['is_edit'] = TRUE;
		}

		$this->data['passengers'] = $this->passenger_model->get_all(['is_active' => 1]);
		$this->data['drivers'] = $this->driver_model->get_all(['status' => 1]);
		$this->data['fleets'] = $this->fleet_model->get_all(['status' => 1]);

		$this->data['main_content'] = 'admin/booking_info/yearly_booking/index';
		$this->data['sub_content'] = 'admin/booking_info/yearly_booking/_form';
		// debug($this->data['is_edit']);
		$this->load->view(BACKEND, $this->data);
	}

	function view_yearly_booking()
	{
		$yearly_booking_id = segment(4);
		$this->data['booking'] = $this->yearly_booking_model->with('driver')->with('passenger')->with('fleet')->get(['id' => $yearly_booking_id]);
		$this->data['booking_logs'] = $this->yearly_time_schedule_model->where('yearly_booking_id', $yearly_booking_id)->get_all();
		// debug($this->data['booking_log']);

		$this->data['main_content'] = 'admin/booking_info/yearly_booking/index';
		$this->data['sub_content'] = 'admin/booking_info/yearly_booking/_show_booking';
		$this->load->view(BACKEND, $this->data);
	}
}
