<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 9:54 AM
 */

class Option extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!is_admin_menu_accessible(3)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin'));
        }
        $this->load->model('options_model');

    }

    function index()
    {
        $post = $this->input->post();
        if ($post) {
            if (!empty($_FILES['logo']['name'])) {
                $files_data = $this->common_library->upload_image('logo', FCPATH . implode(DIRECTORY_SEPARATOR, ['uploads', 'page']), 'logo');
                $post['logo']= $files_data['filename'];
            }

            $this->options_model->update($post, array('id' => 1));
            $this->session->set_flashdata('msg', 'Setting updated');
            redirect('admin/option');
        } else {
            $this->data['main_content'] = 'admin/option/index';
            $this->data['sub_content'] = 'admin/option/form';
            $this->data['options'] = $this->options_model->get(['id' => 1]);
            unset($this->data['options']->id, $this->data['options']->created_at, $this->data['options']->updated_at);
            $this->load->view(BACKEND, $this->data);
        }

    }

}