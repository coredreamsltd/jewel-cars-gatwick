<?php

class Packages extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!is_admin_menu_accessible(13)) {
            set_flash('msg', 'Unauthorized Access.');
            redirect(base_url('admin/login'));
        }
        $this->load->model('packages_model');
        $this->load->model('fleet_model');


    }

    public function index()
    {
        $this->data['fleets'] = $this->fleet_model->get_all();
        $this->data['packages'] = $this->packages_model->order_by('id', 'DESC')->get_all();
        $this->data['main_content'] = 'admin/package/index';
        $this->data['sub_content'] = 'admin/package/package';
        $this->load->view(BACKEND, $this->data);
    }

    public function addUpdate($id = null)
    {
        $post = $this->input->post();
        if (!$post) {
            set_flash('dmsg', 'WARNING! Invalid method call.');
            redirect('admin/packages');
        }

        if (!empty($_FILES['image']['name'])) {
            $package = $this->packages_model->get(array('id' => $id));
            if (!empty($package->image)) {
                $url = 'uploads/package/' . $package->image;
                if (file_exists($url))
                    unlink($url);
            }

            $files_data = $this->common_library->upload_image('image', FCPATH . implode(DIRECTORY_SEPARATOR, ['uploads', 'package']), 'pack_' . time());
            $post['image'] = $files_data['filename'];
        }

//        $post['data'] = json_encode($post['data']);

        if ($id) {
            $this->packages_model->update($post, ['id' => $id]);
        } else {
            $this->packages_model->insert($post);
        }
        set_flash('msg', 'Package saved.');
        redirect('admin/packages');

    }

    public function delete($id = null)
    {
        $package = $this->packages_model->get(array('id' => $id));
        if (!$package) {
            set_flash('dmsg', 'WARNING! package not found.');
            redirect('admin/packages');
        }

        $url = base_url('uploads/package/' . $package->image);
        if (file_exists($url))
            unlink($url);

        $this->packages_model->delete(['id' => $id]);
        set_flash('msg', 'Successfully! Package deleted');
        redirect('admin/packages');
    }

    public function ajaxGetPackage($id = null)
    {
        $package = $this->packages_model->get(array('id' => $id));
        echo json_encode(['status' => true, 'message' => 'success', 'data' => $package]);

    }
}