<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('driver_model');
        $this->load->model('fleet_model');
    }

    function index($offset = '')
    {

        $post = $this->input->post();

        if ($post) {

            switch ($post['report_for']) {

                case 'bookings':
                    $date_from = DateTime::createFromFormat('d/m/Y', $post['date_from'])->format('Y-m-d');
                    $date_to = DateTime::createFromFormat('d/m/Y', $post['date_to'])->format('Y-m-d');

                    $sql = "SELECT"
                        . " b.booking_ref_id, b.client_name, b.phone_cc, b.client_phone, b.client_email, b.pickup_address, b.dropoff_address, b.pickup_date, b.pickup_time, b.vehicle_name, b.total_fare, b.pay_method,b.created_at as booked_datetime,"
                        . " d.name as driver_name, d.email as driver_email, d.phone_cc as driver_phone_cc, d.mobile as driver_mobile"
                        . " FROM tbl_booking_infos as b"
                        . " LEFT JOIN driver as d ON b.driver_id = d.id"
                        . " WHERE (b.created_at BETWEEN '{$date_from}' AND '{$date_to}')"
                        . " AND b.is_cancelled = 0";
                    $bookings = $this->common_model->run_query($sql);

                    if (!$bookings) {
                        set_flash('msg_danger', 'No bookings done from ' . $post['date_from'] . ' to ' . $post['date_to']);
                        redirect($_SERVER['HTTP_REFERER']);
                        exit;
                    }

                    $this->data['reports'] = $bookings;
                    $report_session = array(
                        'title' => 'Bookings Report - ' . SITE_NAME,
                        'data' =>    $this->data['reports'],
                        'filename' => 'bookings-report.xlsx'
                    );
                    setSession('report_session', $report_session);

                    break;

                case 'job_sheet':

                    $date_from = DateTime::createFromFormat('d/m/Y', $post['date_from'])->format('Y-m-d');
                    $date_to = DateTime::createFromFormat('d/m/Y', $post['date_to'])->format('Y-m-d');

                    $sql = "SELECT"
                        . " d.name as driver_name, d.email as driver_email, d.phone_cc as driver_phone_cc, d.mobile as driver_mobile,"
                        . " b.booking_ref_id, b.client_name, b.phone_cc, b.client_phone, b.client_email, b.pickup_address, b.dropoff_address, b.pickup_date, b.pickup_time, b.vehicle_name, b.total_fare, b.pay_method,b.created_at as booked_datetime"
                        . " FROM driver as d"
                        . " JOIN tbl_booking_infos as b  ON b.driver_id = d.id"
                        . " WHERE (b.created_at BETWEEN '{$date_from}' AND '{$date_to}')"
                        . " AND b.is_cancelled = 0";

                    if ($post['driver_id']) {
                        $sql .= " AND d.driver_id = '{$post['driver_id']}'";
                    }

                    $bookings = $this->common_model->run_query($sql);

                    if (!$bookings) {

                        $msg = 'No job done from ' . $post['date_from'] . ' to ' . $post['date_to'];

                        if ($post['driver_id']) {
                            $msg .= ' by driver id: ' . $post['driver_id'];
                        }
                        set_flash('msg_danger', $msg);
                        redirect($_SERVER['HTTP_REFERER']);
                        exit;
                    }

                    $this->data['reports'] = $bookings;
                    $report_session = array(
                        'title' => 'Job Sheet Report - ' . SITE_NAME,
                        'data' =>    $this->data['reports'],
                        'filename' => 'job-sheet-report.xlsx'
                    );
                    setSession('report_session', $report_session);
                    break;

                case 'user_email':
                    $date_from = DateTime::createFromFormat('d/m/Y', $post['date_from'])->format('Y-m-d');
                    $date_to = DateTime::createFromFormat('d/m/Y', $post['date_to'])->format('Y-m-d');

                    if ($post['account_type'] == 'driver') {
                        $sql = "SELECT email as driver_email FROM driver WHERE created_at BETWEEN '$date_from' AND '$date_to'";
                    } else if ($post['account_type'] == 'unregistered') {
                        $sql = "SELECT client_email as unregistered_user_email FROM tbl_booking_infos WHERE passenger_id = 0 AND created_at BETWEEN '$date_from' AND '$date_to' ORDER BY created_at ASC GROUP BY client_email";
                    } else if ($post['account_type'] == 'registered_with_no_bookings') {
                        $sql = "SELECT email as registered_email_with_no_bookings FROM passenger p LEFT JOIN tbl_booking_infos b ON b.passenger_id = p.id WHERE b.id IS NULL AND p.created_at BETWEEN '$date_from' AND '$date_to'";
                    } else {
                        $sql = "SELECT email as {$post['account_type']}_email FROM passenger WHERE type = '{$post['account_type']}' AND created_at BETWEEN '$date_from' AND '$date_to'";
                    }

                    $emails = $this->common_model->run_query($sql);
                    if (!$emails) {
                        set_flash('msg_danger', 'No email found.');
                        redirect($_SERVER['HTTP_REFERER']);
                        exit;
                    }

                    $this->data['reports'] = $emails;
                    $report_session = array(
                        'title' => 'Emails Export - ' . SITE_NAME,
                        'data' => $this->data['reports'],
                        'filename' => 'emails-exported.xlsx'
                    );
                    setSession('report_session', $report_session);
                    break;

                default:
                    break;
            }
        }
        $this->data['main_content'] = 'admin/reports/index';
        $this->data['sub_content'] = 'admin/reports/_generate_report';

        $this->load->view(BACKEND, $this->data);
    }

    function export()
    {

        $report_session = getSession('report_session');
        unSetSession('report_session');

        if ($report_session) {
            $doc_title = $report_session['title'];
            $all_data = $report_session['data'];
            $data_first_row = $report_session['data'][0];
            $fileName = $report_session['filename'];

            $this->load->library('PHPExcel');
            $this->phpexcel->getProperties()
                ->setTitle($doc_title);
            $this->phpexcel->getDefaultStyle()->getAlignment()->setWrapText(true);

            $char = 'A';
            foreach ($data_first_row as $k => $v) {
                $this->phpexcel->getSheet(0)->setCellValue($char++ . '1', ucwords(str_replace('_', ' ', $k)));
            }

            $this->phpexcel->getSheet(0)->getStyle('A1:' . --$char . '1')->getFont()->setBold(true);

            $this->phpexcel->getSheet(0)->fromArray($all_data, '', 'A2');
            $writer = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
            $writer->save(FCPATH . 'uploads' . DIRECTORY_SEPARATOR . 'reports' . DIRECTORY_SEPARATOR . $fileName);
            die('Download your report: <a href="' . base_url('uploads/reports/' . $fileName) . '" target="_blank">DOWNLOAD</a>');
        } else {
            redirect($this->agent->referrer());
        }
    }

    function pdf()
    {
        $report_session = getSession('report_session');
        unSetSession('report_session');
        require FCPATH . 'vendor/autoload.php';
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setTitle('Dynamic PDF');

        $pdf->AddPage();
        if(!empty($report_session)) {
            $html = '<h1>Booking Reports</h1>';
            $html .= '<table border="1" style="font-size: 8px; width: 100%; overflow-x: auto;">
            <thead>
                <tr style="font-weight: bolder;">
                    <th>Booking ref_id</th>
                    <th>Client Name</th>
                    <th>Phone</th>
                    <th>Client Phone</th>
                    <th>Client Email</th>
                    <th>Pickup Address</th>
                    <th>Dropoff Address</th>
                    <th>Pickup Date</th>
                    <th>Pickup Time</th>
                    <th>Vechicle Name</th>
                    <th>Total Fare</th>
                    <th>Pay Method</th>
                    <th>Booked Date Time</th>
                </tr>
            </thead>
            <tbody>';
            foreach($report_session['data'] as $booking)
            {
                $html .= '<tr>
                    <td>'.$booking['booking_ref_id'].'</td>
                    <td>'.$booking['client_name'].'</td>
                    <td>'.$booking['phone_cc'].'</td>
                    <td>'.$booking['client_phone'].'</td>
                    <td>'.$booking['client_email'].'</td>
                    <td>'.$booking['pickup_address'].'</td>
                    <td>'.$booking['dropoff_address'].'</td>
                    <td>'.$booking['pickup_date'].'</td>
                    <td>'.$booking['pickup_time'].'</td>
                    <td>'.$booking['vehicle_name'].'</td>
                    <td>'.$booking['total_fare'].'</td>
                    <td>'.$booking['pay_method'].'</td>
                    <td>'.$booking['booked_datetime'].'</td>
                </tr>';
            }
            $html .= '</tbody></table>';
            // debug($html);
            $pdf->writeHTML($html);
            $pdf->Output('bookingReport.pdf','I');
        } else {
            echo "Some data has already been output, can't send PDF file";
        }
    }

    function driver()
    {

        if ($_POST) {
            $post = $this->input->post();

            $this->db->select('d.id, d.name, d.nickname, SUM(b.driver_fare) as driver_fare_sum, SUM(b.company_commission) as company_commission_sum, SUM(b.driver_fare-b.company_commission) as driver_earning_sum, SUM(b.final_fare-b.driver_fare) as company_earning_sum')
                ->from('tbl_drivers d')
                ->join('tbl_bookings b', 'b.driver_id = d.driver_id');

            $date_from = DateTime::createFromFormat('d/m/Y', $post['date_from'])->format('Y-m-d');
            $date_to = DateTime::createFromFormat('d/m/Y', $post['date_to'])->format('Y-m-d');

            $this->db->where("(STR_TO_DATE(b.pickup_date, '%d/%m/%Y') BETWEEN '$date_from' AND '$date_to')");
            $this->db->where("is_cancelled", 0);

            if ($post['driver_id']) {
                $this->db->where('b.driver_id', $post['driver_id']);
            }

            if ($post['fleet_id']) {
                $this->db->where('b.selected_fleet_id', $post['fleet_id']);
            }

            $this->db->group_by('d.id');

            $data['reports'] = $this->db->get()->result_array();

            if ($data['reports']) {
                $totals = array(
                    'driver_fare_sum' => 0,
                    'company_commission_sum' => 0,
                    'driver_earning_sum' => 0,
                    'company_earning_sum' => 0,
                    'cash_payment_sum' => 0,
                    'card_payment_sum' => 0,
                    'outstanding_amount' => 0
                );
                foreach ($data['reports'] as $index => $report) {

                    $data['reports'][$index]['cash_payment_sum'] = $this->driver_model->total_cash_payment_received($report['id'], $date_from, $date_to);
                    $data['reports'][$index]['card_payment_sum'] = $this->driver_model->total_job_journey_fare($report['id'], $date_from, $date_to) - $data['reports'][$index]['cash_payment_sum'];
                    $data['reports'][$index]['outstanding_amount'] = $report['driver_earning_sum'] - $this->driver_model->total_paid($report['id'], $date_from, $date_to);

                    $totals['driver_fare_sum'] += $report['driver_fare_sum'];
                    $totals['company_commission_sum'] += $report['company_commission_sum'];
                    $totals['driver_earning_sum'] += $report['driver_earning_sum'];
                    $totals['company_earning_sum'] += $report['company_earning_sum'];
                    $totals['cash_payment_sum'] += $data['reports'][$index]['cash_payment_sum'];
                    $totals['card_payment_sum'] += $data['reports'][$index]['card_payment_sum'];
                    $totals['outstanding_amount'] += $data['reports'][$index]['outstanding_amount'];
                }
                $data['totals'] = $totals;
            }

            $report_session = array(
                'title' => 'Driver Report - ' . SITE_NAME,
                'data' => $data['reports'],
                'filename' => 'driver-report.xlsx'
            );
            $this->session->set_userdata('report_session', $report_session);
        }

        $data['drivers'] = $this->driver_model->get_all();
        $data['fleets'] = $this->fleet_model->get_all();
        $data['main_content'] = 'admin/reports/index';
        $data['sub_content'] = 'admin/reports/_driver';

        $this->load->view(BACKEND, $data);
    }

    function accounts()
    {
        $post = $this->input->post();
        if ($post) {

            if ($post['account_type'] == 'driver') {
                $sql = "SELECT name,email,phone_cc,mobile,address FROM driver ORDER BY name DESC";
            } else {
                $sql = "SELECT full_name,email,phone_cc,phone_no,address,city,post_code FROM passenger WHERE `type` = '{$post['account_type']}' ORDER BY full_name DESC";
            }

            $accounts = $this->common_model->run_query($sql);


            if (!$accounts) {
                set_flash('msg_danger', 'No account found.');
                redirect($_SERVER['HTTP_REFERER']);
                exit;
            }

            $this->data['reports'] = $accounts;
            $report_session = array(
                'title' => ucfirst($post['account_type']) . ' Account Export - ' . SITE_NAME,
                'data' => $this->data['reports'],
                'filename' => $post['account_type'] . '-account-exported.xlsx'
            );
            setSession('report_session', $report_session);
        }

        $this->data['main_content'] = 'admin/reports/index';
        $this->data['sub_content'] = 'admin/reports/_accounts';

        $this->load->view(BACKEND, $this->data);
    }
}
