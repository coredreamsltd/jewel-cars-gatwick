<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faqs extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    function index($offset = '')
    {
        $this->data['faqs'] = $this->common_model->get_all('tbl_faqs', '', 'order ASC');
        $this->data['main_content'] = 'admin/faqs/index';
        $this->data['sub_content'] = 'admin/faqs/_faqs';
        $this->load->view(BACKEND, $this->data);
    }

    function add_update()
    {
        $faq_id = segment(4);
        if($_POST) {
            $post = $_POST;
            if($faq_id == '') {
                $this->common_model->insert('tbl_faqs', $post);
            } else {
                $this->common_model->update('tbl_faqs', $post, array('id' => $faq_id));
            }
            set_flash('msg', 'FAQ saved');
            redirect('admin/faqs');
        } else {
            $this->data['main_content']='admin/faqs/index';
            $this->data['sub_content']='admin/faqs/_form';

            if($faq_id != '') {
                $this->data['faq'] = $this->common_model->get_where('tbl_faqs', array('id' => $faq_id));
            }
            $this->load->view(BACKEND, $this->data);
        }
    }

    function delete($id = '')
    {
        $this->common_model->delete_data('tbl_faqs', array('id' => $id));
        $this->session->set_flashdata('msg', 'Deleted successfully');
        redirect('admin/faqs');
    }
}