<?php
require FCPATH . 'vendor/autoload.php';

class Quote extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->helper('form');
		$this->load->model('fare_breakdown_model');
		$this->load->model('admin_model');
		$this->load->model('booking_info_model');
		$this->load->model('additional_rate_model');
		$this->load->model('fleet_model');
		$this->load->model('invoice_model');
		$this->load->model('invoice_bulk_model');
		$this->load->model('fix_rate_by_model');
		$this->load->model('discount_coupon_model');
		$this->load->model('discount_coupon_list_model');
		$this->load->model('discount_account_based_model');
		$this->load->model('location_model');
		$this->load->model('rush_hour_model');
		$this->load->model('holidays_model');
		$this->load->helper('email_helper');
		$this->load->library('TwilioSms');
		$this->load->library('StripeApi');
		$this->load->model('passenger_model');
		$this->load->model('pages_model');
		$this->load->model('queue_job_model');
	}

	function ajaxGetQuote()
	{
		$quote = $this->input->post();
		if (!$this->input->is_ajax_request()) {
			jsonOutput(false, "No direct script access allowed.");
		}

		if (empty($quote['start']) || empty($quote['end']) || empty($quote['start_post_code']) || empty($quote['end_post_code'])) {
			jsonOutput(false, "Either pickup or destination address are not selected. Please select to continue.");
		}

		$journey_type = getSession('journey_type');


		for ($i = 0; $i < 3; ++$i) {
			$latLng = getLatLng($quote['start_post_code'] . ', UK');
			if (!empty($latLng['lat'])) {
				$startLatLng = $latLng;
				break;
			}
		}

		for ($i = 0; $i < 3; ++$i) {
			$latLng = getLatLng($quote['end_post_code'] . ', UK');
			if (!empty($latLng['lat'])) {
				$endLatLng = $latLng;
				break;
			}
		}


		$quote['start_lat'] = $startLatLng['lat'] ?? '';
		$quote['start_lng'] = $startLatLng['lng'] ?? '';
		$quote['end_lat']   = $endLatLng['lat'] ?? '';
		$quote['end_lng']   = $endLatLng['lng'] ?? '';

		if (!empty($quote['way_point_post_code'])) {
			foreach ($quote['way_point_post_code'] as $index => $way_point_post_code) {
				for ($i = 0; $i < 3; ++$i) {
					$latLng = getLatLng($way_point_post_code . ', UK');
					if (!empty($latLng['lat'])) {
						$wayPointLatLng = $latLng;
						break;
					}
				}

				$quote['way_point_lat'][$index] = $wayPointLatLng['lat'] ?? '';
				$quote['way_point_lng'][$index] = $wayPointLatLng['lng'] ?? '';
			}
			$wayPoint = $this->viaPointFormat($quote);
			$quote['way_point_lat_lng'] = $wayPoint;
		}

		$quote['is_start_airport'] = isAirport($quote) ? 1 : 0;
		$quote['is_end_airport'] = isEndAirport($quote) ? 1 : 0;
		$google_data['distance'] = 0;
		// for ($i = 1; $i <= 2; $i++) {
		//     if (array_key_exists('way_point_post_code', $quote) && count($quote['way_point_post_code']) > 0) {
		//         $start_post_code = $quote['start_post_code'];
		//         $google_response_data = 0.00;
		//         foreach ($quote['way_point_post_code'] as $way_point_post_code) {
		//             $google_response_data += calDistance(preg_replace('/\s+/', '', $start_post_code), preg_replace('/\s+/', '', $way_point_post_code))['distance'];
		//             $start_post_code = $way_point_post_code;
		//         }
		//         // debug($google_response_data);
		//         $google_response_data += calDistance(preg_replace('/\s+/', '', $start_post_code), preg_replace('/\s+/', '', $quote['end_post_code']))['distance'];
		//         // debug($google_response_data);
		//         break;
		//     } else {
		//         $google_response_data = calDistance(preg_replace('/\s+/', '', $quote['start_post_code']), preg_replace('/\s+/', '', $quote['end_post_code']));
		//         if (isset($google_response_data['distance'])) {
		//             $google_data = $google_response_data;
		//             break;
		//         }
		//     }
		// }

		$fleet_rows = $this->fleet_model->order_by('sort')->get_all(['status' => 1]);
		$fleets = $this->computeQuoteRate($google_data['distance'], $quote, $fleet_rows, $journey_type);
		if (!$quote || !$fleets) {
			jsonOutput(false, 'Currently we do not serve in this locations. Please try another locations.');
		}

		$data = ['fleets' => $fleets, 'quote' => $quote, 'google_data' => $google_data];
		$_SESSION['journeys'][$quote['journey']][$journey_type] = $data;

		if (empty($_SESSION['journeys'][$quote['journey']]['one_way']['booking_details'])) {
			$_SESSION['journeys'][$quote['journey']]['one_way']['booking_details'] = [
				'name' => '',
				'phone' => '',
				'email' => '',
				'is_baby_seat' => '',
				'is_infant_seat' => '',
				'is_child_seat' => '',
				'is_child_booster_seat' => '',
				'is_booster_seat' => '',
				'baby_age' => '',
				'baby_seats' => 1,
				'infant_seat' => 1,
				'child_seat' => 1,
				'child_booster_seat' => 1,
				'booster_seat' => 1,
				'flight_arrive_from' => '',
				'pickup_date_time' => '',
				'discount_code' => '',
			];
		}

		if ($journey_type == 'one_way') {
			$reverse_quote = $this->reverseQuote($quote);
			$fleet_rows = $this->fleet_model->order_by('sort')->get_all(['status' => 1]);
			$two_way_fleets = $this->computeQuoteRate($google_data['distance'], $reverse_quote, $fleet_rows, 'two_way');
			if (!$reverse_quote || !$two_way_fleets) {
				jsonOutput(false, 'Currently we do not serve in this locations. Please try another locations.');
			}
			$reverse_data = ['fleets' => $two_way_fleets, 'quote' => $reverse_quote, 'google_data' => $google_data];
			$_SESSION['journeys'][$quote['journey']]['two_way'] = $reverse_data;

			if (empty($_SESSION['journeys'][$quote['journey']]['two_way']['booking_details'])) {
				$_SESSION['journeys'][$quote['journey']]['two_way']['booking_details'] = [
					'name' => '',
					'phone' => '',
					'email' => '',
					'is_baby_seat' => '',
					'is_infant_seat' => '',
					'is_child_seat' => '',
					'is_child_booster_seat' => '',
					'is_booster_seat' => '',
					'baby_age' => '',
					'baby_seats' => 1,
					'infant_seat' => 1,
					'child_seat' => 1,
					'child_booster_seat' => 1,
					'booster_seat' => 1,
					'flight_arrive_from' => '',
					'pickup_date_time' => '',
					'discount_code' => '',
				];
			}
		}

		// debug($data['fleets']);
		setSession('screen', 'vehicle-selection');
		setSession('journey', $quote['journey']);
		//debug($quote,$_SESSION);
		jsonOutput(true, 'Fleet with rate', ['screen' => 'vehicle-selection', 'journey' => $quote['journey'], 'journeys' => $_SESSION['journeys']]);
	}
	private function reverseQuote($quote)
	{
		$data = [
			'start' => $quote['end'],
			'start_lat' => $quote['end_lat'],
			'start_lng' => $quote['end_lng'],
			'start_post_code' => $quote['end_post_code'],
			'start_town_or_city' => $quote['end_town_or_city'],
			'start_district' => $quote['end_district'],
			'start_country' => $quote['end_country'],
			'end' => $quote['start'],
			'end_lat' => $quote['start_lat'],
			'end_lng' => $quote['start_lng'],
			'end_post_code' => $quote['start_post_code'],
			'end_town_or_city' => $quote['start_town_or_city'],
			'end_district' => $quote['start_district'],
			'end_country' => $quote['start_country'],
			'journey' => $quote['journey'],
			'is_start_airport' => $quote['is_end_airport'],
			'is_end_airport' => $quote['is_start_airport'],
		];
		if (!empty($quote['stop_point'])) {
			$data['stop_point'] = array_values(array_reverse($quote['stop_point']));
			$data['way_point_lat'] = array_values(array_reverse($quote['way_point_lat']));
			$data['way_point_lng'] = array_values(array_reverse($quote['way_point_lng']));
			$data['way_point_post_code'] = array_values(array_reverse($quote['way_point_post_code']));
			$data['way_point_city'] = array_values(array_reverse($quote['way_point_city']));
			$data['way_point_district'] = array_values(array_reverse($quote['way_point_district']));
			$data['way_point_country'] = array_values(array_reverse($quote['way_point_country']));
		}
		return $data;
	}

	function editScreen()
	{
		$post = $this->input->post();

		if (!$this->input->is_ajax_request()) {
			jsonOutput(false, "No direct script access allowed.");
		}

		setSession('screen', $post['screen']);
		if (!empty($post['journey'])) {
			setSession('journey', $post['journey']);
		}
		if (!empty($post['journey_type'])) {
			setSession('journey_type', $post['journey_type']);
		}
		jsonOutput(true, 'screen updated',  ['screen' => $post['screen']]);
	}

	function ajaxBooking()
	{
		$post = $this->input->post();

		if (!$this->input->is_ajax_request()) {
			jsonOutput(false, "No direct script access allowed.");
		}

		$journey = getSession('journey');
		$journey_type = getSession('journey_type');
		if ($post) {
			$_SESSION['journeys'][$journey]['journey_type'] = $post['journey_type'];
		}

		$journeys = getSession('journeys');

		$quote = $journeys[$journey][$journey_type]['quote'];
		$vehicle_selection = $journeys[$journey][$journey_type]['fleets'];
		$google_data = $journeys[$journey][$journey_type]['google_data'];
		// $vehicle_info = $journeys[$journey]['vehicle_info'];

		if (empty($quote) || empty($vehicle_selection) || empty($google_data)) {
			jsonOutput(false, "Session expired. Please try again.");
		}

		$selected_fleet = (object) null;
		foreach ($vehicle_selection as $key => $fleet) {
			if ($fleet->id == $post['fleet_id']) {
				$selected_fleet = $fleet;
				break;
			}
		}

		if (($post['journey_type']) == 'one_way') {
			$_SESSION['journeys'][$journey]['total'] = $selected_fleet->fare;
		} else {
			$_SESSION['journeys'][$journey]['total'] = $selected_fleet->fare + $_SESSION['journeys'][$journey]['two_way']['fleets'][$key]->fare;
		}

		if ($journey_type == 'one_way' && empty($_SESSION['journeys'][$quote['journey']]['two_way']['selected_fleet'])) {

			foreach ($journeys[$journey]['one_way']['fleets'] as $fleet) {
				if ($fleet->id == $post['fleet_id']) {
					$selected_fleet = $fleet;
					break;
				}
			}
			$_SESSION['journeys'][$journey]['one_way']['selected_fleet'] = $selected_fleet;

			foreach ($journeys[$journey]['two_way']['fleets'] as $fleet) {
				if ($fleet->id == $post['fleet_id']) {
					$selected_fleet = $fleet;
					break;
				}
			}
			$_SESSION['journeys'][$journey]['two_way']['selected_fleet'] = $selected_fleet;
		} else {
			$_SESSION['journeys'][$journey][$journey_type]['selected_fleet'] = $selected_fleet;
		}

		$meet_and_greet = $this->pages_model->where('slug', 'meet-greet-service')->get();
		// debug($meet_and_greet);
		setSession('meet_and_greet', $meet_and_greet);
		setSession('screen', 'booking-form');
		jsonOutput(true, 'booking form', ['screen' => 'booking-form', 'journey' => $quote['journey'], 'journeys' => $_SESSION['journeys'], 'meet_and_greet' => $meet_and_greet]);
	}

	function oneWayBookingInfo()
	{
		if (!$this->input->is_ajax_request()) {
			jsonOutput(false, "No direct script access allowed.");
		}
		$post = $this->input->post();
		$journey = getSession('journey');
		$post['pickup_date'] = date('d/m/Y', strtotime($post['pickup_date_time']));
		$post['pickup_time'] = date('h:i A', strtotime($post['pickup_date_time']));
		$_SESSION['journeys'][$journey]['one_way']['booking_details'] = $post;

		$selected_fleet = $_SESSION['journeys'][$journey]['one_way']['selected_fleet'];
		$additional_rates = $this->processAdditionalRates($selected_fleet, $post);
		$_SESSION['journeys'][$journey]['one_way']['selected_fleet']->charge_details['increment']['rush_hour_charge'] = $additional_rates['rush_hour_charge'] ?? 0;
		$_SESSION['journeys'][$journey]['one_way']['selected_fleet']->charge_details['increment']['holiday_charge'] = $additional_rates['holiday_charge'] ?? 0;
		$_SESSION['journeys'][$journey]['one_way']['selected_fleet']->charge_details['increment']['baby_seater_charge'] = $additional_rates['baby_seater_charge'];
		$_SESSION['journeys'][$journey]['one_way']['selected_fleet']->charge_details['decrement']['coupons_discount'] = !empty($additional_rates['coupons_discount']) ? $additional_rates['coupons_discount'] : '';

		if ($_SESSION['journeys'][$journey]['journey_type'] == 'one_way') {
			setSession('screen', 'confirm');
		} else {
			setSession('screen', 'booking-form-two');
		}
		$meet_and_greet_charge = 0;
		if (!empty($post['meet_and_greet']) && $post['meet_and_greet']) {
			$meet_and_greet_charge = $this->additional_rate_model->get(['fleet_id' => $selected_fleet->id])->meet_and_greet ?? 0;
			$_SESSION['journeys'][$journey]['one_way']['selected_fleet']->charge_details['increment']['meet_and_greet_charge'] = $meet_and_greet_charge;
		}

		$_SESSION['journeys'][$journey]['one_way']['selected_fleet']->fare = ceil($selected_fleet->charge_details['base_fare'] + array_sum($selected_fleet->charge_details['increment']) - array_sum($selected_fleet->charge_details['decrement']));
		$getScreen = getSession('screen');
		jsonOutput(true, 'booking form details set', ['screen' => $getScreen, 'journey' => $_SESSION['journey'], 'journeys' => $_SESSION['journeys']]);
	}

	function twoWayBookingInfo()
	{
		if (!$this->input->is_ajax_request()) {
			jsonOutput(false, "No direct script access allowed.");
		}
		$post = $this->input->post();
		$journey = getSession('journey');
		$post['pickup_date'] = date('d/m/Y', strtotime($post['pickup_date_time']));
		$post['pickup_time'] = date('h:i A', strtotime($post['pickup_date_time']));
		$_SESSION['journeys'][$journey]['two_way']['booking_details'] = $post;

		$selected_fleet = $_SESSION['journeys'][$journey]['two_way']['selected_fleet'];
		$additional_rates = $this->processAdditionalRates($selected_fleet, $post);
		$_SESSION['journeys'][$journey]['two_way']['selected_fleet']->charge_details['increment']['rush_hour_charge'] = $additional_rates['rush_hour_charge'] ?? 0;
		$_SESSION['journeys'][$journey]['two_way']['selected_fleet']->charge_details['increment']['holiday_charge'] = $additional_rates['holiday_charge'] ?? 0;
		$_SESSION['journeys'][$journey]['two_way']['selected_fleet']->charge_details['increment']['baby_seater_charge'] = $additional_rates['baby_seater_charge'];
		$_SESSION['journeys'][$journey]['two_way']['selected_fleet']->charge_details['decrement']['coupons_discount'] = !empty($additional_rates['coupons_discount']) ? $additional_rates['coupons_discount'] : '';
		$meet_and_greet_charge = 0;
		if (!empty($post['meet_and_greet']) && $post['meet_and_greet']) {
			$meet_and_greet_charge = $this->additional_rate_model->get(['fleet_id' => $selected_fleet->id])->meet_and_greet ?? 0;
			$_SESSION['journeys'][$journey]['two_way']['selected_fleet']->charge_details['increment']['meet_and_greet_charge'] = $meet_and_greet_charge;
		}

		$_SESSION['journeys'][$journey]['two_way']['selected_fleet']->fare = ceil($selected_fleet->charge_details['base_fare'] + array_sum($selected_fleet->charge_details['increment']) - array_sum($selected_fleet->charge_details['decrement']));
		setSession('screen', 'confirm');
		jsonOutput(true, 'booking form details set', ['screen' => 'confirm', 'journey' => $journey, 'journeys' => $_SESSION['journeys']]);
	}
	function processDiscount()
	{
		if (!$this->input->is_ajax_request()) {
			jsonOutput(false, "No direct script access allowed.");
		}
		$post = $this->input->post();
		if (!empty($post['discount_code'])) {
			$discountData = $this->discount_coupon_model->discountProcess($post['discount_code']);
			if ($discountData['status']) {
				if ($discountData['discount_type'] == 'by_percent') {
					$discountData['discount'] = round($post['grand_total'] * $discountData['discount'] * 0.01, 2);
				}
				$_SESSION['discount'] = ['code' => $post['discount_code'], 'amount' => $discountData['discount']];
			} else {
				$_SESSION['discount'] = ['code' => '', 'amount' => 0];
			}
		}
		$response = ['discount_amount' => $discountData['discount'], 'grand_total_after_discount' => round($post['grand_total'] - $discountData['discount'], 2)];
		jsonOutput(!empty($discountData['status']) ? $discountData['status'] : false, !empty($discountData['message']) ? $discountData['message'] : '', $response);
	}
	function removeDiscount()
	{
		if (!$this->input->is_ajax_request()) {
			jsonOutput(false, "No direct script access allowed.");
		}
		$_SESSION['discount'] = ['code' => '', 'amount' => 0];
		jsonOutput(true, 'Removed Discount');
	}

	function confirm()
	{
		$post = $this->input->post();
		$journey = getSession('journey');
		$journey_type = getSession('journey_type');
		if ($post) {
			$post['pay_method'] = 'paypal';
			$post['discount_coupon_no'] = '';
			$post['pickup_date'] = date('d/m/Y', strtotime($post['pickup_date_time']));
			$post['pickup_time'] = date('h:i A', strtotime($post['pickup_date_time']));

			if (!empty($post['return_date_time'])) {
				$post['return_date'] = date('d/m/Y', strtotime($post['return_date_time']));
				$post['return_time'] = date('h:i A', strtotime($post['return_date_time']));
			}

			$_SESSION['journeys'][$journey]['booking_details'] = $post;
		}

		$journeys = getSession('journeys');

		$quote = $journeys[$journey]['quote'];
		$vehicle_selection = $journeys[$journey]['fleets'];
		$google_data = $journeys[$journey]['google_data'];
		$vehicle_info = $journeys[$journey]['vehicle_info'];
		$selected_fleet = $journeys[$journey]['selected_fleet'];
		$booking_details = $journeys[$journey]['booking_details'];

		if (empty($quote) || empty($selected_fleet) || empty($google_data) || empty($vehicle_info) || empty($booking_details)) {
			set_flash('dmsg', "Session expired. Please try again.");
			redirect(site_url());
			// jsonOutput(false, "Session expired. Please try again.");
		}
		$grand_total_charge = $this->vehicleSpecificAdditionalRate($quote, $selected_fleet, $booking_details);
		$_SESSION['journeys'][$journey]['grand_total_charge'] = $grand_total_charge;
		setSession('screen', 'confirm');
		redirect(site_url());
		// jsonOutput(true, 'booking form');
	}

	function formatBooking($booking, $user_agent_data, $type, $booking_ref_id, $pay_method = 'cash')
	{
		// Discounts
		if (!empty($booking[$type]['booking_details']['discount_code'])) {
			$this->common_model->update('tbl_discount_coupon_list', array('is_redeemed' => 1), array('coupon_no' => $booking[$type]['booking_details']['discount_code']));
			$coupon = $this->common_model->get_where('tbl_discount_coupon_list', array('coupon_no' => $booking[$type]['booking_details']['discount_code']));
			$this->db->query("UPDATE tbl_discount_coupons SET no_of_coupons_redeemed = no_of_coupons_redeemed+1 WHERE id = {$coupon[0]['discount_coupon_id']}");
		}
		return [
			'booking_ref_id' => $booking_ref_id,
			'passenger_id' => $this->data['passenger'] ? $this->data['passenger']->id : '',
			'distance' => $booking[$type]['google_data']['distance'],
			'pickup_address' => $booking[$type]['quote']['start'],
			'start_lat' => !empty($booking[$type]['quote']['start_lat']) ? $booking[$type]['quote']['start_lat'] : '',
			'start_lng' => !empty($booking[$type]['quote']['start_lng']) ? $booking[$type]['quote']['start_lng'] : '',
			'start_post_code' => $booking[$type]['quote']['start_post_code'],
			'start_town_or_city' => $booking[$type]['quote']['start_town_or_city'],
			'start_district' => $booking[$type]['quote']['start_district'],
			'start_country' => $booking[$type]['quote']['start_country'],

			'dropoff_address' => $booking[$type]['quote']['end'],
			'end_lat' => !empty($booking[$type]['quote']['end_lat']) ? $booking[$type]['quote']['end_lat'] : '',
			'end_lng' => !empty($booking[$type]['quote']['end_lng']) ? $booking[$type]['quote']['end_lng'] : '',
			'end_post_code' => $booking[$type]['quote']['end_post_code'],
			'end_town_or_city' => $booking[$type]['quote']['end_town_or_city'],
			'end_district' => $booking[$type]['quote']['end_district'],
			'end_country' => $booking[$type]['quote']['end_country'],

			'journey_type' => 'one_way',

			'pickup_date' => date('Y-m-d', strtotime($booking[$type]['booking_details']['pickup_date_time'])),
			'pickup_time' => date('H:i:s', strtotime($booking[$type]['booking_details']['pickup_date_time'])),
			'via_point' => NULL,
			'discount_fare' => !empty($booking[$type]['selected_fleet']['charge_details']['decrement']['coupons_discount']) ?: 0,
			'total_fare' => $booking[$type]['selected_fleet']['fare'] - (!empty($booking[$type]['selected_fleet']['charge_details']['decrement']['coupons_discount']) ? $booking[$type]['selected_fleet']['charge_details']['decrement']['coupons_discount'] : 0),
			'vehicle_name' => $booking[$type]['selected_fleet']['title'],
			'selected_fleet_id' => $booking[$type]['selected_fleet']['id'],
			'selected_fleet' => json_encode($booking[$type]['selected_fleet']),

			'meet_and_greet' => isset($booking[$type]['booking_details']['meet_and_greet']) ? $booking[$type]['booking_details']['meet_and_greet'] : '',
			'flight_number' => isset($booking[$type]['booking_details']['flight_number']) ? $booking[$type]['booking_details']['flight_number'] : '',
			'flight_arrive_from' => isset($booking[$type]['booking_details']['flight_arrive_from']) ? $booking[$type]['booking_details']['flight_arrive_from'] : '',

			'client_name' => $booking[$type]['booking_details']['name'],
			'client_email' => $this->data['passenger'] ? $this->data['passenger']->email : '',
			// 'client_email' => $booking[$type]['booking_details']['email'],
			'phone_cc' => $booking[$type]['booking_details']['phone_cc']??'',
			'client_phone' => $booking[$type]['booking_details']['phone'],

			'client_passanger_no' => $booking[$type]['booking_details']['passenger']??1,
			'client_luggage' => $booking[$type]['booking_details']['suitcase']??0,
			'client_hand_luggage' => $booking[$type]['booking_details']['luggage']??0,
			'client_baby_no' => !empty($booking[$type]['booking_details']['is_baby_seat']) ? 1 : 0,
			'baby_age' => !empty($booking[$type]['booking_details']['baby_age']) ? $booking[$type]['booking_details']['baby_age'] : '',

			'infant_seat' => !empty($booking[$type]['booking_details']['infant_seat']) ? $booking[$type]['booking_details']['infant_seat'] : 0,
			'child_seat' => !empty($booking[$type]['booking_details']['child_seat']) ? $booking[$type]['booking_details']['child_seat'] : 0,
			'child_booster_seat' => !empty($booking[$type]['booking_details']['child_booster_seat']) ? $booking[$type]['booking_details']['child_booster_seat'] : 0,
			'booster_seat' => !empty($booking[$type]['booking_details']['booster_seat']) ? $booking[$type]['booking_details']['booster_seat'] : 0,

			'discount_coupon_no' => !empty($booking[$type]['booking_details']['discount_code']) ? $booking[$type]['booking_details']['discount_code'] : '',
			'pay_method' => $pay_method,
			'user_agent_data' => json_encode($user_agent_data),
			'created_at' => date('Y-m-d H:i:s'),
			'booking_status' => 'confirmed',
		];
	}

	function finish()
	{
		$post = $this->input->post();
		if (array_key_exists('passenger', $post)) {
			$this->data['passenger'] = $this->passenger_model->where(['id' => $post['passenger']])->get();
			// debug($this->data['passenger']);
			// $this->data['passenger'] = $post['passenger'];
		}
		// debug($this->data['passenger']);
		if (!$this->input->is_ajax_request()) {
			jsonOutput(false, "No direct script access allowed.");
		}
		if (empty($this->data['passenger'])) {
			setSession('screen', 'account');
			jsonOutput(true, 'account page.', ['redirect_url' => site_url('#booking')]);
		}
		if (empty($post)) {
			jsonOutput(false, "Session expired.");
		}
		$user_agent_data = array(
			'Browser' => $this->agent->browser(),
			'Version' => $this->agent->version(),
			'OS' => $this->agent->platform(),
			'Mobile Device' => $this->agent->mobile(),
			'IP Address' => $this->input->ip_address()
		);
		$booking_db = [];
		$booking_ids = [];

		foreach ($post['booking_data'] as $index => $booking) {
			$booking_ref_id = uniqueid('', 7, 'tbl_booking_infos', 'booking_ref_id');
			$booking_db[] =  $this->formatBooking($booking, $user_agent_data, 'one_way', $booking_ref_id, $post['pay_method']);
			$booking_ids[] = $booking_ref_id;
			if ($booking['journey_type'] == 'two_way') {
				$booking_ref_id = uniqueid('', 7, 'tbl_booking_infos', 'booking_ref_id');
				$booking_db[] =  $this->formatBooking($booking, $user_agent_data, 'two_way', $booking_ref_id, $post['pay_method']);
				$booking_ids[] = $booking_ref_id;
			}
		}
		// Set User Agent Details
		$booking_inserted = $this->booking_info_model->insert_batch($booking_db);
		

		if (!$booking_inserted) {
			jsonOutput(false, "Sorry for the inconvenience. Error occur while generating booking. Please try again.");
		}
		foreach ($booking_ids as $booking_id) {
			$booking = $this->booking_info_model->get(['booking_ref_id' => $booking_id]);
			$this->sendBookingReceivedEmail((array) $booking);
		}

		// $this->sendBookingReceivedNotificationEmail((array) $booking);
		$redirect_url = site_url('thank-you');
		if ($post['pay_method'] != 'cash') {
			$invoice_id = uniqueid('INV', 5, 'tbl_invoice', 'invoice_id');
			$invoice_amount = $post['grand_total'];
			$invoice_data['mail_subject'] = 'Payment Confirmation for Invoice Id: ' . $invoice_id;
			$invoice_data['mail_message'] = 'We have received your booking. For the payment please use the below link';
			$invoice_data['payment_url'] = site_url('quote/payment?invoice_id=' . $invoice_id);

			$invoice_mail_template = $this->invoice_model->create_invoice_bulk($booking_ids, $booking_db[0], $invoice_amount, 'card', false, $invoice_id, $invoice_data);
			$redirect_url = site_url('quote/payment?invoice_id=' . $invoice_id);
			// email_help([$this->data['passenger']->email], $invoice_data['mail_subject'], $invoice_mail_template, [SITE_EMAIL => SITE_NAME]);
		}
		// $this->session->set_flashdata('msg', 'Booking has been done successfully. Your booking ref number is ' . $booking_ref_id . '. No booking is confirmed without payment. Please do payment.');
		$this->session->unset_userdata(['journeys', 'journey', 'journey_type', 'screen']);
		jsonOutput(true, "Booking done.", ['redirect_url' => $redirect_url, 'is_finish' => true, 'screen' => 'payment']);
	}

	function payment()
	{
		$invoice_id = $this->input->get('invoice_id');
		$invoice = $this->invoice_model->get(['invoice_id' => $invoice_id]);
		$this->data['invoice'] = $invoice;
		$this->data['main_content'] = 'frontend/quote/_payment';
		$bookingIDs = [$invoice->booking_id];
		if (empty($invoice->booking_id)) {
			$bulk_invoices = $this->invoice_bulk_model->get_all(['invoice_id' => $invoice_id]);
			$bookingIDs = array_column($bulk_invoices, 'booking_id');
		}

		$this->data['bookings']='';
		// $this->data['bookings'] = $this->db->from('tbl_booking_infos')->where_in('booking_ref_id', $bookingIDs)->get()->result();
		// set_flash('msg', "Booking has been done successfully. Your booking ref number is " . implode(', ', $bookingIDs) . ". No booking is confirmed without payment. Please do payment.");
		if (empty($invoice)) {
			set_flash('dmsg', 'Invalid Invoice Id: ' . $invoice_id);
		} elseif ($invoice->is_cancelled) {
			set_flash('dmsg', 'Invoice id:' . $invoice_id . ' has been cancelled by administrator.');
		} elseif ($invoice->payment_status) {
			set_flash('msg', 'Payment done for invoice id:' . $invoice_id);
		}

		$this->load->view(FRONTEND, $this->data);
	}

	function payment_finish()
	{
		$post = $this->input->post();
		if (empty($post)) {
			set_flash('dmsg', 'WARNIGN! Invalid method call.');
			redirect(site_url('thank-you?status=failed'));
		}
		$invoice_id = $this->input->get('invoice_id');
		$invoice = $this->invoice_model->get(['invoice_id' => $invoice_id]);
		if ($invoice) {
			if (!$invoice->payment_status) {
				$card_data = [
					'card_number' => $post['card_number'],
					'card_expiry_month' => $post['exp_month'],
					'card_expiry_year' => $post['exp_year'],
					'card_verification_number' => $post['card_cvc'],
					'card_holder_name' => $post['card_holder_name'],
				];

				$stripe_card_response = $this->stripeapi->getCardToken($card_data);

				if ($stripe_card_response['status'] == 1) {
					$charge_data = ['charge_amount' => $invoice->invoice_amount];
					// $charge_data = ['charge_amount' => 0.01];
					$stripe_charge_response = $this->stripeapi->createCharge($stripe_card_response['data']->id, $charge_data);

					if ($stripe_charge_response['status'] == 0) {
						$error[] = $stripe_charge_response['msg'];
					}
				} else {
					$error[] = $stripe_card_response['msg'];
				}
			} else {
				die('Payment done for invoice id:' . $invoice_id);
			}
		} else {
			die('Invalid Invoice Id: ' . $invoice_id);
		}

		if (!empty($error)) {
			set_flash('dmsg', implode('<br>', $error));
			redirect(site_url('thank-you?status=failed'));
		} else {

			$this->invoice_model->update(array('payment_status' => 1, 'date_of_payment' => date('Y-m-d H:i:s')), array('invoice_id' => $invoice_id));
			if (empty($invoice->booking_id)) {
				$bulk_invoices = $this->invoice_bulk_model->get_all(['invoice_id' => $invoice_id]);
				if (!empty($bulk_invoices)) {
					foreach ($bulk_invoices as $bulk_invoice) {
						$this->booking_info_model->update(['is_paid' => 1], ['booking_ref_id' => $bulk_invoice->booking_id]);
					}
				}
			} else {
				$this->booking_info_model->update(['is_paid' => 1], ['booking_ref_id' => $invoice->booking_id]);
			}
			$this->booking_info_model->send_auto_receipt_email($invoice->invoice_id);
			set_flash('msg', "Payment successful(Invoice ID: " . $invoice_id . "). Thank you for booking. Your booking reference number is " . $bulk_invoice->booking_id);
			redirect(site_url('thank-you?status=success'));
		}
	}

	private function computeQuoteRate($distance, $quote, $fleets, $journey_type = 'one_way')
	{
		$routeFare = $this->calculate_route_fare($quote, $journey_type, $fleets);

		foreach ($fleets as $key => $fleet) {
			$additional_rate = $this->additional_rate_model->get(['fleet_id' => $fleet->id]);
			$fleet->charge_details = [];
			$fleet->fare = $base_fare = $routeFare[$fleet->id] ?? 0;

			if ($base_fare <= 0) {
				$googleData = calDistance(
					$quote['start_lat'],
					$quote['start_lng'],
					$quote['end_lat'],
					$quote['end_lng'],
					$quote['way_point_lat_lng'] ?? null
				);

				$base_fare = $this->breakDownCalculation($googleData['distance'], $fleet);
			}
			$fare = null;
			if (!empty($base_fare))
				$fare = $this->additionalRateCalculationForFare($base_fare, $fleet, $quote, $journey_type);

			$fleet->fare = $fare['rate'] ?? 0;
			$fleet->charge_details = $fare['charge_details'] ?? [];
			$fleet->charge_details['base_fare'] = $base_fare;

			if (empty($fleet->fare) || $fleet->fare <= 0) {
				continue;
			}

			$additional_via_point = (!empty($quote['stop_point']) ? count($quote['stop_point']) * $additional_rate->via_point : 0);
			$fleet->charge_details['increment']['additional_via_point'] = $additional_via_point;
			$fleet->fare += $additional_via_point;
			// $fleet->round_trip_fare += $additional_via_point;

			if ($additional_rate->raise_by_type == 'percentage') {
				$raise_by_one_amount = round($base_fare * 0.01 * $additional_rate->raise_by, 2);
			} else {
				$raise_by_one_amount = round($additional_rate->raise_by, 2);
			}
			$fleet->charge_details['increment']['raise_by_amount'] = round($raise_by_one_amount, 2);
			$fleet->fare += $raise_by_one_amount;
			$fleet->fare = ceil($fleet->fare);
		}
		return array_values($fleets);
	}

	private function calculate_route_fare1($quote, $journey_type)
	{
		$route_rate = ['base_price' => 0, 'fleet_rates' => []];

		if ($journey_type == 'one_way') {
			$location_1 = $this->db->get_where('tbl_locations', ['postcode' => $quote['start_post_code']])->row();
			if (empty($location_1)) {
				$location_1 = $this->db->get_where('tbl_locations', ['postcode' => explode(' ', $quote['start_post_code'])[0]])->row();
			}

			$location_2 = $this->db->get_where('tbl_locations', ['postcode' => $quote['end_post_code']])->row();

			$via_location = [];
			if (array_key_exists('way_point_post_code', $quote) && count($quote['way_point_post_code']) > 0) {
				foreach ($quote['way_point_post_code'] as $post_code) {
					$via_location[] = $this->db->get_where('tbl_locations', ['postcode' => $post_code])->row();
				}
			}

			if (empty($location_2)) {
				$location_2 = $this->db->get_where('tbl_locations', ['postcode' => explode(' ', $quote['end_post_code'])[0]])->row();
			}

			if (!empty($via_location) && !empty($location_1->id) && !empty($location_2->id)) {
				$new_location_1 = $location_1;
				$rates = [];
				foreach ($via_location as $index => $location_3) {
					$rates[] = $this->db->like('from_id', $new_location_1->id)->like('to_id', $location_3->id)->get('rate')->row();
					// debug($this->db->last_query());
					if (empty($rates)) {
						die('Rate not found for this location. Please try selecting different location.');
					}
					$new_location_1 = $location_3;
					$route_rate['base_price'] += round($rates[$index]->base_price ?? 0, 2);
				}
				$rates[] = $this->db->like('from_id', $new_location_1->id)->like('to_id', $location_2->id)->get('rate')->row();
				$count_rates = count($rates);
				foreach ($rates as $key => $rate) {
					if (empty($rate)) {
						continue;
					}
					// $route_rate['base_price'] += round($rate->base_price, 2);
					$rate->fleet_rates = json_decode($rate->fleet_rates, true);
				}
				$initial_fleet_rates = $rates[0]->fleet_rates;

				foreach ($initial_fleet_rates as $key => $initial_fleet_rate) {
					$temp_value = 0;
					for ($i = 0; $i < $count_rates; ++$i) {
						$temp_value += $rates[$i]->fleet_rates[$key] ?? 0;
						$route_rate['fleet_rates'][$key] = $temp_value;
					}
				}
				$route_rate['base_price'] += round(end($rates)->base_price, 2);

				// For two_way
				$_SESSION['route_rate_temp'] = $route_rate;
			} else if (!empty($location_1->id) && !empty($location_2->id)) {
				$rate = $this->db->like('from_id', $location_1->id)->like('to_id', $location_2->id)->get('rate')->row();
				if (!empty($rate->base_price)) {
					$route_rate['base_price'] = round($rate->base_price, 2);
					$route_rate['fleet_rates'] = json_decode($rate->fleet_rates);
				}
			}
		}

		if ($route_rate['base_price'] == 0 && $journey_type == 'two_way' && array_key_exists('way_point_post_code', $quote)) {
			$route_rate = $this->session->route_rate_temp;
		} else if ($route_rate['base_price'] == 0 && $journey_type == 'two_way') {
			$location_1 = $this->db->get_where('tbl_locations', ['postcode' => $quote['end_post_code']])->row();
			if (empty($location_1)) {
				$location_1 = $this->db->get_where('tbl_locations', ['postcode' => explode(' ', $quote['end_post_code'])[0]])->row();
			}

			$location_2 = $this->db->get_where('tbl_locations', ['postcode' => $quote['start_post_code']])->row();
			if (empty($location_2)) {
				$location_2 = $this->db->get_where('tbl_locations', ['postcode' => explode(' ', $quote['start_post_code'])[0]])->row();
			}

			if (!empty($location_1->id) && !empty($location_2->id)) {
				$rate = $this->db->like('from_id', '"' . $location_1->id . '"')->like('to_id', '"' . $location_2->id . '"')->get('rate')->row();
				if (!empty($rate->base_price) && (!empty($rate->direction) && $rate->direction == 'two_way')) {
					$route_rate['base_price'] = round($rate->base_price, 2);
					$route_rate['fleet_rates'] = json_decode($rate->fleet_rates);
				}
			}
		}
		// debug($route_rate);
		return $route_rate;
	}

	public function calculate_route_fare($quote, $journey_type, $fleets)
	{
		$base_price = [];
		$locationRows = $this->getLocations($quote);

		$length = count($locationRows);
		if ($length > 0) {
			$rates = [];
			for ($i = 0; $i < $length - 1; $i++) {
				if (!empty($locationRows[$i]->id) && !empty($locationRows[$i + 1]->id)) {
					$row = $this->db->like('from_id', $locationRows[$i]->id)->like('to_id', $locationRows[$i + 1]->id)->get('rate')->row();
					if (!$row) {
						$row = $this->db->like('from_id', $locationRows[$i + 1]->id)->like('to_id', $locationRows[$i]->id)->where('direction', 'two_way')->get('rate')->row();
					}
					$rates[] = $row;
				} else {
					$rates[] = null;
				}
			}

			foreach ($rates as $rate) {
				if (empty($rate)) {
					$base_price = [];
					break;
				}

				foreach ($fleets as $fleet) {
					$fleets_rates = json_decode($rate->fleet_rates, true);
					$base_price[$fleet->id] =  ($base_price[$fleet->id] ?? 0) + $rate->base_price * ($fleets_rates[$fleet->id] ?? 0);
				}
			}
		}

		return $base_price;
	}

	public function getLocations($quote)
	{
		$locationPostcodes = $this->getLocationPostcode($quote);
		$locationRows = [];

		foreach ($locationPostcodes as $post_code) {
			$location_row = $this->db->get_where('tbl_locations', ['postcode' => $post_code])->row();
			if (empty($location_row)) {
				$location_row = $this->db->get_where('tbl_locations', ['postcode' => explode(' ', $post_code)[0]])->row();
			}
			$locationRows[] = $location_row;
		}
		return $locationRows;
	}

	public function getLocationPostcode($quote)
	{
		$locationPostcodes = [];
		$locationPostcodes[] = $quote['start_post_code'];
		if (!empty($quote['way_point_post_code'])) {
			foreach ($quote['way_point_post_code'] as $way_point_post_code) {
				$locationPostcodes[] = $way_point_post_code;
			}
		}
		$locationPostcodes[] = $quote['end_post_code'];
		return $locationPostcodes;
	}

	private function checkRushHour($fleet, $quote)
	{

		$check_time = $this->rush_hour_model->get_all(['fleet_id' => $fleet->id, 'status' => 1]);
		if (!empty($check_time)) {
			foreach ($check_time as $check) {
				if ($this->IsBetween($check->start_time, $check->end_time, $quote['time'])) {
					return round($check->charge_type == 'amount' ? $check->charge : $check->charge * $fleet->fare * 0.01, 2);
					break;
				}
			}
		}
		return 0;
	}

	private function checkHolidays($fleet, $quote)
	{

		$allHolidays = $this->holidays_model->get_all(['fleet_id' => $fleet->id, 'is_active' => 1]);
		$userDateTime = DateTime::createFromFormat('d/m/Y h:i A', $quote['date'] . ' ' . $quote['time']);

		if (!empty($allHolidays)) {
			foreach ($allHolidays as $holiday) {
				$ourStartingDate = DateTime::createFromFormat('Y-m-d H:i:s', $holiday->starting_date . ' ' . $holiday->starting_time);
				$ourEndingDate = DateTime::createFromFormat('Y-m-d H:i:s', $holiday->ending_date . ' ' . $holiday->ending_time);
				if ($ourStartingDate <= $userDateTime && $ourEndingDate >= $userDateTime) {
					$data = [
						'charge' => $holiday->charge,
						'type' => $holiday->charge_type
					];
					return $data;
					break;
				}
			}
		}
		return;
	}

	private function IsBetween($from, $till, $input)
	{
		$f = DateTime::createFromFormat('!H:i:s', $from);
		$t = DateTime::createFromFormat('!H:i:s', $till);
		$i = DateTime::createFromFormat('!H:i', $input);
		if ($f > $t)
			$t->modify('+1 day');
		return ($f <= $i && $i <= $t) || ($f <= $i->modify('+1 day') && $i <= $t);
	}

	private function locationRateCalculation($quote, $distance, $fleet)
	{

		$sql = "SELECT *,ST_AsText(coordinates) coordinates  FROM `zones`"
			. "WHERE ST_WITHIN(Point({$quote['start_lng']},{$quote['start_lat']}), coordinates) "
			. "AND ST_WITHIN(Point({$quote['end_lng']},{$quote['end_lat']}), coordinates) "
			. "AND `fleet_id`= {$fleet->id}";

		$location_data = $this->db->query($sql)->result();


		if (empty($location_data)) {
			$fare = 0;
			return $fare;
		}

		if (count($location_data) == 1) {
			$location_data = $location_data[0];
		} else {
			$location_data = $this->locationBestMatch($location_data);
		}

		$fare = $location_data->rate;
		return $fare;
	}
	private function locationBestMatch($zone_data)
	{
		$bestMatch = $zone_data[0];
		foreach ($zone_data as $index => $zone_rate) {

			if ($index == 0) {
				continue;
			}
			// NOTE:
			// check if next zone lies inside bestMatch
			// if so, next zone will be the bestMatch
			$sql = "SELECT
                    ST_WITHIN(
                        ST_GeomFromText('$zone_rate->coordinates'),
                        ST_GeomFromText('$bestMatch->coordinates')
                    ) as z1_result";
			$r = $this->db->query($sql)->row();

			if ($r->z1_result) {
				$bestMatch = $zone_rate;
				continue;
			}
		}
		return $bestMatch;
	}
	private function routeRateCalculation($quote, $distance, $fleet)
	{
		$sql = "SELECT *,ST_AsText(coordinates_1) coordinates_1,ST_AsText(coordinates_2) coordinates_2  FROM `routes`"
			. " WHERE (ST_WITHIN(Point({$quote['start_lng']},{$quote['start_lat']}), coordinates_1) "
			. " AND ST_WITHIN(Point({$quote['end_lng']},{$quote['end_lat']}), coordinates_2)) ";

		if ($quote['journey_type'] == 'two_way') {
			$sql .= " OR (ST_WITHIN(Point({$quote['end_lng']},{$quote['end_lat']}), coordinates_1) "
				. " AND ST_WITHIN(Point({$quote['start_lng']},{$quote['start_lat']}), coordinates_2)) ";
		}

		$sql .= " AND `fleet_id`= {$fleet->id}";

		$zone_data = $this->db->query($sql)->result();


		if (empty($zone_data)) {
			$fare = 0;
			return $fare;
		}

		if (count($zone_data) == 1) {
			$zone_data = $zone_data[0];
		} else {
			$zone_data = $this->routeBestMatch($zone_data);
		}

		$fare = $zone_data->rate;

		return $fare;
	}
	private function routeBestMatch($zone_data)
	{
		$bestMatch = $zone_data[0];
		foreach ($zone_data as $index => $zone_rate) {

			if ($index == 0) {
				continue;
			}
			// NOTE:
			// check if next zone lies inside bestMatch
			// if so, next zone will be the bestMatch
			$sql = "SELECT
                    ST_WITHIN(
                        ST_GeomFromText('$zone_rate->coordinates_1'),
                        ST_GeomFromText('$bestMatch->coordinates_1')
                    ) as z1_result,
                    ST_WITHIN(
                        ST_GeomFromText('$zone_rate->coordinates_2'),
                        ST_GeomFromText('$bestMatch->coordinates_2')
                    ) as z2_result";
			$r = $this->db->query($sql)->row();

			if ($r->z1_result && $r->z2_result) {
				$bestMatch = $zone_rate;
				continue;
			}
		}
		return $bestMatch;
	}
	private function ZoneRateCalculation($quote, $distance, $fleet)
	{
		$sql = "SELECT *  FROM `zones_rate` as zr "
			. "INNER JOIN `zones` as z1 on z1.id=zr.from_id "
			. "INNER JOIN `zones` as z2 on z2.id=zr.to_id "
			. "WHERE ST_WITHIN(Point({$quote['start_lng']},{$quote['start_lat']}), z1.coordinates) "
			. "AND ST_WITHIN(Point({$quote['end_lng']},{$quote['end_lat']}), z2.coordinates) "
			. "AND `fleet_id`= {$fleet->id}";

		$zone_data = $this->db->query($sql)->result();


		if (empty($zone_data)) {
			$fare = 0;
			return $fare;
		}

		if (count($zone_data) == 1) {
			$zone_data = $zone_data[0];
		} else {
			// find best match
			$zone_data = $this->zoneBestMatch($zone_data);
		}
		//       Zone Fare rate calculation
		if ($zone_data->rate_type == "fix") {
			$fare = $zone_data->rate;
		} else {
			$fare = $zone_data->rate * $distance;
			$fare = ($zone_data->minimum_rate >= $fare) ? $zone_data->minimum_rate : $fare;
		}
		return $fare;
	}

	private function zoneBestMatch($zone_data)
	{
		$bestMatch = $zone_data[0];
		foreach ($zone_data as $index => $zone_rate) {

			if ($index == 0) {
				continue;
			}
			// NOTE:
			// check if next zone lies inside bestMatch
			// if so, next zone will be the bestMatch
			$sql = "SELECT
                    ST_WITHIN(
                        ST_GeomFromText('$zone_rate->z1_coordinates'),
                        ST_GeomFromText('$bestMatch->z1_coordinates')
                    ) as z1_result,
                    ST_WITHIN(
                        ST_GeomFromText('$zone_rate->z2_coordinates'),
                        ST_GeomFromText('$bestMatch->z2_coordinates')
                    ) as z2_result";
			$r = $this->db->query($sql)->row();

			if ($r->z1_result && $r->z2_result) {
				$bestMatch = $zone_rate;
				continue;
			}
		}
		return $bestMatch;
	}

	private function breakDownCalculation($distance, $fleet)
	{
		$min_rate = $this->fare_breakdown_model->get(['is_min' => 1, 'fleet_id' => $fleet->id, 'start <=' => $distance]);
		if (empty($min_rate->rate)) {
			return 0;
		}
		if ($distance > $min_rate->end) {
			$fare = $min_rate->rate;
			$fare_ranges = $this->fare_breakdown_model->order_by('start', 'asc')->get_all(['is_min' => 0, 'fleet_id' => $fleet->id, 'start <=' => $distance]);
			$new_dist = $distance - $min_rate->end;
			if ($fare_ranges) {
				foreach ($fare_ranges as $fare_range) {
					$fare += ($new_dist < $fare_range->end - $fare_range->start) ? ($new_dist * $fare_range->rate) : (($fare_range->end - $fare_range->start) * $fare_range->rate);
					$new_dist = $new_dist - ($fare_range->end - $fare_range->start);
				}
			}
			$breakdown_fare = $fare;
		} else {
			$breakdown_fare = $min_rate->rate;
		}
		return round($breakdown_fare, 2);
	}

	private function RoundTripRateCalculationForLocation($fare, $fleet, $quote)
	{

		$additional_rate = $this->additional_rate_model->get(['fleet_id' => $fleet->id]);
		$multi_airport = json_decode($additional_rate->multi_airport, true);
		$multi_airport_dropoff = json_decode($additional_rate->multi_airport_dropoff, true);
		$multi_seaport = json_decode($additional_rate->multi_seaport, true);
		$multi_station = json_decode($additional_rate->multi_station, true);
		$oneWayAirport = isAirport($quote) ? $multi_airport[isAirport($quote)] : 0;
		$twoWayAirport = (($quote['journey_type'] == 'two_way') && isEndAirport($quote)) ? (!empty($multi_airport[isEndAirport($quote)]) ? $multi_airport[isEndAirport($quote)] : 0) : 0;

		$oneWayAirportDropOff = isEndAirport($quote) ? $multi_airport_dropoff[isEndAirport($quote)] : 0;
		$twoWayAirportDropOff = (($quote['journey_type'] == 'two_way') && isAirport($quote)) ? (!empty($multi_airport_dropoff[isAirport($quote)]) ? $multi_airport_dropoff[isAirport($quote)] : 0) : 0;

		$airportCharge = ($oneWayAirport + (($quote['journey_type'] == 'two_way') ? $twoWayAirport : 0));
		$airportCharge += ($oneWayAirportDropOff + (($quote['journey_type'] == 'two_way') ? $twoWayAirportDropOff : 0));

		$airportCharge = round($airportCharge, 2);

		$oneWaySeaport = isSeaport($quote) ? $multi_seaport[isSeaport($quote)] : 0;
		$twoWaySeaport = (($quote['journey_type'] == 'two_way') && isEndSeaport($quote)) ? (!empty($multi_airport[isEndSeaport($quote)]) ? $multi_airport[isEndSeaport($quote)] : 0) : 0;
		$seaportCharge = $oneWaySeaport + (($quote['journey_type'] == 'two_way') ? $twoWaySeaport : 0);

		$oneWayStation = isStation($quote) ? $multi_station[isStation($quote)] : 0;
		$twoWayStation = (($quote['journey_type'] == 'two_way') && isEndStation($quote)) ? (!empty($multi_airport[isEndStation($quote)]) ? $multi_airport[isEndStation($quote)] : 0) : 0;
		$stationCharge = $oneWayStation + (($quote['journey_type'] == 'two_way') ? $twoWayStation : 0);

		$roundTripCharge = round($fare * $additional_rate->round_trip * 0.01, 2);
		if ($airportCharge) {
			$one_way_fare = $fare + $airportCharge;
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $airportCharge;
		} elseif ($seaportCharge) {
			$one_way_fare = ($fare + $seaportCharge);
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $seaportCharge;
		} elseif ($stationCharge) {
			$one_way_fare = ($fare + $stationCharge);
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $stationCharge;
		} else {
			$one_way_fare = $fare;
			$two_way_fare = $fare + ($fare - $roundTripCharge);
		}

		$charge_details = [
			'airport_charge' => round($airportCharge, 2),
			'seaport_charge' => round($seaportCharge, 2),
			'station_charge' => round($stationCharge, 2),
			'round_trip_discount' => round($roundTripCharge, 2),
			'round_trip_discount_percentage' => $additional_rate->round_trip
		];
		return (['rate' => round($one_way_fare, 2), 'round_trip_rate' => round($two_way_fare, 2), 'charge_details' => $charge_details]);
	}
	private function RoundTripRateCalculationForRoute($fare, $fleet, $quote)
	{

		$additional_rate = $this->additional_rate_model->get(['fleet_id' => $fleet->id]);
		$multi_airport = json_decode($additional_rate->multi_airport, true);
		$multi_seaport = json_decode($additional_rate->multi_seaport, true);
		$multi_station = json_decode($additional_rate->multi_station, true);

		$oneWayAirport = isAirport($quote) ? $multi_airport[isAirport($quote)] : 0;
		$twoWayAirport = (($quote['journey_type'] == 'two_way') && isEndAirport($quote)) ? (!empty($multi_airport[isEndAirport($quote)]) ? $multi_airport[isEndAirport($quote)] : 0) : 0;
		$airportCharge = round($oneWayAirport + (($quote['journey_type'] == 'two_way') ? $twoWayAirport : 0), 2);

		$oneWaySeaport = isSeaport($quote) ? $multi_seaport[isSeaport($quote)] : 0;
		$twoWaySeaport = (($quote['journey_type'] == 'two_way') && isEndSeaport($quote)) ? (!empty($multi_airport[isEndSeaport($quote)]) ? $multi_airport[isEndSeaport($quote)] : 0) : 0;
		$seaportCharge = $oneWaySeaport + (($quote['journey_type'] == 'two_way') ? $twoWaySeaport : 0);

		$oneWayStation = isStation($quote) ? $multi_station[isStation($quote)] : 0;
		$twoWayStation = (($quote['journey_type'] == 'two_way') && isEndStation($quote)) ? (!empty($multi_airport[isEndStation($quote)]) ? $multi_airport[isEndStation($quote)] : 0) : 0;
		$stationCharge = $oneWayStation + (($quote['journey_type'] == 'two_way') ? $twoWayStation : 0);

		$roundTripCharge = round($fare * $additional_rate->round_trip * 0.01, 2);
		if ($airportCharge) {
			$one_way_fare = $fare + $airportCharge;
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $airportCharge;
		} elseif ($seaportCharge) {
			$one_way_fare = ($fare + $seaportCharge);
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $seaportCharge;
		} elseif ($stationCharge) {
			$one_way_fare = ($fare + $stationCharge);
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $stationCharge;
		} else {
			$one_way_fare = $fare;
			$two_way_fare = $fare + ($fare - $roundTripCharge);
		}

		$charge_details = [
			'airport_charge' => round($airportCharge, 2),
			'seaport_charge' => round($seaportCharge, 2),
			'station_charge' => round($stationCharge, 2),
			'round_trip_discount' => round($roundTripCharge, 2),
			'round_trip_discount_percentage' => $additional_rate->round_trip
		];
		return (['rate' => round($one_way_fare, 2), 'round_trip_rate' => round($two_way_fare, 2), 'charge_details' => $charge_details]);
	}

	private function RoundTripRateCalculationForZone($fare, $fleet, $quote)
	{

		$additional_rate = $this->additional_rate_model->get(['fleet_id' => $fleet->id]);
		$multi_airport = json_decode($additional_rate->multi_airport, true);
		$multi_seaport = json_decode($additional_rate->multi_seaport, true);
		$multi_station = json_decode($additional_rate->multi_station, true);

		$oneWayAirport = isAirport($quote) ? $multi_airport[isAirport($quote)] : 0;
		$twoWayAirport = (($quote['journey_type'] == 'two_way') && isEndAirport($quote)) ? (!empty($multi_airport[isEndAirport($quote)]) ? $multi_airport[isEndAirport($quote)] : 0) : 0;
		$airportCharge = round($oneWayAirport + (($quote['journey_type'] == 'two_way') ? $twoWayAirport : 0), 2);

		$oneWaySeaport = isSeaport($quote) ? $multi_seaport[isSeaport($quote)] : 0;
		$twoWaySeaport = (($quote['journey_type'] == 'two_way') && isEndSeaport($quote)) ? (!empty($multi_airport[isEndSeaport($quote)]) ? $multi_airport[isEndSeaport($quote)] : 0) : 0;
		$seaportCharge = $oneWaySeaport + (($quote['journey_type'] == 'two_way') ? $twoWaySeaport : 0);

		$oneWayStation = isStation($quote) ? $multi_station[isStation($quote)] : 0;
		$twoWayStation = (($quote['journey_type'] == 'two_way') && isEndStation($quote)) ? (!empty($multi_airport[isEndStation($quote)]) ? $multi_airport[isEndStation($quote)] : 0) : 0;
		$stationCharge = $oneWayStation + (($quote['journey_type'] == 'two_way') ? $twoWayStation : 0);

		$roundTripCharge = round($fare * $additional_rate->round_trip * 0.01, 2);
		if ($airportCharge) {
			$one_way_fare = $fare + $airportCharge;
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $airportCharge;
		} elseif ($seaportCharge) {
			$one_way_fare = ($fare + $seaportCharge);
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $seaportCharge;
		} elseif ($stationCharge) {
			$one_way_fare = ($fare + $stationCharge);
			$two_way_fare = $fare + ($fare - $roundTripCharge) + $stationCharge;
		} else {
			$one_way_fare = $fare;
			$two_way_fare = $fare + ($fare - $roundTripCharge);
		}

		$charge_details = [
			'airport_charge' => round($airportCharge, 2),
			'seaport_charge' => round($seaportCharge, 2),
			'station_charge' => round($stationCharge, 2),
			'round_trip_discount' => round($roundTripCharge, 2),
			'round_trip_discount_percentage' => $additional_rate->round_trip
		];
		return (['rate' => round($one_way_fare, 2), 'round_trip_rate' => round($two_way_fare, 2), 'charge_details' => $charge_details]);
	}

	private function additionalRateCalculationForFare($fare, $fleet, $quote, $journey_type)
	{
		$additional_rate = $this->additional_rate_model->get(['fleet_id' => $fleet->id]);
		$multi_airport = json_decode($additional_rate->multi_airport, true);
		$multi_airport_dropoff = json_decode($additional_rate->multi_airport_dropoff, true);
		$multi_seaport = json_decode($additional_rate->multi_seaport, true);
		$multi_station = json_decode($additional_rate->multi_station, true);

		if ($journey_type == 'one_way')
			$oneWayAirport = isAirport($quote) ? $multi_airport[isAirport($quote)] : 0;
		else
			$oneWayAirport = isEndAirport($quote) ? $multi_airport[isEndAirport($quote)] : 0;

		if ($journey_type == 'one_way')
			$oneWayAirportDropOff = isEndAirport($quote) ? $multi_airport_dropoff[isEndAirport($quote)] : 0;
		else
			$oneWayAirportDropOff = isAirport($quote) ? $multi_airport_dropoff[isAirport($quote)] : 0;

		$airportCharge = $oneWayAirport + $oneWayAirportDropOff;

		if ($journey_type == 'one_way')
			$oneWaySeaport = isSeaport($quote) ? $multi_seaport[isSeaport($quote)] : 0;
		else
			$oneWaySeaport = isEndSeaport($quote) ? $multi_seaport[isEndSeaport($quote)] : 0;

		$seaportCharge = $oneWaySeaport;
		if ($journey_type == 'one_way')
			$oneWayStation = isStation($quote) ? $multi_station[isStation($quote)] : 0;
		else
			$oneWayStation = isEndStation($quote) ? $multi_station[isEndStation($quote)] : 0;

		$stationCharge = $oneWayStation;

		// viaPoint extra charge
		$numberOfStopPoints = 0;
		$numberOfStopPoints = count($quote['stop_point'] ?? []);
		$viaPointCharge = $numberOfStopPoints * 10;


		if ($airportCharge) {
			$one_way_fare = $fare + $airportCharge;
		} elseif ($seaportCharge) {
			$one_way_fare = $fare + $seaportCharge;
		} elseif ($stationCharge) {
			$one_way_fare = $fare + $stationCharge;
		} else {
			$one_way_fare = $fare;
		}

		$roundTripCharge = ($journey_type == 'two_way') ? ceil($fare * $additional_rate->round_trip * 0.01) : 0;
		if ($roundTripCharge) {
			$one_way_fare -= $roundTripCharge;
		}

		$charge_details = [
			'increment' => [
				'airport_charge' => ceil($airportCharge),
				'seaport_charge' => ceil($seaportCharge),
				'station_charge' => ceil($stationCharge),
				'via_point_charge' => ceil($viaPointCharge),
			],
			'decrement' => [
				'round_trip_discount' => ceil($roundTripCharge),
				// 'round_trip_discount_percentage' => $additional_rate->round_trip
			],
		];
		return (['rate' => ceil($one_way_fare), 'charge_details' => $charge_details]);
	}

	private function processAdditionalRates($selected_fleet, $booking_details)
	{
		$additional_rate = $this->additional_rate_model->get(['fleet_id' => $selected_fleet->id]);
		$baby_seat = ($booking_details['infant_seat'] ?? 0) + ($booking_details['child_seat'] ?? 0) + ($booking_details['child_booster_seat'] ?? 0) + ($booking_details['booster_seat'] ?? 0);
		$grand_total_charge['baby_seater_charge'] = !empty($baby_seat) ? ($additional_rate->baby_seater * $baby_seat) : 0;
		if (!empty($booking_details['discount_code'])) {
			$discountData = $this->discount_coupon_model->discountProcess($booking_details['discount_code']);
			if ($discountData['status']) {
				if ($discountData['discount_type'] == 'by_percent') {
					$discountData['discount'] = round($selected_fleet->fare * $discountData['discount'] * 0.01, 2);
				}
			}
			$grand_total_charge['coupons_discount'] = $discountData['discount'];
		}

		$holidayCharge = $this->checkHolidays($selected_fleet, ['date' => $booking_details['pickup_date'], 'time' => $booking_details['pickup_time']]);
		if ($holidayCharge['charge'] ?? false) {
			$grand_total_charge['holiday_charge'] = round($holidayCharge['type'] == 'percentage' ? $holidayCharge['charge'] * 0.01 * $selected_fleet->fare : $holidayCharge['charge'], 2);
		}

		$rushHourCharge = $this->checkRushHour($selected_fleet, ['time' => date('H:i', strtotime($booking_details['pickup_time']))]);
		if ($rushHourCharge) {
			$grand_total_charge['rush_hour_charge'] = round($rushHourCharge, 2);
		}

		return $grand_total_charge;
	}

	private function vehicleSpecificAdditionalRate($quote, $selected_fleet, $booking_info)
	{

		$additional_rate = $this->additional_rate_model->get(['fleet_id' => $selected_fleet->id]);

		$additional_airport_pickup = (isAirport($quote)) ? $additional_rate->airport_pickup_fee : 0;
		$additional_seaport_pickup = (isSeaport($quote)) ? $additional_rate->airport_pickup_fee : 0;

		$additional_baby_seater_charge = !empty($booking_info->client_baby_no) ? ($additional_rate->baby_seater * $booking_info->client_baby_no) : 0;
		$additional_waiting_time = 0;
		$additional_via_point = (!empty($quote['stop_point']) ? count($quote['stop_point']) * $additional_rate->via_point : 0);

		// $additional_meet_and_greet = isset($vehicle_info['meet_and_greet']) && $vehicle_info['meet_and_greet'] == 'yes' ? $additional_rate->meet_and_greet : 0;
		// $return_additional_meet_and_greet = isset($vehicle_info['return_meet_and_greet']) && $vehicle_info['return_meet_and_greet'] == 'yes' ? $additional_rate->meet_and_greet : 0;
		/*added on frontend*/
		$additional_meet_and_greet = 0;
		$return_additional_meet_and_greet = 0;

		$additional_meet_and_greet = isset($vehicle_info['meet_and_greet']) && $vehicle_info['meet_and_greet'] == 'yes' ? $additional_rate->meet_and_greet : 0;

		$return_additional_meet_and_greet = isset($vehicle_info['return_meet_and_greet']) && $vehicle_info['return_meet_and_greet'] == 'yes' ? $additional_rate->meet_and_greet : 0;

		if ($booking_info['pay_method'] == 'cash') {
			$additional_charge = $additional_meet_and_greet + $return_additional_meet_and_greet + $additional_waiting_time + $additional_baby_seater_charge;
			$total = ($selected_fleet->total) + $additional_charge;
			$grand_total_charge = array(
				'additional_airport_pickup_charge' => $additional_airport_pickup,
				'additional_seaport_pickup_charge' => $additional_seaport_pickup,
				'additional_baby_seater_charge' => $additional_baby_seater_charge,
				'additional_card_service_charge' => '0',
				'additional_waiting_time' => $additional_waiting_time,
				'additional_via_point' => $additional_via_point,
				'additional_meet_and_greet' => $additional_meet_and_greet + $return_additional_meet_and_greet,
				'additional_charge' => $additional_charge + $additional_airport_pickup + $additional_seaport_pickup,
				'total' => $total
			);
		} else {
			$additional_charge = 0;
			$total = 0;
			$grand_total_charge = array(
				'additional_airport_pickup_charge' => $additional_airport_pickup,
				'additional_seaport_pickup_charge' => $additional_seaport_pickup,
				'additional_baby_seater_charge' => $additional_baby_seater_charge,
				'additional_meet_and_greet' => $additional_meet_and_greet + $return_additional_meet_and_greet,
				'additional_waiting_time' => $additional_waiting_time,
				'additional_via_point' => $additional_via_point,
			);
			if ($additional_rate->card_fee_type == 'By_Percentage') {
				$additional_charge = $additional_waiting_time + $additional_baby_seater_charge;
				$sub_additional_charge = $selected_fleet->total + $additional_charge;
				$additional_card_service_charge = $sub_additional_charge * (($additional_rate->card_fee) / 100);
				$subtotal = $sub_additional_charge + $additional_card_service_charge + $additional_meet_and_greet + $return_additional_meet_and_greet;
				$total = $subtotal;
				$grand_total_charge['additional_charge'] = $additional_charge + $additional_card_service_charge + $additional_airport_pickup + $additional_seaport_pickup + $additional_meet_and_greet;
				$grand_total_charge['additional_card_service_charge'] = $additional_card_service_charge;
				$grand_total_charge['total'] = $total;
			} else {
				$additional_charge = $additional_rate->card_fee + $additional_waiting_time + $additional_baby_seater_charge;
				$total = $additional_charge + $selected_fleet->total + $additional_meet_and_greet + $return_additional_meet_and_greet;

				$grand_total_charge['additional_charge'] = $additional_charge + $additional_airport_pickup + $additional_seaport_pickup + $additional_meet_and_greet + $return_additional_meet_and_greet;
				$grand_total_charge['additional_card_service_charge'] = $additional_rate->card_fee;
				$grand_total_charge['total'] = $total;
			}
		}
		$grand_total_charge['vehicle_fare'] = $selected_fleet->total - $additional_airport_pickup;
		$discountData = $this->discount_coupon_model->discountProcess($booking_info);

		if ($discountData['status']) {
			if ($discountData['discount_type'] == 'by_percent') {
				$discountData['discount'] = round($grand_total_charge['total'] * $discountData['discount'] * 0.01, 2);
			}
			$grand_total_charge['total'] = round($grand_total_charge['total'] - $discountData['discount'], 2);
		}
		$grand_total_charge['discountCharge'] = $discountData['discount'];
		$grand_total_charge['discountStatus'] = $discountData;

		//debug($grand_total_charge);
		// setSession('grand_total_charge', $grand_total_charge['total']);
		// setSession('grand_total_details', $grand_total_charge);
		return $grand_total_charge;
	}

	private function sendBookingReceivedEmail($booking_infos)
	{
		$email_template = $this->db->get('email_templates')->row();
		$mail_message = !empty($email_template->booking_confirmation_top) ? $email_template->booking_confirmation_top : "Your quote request details are as follows. We will get back to you soon as possible.";
		$admin_emailer_template = $this->load->view('emailer/booking_emailler', array('data' => $booking_infos, 'emailer_to' => 'admin'), true);
		$client_emailer_template = $this->load->view('emailer/booking_emailler', array('data' => $booking_infos, 'emailer_to' => 'client', 'mail_message' => $mail_message), true);
		$pax_account_emailer_template = $this->load->view('emailer/booking_emailler', array('data' => $booking_infos, 'passenger' => $this->data['passenger'], 'emailer_to' => 'account_client', 'mail_message' => $mail_message), true);

		$admin_mergedHtml = common_emogrifier($admin_emailer_template);
		$client_mergedHtml = common_emogrifier($client_emailer_template);
		$pax_account_mergedHtml = common_emogrifier($pax_account_emailer_template);

		$this->booking_info_model->update(['mail_subject_confirm' => SITE_NAME . " Booking Confirmation [{$booking_infos['booking_ref_id']}]", 'mail_template_confirm' => $client_mergedHtml], ['id' => $booking_infos['id']]);

		// mail to admin between 8am - 5pm
// 		$date = new DateTime();
// 		$current_time = date('H:i', strtotime($date->format('H:i:s')));
// 		if ($this->IsBetween('0800:00', '17:00:00', $current_time)) {
// 			email_help([ADMIN_EMAIL], "Booking Received - " . SITE_NAME . " [{$booking_infos['booking_ref_id']}]", $admin_mergedHtml, [SITE_EMAIL => SITE_NAME]);
// 		} else {
// 			$post = [
// 				'booking_ref_id' => $booking_infos['booking_ref_id'],
// 			];
// 			$this->queue_job_model->insert($post);
// 		}

		// $filePath = FCPATH . '/uploads/booking' . $booking_infos['booking_ref_id'] . '.pdf';
		// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->setTitle('Booking-' . $booking_infos['booking_ref_id']);
		// $pdf->AddPage();
		// $pdf->writeHTML($client_mergedHtml);
		// $pdf->Output($filePath, 'F');
		// $attachments = ['booking' => $filePath];
		// email_help($booking_infos['client_email'], "Booking Received - " . SITE_NAME . " [{$booking_infos['booking_ref_id']}]", $client_mergedHtml, [SITE_EMAIL => SITE_NAME], $attachments);
		// email_help($this->data['passenger']->email, "Booking Received - " . SITE_NAME . " [{$booking_infos['booking_ref_id']}]", $pax_account_mergedHtml, [SITE_EMAIL => SITE_NAME], $attachments);
		return ['admin' => $admin_mergedHtml, 'client' => $client_mergedHtml, 'pax' => $pax_account_mergedHtml];
	}

	private function sendBookingReceivedNotificationEmail($booking_infos)
	{
		$booking_infos['mail_message'] = "Your booking [{$booking_infos['booking_ref_id']}] request has been received. You will soon receive a confirmation email after we have processed your booking.
         If you have not received a confirmation email within 2 hours of booking, Please check your spam box and contact us to find out about the status of your booking. 
         Telephone: " . SITE_NUMBER . " Email: " . SITE_EMAIL . ". Passengers making a booking between midnight and 06:00 AM will receive their confirmation email after 07:00 AM. ";

		$emailer_template = common_emogrifier($this->load->view('emailer/emailler_booking_received_notification', array('data' => $booking_infos), true));

		email_help([$booking_infos['client_email'], ADMIN_EMAIL_1], "Booking received - " . SITE_NAME . "[{$booking_infos['booking_ref_id']}]", $emailer_template, [SITE_EMAIL => SITE_NAME]);
	}

	function paypal_ipn()
	{
		$invoice_id = $this->input->get('invoice_id');
		if (!$invoice_id) {
			show_404('Warning!. Invalid method call.');
		}

		$post = $this->input->post();

		if (!isset($post['payment_gross'])) {
			show_error('Invalid ipn request.');
		}
		$this->invoice_model->update(array('payment_status' => 1, 'date_of_payment' => date('Y-m-d H:i:s')), array('invoice_id' => $invoice_id));
		$invoice = $this->invoice_model->get(['invoice_id' => $invoice_id]);
		if (empty($invoice->booking_id)) {
			$invoice = $this->invoice_bulk_model->get(['invoice_id' => $invoice_id]);
		}
		if (!empty($invoice->booking_id)) {
			$this->booking_info_model->update(['is_paid' => 1], ['booking_ref_id' => $invoice->booking_id]);
		}
	}

	function verify_payment_worldpay()
	{

		parse_str(file_get_contents("php://input"), $data);
		$data = (object) $data;

		$invoice = $this->db->get_where('tbl_invoice', array('invoice_id' => $data->cartId))->row();
		if ($invoice) {

			if ($data->transStatus == 'Y') {
				try {
					// send receipt
					//                    $this->booking_model->send_auto_receipt_email($invoice->invoice_id);
					$this->db->update('tbl_invoice', array('payment_status' => 1, 'date_of_payment' => date('Y-m-d H:i:s')), array('invoice_id' => $data->cartId));
				} catch (Exception $e) {
					log_message('error', 'WORLDPAY ERR: ' . $e->getMessage());
				}
			}
		}
	}

	private function viaPointFormat($get)
	{
		//Way point formatting
		$way_latlng = '';
		if (!empty($get['way_point_lat']) && !empty($get['way_point_lng'])) {
			foreach ($get['way_point_lat'] as $key => $way_lat) :
				if (!empty($way_lat))
					$way_point_lat[$key] = $way_lat;
			endforeach;
			foreach ($get['way_point_lng'] as $key => $way_lng) :
				if (!empty($way_lng))
					$way_point_lng[$key] = $way_lng;
			endforeach;


			$way_points_concate = [];
			for ($i = 0; $i < count($way_point_lat); $i++) :
				$way_points_concate[] = $way_point_lat[$i] . "," . $way_point_lng[$i];
			endfor;

			$way_latlng = implode('|', $way_points_concate);
		}

		return $way_latlng;
	}

	function ajaxDateTimeCheck()
	{
		$pickup_date = $this->input->post('date');
		$pickup_time = $this->input->post('time');

		$pickup_datetime = DateTime::createFromFormat('Y-m-d H:i', $pickup_date . " " . $pickup_time);
		$current_date = date('Y-m-d H:i:s', strtotime('+' . MIN_BOOKING_HOURS . ' hours'));
		$next_date = new DateTime($current_date);

		if ($pickup_datetime < $next_date) {
			$msg = '<h4>To ensure the quality of service, we are unable to offer online bookings for transfers commencing within the next ' . MIN_BOOKING_HOURS . ' hours.' .
				'If you require an urgent transfer please call our contact centre on <a href="tel:' . SITE_NUMBER . '">' . SITE_NUMBER . '</a> or email us at <a href="mailto:' . SITE_EMAIL . '">' . SITE_EMAIL . '</a> and we are happy to help.</h4>';
			echo json_encode(['status' => false, 'message' => $msg, 'data' => '']);
			return;
		}
		echo json_encode(['status' => true, 'message' => 'success', 'data' => '']);
		return;
	}

	function postCodeFormat($data)
	{
		$postcodeRegex = "/((GIR 0AA)|((([A-PR-UWYZ][0-9][0-9]?)|(([A-PR-UWYZ][A-HK-Y][0-9][0-9]?)|(([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY])))) [0-9][ABD-HJLNP-UW-Z]{2}))/i";
		$is_start = $is_end = null;
		if (preg_match($postcodeRegex, $data['start'], $matches_start)) {
			$postcode_start = explode(' ', $matches_start[0]);
			$is_start = $postcode_start[0];
		}

		if (preg_match($postcodeRegex, $data['end'], $matches_end)) {
			$postcode_end = explode(' ', $matches_end[0]);
			$is_end = $postcode_end[0];
		}
		return (['start_post_code' => $is_start, 'end_post_code' => $is_end]);
	}

	function ajax_process_discount_coupon()
	{
		$discount_coupon_no = $this->input->post('discount_coupon');
		$final_fare = $this->input->post('final_fare');
		$msg_discount = array(
			'status' => 0,
			'msg' => '',
			'amount' => 0,
			'coupon_no' => $discount_coupon_no,
			'discount_coupon' => null
		);

		if (!$discount_coupon_no) {
			$msg_discount['msg'] = 'You did not enter any discount coupons.';
			jsonOutput($msg_discount['status'], $msg_discount['msg'], $msg_discount);
		}

		$coupon = $this->db->where(array('coupon_no' => $discount_coupon_no))->get('tbl_discount_coupon_list')->row();
		if (!$coupon) {
			$msg_discount['msg'] = 'Invalid coupon (' . $discount_coupon_no . ')';
			jsonOutput($msg_discount['status'], $msg_discount['msg'], $msg_discount);
		}

		$dc = $this->db->where(array('id' => $coupon->discount_coupon_id))->get('tbl_discount_coupons')->row();

		$today = date_create();
		$valid_from = date_create_from_format('d/m/Y', $dc->valid_from);
		$valid_to = date_create_from_format('d/m/Y', $dc->valid_to);

		if (!($today >= $valid_from && $today <= $valid_to)) { // date check
			$msg_discount['msg'] = 'Your coupon (' . $discount_coupon_no . ') is valid but out of date.';
			jsonOutput($msg_discount['status'], $msg_discount['msg'], $msg_discount);
		}

		if (!$dc->is_active) {
			$msg_discount['msg'] = 'This coupon (' . $discount_coupon_no . ') is currently disabled.';
			jsonOutput($msg_discount['status'], $msg_discount['msg'], $msg_discount);
		}

		if ($coupon->is_redeemed) {

			if (!$dc->is_reusable) {
				$msg_discount['msg'] = 'This coupon (' . $discount_coupon_no . ') is already redeemed.';
				jsonOutput($msg_discount['status'], $msg_discount['msg'], $msg_discount);
			}

			if ($dc->max_redeem_limit != 0 && $dc->max_redeem_limit == $dc->no_of_coupons_redeemed) {
				$msg_discount['msg'] = 'This coupon (' . $discount_coupon_no . ') has exceeded its max usage limit.';
				jsonOutput($msg_discount['status'], $msg_discount['msg'], $msg_discount);
			}

			$msg_discount['status'] = 1;
			$msg_discount['amount'] = round($dc->discount_type == 'by_percent' ? $final_fare * $dc->discount * 0.01 : $dc->discount);
			$msg_discount['msg'] = $dc->coupon_title . ' (' . ($dc->discount_type == 'by_percent' ? $dc->discount . '%' : '$' . $dc->discount) . ')';
			$msg_discount['discount_coupon'] = $dc;
			jsonOutput($msg_discount['status'], $msg_discount['msg'], $msg_discount);
		}

		$msg_discount['status'] = 1;
		$msg_discount['amount'] = round($dc->discount_type == 'by_percent' ? $final_fare * $dc->discount * 0.01 : $dc->discount);
		$msg_discount['msg'] = $dc->coupon_title . ' (' . ($dc->discount_type == 'by_percent' ? $dc->discount . '%' : '$' . $dc->discount) . ')';
		$msg_discount['discount_coupon'] = $dc;
		jsonOutput($msg_discount['status'], $msg_discount['msg'], $msg_discount);
	}

	function ajax_get_locations()
	{

		if (!$this->input->is_ajax_request()) {
			jsonOutput(0, 'Invalid method call.');
		}
		$keywords = $_GET['q'];
		$result = $this->location_model->search($keywords);

		$see_more_data = [
			'id' => 0,
			// 'label' => '<span class="see-more" style="display:inline-block; text-align:right"> Load more...</span>',
			'label' => 'Load more...',
			'postcode' => '',
			'logo' => 'fa-plus'
		];

		if ($result) {
			$data = array();
			foreach ($result as $r) {
				array_push($data, array(
					'id' => $r->id,
					'label' => $r->name,
					'postcode' => $r->postcode,
					'logo' => getFAByLocationTypeId($r->location_type_id)
				));
			}
			array_push($data, $see_more_data);
			jsonOutput(1, 'query: ' . $keywords, $data);
		} else {
			jsonOutput(0, 'something went wrong (Keywords):' . $keywords, [$see_more_data]);
		}
	}
}
