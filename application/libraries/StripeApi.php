<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/vendor/autoload.php';
class StripeApi
{

    function __construct()
    {
        \Stripe\Stripe::setApiKey(STR_SECRET_KEY);
        
    }

    public function createCharge($card_token, $data)
    {

        try {
            $response_data = \Stripe\Charge::create([
                'amount' => $data['charge_amount'] * 100,
                'currency' => 'GBP',
                "source" => $card_token,
                // 'description' => '',
            ]);
            $response = ['status' => 1, 'msg' => "Payment success.", 'data' => $response_data];
        } catch (Exception $e) {
            $response = ['status' => 0, 'msg' => $e->getMessage()];
        }
        return $response;
    }

    public function getCardToken($data)
    {
        try {
            $response_data = \Stripe\Token::create([
                'card' => [
                    'number' => $data['card_number'],
                    'exp_month' => $data['card_expiry_month'],
                    'exp_year' => $data['card_expiry_year'],
                    'cvc' => $data['card_verification_number'],
                    'name' => $data['card_holder_name'],
                    'address_country' => 'UK',
                    'currency' => 'GBP',
                ]
            ]);
            $response = ['status' => 1, 'msg' => "Card added.", 'data' => $response_data];
        } catch (Exception $e) {
            $response = ['status' => 0, 'msg' => $e->getMessage()];
        }
        return $response;
    }
}
