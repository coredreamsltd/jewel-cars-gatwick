<?php
use Twilio\Rest\Client;

class TwilioSms
{
    
    private $sid = "ACXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    private $token = "your_auth_token";
    private $sender = SMS_SENDER;
    private $client;
    
    public function __construct()
    {
        return [
            'success' => true,
            'data' => 'test'
        ];
        
        $this->client = new Client($this->sid, $this->token);
    }

    function send_message($post_body)
    {
        return [
            'success' => true,
            'data' => 'test'
        ];
        try {

            $http_response = [];

            foreach($post_body['to'] as $to){
                $http_response[] = $this->client->messages
                    ->create(
                        $to,
                        array(
                            "from" => $post_body['from'],
                            "body" => $post_body['body']
                        )
                    );
            }

            return [
                'success' => true,
                'data' => $http_response
            ];
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
            return [
                'success' => false,
                'msg' => $e->getMessage()
            ];
        }
    }

    function seven_bit_sms($message, $msisdn, $parts = 0)
    {
        $post_body = array(
            'from' => $this->sender,
            'to' => explode(',', $msisdn),
            'body' => $message
        );

        return $post_body;
    }


    public function calculate_parts($message)
    {
        return null;
    }

    function formatted_server_response($result)
    {
        $this_result = "";

        if ($result['success']) {
            $this_result .= "Success";
        } else {
            $this_result .= $result['msg'];
        }
        return $this_result;
    }
}
