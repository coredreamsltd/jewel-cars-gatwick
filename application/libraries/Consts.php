<?php

class Consts {

    private $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->setConstants();
    }

    private function setConstants() {
        $settings = $this->CI->db->get('options')->row();
        foreach ($settings as $k => $v) {
            define((string) strtoupper($k), $v);
        }
        return;
    }

}
