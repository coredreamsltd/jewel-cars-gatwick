<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Index';
$route['404_override'] = 'page';
$route['translate_uri_dashes'] = TRUE;

$route['admin'] = 'admin/index';
$route['admin/logout'] = 'admin/index/logout';
$route['admin/content/add-update'] = 'admin/content/add_update';
$route['admin/content/add-update/(:num)'] = 'admin/content/add_update';
$route['admin/content/pages/(:num)'] = 'admin/content/pages/$1';
$route['admin/content/delete-page/(:num)'] = 'admin/content/delete_page/$1';
$route['admin/content/delete-banner/(:num)'] = 'admin/content/delete_banner/$1';
$route['admin/content/add-update-banner'] = 'admin/content/add_update_banner';
$route['admin/content/add-update-banner/(:num)'] = 'admin/content/add_update_banner';


$route['admin/rate/routes-rate'] = 'admin/rate/routes_rate';
$route['admin/rate/routes-rate/(:num)'] = 'admin/rate/routes_rate';
$route['admin/rate/add-update-routes-rate'] = 'admin/rate/add_update_routes_rate';
$route['admin/rate/add-update-routes-rate/(:num)'] = 'admin/rate/add_update_routes_rate';

$route['admin/rate/additional-charges'] = 'admin/rate/additional_charges';
$route['admin/rate/add-update-additional-charges'] = 'admin/rate/add_update_additional_charges';
$route['admin/rate/add-update-additional-charges/(:num)'] = 'admin/rate/add_update_additional_charges';

$route['admin/location/add-update'] = 'admin/location/add_update';
$route['admin/location/add-update/(:num)'] = 'admin/location/add_update/$1';
$route['admin/location/(:num)'] = 'admin/location/index/$1';

$route['passenger/login'] = 'accounts/index/passengers';
$route['passenger/register'] = 'accounts/index/passenger_register';

$route['driver/login'] = 'accounts/index/drivers';
$route['driver/register'] = 'accounts/driver/register';
$route['reset-password'] = 'index/reset_password';

