<footer style="    background: #1d1d1d; padding: 15px 0 5px 0; color: #fff">
    <div class="container">
        <div class="dshbd-ft-end">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="ft-copy">
                        <p>&copy; Copyright <?= date('Y') ?>. All Rights Reserved. <span><?= SITE_NAME ?></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Slick -->
<!--<script src="<?= base_url('assets') ?>/bower_components/slick-carousel/slick/slick.min.js" type="text/javascript"></script>-->
<!-- jquery ui -->
<script src="<?= base_url('assets') ?>/bower_components/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets') ?>/bower_components/timepicker/timepicker.js"></script>
<script src="<?= base_url('assets') ?>/bower_components/geocomplete/jquery.geocomplete.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/bower_components/jquery-validation/dist/jquery.validate.min.js'); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDT6zEpyAmE5P2k_TPVecXzUx8IJqEUg0A"></script>
<!-- bxslider -->
<script src="<?= base_url('assets') ?>/bower_components/bxslider-4/dist/jquery.bxslider.js"></script>
<!--Drawer secondary-expandable-navigation js-->
<script src="<?= base_url('assets') ?>/drawer/js/modernizr.js"></script>
<script src="<?= base_url('assets') ?>/drawer/js/main.js"></script>
<!-- CC Validator -->
<script src="<?= base_url('assets') ?>/bower_components/jquery-creditcardvalidator/jquery.creditCardValidator.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/assets/bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/assets/bower_components/datatables/media/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!--custom-->
<script src="<?= base_url('assets') ?>/js/custom.js" type="text/javascript"></script>
<script src="<?= base_url('assets') ?>/js/bootbox.min.js" type="text/javascript"></script>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.right = "0px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.right = "-250px";
    }
</script>

</body>
</html>


