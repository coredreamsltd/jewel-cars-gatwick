<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= site_url('assets/images/aalux.png') ?>" type="image/png" sizes="16x16">
    <title><?= SITE_NAME ?></title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Jquery UI -->
    <link href="<?= base_url('assets') ?>/bower_components/jquery-ui/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <!-- Fontawesome -->
    <link href="<?= base_url('assets') ?>/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Bxslider -->
    <link href="<?= base_url('assets') ?>/bower_components/bxslider-4/dist/jquery.bxslider.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/dist/css/AdminLTE.min.css">
    <!-- Datatables -->
    <link href="<?= base_url() ?>/assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!--Hover-->
    <link href="<?= base_url('assets') ?>/bower_components/hover/css/hover.css" rel="stylesheet" type="text/css" />
    <!--Flaticon-->
    <link href="<?= base_url('assets') ?>/flaticon/font/flaticon.css" rel="stylesheet" type="text/css" />
    <!--Drawer secondary-expandable-navigation Css-->
    <link rel="stylesheet" href="<?= base_url('assets') ?>/drawer/css/reset.css">
    <link rel="stylesheet" href="<?= base_url('assets') ?>/drawer/css/style.css">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/timepicker/timepicker.css') ?>">
    <!-- Custom CSS -->
    <link href="<?= base_url('assets') ?>/css/global.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets') ?>/css/dashboard.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets') ?>/css/dash-responsive.css" rel="stylesheet" type="text/css" />
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,700" rel="stylesheet">
    <!-- jquery -->
    <script src="<?= base_url('assets') ?>/bower_components/jquery/dist/jquery.js"></script>
</head>

<body>


    <div class="dash-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-sm-4 hidden-xs ">
                    <div class="top-acc">
                        <ul class="list-inline">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-user"></i><?= $driver->name ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="logo text-center">
                        <a href="<?= site_url() ?>">
                            <?=SITE_NAME?>
                        </a>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4 col-xs-6 is-relative">
                    <div class="top-acc">
                        <ul class="list-inline">
                            <li>
                                <button type="btn btn-primary" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
                                    <span class="fa fa-bars"></span>
                                </button>

                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-arrow-left"></span></button>

                </div>
                <div class="modal-body">
                    <ul class="nav side-nav">
                        <li><a href="<?= site_url('accounts/driver/profile') ?>"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="<?= site_url('accounts/driver/vehicleAndDocument') ?>"><i class="fa fa-book"></i> Vehicle & Documents</a></li>
                        <li><a href="<?= site_url('accounts/driver/jobs') ?>"><i class="fa fa-tasks"></i> Jobs</a></li>
                        <li><a href="<?= site_url('accounts/driver/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
                    </ul>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div>
    <style>
        .modal.right .modal-dialog {
            position: fixed;
            margin: auto;
            width: 320px;
            height: 100%;
            -webkit-transform: translate3d(0%, 0, 0);
            -ms-transform: translate3d(0%, 0, 0);
            -o-transform: translate3d(0%, 0, 0);
            transform: translate3d(0%, 0, 0);
        }


        .modal.right .modal-content {
            height: 100%;
            overflow-y: auto;
        }


        .modal.right .modal-body {
            margin-top: 30px;
        }


        /*Right*/
        .modal.right.fade .modal-dialog {
            right: -320px;
            -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
            -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
            -o-transition: opacity 0.3s linear, right 0.3s ease-out;
            transition: opacity 0.3s linear, right 0.3s ease-out;
        }

        .modal.right.fade.in .modal-dialog {
            right: 0;
        }

        /* ----- MODAL STYLE ----- */
        .modal-content {
            border-radius: 0;
            border: none;
        }

        .modal-header {
            border-bottom-color: #EEEEEE;
            background-color: #FAFAFA;
        }

        .modal-body {
            padding: 0px;
        }

        .modal-body .side-nav {
            border-top: 1px solid #e2e2e2;
        }

        .side-nav li {
            display: block;

        }

        .side-nav li a {
            color: #000;
            text-decoration: none;
            padding: 20px;
            border-bottom: 1px solid #e2e2e2;
        }

        .side-nav li a i {
            padding-right: 10px;
            color: #777;

        }

        .side-nav li a:hover {
            background: #1D96BB;
            color: #fff;
        }

        .side-nav li a:hover i {

            color: #fff;
        }

        .side-nav .active {
            background: #1D96BB;
            color: #fff;

        }

        .side-nav .active a {

            color: #fff;

        }

        .side-nav .active i {

            color: #fdc70f;

        }

        .modal-dialog .modal-content .modal-header .close {
            color: #1d96bb;
            font-size: 24px;
            float: left;

        }

        .dash-nav {
            float: none;
            display: none;
        }
    </style>