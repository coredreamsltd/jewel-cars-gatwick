<main style="    min-height: 85vh;     padding-top: 40px;">
    <section class="dshbd-wpr">
        <div class="dshbd-content">
            <div class="container">
                <?php flash(); ?>
                <div class="dshbd-table dshbd-driver-table">
                    <h2 class="title">Jobs</h2>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped dt-table">
                            <thead>
                                <tr>
                                    <th>Booking Id</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Vehicle</th>
                                    <th>Pickup Date/Time</th>
                                    <th>Final Fare</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($jobs) : ?>
                                    <?php foreach ($jobs as $j) : ?>
                                        <tr>
                                            <td><?= $j->booking_ref_id ?></td>
                                            <td><?= $j->pickup_address ?></td>
                                            <td><?= $j->dropoff_address ?></td>
                                            <td><?= $j->vehicle_name ?></td>
                                            <td> <?= DateTime::createFromFormat('Y-m-d', $j->pickup_date)->format('D, d M Y') . ', ' . date('h:i A', strtotime($j->pickup_time)) ?></td>
                                            <td><?= CURRENCY . $j->driver_fare ?></td>
                                            <td>
                                                <span class="label label-<?= ($j->job_status == 'assigned') ? 'primary' : (($j->job_status == 'rejected') ? 'danger' : (($j->job_status == 'accepted') ? 'success' : 'warning')) ?>">
                                                    <?= $j->job_status ? strtoupper($j->job_status) : 'PENDING' ?>
                                                </span>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <?php if ($j->job_status == 'assigned') : ?>
                                                            <li>
                                                                <a href="#!" onclick="acceptJob('<?= $j->id ?>','<?= $driver->id ?>')"><i class="fa fa-check-square-o text-success"> </i>
                                                                    Accept</a>
                                                            </li>
                                                            <li>
                                                                <a onclick="rejectJob('<?= $j->id ?>','<?= $driver->id ?>')" href="#!"><i class="fa fa-times text-danger"> </i> Reject</a>
                                                            </li>
                                                        <?php endif; ?>
                                                        <li>
                                                            <a href="<?= base_url('accounts/driver/jobDetails/' . $j->id) ?>"><i class="fa fa-eye text-primary"> </i> View Details</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<div class="cstm-modal inr-wpr">
    <div class="modal fade modal-dshbd-vdetail-delete" id="accept-dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <form method="post" action="<?php echo base_url('accounts/driver/acceptJob') ?>">
                        <input type="hidden" name="booking_id">
                        <input type="hidden" name="driver_id">
                        <div class="inr-form temp-login-form fp-title">
                            <h2>Accept Job</h2>
                            <h3>Are you sure you want to accept job. Once you accept it! You cannot revert it back.</h3>
                            <ul class="list-unstyled list-inline">
                                <li>
                                    <button type="submit" class="btn btn-custom hvr-bounce-to-right">Yes</button>
                                </li>
                                <li>
                                    <button type="button" data-dismiss="modal" class="btn btn-custom hvr-bounce-to-right">No
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cstm-modal inr-wpr">
    <div class="modal fade modal-dshbd-vdetail-delete" id="cancle-dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <form method="post" action="<?php echo base_url('accounts/driver/rejectJob') ?>">
                        <input type="hidden" name="booking_id">
                        <input type="hidden" name="driver_id">
                        <div class="inr-form temp-login-form fp-title">
                            <h2>Accept Job</h2>
                            <h3>Are you sure you want to reject job. Once you reject it! You cannot revert it back.</h3>
                            <ul class="list-unstyled list-inline">
                                <li>
                                    <button type="submit" class="btn btn-custom hvr-bounce-to-right">Yes</button>
                                </li>
                                <li>
                                    <button type="button" data-dismiss="modal" class="btn btn-custom hvr-bounce-to-right">No
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.dt-table').dataTable();
    });

    function acceptJob(booking_id, driver_id) {
        $('#accept-dialog input[name="booking_id"]').val(booking_id);
        $('#accept-dialog input[name="driver_id"]').val(driver_id);
        $('#accept-dialog').modal('show');
    }

    function rejectJob(booking_id, driver_id) {
        $('#cancle-dialog input[name="booking_id"]').val(booking_id);
        $('#cancle-dialog input[name="driver_id"]').val(driver_id);
        $('#cancle-dialog').modal('show');
    }
</script>