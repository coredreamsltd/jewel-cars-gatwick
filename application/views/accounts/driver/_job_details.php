<main style="    min-height: 85vh;     padding-top: 40px;">
    <section class="dshbd-wpr">
        <div class="dshbd-content dshbd-booking-detail">
            <div class="container">
                <div class="dshbd-table">
                    <h2 class="title">Booking Detail</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Personal Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td><?= $booking->client_name ?></td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td><?= $booking->client_email ?></td>
                                        </tr>
                                        <tr>
                                            <th>Contact No.</th>
                                            <td><?= $booking->phone_cc . $booking->client_phone ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Journey Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Pickup Address</th>
                                            <td><?= $booking->pickup_address ?><br>
                                                <small>
                                                    <strong>
                                                        <i>
                                                            <?= $booking->start_town_or_city ?>,
                                                            <?= $booking->start_district ?>,
                                                            <?= $booking->start_post_code ?>,
                                                            <?= $booking->start_country ?>
                                                        </i>
                                                    </strong>
                                                </small>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Dropoff Address</th>
                                            <td><?= $booking->dropoff_address ?><br>
                                                <small>
                                                    <strong>
                                                        <i>
                                                            <?= $booking->end_town_or_city ?>,
                                                            <?= $booking->end_district ?>,
                                                            <?= $booking->end_post_code ?>,
                                                            <?= $booking->end_country ?>
                                                        </i>
                                                    </strong>
                                                </small>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Pickup Date/Time</th>
                                            <td>
                                                <?= DateTime::createFromFormat('Y-m-d', $booking->pickup_date)->format('D M d, Y') ?>
                                                (<?= date('h:i A', strtotime($booking->pickup_time)) ?>)

                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Vehicle Type</th>
                                            <td><?= $booking->vehicle_name ?></td>
                                        </tr>
                                        <tr>
                                            <td>No. of Passenger/Luggage</td>
                                            <td><?= $booking->client_passanger_no ?> / <?= $booking->client_luggage ?></td>
                                        </tr>
                                        <tr>
                                            <th>Infant Seat 0-12 months</th>
                                            <td> <?= $booking->infant_seat ?: '0 (Not Required)'; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Child Seat 1-2 Years </th>
                                            <td> <?= $booking->child_seat ?: '0 (Not Required)'; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Child Booster Seat 2-4 Years</th>
                                            <td> <?= $booking->child_booster_seat ?: '0 (Not Required)'; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Booster Seat 4 Years +</th>
                                            <td> <?= $booking->booster_seat ?: '0 (Not Required)'; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?php if (isAirport((array)($booking))) : ?>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Flight Arrival Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Flight Number</td>
                                                <td><?= $booking->flight_number ?></td>
                                            </tr>
                                            <tr>
                                                <td>Flight Arriving From</td>
                                                <td><?= $booking->flight_arrive_from ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Payment Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Booking ID</th>
                                            <td><?= $booking->booking_ref_id ?></td>
                                        </tr>
                                        <tr>
                                            <th>Total Fare</th>
                                            <td><?= CURRENCY . $booking->driver_fare ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Special Instruction</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="2"><?= $booking->message?:'-' ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>