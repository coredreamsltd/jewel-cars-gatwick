<main style="    min-height: 85vh;     padding-top: 40px;">
    <section class="dshbd-wpr">
        <div class="dshbd-title fp-title">
            <div class="container">
                <h1> Dashboard <span>Driver</span></h1>
            </div>
        </div>
        <div class="dshbd-content">
            <div class="container">
                <div class="dshbd-profile-intro">
                    <div class="row">
                        <?php flash(); ?>
                        <div class="col-md-3">
                            <div class="dshbd-profile-img">
                                <figure>
                                    <?php if ($driver->image != null): ?>
                                        <img src="<?= base_url('uploads/driver') . '/' . $driver->image; ?>">
                                    <?php else: ?>
                                        <img src="<?= base_url('uploads/driver/default.png'); ?>">
                                    <?php endif; ?>
                                </figure>
                            </div>
                            <div class="dshbd-edit-profle">
                                <h2>Welcome, <b><?= $driver->fullname; ?></b></h2>
                            </div>
                        </div>
                        <div class="col-md-9">                            
                            <div class="d-tabs">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="data-card blue">
                                            <a href="<?= site_url('accounts/driver/jobs'); ?>">
                                                <span>
                                                    <?= $new_jobs; ?>
                                                </span>
                                                <div class="text-right">
                                                    <i class="fa fa-calendar-plus-o"></i>
                                                </div>
                                                <div class="clearfix"></div>
                                                <h4>New Jobs </h4>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="data-card green">
                                            <a href="<?= site_url('accounts/driver/jobs'); ?>">
                                                <span>
                                                    <?= $total_jobs; ?>
                                                </span>
                                                <div class="text-right">
                                                    <i class="fa fa-calendar-check-o"></i>
                                                </div>
                                                <div class="clearfix"></div>
                                                <h4>Total Jobs History </h4>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
