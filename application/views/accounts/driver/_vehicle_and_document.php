<main>
    <section class="dshbd-wpr">
        <div class="dshbd-title fp-title">
            <div class="container">
                <h1> Document & Vehicle </h1>
            </div>
        </div>
        <div class="dshbd-content">
            <div class="container">
                <?php flash() ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form action="<?= site_url('accounts/driver/updateDriverDocuments/' . $driver->id) ?>" method="post" enctype="multipart/form-data">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Driver Documents</h3>
                                    <span class="pull-right">
                                        <button type="submit" class="btn btn-flat btn-primary"> Save</button>
                                    </span>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <th width="40%">NAT Insurance No.:</th>
                                                        <td>
                                                            <input type="text" name="national_insurance_no" class="form-control" value="<?= $driver->national_insurance_no ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>DVLA Licence No.:</th>
                                                        <td>
                                                            <input type="text" name="dvla_licence_no" class="form-control" value="<?= $driver->dvla_licence_no ?>">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>DVLA Licence Exp.:</th>
                                                        <td><input type="text" name="dvla_licence_expiry_date" class="form-control datepicker" required value="<?= !empty($driver->dvla_licence_expiry_date) && $driver->dvla_licence_expiry_date != "0000-00-00" ? SMD_dateformat($driver->dvla_licence_expiry_date) : '' ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Insurance Name:</th>
                                                        <td>
                                                            <input type="text" name="insurance_name" class="form-control" value="<?= $driver->insurance_name ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Insurance Expiry Date:</th>
                                                        <td>
                                                            <input type="text" name="insurance_expiry_date" class="form-control datepicker" required value="<?= !empty($driver->insurance_expiry_date) && $driver->insurance_expiry_date != "0000-00-00" ? SMD_dateformat($driver->insurance_expiry_date) : '' ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PCO Driver Licence No.:</th>
                                                        <td>
                                                            <input type="text" name="pco_driver_licence_no" class="form-control" value="<?= $driver->pco_driver_licence_no ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PCO Driver Exp.:</th>
                                                        <td>
                                                            <input type="text" name="pco_driver_expiry_date" class="form-control datepicker" required value="<?= !empty($driver->pco_driver_expiry_date) && $driver->pco_driver_expiry_date != "0000-00-00" ? SMD_dateformat($driver->pco_driver_expiry_date) : '' ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PCO Driver Batch:</th>
                                                        <td>
                                                            <input type="text" name="pco_driver_batch" class="form-control" value="<?= $driver->pco_driver_batch ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PCO Licence:</th>
                                                        <td>
                                                            <input type="text" name="pco_licence" class="form-control" value="<?= $driver->pco_licence ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PCO Badge:</th>
                                                        <td>
                                                            <input type="text" name="pco_badge" class="form-control" value="<?= $driver->pco_badge ?>">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <th width="40%">Hire Agreement:</th>
                                                        <td>
                                                            <input type="file" name="file_hire_agreement">
                                                            <?php if ($driver->file_hire_agreement) : ?>
                                                                <a href="<?= base_url('uploads/documents/driver/' . $driver->id . '/' . $driver->file_hire_agreement) ?>" target="_blank">View File</a>
                                                            <?php else : ?>
                                                                <span class="text-danger">- Not Uploaded -</span>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Driver Licence Photo (Front):</th>
                                                        <td>
                                                            <input type="file" name="file_driver_licence_photo_front">
                                                            <?php if ($driver->file_driver_licence_photo_front) : ?>
                                                                <a href="<?= base_url('uploads/documents/driver/' . $driver->id . '/' . $driver->file_driver_licence_photo_front) ?>" target="_blank">View File</a>
                                                            <?php else : ?>
                                                                <span class="text-danger">- Not Uploaded -</span>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Driver Licence Photo (Back/Counterpart):</th>
                                                        <td>
                                                            <input type="file" name="file_driver_licence_photo_back">
                                                            <?php if ($driver->file_driver_licence_photo_back) : ?>
                                                                <a href="<?= base_url('uploads/documents/driver/' . $driver->id . '/' . $driver->file_driver_licence_photo_back) ?>" target="_blank">View File</a>
                                                            <?php else : ?>
                                                                <span class="text-danger">- Not Uploaded -</span>
                                                            <?php endif; ?>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <th>Other Documents:</th>
                                                        <td>
                                                            <input type="file" name="file_other_docs">
                                                            <?php if ($driver->file_other_docs) : ?>
                                                                <a href="<?= base_url('uploads/documents/driver/' . $driver->id . '/' . $driver->file_other_docs) ?>" target="_blank">View File</a>
                                                            <?php else : ?>
                                                                <span class="text-danger">- Not Uploaded -</span>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PCO Driver Batch:</th>
                                                        <td>
                                                            <input type="file" name="file_pco_driver_batch">
                                                            <?php if ($driver->file_pco_driver_batch) : ?>
                                                                <a href="<?= base_url('uploads/documents/driver/' . $driver->id . '/' . $driver->file_pco_driver_batch) ?>" target="_blank">View File</a>
                                                            <?php else : ?>
                                                                <span class="text-danger">- Not Uploaded -</span>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>T&C Signed Copy:</th>
                                                        <td>
                                                            <input type="file" name="file_tc_signed_copy">
                                                            <?php if ($driver->file_tc_signed_copy) : ?>
                                                                <a href="<?= base_url('uploads/documents/driver/' . $driver->id . '/' . $driver->file_tc_signed_copy) ?>" target="_blank">View File</a>
                                                            <?php else : ?>
                                                                <span class="text-danger">- Not Uploaded -</span>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PCO Licence:</th>
                                                        <td>
                                                            <input type="file" name="file_pco_licence">
                                                            <?php if ($driver->file_pco_licence) : ?>
                                                                <a href="<?= base_url('uploads/documents/driver/' . $driver->id . '/' . $driver->file_pco_licence) ?>" target="_blank">View File</a>
                                                            <?php else : ?>
                                                                <span class="text-danger">- Not Uploaded -</span>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PCO Badge</th>
                                                        <td>
                                                            <input type="file" name="file_pco_badge">
                                                            <?php if ($driver->file_pco_badge) : ?>
                                                                <a href="<?= base_url('uploads/documents/driver/' . $driver->id . '/' . $driver->file_pco_badge) ?>" target="_blank">View File</a>
                                                            <?php else : ?>
                                                                <span class="text-danger">- Not Uploaded -</span>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.box-header -->
                        </form>
                    </div><!-- /.box -->
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title">Vehicle Details</h3>
                                <span class="pull-right">
                                    <a href="#!" id="btn-add-vehicle" class="btn btn-flat btn-success"><i class="fa fa-plus"></i> Add New Vehicle</a>
                                </span>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped ">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Vehicle</th>
                                                        <th>Colour</th>
                                                        <th>PHV Licence</th>
                                                        <th>MOT</th>
                                                        <th>Taxi/Private Hire Insurance</th>
                                                        <th>Vehicle Log Book/V5 Page 1</th>
                                                        <th>Road Tax Expiry</th>
                                                        <th>Fleet Make</th>
                                                        <th>Fleet Model</th>
                                                        <th>Fleet Registration No./ Vehicle Number</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($driver_fleets) :
                                                        foreach ($driver_fleets as $key => $df) :
                                                    ?>
                                                            <tr>
                                                                <td><?= ++$key; ?></td>
                                                                <td><?= $df->title ?>
                                                                    </span>
                                                                    <br>
                                                                    <a href="<?= site_url('accounts/driver/deleteVehicle/' . $df->id) ?>" onclick="return confirm('Confirm delete?')" class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                                                </td>
                                                                <td>
                                                                    <form method="post" action="<?= base_url('accounts/driver/changeColor/' . $df->id) ?>">
                                                                        <input type="text" class="form-control" value="<?= $df->color ?>" name="color" placeholder="Colour" /> <br />
                                                                        <button type="submit" class="btn-sm btn-primary">
                                                                            Save
                                                                        </button>
                                                                    </form>
                                                                </td>


                                                                <td>
                                                                    <?php if ($df->file_phv_licence != '') : ?>
                                                                        <a href="<?= base_url('uploads/documents/driver/' . $df->driver_id . '/' . $df->file_phv_licence) ?>" target="_blank">View File</a><br>
                                                                        <?= SMD_dateformat($df->phv_licence_expiry_date) ?>
                                                                        <br>
                                                                        <a href="#!" onclick="edit_expiry_date(<?= $df->id ?>, 'phv_licence_expiry_date', '<?= SMD_dateformat($df->phv_licence_expiry_date) ?>')"><i class="fa fa-edit"></i></a>
                                                                        <button class="btn btn-default" onclick="uploadDialog('<?php echo $df->title ?> - PHV Licence', '<?= $df->phv_licence_expiry_date ?>', 'file_phv_licence_id','phv_licence_expiry_date', '<?php echo $df->id ?>')">
                                                                            Upload New
                                                                        </button>
                                                                    <?php else : ?>
                                                                        <button class="btn btn-default" onclick="uploadDialog('<?php echo $df->title ?> - PHV Licence', '<?= $df->phv_licence_expiry_date ?>', 'file_phv_licence_id','phv_licence_expiry_date', '<?php echo $df->id ?>')">
                                                                            Upload New
                                                                        </button>
                                                                    <?php endif; ?>
                                                                </td>


                                                                <td>
                                                                    <?php if ($df->file_mot != '') : ?>
                                                                        <a href="<?= base_url('uploads/documents/driver/' . $df->driver_id . '/' . $df->file_mot) ?>" target="_blank">View File</a><br>
                                                                        <?= SMD_dateformat($df->mot_expiry_date) ?>
                                                                        <br>
                                                                        <a href="#!" onclick="edit_expiry_date(<?= $df->id ?>, 'mot_expiry_date', '<?= SMD_dateformat($df->mot_expiry_date) ?>')"><i class="fa fa-edit"></i></a>
                                                                        <button class="btn btn-default" onclick="uploadDialog('<?php echo $df->title ?> - MOT', '<?= $df->mot_expiry_date ?>', 'file_mot_id','mot_expiry_date', '<?php echo $df->id ?>')">
                                                                            Upload New
                                                                        </button>
                                                                    <?php else : ?>
                                                                        <button class="btn btn-default" onclick="uploadDialog('<?php echo $df->title ?> - MOT', '<?= $df->mot_expiry_date ?>', 'file_mot_id','mot_expiry_date', '<?php echo $df->id ?>')">
                                                                            Upload New
                                                                        </button>
                                                                    <?php endif; ?>
                                                                </td>


                                                                <td>
                                                                    <?php if ($df->file_taxi_private_hire_insurance != '') : ?>
                                                                        <a href="<?= base_url('uploads/documents/driver/' . $df->driver_id . '/' . $df->file_taxi_private_hire_insurance) ?>" target="_blank">View File</a><br>
                                                                        <?= SMD_dateformat($df->taxi_private_hire_insurance_expiry_date) ?>
                                                                        <br>
                                                                        <a href="#!" onclick="edit_expiry_date(<?= $df->id ?>, 'taxi_private_hire_insurance_expiry_date', '<?= SMD_dateformat($df->taxi_private_hire_insurance_expiry_date) ?>')"><i class="fa fa-edit"></i></a>
                                                                        <button class="btn btn-default" onclick="uploadDialog('<?php echo $df->title ?> - Taxi/Private Hire Insurance', '<?= $df->file_taxi_private_hire_insurance ?>', 'file_taxi_private_hire_insurance_id','taxi_private_hire_insurance_expiry_date', '<?php echo $df->id ?>')">
                                                                            Upload New
                                                                        </button>
                                                                    <?php else : ?>
                                                                        <button class="btn btn-default" onclick="uploadDialog('<?php echo $df->title ?> - Taxi/Private Hire Insurance', '<?= $df->file_taxi_private_hire_insurance ?>', 'file_taxi_private_hire_insurance_id','taxi_private_hire_insurance_expiry_date', '<?php echo $df->id ?>')">
                                                                            Upload New
                                                                        </button>
                                                                    <?php endif; ?>
                                                                </td>

                                                                <td>
                                                                    <?php if ($df->file_vehicle_log != '') : ?>
                                                                        <a href="<?= base_url('uploads/documents/driver/' . $df->driver_id . '/' . $df->file_vehicle_log) ?>" target="_blank">View File</a><br>
                                                                        <button class="btn btn-default" onclick="uploadDialog('<?php echo $df->title ?> - Vehicle Log Book Page 1', '', 'file_vehicle_log_id','', '<?php echo $df->id ?>')">
                                                                            Upload New
                                                                        </button>
                                                                    <?php else : ?>
                                                                        <button class="btn btn-default" onclick="uploadDialog('<?php echo $df->title ?> - Vehicle Log Book Page 1', '', 'file_vehicle_log_id','', '<?php echo $df->id ?>')">
                                                                            Upload New
                                                                        </button>
                                                                    <?php endif; ?>
                                                                </td>

                                                                <td>
                                                                    <?= $df->road_tax_expiry_date ? SMD_dateformat($df->road_tax_expiry_date) : "<span class='label label-primary'>Not Set</span>" ?>
                                                                    <br>
                                                                    <a href="#!" onclick="edit_expiry_date(<?= $df->id ?>, 'road_tax_expiry_date', '<?= SMD_dateformat($df->road_tax_expiry_date) ?>')"><i class="fa fa-edit"></i></a>
                                                                </td>

                                                                <td>
                                                                    <form method="post" action="<?= base_url('accounts/driver/change_fleet_make_model_registration/' . $df->id) ?>">
                                                                        <input type="text" class="form-control" value="<?= $df->fleet_make ?>" name="fleet_make" placeholder="Fleet make name" /> <br />
                                                                        <button type="submit" class="btn-sm btn-primary">
                                                                            Save
                                                                        </button>
                                                                    </form>
                                                                </td>
                                                                <td>
                                                                    <form method="post" action="<?= base_url('accounts/driver/change_fleet_make_model_registration/' . $df->id) ?>">
                                                                        <input type="text" class="form-control" value="<?= $df->fleet_model ?>" name="fleet_model" placeholder="Fleet Model" /> <br />
                                                                        <button type="submit" class="btn-sm btn-primary">
                                                                            Save
                                                                        </button>
                                                                    </form>
                                                                </td>
                                                                <td>
                                                                    <form method="post" action="<?= base_url('accounts/driver/change_fleet_make_model_registration/' . $df->id) ?>">
                                                                        <input type="text" class="form-control" value="<?= $df->fleet_registration_no ?>" name="fleet_registration_no" placeholder="Fleet registration no." /> <br />
                                                                        <button type="submit" class="btn-sm btn-primary">
                                                                            Save
                                                                        </button>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        endforeach;
                                                        ?>

                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!--add-vehicle modal-->
<div class="modal fade" id="add-vehicle-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" id="upload-dialog-title">Add More Vehicles</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <?php
                    if ($fleets) :
                        foreach ($fleets as $fleet) :
                    ?>
                            <tr>
                                <td><img src="<?php echo base_url('uploads/fleet/' . $fleet->img_name); ?>" width="100"></td>
                                <td><?php echo $fleet->title; ?></td>
                                <td>
                                    <form action="<?= site_url('accounts/driver/addVehicle') ?>" method="post">
                                        <input type="hidden" name="driver_id" value="<?= $driver->id ?>">
                                        <input type="hidden" name="fleet_id" value="<?= $fleet->id ?>">
                                        <button type="submit" class="btn btn-success">Add This</button>
                                    </form>
                                </td>
                            </tr>
                    <?php
                        endforeach;
                    endif;
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<!--add-vehicle modal end-->

<!--Upload document-->
<div class="modal fade custom-modal" id="upload-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" id="upload-dialog-title"></h4>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo site_url('accounts/driver/uploadDocument/' . $driver->id) ?>" enctype="multipart/form-data">

                <input type="hidden" name="field_name">
                <input type="hidden" name="field_name_expiry">
                <input type="hidden" name="document_name">
                <input type="hidden" name="df_id">

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-4" style="color: #000;">Add vehicle Document</label>
                        <input type="file" class="col-sm-8" name="document" required="">
                    </div>
                    <div class="form-group" id="exp-date">
                        <label class="col-sm-4" style="color: #000;">Expiry Date</label>
                        <input type="text" class="col-sm-4 datepicker" name="exp_date" required="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Upload document-->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-expdate">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Modify Expiry Date</h4>
            </div>
            <form action="<?= base_url('accounts/driver/updateExpDate/' . $driver->id) ?>" method="post">
                <input type="hidden" name="df_id">
                <input type="hidden" name="new_exp_filed_name">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">New Expiry Date</label>
                        <input id="new-expdate" type="text" name="new_exp_date" class="form-control datepicker" placeholder="New Expiry Date" required="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update New Expiry Date</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
    $(document).ready(function() {
        $('.dt-table').dataTable();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            minDate: 0
        });
        $('#btn-add-vehicle').click(function() {
            $('#add-vehicle-dialog').modal('show');
        });
    });

    function uploadDialog(title, exp_date, file_key, file_key_exp, df_id) {

        $('#upload-dialog h4').html(title);
        $('input[name="field_name"]').val(file_key);
        $('input[name="field_name_expiry"]').val(file_key_exp);

        if (exp_date != '0000-00-00') {
            $('input[name="exp_date"]').val(exp_date);
        }

        $('input[name="document_name"]').val(title);
        $('input[name="df_id"]').val(df_id);

        if (file_key == 'file_vehicle_log_id') {
            $('#exp-date input').attr('disabled', '');
            $('#exp-date').hide();
        } else {
            $('#exp-date input').removeAttr('disabled');
            $('#exp-date').show();
        }

        $('#upload-dialog').modal('show');
    }

    function edit_expiry_date(df_id, expdate_key, expdate_val) {

        $('[name=df_id]').val(df_id);
        $('[name=new_exp_filed_name]').val(expdate_key);

        $('#new-expdate').val(expdate_val);

        $('#modal-expdate').modal();
    }
</script>