<main style="    min-height: 85vh;     padding-top: 40px;">
    <section class="dshbd-wpr">
        <div class="dshbd-title fp-title">
            <div class="container">
                <h1> Dashboard <span>Driver</span></h1>
            </div>
        </div>
        <div class="dshbd-content">
            <div class="container">
                <div class="dshbd-profile-intro">
                    <?php flash(); ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="dshbd-profile-img">
                                <figure>
                                    <?php if ($driver->image): ?>
                                        <img src="<?= base_url('uploads/driver') . '/' . $driver->image; ?>" style="width: 200px; object-fit: cover">
                                    <?php else: ?>
                                        <img src="<?= base_url('uploads/driver/default.png'); ?>">
                                    <?php endif; ?>
                                </figure>
                            </div>
                            <div class="dshbd-edit-profle">
                                <a href="#" class="btn btn-green" data-toggle="modal"
                                   data-target=".modal-dshbd-edit-detail" title="Boston Airport Cheap Car">Edit
                                    Details</a>
                            </div>
                         
                        </div>
                        <div class="col-md-9">
                            <div class="dshbd-profile-detail fp-para">
                                <ul class=" list-unstyled">
                                    <li>
                                        <i class="fa fa-user"> </i> <label>Full Name</label>
                                        <p><?= $driver->name; ?></p>
                                    </li>
                                    <li>
                                        <i class="fa fa-phone"> </i> <label>Phone No.</label>
                                        <p><?= $driver->phone_cc.$driver->mobile; ?></p>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"> </i> <label>E-mail Address</label>
                                        <p><?= $driver->email; ?></p>
                                    </li>

                                    <li>
                                        <i class="fa fa-map-marker"> </i> <label>Address</label>
                                        <p><?= $driver->address; ?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!--Modal Profile Edit -->
<div class="cstm-modal inr-wpr">
    <div class="modal fade modal-dshbd-edit-detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="inr-form">
                        <h2>Edit Details</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <form action="<?= site_url('accounts/driver/updateProfile/' . $driver->id) ?>"
                                      method="post"
                                      enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label>Image:</label>
                                            <input type="file" name="driver_image">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Name:</label>
                                            <input type="text" name="name" value="<?= $driver->name ?>"
                                                   class="form-control ">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Email:</label>
                                            <input type="text" name="email" value="<?= $driver->email ?>"
                                                   class="form-control">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Phone CC:</label>
                                            <input type="text" name="phone_cc" value="<?= $driver->phone_cc ?>"
                                                   class="form-control ">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Mobile:</label>
                                            <input type="text" name="mobile" value="<?= $driver->mobile ?>"
                                                   class="form-control ">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Address:</label>
                                            <input type="text" name="address"
                                                   value="<?= $driver->address ?>"
                                                   class="form-control ">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Password:</label>
                                            <input type="password" name="password"
                                                   value="" placeholder="Leave empty if do not want to change"
                                                   class="form-control ">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <button type="submit" class="btn btn-flat btn-info"> Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>