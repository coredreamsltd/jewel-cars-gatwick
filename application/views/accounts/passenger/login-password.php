<main class="hav-vector worldwide-wrapper">
    <section class="sign-in-wrapper section-gutter">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3">
                    <h1 class="text-center">
                        Login by <?=$data['login_by']?>
                        <?= ($data['login_by']=='email')?$passenger->email:$passenger->phone_cc.' '.$passenger->phone_no ?>
                    </h1>
                    <div class="login-form login-verification">
                        <div class="form group">
                            <label>Enter your password</label>
                            <input type="hidden" class="form-control" name="passenger_id" value="<?= $passenger->id ?>">
                            <input type="password" class="form-control" name="password" required>
                        </div>
                        <button type="button" class="btn-black btn-login-verification">Next</button>
                        <a href="#!" data-toggle="modal" class="float-right modal-trigger forget" data-target="#forgot-password">Forgot Password?</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>