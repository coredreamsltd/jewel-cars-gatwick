<main class=" hav-vector worldwide-wrapper">
    <section class="sign-in-wrapper section-gutter">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3">
                    <h1>2-Step Verification - SMS</h1>
                    <p class="mb-4">Enter the 4-digit code sent to you <?=$passenger->phone_cc.$passenger->phone_no?></p>
                    <div class="login-form">
                        <div class="form group">
                            <label>Verification Code</label>
                            <input type="text" class="form-control" name="code" maxlength="4" required>
                        </div>
                        <button type="button" class="btn-black btn-sms-code-verify">Verify</button>
                       <p>Resend code by <a href="#!">SMS | Voice </a></p>
                       <p>Having trouble? <a href="<?=site_url('help')?>" target="_blank">Get help </a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>