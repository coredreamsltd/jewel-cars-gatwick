<?php $nav = $this->uri->segment(3); ?>
<div class="dash-nav">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav">
                <li class="active"><a href="<?= site_url('accounts/passenger/dashboard') ?>"><i class="fa fa-home"></i>Home</a></li>
                <li><a href="<?= site_url('accounts/passenger/profile') ?>"><i class="fa fa-user"></i>Profile</a></li>
                <li><a href="<?= site_url('accounts/passenger/booking') ?>"><i class="fa fa-book"></i>Booking</a></li>
                <li><a href="<?= site_url('accounts/passenger/booking_history') ?>"><i class="fa fa-history"></i>Booking History</a></li>
                <li><a href="<?= site_url() ?>"><i class="fa fa-calendar-o"></i>Make New Booking</a></li>
                <li><a href="<?= site_url('accounts/passenger/logout') ?>"><i class="fa fa-sign-out"></i>Logout</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</div>