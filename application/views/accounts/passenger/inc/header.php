<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <title><?= SITE_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link href="<?= base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/bootstrap-slider-master/dist/css/bootstrap-slider.min.css">
        <!-- Jquery UI -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- jquery timepicker -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/jt.timepicker/jquery.timepicker.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/slick-carousel/slick/slick.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/slick-carousel/slick/slick-theme.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/rangeslider.js-2.3.0/rangeslider.js-2.3.0/rangeslider.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/master.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/dashboard.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css">
        <script src="<?= base_url() ?>assets/bower_components/jquery/dist/jquery.js"></script>

    </head>
    <body>
        <div class="dash-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 ">
                        <div class="top-acc">
                            <ul class="list-inline">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                                        <i class="fa fa-user"></i><?=$passenger->full_name?>
                                        
                                    </a>
                                    <ul class="dropdown-menu" >
                                        <li><a href="<?= site_url('accounts/passenger/profile') ?>">Profile</a></li>
                                        <li><a href="<?= site_url('accounts/passenger/dashboard') ?>">Dashboard</a></li>
                                        <li><a href="<?= site_url('accounts/passenger/logout') ?>">Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="logo text-center">
                            <a href="<?= site_url() ?>"><?=SITE_NAME?></a>
                        </div>
                    </div>
                    <div class="col-md-4 is-relative">
                        <div class="top-acc">
                            <ul class="list-inline">
                                <li>
                                    <button type="btn btn-primary" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
                                        <span class="fa fa-bars"></span>
                                    </button>

                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-arrow-left"></span></button>

                    </div>

                    <div class="modal-body">

                        <ul class="nav side-nav">
                            <li><a href="<?= site_url('accounts/passenger/dashboard') ?>"><i class="fa fa-home"></i>Home</a></li>
                            <li><a href="<?= site_url('accounts/passenger/profile') ?>"><i class="fa fa-user"></i>Profile</a></li>
                            <li><a href="<?= site_url('accounts/passenger/booking') ?>"><i class="fa fa-book"></i>Current Booking</a></li>
                            <li><a href="<?= site_url('accounts/passenger/booking_history') ?>"><i class="fa fa-history"></i>Booking History</a></li>
                            <li><a href="<?= site_url('') ?>"><i class="fa fa-calendar-o"></i>Make New Booking</a></li>
                            <li><a href="<?= site_url('accounts/passenger/logout') ?>"><i class="fa fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div>
        <style>

            .modal.right .modal-dialog {
                position: fixed;
                margin: auto;
                width: 320px;
                height: 100%;
                -webkit-transform: translate3d(0%, 0, 0);
                -ms-transform: translate3d(0%, 0, 0);
                -o-transform: translate3d(0%, 0, 0);
                transform: translate3d(0%, 0, 0);
            }


            .modal.right .modal-content {
                height: 100%;
                overflow-y: auto;
            }


            .modal.right .modal-body {
                margin-top:30px;
            }


            /*Right*/
            .modal.right.fade .modal-dialog {
                right: -320px;
                -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
                -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
                -o-transition: opacity 0.3s linear, right 0.3s ease-out;
                transition: opacity 0.3s linear, right 0.3s ease-out;
            }

            .modal.right.fade.in .modal-dialog {
                right: 0;
            }

            /* ----- MODAL STYLE ----- */
            .modal-content {
                border-radius: 0;
                border: none;
            }

            .modal-header {
                border-bottom-color: #EEEEEE;
                background-color: #FAFAFA;
            }
            .modal-body{
                padding: 0px;
            }
            .modal-body .side-nav{
                border-top: 1px solid #e2e2e2;
            }
            .side-nav li{
                display: block;

            }
            .side-nav li a{
                color: #000;
                text-decoration:none;
                padding: 20px;
                border-bottom: 1px solid #e2e2e2;
            }
            .side-nav li a i{
                padding-right: 10px;
                color: #777;

            }
            .side-nav li a:hover{
                background:#1D96BB;
                color: #fff;
            }
            .side-nav li a:hover i{

                color: #fff;
            }
            .side-nav .active{
                background:#1D96BB;
                color: #fff;

            }
            .side-nav .active a{

                color: #fff;

            }
            .side-nav .active i{

                color: #fdc70f;

            }
            .modal-dialog .modal-content .modal-header .close {
                color: #1d96bb;
                font-size: 24px;
                float:left;

            }
            .dash-nav{
                float: none;
                display: none;
            }

        </style>





