<?php $this->load->view('frontend/includes/header'); ?>
<main class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="widget">
                    <div class="contact-form journey-details">
                        <?php flash(); ?>
                        <form class="login-from" method="post">
                            <h2>Login To Jewel Cars Gatwick</h2>
                            <div class="form-group">
                                <label>Email</label>
                                <span class="addon"><img src="<?= base_url() ?>assets/images/email.svg"></span>
                                <input type="email" class="form-control" name="email" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <span class="addon"><img src="<?= base_url() ?>assets/images/lock.svg"></span>
                                <input type="password" class="form-control" name="password" placeholder="" required>
                            </div>
                            <button type="submit" class="btn  btn-block btn-main">Login</button>
                        </form>
                        <a href="javascript:void(0)" class="forgot-password">Forgot Password?</a>
                        <hr>
                        <p>Don't have an Account? <a href="<?= site_url('passenger/register') ?>">Sign Up</a></p>
                        <form class="forgot-password-from" style="display: none;" action="<?= site_url('reset-password') ?>" method="post">
                            <h2>Lost Your Password?</h2>
                            <div class="form-group">
                                <label>Email</label>
                                <span class="addon"><img src="<?= base_url() ?>assets/images/email.svg"></span>
                                <input type="email" class="form-control" placeholder="" name="email" required>
                            </div>
                            <input type="hidden" name="type" value="passenger">
                            <button type="submit" class="btn btn-block btn-main">Reset Password</button>
                            <p>Back to <a href="javascript:void(0)" class="login">Login</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $this->load->view('frontend/includes/footer'); ?>