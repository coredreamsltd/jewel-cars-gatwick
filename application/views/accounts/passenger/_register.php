<?php $this->load->view('frontend/includes/header'); ?>
<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="widget">
                    <div class="contact-form journey-details">
                        <?php flash() ?>
                        <form class="login-from" method="post" action="<?= site_url('accounts/index/passenger_register'); ?>" enctype="multipart/form-data">
                            <input type="hidden" name="type" value="personal">
                            <h2>Create an Account</h2>
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <span class="addon"><img src="<?= base_url() ?>assets/images/user.svg"></span>
                                        <input type="text" class="form-control" placeholder="" name="fname" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <span class="addon"><img src="<?= base_url() ?>assets/images/user.svg"></span>
                                        <input type="text" class="form-control" placeholder="" name="lname" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <span class="addon"><img src="<?= base_url() ?>assets/images/email.svg"></span>
                                <input type="email" class="form-control" placeholder="" name="email" required>
                            </div>
                            <div class="form-group">
                                <label>Mobile Number</label>
                                <input type="hidden" name="phone_cc">
                                <input type="number" class="form-control countrycode" placeholder="" name="phone_no">
                                <!-- <span class="addon"><img src="<?= base_url() ?>assets/images/phone2.svg"></span> -->
                            </div>
                            <div class="form-group">
                                <label>Billing Address, Line 1</label>
                                <span class="addon"><img src="<?= base_url() ?>assets/images/location.svg"></span>
                                <input type="text" class="form-control" placeholder="" name="address" required>
                            </div>
                            <div class="form-group">
                                <label>Billing Address, Line 2 (Optional)</label>
                                <span class="addon"><img src="<?= base_url() ?>assets/images/location.svg"></span>
                                <input type="text" class="form-control" placeholder="" name="address_1">
                            </div>
                            <div class="row form-row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>City</label>
                                        <span class="addon"><img src="<?= base_url() ?>assets/images/location.svg"></span>
                                        <input type="text" class="form-control" placeholder="" name="city" required>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Postcode</label>
                                        <span class="addon"><img src="<?= base_url() ?>assets/images/location.svg"></span>
                                        <input type="text" class="form-control" placeholder="" name="post_code" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <span class="addon"><img src="<?= base_url() ?>assets/images/lock.svg"></span>
                                <input type="password" class="form-control" placeholder="" name="password" required>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <span class="addon"><img src="<?= base_url() ?>assets/images/lock.svg"></span>
                                <input type="password" class="form-control" placeholder="" name="confirm_password" required>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="accept-terms" name="" required>
                                <label class="form-check-label" for="accept-terms">I Agree with the
                                    <a href="<?= site_url('terms-and-conditions') ?>">Terms and Conditions</a>.
                                </label>
                            </div>
                            <button type="submit" class="btn  btn-block btn-main">Register</button>
                            <br>
                            <hr>
                            <p>Already have an Account? <a href="<?= site_url('passenger/login') ?>">Sign In</a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('[name=confirm_password]').change(function() {
            var passw = $('[name=password]').val();
            var cmf_passw = $('[name=confirm_password]').val();

            if (passw != cmf_passw) {
                $(this).val('');
                bootbox.alert('Entered confirm password mismatch.');
                return false;
            }

        });
    });
</script>
<?php $this->load->view('frontend/includes/footer'); ?>