<div class="main-body">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-8 col-md-offset-2">
                <div class="dash-m-body">
                    <div class="dash-title">
                        <h2>Profile</h2>
                    </div>
                    <div class="dash-body">
                        <div class="profile">
                            <div class="row">
                                <?php flash(); ?>
                                <div class="col-md-8  col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <h4><i class="fa fa-user"></i> Full Name</h4>
                                                    <p><?= $passenger->full_name ?></p>
                                                </li>
                                                <li>
                                                    <h4><i class="fa fa-mobile"></i> Phone</h4>
                                                    <p><?= $passenger->phone_cc . $passenger->phone_no ?></p>
                                                </li>
                                                <li>
                                                    <h4><i class="fa fa-phone"></i> Tel Phone</h4>
                                                    <p><?= $passenger->tel_phone_cc . $passenger->tel_phone_no ?></p>
                                                </li>
                                                <li>
                                                    <h4><i class="fa fa-envelope-o"></i> Email Address</h4>
                                                    <p><?= $passenger->email ?></p>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <h4><i class="fa fa-map-marker"></i> Address</h4>
                                                    <p><?= $passenger->address ?></p>
                                                </li>
                                                <li>
                                                    <h4><i class="fa fa-map-marker"></i> Address 1</h4>
                                                    <p><?= $passenger->address_1 ?></p>
                                                </li>
                                                <li>
                                                    <h4><i class="fa fa-map-pin"></i> City</h4>
                                                    <p><?= $passenger->city ?></p>
                                                </li>
                                                <li>
                                                    <h4><i class="fa fa-map-signs"></i> Post Code</h4>
                                                    <p><?= $passenger->post_code ?></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <br>
                                    <hr>
                                    <div class="is_flex">
                                        <a href="#" data-toggle="modal" data-target="#edit-profile" class="btn btn-ghost">Edit
                                            Details</a>
                                        <a href="#" data-toggle="modal" data-target="#change-password" class="btn btn-custom">Change Password</a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <br>
                                    <figure>
                                        <?php if (!empty($passenger->image)) : ?>
                                            <img src="<?= base_url('uploads/passenger/' . $passenger->image); ?>">
                                        <?php else : ?>
                                            <img src="<?= base_url('uploads/passenger/default.jpg'); ?>">
                                        <?php endif; ?>
                                    </figure>
                                    <br>
                                    <div class="text-center">
                                        <small>*Use square photo for best results</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="custom-modal">
    <div class="modal fade" id="edit-profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="cancellationLabel">Edit Profile</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="custom-form">
                        <form method="post" action="<?= site_url('accounts/passenger/profile'); ?>" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label">Photo</label>
                                <input type="file" name="image">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Full Name</label>
                                        <input type="text" class="form-control" name="full_name" placeholder="Enter Full Name" value="<?= $passenger->full_name ?>" required="">
                                    </div>
                                </div>
        
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Tel Phone</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="" name="tel_phone_cc" placeholder="Enter Phone CC" value="<?= $passenger->tel_phone_cc ?>" required="" style="height:20px;width:40px">
                                            </span>
                                            <input type="text" class="form-control" name="tel_phone_no" placeholder="Enter Phone" value="<?= $passenger->tel_phone_no ?>" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Phone</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="" name="phone_cc" placeholder="Enter Phone CC" value="<?= $passenger->phone_cc ?>" required="" style="height:20px;width:40px">
                                            </span>
                                            <input type="text" class="form-control" name="phone_no" placeholder="Enter Phone" value="<?= $passenger->phone_no ?>" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Address</label>
                                        <input type="text" class="form-control" placeholder=" Enter Address " name="address" value="<?= $passenger->address ?>" required="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Address 1</label>
                                        <input type="text" class="form-control" placeholder=" Enter Address 1" name="address_1" value="<?= $passenger->address_1 ?>" required="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <input type="text" class="form-control" placeholder=" Enter City " name="city" value="<?= $passenger->city ?>" required="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Post Code</label>
                                        <input type="text" class="form-control" placeholder=" Enter Post Code " name="post_code" value="<?= $passenger->post_code ?>" required="">
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-primary" type="submit">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="custom-modal">
    <div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title text-center">Change Password</h2>
                </div>
                <div class="modal-body">
                    <div class="custom-form">
                        <form method="post">
                            <div class="form-group">
                                <label class="control-label">Old Password</label>
                                <input type="password" class="form-control" placeholder="Enter Old Password" name="old_password" id="register-old-password" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">New Password</label>
                                <input type="password" class="form-control" placeholder="Enter New Password" name="password" id="register-password1" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Confirm Password</label>
                                <input type="password" class="form-control" placeholder="Re-type New Password" name="password2" id="register-password2" required="">
                            </div>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".dshbd-change-pswd-list").find("input").attr("disabled", "disabled");

        $(".dshbd-change-pswd").click(function() {
            if ($(".dshbd-change-pswd").is(":checked")) {
                $(".dshbd-change-pswd-list").find("input").removeAttr("disabled");
            } else {
                $(".dshbd-change-pswd-list").find("input").attr("disabled", "disabled");
            }
        });

        $(".dshbd-change-pswd-list").hide();

        $(".dshbd-change-pswd").click(function() {
            if ($(".dshbd-change-pswd").is(":checked")) {
                $(".dshbd-change-pswd-list").slideDown();
            } else {
                $(".dshbd-change-pswd-list").slideUp();
            }
        });

        $('#register-password2').change(function() {
            var sec1 = $('#register-password1').val();
            var sec2 = $('#register-password2').val();


            if (sec1 !== sec2) {
                bootbox.alert("Password Does not Match. Enter it again!");
                $('#register-password2').val('');
                $('#register-password2').focus();
            }

        });
    });

    $('#register-old-password').change(function() {
        var old_password = $(this).val();
        var email = '<?= $passenger->email ?>';

        if (old_password === '' || email === '')
            return;

        $.ajax({
            type: "POST",
            url: "<?= site_url('accounts/passenger/ajax_checkPassword') ?>",
            data: {
                'password': old_password,
                'email': email
            },
            dataType: "json",
            success: function(response) {
                if (response.isMatch === '0') {
                    bootbox.alert("Password does not matches. Please try again!");
                    $('#register-old-password').val('');
                    $('#register-old-password').focus();
                }
            }
        });

    });
</script>