<div class="main-body">
    <div class="container-fluid">
        <div class="row">
            
            <div class="col-md-12">
                <div class="dash-m-body">
                    <div class="dash-title">
                        <h2><?=ucwords(str_replace('_',' ',segment(3)))?></h2>
                    </div>
                    <div class="dash-body">
                        <div class="table-responsive">
                             <table class="table table-bordered table-striped" id="dt-bookings">
                                        <thead>
                                            <tr>
                                                <th width="2%">#</th>
                                                <th width="5%">BK.ID</th>
                                                <th width="55%">From - To</th>
                                                <th width="20%">Pickup Date & Time </th>
                                                <th width="5%">Status</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                               <?php if ($bookings): ?>
                                    <?php foreach ($bookings as $index=>$b): ?>
                                            <tr>
                                                  <td><?= ++$index; ?></td>
                                                  <td><?= $b->booking_ref_id ?></td>
                                                  <td><?= $b->pickup_address ?> - <?= $b->dropoff_address ?></td>
                                                  
                                                  <td>
                                                       <?= DateTime::createFromFormat('Y-m-d', $b->pickup_date)->format('D, d M Y') ?>
                                                        <?= date('h:i A', strtotime($b->pickup_time)) ?>
                                                  </td>
                                                  <td>
                                                      <span class="badge"><?=$b->booking_status?></span>
                                                  </td>
                                                  <td>
                                                       <a href="<?= base_url('accounts/passenger/booking_details/' . $b->booking_ref_id) ?>" class="btn btn-sm btn-primary">View
                                                            Details
                                                  </td>
                                            </tr>
                                                 <?php endforeach; ?>
                                <?php endif; ?>
                                        </tbody>
                                    </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#dt-bookings').dataTable();
    });

</script>