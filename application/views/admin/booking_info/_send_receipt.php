<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header" style="padding: 10px 0;">
                <h3 class="box-title">Generate Receipt for Invoice Id: <strong><?= segment(4) ?></strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-raise-invoice" method="post">
                            <input type="hidden" name="invoice_id" value="<?= $invoice['invoice_id'] ?>">
                            <input type="hidden" name="booking_id" value="<?= $invoice['booking_id'] ?>">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Receipt?</label>
                                            <div class="input-group col-md-4">
                                                <label class="radio-inline">
                                                    <input type="radio" name="is_send_email" value="1" checked=""> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="is_send_email" value="0"> No
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">
                                                Recipients Email
                                                <a href="#!" data-toggle="tooltip" title="If more than one email please separate emails with a comma (,) but with no space."><i class="fa fa-info-circle"></i></a>
                                            </label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input class="form-control" name="customer_email" value="<?= $invoice['customer_email'] ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Invoice Amount</label>
                                            <div class="input-group col-md-2">
                                                <span class="input-group-addon"><i class="fa fa-gbp"></i> </span>
                                                <input class="form-control number" value="<?= $invoice['invoice_amount'] ?>" disabled="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Date of Payment</label>
                                            <div class="input-group col-md-2">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i> </span>
                                                <input class="form-control all-datepicker" name="date_of_payment" value="<?= $invoice['date_of_payment'] ? $invoice['date_of_payment'] : '' ?>" required>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <label class="control-label col-md-12">Payment Method</label>
                                                <div class="col-md-3">
                                                    <select name="invoice_payment_method" class="form-control"> 
                                                        <option value="card">Card</option>
<!--                                                        <option value="paypal">Paypal</option>-->
                                                        <option value="cash">Cash to driver</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Subject</label>
                                            <div class="input-group col-md-12">
                                                <span class="input-group-addon"><i class="fa fa-pencil"></i> </span>
                                                <input class="form-control" name="mail_subject" value="Payment Receipt for Invoice ID: <?= $invoice['invoice_id'] ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Message</label>
                                            <textarea class="form-control" name="mail_message" id="message" required="">
                                                <?php if ($invoice['booking_id'] && $invoice['cart_id'] && $this->invoice_model->get_due_amount($invoice['booking_id'], $invoice['invoice_amount']) > 0): ?>
                                                    Payment of <?= CURRENCY . $invoice['invoice_amount'] ?> has been received.
                                                <?php else: ?>
                                                    Thank you! Payment of <?= CURRENCY . $invoice['invoice_amount'] ?> has been received.
                                                    <br>
                                                <?php endif; ?>
                                            </textarea>
                                            <script type="text/javascript">
                                                var editor = CKEDITOR.replace('message',
                                                        {
                                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                                        });
                                                CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-search"> </i> Preview Receipt Email</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-raise-invoice">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" style="font-family: Times New Roman !important;">

            </div>
            <div class="modal-footer">
                <form action="" id="form-invoice" name="form_invisible" method="post">
                    <input type="hidden" name="i_invoice_id" value="<?= $invoice['invoice_id'] ?>">
                    <input type="hidden" name="i_booking_id" value="<?= $invoice['booking_id'] ?>">
                    <input type="hidden" name="i_is_send_email">
                    <input type="hidden" name="i_customer_email">
                    <input type="hidden" name="i_date_of_payment">
                    <input type="hidden" name="i_invoice_payment_method">
                    <input type="hidden" name="i_mail_subject_receipt">
                    <input type="hidden" name="i_mail_template_receipt">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitting...'; this.form.submit();">Generate & Email Receipt</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(function () {
        $('#form-raise-invoice').submit(function () {

//          if ($(this).valid()) {

                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();

                $.post('<?= site_url('admin/booking/ajax_preview_receipt') ?>',
                        {'formdata': $(this).serialize()},
                        function (response) {
                            response = jQuery.parseJSON(response);
                            $('#form-invoice [name=i_is_send_email]').val(response.data.is_send_email);
                            $('#form-invoice [name=i_customer_email]').val(response.data.customer_email);
                            $('#form-invoice [name=i_date_of_payment]').val(response.data.date_of_payment);
                            $('#form-invoice [name=i_invoice_payment_method]').val(response.data.invoice_payment_method);
                            $('#form-invoice [name=i_mail_subject_receipt]').val(response.data.mail_subject);
                            $('#form-invoice [name=i_mail_template_receipt]').val(response.data.mail_template);

                            $('#modal-raise-invoice .modal-body').html(response.data.mail_template);
                            $('#modal-raise-invoice .modal-header h4').html("<strong>Mail Subject: </strong>" + response.data.mail_subject);
                            $('#modal-raise-invoice').modal();
                        }
                );
                return false;

//            } else {
//                alert('Please make sure all the required fields are filled properly.');
//            }
        });
    });
</script>
