    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <a href="<?= site_url('admin/booking/view/' . $booking->id) ?>" target="_blank" class="btn btn-block btn-success"> View Booking</a>
            </div>
            <div class="form-group">
                <label>Driver</label>
                <select class="form-control" name="driver_id" data-booking-id='<?= $booking->id ?>' data-booking-ref-id='<?= $booking->booking_ref_id ?>'>
                    <option value="">-Select-</option>
                    <?php if (!empty($drivers)) : foreach ($drivers as $driver) : ?>
                            <option value="<?= $driver->id ?>--|<?= $driver->name ?>" <?= $booking->driver_id == $driver->id ? 'selected' : '' ?>>
                                <?= $driver->name ?>
                            </option>
                    <?php endforeach;
                    endif; ?>
                </select>
            </div>
            <h3>
                Routes <small>[<?= $booking->booking_ref_id ?>]</small>
            </h3>
            <ul class="list-unstyled">
                <li><i class="fa fa-map-marker text-green"></i> <?= $booking->pickup_address ?></li>
                <li><i class="fa fa-map-marker text-red"></i> <?= $booking->dropoff_address ?></li>
                <li><i class="fa fa-car"></i> <?= $booking->vehicle_name ?></li>
            </ul>
            <ul class="list-unstyled">
                <li>
                    <h3>Customer Details</h3>
                </li>
                <li><span>Name: </span> <?= $booking->client_name ?></li>
                <li><span>Email: </span> <?= $booking->client_email ?></li>
                <li><span>Contact No.: </span> <?= $booking->client_phone ?></li>
            </ul>
            <br>
        </div>
        <div class="col-md-12">
            <div id="map" class="map-canvas">
            </div>
        </div>
    </div>