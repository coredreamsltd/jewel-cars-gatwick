<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    Booking list<br><br>
                    <a href="#!" class="btn btn-default" id="btn-search">
                        <i class="fa fa-search"></i> Advanced Search
                    </a>
                </h3>
                <div class="pull-right">
                    <a href="<?= site_url('admin/booking?action=search&status=new') ?>" class="btn btn-danger">
                        NEW<br>
                        <strong><?= $counts['new_jobs'] ?></strong>
                    </a>
                    <a href="<?= site_url('admin/booking?action=search&status=confirmed') ?>" class="btn btn-warning">
                        CONFIRMED<br><strong><?= $counts['confirmed_jobs'] ?></strong>
                    </a>
                    <a href="<?= base_url('admin/booking?action=search&status=job_assigned') ?>" class="btn btn-primary">
                        JOB ASSIGNED<br><strong><?= $counts['driver_assigned_jobs'] ?></strong>
                    </a>
                    <a href="<?= base_url('admin/booking?action=search&status=job_accepted') ?>" class="btn btn-success">
                        JOB ACCEPTED<br><strong><?= $counts['driver_accepted_jobs'] ?></strong>
                    </a>
                    <a href="<?= base_url('admin/booking?action=search&status=job_rejected') ?>" class="btn btn-danger">
                        JOB REJECTED<br><strong><?= $counts['driver_rejected_jobs'] ?></strong>
                    </a>
                    <a href="<?= site_url('admin/booking?action=search&status=completed') ?>" class="btn btn-primary">
                        COMPLETED<br><strong><?= $counts['completed_jobs'] ?></strong>
                    </a>
                    <a href="<?= site_url('admin/booking?action=search&status=cancelled') ?>" class="btn btn-danger">
                        CANCELLED<br><strong><?= $counts['cancelled_jobs'] ?></strong>
                    </a>
                </div>
                <div class="col-md-12">
                    <!-- <div class="bulk-actions pull-left">
                        <div class="form-group">
                            <select name="bulk_action" class="form-control" onchange="performBulkAction(this.value)">
                                <option value="">- Bulk Actions -</option>
                                <option value="invoice_bulk">Send Invoice</option>
                            </select>
                        </div>
                    </div> -->
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" id="form-bulk-action">
                    <table class="table table-bordered table-striped main-booking-list" id="dt-booking">
                        <thead>
                            <tr>
                                <!-- <th><input type="checkbox" name="checkall"></th> -->
                                <th style="width: 3px">#</th>
                                <th width="25%">When & What</th>
                                <th width="20%"> From A to B</th>
                                <th width="20%">Passenger</th>
                                <th width="20%">Driver & Vehicle </th>
                                <th width="15%">Payment </th>
                                <th width="5%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($booking_infos)) : foreach ($booking_infos as $index => $b) : ?>
                                    <?php
                                    if ($b->booking_status == 'not confirmed') {
                                        $color_class = 'task-danger';
                                    } else if ($b->job_status == 'completed') {
                                        $color_class = 'task-primary';
                                    } else if (($b->booking_status == 'confirmed' || $b->booking_status == 'reconfirmed')) {
                                        $color_class = 'task-warning';
                                    } else if ($b->job_status != 'rejected' && $b->job_status != 'completed') {
                                        $color_class = 'task-success';
                                    } ?>
                                    <tr class="<?= $color_class ?>">
                                        <!-- <td><input type="checkbox" name="checked_item[]" value="<?= $b->id ?>"></td> -->
                                        <td><?= ++$index; ?></td>
                                        <td>
                                            <p>
                                                <?= DateTime::createFromFormat('Y-m-d', $b->pickup_date)->format('D, d M Y') . ', ' . date('h:i A', strtotime($b->pickup_time)) ?>
                                            </p>

                                            <p class="text-danger">
                                                <i class="fa fa-<?= $b->journey_type == 'one_way' ? 'arrow-right' : 'refresh' ?>"></i>
                                                <?= triptype_text($b->journey_type) ?>
                                            </p>

                                            <?php if ($b->client_return_date && $b->client_return_time) : ?>
                                                <p>
                                                    <?= DateTime::createFromFormat('Y-m-d', $b->client_return_date)->format('D, d M Y') . ', ' . date('h:ia', strtotime($b->client_return_time)) ?>
                                                </p>
                                            <?php else : ?>

                                            <?php endif; ?>
                                            <?php if (isAirport((array) $b)) : ?>
                                                <strong><i class="fa fa-plane"> </i> <?= $b->flight_number ?> <strong>(<?= $b->flight_name ?>)</strong>
                                            <?php endif; ?>
                                            <p> <strong><?php echo $b->booking_ref_id ?></strong> / (<?= $b->vehicle_name ?>)</p>
                                        </td>
                                        <td>

                                            <p class="route-wrap"><b>A:</b>&nbsp; <?php echo $b->pickup_address ?></p>
                                            <?php if ($b->dropoff_address) : ?>
                                                <p class="route-wrap"><b>B:</b>&nbsp; <?php echo $b->dropoff_address ?></p>
                                            <?php endif; ?>
                                           
                                        </td>

                                        <td>
                                            <p><i class="fa fa-user"></i>&nbsp; <?php echo $b->client_name ?></p>
                                            <p><i class="fa fa-envelope"></i>&nbsp; <?php echo $b->client_email ?></p>
                                            <p><i class="fa fa-phone"></i>&nbsp; <?php echo $b->phone_cc . $b->client_phone ?></p>
                                        </td>
                                        <td>
                                            <p><i class="fa fa-cab"></i>&nbsp; <?= !empty($b->vehicle_name) ? $b->vehicle_name : '' ?></p>
                                            <p><i class="fa fa-user-secret"></i>&nbsp; <?= !empty($b->driver_name) ? $b->driver_name : '<i>-Not assigned-</i>' ?></p>
                                            <p><i class="fa fa-phone"></i>&nbsp; <?= !empty($b->driver_mobile) ? $b->driver_phone_cc . $b->driver_mobile : '<i>-Not assigned-</i>' ?></p>
                                        </td>
                                        <td>
                                            <p class="show-fare">
                                                <strong>
                                                    <i class="fa fa-<?= ($b->pay_method == 'cash' ? 'money' : ($b->pay_method == 'paypal' ? 'paypal' : 'credit-card')) ?>"></i>
                                                </strong>
                                                <?= CURRENCY . $b->total_fare ?><br>
                                            </p>
                                            <?php
                                            $invoices = $this->invoice_model->get_all_by_booking_id($b->booking_ref_id);
                                            if (!$invoices) {
                                                echo '<span class="label label-danger">Invoice Not Raised</span>';
                                            } else if ($invoices) {
                                                $is_all_invoice_paid = false;
                                                $has_unpaid_invoice = false;

                                                foreach ($invoices as $i) {
                                                    if ($i->payment_status) {
                                                        $is_all_invoice_paid = true;
                                                        continue;
                                                    }

                                                    if (!$i->payment_status && !$i->is_cancelled) {
                                                        $is_all_invoice_paid = false;
                                                        $has_unpaid_invoice = true;
                                                        break;
                                                    }
                                                }

                                                if ($is_all_invoice_paid) {
                                                    echo '<span class="label label-success">Paid</span>';
                                                } else if ($has_unpaid_invoice) {
                                                    echo '<br><span class="label label-warning">Invoice raised, Not Paid</span>';
                                                } else {
                                                    echo '<br><span class="label label-danger">Invoice Not Raised</span>';
                                                }
                                            }

                                            ?>

                                        </td>
                                        <td>
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action
                                                    <span class="fa fa-caret-down"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="<?php echo site_url('admin/booking/view/' . $b->id) ?>">
                                                            <i class="fa fa-tasks text-primary"></i> View Details
                                                        </a>
                                                    </li>
                                                    <?php if($this->session->userdata['account_type'] != 3) :?>
                                                        <li>
                                                            <a href="<?php echo site_url('admin/booking/update/' . $b->id) ?>">
                                                            <i class="fa fa-edit text-success"></i> Edit
                                                                </a>
                                                        </li>
                                                        <li>
                                                            <a href="#!" onclick="reBook('<?= $b->id ?>')">
                                                                <i class="fa fa-history text-yellow"></i> Re-Book
                                                            </a>
                                                        </li>
                                                        <?php if ($b->journey_type == 'one_way') : ?>
                                                            <li>
                                                                <a href="#!" onclick="returnBook('<?= $b->id ?>')">
                                                                    <i class="fa fa-refresh text-info"></i> Return-Book
                                                                </a>
                                                            </li>
                                                        <?php endif; ?>
                                                        <li>
                                                            <a href="#!" onclick="assignDriver('<?= $b->id ?>','<?= $b->total_fare ?>');">
                                                                <i class="fa fa-sign-in text-warning"></i>Assign Driver</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= site_url('admin/booking/mark_completed/' . $b->id) ?>" onclick="return confirm('Confirm mark as completed?')">
                                                                <i class="fa fa-check text-success"></i> Completed
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url('admin/booking/cancel/' . $b->id) ?>" onclick="return confirm('Are you sure?')">
                                                                <i class="fa fa-times text-danger"></i> Cancel
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url('admin/booking/delete/' . $b->id) ?>" onclick="return confirm('Are you sure?')">
                                                                <i class="fa fa-trash text-danger"></i> Delete
                                                            </a>
                                                        </li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>


                                        </td>
                                    </tr>
                                <?php
                                endforeach;
                                ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </form>
            </div><!-- /.box-body -->

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->


<div class="modal fade" id="assign-driver" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Assign Driver</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= site_url('admin/booking/assignDriver') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">Set Driver Fare: ( <i class="fa fa-gbp"></i>)</span>
                            <input type="text" name="driver_fare" required class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="booking_id">
                        <label class="form-control-label">Drivers Name:</label>
                        <select class="form-control" name="driver_id">
                            <?php foreach ($drivers as $driver) : ?>
                                <option value="<?= $driver->id ?>"><?= $driver->name . ' -[' . $driver->email . ']' ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="re-book-model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Re-Booking</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= site_url('admin/booking/reBook') ?>" method="post">
                <div class="modal-body row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pick Up Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="pickup_date" required class="form-control datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pick Up Time</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hourglass"></i></span>
                                <input type="text" name="pickup_time" required class="form-control bk-timepicker">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="booking_id">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Re-book</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="return-book-model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Return Booking</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= site_url('admin/booking/returnBook') ?>" method="post">
                <div class="modal-body row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Return Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="pickup_date" required class="form-control datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Return Time</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hourglass"></i></span>
                                <input type="text" name="pickup_time" required class="form-control bk-timepicker">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="booking_id">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Return Book</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-search" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Advanced Search</h4>
            </div>
            <form action="<?= base_url('admin/booking') ?>" method="get">
                <input type="hidden" name="action" value="search">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Booking RefID(s)</label>
                            <input type="text" name="booking_ref_ids" class="form-control" placeholder="Booking RefIDs (comma separated)" value="<?= !empty($_GET['booking_ref_ids']) ? $_GET['booking_ref_ids'] : '' ?>">
                        </div>
                        <div class="col-md-6">
                            <label>Trip Type</label>
                            <select class="form-control" name="journey_type">
                                <option value="">----Select--</option>
                                <option value="one_way" <?= !empty($_GET['journey_type']) && $_GET['journey_type'] == 'one_way' ? 'selected' : '' ?>>One Way</option>
                                <option value="two_way" <?= !empty($_GET['journey_type']) && $_GET['journey_type'] == 'two_way' ? 'selected' : '' ?>>Two Way</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Booked Date (From)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control all-datepicker" name="booked_date_from" value="<?= !empty($_GET['booked_date_from']) ? $_GET['booked_date_from'] : '' ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Booked Date (To)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control all-datepicker" name="booked_date_to" value="<?= !empty($_GET['booked_date_to']) ? $_GET['booked_date_to'] : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Pickup Date (From)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control all-datepicker" name="pickup_date_from" value="<?= !empty($_GET['pickup_date_from']) ? $_GET['pickup_date_from'] : '' ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Pickup Date (To)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control all-datepicker" name="pickup_date_to" value="<?= !empty($_GET['pickup_date_to']) ? $_GET['pickup_date_to'] : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Passenger name</label>
                                <input type="text" class="form-control" name="client_name" value="<?= !empty($_GET['client_name']) ? $_GET['client_name'] : '' ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Booked by name</label>
                                <input type="text" class="form-control" name="booked_by_name" value="<?= !empty($_GET['booked_by_name']) ? $_GET['booked_by_name'] : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Passenger Email</label>
                            <input type="email" name="client_email" class="form-control" value="<?= !empty($_GET['client_email']) ? $_GET['client_email'] : '' ?>">
                        </div>
                        <div class="col-md-6">
                            <label>Driver</label>
                            <select class="form-control" name="driver_id">
                                <option value="">- All -</option>
                                <?php if (!empty($drivers)) : foreach ($drivers as $d) : ?>
                                        <option value="<?= $d->id ?>" <?= !empty($_GET['driver_id']) && $_GET['driver_id'] == $d->id ? 'selected' : '' ?>>
                                            <?= $d->name ?> [<?= $d->email ?>]
                                        </option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label>Booking Status</label><br>
                            <label class="radio-inline">
                                <input type="radio" name="status" value="any" <?= isset($_GET['status']) && $_GET['status'] == 'any' ? 'checked' : '' ?> required> <strong>Any</strong>
                            </label>
                            <label class="radio-inline text-danger">
                                <input type="radio" name="status" value="new" <?= isset($_GET['status']) && $_GET['status'] == 'new' ? 'checked' : '' ?>> <strong>New</strong>
                            </label>
                            <label class="radio-inline text-warning">
                                <input type="radio" name="status" value="confirmed" <?= isset($_GET['status']) && $_GET['status'] == 'confirmed' ? 'checked' : '' ?>> <strong>Confirmed</strong>
                            </label>
                            <label class="radio-inline text-success">
                                <input type="radio" name="status" value="job_assigned" <?= isset($_GET['status']) && $_GET['status'] == 'job_assigned' ? 'checked' : '' ?>> <strong>Job Assigned</strong>
                            </label>
                            <label class="radio-inline text-primary">
                                <input type="radio" name="status" value="completed" <?= isset($_GET['status']) && $_GET['status'] == 'completed' ? 'checked' : '' ?>> <strong>Completed</strong>
                            </label>
                            <label class="radio-inline text-red">
                                <input type="radio" name="status" value="cancelled" <?= isset($_GET['status']) && $_GET['status'] == 'cancelled' ? 'checked' : '' ?>> <strong>Cancelled</strong>
                            </label>
                        </div>
                        <div class="col-md-12">
                            <label>Invoice Status</label><br>
                            <label class="radio-inline">
                                <input type="radio" name="invoice_status" value="raise" <?= isset($_GET['invoice_status']) && $_GET['invoice_status'] == 'raise' ? 'checked' : '' ?>> <strong class="text-success">Raised</strong>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="invoice_status" value="not_raise" <?= isset($_GET['invoice_status']) && $_GET['invoice_status'] == 'not_raise' ? 'checked' : '' ?>> <strong class="text-danger">Not Raised</strong>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary"><i class="fa fa-search"></i> Apply Search Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dt-booking').dataTable();
        $('.bk-timepicker').timepicker({
            timeFormat: 'H:i',
            step: 5,
            disableTouchKeyboard: true,
        });
        $('#btn-search').click(function() {
            $('#modal-search').modal();
        });
    });

    function reBook(booking_id) {
        $('input[name=booking_id]').val(booking_id);
        $('#re-book-model').modal('show');
    }

    function returnBook(booking_id) {
        $('input[name=booking_id]').val(booking_id);
        $('#return-book-model').modal('show');
    }

    function assignDriver(booking_id, total_fare) {
        $('input[name=booking_id]').val(booking_id);
        $('input[name=driver_fare]').val(total_fare);
        $('#assign-driver').modal('show');
    }

    function performBulkAction(bulk_action) {

        if (bulk_action == "")
            return false;

        var no_of_checked = $('[name="checked_item[]"]:checked').length;
        if (no_of_checked < 1) {
            alert('Please check atleast one item.');
            $('[name=bulk_action]').val("");
            return false;
        }

        $('#form-bulk-action').attr('action', '<?= site_url("admin/booking") ?>' + '/' + bulk_action).submit();
    }
</script>