<!-- Small boxes (Stat box) -->
<div class="row">
	<div class="col-md-12">
		<?php flash() ?>
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">
					Booking Details for booking id: <strong><?= $booking->booking_ref_id ?></strong>
				</h3>
				<?php if ($this->session->userdata['account_type'] != 3) : ?>
					<div class="pull-right">
						<a class="btn btn-default" href="<?= base_url('admin/booking/update/' . $booking->id) ?>"><i class="fa fa-edit"></i> Edit Details</a>
					</div>
				<?php endif; ?>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="table-details">
							<h4>Personal Details</h4>
							<table class="table">
								<tbody>
									<tr>
										<th>Name</th>
										<td><?= $booking->client_name ?></td>
									</tr>
									<tr>
										<th>Contact No.</th>
										<td><?= $booking->phone_cc . $booking->client_phone ?></td>
									</tr>
									<tr>
										<th>Email</th>
										<td><?= $booking->client_email ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="table-details">
							<h4>Travel Details</h4>
							<table class="table">
								<tbody>
									<!-- <tr>
                                        <th>Trip Type</th>
                                        <td><?= triptype_text($booking->journey_type) ?></td>
                                    </tr> -->
									<tr>
										<th>Pickup Date / Time</th>
										<td>
											<?= DateTime::createFromFormat('Y-m-d', $booking->pickup_date)->format('l, d M Y') ?>
											<?= date('h:i A', strtotime($booking->pickup_time)) ?>
										</td>
									</tr>
									<tr>
										<th>From</th>
										<td>
											<?= $booking->pickup_address ?><br>
											<small>
												<strong>
													<i>
														<?= $booking->start_town_or_city ?>,
														<?= $booking->start_district ?>,
														<?= $booking->start_post_code ?>,
														<?= $booking->start_country ?>
													</i>
												</strong>
											</small>
										</td>
									</tr>

									<?php if ($booking->via_point) : ?>
										<tr>
											<th>Via Points</th>
											<td>
												<?php foreach (json_decode($booking->via_point) as $via) : ?>
													-<?= $via ?><br>
												<?php endforeach; ?>
											</td>
										</tr>
									<?php endif; ?>
									<tr>
										<th>To</th>
										<td>
											<?= $booking->dropoff_address ?><br>
											<small>
												<strong>
													<i>
														<?= $booking->end_town_or_city ?>,
														<?= $booking->end_district ?>,
														<?= $booking->end_post_code ?>,
														<?= $booking->end_country ?>
													</i>
												</strong>
											</small>
										</td>
									</tr>
									<tr>
										<th>Vehicle</th>
										<td><?= $booking->vehicle_name ?></td>
									</tr>
									<tr>
										<th>Passenger/Suitcase/Hand Luggage</th>
										<td><?= $booking->client_passanger_no . ' / ' . $booking->client_luggage . ' / ' . $booking->client_hand_luggage ?></td>
									</tr>
									<tr>
										<th>Infant Seat 0-12 months</th>
										<td> <?= $booking->infant_seat ?: '0 (Not Required)'; ?></td>
									</tr>
									<tr>
										<th>Child Seat 1-2 Years </th>
										<td> <?= $booking->child_seat ?: '0 (Not Required)'; ?></td>
									</tr>
									<tr>
										<th>Child Booster Seat 2-4 Years</th>
										<td> <?= $booking->child_booster_seat ?: '0 (Not Required)'; ?></td>
									</tr>
									<tr>
										<th>Booster Seat 4 Years +</th>
										<td> <?= $booking->booster_seat ?: '0 (Not Required)'; ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<?php if (isAirport((array) $booking)) : ?>
							<div class="table-details">
								<h4>Flight Arrival Details</h4>
								<table class="table">
									<tbody>
										<tr>
											<th>Flight No.</th>
											<td><?= $booking->flight_number ?></td>
										</tr>
										<tr>
											<th>Flight Arriving From</th>
											<td><?= $booking->flight_arrive_from ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php endif; ?>
						<div class="table-details">
							<h4>Other Details</h4>
							<table class="table">
								<tbody>
									<tr>
										<th>Special Instruction</th>
										<td><?= $booking->message ?: '-' ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="table-details">
							<h4>Booking Status</h4>
							<table class="table">
								<tbody>
									<tr>
										<th>Is Confirmed?</th>
										<td>
											<?php if ($booking->booking_status == 'confirmed' || $booking->booking_status == 'reconfirmed') : ?>
												<a href="<?= base_url('admin/booking/preview_confirm/' . $booking->id) ?>" target="_blank">
													<label class="label label-success"> YES</label>
													<a href="<?= base_url('admin/booking/resend_confirm_booking/' . $booking->id) ?>" class="btn btn-sm btn-default"><i class="fa fa-plane"></i> Re-Send Confirm
														Booking
													</a>
												</a>
											<?php else : ?>
												<label class="label label-danger">NO</label>
											<?php endif; ?>
											<?php if ($this->session->userdata['account_type'] != 3) : ?>
												<!-- <a href="<?= base_url('admin/booking/confirm_booking?booking_id=' . $booking->id) ?>" class="btn btn-sm btn-default"><i class="fa fa-check"></i> Confirm
                                                    Booking</a> -->
											<?php endif ?>
										</td>
									</tr>
									<tr>
										<th>Is Assigned?</th>
										<td>
											<?php if ($booking->job_status && $booking->driver_id) : ?>
												<label class="label label-<?= $booking->job_status == 'assigned' ? 'primary' : ($booking->job_status == 'accepted' ? 'success' : 'danger') ?>"> <?= strtoupper($booking->job_status) ?></label>
												<a href="<?= site_url('admin/driver/details/' . $booking->driver_id) ?>" target="_blank"><i class="fa fa-user-secret"></i> <?= $driver->name ?></a>
												(Driver fare: <?= CURRENCY . $booking->driver_fare ?>)
											<?php else : ?>
												<label class="label label-warning">Not assigned</label>
											<?php endif; ?>

											<?php if ($this->session->userdata['account_type'] != 3) : ?>
												<button class="btn btn-sm btn-default" onclick="assignDriver('<?= $booking->id ?>','<?= $booking->total_fare ?>');">
													<i class="fa fa-sign-in"></i> <?= $booking->driver_id ? 'Re-' : '' ?>Assign
												</button>
											<?php endif; ?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="table-details">
							<h4>Payment Details</h4>
							<table class="table">
								<tbody>
									<tr>
										<th>Journey Fare</th>
										<td>
											<?= CURRENCY . $booking->total_fare ?>
											<?php if ($this->session->userdata['account_type'] != 3) : ?>
												<a href="#!" onclick="modal_edit_final_fare()"><i class="fa fa-edit"></i>
													Edit</a> <?php endif; ?>
											<br>
											<span class="text-success"><?= $booking->final_fare_edit_reason ?: '' ?></span>
										</td>
									</tr>
									<tr>
										<th>Discount Coupon Used</th>
										<td><?= ($booking->discount_coupon_no) ?: '-' ?></td>
									</tr>
									<tr>
										<th>Payment Type</th>
										<td><?= $booking->pay_method ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<?php if ($this->session->userdata['account_type'] != 3) : ?>
							<div class="table-details">
								<h4>Payment Request Details</h4>
								<table class="table">
									<tbody>
										<tr>
											<th>Invoice Id</th>
											<th>Amount</th>
											<th>Payment Method</th>
											<th>Payment</th>
											<th>Invoice/Receipt Mailed?</th>
											<th>Actions</th>
										</tr>
										<?php if ($invoices) : foreach ($invoices as $i) : ?>
												<tr>
													<td><?= $i->invoice_id ?></td>
													<td>
														<?php if (!$i->partial_amount || $i->partial_amount == $i->invoice_amount) : ?>
															<?= CURRENCY . $i->invoice_amount ?>
														<?php else : ?>
															<?= CURRENCY . $i->partial_amount ?> / <?= CURRENCY . $i->invoice_amount ?>
														<?php endif; ?>
													</td>
													<td><?= strtoupper(str_ireplace("_", " ", $i->invoice_payment_method)) ?></td>
													<?php if ($i->is_cancelled) : ?>
														<td><span class="label label-danger">Invoice Cancelled</span></td>
														<td>&nbsp;</td>
														<td><a href="<?= base_url('admin/booking/invoice/' . $i->invoice_id) ?>" target="_blank"><i class="fa fa-search"></i> Preview</a></td>
													<?php else : ?>
														<td>
															<?php if (!$i->payment_status) : ?>
																<span class="col-sm-12 label label-danger">Not Paid</span>
															<?php else : ?>
																<span class="col-sm-12 label label-success">Paid</span>
															<?php endif; ?>
														</td>
														<td>
															<?php if ($i->invoice_mailed_datetime > 0) : ?>
																<a data-toggle="tooltip" data-placement="top" title="<?= $i->invoice_mailed_datetime ?>"><i class="fa fa-check-circle text-success"></i></a>
															<?php else : ?>
																<i class="fa fa-times-circle text-danger"></i>
															<?php endif; ?>
															/
															<?php if ($i->receipt_mailed_datetime > 0) : ?>
																<a data-toggle="tooltip" data-placement="top" title="<?= $i->receipt_mailed_datetime ?>"><i class="fa fa-check-circle text-success"></i></a>
															<?php else : ?>
																<i class="fa fa-times-circle text-danger"></i>
															<?php endif; ?>
														</td>
														<td>
															<div class="btn-group">
																<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Action <span class="caret"></span>
																</button>
																<ul class="dropdown-menu dropdown-menu-right">
																	<?php if (!$i->payment_status) : ?>
																		<li>
																			<a href="<?= base_url('admin/booking/invoice/' . $i->invoice_id) ?>" target="_blank"><i class="fa fa-search"></i> Preview
																				Payment Request</a>
																		</li>
																		<li><a href="#!" onclick="modal_resend_invoice('<?= $i->invoice_id ?>')"><i class="fa fa-refresh"></i> Resend Payment
																				Request</a></li>
																		<li>
																			<a href="<?= base_url('admin/booking/mark_paid/' . $i->invoice_id) ?>" onclick="return confirm('Do you really want to mark this invoice as paid?')"><i class="fa fa-check"></i> Mark as Paid</a>
																		</li>
																		<li>
																			<a href="<?= base_url('admin/booking/cancel_invoice/' . $i->invoice_id) ?>" onclick="return confirm('Are you sure to cancel?')" class="text-danger"><i class="fa fa-times"></i>
																				Cancel</a>
																		</li>
																	<?php elseif ($i->payment_status && !$i->mail_template_receipt) : ?>
																		<li>
																			<a href="<?= base_url('admin/booking/send_receipt/' . $i->invoice_id) ?>"><i class="fa fa-ticket"></i> Send Receipt</a>
																		</li>

																	<?php elseif ($i->payment_status && $i->mail_template_receipt) : ?>
																		<li>
																			<a href="<?= base_url('admin/booking/receipt/' . $i->invoice_id) ?>" target="_blank"><i class="fa fa-search"></i> Preview
																				Receipt</a>
																		</li>
																		<li>
																			<a href="<?= base_url('admin/booking/resend_receipt/' . $i->invoice_id) ?>"><i class="fa fa-refresh"></i> Resend
																				Receipt</a>
																		</li>
																	<?php endif; ?>
																	<?php if (!$i->payment_status) : ?>
																		<li><a href="#!" class="btn-invoice-edit" data-invoice-id="<?= $i->invoice_id ?>"><i class="fa fa-edit"></i> Edit</a></li>
																	<?php endif; ?>
																</ul>
															</div>
														</td>
													<?php endif; ?>

												</tr>
											<?php
											endforeach;
										else :
											?>
											<tr>
												<td colspan="6" class="text-danger">No invoice generated yet.</td>
											</tr>
										<?php endif; ?>

										<tr>
											<th>Amount Due:</th>
											<td colspan="5">
												<span class="text-danger"><?= CURRENCY . $amount_due ?></span>
												<?php if ($amount_due > 0  && ($this->session->userdata['account_type'] != 3)) : ?>
													<div class="pull-right">
														<a href="<?= base_url('admin/booking/raise_invoice?booking_id=' . $booking->booking_ref_id) ?>" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane"></i>
															Request Payment</a>
														<!--     <a href="<?/*= base_url('admin/booking/receipt_manual/' . $booking->booking_ref_id) */ ?>"
                                                   class="btn btn-sm btn-success"><i class="fa fa-money"></i> Add
                                                    Payment</a>-->
													</div>
												<?php endif; ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php endif; ?>


						<div class="table-details">
							<h4>Miscellaneous Information</h4>
							<?php
							$user_agent_data = json_decode($booking->user_agent_data);
							if ($user_agent_data) :
							?>

								<table class="table">
									<tbody>
										<tr>
											<th>Browser</th>
											<td><?= $user_agent_data->Browser . '(' . $user_agent_data->Version . ')' ?></td>
										</tr>
										<tr>
											<th>OS</th>
											<td><?= $user_agent_data->OS ?></td>
										<tr>
											<th>Mobile Device</th>
											<td><?= $user_agent_data->{'Mobile Device'} ?></td>
										</tr>
										<tr>
											<th>IP Address</th>
											<td><?= $user_agent_data->{'IP Address'} ?></td>
										</tr>
									</tbody>
								</table>
							<?php else : ?>
								<p class="text-center">None</p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
</div><!-- /.row -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-edit-final-fare">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Edit Final Fare</h4>
			</div>
			<form action="<?= base_url('admin/booking/edit_final_fare') ?>" method="post">
				<input type="hidden" name="booking_id" value="<?= $booking->id ?>">
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label class="control-label">New Final Fare</label>
								<div class="input-group">
									<span class="input-group-addon"><?= CURRENCY ?></span>
									<input class="form-control" type="text" name="total_fare" value="<?= $booking->total_fare ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">Reason for edit</label>
						<textarea class="form-control" name="final_fare_edit_reason">(Prev Fare:<?= CURRENCY . $booking->total_fare ?>) Changed because ...</textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-invoice-edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Edit Invoice/Receipt</h4>
			</div>
			<form action="<?= site_url('admin/booking/invoice_edit') ?>" method="post">
				<input type="hidden" name="invoice_id">
				<div class="modal-body">
					<div class="form-group">
						<label>Payment Method</label>
						<select name="invoice_payment_method" class="form-control" required="">
							<option value="">- select -</option>
							<option value="credit-card">Credit Card</option>
							<option value="cash">Cash to driver</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-preview-resend-invoice">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="post" action="<?= base_url('admin/booking/resend_invoice') ?>" id="form-preview-resend-invoice">
				<input type="hidden" name="invoice_id">
				<input type="hidden" name="mail_template">

				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" onclick="submit_resend_invoice()">Resend</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="assign-driver" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Assign Driver</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?= site_url('admin/booking/assignDriver') ?>" method="post">
				<div class="modal-body">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">Set Driver Fare: ( <i class="fa fa-gbp"></i>)</span>
							<input type="text" name="driver_fare" required class="form-control">
						</div>
					</div>
					<div class="form-group">
						<input type="hidden" name="booking_id">
						<label class="form-control-label">Drivers Name:</label>
						<select class="form-control" name="driver_id">
							<?php foreach ($drivers as $driver) : ?>
								<option value="<?= $driver->id ?>"><?= $driver->name . ' -[' . $driver->email . ']' ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary"> Assign</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	var SITE_URL = '<?= site_url() ?>';
	$(document).ready(function() {
		$('.btn-invoice-edit').click(function() {

			var invoice_id = $(this).attr('data-invoice-id');
			$.ajax({
				url: SITE_URL + "admin/booking/ajax_get_invoice_details",
				dataType: 'json',
				methode: 'get',
				data: {
					invoice_id: invoice_id
				},
				success: function(response) {

					if (!response.status) {
						alert(response.msg);
						return;
					}

					var modal_invoice_edit = $('#modal-invoice-edit');
					modal_invoice_edit.find('[name=invoice_id]').val(response.data.invoice_id);
					modal_invoice_edit.find('[name=invoice_payment_method]').val(response.data.invoice_payment_method);
					modal_invoice_edit.modal();

				},
				error: function() {
					alert('Server error occured.');
				}
			});


		});
	});

	function assignDriver(booking_id, total_fare) {
		$('input[name=booking_id]').val(booking_id);
		$('input[name=driver_fare]').val(total_fare);
		$('#assign-driver').modal('show');
	}

	function modal_edit_final_fare() {
		$('#modal-edit-final-fare').modal();
	}

	function submit_resend_invoice() {
		var preview_modal = $("#modal-preview-resend-invoice");
		preview_modal.find('[name=mail_template]').val(preview_modal.find('.modal-body iframe').contents().find('body').html());
		preview_modal.find('form').submit();
	}

	function modal_resend_invoice(invoice_id) {

		var preview_modal = $('#modal-preview-resend-invoice');

		preview_modal.find('[name=invoice_id]').val(invoice_id);

		$.post(
			'<?= base_url('admin/booking/ajax_preview_resend_invoice') ?>', {
				invoice_id: invoice_id
			},
			function($response) {
				$response = $.parseJSON($response);
				console.log($response);
				$("#modal-preview-resend-invoice .modal-body").html('');
				var $iframe = $("<iframe style='width:100%; height:600px'></iframe>").appendTo("#modal-preview-resend-invoice .modal-body");
				var iframe = $iframe[0];
				var doc = iframe.document;
				var content = $response.data.mail_template;
				if (iframe.contentDocument) {
					doc = iframe.contentDocument;
				} else if (iframe.contentWindow) {
					doc = iframe.contentWindow.document;
				}
				doc.open();
				doc.writeln(content);
				doc.close();

				var editable = doc.getElementById('mail-message');

				if (editable) {
					CKEDITOR.inline(editable);
				}

				preview_modal.modal();

			}
		);

	}
</script>
