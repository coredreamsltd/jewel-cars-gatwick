<script>
    $(document).ready(function() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 55.0844622,
                lng: -2.7280502
            },
            scrollwheel: true,
            zoom: 4,
            mapTypeControl: false,
            streetViewControl: false

        });
        var stop_point = <?= (!empty($booking->via_point)) ? json_encode($booking->via_point) : "''" ?>;
        var way_point_lat = <?= !empty($booking->way_point_lat) ? json_encode($booking->way_point_lat) : "''" ?>;
        var way_point_lng = <?= !empty($booking->way_point_lng) ? json_encode($booking->way_point_lng) : "''" ?>;
        var start = "<?= !empty($booking->pickup_address) ? $booking->pickup_address : '' ?>";
        var start_lat = "<?= !empty($booking->start_lat) ? $booking->start_lat : '' ?>";
        var start_lng = "<?= !empty($booking->start_lng) ? $booking->start_lng : '' ?>";
        var end = "<?= !empty($booking->dropoff_address) ? $booking->dropoff_address : '' ?>";
        var end_lat = "<?= !empty($booking->end_lat) ? $booking->end_lat : '' ?>";
        var end_lng = "<?= !empty($booking->end_lng) ? $booking->end_lng : '' ?>";
        var polyline_data = <?= !empty($booking->polyline_map_data) ? $booking->polyline_map_data : "null" ?>;

        if ((null !== polyline_data) == true) {
            mapsApi_route(document.getElementById('map'), start, start_lat, start_lng, end, end_lat, end_lng, polyline_data, stop_point, way_point_lat, way_point_lng);
            // google.maps.event.trigger(map, 'resize');
            // setMapBounds(map, polyline_data);
        }


    });


    function mapsApi_route(map_div, start, start_lat, start_lng, end, end_lat, end_lng, polyline_map_data, stop_point, way_point_lat, way_point_lng) {
        var start_lat_lng = {
            lat: parseFloat(start_lat),
            lng: parseFloat(start_lng)
        };
        var end_lat_lng = {
            lat: parseFloat(end_lat),
            lng: parseFloat(end_lng)
        };
        var way_point_lat_lng = [];
        if (way_point_lat !== '') {

            $.each(way_point_lat, function(index, lat) {
                way_point_lat_lng[index] = {
                    lat: parseFloat(lat),
                    lng: parseFloat(way_point_lng[index])
                };
            });
        }
        // console.log(way_point_lat_lng);

        map = new google.maps.Map(map_div, {
            mapTypeControl: false,
            streetViewControl: false,
            zoom:3
        });
        // Draw polyline
        var rideCoordinates = google.maps.geometry.encoding.decodePath(polyline_map_data.overview_polyline.points);
        var ridePath = new google.maps.Polyline({
            map: map,
            path: rideCoordinates,
            strokeColor: "#518191",
            strokeOpacity: 1,
        });
        // set map bounds
        setMapBounds(map, polyline_map_data);

        var markerA = new google.maps.Marker({
            map: map,
            position: start_lat_lng,
            icon: '<?= base_url('assets/images/start.png'); ?>'
        });
        var infowindowA = new google.maps.InfoWindow({
            content: '<strong>FROM: </strong>' + start
        });
        markerA.addListener('click', function() {
            infowindowA.open(map, markerA);
        });
        var markerB = new google.maps.Marker({
            map: map,
            position: end_lat_lng,
            icon: '<?= base_url('assets/images/end.png'); ?>'
        });
        var infowindowB = new google.maps.InfoWindow({
            content: '<strong>TO: </strong>' + end
        });
        markerB.addListener('click', function() {
            infowindowB.open(map, markerB);
        });

        if (way_point_lat_lng !== '') {
            $.each(way_point_lat_lng, function(index, waypoint) {

                var marker = new google.maps.Marker({
                    map: map,
                    position: waypoint,
                    icon: '<?= base_url('assets/images/via.png'); ?>'
                });
                var infowindow = new google.maps.InfoWindow({
                    content: '<strong>VIA: </strong>' + stop_point[index]
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });

            });
        }


    }

    function setMapBounds(map, polyline_map_data) {
        var ne = new google.maps.LatLng(polyline_map_data.bounds.northeast.lat, polyline_map_data.bounds.northeast.lng);
        var sw = new google.maps.LatLng(polyline_map_data.bounds.southwest.lat, polyline_map_data.bounds.southwest.lng);
        map.fitBounds(new google.maps.LatLngBounds(sw, ne));
    }
</script>