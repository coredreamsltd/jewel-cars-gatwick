<div class="box">
    <div class="box-header">
        <h3 class="box-title">Booking Diary</h3>
        <div class="pull-right">
            <ul class="list-inline">
                <li>
                    <span class="text-yellow"><i class="fa fa-square"></i></span> Pending
                </li>
                <li>
                    <span class="text-red"><i class="fa fa-square"></i></span> Cancelled/Rejected
                </li>
                <li>
                    <span class="text-green"><i class="fa fa-square"></i></span> Assigned/Accepted
                </li>
                <li>
                    <span class="text-blue"><i class="fa fa-square"></i></span> Completed
                </li>
            </ul>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div id="calendar-job"></div>
            </div>
        </div>
    </div>
</div>

<!-- Over view modal section-->
<div class="modal right fade booking-detail-modal" id="view-action-list" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Booking Information </h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<script>
    var SITE_URL = '<?= site_url() ?>';
    $(document).ready(function() {
        $('#calendar-job').fullCalendar({
            schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
            customButtons: {
                datePickerJobButton: {
                    text: 'calendar',
                    click: function() {
                        $("input#datepicker").remove(); //dynamically appended every time on custom button click
                        var $btnCustom = $('.fc-datePickerJobButton-button'); // name of custom  button in the generated code
                        $btnCustom.after('<input type="text" id="datepicker"/>');

                        $('#datepicker').datepicker().on('changeDate', function(e) {
                            var date = getDateFormat(e.date);
                            $('#calendar-job').fullCalendar('gotoDate', date);
                        });

                        var $btnDatepicker = $("#calendar-job .ui-datepicker-trigger"); // name of the generated datepicker UI
                        //Below are required for manipulating dynamically created datepicker on custom button click
                        $("#calendar-job #datepicker").show().focus().hide();
                        $btnDatepicker.trigger("click"); //dynamically generated button for datepicker when clicked on input textbox
                        $btnDatepicker.hide();
                        $btnDatepicker.remove();
                    }
                }
            },
            header: {
                left: 'datePickerJobButton prev today next',
                center: 'title',
                right: 'timelineDay,timelineWeek,timelineMonth'
            },
            resourceLabelText: 'Job(s)',
            resourceColumns: [{
                width: 200
            }],
            defaultView: 'timelineDay',
            views: {
                timelineDay: {
                    slotDuration: '00:10:00'
                },
                timelineWeek: {
                    slotDuration: '24:00:00'
                },
                timelineMonth: {
                    slotDuration: '24:00:00'
                }
            },
            editable: false,
            height: 500,
            refetchResourcesOnNavigate: true,
            dayClick: function(datetime) {
                var date = datetime.format('DD/MM/YYYY');
                var time = datetime.format('h:mma');
                addBooking(date, time);
            },
            resources: function(callback) {
                setTimeout(function() {
                    var view = $('#calendar-job').fullCalendar('getView');
                    $.ajax({
                        url: '<?= site_url('admin/booking/ajax_get_resources') ?>',
                        dataType: 'json',
                        cache: false,
                        asyc: false,
                        data: {
                            start: view.start.format(),
                            end: view.end.format(),
                        }
                    }).then(function(resources) {
                        callback(resources)
                    });
                }, 0);
            },
            resourceRender: function(resourceObj, labelTds, bodyTds) {
                if (resourceObj.id === 'none') {
                    html = '<div><div class="fc-cell-content" style="height: 350px;"><span class="fc-expander-space"><span class="fc-icon"></span></span>' +
                        '<div class="media-body"><i class="fa fa-book"></i> ' +
                        ' <span>' + resourceObj.title + '</span>' +
                        '</div></div></div>';
                    labelTds.html(html);
                } else {
                    html = '<div><div class="fc-cell-content"><span class="fc-expander-space"><span class="fc-icon"></span></span>' +
                        '<div class="media-body">';
                    html += '<i class="fa fa-book"></i> ';

                    html += resourceObj.booking_ref_id +
                        ', <i class="fa fa-clock-o"></i> ' + resourceObj.pickup_time +
                        '</div></div></div>';
                    labelTds.html(html);
                    labelTds.attr("onclick", "getBookingView(" + resourceObj.id + ")");
                    if (resourceObj.is_cancelled == 1) {
                        labelTds.find('.fc-cell-content').addClass('red');
                    } else if (resourceObj.job_status == '') {
                        labelTds.find('.fc-cell-content').addClass('yellow');
                    } else if (resourceObj.job_status == 'completed') {
                        labelTds.find('.fc-cell-content').addClass('green');
                    } else if (resourceObj.job_status == 'assigned' || resourceObj.job_status == 'accepted') {
                        labelTds.find('.fc-cell-content').addClass('blue');
                    } else {
                        labelTds.find('.fc-cell-content').addClass('red');
                    }
                }
            },
            events: '<?= site_url('admin/booking/ajax_get_events') ?>',
            eventRender: function(event, element, view) {
                if (event.booking_type === 'bid') {
                    element.find('.fc-content').attr("onclick", "performAction(" + event.id + ")");
                } else {
                    element.find('.fc-content').attr("onclick", "performAction(" + event.id + ")");
                }
                var html = event.booking_ref_id + '<br>' + event.pickup_time;
                element.find('.fc-title').html(html);

                element.find('.fc-time').html('<i class="fa fa-book"></i>');

                if (event.is_cancelled == 1) {
                    element.addClass('cst-bg-red');
                } else if (event.job_status == '') {
                    element.addClass('cst-bg-yellow');
                } else if (event.job_status == 'completed') {
                    element.addClass('cst-bg-green');
                } else if (event.job_status == 'assigned' || event.job_status == 'completed') {
                    element.addClass('cst-bg-blue');
                } else {
                    element.addClass('cst-bg-red');
                }

            },
            eventMouseover: function(event, jsEvent, view) {
                var fa = '';
                fa = '<i class="fa fa-book"></i> ';

                var desc = '<div>' + fa +
                    '<strong> ' + event.booking_ref_id + '</strong>' +
                    '<br><i class="fa fa-user"></i><strong> ' + event.booked_by + '</strong>' +
                    '<br><i class="fa fa-envelope"></i><strong> ' + event.email + '</strong>' +
                    '<br><span> <i class="fa fa-map-marker text-success"></i> ' + event.pickup_address + '</span>' +
                    '<br><span><i class="fa fa-map-marker text-danger"></i> ' + event.dropoff_address + '</span>' +
                    '</div>';
                $(this).popover({
                    placement: 'top',
                    trigger: 'manual',
                    content: desc,
                    container: '#calendar-job',
                    html: true
                }).popover('toggle');

            },
            eventMouseout: function(event, jsEvent, view) {
                $(this).popover('toggle');
            }
        });

        $('.booking-detail-modal').on("change", "[name=driver_id]", function(e) {
            e.preventDefault();
            var tgt_this = $(this);
            var opt = $(this).val().split('--|');
            console.log(opt);
            var driver_id = opt[0];
            var driver_name = opt[1];
            var booking_id = $(this).data('booking-id');
            var booking_ref_id = $(this).data('booking-ref-id');
            swal({
                    title: "Assign Driver",
                    text: "Are you sure? You want to assign a driver '" + driver_name + "' to booking [" + booking_ref_id + ']',
                    type: "info",
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Yes",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function() {
                    // swal({
                    //     title: "",
                    //     text: '',
                    //     imageUrl: 'https://i.gifer.com/4V0b.gif',
                    //     showConfirmButton: false
                    // });
                    $.ajax({
                        url: SITE_URL + "admin/booking/assignDriver",
                        type: 'post',
                        data: {
                            booking_id: booking_id,
                            driver_id: driver_id
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status) {
                                swal({
                                    title: "Success",
                                    text: response.msg,
                                    type: "success",
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                // new Noty({
                                //     type: 'success',
                                //     layout: 'topRight',
                                //     theme: 'nest',
                                //     text: response.msg,
                                //     timeout: 2000
                                // }).show();
                                getBookingView(booking_id);
                            }
                        }
                    });

                });
        });
    });

    function getBookingView(id) {
        $.ajax({
            url: SITE_URL + "admin/booking/ajaxViewBooking/" + id,
            method: 'post',
            dataType: 'json',
            success: function(response) {
                if (response.status) {
                    $('.booking-detail-modal .modal-body').html(response.data.html);
                    $('.booking-detail-modal').modal();
                }
            }
        });
    }

    function performAction(booking_id) {
        getBookingView(booking_id);
        // alert(booking_id);
    }

    function addBooking(date, time) {
        // alert(date+' -  '+time);
    }

    function getDateFormat(date, type) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        var date = new Date();
        date.toLocaleDateString();
        if (type == 1) {
            return [day, month, year].join('/');
        } else {
            return [year, month, day].join('-');
        }
    }
</script>