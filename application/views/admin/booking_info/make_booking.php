<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Make New Booking
            </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-5">
                    <form method="post" id="admin-form-quote">
                        <input type="hidden" name="fleet_name">
                        <div class="box" id="step1">
                            <div class="box-header">
                                <h3 class="box-title">STEP 1: Select journey route</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-sm btn-primary btn-edit" style="display:none">Edit</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <input type="hidden" name="service_type" value="point_to_point">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <ul class="list-inline">
                                                <li>
                                                    <label class="control-label">Journey Type: </label>
                                                </li>
                                                <li>
                                                    <label><input type="radio" name="journey_type" value="one_way" checked> One Way</label>
                                                </li>
                                                <li>
                                                    <label><input type="radio" name="journey_type" value="two_way"> Two Way</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Pickup Location</label>
                                    <input name="start_select" class="form-control ui-widget ui-widget-content" required="" placeholder="eg; airports, postcodes, seaports">
                                    <input type="hidden" name="start">
                                    <input type="hidden" name="start_id">

                                </div>
                                <div class="form-group">
                                    <label class="control-label">Dropoff Location</label>
                                    <input name="end_select" class="form-control ui-widget ui-widget-content" required="" placeholder="eg; airports, postcodes, seaports">
                                    <input type="hidden" name="end">
                                    <input type="hidden" name="end_id">
                                </div>
                                <!-- <div class="input_fields_wrap">
                                </div> -->
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="control-label">Pickup Date</label>
                                            <input type="text" name="pickup_date" class="form-control datepicker" placeholder="mm/dd/yyyy" required="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label">Pickup Time</label>
                                            <input type="text" name="pickup_time" class="form-control timepicker" placeholder="hh:mm am/pm" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row return-details" style="display: none">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="control-label">Return Date</label>
                                            <input type="text" name="return_date" class="form-control datepicker" placeholder="mm/dd/yyyy" required="" autocomplete="off" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label">Return Time</label>
                                            <input type="text" name="return_time" class="form-control timepicker" placeholder="hh:mm am/pm" required="" disabled>
                                        </div>
                                    </div>
                                </div>

                                <button class="btn btn-block btn-info" type="button" onclick="calculate_fare()"><i class="fa fa-calculator"></i> Calculate Journey Fare</button>
                            </div>
                        </div>
                        <div class="box collapsed-box" id="step2">
                            <div class="box-header">
                                <h3 class="box-title">STEP 2: Fill Booking Info</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-sm btn-primary btn-edit" style="display:none">Edit</button>
                                </div>
                            </div>

                            <div class="box-body" style="display: none;">

                                <h4 class="bg-pattern">Personal Details</h4>
                                <input type="hidden" name="passenger_id">

                                <div class="form-group">
                                    <label class="control-label">Passenger Name</label>
                                    <input type="text" class="form-control" name="customer_name" required="">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Email Address</label>
                                    <input type="email" class="form-control" name="customer_email">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Cell No.</label>
                                    <input type="text" class="form-control" name="customer_cell_no">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Street Address</label>
                                    <input type="text" class="form-control" name="customer_address" required="">
                                </div>

                                <h4 class="bg-pattern">Passenger Details</h4>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Passenger</label>
                                            <select class="form-control" name="passengers" required="">
                                                <?php for ($i = 1; $i <= 16; $i++) : ?>
                                                    <option value="<?= $i ?>"><?= $i ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Luggage</label>
                                            <select class="form-control" name="luggage">
                                                <?php for ($i = 0; $i <= 16; $i++) : ?>
                                                    <option value="<?= $i ?>"><?= $i ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Baby Seat</label>
                                            <select class="form-control" name="baby_seat" onchange="populate_baby_seat_details()">
                                                <?php for ($i = 0; $i <= 5; $i++) : ?>
                                                    <option value="<?= $i ?>"><?= $i ?></option>
                                                <?php endfor; ?>
                                            </select>
                                            <div id="baby-seat-details" style="margin-top: 10px"></div>
                                        </div>
                                    </div>
                                </div>

                                <div id="flight-arrival-details" style="display:none">
                                    <h4 class="bg-pattern">Flight Arrival Details</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Flight Name</label>
                                                <input type="text" class="form-control" name="flight_name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Flight No.</label>
                                                <input type="text" class="form-control" name="flight_no">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h4 class="bg-pattern">Special Instructions</h4>
                                <div class="form-group">
                                    <label class="control-label">Message</label>
                                    <textarea class="form-control" name="special_instruction"></textarea>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Payment Method</label>
                                    <div class="col-md-12">
                                        <label class="radio-inline">
                                            <input type="radio" name="payment_method" value="cash" class="flat-green"> Pay Later
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="payment_method" value="card" class="flat-green"> Credit card
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <button class="btn btn-block btn-warning" type="button" onclick="preview_emailer()"><i class="fa fa-search"></i> Preview Booking</button>
                                <button class="btn btn-block btn-success" type="submit"><i class="fa fa-check-circle"></i> Finish Booking</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-7 position-relative">
                    <div class="loader" id="loader-map" style="display:none">
                        <i class="fa fa-gear fa-3x fa-spin"></i>
                        <h2>Loading... </h2>
                    </div>
                    <div id="quote-data" style="display:none">
                        <legend>Select Fleet</legend>
                        <div class="row" id="fleets">
                            <?php
                            if ($fleets) :
                                foreach ($fleets as $fleet) :
                            ?>
                                    <div class="col-md-3 fleet" id="fleet-<?= $fleet->id ?>">
                                        <label>
                                            <input type="radio" name="fleet_id" value="<?= $fleet->id ?>">
                                            <span class="text-center">
                                                <figure>
                                                    <img src="<?= base_url('uploads/fleet/' . $fleet->img_name) ?>" width="100%">
                                                </figure>
                                                <small><?= $fleet->title ?></small>
                                                <h3 class="price"></h3>
                                            </span>
                                            <div class="fleet-detail">
                                                <h2><?= $fleet->title ?></h2>
                                                <p><?= $fleet->desc ?></p>
                                                <ul class="list-inline">
                                                    <li><i class="fa fa-users"></i> <?= $fleet->passengers ?></li>
                                                    <li><i class="fa fa-suitcase"></i> <?= $fleet->luggage ?></li>
                                                    <li><i class="fa fa-briefcase"></i> <?= $fleet->suitcases ?></li>
                                                </ul>
                                                <table class="table fare-details">
                                                </table>
                                            </div>
                                        </label>
                                    </div>
                            <?php
                                endforeach;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="floating-price" style="display:none">
    <div class="price">
        <small>Final Fare</small>$ <span></span>
    </div>
    <div class="floating-price-details">
        <table class="table">
            <tbody>
                <tr>
                    <th width="70%">Journey Fare</th>
                    <td width="30%"><span><?= CURRENCY ?></span> <input type="text" class="form-control" id="journey_fare" onkeyup="recalculate_total()"></td>
                </tr>
            </tbody>
            <tbody id="fp-add-charges"></tbody>
            <tbody>
                <tr>
                    <th>Discount</th>
                    <td><span><?= CURRENCY ?></span> <input type="text" class="form-control" id="discount" onkeyup="recalculate_total()" value="0"></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>Final Fare</th>
                    <td><span><?= CURRENCY ?></span> <input type="text" class="form-control" id="final_fare"></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="modal" id="modal-authorize-card-payment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Authorize.net Secure Payment</h4>
            </div>
            <form method="post" id="form-authorizenet">
                <input type="hidden" name="booking_ref_id">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Select card <span class="text-danger">*</span></label>
                        <select class="form-control" name="creditcard_id">
                            <option value="">New card</option>
                        </select>
                    </div>

                    <div id="cc-details">
                        <div class="form-group">
                            <label class="control-label">Card Number <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon" id="card-image"><img src="<?= base_url('assets/images/cc/default.png') ?>"></span>
                                <input type="text" class="form-control" name="card_number" placeholder="xxxxxxxxxxxxxxx" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Card Expiry Year/Month <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control auto-tab" name="expiry_month" placeholder="MM" required="" min="1" max="12" maxlength="2">
                                        <span class="input-group-addon">/</span>
                                        <input type="text" class="form-control auto-tab" name="expiry_year" placeholder="YY" required="" min="<?= date('y') ?>" max="99" maxlength="2">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">CCV <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control auto-tab" name="card_code" placeholder="123" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Card Holder's Full Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control auto-tab" name="card_holder_name" placeholder="Full Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Save Credit Card? <span class="text-danger">*</span></label>
                            <br>
                            <label class="radio-inline">
                                <input type="radio" name="is_save_cc" value="1"> Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="is_save_cc" value="0" checked=""> No
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" id="link-cancel" class="btn btn-danger" onclick="return confim('Do you really want to cancel this booking?')">Cancel Booking</a>
                    <button type="submit" class="btn btn-success">Process Payment...</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-alert">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-preview-booking">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title">
                    Booking Preview
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-warning" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-edit"></i> Edit Details</span></button>
                        <button type="button" class="btn btn-sm btn-success" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-check"></i> Done</span></button>
                    </div>
                </h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div class="modal" id="preview-raise-invoice-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Raise Invoice
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-warning" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times"></i> Generate Later</span></button>
                    </div>
                </h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-raise-invoice">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <form id="form-invoice" action="<?= site_url('') ?>" name="form_invisible" method="post">
                    <input type="hidden" name="i_is_send_email">
                    <input type="hidden" name="i_invoice_id">
                    <input type="hidden" name="i_booking_id">
                    <input type="hidden" name="i_customer_email">
                    <input type="hidden" name="i_invoice_amount">
                    <input type="hidden" name="i_invoice_payment_method">
                    <input type="hidden" name="i_mail_subject">
                    <input type="hidden" name="i_mail_template">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Send Payment Confirmation Email</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>
    var SITE_URL = '<?= site_url() ?>';
    var BASE_URL = '<?= base_url() ?>';
    var CURRENCY = '<?= CURRENCY ?>';
    var CURRENT_USER = 'admin';
    $(function() {

        // Extend the autocomplete widget, using our own application namespace.
        $.widget("app.autocomplete", $.ui.autocomplete, {
            // The _renderItem() method is responsible for rendering each
            // menu item in the autocomplete menu.
            _renderItem: function(ul, item) {

                // We want the rendered menu item generated by the default implementation.
                var result = this._super(ul, item);

                // If there is logo data, add our custom CSS class, and the specific
                // logo URL.

                if (item.logo) {
                    result.find('li').addClass("fa " + item.logo);
                }

                return result;

            }

        });

        $("[name=start_select]").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: SITE_URL + "admin/booking/ajax_getLocations",
                    data: {
                        q: request.term
                    },
                    success: function(data) {
                        var parsedData = jQuery.parseJSON(data);
                        response(parsedData.data);
                    }
                });
            },
            select: function(event, ui) {
                $("[name=start_id]").val(ui.item.id);
                $("[name=start]").val(ui.item.label);

                if(ui.item.type_id==1){
                    $("#flight-arrival-details").show();
                }else{
                    $("#flight-arrival-details").hide();
                }
            }
        });

        $("[name=end_select]").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: SITE_URL + "admin/booking/ajax_getLocations",
                    data: {
                        q: request.term
                    },
                    success: function(data) {
                        var parsedData = jQuery.parseJSON(data);
                        response(parsedData.data);
                    }
                });
            },
            select: function(event, ui) {
                $("[name=end_id]").val(ui.item.id);
                $("[name=end]").val(ui.item.label);
            }
        });
    });
</script>
<script src="<?= base_url('assets/admin/js/tm-new-booking.js') ?>" type="text/javascript"></script>