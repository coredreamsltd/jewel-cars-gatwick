<!-- Small boxes (Stat box) -->
<?php $email_template=$this->db->get('email_templates')->row()?>
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header" style="padding: 10px 0;">
                <h3 class="box-title">Confirm status for Booking Id: <strong><?= $booking->booking_ref_id ?></strong>
                </h3>
                <div class="pull-right">
                    <a class="btn btn-primary" href="<?= base_url('admin/booking/update/' . $booking->id) ?>"><i class="fa fa-edit"></i> Edit Details</a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-raise-invoice" method="post">
                            <input type="hidden" name="booking_id" value="<?= $booking->booking_ref_id ?>">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php
                                            $recipients = $booking->client_email;
                                            ?>
                                            <label class="control-label">
                                                Recipients Email
                                                <a href="#!" data-toggle="tooltip" title="If more than one email please separate emails with a comma (,) but with no space."><i class="fa fa-info-circle"></i></a>
                                            </label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input class="form-control" name="customer_email" value="<?= $recipients ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Subject</label>
                                            <div class="input-group col-md-12">
                                                <span class="input-group-addon"><i class="fa fa-pencil"></i> </span>
                                                <input class="form-control" name="mail_subject" value="<?= SITE_NAME ?> Booking Confirmation [<?= $booking->booking_ref_id ?>]" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Message</label>
                                            <textarea class="form-control" name="mail_message" id="message" required=""><?=!empty($email_template->booking_confirmation_top)?$email_template->booking_confirmation_top:''?></textarea>
                                            <script type="text/javascript">
                                                var editor = CKEDITOR.replace('mail_message', {
                                                    customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                                });
                                                CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                            </script>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <td>
                                            <label class="control-label">Email Bottom Message</label>
                                            <textarea class="form-control" name="mail_bottom_message"></textarea>
                                            <!--<script type="text/javascript">-->
                                            <!--    var editor = CKEDITOR.replace('mail_bottom_message',-->
                                            <!--        {-->
                                            <!--            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'-->
                                            <!--        });-->
                                            <!--    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');-->
                                            <!--</script>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="btn btn-primary pull-right preview-btn" type="button"><i class="fa fa-search"> </i> Preview Confirmation Email
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-raise-invoice">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" style="font-family: Times New Roman !important;">

            </div>
            <div class="modal-footer">
                <form action="" id="form-invoice" name="form_invisible" method="post">
                    <input type="hidden" name="i_booking_id">
                    <input type="hidden" name="i_customer_email">
                    <input type="hidden" name="i_mail_subject">
                    <input type="hidden" name="i_mail_template">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitting...'; this.form.submit();">Generate & Email Confirmation</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $(function() {

        $('.preview-btn').click(function() {
            var form = $('#form-raise-invoice');
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            $.ajax({
                url: '<?= site_url('admin/booking/ajax_preview_confirm_booking') ?>',
                dataType: 'json',
                method: 'post',
                data: {
                    formdata: form.serialize()
                },
                success: function(response) {

                    $('#form-invoice [name=i_booking_id]').val(response.data.booking_id);
                    $('#form-invoice [name=i_customer_email]').val(response.data.customer_email);
                    $('#form-invoice [name=i_mail_subject]').val(response.data.mail_subject);
                    $('#form-invoice [name=i_mail_template]').val(response.data.mail_template);

                    $('#modal-raise-invoice .modal-body').html(response.data.mail_template);
                    $('#modal-raise-invoice .modal-header h4').html("<strong>Mail Subject: </strong>" + response.data.mail_subject);
                    $('#modal-raise-invoice').modal();
                }
            });
        });


    });
</script>