<!-- <h2 v-show="screen == 'vehicle-selection'" style="display: none;">Vehicle Selection</h2> -->

<div v-show="screen == 'vehicle-selection'" style="display: none;">
    <div class="box-header">
        <h3 class="box-title">Vehicle Manager | Select</h3>
    </div><!-- /.box-header -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">English</a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content ">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row english">
                <div class="col-sm-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <h5>Pick up:</h5>
                                                <span>{{booking[journey_type].quote.start}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <h5>Drop off:</h5>
                                                <span>{{booking[journey_type].quote.end}}</span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="my_card">
                                                <div id="map" class="map-canvas"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row container">
                                        <div class="fleet_price" v-for="(fleet,index) in  (journey_type == 'one_way'?booking.one_way.fleets:booking.two_way.fleets)" v-if="bookings[journey].one_way.fleets[index] && (bookings[journey].one_way.fleets[index].fare || bookings[journey].two_way.fleets[index].fare) ">
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <h2>{{fleet.title}}</h2>
                                                        <img v-bind:src="'<?= base_url() ?>/uploads/fleet/' + fleet.img_name">
                                                        <p>{{fleet.desc_1}}</p>
                                                    </div>
                                                    <div class="col-md-5" style="margin-top: 25px;">
                                                        <label class="btn btn-primary" v-if="bookings[journey].one_way.fleets[index].fare" style="margin-bottom: 10px;">Single <?= CURRENCY ?> {{bookings[journey].one_way.fleets[index].fare}}
                                                            <input type="radio" name="journey_type" value="one_way" :data-fleet-id="fleet.id" v-on:click="selectVehicle()">
                                                        </label>
                                                        <label class="btn btn-primary" v-if="bookings[journey].two_way.fleets[index].fare">Return <?= CURRENCY ?> {{bookings[journey].one_way.fleets[index].fare + bookings[journey].two_way.fleets[index].fare}}
                                                            <input type="radio" name="journey_type" value="two_way" :data-fleet-id="fleet.id" v-on:click="selectVehicle()">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>