<form class="forms" id="quote-form" method="post" v-show="screen == 'quote-form'">
    <div class="box-header">
        <h3 class="box-title">Booking Manager | Add</h3>
        <div class="pull-right">
            <button type="button" class="btn btn-lg btn-flat btn-primary" @click="getQuote()"> Save</button>
        </div>
    </div><!-- /.box-header -->
    <!-- Tab panes -->
    <div class="tab-content ">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row english">
                <div class="col-sm-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Pick up location <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control start trg-solo" placeholder="Eg: Gatwick Airport or RH6 ONP" name="start" required autocomplete="off">
                                                <span class='error'></span>
                                                <span class="startLatLng trg-bulk">
                                                    <input type="hidden" data-geo="lat" name="start_lat" :value="journey_type=='one_way'?booking.one_way.quote.start_lat:booking[journey_type].quote.start_lat">
                                                    <input type="hidden" data-geo="lng" name="start_lng" :value="booking[journey_type].quote.start_lng">
                                                    <input type="hidden" name="start_post_code" :value="booking[journey_type].quote.start_post_code">
                                                    <input type="hidden" name="start_town_or_city" :value="booking[journey_type].quote.start_town_or_city">
                                                    <input type="hidden" name="start_district" :value="booking[journey_type].quote.start_district">
                                                    <input type="hidden" name="start_country" :value="booking[journey_type].quote.start_country">
                                                </span>
                                                <div class="location-suggestion-container start-loader"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Drop off location <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control end trg-solo search_keyword" placeholder="Eg: Gatwick Airport or RH6 ONP" name="end" required autocomplete="off">
                                            <span class='error'></span>
                                            <span class="endLatLng trg-bulk">
                                                <input type="hidden" data-geo="lat" name="end_lat" :value="booking[journey_type].quote.end_lat">
                                                <input type="hidden" data-geo="lng" name="end_lng" :value="booking[journey_type].quote.end_lng">
                                                <input type="hidden" name="end_post_code" :value="booking[journey_type].quote.end_post_code">
                                                <input type="hidden" name="end_town_or_city" :value="booking[journey_type].quote.end_town_or_city">
                                                <input type="hidden" name="end_district" :value="booking[journey_type].quote.end_district">
                                                <input type="hidden" name="end_country" :value="booking[journey_type].quote.end_country">
                                            </span>
                                            <div class="location-suggestion-container end-loader"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>