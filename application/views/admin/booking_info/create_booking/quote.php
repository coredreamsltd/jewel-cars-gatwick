<div class="row">
    <div class="col-md-12">
        <div class="box" id="admin-quote-booking-page">
            <?php $this->load->view('admin/booking_info/create_booking/create') ?>
            <?php $this->load->view('admin/booking_info/create_booking/vehicle_selection') ?>
            <?php $this->load->view('admin/booking_info/create_booking/booking') ?>
            <?php $this->load->view('admin/booking_info/create_booking/booking-two-way') ?>
            <?php $this->load->view('admin/booking_info/create_booking/confirm') ?>
            <?php $this->load->view('admin/booking_info/create_booking/payment') ?>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script type="text/javascript" src="https://cc-cdn.com/generic/scripts/v1/cc_c2a.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/maplace-js/0.2.10/maplace.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.8/vue.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    var SITE_URL = '<?= site_url() ?>';
    var coupon_discount_code = '<?= getSession('discount')['code'] ?>';
    var coupon_discount_amount = '<?= getSession('discount')['amount'] ?>';
    var journey_type = '<?= getSession('journey_type') ?>';
    var journey = <?= getSession('journey') ?: 0 ?>;
    var screen = '<?= getSession('screen') ?>';
    var journeys = JSON.parse(JSON.stringify(<?= json_encode(getSession('journeys')) ?>));
    var additional_rate = JSON.parse('<?= $additional_rate ? json_encode($additional_rate) : 'null' ?>');
    var passenger = JSON.parse('<?= $passenger ? json_encode($passenger) : 'null' ?>');
    var meet_and_greet_page = JSON.parse(JSON.stringify(<?= json_encode(getSession('meet_and_greet') ? getSession('meet_and_greet') : 'null') ?>));
    $(function() {
        var new_vue = new Vue({
            el: "#admin-quote-booking-page",
            data() {
                return {
                    bookings: (typeof(journeys) != "undefined" && journeys !== null) ? journeys : {
                        one_way: {
                            quote: {},
                            fleets: [],
                            selected_fleet: {},
                            booking_details: {},
                            grand_total_charge: {}
                        },
                        two_way: {
                            quote: {},
                            fleets: [],
                            selected_fleet: {},
                            booking_details: {},
                            grand_total_charge: {}
                        }
                    },
                    booking: {
                        one_way: {
                            quote: (journeys[journey]['one_way'].quote) ? journeys[journey]['one_way'].quote : {},
                            fleets: (journeys[journey]['one_way'].fleets) ? journeys[journey]['one_way'].fleets : [],
                            selected_fleet: (journeys[journey]['one_way'].selected_fleet) ? journeys[journey]['one_way'].selected_fleet : {},
                            booking_details: (journeys[journey]['one_way'].booking_details) ? journeys[journey]['one_way'].booking_details : {},
                            grand_total_charge: (journeys[journey]['one_way'].grand_total_charge) ? journeys[journey]['one_way'].grand_total_charge : {}
                        },
                        two_way: {
                            quote: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].quote : {},
                            fleets: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].fleets : [],
                            selected_fleet: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].selected_fleet : {},
                            booking_details: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].booking_details : {},
                            grand_total_charge: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].grand_total_charge : {}
                        },
                        journey_type: (journeys[journey].journey_type) ? journeys[journey].journey_type : 'one_way',
                    },
                    screen: screen,
                    screenType: '',
                    additional_rate: additional_rate,
                    passenger: passenger,
                    grand_total: 0,
                    coupon_discount_code: coupon_discount_code,
                    coupon_discount_amount: coupon_discount_amount,
                    grand_total_after_discount: 0,
                    accept_terms: false,
                    selectedPassenger: '',
                    pay_method: 'card',
                    baby_ages: ['0 to 8 Months', '9 to 15 Months', '16 to 24 Months', '25 to 36 Months', '37 to 40 Months', 'Over 40 months'],
                    max_fields: 5, //maximum input boxes allowed
                    x: 0,
                    count: 0, //initlal text box count
                    meet_and_greet: meet_and_greet_page
                }
            },
            methods: {
                getQuote: function() {
                    var _this = this;
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    if (!$('#quote-form').valid()) {
                        return false;
                    }
                    var quote_data = objectifyForm($('#quote-form').serializeArray());
                    quote_data.journey = journey;
                    this.booking.journey_type.quote = quote_data;
                    this.form_submit_status = true;
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait, calculating fares...  <i class="fa fa-cog fa-spin pull-right"></i></a>');
                    $.ajax({
                        type: "POST",
                        url: SITE_URL + "quote/ajaxGetQuote",
                        data: quote_data,
                        dataType: 'json',
                        success: function(response) {
                            // console.log(response);
                            if (!response.status) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }

                            //_this.bookings = response.data.journeys;
                            //_this.booking=response.data.journeys[response.data.journey];
                            let journeys = response.data.journeys;
                            let journey = response.data.journey;

                            // console.log(journeys[journey]['one_way'].quote ? journeys[journey]['one_way'].quote : '');
                            _this.booking = {
                                one_way: {
                                    quote: (journeys[journey]['one_way'].quote) ? journeys[journey]['one_way'].quote : {},
                                    fleets: (journeys[journey]['one_way'].fleets) ? journeys[journey]['one_way'].fleets : [],
                                    selected_fleet: (journeys[journey]['one_way'].selected_fleet) ? journeys[journey]['one_way'].selected_fleet : {},
                                    booking_details: (journeys[journey]['one_way'].booking_details) ? journeys[journey]['one_way'].booking_details : {},
                                    grand_total_charge: (journeys[journey]['one_way'].grand_total_charge) ? journeys[journey]['one_way'].grand_total_charge : {}
                                },
                                two_way: {
                                    quote: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].quote : {},
                                    fleets: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].fleets : [],
                                    selected_fleet: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].selected_fleet : {},
                                    booking_details: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].booking_details : {},
                                    grand_total_charge: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].grand_total_charge : {}
                                },
                                journey_type: (journeys[journey].journey_type) ? journeys[journey].journey_type : 'one_way',
                            };
                            _this.bookings = journeys;
                            new google.maps.Map(document.getElementById("map"), {
                                zoom: 1,
                                center: {
                                    lat: 52.8807629,
                                    lng: -1.5661885
                                },
                                mapTypeControl: false,
                            });
                            _this.geocode('start');
                            _this.geocode('end');
                            _this.screen = "vehicle-selection";
                        }
                    });
                },
                loadData() {
                    var vm = this;
                    $(".all-datepicker").flatpickr();
                    $(".datetime-picker").flatpickr({
                        minDate: "today",
                        enableTime: true,
                        altInput: true,
                        time_24hr: true,
                        altFormat: "l j F, Y H:i",
                        dateFormat: "Y-m-d H:i",
                        position: 'below',
                        close: true,
                        onChange: function(selectedDates, dateStr, instance, timezone_offset_get) {
                            var datetime = new Date(selectedDates);
                            var cHour = datetime.getHours();
                            var cMin = datetime.getMinutes();
                            if (cHour == 0 && cMin == 0) {
                                html = '<li class="validation condition warning"><i class="fas fa-info-circle"></i> To avoid confusion, please enter 23:55 (on the previous day) or 00:05 instead</li>';
                                $('.date-time-validation .validation').html(html);
                            } else {
                                $('.date-time-validation .validation').html('');
                            }
                        },
                        onOpen: function(selectedDates, dateStr, instance) {
                            $(instance.timeContainer).append('<button class="flatpickr-close">Apply</button>');
                            $(instance.timeContainer).find('.flatpickr-close').css({
                                "background-color": "#ffca09",
                                "color": "#000",
                                "border": "0",
                                "border-radius": "0 0 5px 0",
                                "padding": "0 15px 0 15px",
                                "font-weight": "700",
                                "cursor": "pointer"
                            });
                        },
                        onClose: function(selectedDates, dateStr, instance) {
                            $(instance.timeContainer).find('.flatpickr-close').remove();
                        }
                    });

                    // duplicating alert calenders on different places
                    // $('body').on('click', '.datetime-picker', function() {
                    //     $('.flatpickr-calendar').addClass('open');
                    // });

                    $('body').on('click', '.flatpickr-close', function() {
                        $('.flatpickr-calendar').removeClass('open');
                        $('.datetime-picker').removeClass('active');
                    });

                    $('[name=pickup_date]').change(function() {
                        var start_date = $(this).val();
                        var arr = start_date.split('/');

                        $('[name=return_date]').val('');
                        $('[name=return_date]').datepicker('destroy');
                        $('[name=return_date]').removeAttr('disabled', '');
                        $('[name=return_date]').datepicker({
                            dateFormat: 'dd/mm/yy',
                            yearRange: '1999:2050',
                            minDate: new Date(arr[2], arr[1] - 1, arr[0])
                        });
                    });

                    $('.timepicker').timepicker({
                        timeFormat: 'H:i',
                        step: 5,
                        disableTouchKeyboard: true,
                    });

                    $('.start').keyup(function() {
                        $('.startLatLng').find('input').val('');
                    });

                    $('.end').keyup(function() {
                        $('.endLatLng').find('input').val('');
                    });

                    $.widget("app.autocomplete", $.ui.autocomplete, {
                        _renderItem: function(ul, item) {
                            var result = this._super(ul, item);
                            result.find("div").html('<i class="fa ' + item.logo + '"></i> <span class="auto-item-query">' + item.label + ', ' + item.postcode + '</span>');
                            return result;
                        },
                    });

                    $(".start,.end").click(function() {
                        // $("#cc_c2a").hide();
                    });
                    $(".start").autocomplete({
                        autoFocus: 1,
                        minLength: 1,
                        source: function(request, response) {
                            $.ajax({
                                url: SITE_URL + "/quote/ajax_get_locations",
                                data: {
                                    q: request.term
                                },
                                dataType: "json",
                                success: function(data) {
                                    $(".start-loader").html('');
                                    if (data.status == 0) {
                                        loadFechify('start');
                                        return;
                                    }
                                    // $('#cc_c2a').hide();
                                    response(data.data);
                                },
                            });
                        },
                        search: function(event, ui) {
                            $("#ui-id-1").css("display", "none");
                            $('#cc_c2a').hide();
                            $(".start-loader").html('<div><p><i class="fa fa-spin fa-cog"></i>Finding Locations.....</p></div>');
                        },
                        select: function(event, ui) {
                            if (ui.item.id <= 0) {
                                loadFechify('start');
                                return false;
                            }

                            $(".start").val(ui.item.label + ', ' + ui.item.postcode);
                            $("[name=start_lat]").val('');
                            $("[name=start_lng]").val('');
                            $("[name=start_post_code]").val(ui.item.postcode);
                            $("[name=start_town_or_city]").val('');
                            $("[name=start_district]").val('');
                            $("[name=start_country]").val('');

                            vm.booking[journey_type].quote.start = ui.item.label + ', ' + ui.item.postcode;
                            vm.booking[journey_type].quote.start_lat = '';
                            vm.booking[journey_type].quote.start_lng = '';
                            vm.booking[journey_type].quote.start_post_code = ui.item.postcode;
                            vm.booking[journey_type].quote.start_town_or_city = '';
                            vm.booking[journey_type].quote.start_district = '';
                            vm.booking[journey_type].quote.start_country = '';
                            return false;
                        },
                    });

                    $(".end").autocomplete({
                        autoFocus: 1,
                        minLength: 1,
                        source: function(request, response) {
                            $.ajax({
                                url: SITE_URL + "/quote/ajax_get_locations",
                                data: {
                                    q: request.term
                                },
                                dataType: "json",
                                success: function(data) {
                                    $(".end-loader").html('');
                                    if (data.status == 0) {
                                        loadFechify('end');
                                        return;
                                    }
                                    response(data.data);
                                },
                            });
                        },
                        search: function(event, ui) {
                            $("#ui-id-2").css("display", "none");
                            $('#cc_c2a').hide();
                            $(".end-loader").html('<div><p><i class="fa fa-spin fa-cog"></i>Finding Locations.....</p></div>');
                        },
                        select: function(event, ui) {
                            if (ui.item.id <= 0) {
                                loadFechify('end');
                                return false;
                            }

                            $(".end").val(ui.item.label + ', ' + ui.item.postcode);
                            vm.booking[journey_type].quote.end = ui.item.label + ', ' + ui.item.postcode;

                            $("[name=end_lat]").val('');
                            $("[name=end_lng]").val('');
                            $("[name=end_post_code]").val(ui.item.postcode);
                            $("[name=end_town_or_city]").val('');
                            $("[name=end_district]").val('');
                            $("[name=end_country]").val('');

                            vm.booking[journey_type].quote.end_lat = '';
                            vm.booking[journey_type].quote.end_lng = '';
                            vm.booking[journey_type].quote.end_post_code = ui.item.postcode;
                            vm.booking[journey_type].quote.end_town_or_city = '';
                            vm.booking[journey_type].quote.end_district = '';
                            vm.booking[journey_type].quote.end_country = '';
                            return false;
                        },
                    });
                },
                calculateTotalFare: function() {
                    var that = this;
                    if (this.bookings) {
                        $(this.bookings).each(function(i, booking) {
                            if (booking.one_way.selected_fleet) {
                                that.grand_total = parseInt(parseFloat(that.grand_total) + parseFloat(booking.one_way.selected_fleet.fare) + parseFloat(booking.journey_type == 'two_way' ? booking.two_way.selected_fleet.fare : 0));
                            }
                        });
                    }
                },
                geocode: function(locationType) {
                    let geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                            address: this.booking[this.booking.journey_type].quote[locationType]
                        })
                        .then((response) => {
                            let results = response.results;
                            this.booking[this.booking.journey_type].quote[locationType + '_lat'] = results[0].geometry.location.lat();
                            this.booking[this.booking.journey_type].quote[locationType + '_lng'] = results[0].geometry.location.lng();
                            if (this.booking[this.booking.journey_type].quote.start_lat && this.booking[this.booking.journey_type].quote.end_lat) {
                                this.drawMap();
                            }
                        })
                        .catch((e) => {
                            alert("Geocode was not successful for the following reason: " + e);
                        });
                },
                drawMap: function() {
                    new Maplace({
                        locations: [{
                                lat: this.booking[this.booking.journey_type].quote.start_lat,
                                lon: this.booking[this.booking.journey_type].quote.start_lng,
                                type: 'marker',
                                title: this.booking[this.booking.journey_type].quote.start,
                                html: this.booking[this.booking.journey_type].quote.start
                            },
                            {
                                lat: this.booking[this.booking.journey_type].quote.end_lat,
                                lon: this.booking[this.booking.journey_type].quote.end_lng,
                                type: 'marker',
                                title: this.booking[this.booking.journey_type].quote.end,
                                html: this.booking[this.booking.journey_type].quote.end
                            }
                        ],
                        map_div: '#map',
                        generate_controls: false,
                        type: 'directions',
                        draggable: false,
                    }).Load();
                },
                editScreen: function(screen, journey = null, journey_type = 'one_way') {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait....  <i class="fa fa-cog fa-spin pull-right"></i></a>');
                    $.ajax({
                        type: "POST",
                        url: SITE_URL + "/quote/editScreen",
                        data: {
                            screen: screen,
                            journey: journey,
                            journey_type: journey_type,
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (!response.status) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            new_vue.screen = response.data.screen;
                        }
                    });
                },
                selectVehicle: function() {
                    var _this = this;
                    var journey_type = $('[name=journey_type]:checked').val();
                    var fleet_id = $('[name=journey_type]:checked').data('fleet-id');
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    if (!journey_type || !fleet_id) {
                        $('.advice.error').html('<div class="alert alert-danger">Please at least select one vehicle.</div>');
                        $('.advice.error').removeClass('d-none');
                        return;
                    }
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait, calculating fares...  <i class="fa fa-cog fa-spin pull-right"></i></a>');
                    $.ajax({
                        url: '<?= site_url('quote/ajaxBooking') ?>',
                        method: 'POST',
                        data: {
                            fleet_id: fleet_id,
                            journey_type: journey_type
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status !== true) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            
                            let journeys = response.data.journeys;
                            let journey = response.data.journey;

                            // meet and greet
                            _this.meet_and_greet.excerpt = response.data.meet_and_greet.excerpt.replace("SERVICE_CHARGE", _this.additional_rate.meet_and_greet);
                            _this.meet_and_greet.desc = response.data.meet_and_greet.desc.replace("SERVICE_CHARGE", 0);

                            _this.booking = {
                                one_way: {
                                    quote: (journeys[journey]['one_way'].quote) ? journeys[journey]['one_way'].quote : {},
                                    fleets: (journeys[journey]['one_way'].fleets) ? journeys[journey]['one_way'].fleets : [],
                                    selected_fleet: (journeys[journey]['one_way'].selected_fleet) ? journeys[journey]['one_way'].selected_fleet : {},
                                    booking_details: (journeys[journey]['one_way'].booking_details) ? journeys[journey]['one_way'].booking_details : {},
                                    grand_total_charge: (journeys[journey]['one_way'].grand_total_charge) ? journeys[journey]['one_way'].grand_total_charge : {}
                                },
                                two_way: {
                                    quote: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].quote : {},
                                    fleets: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].fleets : [],
                                    selected_fleet: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].selected_fleet : {},
                                    booking_details: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].booking_details : {},
                                    grand_total_charge: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].grand_total_charge : {}
                                },
                                journey_type: (journeys[journey].journey_type) ? journeys[journey].journey_type : 'one_way',
                            };
                            _this.bookings = journeys;

                            _this.booking.one_way.selected_fleet.passengers = parseInt(_this.booking.one_way.selected_fleet.passengers);
                            _this.booking.one_way.selected_fleet.suitcases = parseInt(_this.booking.one_way.selected_fleet.suitcases);
                            _this.booking.one_way.selected_fleet.infant_seats = parseInt(_this.booking.one_way.selected_fleet.infant_seats);
                            _this.booking.one_way.selected_fleet.baby_seats = parseInt(_this.booking.one_way.selected_fleet.baby_seats);
                            _this.booking.one_way.selected_fleet.child_booster_seats = parseInt(_this.booking.one_way.selected_fleet.child_booster_seats);
                            _this.booking.one_way.selected_fleet.booster_seats = parseInt(_this.booking.one_way.selected_fleet.booster_seats);
                            _this.booking.one_way.booking_details.discount_code = _this.booking.one_way.booking_details.discount_code ? _this.booking.one_way.booking_details.discount_code : '';
                            new_vue.screen = 'booking-form';
                        }
                    });
                },
                submitOneBooking: function() {
                    if (!$('#booking-info-form').valid()) {
                        return false;
                    }
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait.....  <i class="fa fa-cog fa-spin pull-right"></i></a>');
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    var booking_data = objectifyForm($('#booking-info-form').serializeArray());
                    booking_data.coupon_discount_code = this.coupon_discount_code;
                    var _this = this;
                    $.ajax({

                        url: '<?= site_url('quote/oneWayBookingInfo') ?>',
                        method: 'POST',
                        data: booking_data,
                        dataType: 'json',
                        success: function(response) {
                            if (!response.status) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            let journeys = response.data.journeys;
                            let journey = response.data.journey;
                            
                            _this.booking = {
                                one_way: {
                                    quote: (journeys[journey]['one_way'].quote) ? journeys[journey]['one_way'].quote : {},
                                    fleets: (journeys[journey]['one_way'].fleets) ? journeys[journey]['one_way'].fleets : [],
                                    selected_fleet: (journeys[journey]['one_way'].selected_fleet) ? journeys[journey]['one_way'].selected_fleet : {},
                                    booking_details: (journeys[journey]['one_way'].booking_details) ? journeys[journey]['one_way'].booking_details : {},
                                    grand_total_charge: (journeys[journey]['one_way'].grand_total_charge) ? journeys[journey]['one_way'].grand_total_charge : {}
                                },
                                two_way: {
                                    quote: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].quote : {},
                                    fleets: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].fleets : [],
                                    selected_fleet: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].selected_fleet : {},
                                    booking_details: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].booking_details : {},
                                    grand_total_charge: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].grand_total_charge : {}
                                },
                                journey_type: (journeys[journey].journey_type) ? journeys[journey].journey_type : 'one_way',
                            };
                            _this.bookings = journeys;
                            


                            if(response.data.screen == 'booking-form-two')
                            {
                                _this.booking.two_way.selected_fleet.passengers = parseInt(_this.booking.two_way.selected_fleet.passengers);
                                _this.booking.two_way.selected_fleet.suitcases = parseInt(_this.booking.two_way.selected_fleet.suitcases);
                                _this.booking.two_way.selected_fleet.baby_seats = parseInt(_this.booking.two_way.selected_fleet.baby_seats);
                                _this.booking.two_way.selected_fleet.infant_seats = parseInt(_this.booking.two_way.selected_fleet.infant_seats);
                                _this.booking.two_way.selected_fleet.child_booster_seats = parseInt(_this.booking.two_way.selected_fleet.child_booster_seats);
                                _this.booking.two_way.selected_fleet.booster_seats = parseInt(_this.booking.two_way.selected_fleet.booster_seats);
                                _this.booking.two_way.booking_details.name = _this.booking.two_way.booking_details.name ? _this.booking.two_way.booking_details.name : _this.booking.one_way.booking_details.name;
                                _this.booking.two_way.booking_details.phone = _this.booking.two_way.booking_details.phone ? _this.booking.two_way.booking_details.phone : _this.booking.one_way.booking_details.phone;
                            }

                            _this.calculateTotalFare();
                            _this.grand_total_after_discount = parseFloat(parseFloat(_this.grand_total) - parseFloat(_this.coupon_discount_amount)).toFixed(2);

                            new_vue.screen = response.data.screen;
                        }
                    });

                },
                submitTwoBooking: function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    if (!$('#booking-info-form-two').valid()) {
                        return false;
                    }
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait.....  <i class="fa fa-cog fa-spin pull-right"></i></a>');

                    var booking_data = objectifyForm($('#booking-info-form-two').serializeArray());
                    // alert('hell yhaa!');
                    $.ajax({
                        url: '<?= site_url('quote/twoWayBookingInfo') ?>',
                        method: 'POST',
                        data: booking_data,
                        dataType: 'json',
                        success: function(response) {
                            if (response.status !== true) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            new_vue.screen = response.data.screen;
                        }
                    });

                },
                payNow: function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    $('.full-page-loader').show();
                    var that = this;
                    var bookings = $.parseJSON(JSON.stringify(this.bookings));
                    var booking_data = [];
                    $.each(bookings, function(i, booking) {
                        delete booking.one_way.fleets;
                        delete booking.two_way.fleets;
                        console.log('----------', booking, '-----------');
                        booking_data[i] = booking;
                    });

                    // alert(this.selectedPassenger);
                    var _this = this;
                    var passenger = this.selectedPassenger;
                    $.ajax({
                        url: '<?= site_url('quote/finish') ?>',
                        method: 'POST',
                        data: {
                            booking_data: booking_data,
                            grand_total: that.grand_total,
                            coupon_discount_code: that.coupon_discount_code,
                            coupon_discount_amount: that.coupon_discount_amount,
                            pay_method: that.pay_method,
                            passenger: passenger,
                        },
                        dataType: 'json',
                        success: function(response) {
                            $('.full-page-loader').hide();
                            if (response.status !== true) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000,
                                });
                                return;
                            }
                            
                            if (response.data.is_finish != undefined) {
                                // screen = response.data.redirect_url;
                                location.href = SITE_URL +"/admin/booking";
                            } else {
                                alert('Something went wrong');
                            }
                        }
                    });

                },
                applyDiscount: function() {
                    var that = this;
                    $.ajax({
                        url: '<?= site_url('quote/processDiscount') ?>',
                        method: 'POST',
                        data: {
                            discount_code: that.coupon_discount_code,
                            grand_total: that.grand_total
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (!response.status) {
                                that.coupon_discount_code = '';
                                that.coupon_discount_amount = 0;
                                that.grand_total_after_discount = that.grand_total;
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            that.coupon_discount_amount = response.data.discount_amount;
                            that.grand_total_after_discount = response.data.grand_total_after_discount;
                        }
                    });

                },
                basket: function(param) {
                    $.ajax({
                        url: '<?= site_url('admin/booking/basket') ?>',
                        method: 'POST',
                        data: {
                            type: param
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status !== true) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            location.replace("<?= site_url('admin/booking/quote') ?>");
                            location.reload();
                        }
                    });
                }
            },
            mounted() {
                let map;
                var vm = this;
                this.screen = 'quote-form';
                this.loadData();
                this.calculateTotalFare();
                this.grand_total_after_discount = parseFloat(parseFloat(this.grand_total) - parseFloat(this.coupon_discount_amount)).toFixed(2);
                if (screen == 'quote-form') {
                    if (this.booking[this.journey_type].quote.stop_point) {
                        var html = '';
                        $.each(this.booking[this.journey_type].quote.stop_point, function(i, v) {
                            html += '<div class="form-group"> <label>Via</label> <span class="addon"><img src="./assets/images/location.svg"></span>' +
                                '<input type="text" class="form-control trg-solo way_point_address' + vm.x + '" value="' + v + '" placeholder="Eg: Gatwick Airport or RH6 ONP" name="stop_point[' + vm.count + ']" required autocomplete="off">' +
                                '<span class="trg-bulk way_point' + vm.count + '">' +
                                '<input type="hidden" class="lat" name="way_point_lat[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_lat[i] + '">' +
                                '<input type="hidden" class="lng" name="way_point_lng[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_lng[i] + '">' +
                                '<input type="hidden" class="postcode" name="way_point_post_code[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_post_code[i] + '"> ' +
                                '<input type="hidden" class="city" name="way_point_city[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_city[i] + '"> ' +
                                '<input type="hidden" class="district" name="way_point_district[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_district[i] + '">' +
                                '<input type="hidden" class="country" name="way_point_country[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_country[i] + '">' +
                                '</span>' +
                                '<a href="javascript:void(0)" class="remove_field">' +
                                '<i class="fa fa-times text-danger"></i>' +
                                '</a>' +
                                '</div>' +
                                '<div class="options"><a></a> <a href="javascript:void(0)" class="toggle field-swap"><img src="<?= base_url('assets/images') ?>/two-way.svg"></a> </div>';
                            vm.way_point(vm.count);
                            vm.x++;
                            vm.count++;
                        });
                        $(html).insertBefore($(".input-fields-wrap"));
                    }
                    this.screenType = "";
                } else if (screen == 'vehicle-selection') {
                    map = new google.maps.Map(document.getElementById("map"), {
                        zoom: 1,
                        center: {
                            lat: 52.8807629,
                            lng: -1.5661885
                        },
                        mapTypeControl: false,
                    });
                    this.geocode('start');
                    this.geocode('end');
                    this.screenType = "quote-form";

                } else if (screen == 'booking-form') {
                    this.meet_and_greet.excerpt = this.meet_and_greet.excerpt.replace("SERVICE_CHARGE", this.additional_rate.meet_and_greet);
                    this.meet_and_greet.desc = this.meet_and_greet.desc.replace("SERVICE_CHARGE", this.additional_rate.pickup_charge);
                    this.booking.one_way.selected_fleet.passengers = parseInt(this.booking.one_way.selected_fleet.passengers);
                    this.booking.one_way.selected_fleet.suitcases = parseInt(this.booking.one_way.selected_fleet.suitcases);
                    this.booking.one_way.selected_fleet.infant_seats = parseInt(this.booking.one_way.selected_fleet.infant_seats);
                    this.booking.one_way.selected_fleet.baby_seats = parseInt(this.booking.one_way.selected_fleet.baby_seats);
                    this.booking.one_way.selected_fleet.child_booster_seats = parseInt(this.booking.one_way.selected_fleet.child_booster_seats);
                    this.booking.one_way.selected_fleet.booster_seats = parseInt(this.booking.one_way.selected_fleet.booster_seats);
                    this.booking.one_way.booking_details.discount_code = this.booking.one_way.booking_details.discount_code ? this.booking.one_way.booking_details.discount_code : '';
                    this.screenType = "vehicle-selection";
                } else if (screen == 'booking-form-two') {
                    this.meet_and_greet.excerpt = this.meet_and_greet.excerpt.replace("SERVICE_CHARGE", this.additional_rate.meet_and_greet);
                    this.meet_and_greet.desc = this.meet_and_greet.desc.replace("SERVICE_CHARGE", this.additional_rate.pickup_charge);
                    this.booking.two_way.selected_fleet.passengers = parseInt(this.booking.two_way.selected_fleet.passengers);
                    this.booking.two_way.selected_fleet.suitcases = parseInt(this.booking.two_way.selected_fleet.suitcases);
                    this.booking.two_way.selected_fleet.baby_seats = parseInt(this.booking.two_way.selected_fleet.baby_seats);
                    this.booking.two_way.selected_fleet.infant_seats = parseInt(this.booking.two_way.selected_fleet.infant_seats);
                    this.booking.two_way.selected_fleet.child_booster_seats = parseInt(this.booking.two_way.selected_fleet.child_booster_seats);
                    this.booking.two_way.selected_fleet.booster_seats = parseInt(this.booking.two_way.selected_fleet.booster_seats);
                    this.booking.two_way.booking_details.name = this.booking.two_way.booking_details.name ? this.booking.two_way.booking_details.name : this.booking.one_way.booking_details.name;
                    this.booking.two_way.booking_details.phone = this.booking.two_way.booking_details.phone ? this.booking.two_way.booking_details.phone : this.booking.one_way.booking_details.phone;
                    this.screenType = "vehicle-selection";
                }
            },
            computed: {
                isDisabled: function() {
                    return !this.accept_terms;
                }
            },
        });
    });
    var cc_c2a_obj = new clickToAddress({
        accessToken: '<?= FECHIFY_ACCESS_TOKEN ?>',
        gfxMode: 1,
        countryMatchWith: 'iso_3',
        enabledCountries: ['GBR'],
        style: {
            ambient: 'light',
            accent: 'default'
        },
        domMode: 'class'
    });

    function loadFechify(trgClass, trgKey = 0) {
        $(".ui-widget-content").hide();
        $('#cc_c2a').show();
        getCraftyData(trgClass, trgKey);
    }

    function getCraftyData(trgClass, trgKey) {
        let _this = this;
        this.trgClass = trgClass;
        cc_c2a_obj.attach({
            search: trgClass
        }, {
            onResultSelected: function(c2a, elements, address) {
                let res;
                if (address.line_1) {
                    res = address.line_1 + ', ' + address.line_2 + ', ' + address.locality + ', ' + address.postal_code;
                } else {
                    res = address.company_name + ', ' + address.locality + ', ' + address.postal_code;
                }

                $("." + _this.trgClass).val(res);
                if (_this.trgClass == 'start') {
                    $('[name=start_town_or_city]').val(address.line_1 + ', ' + address.line_2);
                    $('[name=start_post_code]').val(address.postal_code);
                    $('[name=start_country]').val(address.country_name);
                    $('[name=start_district]').val(address.locality);
                } else if (_this.trgClass == 'end') {
                    $('[name=end_town_or_city]').val(address.line_1 + ', ' + address.line_2);
                    $('[name=end_post_code]').val(address.postal_code);
                    $('[name=end_country]').val(address.country_name);
                    $('[name=end_district]').val(address.locality);
                } else {
                    $('.way_point' + trgKey + ' .city').val(address.line_1 + ', ' + address.line_2);
                    $('.way_point' + trgKey + ' .postcode').val(address.postal_code);
                    $('.way_point' + trgKey + ' .country').val(address.country_name);
                    $('.way_point' + trgKey + ' .district').val(address.locality);
                }
            }
        });
    }

    function initFechify() {
        var config = {
            accessToken: '<?= FECHIFY_ACCESS_TOKEN ?>', // Replace this with your access token
            gfxMode: 1,
            countryMatchWith: "text",
            enabledCountries: ["United Kingdom"],
            domMode: 'name',
            placeholders: false,
            onResultSelected: function(c2a, elements, address) {
                let trgClass;
                if ($(elements.search).hasClass('start')) {
                    trgClass = $('.start');
                } else if ($(elements.search).hasClass('end')) {
                    trgClass = $('.end');
                }
                if (address.line_1) {
                    trgClass.val(address.line_1 + ', ' + address.line_2 + ', ' + address.locality + ', ' + address.postal_code);
                } else {
                    trgClass.val(address.company_name + ', ' + address.locality + ', ' + address.postal_code);
                }
            },
        };
        cc = new clickToAddress(config);
        cc.attach({
            search: 'start',
            town: 'start_town_or_city',
            postcode: 'start_post_code',
            county: 'start_country',
            line_1: 'start_district'
        });
        cc.attach({
            search: 'end',
            town: 'end_town_or_city',
            postcode: 'end_post_code',
            county: 'end_country',
            line_1: 'end_district'
        });
    }

    function numberTwoDigit(n) {
        return n > 9 ? "" + n : "0" + n;
    }

    function objectifyForm(inp) {
        var rObject = {};
        for (var i = 0; i < inp.length; i++) {
            if (inp[i]['name'].substr(inp[i]['name'].length - 2) == "[]") {
                var tmp = inp[i]['name'].substr(0, inp[i]['name'].length - 2);
                if (Array.isArray(rObject[tmp])) {
                    rObject[tmp].push(inp[i]['value']);
                } else {
                    rObject[tmp] = [];
                    rObject[tmp].push(inp[i]['value']);
                }
            } else {
                rObject[inp[i]['name']] = inp[i]['value'];
            }
        }

        return rObject;
    }
</script>