<form class="forms" id="quote-form" method="post" v-show="screen == 'payment'" style="display: none;">
    <div class="box-header">
        <h3 class="box-title">Make a Payment</h3>
        <div class="pull-right">
            <button type="button" class="btn btn-lg btn-flat btn-primary" @click="getQuote()"> Submit</button>
        </div>
    </div><!-- /.box-header -->
    <!-- Tab panes -->
    <div class="tab-content ">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row english">
                <div class="col-sm-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Credit holder name <span class="text-danger">*</span></label>
                                                <input class="form-control auto-tab" type="text" name="card_holder_name" placeholder="Enter credit holder name" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Credit card number  <span class="text-danger">*</span></label>
                                                <input class="form-control auto-tab" type="text" name="card_number" placeholder="Enter credit card number" maxlength="16" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Expiry month <span class="text-danger">*</span></label>
                                                <input class="form-control auto-tab" name="exp_month" placeholder="MM" required min="1" max="12" maxlength="2">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Expiry year  <span class="text-danger">*</span></label>
                                                <input class="form-control auto-tab" name="exp_year" placeholder="YY" required min="<?= date('y') ?>" max="99" maxlength="2">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>CVC <span class="text-danger">*</span></label>
                                                <input class="form-control auto-tab" name="card_cvc" placeholder="1234" maxlength="4" required>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(function() {

        $('.auto-tab').keyup(function() {
            auto_tab($(this));
        });

        $('[name=card_number]').validateCreditCard(function(result) {
            if (result.card_type) {

                var cardLength, cvvLength;

                cardLength = result.card_type.valid_length[0];

                if (result.card_type.name === 'visa' || result.card_type.name === 'visa_electron' || result.card_type.name === 'mastercard') {
                    cvvLength = 3;
                } else if (result.card_type.name === 'amex') {
                    cvvLength = 4;
                } else {
                    cvvLength = 4;
                }

                $('[name=card_number]').attr('maxLength', cardLength);
                $('[name=card_code]').attr('maxLength', cvvLength);

                auto_tab($(this));

                $('#card-image img').attr('src', '<?= base_url('assets/images/cc') ?>/' + result.card_type.name + '.png');
            } else {
                $('#card-image img').attr('src', '<?= base_url('assets/images/cc/default.png') ?>');
            }
        });
    });

    function auto_tab(element) {
        if (element.val().length == element.attr('maxLength')) {
            var inputs = element.closest('form').find('.auto-tab');
            inputs.eq(inputs.index(element) + 1).focus();
        }
    }
</script>