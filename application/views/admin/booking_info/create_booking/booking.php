<!-- <h2 v-show="screen == 'vehicle-selection'" style="display: none;">Vehicle Selection</h2> -->

<div v-show="screen == 'booking-form'" style="display: none;">
    <div class="box-header">
        <h3 class="box-title">Location Details</h3>
    </div><!-- /.box-header -->
    <!-- Tab panes -->
    <div class="tab-content ">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="row english">
                <div class="col-sm-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h5>Pick up:</h5>
                                                <span>{{booking[journey_type].quote.start}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h5>Drop off:</h5>
                                                <span>{{booking[journey_type].quote.end}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                <form id="booking-info-form" method="post">
                                        <div class="box-header">
                                            <h3 class="box-title">Passenger Detail | Add</h3>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-lg btn-flat btn-primary" @click="submitOneBooking()"> Save</button>
                                            </div>
                                        </div><!-- /.box-header -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Head Passenger Full Name</label>
                                                    <input type="text" class="form-control" name="name" v-model="booking.one_way.booking_details.name" required>
                                                    <ul class='validation'></ul>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Mobile Number</label>
                                                    <!--<span class="addon"><img src="<?= base_url('assets/images') ?>/phone2.svg"></span>-->
                                                    <input type="hidden" name="phone_cc">
                                                    <input type="number" class="form-control countrycode" name="phone" v-model="booking.one_way.booking_details.phone" required>
                                                    <ul class='validation'></ul>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Passengers </label>
                                                    <select class="form-control" name="passenger" required v-if="booking.one_way.selected_fleet != ''">
                                                        <option v-for="pax in booking.one_way.selected_fleet.passengers" :value="pax">
                                                            {{pax}} Person
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Luggages</label>
                                                    <select class="form-control" name="suitcase" required v-if="booking.one_way.selected_fleet != ''">
                                                        <option value="0">0 Luggage</option>
                                                        <option v-for="lug in booking.one_way.selected_fleet.suitcases" :value="lug">
                                                            {{lug}} Luggage
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" v-if="booking.one_way.quote.is_start_airport">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Flight Arriving From</label>
                                                    <input type="text" class="form-control" name="flight_arrive_from" v-model="booking.one_way.booking_details.flight_arrive_from" required>
                                                    <ul class='validation'></ul>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Flight Number</label>
                                                    <input type="text" class="form-control" minlength="2" name="flight_number" v-model="booking.one_way.booking_details.flight_number" required>
                                                    <ul class='validation'></ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group" v-if="booking.one_way.quote.is_start_airport">
                                                    <label for="meet_and_greet">Meet & Greet Service</label>
                                                    <span class="addon"><img src="<?= base_url('assets/images') ?>/meet-greet.svg"></span>                                                    
                                                    <!-- <select class="form-control" name="meet_and_greet" required>
                                                        <option value="0" v-html="meet_and_greet.desc"></option>
                                                        <option value="1" v-html="meet_and_greet.excerpt"></option>
                                                    </select> -->
                                                    <span style="margin-left: 10px;"><input type="checkbox" name="meet_and_greet" id="meet_and_greet" value="5.00" style="transform: scale(1.5);" /></span>
                                                    <div class="">
                                                        <span class="d-inline"><img src="<?= base_url('assets/images') ?>/info.svg">
                                                            <?= CURRENCY ?> 5 will be charge for Meet & Greet Service.
                                                        </span>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group date-time-validation">
                                                    <label v-if="booking.one_way.quote.is_start_airport">Arrival Date & Time</label>
                                                    <label v-else>Pick-Up Date & Time</label>
                                                    <span class="addon"><img src="<?= base_url('assets/images') ?>/calendar.svg"></span>
                                                    <input type="text" class="form-control datetime-picker" name="pickup_date_time" v-model="booking.one_way.booking_details.pickup_date_time" required>
                                                    <ul class='validation'></ul>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>