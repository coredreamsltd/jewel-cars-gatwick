<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Yearly Booking List</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/booking/add_update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-striped" id="dt-table">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th>Driver</th>
                            <th>Passenger</th>
                            <th>Fleet</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Home Pickup Time</th>
                            <th>School Pickup Time</th>
                            <th>Pickup Address</th>
                            <th>Dropoff Address</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  if (!empty($yearly_bookings)): foreach($yearly_bookings as $booking) :?>
                            <tr>
                                <td><?= $booking->id ?></td>
                                <td><?= $booking->driver->name ?></td>
                                <td><?= $booking->passenger->full_name ?></td>
                                <td><?= $booking->fleet->title ?></td>
                                <td><?= $booking->start_date ?></td>
                                <td><?= $booking->end_date ?></td>
                                <td><?= date('h:i a', strtotime($booking->home_pickup_time)) ?></td>
                                <td><?= date('h:i a', strtotime($booking->school_pickup_time)) ?></td>
                                <td><?= $booking->pickup_address ?></td>
                                <td><?= $booking->dropoff_address ?></td>
                                <td><?= $booking->status ? '<label class="label label-success">ACTIVE</label>' : '<label class="label label-danger">IN ACTIVE</label>' ?></td>
                                <td>
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action
                                            <span class="fa fa-caret-down"></span></button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="<?php echo base_url('admin/booking/add_update/' . $booking->id) ?>">
                                                    <i class="fa fa-edit text-primary"></i>Edit</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('admin/booking/view_yearly_booking/' . $booking->id) ?>">
                                                    <i class="fa fa-eye text-primary"></i>View</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; endif ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box-body -->

    </div><!-- /.box -->
</div>
</div><!-- /.row -->

<script>
    var SITE_URL = '<?= site_url() ?>';
    $(document).ready(function() {
        $('#dt-table').dataTable();
    });
</script>