<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <!-- <div class="box-header">
                <h3 class="box-title">Yearly Booking Details</h3>
            </div> -->
            <div class="box-body">
                <div class="col-md-6">
                    <h4>Yearly Booking Details</h4>
                    <table class="table">
                        <tr>
                            <th>Driver</th>
                            <td><?= $booking->driver->name ?></td>
                        </tr>
                        <tr>
                            <th>Passenger</th>
                            <td><?= $booking->passenger->full_name ?></td>
                        </tr>
                        <tr>
                            <th>Fleet</th>
                            <td><?= $booking->fleet->title ?></td>
                        </tr>
                        <tr>
                            <th>Start Date</th>
                            <td><?= $booking->start_date ?></td>
                        </tr>
                        <tr>
                            <th>End Date</th>
                            <td><?= $booking->end_date ?></td>
                        </tr>
                        <tr>
                            <th>Home Pickup Time</th>
                            <td><?= date('h:i a', strtotime($booking->home_pickup_time)) ?></td>
                        </tr>
                        <tr>
                            <th>School Pickup Time</th>
                            <td><?= date('h:i a', strtotime($booking->school_pickup_time)) ?></td>
                        </tr>
                        <tr>
                            <th>Pickup Address</th>
                            <td><?= $booking->pickup_address ?></td>
                        </tr>
                        <tr>
                            <th>Dropoff Address</th>
                            <td><?= $booking->dropoff_address ?></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td><?= $booking->status ? '<label class="label label-success">ACTIVE</label>' : '<label class="label label-danger">IN ACTIVE</label>' ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <h4>Yearly Booking Schedule</h4>
                    <table class="table table-bordered" id="dt-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Home Pickup Time</th>
                                <th>School Pickup Time</th>
                                <th>Pickup Date</th>
                                <th>Is Emailed?</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if (!empty($booking_logs)) :
                                foreach ($booking_logs as $booking) :
                                    ?>
                                    <tr>
                                        <td><?= $booking->id ?></td>
                                        <td><?= date('h:i a', strtotime($booking->home_pickup_time)) ?></td>
                                        <td><?= date('h:i a', strtotime($booking->school_pickup_time)) ?></td>
                                        <td><?= $booking->pickup_date ?></td>
                                        <td><?= $booking->is_email_sent ? '<i class="fa fa-check fa-2x text-success"></i>' : '<i class="fa fa-times-circle fa-2x text-danger"></i>' ?></td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->