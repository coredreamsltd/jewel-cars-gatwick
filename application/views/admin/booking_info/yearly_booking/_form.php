<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Yearly Booking | <?php echo ($is_edit) ? 'Update' : 'Add' ?></h3>

            </div><!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" class="forms" enctype="multipart/form-data">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Driver<span class="text-danger">*</span></label>
                                            <select class="form-control" name="driver_id" required>
                                                <option selected>Select Driver</option>
                                                <?php foreach($drivers as $driver) :?>
                                                    <option value="<?= $driver->id ?>" <?= ($is_edit && $driver->id == $yearly_booking->driver->id) ? 'selected' : '' ?>><?= $driver->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Passenger<span class="text-danger">*</span></label>
                                            <select class="form-control" name="passenger_id" required>
                                                <option selected>Select Passenger</option>
                                                <?php foreach($passengers as $passenger) :?>
                                                    <option value="<?= $passenger->id ?>" <?= ($is_edit && $passenger->id == $yearly_booking->passenger->id) ? 'selected' : '' ?>><?= $passenger->full_name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Fleet<span class="text-danger">*</span></label>
                                            <select class="form-control" name="fleet_id" required>
                                                <option selected>Select Fleet</option>
                                                <?php foreach($fleets as $fleet) :?>
                                                    <option value="<?= $fleet->id ?>" <?= ($is_edit && $fleet->id == $yearly_booking->fleet->id) ? 'selected' : '' ?>><?= $fleet->title ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Start Date<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control datepicker" name="start_date" value="<?= $is_edit ? $yearly_booking->start_date : '' ?>" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>End Date<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control datepicker" name="end_date" value="<?= $is_edit ? $yearly_booking->end_date : '' ?>" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Home Pickup Time<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control bk-timepicker" name="home_pickup_time" value="<?= $is_edit ? $yearly_booking->home_pickup_time : '' ?>" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>School Pickup Time<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control bk-timepicker" name="school_pickup_time" value="<?= $is_edit ? $yearly_booking->school_pickup_time : '' ?>" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Pickup Address<span class="text-danger">*</span></label>
                                            <input name="pickup_address" class="form-control" required="" placeholder="eg; airports, postcodes, seaports" value="<?= $is_edit ? $yearly_booking->pickup_address : '' ?>">
                                            <!-- <input type="hidden" name="start"> -->
                                            <input type="hidden" name="start_id">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Dropoff Address<span class="text-danger">*</span></label>
                                            <input name="dropoff_address" class="form-control" required="" placeholder="eg; airports, postcodes, seaports" value="<?= $is_edit ? $yearly_booking->dropoff_address : '' ?>">
                                            <!-- <input type="hidden" name="end"> -->
                                            <input type="hidden" name="end_id">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Status <span class="text-danger">*</span></label><br>
                                            <label class="text-success"><input type="radio" name="status" value="1" checked> ACTIVE</label>
                                            <label class="text-danger"><input type="radio" name="status" value="0" <?php echo ($is_edit && !$yearly_booking->status) ? 'checked' : '' ?>> IN-ACTIVE</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><input class="btn btn-primary" type="submit" value="Save"></td>
                            </tr>
                        </tbody>
                    </table>
                    <!--  </form> -->
                </form>
            </div>

        </div><!-- /.box -->
    </div>
</div>

<script>
    var SITE_URL = '<?= site_url() ?>';
    var BASE_URL = '<?= base_url() ?>';
    var CURRENCY = '<?= CURRENCY ?>';
    var CURRENT_USER = 'admin';
    $(document).ready(function() {
        $('.bk-timepicker').timepicker({
            timeFormat: 'h:i a',
            step: 5,
            disableTouchKeyboard: true,
        });

        $('.datepicker').datepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });

        // location
        $("[name=pickup_address]").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: SITE_URL + "admin/booking/ajax_getLocations",
                    data: {
                        q: request.term
                    },
                    success: function(data) {
                        var parsedData = jQuery.parseJSON(data);
                        response(parsedData.data);
                    }
                });
            },
            select: function(event, ui) {
                $("[name=start_id]").val(ui.item.id);
                // $("[name=start]").val(ui.item.label);

            }
        });

        $("[name=dropoff_address]").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: SITE_URL + "admin/booking/ajax_getLocations",
                    data: {
                        q: request.term
                    },
                    success: function(data) {
                        var parsedData = jQuery.parseJSON(data);
                        response(parsedData.data);
                    }
                });
            },
            select: function(event, ui) {
                $("[name=end_id]").val(ui.item.id);
                // $("[name=end]").val(ui.item.label);
            }
        });
    });
</script>