<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <form action="" method="post">
            <div class="box">
                <div class="box-header" style="padding: 10px 0; margin-right: 10px;">
                    <h3 class="box-title">Booking Details for booking id:
                        <strong> <?= $booking_infos->booking_ref_id ?></strong></h3>
                    <div class="pull-right">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save Changes</button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="table-details">
                                <h4>Personal Details</h4>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td><input type="text" class="form-control" name="client_name" value="<?= $booking_infos->client_name ?>"></td>
                                        </tr>
                                        <?php if ($booking_infos->lead_passenger_name) : ?>
                                            <tr>
                                                <th>Lead Passenger Name</th>
                                                <td><input type="text" class="form-control" name="lead_passenger_name" value="<?= $booking_infos->lead_passenger_name ?>"></td>
                                            </tr>
                                            <tr>
                                                <th>Lead Passenger Contact No.</th>
                                                <td>
                                                    <!--                                                <input type="text" name="lead_phone_cc"-->
                                                    <!--                                                       value="-->
                                                    <?//= $booking_infos->lead_phone_cc ?>
                                                    <!--">-->
                                                    <input type="text" class="form-control" name="lead_passenger_phone" value="<?= $booking_infos->lead_passenger_phone ?>"></td>
                                            </tr>
                                        <?php endif; ?>
                                        <tr>
                                            <th>Contact No.</th>
                                            <td>
                                                <input type="hidden" name="phone_cc" value="<?= $booking_infos->phone_cc ?>">
                                                <input type="text" class="form-control call-code" name="client_phone" value="<?= $booking_infos->client_phone ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td><input type="text" class="form-control" name="client_email" value="<?= $booking_infos->client_email ?>"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-details">
                                <h4>Travel Details</h4>
                                <table class="table">
                                    <tbody>
                                        <!-- <tr>
                                            <th>Trip Type</th>
                                            <td>
                                                <label class="radio-inline">
                                                    <input type="radio" name="journey_type" value="one_way" <?= $booking_infos->journey_type == 'one_way' ? 'checked' : '' ?>>
                                                    One Way
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="journey_type" value="two_way" <?= $booking_infos->journey_type == 'two_way' ? 'checked' : '' ?>>
                                                    Round Trip
                                                </label>
                                            </td>
                                        </tr> -->
                                        <tr>
                                            <th>Pickup Date</th>
                                            <td><input type="text" class="form-control datepicker" name="pickup_date" value="<?= date('d/m/Y',strtotime($booking_infos->pickup_date)) ?>"></td>
                                        </tr>
                                        <tr>
                                            <th>Pickup Time</th>
                                            <td><input type="text" class="form-control bk-timepicker" name="pickup_time" value="<?= $booking_infos->pickup_time ?>"></td>
                                        </tr>

                                        <!-- <tr class="return">
                                            <th>Return Date</th>
                                            <td><input type="text" class="form-control datepicker" name="client_return_date" value="<?= $booking_infos->client_return_date ?>" required></td>
                                        </tr>
                                        <tr class="return">
                                            <th>Return Time</th>
                                            <td><input type="text" class="form-control bk-timepicker" name="client_return_time" value="<?= $booking_infos->client_return_time ?>" required></td>
                                        </tr> -->
                                        <tr>
                                            <th>From</th>
                                            <td><input type="text" class="form-control map" name="pickup_address" value="<?= $booking_infos->pickup_address ?>"></td>
                                        </tr>
                                        <?php if ($booking_infos->via_point) : ?>
                                            <tr>
                                                <th> Via Points</th>
                                                <td>
                                                    <?php foreach (json_decode($booking_infos->via_point) as $via) : ?>
                                                        <input type="text" class="form-control map" name="via_point[]" value="<?= $via ?>">
                                                    <?php endforeach; ?>

                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        <tr>
                                            <th>To</th>
                                            <td><input type="text" class="form-control map" name="dropoff_address" value="<?= $booking_infos->dropoff_address ?>"></td>
                                        </tr>
                                        <?php if ($booking_infos->pickup_address_door_no) : ?>
                                            <tr>
                                                <th>Pickup Address House/Flat no./Door no.</th>
                                                <td><input type="text" class="form-control " name="pickup_address_door_no" value="<?= $booking_infos->pickup_address_door_no ?>"></td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if ($booking_infos->pickup_address_line) : ?>
                                            <tr>
                                                <th>Pickup Address Line</th>
                                                <td><input type="text" class="form-control " name="pickup_address_line" value="<?= $booking_infos->pickup_address_line ?>"></td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if ($booking_infos->pickup_address_town_city) : ?>
                                            <tr>
                                                <th>Pickup Address Town/City</th>
                                                <td><input type="text" class="form-control" name="pickup_address_town_city" value="<?= $booking_infos->pickup_address_town_city ?>"></td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if ($booking_infos->pickup_address_post_code) : ?>
                                            <tr>
                                                <th>Pickup Address Postcode</th>
                                                <td><input type="text" class="form-control " name="pickup_address_post_code" value="<?= $booking_infos->pickup_address_post_code ?>"></td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if ($booking_infos->dropoff_address_door_no) : ?>
                                            <tr>
                                                <th>Dropoff Address House/Flat no./Door no.</th>
                                                <td><input type="text" class="form-control " name="dropoff_address_door_no" value="<?= $booking_infos->dropoff_address_door_no ?>"></td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if ($booking_infos->dropoff_address_line) : ?>
                                            <tr>
                                                <th>Dropoff Address Line</th>
                                                <td><input type="text" class="form-control " name="dropoff_address_line" value="<?= $booking_infos->dropoff_address_line ?>"></td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if ($booking_infos->dropoff_address_town_city) : ?>
                                            <tr>
                                                <th>Dropoff Address Town/City</th>
                                                <td><input type="text" class="form-control" name="dropoff_address_town_city" value="<?= $booking_infos->dropoff_address_town_city ?>"></td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if ($booking_infos->dropoff_address_postcode) : ?>
                                            <tr>
                                                <th>Dropoff Address Postcode</th>
                                                <td><input type="text" class="form-control " name="dropoff_address_postcode" value="<?= $booking_infos->dropoff_address_postcode ?>"></td>
                                            </tr>
                                        <?php endif; ?>

                                        <tr>
                                            <th>Car</th>
                                            <td>
                                                <select class="form-control" name="fleet_id" required>
                                                    <?php foreach ($fleets as $fleet) : ?>
                                                        <option value="<?= $fleet->id . '-|-' . $fleet->title ?>" <?= $booking_infos->selected_fleet_id == $fleet->id ? 'selected' : '' ?>>
                                                            <?= $fleet->title ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>No. of Passengers</th>
                                            <td><input type="text" class="form-control" name="client_passanger_no" value="<?= $booking_infos->client_passanger_no ?>"></td>
                                        </tr>
                                        <tr>
                                            <th>Suitcase</th>
                                            <td><input type="text" class="form-control" name="client_luggage" value="<?= $booking_infos->client_luggage ?>"></td>
                                        </tr>
                                        <tr>
                                            <th>Hand Luggage</th>
                                            <td><input type="text" class="form-control" name="client_hand_luggage" value="<?= $booking_infos->client_hand_luggage ?>"></td>
                                        </tr>
                                        <tr>
                                            <th>Baby Seats</th>
                                            <td><input type="text" class="form-control" name="client_baby_no" value="<?= $booking_infos->client_baby_no ?>"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <?php if (isAirport((array) $booking_infos)) : ?>
                                <div class="wrapper-terminal-info">
                                    <div class="table-details">
                                        <h4>Flight Arrival Details</h4>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Flight No.</th>
                                                    <td><input type="text" class="form-control" name="flight_number" value="<?= $booking_infos->flight_number ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Flight Arriving From</th>
                                                    <td><input type="text" class="form-control" name="flight_arrive_from" value="<?= $booking_infos->flight_arrive_from ?>"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (isSeaport((array) $booking_infos)) : ?>
                                <div class="wrapper-terminal-info">
                                    <div class="table-details">
                                        <h4>Seaport Details</h4>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Cruise Name</th>
                                                    <td><input type="text" class="form-control" name="cruise_name" value="<?= $booking_infos->cruise_name ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Coming From</th>
                                                    <td><input type="text" class="form-control" name="cruise_arrive_from" value="<?= $booking_infos->cruise_arrive_from ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Driver Meeting Point</th>
                                                    <td><input type="text" class="form-control" name="meeting_terminal" value="<?= $booking_infos->meeting_terminal ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Pick up time after docking</th>
                                                    <td><input type="text" class="form-control" name="cruise_after_landing_time" value="<?= $booking_infos->cruise_after_landing_time ?>">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($booking_infos->journey_type == 'two_way') : ?>
                                <?php if (isEndAirport((array) $booking_infos)) : ?>
                                    <div class="wrapper-terminal-info">
                                        <div class="table-details">
                                            <h4>Return Flight Arrival Details</h4>
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th>Flight No.</th>
                                                        <td><input type="text" class="form-control" name="return_flight_no" value="<?= $booking_infos->return_flight_no ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Flight Arriving From</th>
                                                        <td><input type="text" class="form-control" name="return_flight_arrive_from" value="<?= $booking_infos->return_flight_arrive_from ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Pickup after landing time</th>
                                                        <td><input type="text" class="form-control" name="return_after_landing_time" value="<?= $booking_infos->return_after_landing_time ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Display Name</th>
                                                        <td>
                                                            <input type="text" class="form-control" name="return_display_name" value="<?= $booking_infos->return_display_name ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Driver Meeting Point</th>
                                                        <td><input type="text" class="form-control" name="meeting_terminal_return" value="<?= $booking_infos->meeting_terminal_return ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Travelling Class</th>
                                                        <td>
                                                            <select class="form-control" name="return_class_type" required>
                                                                <option value="business-class" <?= $booking_infos->return_class_type == 'business-class' ? 'selected' : '' ?>>
                                                                    Business Class
                                                                </option>
                                                                <option value="economy-class" <?= $booking_infos->return_class_type == 'economy-class' ? 'selected' : '' ?>>
                                                                    Economy Class
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Passport Type</th>
                                                        <td>
                                                            <select class="form-control" name="return_password_type" required>
                                                                <option value="uk-passport" <?= $booking_infos->return_password_type == 'uk-passport' ? 'selected' : '' ?>>
                                                                    UK
                                                                </option>
                                                                <option value="european-passport" <?= $booking_infos->return_password_type == 'european-passport' ? 'selected' : '' ?>>
                                                                    European
                                                                </option>
                                                                <option value="other-passport" <?= $booking_infos->return_password_type == 'other-passport' ? 'selected' : '' ?>>
                                                                    others
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if (isEndSeaport((array) $booking_infos)) : ?>
                                    <div class="wrapper-terminal-info">
                                        <div class="table-details">
                                            <h4>Return Seaport Details</h4>
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th>Cruise Name</th>
                                                        <td><input type="text" class="form-control" name="return_cruise_name" value="<?= $booking_infos->return_cruise_name ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Coming From</th>
                                                        <td><input type="text" class="form-control" name="return_cruise_arrive_from" value="<?= $booking_infos->return_cruise_arrive_from ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Driver Meeting Point</th>
                                                        <td><input type="text" class="form-control" name="meeting_terminal_return" value="<?= $booking_infos->meeting_terminal_return ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Pick up time after docking</th>
                                                        <td><input type="text" class="form-control" name="return_cruise_after_landing_time" value="<?= $booking_infos->return_cruise_after_landing_time ?>">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>

                            <div class="table-details">
                                <h4>Other Details</h4>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Special Instruction</th>
                                            <td><textarea class="form-control" name="special_instruction"><?= $booking_infos->message ?></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="table-details">
                                <h4>Booking Status</h4>
                                <p class="text-danger">This content is not editable from here.</p>
                            </div>
                            <div class="table-details">
                                <h4>Payment Details</h4>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Payment Type</th>
                                            <td>
                                                <!-- <label class="radio-inline">
                                                    <input type="radio" name="pay_method" value="debit-card" <?= $booking_infos->pay_method == 'debit-card' ? 'checked' : '' ?>>
                                                    Debit Card
                                                </label> -->
                                                <label class="radio-inline">
                                                    <input type="radio" name="pay_method" value="card" <?= $booking_infos->pay_method == 'card' ? 'checked' : '' ?>>
                                                    Credit Card
                                                </label>
                                                <!-- <label class="radio-inline">
                                                    <input type="radio" name="pay_method" value="cash" <?= $booking_infos->pay_method == 'cash' ? 'checked' : '' ?>>
                                                    Cash
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="pay_method" value="paypal" <?= $booking_infos->pay_method == 'paypal' ? 'checked' : '' ?>>
                                                    Paypal
                                                </label> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Journey Total Fare</th>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"> <?= CURRENCY ?></span>
                                                    <input type="text" class="form-control" name="total_fare" value="<?= $booking_infos->total_fare ?>">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </form>
    </div>
</div><!-- /.row -->
<script>
    function checkJourneyType(type) {
        if (type == 'two_way') {
            $('.return').find('input').removeAttr('disabled', '');
            $('.return').show();
        } else {
            $('.return').find('input').attr('disabled', '');
            $('.return').hide();

        }
    }
    $(document).ready(function() {
        checkJourneyType('<?= $booking_infos->journey_type ?>');
        $('[name=journey_type]').change(function() {
            checkJourneyType($(this).val());
        });
        $('.bk-timepicker').timepicker({
            timeFormat: 'H:i',
            step: 5,
            disableTouchKeyboard: true,
        });
        check_all_terminals();

        $('.select-terminal').change(function() {
            check_single_terminal(this);
        });

    });

    function change_fleet(fleet_title) {
        $('[name=fleet_title]').val(fleet_title);
    }

    function check_single_terminal(select_terminal) {

        var terminal_id = $(select_terminal).find(':selected').attr('data-terminal-id');

        if (terminal_id == "" || typeof(terminal_id) === 'undefined') {
            return;
        }

        $.ajax({
            url: SITE_URL + "index/ajax_get_terminal_details/" + terminal_id,
            dataType: 'json',
            success: function(response) {

                if (!response.status) {
                    alert(response.msg);
                    return;
                }

                $(select_terminal).parents('.form-group').find('.terminal-info').html(response.data.description);
                $(select_terminal).parents('.wrapper-terminal-info').find('textarea.hidden-terminal-info').html(response.data.description);

            },
            error: function() {
                alert('Server error occured.');
            }
        });
    }

    function check_all_terminals() {
        var select_terminals = $('.select-terminal');

        $.each(select_terminals, function(index, select_terminal) {
            check_single_terminal(select_terminal);
        });

    }
</script>
<script>
    $(function() {
        $('.call-code').intlTelInput({
            separateDialCode: true,
            initialCountry: "auto"
        });
        $('.call-code').intlTelInput("setCountry", '<?= getCountryByCountryCode((!empty($booking_infos->phone_cc) ? $booking_infos->phone_cc : 'uk')) ?>');
        $("input").on("countrychange", function(e, countryData) {
            $('[name=phone_cc]').val('+' + countryData.dialCode);
        });
    });
</script>
