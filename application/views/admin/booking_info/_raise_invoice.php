<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Payment Confirmation for - Booking ID: <strong><?= $_GET['booking_id'] ?></strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-raise-invoice" method="post">
                            <input type="hidden" name="booking_id" value="<?= $_GET['booking_id'] ?>">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Payment Request?</label>
                                            <div class="input-group col-md-4">
                                                <label class="radio-inline">
                                                    <input type="radio" name="is_send_email" value="1" checked=""> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="is_send_email" value="0"> No
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Customer Name</label>
                                            <div class="input-group col-md-4">
                                                <span class="input-group-addon"><i class="fa fa-user"></i> </span>
                                                <input type="hidden" name="customer_name" value="<?= $booking->client_name ?>">
                                                <input type="text" class="form-control" value="<?= $booking->client_name ?>" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php
                                            $recipients = $booking->client_email;

                                            ?>
                                            <label class="control-label">
                                                Recipients Email
                                                <a href="#!" data-toggle="tooltip" title="If more than one email please separate emails with a comma (,) but with no space."><i class="fa fa-info-circle"></i></a>
                                            </label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input class="form-control" name="customer_email" value="<?= $recipients ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Request Amount</label>
                                            <div class="input-group col-md-2">
                                                <span class="input-group-addon"><i class="fa fa-gbp"></i> </span>
                                                <?php
                                                $due = $this->invoice_model->get_due_amount($booking->booking_ref_id);
                                                ?>
                                                <input class="form-control number" name="invoice_amount" value="<?= $due ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Subject</label>
                                            <div class="input-group col-md-12">
                                                <span class="input-group-addon"><i class="fa fa-pencil"></i> </span>
                                                <input class="form-control" name="mail_subject" value="Payment Confirmation for - Booking ID: <?= $booking->booking_ref_id ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Message</label>
                                            <textarea class="form-control" name="mail_message" id="message" required="">
                                                We have received your booking. Please use the link at the bottom to make the payment.
                                            </textarea>
                                            <script type="text/javascript">
                                                var editor = CKEDITOR.replace('message', {
                                                    customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                                });
                                                CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-search"> </i> Preview Request Payment Email
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-raise-invoice">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <form action="" id="form-invoice" name="form_invisible" method="post">
                    <input type="hidden" name="i_is_send_email">
                    <input type="hidden" name="i_invoice_id">
                    <input type="hidden" name="i_booking_id">
                    <input type="hidden" name="i_customer_email">
                    <input type="hidden" name="i_invoice_amount">
                    <input type="hidden" name="i_mail_subject">
                    <input type="hidden" name="i_mail_template">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitting...'; this.form.submit();">Send Payment Request Email</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(function() {
        $('#form-raise-invoice').submit(function() {
            if ($(this).valid()) {

                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();

                $.post(
                    '<?= site_url('admin/booking/ajax_preview_invoice') ?>', {
                        'formdata': $(this).serialize()
                    },
                    function(response) {

                        response = jQuery.parseJSON(response);

                        $('#form-invoice [name=i_is_send_email]').val(response.data.is_send_email);
                        $('#form-invoice [name=i_invoice_id]').val(response.data.invoice_id);
                        $('#form-invoice [name=i_booking_id]').val(response.data.booking_id);
                        $('#form-invoice [name=i_customer_email]').val(response.data.customer_email);
                        $('#form-invoice [name=i_invoice_amount]').val(response.data.invoice_amount);
                        $('#form-invoice [name=i_mail_subject]').val(response.data.mail_subject);
                        $('#form-invoice [name=i_mail_template]').val(response.data.mail_template);

                        $('#modal-raise-invoice .modal-body').html(response.data.mail_template);
                        $('#modal-raise-invoice .modal-header h4').html("<strong>Mail Subject: </strong>" + response.data.mail_subject);
                        $('#modal-raise-invoice').modal();
                    }
                );
                return false;

            } else {
                alert('Please make sure all the required fields are filled properly.');
            }
        });
    });

    function mySubmitFunction(e) {
        e.preventDefault();
    }
</script>
