<?php
$isNew = true;
if(segment(4) != '') {
    $faq = $faq[0];
    $isNew = false;
}
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">FAQ's Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" class="forms">
                    <table class="table table-bordered">
                        <tr>
                            <td>
                                <label>Order <span class="text-danger">*</span></label>
                                <input class="form-control digits required" type="text" name="order" value="<?php echo (!$isNew) ? $faq['order'] : '' ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Title <span class="text-danger">*</span></label>
                                <input class="form-control required" type="text" name="title" placeholder="Enter title" value="<?php echo (!$isNew) ? $faq['title'] : '' ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Description <span class="text-danger">*</span></label>
                                <textarea name="content" class="form-control"><?php echo (!$isNew) ? $faq['content'] : '' ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn btn-primary" type="submit" value="Save">
                            </td>
                        </tr>
                    </table>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
