<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Invoice Id: <?= $invoice->invoice_id ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <tr>
                        <th>Invoice ID</th>
                        <td><?= $invoice->invoice_id ?></td>
                    </tr>
                    <tr>
                        <th>Generated Date</th>
                        <td><?= date('d/m/Y', strtotime($invoice->created_at)) ?></td>
                    </tr>
                    <tr>
                        <th>Email Recipients</th>
                        <td><?= $invoice->customer_email ?></td>
                    </tr>
                    <tr>
                        <th>Invoice Amount</th>
                        <td><?= CURRENCY . $invoice->invoice_amount ?></td>
                    </tr>
                    <tr>
                        <th>Invoice Type</th>
                        <td>
                            <?php
                            if ($invoice->booking_id) {
                                echo 'Individual Invoice';
                            } else {
                                echo 'Bulk Invoice';
                            }
                            ?>
                        </td>
                    </tr>
                    <?php if ($invoice->is_cancelled): ?>
                        <tr>
                            <td colspan="2">
                                <span class="label label-danger">Invoice Cancelled</span>
                                <a href="<?= base_url('admin/booking/invoice/' . $invoice->invoice_id) ?>" target="_blank"><i class="fa fa-search"></i> Preview</a>
                            </td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <th>Payment Status</th>
                            <td>
                                <?php if (!$invoice->payment_status): ?>
                                    <a href="<?= base_url('admin/booking/send_receipt/' . $invoice->invoice_id) ?>" class="btn btn-default btn-sm"><i class="fa fa-check"></i> Send Receipt</a>
                                <?php else: ?>
                                    <span class="label label-success">Paid</span>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Options</th>
                            <td>
                                <?php if (!$invoice->payment_status): ?>
                                    <a href="<?= base_url('admin/booking/invoice/' . $invoice->invoice_id) ?>" class="btn btn-default btn-sm" target="_blank"><i class="fa fa-search"></i> Preview</a>
                                    <a href="#!" onclick="modal_resend_invoice('<?= $invoice->invoice_id ?>')" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Resend</a>
                                    <a href="<?= base_url('admin/booking/cancel_invoice/' . $invoice->invoice_id) ?>" class="btn btn-default btn-sm" onclick="return confirm('Are you sure to cancel?')" class="text-danger"><i class="fa fa-times"></i> Cancel</a>
                                <?php else: ?>
                                    <a href="<?= base_url('admin/booking/receipt/' . $invoice->invoice_id) ?>" class="btn btn-default btn-sm" target="_blank"><i class="fa fa-search"></i> Preview Receipt</a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->


<div class="modal fade" tabindex="-1" role="dialog" id="modal-preview-resend-invoice">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post" action="<?= base_url('admin/booking/resend_invoice') ?>" id="form-preview-resend-invoice">
                <input type="hidden" name="invoice_id">
                <input type="hidden" name="mail_template">

                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="submit_resend_invoice()">Resend</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>

    function modal_resend_invoice(invoice_id) {

        var preview_modal = $('#modal-preview-resend-invoice');

        preview_modal.find('[name=invoice_id]').val(invoice_id);

        $.post(
                '<?= base_url('admin/booking/ajax_preview_resend_invoice') ?>',
                {invoice_id: invoice_id},
        function ($response) {
            $response = $.parseJSON($response);
            console.log($response);
            $("#modal-preview-resend-invoice .modal-body").html('');
            var $iframe = $("<iframe style='width:100%; height:600px'></iframe>").appendTo("#modal-preview-resend-invoice .modal-body");
            var iframe = $iframe[0];
            var doc = iframe.document;
            var content = $response.data.mail_template;
            if (iframe.contentDocument) {
                doc = iframe.contentDocument;
            }
            else if (iframe.contentWindow) {
                doc = iframe.contentWindow.document;
            }
            doc.open();
            doc.writeln(content);
            doc.close();

            var editable = doc.getElementById('mail-message');

            if (editable) {
                CKEDITOR.inline(editable);
            }

            preview_modal.modal();

        }
        );

    }

    function submit_resend_invoice() {
        var preview_modal = $("#modal-preview-resend-invoice");
        preview_modal.find('[name=mail_template]').val(preview_modal.find('.modal-body iframe').contents().find('body').html());
        preview_modal.find('form').submit();
    }

</script>
