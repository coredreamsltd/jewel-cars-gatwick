<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title btn-block">
                    Invoice List
                    <div class="pull-right">
<!--                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-search"><i class="fa fa-filter"></i> Filter Result</button>-->
                    </div>
                </h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <!--<div class="form-group pull-left">
                            <select name="bulk_action" onchange="performBulkAction(this.value)">
                                <option value="">- Bulk Actions -</option>
                                <option value="receipt_bulk">Send Receipt</option>
                            </select>
                        </div>-->
                    </div>
                    <div class="col-md-6">
                        <form>
                            <?php
                            if ($_GET):
                                foreach ($_GET as $k => $v):
                                    if ($k == 'search')
                                        continue;
                                    ?>
                                    <input type="hidden" name="<?= $k ?>" value="<?= $v ?>">
                                    <?php
                                endforeach;
                            endif;
                            ?>

                            <div class="form-group pull-right">
                                <label>Search: </label>
                                <input type="text" name="search" value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>">
                            </div>
                        </form>
                    </div>
                </div>
                <form action="" method="post" id="form-bulk-action">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
<!--                                    <th><input type="checkbox" name="checkall"></th>-->
                                    <th>#</th>
                                    <th>Invoice ID</th>
                                    <th>Generated Date</th>
                                    <th>Emailed To</th>
                                    <th>Invoice Amount</th>
                                    <th>Is Invoice Mailed?</th>
                                    <th>Is Receipt Mailed?</th>
                                    <th>Payment Status</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($invoices)):
                                    $count = isset($_GET['per_page']) ? $_GET['per_page'] : 0;                                
                                foreach ($invoices as $index => $i) :
                                ?>
                                <tr>
<!--                                    <td><input type="checkbox" name="checked_item[]" value="--><?//= $i['id'] ?><!--"></td>-->
                                    <td><?= ++$count ?></td>
                                    <td>
                                        <a href="<?= base_url('admin/invoice/details/' . $i['invoice_id']) ?>">
                                            <?php echo $i['invoice_id'] ?>
                                        </a>
                                        <br>
                                        <label class="label label-default">
                                            <?php
                                            if ($i['booking_id']) {
                                            echo 'Individual Invoice';
                                            } else {
                                            echo 'Bulk Invoice';
                                            }
                                            ?>
                                        </label>
                                    </td>
                                    <td><?php echo date('d/m/Y', strtotime($i['created_at'])) ?></td>
                                    <td><?php echo implode('<br>', explode(',', $i['customer_email'])) ?></td>
                                    <td><?php echo CURRENCY . $i['invoice_amount'] ?></td>
                                    <td>
                                        <?php if ($i['invoice_mailed_datetime'] > 0): ?>
                                        <a data-toggle="tooltip" data-placement="top" title="<?= $i['invoice_mailed_datetime'] ?>"><i class="fa fa-check-circle fa-2x text-success"></i></a>
                                        <?php else: ?>
                                        <i class="fa fa-times-circle fa-2x text-danger"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($i['receipt_mailed_datetime'] > 0): ?>
                                        <a data-toggle="tooltip" data-placement="top" title="<?= $i['receipt_mailed_datetime'] ?>"><i class="fa fa-check-circle fa-2x text-success"></i></a>
                                        <?php else: ?>
                                        <i class="fa fa-times-circle fa-2x text-danger"></i>
                                        <?php endif; ?>
                                    </td>

                                    <?php if ($i['is_cancelled']): ?>
                                    <td><span class="label label-danger">Invoice Cancelled</span></td>
                                    <td><a href="<?= base_url('admin/booking/invoice/' . $i['invoice_id']) ?>" class="btn btn-default btn-sm" target="_blank"><i class="fa fa-search"></i> Preview</a></td>
                                    <?php else: ?>
                                    <td>
                                        <?php if (!$i['payment_status']): ?>
                                        <span class="col-sm-12 label label-danger">Not Paid</span>
                                        <?php else: ?>
                                        <span class="col-sm-12 label label-success">Paid</span>
                                        <?php endif; ?>
                                    </td>
                                    <td>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <?php if (!$i['payment_status']): ?>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?= base_url('admin/booking/invoice/' . $i['invoice_id']) ?>" target="_blank"><i class="fa fa-search"></i> Preview Payment Request</a></li>
                                                <li><a href="#!" onclick="modal_resend_invoice('<?= $i['invoice_id'] ?>')"><i class="fa fa-refresh"></i> Resend Payment Request</a></li>
                                                <li><a href="<?= base_url('admin/booking/mark_paid/' . $i['invoice_id']) ?>" onclick="return confirm('Do you really want to mark this invoice as paid?')"><i class="fa fa-check"></i> Mark as Paid</a></li>
                                                <li><a href="<?= base_url('admin/booking/cancel_invoice/' . $i['invoice_id']) ?>" onclick="return confirm('Are you sure to cancel?')" class="text-danger"><i class="fa fa-times"></i> Cancel</a></li>
                                            </ul>
                                            <?php elseif ($i['payment_status'] &&!$i['mail_template_receipt']): ?>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?= base_url('admin/booking/send_receipt/' . $i['invoice_id']) ?>"><i class="fa fa-ticket"></i> Send Receipt</a></li>
                                            </ul>
                                            <?php elseif ($i['payment_status'] && $i['mail_template_receipt']): ?>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?= base_url('admin/booking/receipt/' . $i['invoice_id']) ?>" target="_blank"><i class="fa fa-search"></i> Preview Receipt</a></li>
                                                <li><a href="<?= base_url('admin/booking/resend_receipt/' . $i['invoice_id']) ?>"><i class="fa fa-refresh"></i> Resend Receipt</a></li>
                                            </ul>
                                            <?php endif; ?>
                                        </div>

                                    </td>
                                    <?php endif; ?>

                                </tr>
                                <?php
                                endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-4">
                        Total Entries: <?= $total_booking_rows ?> Invoices
                    </div>
                    <div class="col-md-8">
                        <div class="pull-right">
                            <style>.pagination{margin:0}</style>
                            <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>

                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->


<!-- Modal -->
<div class="modal fade" id="modal-search" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Filter Invoice Result</h4>
            </div>
            <form method="get">
                <input type="hidden" name="action" value="search">
                <div class="modal-body">  
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Generated Date (From)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control all-datepicker" name="booked_date_from">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Generated Date (To)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control all-datepicker" name="booked_date_to">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Invoice Status</label><br>
                            <label class="radio-inline">
                                <input type="radio" name="status" value="" checked=""> <strong>Any</strong>
                            </label>
                            <label class="radio-inline text-success">
                                <input type="radio" name="status" value="paid"> <strong>Paid</strong>
                            </label>
                            <label class="radio-inline text-warning">
                                <input type="radio" name="status" value="not_paid"> <strong>Not Paid</strong>
                            </label>
                            <label class="radio-inline text-danger">
                                <input type="radio" name="text-danger" value="cancelled"> <strong>Cancelled</strong>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Apply Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-preview-resend-invoice">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post" action="<?= base_url('admin/booking/resend_invoice') ?>" id="form-preview-resend-invoice">
                <input type="hidden" name="invoice_id">
                <input type="hidden" name="mail_template">

                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="submit_resend_invoice()">Resend</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function () {

        $('[name=checkall]').click(function (event) {
            if (this.checked) {
                $('tbody :checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $('tbody :checkbox').each(function () {
                    this.checked = false;
                });
            }
        });


        performBulkAction = function (bulk_action) {

            if (bulk_action == "")
                return false;

            var no_of_checked = $('[name="checked_item[]"]:checked').length;
            if (no_of_checked < 1) {
                alert('Please check atleast one item.');
                $('[name=bulk_action]').val("");
                return false;
            }

            $('#form-bulk-action').attr('action', '<?= base_url("admin/invoice/") ?>' + '/' + bulk_action).submit();
        }

    });

    function modal_resend_invoice(invoice_id) {

        var preview_modal = $('#modal-preview-resend-invoice');

        preview_modal.find('[name=invoice_id]').val(invoice_id);

        $.post(
                '<?= base_url('admin/booking/ajax_preview_resend_invoice') ?>',
                {invoice_id: invoice_id},
                function ($response) {
                    $response = $.parseJSON($response);
                    console.log($response);
                    $("#modal-preview-resend-invoice .modal-body").html('');
                    var $iframe = $("<iframe style='width:100%; height:600px'></iframe>").appendTo("#modal-preview-resend-invoice .modal-body");
                    var iframe = $iframe[0];
                    var doc = iframe.document;
                    var content = $response.data.mail_template;
                    if (iframe.contentDocument) {
                        doc = iframe.contentDocument;
                    } else if (iframe.contentWindow) {
                        doc = iframe.contentWindow.document;
                    }
                    doc.open();
                    doc.writeln(content);
                    doc.close();

                    var editable = doc.getElementById('mail-message');

                    if (editable) {
                        CKEDITOR.inline(editable);
                    }

                    preview_modal.modal();

                }
        );

    }

    function submit_resend_invoice() {
        var preview_modal = $("#modal-preview-resend-invoice");
        preview_modal.find('[name=mail_template]').val(preview_modal.find('.modal-body iframe').contents().find('body').html());
        preview_modal.find('form').submit();
    }



</script>