<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header" style="padding: 10px 0;">
                <h3 class="box-title">Generate Bulk Receipt</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-raise-invoice" method="post">
                            <input type="hidden" name="invoice_ids" value="<?= implode(',', $invoice_ids) ?>">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label class="control-label">
                                                Recipient Name
                                            </label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input class="form-control" name="customer_name" value="<?= isset($account) ? $account['fname'].' '.$account['lname']  : '' ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">
                                                Recipient Email
                                                <a href="#!" data-toggle="tooltip" title="If more than one email please separate emails with a comma (,) but with no space."><i class="fa fa-info-circle"></i></a>
                                            </label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input class="form-control" name="customer_email" value="<?= isset($account) ? $account['email'] : '' ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Receipt Amount</label>
                                            <div class="input-group col-md-2">
                                                <span class="input-group-addon"><i class="fa fa-gbp"></i> </span>
                                                <input type="hidden" name="receipt_amount" value="<?= $invoice_total ?>">
                                                <input class="form-control number" value="<?= $invoice_total ?>" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Subject</label>
                                            <div class="input-group col-md-12">
                                                <span class="input-group-addon"><i class="fa fa-pencil"></i> </span>
                                                <input class="form-control" name="mail_subject" value="<?= SITE_NAME ?> - Bulk Receipt for invoice ids: <?= implode(',', $invoice_ids) ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Message</label>
                                            <textarea class="form-control" name="mail_message" id="message" required="">
                                                
                                                The total receipt amount for invoices (<?= implode(',', $invoice_ids) ?>) is <?= SITE_CURRENCY.$invoice_total ?>

                                            </textarea>
                                            <script type="text/javascript">
                                                var editor = CKEDITOR.replace('message',
                                                        {
                                                            customConfig: '<?php echo base_url('assets/admin-assets/js/plugins/ckeditor/my_config.js') ?>'
                                                        });
                                                CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin-assets/js/plugins/ckfinder/') ?>');
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-search"> </i> Preview Invoice Email</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-raise-invoice">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" style="font-family: Times New Roman !important;">

            </div>
            <div class="modal-footer">
                <form action="" id="form-invoice" name="form_invisible" method="post">
                    <input type="hidden" name="i_invoice_ids" value="<?= implode(',', $invoice_ids) ?>">
                    <input type="hidden" name="i_customer_name">
                    <input type="hidden" name="i_customer_email">
                    <input type="hidden" name="i_receipt_amount" value="<?= $invoice_total ?>">
                    <input type="hidden" name="i_mail_subject">
                    <input type="hidden" name="i_mail_template">
                    
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Send Receipt</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    $(function () {
        $('#form-raise-invoice').submit(function () {
            if ($(this).valid()) {

                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();

                $.post(
                        '<?= base_url('admin/invoice/ajax_preview_receipt_bulk') ?>',
                        {'formdata': $('form').serialize()},
                function (response) {

                    response = jQuery.parseJSON(response);
                    
                    $('#form-invoice [name=i_customer_name]').val(response.data.customer_name);
                    $('#form-invoice [name=i_customer_email]').val(response.data.customer_email);
                    $('#form-invoice [name=i_mail_subject]').val(response.data.mail_subject);
                    $('#form-invoice [name=i_mail_template]').val(response.data.mail_template);
                    
                    $('#modal-raise-invoice .modal-body').html(response.data.mail_template);
                    $('#modal-raise-invoice .modal-header h4').html("<strong>Mail Subject: </strong>" + response.data.mail_subject);
                    $('#modal-raise-invoice').modal();
                }
                );
                return false;

            } else {
                alert('Please make sure all the required fields are filled properly.');
            }
        });
    });
</script>