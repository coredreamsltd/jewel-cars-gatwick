<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <form action="" method="post" class="forms" enctype="multipart/form-data">
                <div class="box-header">
                    <h3 class="box-title">Fleet Manager | <?php echo ($is_edit) ? 'Update' : 'Add' ?></h3>
                    <div class="pull-right">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General Info</a></li>
                        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Fare Break Down Rate</a></li>
                        <!-- <li role="presentation"><a href="#tab7" aria-controls="tab7" role="tab" data-toggle="tab">Locations</a></li> -->
                        <!-- <li role="presentation"><a href="#tab8" aria-controls="tab8" role="tab" data-toggle="tab">Routes</a></li> -->
                        <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Additional Rates</a></li>
                        <li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Airport Rates</a></li>
                        <!-- <li role="presentation"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">Seaport Rates</a></li> -->
                        <!-- <li role="presentation"><a href="#tab9" aria-controls="tab9" role="tab" data-toggle="tab">Station Rates</a></li> -->
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab1">
                            <?php $this->load->view('admin/fleet/navtab-parts/tab1') ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab3">
                            <?php $this->load->view('admin/fleet/navtab-parts/tab3') ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab4">
                            <?php $this->load->view('admin/fleet/navtab-parts/tab4') ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab5">
                            <?php $this->load->view('admin/fleet/navtab-parts/tab5') ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab6">
                            <?php $this->load->view('admin/fleet/navtab-parts/tab6') ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab7">
                            <?php $this->load->view('admin/fleet/navtab-parts/tab7') ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab8">
                            <?php $this->load->view('admin/fleet/navtab-parts/tab8') ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab9">
                            <?php $this->load->view('admin/fleet/navtab-parts/tab9') ?>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(function() {
        $('a[data-toggle="tab"]').on('click', function(e) {
            activeTab = e.target;
        })
        var hash = window.location.hash;

        if (hash && hash.includes('tab')) {
            $('.box-body').find('[role="presentation"]').removeClass('active');
            $('.box-body').find('.tab-pane').removeClass('active');
            $(hash).addClass('active');
            $('.box-body').find('[href="' + hash + '"]').parent('li').addClass('active');
        }
    });
</script>