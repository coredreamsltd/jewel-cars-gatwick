<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Fleet List</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/fleet_manager/add_update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-striped" id="dt-table">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th>Image</th>
                            <th>Vehicle Name</th>
                            <th>Class</th>
                            <th>Capacity</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($fleets)) {
                            foreach ($fleets as $index => $fleet) { ?>
                                <tr>
                                    <td><?php echo ++$index; ?></td>
                                    <td><img src="<?= base_url('uploads/fleet/' . $fleet->img_name) ?>" width="100"></td>
                                    <td><?php echo $fleet->title ?></td>
                                    <td><?=ucwords(str_replace('_',' ',$fleet->class)) ?></td>

                                    <td class="sorting_1">
                                        <i class="fa fa-users"> <?= $fleet->passengers ?></i>
                                        <i class="fa fa-briefcase"> <?= $fleet->luggage ?></i>
                                        <i class="fa fa-suitcase"> <?= $fleet->suitcases ?></i>
                                        <i class="fa fa-child"> <?= $fleet->baby_seats ?></i>
                                    </td>
                                    <td><?= $fleet->status ? '<label class="label label-success">ACTIVE</label>' : '<label class="label label-danger">IN ACTIVE</label>' ?></td>
                                    <td>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action
                                                <span class="fa fa-caret-down"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?php echo base_url('admin/fleet_manager/add_update/' . $fleet->id) ?>">
                                                        <i class="fa fa-edit text-primary"></i>Edit</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('admin/fleet_manager/delete/' . $fleet->id) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o text-danger"></i>Delete</a>
                                                </li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                        <?php }
                        } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box-body -->

    </div><!-- /.box -->
</div>
</div><!-- /.row -->

<!-- Modal -->
<div class="modal fade" id="fleet-bulk-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Bulk Rate Increment</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Select Fleet To Increment Rate As</label>
                        <select class="form-control" name="selected_fleet">
                            <option value="" selected disabled>Select Fleet</option>
                            <?php foreach ($fleets as $fleet) : ?>
                                <option value="<?= $fleet->id ?>"><?= $fleet->title ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div id="ajax-fleet-increment-fleets">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    var SITE_URL = '<?= site_url() ?>';
    $(document).ready(function() {
        $('#dt-table').dataTable();

        $('[name=selected_fleet]').change(function() {
            var selectedFleetID = $('[name=selected_fleet]').val();
            if (!selectedFleetID) {}
            getFleet();
        });
    });

    function blukRatetrigger() {
        $('#fleet-bulk-modal #ajax-fleet-increment-fleets').html('');
        $('#fleet-bulk-modal [name=selected_fleet]').val('');
        $('#fleet-bulk-modal').modal();

    }

    function getFleet() {
        $.ajax({
            type: 'POST',
            url: SITE_URL + "admin/fleet_manager/ajaxGetIncrementFleet",
            data: {
                'fleet_id': $('[name=selected_fleet]').val()
            },
            dataType: 'json',
            success: function(response) {
                if (response.status) {
                    $('#ajax-fleet-increment-fleets').html(response.data.html);
                }
            }
        });
    }
</script>
