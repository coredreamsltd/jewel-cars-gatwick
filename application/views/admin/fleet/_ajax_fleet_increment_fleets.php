<legend class="text-center">Please enter the value as needed</legend>
  <div class="col-md-12">
      <label>Increment Type</label>
        <select class="form-control" name="increment_type">
            <option value='percentage'>By Percentage (%)</option>
            <option value='amount'>(<?=CURRENCY?>) By Amount</option>
        </select>
     </div>
<?php foreach ($fleets as $fleet): ?>
    <div class="col-md-4">
        <label> <?= $fleet->title ?></label>
        <div class="input-group">
            <input type="hidden" class="form-control" name="fleetsToIncrease[]" value="<?= $fleet->id ?>">
            <input class="form-control" name="value[]" value="0">
            <!--<span class="input-group-addon">%</span>-->
        </div>
    </div>
<?php endforeach; ?>
<div class="col-md-12 ">
    <div class="pull-right">
        <button class="btn btn-flat btn-success" onclick="applyBulkRate()"> Apply</button>
        <button type="button" class="btn btn-flat btn-danger" data-dismiss="modal">Cancel</button>
    </div>
</div>

<script>
    function applyBulkRate() {
        var value = [];
        var fleetsToIncrease = [];
        $('input[name^="value"]').each(function () {
            value.push($(this).val());
        });
        $('input[name^="fleetsToIncrease"]').each(function () {
            fleetsToIncrease.push($(this).val());
        });

        $.ajax({
            type: 'POST',
            url: SITE_URL + "admin/fleet_manager/ajaxBulkRateIncrement",
            data: {
                'increment_type': $('[name=increment_type]').val(),
                'fleet_id': $('[name=selected_fleet]').val(),
                'value': value,
                'fleetsToIncrease': fleetsToIncrease,
            },
            dataType: 'json',
            success: function (response) {
                if (!response.status) {
                    toastr.error(response.message);
                    return;
                }
                toastr.success(response.message);
                $('#fleet-bulk-modal').modal('hide');
            }
        });
    }
</script>

