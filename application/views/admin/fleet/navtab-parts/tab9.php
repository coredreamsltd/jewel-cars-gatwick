<div class="row box-body">
    <?php if (empty($fleet)) : ?>
        <div class='alert alert-info'><i class='fa fa-info-circle'></i> Please add rest of the info & save the fleet in
            order to manage airport rates.
        </div>
    <?php else : ?>
        <div class="col-md-6">
            <legend>Station Pick Up Fare</legend>
            <?php $multi_stations = json_decode($additional_rate->multi_station, true); ?>

            <?php if (!empty($specific_locations)) : foreach ($specific_locations as $key => $airport) : ?>
                <?php if($airport->type!='station'){continue;}?>
                    <label><?= ucwords($airport->title) ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><?= CURRENCY ?></span>
                        <input type="text" name="multi_station[<?= $airport->id ?>]" class="form-control" value="<?= !empty($multi_stations[$airport->id]) ? $multi_stations[$airport->id] : 0 ?>">
                    </div>
            <?php endforeach;
            endif; ?>
        </div>
        <div class="col-md-6">
            <legend>Station Drop Off Fare</legend>
            <?php $multi_stations = json_decode($additional_rate->multi_station_dropoff, true); ?>

            <?php if (!empty($specific_locations)) : foreach ($specific_locations as $key => $airport) : ?>
                <?php if($airport->type!='station'){continue;}?>
                    <label><?= ucwords($airport->title) ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><?= CURRENCY ?></span>
                        <input type="text" name="multi_station_dropoff[<?= $airport->id ?>]" class="form-control" value="<?= !empty($multi_stations[$airport->id]) ? $multi_stations[$airport->id] : 0 ?>">
                    </div>
            <?php endforeach;
            endif; ?>
        </div>
    <?php endif; ?>
</div>