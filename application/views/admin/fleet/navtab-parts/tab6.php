<div class="row box-body">
    <?php if (empty($fleet)) : ?>
        <div class='alert alert-info'><i class='fa fa-info-circle'></i> Please add rest of the info & save the fleet in
            order to manage seaport rates.
        </div>
    <?php else : ?>
        <div class="col-md-6">
            <legend>Seaport Pick Up Fare</legend>
            <?php $multi_seaports = json_decode($additional_rate->multi_seaport, true); ?>
            <?php if (!empty($specific_locations)) : foreach ($specific_locations as $key => $seaport) : ?>
                <?php if($seaport->type!='seaport'){continue;}?>
                    <label><?= ucwords($seaport->title) ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><?= CURRENCY ?></span>
                        <input type="text" name="multi_seaport[<?= $seaport->id ?>]" class="form-control" value="<?= !empty($multi_seaports[$seaport->id]) ? $multi_seaports[$seaport->id] : 0 ?>">
                    </div>
            <?php endforeach;
            endif; ?>
        </div>
        <div class="col-md-6">
            <legend>Seaport Drop Off Fare</legend>
            <?php $multi_seaports = json_decode($additional_rate->multi_seaport_dropoff, true); ?>
            <?php if (!empty($specific_locations)) : foreach ($specific_locations as $key => $seaport) : ?>
                <?php if($seaport->type!='seaport'){continue;}?>
                    <label><?= ucwords($seaport->title) ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><?= CURRENCY ?></span>
                        <input type="text" name="multi_seaport_dropoff[<?= $seaport->id ?>]" class="form-control" value="<?= !empty($multi_seaports[$seaport->id]) ? $multi_seaports[$seaport->id] : 0 ?>">
                    </div>
            <?php endforeach;
            endif; ?>
        </div>
    <?php endif; ?>
</div>