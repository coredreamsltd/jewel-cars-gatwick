<div class="row box-body">
    <div class="col-md-3">
        <label class="control-label">Card Charge<span class="text-danger">*</span></label>
        <div class="input-group">
            <input type="text" name="card_fee" class="form-control" value="<?php echo ($is_edit && $additional_rate) ? $additional_rate->card_fee : '' ?>">
            <span class="input-group-addon">
                <select name="card_fee_type" required style="height: 20px">
                    <option value="By_Amount" <?= ($is_edit && $additional_rate) ? (($additional_rate->card_fee_type == 'By_Amount') ? 'selected' : '') : '' ?>>
                        By Amount(<?= CURRENCY ?>)
                    </option>
                    <option value="By_Percentage" <?= ($is_edit && $additional_rate) ? (($additional_rate->card_fee_type == 'By_Percentage') ? 'selected' : '') : '' ?>>
                        By Percentage(%)
                    </option>
                </select>
            </span>
        </div>
    </div>
    <div class="col-md-3">
        <label class="control-label">Baby Seat<span class="text-danger">*</span></label>
        <div class="input-group">
            <span class="input-group-addon"><?= CURRENCY ?></span>
            <input type="text" name="baby_seater" class="form-control" value="<?php echo ($is_edit && $additional_rate) ? $additional_rate->baby_seater : '' ?>">
            <span class="input-group-addon">per seater</span>
        </div>
    </div>

    <div class="col-md-3">
        <label class="control-label">Waiting Time Charge <span class="text-danger">*</span></label>
        <div class="input-group">
            <span class="input-group-addon"><?= CURRENCY ?></span>
            <input type="text" name="waiting_time" class="form-control" value="<?php echo ($is_edit && $additional_rate) ? $additional_rate->waiting_time : '' ?>">
            <span class="input-group-addon"> per minute</span>
        </div>
    </div>
    <div class="col-md-3">
        <label class="control-label">Raise By<span class="text-danger">*</span></label>
        <div class="input-group">
            <input type="text" name="raise_by" class="form-control" value="<?php echo ($is_edit && $additional_rate) ? $additional_rate->raise_by : '' ?>">
            <span class="input-group-addon">
                <select name="raise_by_type" required style="height: 20px">
                    <option value="amount" <?= ($is_edit && $additional_rate) ? (($additional_rate->raise_by_type == 'amount') ? 'selected' : '') : '' ?>>
                        By Amount(<?= CURRENCY ?>)
                    </option>
                    <option value="percentage" <?= ($is_edit && $additional_rate) ? (($additional_rate->raise_by_type == 'percentage') ? 'selected' : '') : '' ?>>
                        By Percentage(%)
                    </option>
                </select>
            </span>
        </div>
    </div>
    <div class="col-md-3">
        <label class="control-label">Meet $ Greet<span class="text-danger">*</span></label>
        <div class="input-group">
            <span class="input-group-addon"><?= CURRENCY ?></span>
            <input type="text" name="meet_and_greet" class="form-control" value="<?php echo ($is_edit && $additional_rate) ? $additional_rate->meet_and_greet : '' ?>">
        </div>
    </div>
    <div class="col-md-3">
        <label class="control-label">Via Pont Charge<span class="text-danger">*</span></label>
        <div class="input-group">
            <span class="input-group-addon"><?= CURRENCY ?></span>
            <input type="text" name="via_point" class="form-control" value="<?php echo ($is_edit && $additional_rate) ? $additional_rate->via_point : '' ?>">
            <span class="input-group-addon">per stop</span>
        </div>
    </div>
    <div class="col-md-3">
        <label class="control-label">Round Trip Discount<span class="text-danger">*</span></label>
        <div class="input-group">
            <input type="text" name="round_trip" class="form-control" value="<?php echo ($is_edit && $additional_rate) ? $additional_rate->round_trip : '' ?>">
            <span class="input-group-addon">%</span>
        </div>
    </div>

    <div class="col-md-3">
        <label class="control-label">Pickup Charge<span class="text-danger">*</span></label>
        <div class="input-group">
            <span class="input-group-addon"><?= CURRENCY ?></span>
            <input type="text" name="pickup_charge" class="form-control" value="<?php echo ($is_edit && $additional_rate) ? (!empty($additional_rate->pickup_charge) ? $additional_rate->pickup_charge : '') : '' ?>">
            <span class="input-group-addon">per pickup</span>
        </div>
    </div>
<!-- 
    <div class="col-md-3">
        <label class="control-label">Ski/Snowboard<span class="text-danger">*</span></label>
        <div class="input-group">
            <span class="input-group-addon"><?= CURRENCY ?></span>
            <input type="text" name="ski_charge" class="form-control" value="<?php echo ($is_edit && $additional_rate->ski_charge) ? $additional_rate->ski_charge : '' ?>">
        </div>
    </div> -->
    
</div>

<div class="panel-group login-box-body">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="rush-hour">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#add-rush-hour" aria-expanded="false" aria-controls="add-rush-hour">
                    <!-- RUSH HOUR CHARGE -->
					NIGHT CHARGE
                </a>
            </h4>
        </div>
        <div id="add-rush-hour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="rush-hour">
            <div class="panel-body" id="ajax-rush-hour-rate">
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="holiday-rate">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#add-holiday-rate" aria-expanded="false" aria-controls="add-rush-hour">
                    HOLIDAY CHARGE
                </a>
            </h4>
        </div>
        <div id="add-holiday-rate" class="panel-collapse  in" role="tabpanel" aria-labelledby="holiday-rate">
            <div class="panel-body" id="ajax-holiday-rate">
            </div>
        </div>
    </div>
</div>

<script>
    var fleet_id = '<?= isset($fleet->id) ? $fleet->id : '' ?>';
    var SITE_URL = '<?= site_url() ?>';
    $(document).ready(function() {
        load_ajax_view_rush_hour_rate();
        load_ajax_view_holiday_rate();
    });

    function load_ajax_view_rush_hour_rate() {
        $.ajax({
            url: SITE_URL + "admin/fleet_manager/ajax_rush_hour_rate_view/" + fleet_id,
            dataType: 'json',
            success: function(response) {

                if (!response.status) {
                    $('#ajax-rush-hour-rate').html("<div class='alert alert-info'><i class='fa fa-info-circle'></i> Please add rest of the info & save the fleet in order to manage rush hour charge.</div>");
                    return;
                }
                $('#ajax-rush-hour-rate').html(response.data.html);

            }
        });
    }

    function load_ajax_view_holiday_rate() {
        $.ajax({
            url: SITE_URL + "admin/fleet_manager/ajax_holiday_rate_view/" + fleet_id,
            dataType: 'json',
            success: function(response) {

                if (!response.status) {
                    $('#ajax-holiday-rate').html("<div class='alert alert-info'><i class='fa fa-info-circle'></i> Please add rest of the info & save the fleet in order to manage rush hour charge.</div>");
                    return;
                }
                $('#ajax-holiday-rate').html(response.data.html);

            }
        });
    }
</script>
