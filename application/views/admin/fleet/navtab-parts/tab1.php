<div class="box-body">
    <div class="row">
        <div class="col-md-3 form-group">
            <label>Vehicle Name <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="title" placeholder="Enter name of Vehicle" value="<?php echo ($is_edit) ? $fleet->title : '' ?>" required="">
        </div>
        <div class="col-md-3 form-group">
            <label>Class <span class="text-danger">*</span></label>
            <select class="form-control" name="class">
                <option value="economy_class" <?= ($is_edit && $fleet->class == 'economy_class') ? 'selected' : '' ?>>Economy Class</option>
                <option value="business_class" <?= ($is_edit && $fleet->class == 'business_class') ? 'selected' : '' ?>>Business Class</option>
                <option value="standard_class" <?= ($is_edit && $fleet->class == 'standard_class') ? 'selected' : '' ?>>Standard Class</option>
                <option value="executive_class" <?= ($is_edit && $fleet->class == 'executive_class') ? 'selected' : '' ?>>Executive Class</option>
                <option value="first_class" <?= ($is_edit && $fleet->class == 'first_class') ? 'selected' : '' ?>>First Class</option>
                <option value="vip_class" <?= ($is_edit && $fleet->class == 'vip_class') ? 'selected' : '' ?>>Vip Class</option>
            </select>
        </div>
        <div class="col-md-2 form-group">
            <label>Passengers <span class="text-danger">*</span></label>
            <input class="form-control" type="number" name="passengers" placeholder="Enter passenger limit" value="<?php echo ($is_edit) ? $fleet->passengers : '' ?>" required="">
        </div>
        <div class="col-md-2 form-group">
            <label>Suitcases <span class="text-danger">*</span></label>
            <input class="form-control" type="number" name="suitcases" placeholder="Enter suitcase limit" value="<?php echo ($is_edit) ? $fleet->suitcases : '' ?>" required="">
        </div>
        <div class="col-md-2 form-group">
            <label>Hand Luggage <span class="text-danger">*</span></label>
            <input class="form-control" type="number" name="luggage" placeholder="Enter luggage limit" value="<?php echo ($is_edit) ? $fleet->luggage : '' ?>" required="">
        </div>
        <div class="col-md-2 form-group">
            <label>Infant Seats <span class="text-danger">*</span></label>
            <input class="form-control" type="number" name="infant_seats" placeholder="Enter baby seat limit" value="<?php echo ($is_edit) ? $fleet->infant_seats : '' ?>" required="">
        </div>
        <div class="col-md-2 form-group">
            <label>Child Seats <span class="text-danger">*</span></label>
            <input class="form-control" type="number" name="baby_seats" placeholder="Enter baby seat limit" value="<?php echo ($is_edit) ? $fleet->baby_seats : '' ?>" required="">
        </div>
        <div class="col-md-2 form-group">
            <label>Child Booster Seats <span class="text-danger">*</span></label>
            <input class="form-control" type="number" name="child_booster_seats" placeholder="Enter baby seat limit" value="<?php echo ($is_edit) ? $fleet->child_booster_seats : '' ?>" required="">
        </div>
        <div class="col-md-2 form-group">
            <label>Booster Seats <span class="text-danger">*</span></label>
            <input class="form-control" type="number" name="booster_seats" placeholder="Enter baby seat limit" value="<?php echo ($is_edit) ? $fleet->booster_seats : '' ?>" required="">
        </div>
        <div class="col-md-3 form-group">
            <label>Status <span class="text-danger">*</span></label><br>
            <label class="text-success"><input type="radio" name="status" value="1" checked> ACTIVE</label>
            <label class="text-danger"><input type="radio" name="status" value="0" <?php echo ($is_edit && !$fleet->status) ? 'checked' : '' ?>> IN-ACTIVE</label>
        </div>
        <div class="col-md-12 form-group">
            <label>Description <span class="text-danger">*</span></label><br>
            <textarea class="form-control" name="desc" placeholder="Enter description"><?php echo ($is_edit) ? $fleet->desc : '' ?></textarea>
        </div>
        <div class="col-md-12 form-group">
            <label>Description on Quote <span class="text-danger">*</span></label><br>
            <textarea class="form-control" name="desc_1" placeholder="Enter description"><?php echo ($is_edit) ? $fleet->desc_1 : '' ?></textarea>
        </div>
        <div class="col-md-12 form-group">
            <label>Vehicle Image <span class="text-danger">*</span></label>
            <input class="<?php echo ($is_edit) ? 'required' : '' ?>" type="file" name="img_name">
            <?php if ($is_edit && $fleet->img_name) { ?>
                <div>
                    <img src="<?php echo base_url('uploads/fleet/' . $fleet->img_name) ?>" class="" width="300px">
                </div>
            <?php } ?>
        </div>

    </div>

</div>
<script>
    var fleet_id = <?= !empty($fleet->id) ? $fleet->id : 'null' ?>;
    var SITE_URL = '<?= site_url() ?>';

    // $(function() {
    //     $('#toggle-one').bootstrapToggle({
    //         on: 'YES',
    //         off: 'NO'
    //     });
    //     $('#toggle-status').bootstrapToggle({
    //         on: 'Active',
    //         off: 'In-active'
    //     });
    // });
</script>
