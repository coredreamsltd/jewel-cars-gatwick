<div class="box-body">
    <?php if (empty($fleet->id)) : ?>
        <br>
        <div class="alert alert-info">
            <i class="fa fa-info-circle"></i> Please add rest of the info & save the fleet in order to manage rates.
        </div>
    <?php else : ?>
        <div id="ajax-google-miles-rate"></div>
    <?php endif; ?>
</div><!-- /.box-body -->
<script>
    var fleet_id = <?= !empty($fleet->id)?$fleet->id:0 ?>;
    var SITE_URL = '<?= site_url() ?>';
    $(document).ready(function() {
        load_ajax_view_google_miles_rate();
    });

    function saveHourlyRate() {
     
        var hourly_rate = $('[name=hourly_rate]').val();
        $.ajax({
            url: SITE_URL + "admin/fleet_manager/ajax_save_hourly_rate/" + fleet_id,
            dataType: 'json',
            type: 'post',
            data: {
                hourly_rate: hourly_rate
            },
            success: function(response) {
                if (!response.status) {
                    toastr.success(response.message, 'Error');

                    return;
                }
                toastr.success('Successfully Saved!', 'Success');

            }
        });
    }

    function load_ajax_view_google_miles_rate() {
        if(!fleet_id){
            return;
        }
        $.ajax({
            url: SITE_URL + "admin/fleet_manager/ajax_google_miles_rate/" + fleet_id,
            dataType: 'json',
            success: function(response) {

                if (!response.status) {
                    alert(response.message);
                    return;
                }

                $('#ajax-google-miles-rate').html(response.data.html);

            }
        });
    }
</script>