<div class="box-body">
    <?php if (!((!empty($service_area_id) ? $service_area_id : false) ? true : false)) : ?>
        <h4 class="text-center">Please! Select service area</h4>
    <?php else : ?>
        <a href="<?= site_url('admin/zone-manager/add-edit?fleet_id=' . segment('4')) ?>" class="btn btn-sm btn-success pull-right">
            <i class="fa fa-plus"></i> Add New
        </a>
        <table class="table table-bordered dt-tables">
            <thead>
                <tr>
                    <th width="3%">#</th>
                    <th>Location</th>
                    <th>Setting</th>
                    <th width="5%">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($zone_rates)) : foreach ($zone_rates as $index => $rate) : ?>
                        <tr>
                            <td><?= ++$index ?></td>
                            <td><strong><?= $rate->title ?><strong></td>
                            <td>
                                Distance included: <strong><?= $rate->rate_distance . DISTANCE ?></strong> |
                                Price: <strong><?= CURRENCY . $rate->rate ?></strong> |
                                Radius: <strong><?= $rate->radius . DISTANCE ?></strong>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="<?= site_url('admin/zone-manager/add-edit/' . $rate->id . '?fleet_id=' . segment('4')) ?>"><i class="fa fa-edit"></i> Edit</a></li>
                                        <li>
                                            <a href="<?= site_url('admin/fleet-manager/deleteFleetLocationRate/' . $rate->id) ?>" onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash"></i> Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                <?php endforeach;
                endif;
                ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>