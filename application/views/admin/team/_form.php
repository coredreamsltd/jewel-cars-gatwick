<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Team | <?php echo (empty($team)) ? 'Add' : 'Update' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" class="forms" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Name <span class="text-danger">*</span></label>
                            <input class="form-control " type="text" name="name" value="<?php echo (!empty($team)) ? $team->name : '' ?>" required>
                        </div>
                        <div class="col-md-4">
                            <label>Title <span class="text-danger">*</span></label>
                            <input class="form-control " type="text" name="title" placeholder="Enter title" value="<?php echo (!empty($team)) ? $team->title : '' ?>" required>
                        </div>
                        <div class="col-md-4">
                            <label>Image <span class="text-danger">*</span></label>
                            <input type="file" name="image">
                            <?php if (!empty($team->image)) : ?>
                                <img width="100px" src="<?php echo base_url('uploads/page/' . $team->image) ?>" class="img-responsive img-thumbnail">
                            <?php endif; ?>
                        </div>
                        <div class="col-md-12">
                            <label>Description <span class="text-danger">*</span></label>
                            <textarea name="content" class="form-control"><?php echo (!empty($team)) ? $team->content : '' ?></textarea>
                        </div>
                        <div class="col-md-12 ">
                            <br>
                            <button class="btn btn-primary"> Save</button>
                        </div>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->