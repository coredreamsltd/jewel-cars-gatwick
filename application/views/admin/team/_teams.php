<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Team List</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/content/add_update_team') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dt-table">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th width="5%">Image</th>
                                <th>Title</th>
                                <th>Name</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($teams)) :
                                foreach ($teams as $index => $t) :
                            ?>
                                    <tr>
                                        <td><?php echo ++$index; ?></td>
                                        <td>
                                            <img src="<?=base_url('uploads/page/'.($t->image?:'default.jpg'))?>" class="circular-profile profile-img-zoom">
                                        </td>
                                        <td><?php echo $t->title ?></td>
                                        <td><?php echo $t->name ?></td>
                                        <td>
                                            <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/content/add_update_team/' . $t->id) ?>"><i class="fa fa-edit text-primary"></i> Edit</a>
                                            <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/content/delete_team/' . $t->id) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash text-danger"></i> Delete</a>
                                        </td>
                                    </tr>
                            <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->