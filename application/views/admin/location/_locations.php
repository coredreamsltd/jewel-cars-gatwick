<?php $count = segment(3) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">

                <h3 class="box-title">Locations - <?= isset($_GET['type']) ? getLocationType($_GET['type']) : 'All' ?></h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/location/add-update?type=' . $_GET['type']) ?>" class="btn btn-success"><i class="fa fa-plus-square"> Add New Location</i></a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                    <table class="table table-bordered dt-tables">
                        <thead>
                            <tr>
                                <th width="2%">#</th>
                                <th>Display Location Name</th>
                                <th>Postcode</th>
                                <th>Full Address</th>
                                <th>Location Type</th>
                                <th>Search</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($locations):
                                foreach ($locations as $l) :
                                    ?>
                                    <tr>
                                        <td><?= $count++; ?></td>
                                        <td><?php echo $l['name'] ?></td>
                                        <td><?php echo $l['postcode'] ?></td>
                                        <td><?php echo $l['address']?:'-' ?></td>
                                        <td><?php echo $l['location_type'] ?></td>
                                        <td><?php echo $l['search_text'] ?></td>
                                        <td>
                                            <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/location/add-update/' . $l['id']) ?>"><i class="fa fa-trash text-primary"> Edit</i></a>
                                            <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/location/delete_location/' . $l['id']) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash text-danger"> Delete</i></a>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->