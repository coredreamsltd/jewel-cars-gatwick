<?php
$isNew = true;
if (segment(4) != '') {
    $isNew = false;
}
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box" style="min-height: 600px">
            <div class="box-header">
                <h3 class="box-title">Location Category Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" class="forms">
                    <div class="form-group">
                        <label>Location Type <span class="text-danger">*</span></label>
                        <input type="text" placeholder="Enter location type" class="form-control " required name="type" value="<?php echo !$isNew ? $location_type[0]['type'] : '' ?>">

                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Location Icon <span class="text-danger">*</span></label>
                                <input type="text" class="form-control " required name="fa_icon" value="<?php echo !$isNew ? $location_type[0]['fa_icon'] : '' ?>">
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Search By(<i>use comma (,) to add multiple text</i>)</label>
                                    <textarea name="search_text" class="form-control"><?= (!$isNew && $location_type[0]['search_text']) ? $location_type[0]['search_text'] : '' ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save</button>
                    </div>

                </form>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
</div><!-- /.row -->
<script>
    $(function() {
        $('[name=fa_icon]').iconpicker();
    });
</script>