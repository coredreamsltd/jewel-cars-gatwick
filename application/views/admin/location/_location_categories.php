<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">

                <h3 class="box-title">Location Categories</h3>
                <span class="pull-right add-new">
                    <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-line-chart"></i> Import Locations
                    </button>
                    <a href="<?php echo base_url('admin/location/add_update_location_category') ?>"
                       class="btn btn-success btn-flat"><i class="fa fa-plus-square"> Add New Category</i></a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-stripped table-hover dt-tables">
                        <thead>
                        <tr>
                            <th width="2%">S.No</th>
                            <th width="5%">Location Type ID</th>
                            <th width="28%">Location Type</th>
                            <th width="30%">Locations Under this Category</th>
                            <th>Search</th>
                            <th width="15%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (!empty($location_types))
                            foreach ($location_types as $lt) {
                                ?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo $lt['id']; ?></td>
                                    <td><i class="fa <?= $lt['fa_icon'] ?>"></i> <?php echo $lt['type'] ?></td>
                                    <td>
                                        <a href="<?php echo base_url('admin/location?type=' . $lt['id']) ?>"
                                           class="btn btn-default"><i class="fa fa-list"> View All</i></a>
                                        <a href="<?php echo base_url('admin/location/add-update?type=' . $lt['id']) ?>"
                                           class="btn btn-default"><i class="fa fa-plus-square text-success"> Add New
                                                Location</i></a>
                                    </td>
                                    <td><?=$lt['search_text']?></td>
                                    <td>
                                        <a class="btn btn-sm btn-default"
                                           href="<?php echo base_url('admin/location/add_update_location_category/' . $lt['id']) ?>"><i
                                                    class="fa fa-edit text-primary"> Edit</i></a>
                                        <!--<a class="btn btn-sm btn-default" href="<?php echo base_url('admin/location/delete_location_category/' . $lt['id']) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash text-danger"> Delete</i></a>-->
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.box-body -->

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Import Location Data</h4>
            </div>
            <form action="<?=site_url('admin/rate/importLocationWithRates')?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="file" name="file" required>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-flat"> Import</button>
                </div>
            </form>
        </div>

    </div>
</div>