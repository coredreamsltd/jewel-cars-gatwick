<?php flash() ?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Location Groups</h3>
        <span class="pull-right add-new">
            <a href="<?php echo base_url('admin/location/add_edit_group') ?>" class="btn btn-success"><i class="fa fa-plus-square"> Add New Group</i></a>
        </span>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-stripped table-hover" id="dt-table">
                <thead>
                    <tr>
                        <th width="2%">SN</th>
                        <th>Group Name</th>
                        <th width="5%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($location_groups):
                        foreach ($location_groups as $index => $group):
                            ?>
                            <tr>
                                <td><?= ++$index; ?></td>
                                <td><?= $group->group_name ?></td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?= site_url('admin/location/add_edit_group/'.$group->id) ?>"><i class="fa fa-edit"></i> Edit</a></li>
                                            <li><a href="<?= site_url('admin/location/delete_group/'.$group->id) ?>" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->

</div><!-- /.box -->