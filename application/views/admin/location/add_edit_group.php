<div class="box">
    <form method="post">
        <div class="box-header">
            <h3 class="box-title btn-block">Location Group | <?= !$isEdit ? 'Add' : 'Update' ?>

                <div class="pull-right">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                    <?php if($isEdit): ?>
                    <button type="button" class="btn btn-success" onclick="update_all_rates(<?= $location_group->id ?>)"><i class="fa fa-refresh"></i> Update All Rates</button>
                    <?php endif; ?>
                </div>

            </h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php flash() ?>

            <input type="hidden" name="location_ids" value="<?= $isEdit ? $location_ids : '' ?>">

            <div class="form-group">
                <label>Group Name</label>
                <input type="text" class="form-control" name="group_name" value="<?= $isEdit ? $location_group->group_name : '' ?>" required="">
            </div>

            <div class="row">
                <div class="col-md-6">
                    <table class="table" id="dt-locations">
                        <thead>
                            <tr>
                                <th>Location Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($locations):
                                foreach ($locations as $location):
                                    ?>
                                    <tr>
                                        <td><?= $location->name ?></td>
                                        <td><button data-location-id="<?= $location->id ?>" data-json="<?= htmlspecialchars(json_encode($location)) ?>" class="btn btn-sm btn-success add-to-group" type="button" <?= $location->lg_id ? 'disabled' : '' ?>><i class="fa fa-long-arrow-right"></i></button></td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table" id="dt-locations-selected">
                        <thead>
                            <tr>
                                <th>Location Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($isEdit && $group_locations):
                                foreach ($group_locations as $location):
                                    ?>
                                    <tr>
                                        <td><?= $location->name ?></td>
                                        <td><button data-location-id="<?= $location->id ?>" data-json="<?= htmlspecialchars(json_encode($location)) ?>" class="btn btn-sm btn-danger remove-from-group" type="button"><i class="fa fa-long-arrow-left"></i></button></td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box-body -->

<div id="modal-update-all-rates" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Updating all group rates</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var BASE_URL = '<?=base_url()?>';
    var SITE_URL = '<?=site_url()?>';
    var dt_locations, dt_locations_selected, location_ids;

    jQuery(document).ready(function () {
        dt_locations = $('#dt-locations').DataTable();
        dt_locations_selected = $('#dt-locations-selected').DataTable();
        location_ids = $('[name=location_ids]').val() == "" ? [] : $('[name=location_ids]').val().split(',');

        $('body').on('click', '.add-to-group', function (e) {
            var data = JSON.parse($(this).attr('data-json'));
            $(this).attr('disabled', '');

            dt_locations_selected.row.add([
                data.name,
                "<button data-location-id='" + data.id + "' data-json='" + $(this).attr('data-json') + "' class='btn btn-sm btn-danger remove-from-group' type='button'><i class='fa fa-long-arrow-left'></i></button>"
            ]).draw(false);

            location_ids.push(data.id);
            $('[name=location_ids]').val(location_ids.toString());
        });

        $('body').on('click', '.remove-from-group', function (e) {
            var data = JSON.parse($(this).attr('data-json'));
            $(this).parents('tr').addClass('remove');
            $('#dt-locations').find('[data-location-id=' + data.id + ']').removeAttr('disabled');
            dt_locations_selected.row('.remove').remove().draw(false);

            location_ids.splice(location_ids.indexOf(data.id), 1);
            $('[name=location_ids]').val(location_ids.toString());
        });

    });


    function update_all_rates(group_id) {

        $.ajax({
            url: SITE_URL + 'admin/rate/ajax_get_group_rates/' + group_id,
            dataType: 'json',
            success: function (response) {

                if (!response.status) {
                    alert(response.msg);
                    return;
                }

                var modal_update_all_rates = $('#modal-update-all-rates');
                modal_update_all_rates.find('.modal-body').html();

                $.each(response.data, function (index, group_rate) {


                    var modal_html = "<li>"
                            + group_rate.from_group_name
                            + " - ";

                    if (group_rate.to_group_id) {
                        modal_html += group_rate.to_group_name;
                    } else if (group_rate.to_location_id) {
                        modal_html += group_rate.to_location_name;
                    }

                    modal_html += "<span class='pull-right' id='gr-" + group_rate.id + "'><i class='fa fa-refresh fa-spin'></i></span>"
                            + "</li>";

                    modal_update_all_rates.find('.modal-body').append(modal_html);

                    if (group_rate.to_group_id !== null) {
                        group_rate.group_rate_type = 'group-group';
                    } else if (group_rate.to_location_id !== null) {
                        group_rate.group_rate_type = 'group-location';
                    } else {
                        return;
                    }

                    $.ajax({
                        url: SITE_URL + 'admin/rate/add_edit_group_rate/' + group_rate.id,
                        dataType: 'json',
                        method: 'post',
                        data: group_rate,
                        success: function (resp) {
                            $('#gr-' + group_rate.id).html('<i class="fa fa-check-circle"></i>');
                        },
                        error: function () {
                            alert('server error.');
                            return;
                        }
                    });

                });

                modal_update_all_rates.modal();

            },
            error: function () {
                alert('server error.');
                return;
            }
        })

    }



</script>