<?php
$isNew = true;
if (segment(4) != '') {
    $location = $location[0];
    $search_text = $location['search_text'];
    $isNew = false;
    $name = $location['name'];
    $postcode = $location['postcode'];
    $address = $location['address'];
    $type_id = $location['location_type_id'];
}
$type_id = (!$isNew ? $type_id : (isset($_GET['type']) ? $_GET['type'] : ''));
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Location - <?= getLocationType($type_id) ?> | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php flash() ?>
                <div class="row">
                    <div class="col-md-6">
                        <form action="" method="post" class="forms" enctype="multipart/form-data">

                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Display Location Name <span class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter location" class="form-control" name="name" value="<?php echo !$isNew ? $name : '' ?>" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Postcode <span class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter postcode" class="form-control" name="postcode" value="<?php echo !$isNew ? $postcode : '' ?>" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Full Address</label>
                                            <input type="text" placeholder="Enter address" class="form-control" name="address" value="<?php echo !$isNew ? $address : '' ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (!empty($location['search_location'])) : ?>
                                                <input class="form-control" value="<?= $location['search_location'] ?>" disabled>
                                            <?php endif; ?>
                                            <label>Search By(<i>use comma (,) to add multiple text</i>)</label>
                                            <textarea name="search_text" class="form-control"><?= !empty($search_text) ? $search_text : '' ?></textarea>
                                        </td>
                                    </tr>
                                    <input type="hidden" name="location_type_id" value="<?= $type_id ?>">
                                    <tr>
                                        <td>
                                            <button id="has-ckeditor" class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div class="col-md-6">

                        <?php if ($isEdit && 0) : ?>
                            <h3>Terminals
                                <div class="pull-right">
                                    <a href="#!" id="btn-modal-terminal" class="btn btn-success"><i class="fa fa-plus"></i> Add new terminal</a>
                                </div>
                            </h3>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th width="70%">Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($terminals) :
                                        foreach ($terminals as $index => $terminal) :
                                    ?>
                                            <tr>
                                                <td><?= ++$index ?></td>
                                                <td><?= $terminal->name ?></td>
                                                <td>
                                                    <div class="dropdown">
                                                        <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">Actions
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#!" data-terminal-id="<?= $terminal->id ?>" class="btn-edit-terminal"><i class="fa fa-edit"></i> Edit</a></li>
                                                            <li><a href="<?= site_url('admin/location/delete_terminal/' . $terminal->id) ?>" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i> Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php
                                        endforeach;
                                    else :
                                        ?>
                                        <tr>
                                            <td colspan="3">- no terminals added -</td>
                                        </tr>
                                    <?php
                                    endif;
                                    ?>

                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div id="modal-terminal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Terminal | Add/Edit</h4>
            </div>
            <form action="<?= site_url('admin/location/add_edit_terminal') ?>" method="post">
                <input type="hidden" name="terminal_id">
                <input type="hidden" name="location_id" value="<?= $isEdit ? $location['id'] : '' ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" required="">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="ckeditor-description" required=""></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(document).ready(function() {
        var BASE_URL = '<?= base_url() ?>';
        var SITE_URL = '<?= site_url() ?>';
        var modal_terminal = $('#modal-terminal');

        var editor = CKEDITOR.replace('ckeditor-description', {
            customConfig: BASE_URL + 'assets/admin/js/plugins/ckeditor/my_config.js'
        });
        CKFinder.setupCKEditor(editor, BASE_URL + 'assets/admin/js/plugins/ckfinder');

        $('#btn-modal-terminal').click(function() {
            modal_terminal.find('form')[0].reset();
            modal_terminal.find('[name=terminal_id]').val('');

            editor.setData('');
            modal_terminal.modal();
        });

        $('.btn-edit-terminal').click(function() {
            var terminal_id = $(this).attr('data-terminal-id');

            $.ajax({
                url: SITE_URL + "admin/location/ajax_get_terminal_details/" + terminal_id,
                dataType: 'json',
                success: function(response) {

                    if (!response.status) {
                        alert(response.msg);
                        return;
                    }

                    var form = modal_terminal.find('form');

                    form.find('[name=terminal_id]').val(response.data.id);
                    form.find('[name=name]').val(response.data.name);
                    form.find('[name=description]').html(response.data.description);

                    editor.setData(response.data.description);

                    modal_terminal.modal();

                },
                error: function() {
                    alert('Server error occured.');
                }
            });
        });

    });
</script>