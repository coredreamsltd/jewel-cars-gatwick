<?php
$isNew = true;
if(segment(4) != '') {
    $isNew = false;
    $admin = $admin[0];
}
?>

<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <form action="" method="post" class="forms" enctype="multipart/form-data">
                <div class="box-header">
                    <h3 class="box-title">System User | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
                    <div class="pull-right">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div>
                        <?php if( !$isNew && $admin['type'] == 3) :?>
                           <div style="margin-bottom: 10px;">
                                <label>Is Active?</label><br>
                                <label class="radio-inline">
                                    <input type="radio" name="is_active" value="1" checked <?= !$isNew && (date('Y-m-d H:i:s') > $admin['expired_at'])  ? '' : 'disabled' ?>> Yes
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="is_active" value="0" <?= !$isNew && (date('Y-m-d H:i:s') > $admin['expired_at'])  ? 'checked' : '' ?> disabled> No
                                </label>
                           </div>
                        <?php endif; ?>
                        <div style="margin-bottom: 10px;">
                            <label>Username</label>
                            <input type="text" class="form-control" name="username" value="<?php echo (!$isNew) ? $admin['username'] : '';?>"/>
                        </div>
                        <div>
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" value="<?php echo (!$isNew) ? $admin['password'] : '';?>">
                        </div>
                        <?php if(($isNew) || ($admin['type'] == 3)) :?>
                            <div>
                                <input type="hidden" class="form-control" name="type" value="3">
                                <!-- <label>Type</label>
                                <select name="type" id="" class="form-control">
                                    <option value="" selected>Select Type</option>
                                    <option value="2" <?= ((!$isNew) && ($admin['type'] == 2)) ? 'selected' : '';?>>2</option>
                                    <option value="3" <?= ((!$isNew) && ($admin['type'] == 3)) ? 'selected' : '';?>>3</option>
                                </select>
                                <input type="text" class="form-control" name="type" value="<?php echo (!$isNew) ? $admin['type'] : '';?>"> -->
                            </div>
                        <?php endif; ?>
                    </div>

                    <table style="margin-top: 20px;">
                        <?php if($_SESSION['current_user'] == 1): ?>
                            <tr>
                                <td>
                                    <label>Menu Access</label>
                                    <table class="table">
                                        <tr>
                                            <td width="5%"><input type="checkbox" name="menu_access[]" value="1" <?= !$isNew && in_array(1, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">1</td>
                                            <td>Dashboard</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="2" <?= !$isNew && in_array(2, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">2</td>
                                            <td>Booking Manager</td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><input type="checkbox" name="menu_access[]" value="3" <?= !$isNew && in_array(3, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">3</td>
                                            <td>Content Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="4" <?= !$isNew && in_array(4, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">4</td>
                                            <td>Fleet Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="5" <?= !$isNew && in_array(5, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">5</td>
                                            <td>Rate Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="8" <?= !$isNew && in_array(8, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">8</td>
                                            <td>Location Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="8" <?= !$isNew && in_array(8, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">8</td>
                                            <td>Specific Location Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="7" <?= !$isNew && in_array(7, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">7</td>
                                            <td>Driver Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="6" <?= !$isNew && in_array(6, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">6</td>
                                            <td>Passanger Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="10" <?= !$isNew && in_array(10, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">10</td>
                                            <td>Invoice Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="9" <?= !$isNew && in_array(9, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">9</td>
                                            <td>Discounts Manager</td>
                                        </tr>
                                        <!-- <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="11" <?= !$isNew && in_array(11, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">11</td>
                                            <td>Generate Reports</td>
                                        </tr> -->
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="11" <?= !$isNew && in_array(11, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">11</td>
                                            <td>SEO Manager</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="13" <?= !$isNew && in_array(13, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">13</td>
                                            <td>System Users</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name="menu_access[]" value="3" <?= !$isNew && in_array(3, explode(',', $admin['menu_access'])) ? 'checked' : '' ?>></td>
                                            <td width="5%">3</td>
                                            <td>Settings</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->