<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">System User List</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/system_user/add_update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-striped" id="dt-table">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th>Username</th>
                            <th>Type</th>
                            <th>Menu Access</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($admins)) foreach ($admins as $index => $admin) :?>
                            <tr>
                                <td><?php echo ++$index; ?></td>
                                <td><?= $admin->username ?></td>
                                <td><?= $admin->type ?></td>
                                <td><?= $admin->menu_access ?></td>
                                <td>
                                    <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/system_user/add_update/' . $admin->id) ?>"><i class="fa fa-trash text-primary"> Edit</i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box-body -->

    </div><!-- /.box -->
</div>
</div><!-- /.row -->
<script>
    $(document).ready(function() {
        $('#dt-table').dataTable();
    });
</script>