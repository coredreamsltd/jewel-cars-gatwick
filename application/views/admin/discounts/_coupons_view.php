<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Coupons Details</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                    <table class="table">
                        <tr>
                            <th>Coupon Title</th>
                            <td><?= $coupon->coupon_title ?></td>
                        </tr>
                        <tr>
                            <th>Valid From</th>
                            <td><?= $coupon->valid_from ?></td>
                        </tr>
                        <tr>
                            <th>Valid To</th>
                            <td><?= $coupon->valid_to ?></td>
                        </tr>
                    </table>
                    <hr>
                    <table class="table table-bordered" id="dt-table">
                        <thead>
                            <tr>
                                <th>Coupon No.</th>
                                <th>Redeemed Status</th>
                                <th>Is Emailed?</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($coupons)) :
                                foreach ($coupons as $c) :
                                    ?>
                                    <tr>
                                        <td><?php echo $c->coupon_no ?></td>
                                        <td><?php echo $c->is_redeemed ? '<span class="label label-success">Redeemed</span>' : '<span class="label label-danger">Not Redeemed</span>' ?></td>
                                        <td>
                                            <a href="#!" onclick="bootbox.confirm('Confirm emailed status change?', function(result){
                                                if(result){
                                                    window.location = '<?= site_url("admin/discounts/change_is_emailed/{$c->id}/" . ($c->is_emailed ? '0' : '1')) ?>';
                                                }
                                            })">
                                                <span class="label label-<?= $c->is_emailed ? 'success' : 'danger' ?>"><?= $c->is_emailed ? 'Yes' : 'No' ?></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->