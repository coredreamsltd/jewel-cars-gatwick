<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Account Based Discounts</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">

                        <form action="" method="post" class="forms">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Coupon Max Valid For</label>
                                        <div class="input-group">
                                            <input type="text" name="coupon_validity_period" class="form-control" value="<?= $ab_discount->coupon_validity_period ?>">
                                            <span class="input-group-addon">Day(s)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label>Discount Per Coupon</label>
                                <div class="input-group">
                                    <input type="text" name="discount" class="form-control pull-left" value="<?= $ab_discount->discount ?>" style="width: 85px">
                                    <select name="discount_type" class="form-control pull-left" style="width: 70px; margin-left: -1px;">
                                        <option value="by_amount" <?= $ab_discount->discount_type == 'by_amount' ? 'selected' : '' ?>><?= CURRENCY ?></option>
                                        <option value="by_percent" <?= $ab_discount->discount_type == 'by_percent' ? 'selected' : '' ?>>%</option>
                                    </select>
                                </div>
                            </div>

                            <p class="text-danger">Note: One of the below criteria must me to generate a discount coupon.</p>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td width="50%">
                                            <label>Min no. of Booking</label>
                                            <input type="text" name="min_booking" class="form-control" value="<?= $ab_discount->min_booking ?>">
                                        </td>
                                        <td width="50%">
                                            <label>Min amount spent</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><?= CURRENCY ?></span>
                                                <input type="text" name="min_amount_spent" class="form-control" value="<?= $ab_discount->min_amount_spent ?>">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <button class="btn btn-primary"><i class="fa fa-save"></i> Save Discounts</button>
                        </form>
                    </div>
                    <div class="col-md-8">
                    </div>
                </div>
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->