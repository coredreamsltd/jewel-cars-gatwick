<?php flash() ?>

<?php if (isset($account)): ?>
    <h4>Account: <a href="#!" class=""><?= $account->fname . ' ' . $account->lname ?></a></h4>
<?php endif; ?>
    
<div class="box">

    <div class="box-header">
        <h3 class="box-title btn-block">Discount Coupons Manager
            <div class="pull-right">
                <a href="<?php echo site_url('admin/discounts/add_update_coupons'.(isset($account) ? '?account_id='.$account->id : '')) ?>" class="btn btn-success"><i class="fa fa-plus-square"> Add New</i></a>
            </div>
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-striped" id="dt-table">
                <thead>
                    <tr>
                        <th width="1%">#</th>
                        <th>Coupon Title</th>
                        <th>Valid From</th>
                        <th>Valid To</th>
                        <th>Coupons Issued</th>
                        <th>Max Limit</th>
                        <th>Coupons Redeemed</th>
                        <th>Coupon Type</th>
                        <th>Status</th>
                        <th width="8%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($coupons) :
                        foreach ($coupons as $index => $coupon) :
                            ?>
                            <tr>
                                <td><?= ++$index; ?></td>
                                <td><?= $coupon->coupon_title ?></td>
                                <td><?= $coupon->valid_from ?></td>
                                <td><?= $coupon->valid_to ?></td>
                                <td><?= $coupon->no_of_coupons_issued ?></td>
                                <td><?= $coupon->max_redeem_limit ?></td>
                                <td><?= $coupon->no_of_coupons_redeemed ?></td>
                                <td><?= $coupon->is_reusable ? 'Reusable' : 'Not Reusable' ?></td>
                                <td>
                                    <a href="<?= base_url('admin/discounts/change_coupon_status/' . $coupon->id . '?action=' . ($coupon->is_active ? 0 : 1)) ?>" class="label <?= $coupon->is_active ? 'label-success' : 'label-danger' ?>" onclick="return confirm('Confirm action?')"><?= $coupon->is_active ? 'Active' : 'Inactive' ?></a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?= base_url('admin/discounts/coupons_view/' . $coupon->id) ?>"><i class="fa fa-ticket"></i> View Coupons</a></li>
                                            <li><a href="<?= base_url('admin/discounts/add_update_coupons/' . $coupon->id) ?>"><i class="fa fa-edit"></i> Edit</a></li>
                                            <li><a href="<?= base_url('admin/discounts/delete_coupons/' . $coupon->id) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
