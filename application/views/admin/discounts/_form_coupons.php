<?php
$isNew = true;
if (segment(4) != '') {
    $isNew = false;
}
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Coupons Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" class="forms">
                    <?php if (!$isNew) : ?>
                        <input type="hidden" name="id" value="<?= segment(4) ?>">
                    <?php endif; ?>
                    <table class="table table-bordered">
                        <?php if (isset($account)) : ?>
                            <tr>
                                <td>
                                    <label>Account <span class="text-danger">*</span></label>
                                    <p class="form-control-static"><?= $account->fname . ' ' . $account->lname ?></p>
                                    <input type="hidden" name="account_id" value="<?= $account->id ?>">
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td>
                                <label>Coupon Title <span class="text-danger">*</span></label>
                                <input class="form-control required" type="text" name="coupon_title" placeholder="Enter title for coupon" value="<?php echo (!$isNew) ? $coupon->coupon_title : '' ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Is Re-usable? <span class="text-danger">*</span></label>
                                        <br>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_reusable" value="1" <?= !$isNew && $coupon->is_reusable ? 'checked' : '' ?> <?= !$isNew ? 'disabled' : '' ?> required=""> Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_reusable" value="0" <?= !$isNew && !$coupon->is_reusable ? 'checked' : '' ?> <?= !$isNew ? 'disabled' : '' ?>> No
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Status <span class="text-danger">*</span></label>
                                        <br>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" value="1" <?= !$isNew && $coupon->is_active ? 'checked' : '' ?> checked=""> Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" value="0" <?= !$isNew && !$coupon->is_active ? 'checked' : '' ?>> Inactive
                                        </label>
                                    </div>
                                    <input type="hidden" name="is_display_on_site" value="0">
                                    <!--                                    <div class="col-md-2">
                                        <label>Display on site? <span class="text-danger">*</span></label>
                                        <br>
                                        <label class="radio-inline">
                                            <input type="radio"
                                                   name="is_display_on_site"
                                                   value="1"
                                                <?/*= !$isNew && $coupon->is_display_on_site ? 'checked' : '' */?>
                                                   required=""> Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio"
                                                   name="is_display_on_site"
                                                   value="0"
                                                <?/*= !$isNew && !$coupon->is_display_on_site ? 'checked' : '' */?>> No
                                        </label>
                                    </div>-->
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>No. of Coupons <span class="text-danger">*</span></label>
                                        <div id="hidden-no-of-coupons"></div>
                                        <input class="form-control digits required" type="text" name="no_of_coupons_issued" placeholder="Total Coupons" value="<?php echo (!$isNew) ? $coupon->no_of_coupons_issued : 0 ?>" <?= !$isNew ? 'disabled' : '' ?>>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Max Redeem Limit <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control digits required" name="max_redeem_limit" value="<?= !$isNew ? $coupon->max_redeem_limit : '0' ?>">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Valid From <span class="text-danger">*</span></label>
                                        <input class="form-control all-datepicker required" type="text" name="valid_from" placeholder="Valid From" value="<?php echo (!$isNew) ? $coupon->valid_from : '' ?>">
                                    </div>
                                    <div class="col-md-2">
                                        <label>Valid To <span class="text-danger">*</span></label>
                                        <input class="form-control all-datepicker required" type="text" name="valid_to" placeholder="Valid To" value="<?php echo (!$isNew) ? $coupon->valid_to : '' ?>">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Discount Amount<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input class="input-group-addon required" type="text" name="discount" placeholder="Discount Amount" value="<?php echo (!$isNew) ? $coupon->discount : '0' ?>" style="width:100px" <?= !$isNew ? 'disabled' : '' ?>>
                                    <select name="discount_type" class="input-group-addon" style="width: 70px; padding: 5px;" <?= !$isNew ? 'disabled' : '' ?>>
                                        <option <?= (!$isNew && $coupon->discount_type == 'by_amount') ? 'selected' : '' ?> value="by_amount"><?= CURRENCY ?></option>
                                        <option <?= (!$isNew && $coupon->discount_type == 'by_percent') ? 'selected' : '' ?> value="by_percent">%
                                        </option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> <?= $isNew ? 'Generate Coupons' : 'Save Changes' ?>
                                </button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(function() {
        $('[name=is_reusable]').change(function() {
            var isReusable = $('[name=is_reusable]:checked').val();
            if (isReusable == '1') {
                $('[name=no_of_coupons_issued]').val(1).attr('disabled', '');
                $('#hidden-no-of-coupons').html('<input type="hidden" name="no_of_coupons_issued" value="1">');
            } else {
                $('[name=no_of_coupons_issued]').val(0).removeAttr('disabled');
                $('#hidden-no-of-coupons').html('');
            }
        });
    });
</script>