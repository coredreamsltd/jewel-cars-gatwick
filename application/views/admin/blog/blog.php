<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Blog Manager</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo site_url('admin/blog/add_update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" id="dt-pages">
                    <thead>
                        <tr>
                            <th style="width:3%"></th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Short Content</th>
                            <th width="12%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($blogs)):
                            foreach ($blogs as $blog):
                                ?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><img src="<?= base_url('uploads/blog/'.$blog->featured_image) ?>" width="200px"></td>
                                    <td><?php echo $blog->title ?></td>
                                    <td><?php echo $blog->excerpt ?></td>
                                    <td>
                                        <a class="btn btn-sm btn-info" href="<?php echo site_url('admin/blog/add_update/' . $blog->id) ?>">Edit</a>
                                        <a class="btn btn-sm btn-danger" href="<?php echo site_url('admin/blog/delete/' . $blog->id) ?>" onclick="return confirm('Are you sure?')">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(function(){
        $('#dt-pages').dataTable();
    });
</script>