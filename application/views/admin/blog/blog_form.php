<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="" method="post" class="forms" enctype="multipart/form-data">
                <div class="box-header">
                    <h3 class="box-title">Blog Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
                    <span class="pull-right add-new">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                </span>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-7">
                            <label>Title <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="title" value="<?= !$isNew ? $blog->title : '' ?>" required>

                        </div>
                        <div class="col-md-7">
                            <label>Short Content <span class="text-danger">*</span></label>
                            <textarea class="form-control" name="excerpt"
                                      placeholder="Enter short content"><?= !$isNew ? $blog->excerpt : '' ?></textarea>
                            <label for="desc" class="error" style="display:none;">This field is
                                required</label>
                            <script type="text/javascript">
                                var editor = CKEDITOR.replace('excerpt',
                                    {
                                        customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                    });
                                CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                            </script>
                        </div>
                        <div class="col-md-5">
                            <label>Feature Image <span class="text-danger">*</span></label>
                            <input type="file" name="blog" class="required">
                            <?php if (!$isNew) { ?>
                                <td>
                                    <img width="500px"
                                         src="<?php echo base_url('uploads/blog/' . $blog->featured_image) ?>"
                                         class="img-responsive">
                                </td>
                            <?php } ?>
                        </div>
                        <div class="col-md-12">
                            <label>Long Content <span class="text-danger">*</span></label>
                            <textarea class="form-control" name="content"
                                      placeholder="Enter long content"><?= !$isNew ? $blog->content : '' ?></textarea>
                            <label for="desc" class="error" style="display:none;">This field is
                                required</label>
                            <script type="text/javascript">
                                var editor = CKEDITOR.replace('content',
                                    {
                                        customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                    });
                                CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                            </script>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->