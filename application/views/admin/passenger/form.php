<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="" method="post" class="forms" enctype="multipart/form-data">
                <div class="box-header">
                    <h3 class="box-title">Passenger Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
                    <span class="pull-right add-new">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                    </span>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <img style="height:240px;object-fit: cover;" src="<?php echo base_url('uploads/passenger/') ?><?= $passenger->image ?: 'default.jpg' ?>" class="img-responsive">
                            <label>Image <span class="text-danger">*</span></label>
                            <input type="file" name="image">
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="full_name" value="<?= !$isNew ? $passenger->full_name : '' ?>" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Email <span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" disabled="" value="<?= !$isNew ? $passenger->email : '' ?>" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Phone No. <span class="text-danger">*</span></label>
                                    <input type="hidden" name="phone_cc" value="<?= !$isNew ? $passenger->phone_cc : '' ?>">
                                    <input class="form-control call-code" name="phone_no" value="<?= !$isNew ? $passenger->phone_no : '' ?>" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Tel Phone No. <span class="text-danger">*</span></label>
                                    <input type="hidden" name="tel_phone_cc" value="<?= !$isNew ? $passenger->tel_phone_cc : '' ?>">
                                    <input class="form-control call-code" name="tel_phone_no" value="<?= !$isNew ? $passenger->tel_phone_no : '' ?>" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Address <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="address" value="<?= !$isNew ? $passenger->address : '' ?>" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Address 1<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="address_1" value="<?= !$isNew ? $passenger->address_1 : '' ?>" required>
                                </div>
                                <div class="col-md-4">
                                    <label>City <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="city" value="<?= !$isNew ? $passenger->city : '' ?>">
                                </div>
                                <div class="col-md-4">
                                    <label>Post Code <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="post_code" value="<?= !$isNew ? $passenger->post_code : '' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(function() {
        $('.call-code').intlTelInput({
            separateDialCode: true,
            initialCountry: "auto"
        });
        $('.call-code').intlTelInput("setCountry", '<?= getCountryByCountryCode((!$isNew ? $passenger->phone_cc : 'uk')) ?>');
        $("input").on("countrychange", function(e, countryData) {
            $('[name=phone_cc]').val('+' + countryData.dialCode);
            $('[name=tel_phone_cc]').val('+' + countryData.dialCode);
        });
    });
</script>