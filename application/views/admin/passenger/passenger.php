<!-- Small boxes (Stat box) -->
<div class="row">
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-aqua">
      <span class="info-box-icon"><i class="fa fa-user"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Passengers Today</span>
        <span class="info-box-number"><?= $count['today'] ?></span>
        <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
        </div>
        <span class="progress-description">
          100% <small class="pull-right"><?= date('d/m/Y') ?></small>
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-green">
      <span class="info-box-icon"><i class="fa fa-user"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Passengers Monthly</span>
        <span class="info-box-number"><?= $count['month'] ?></span>

        <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
        </div>
        <span class="progress-description">
          100% <small class="pull-right"><?= date('F Y') ?></small>
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-blue">
      <span class="info-box-icon"><i class="fa fa-user"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Passengers Annual</span>
        <span class="info-box-number"><?= $count['annual'] ?></span>

        <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
        </div>
        <span class="progress-description">
          100% <small class="pull-right"><?= date('Y') ?></small>
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

</div>

<div class="row">
  <div class="col-md-12">
    <?php flash() ?>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Passenger Manager</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-striped" id="dt-pages">
          <thead class="thead-dark">
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Address</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (!empty($passengers)) :
              foreach ($passengers as $index => $passenger) :
            ?>
                <tr>
                  <td class="lboarder-<?= $passenger->is_active ? 'success' : 'danger' ?>">
                    <img src="<?= base_url('uploads/passenger/' . ($passenger->image ?: 'default.jpg')) ?>" class="circular-profile profile-img-zoom">
                  </td>
                  <td><?php echo $passenger->full_name ?></td>
                  <td>
                    <?php echo $passenger->phone_cc . $passenger->phone_no ?><br>
                    <!-- <small><i><?php echo $passenger->login_verification_code ? 'verification code: ' . $passenger->login_verification_code : '' ?></i></small> -->
                  </td>
                  <td><?php echo $passenger->email ?></td>
                  <td><?php echo $passenger->address ?></td>
                  <td><?php echo $passenger->is_active ? '<span class="badge bg-green">Enable</span>' : '<span class="badge bg-red">Disabled</span>' ?></td>
                  <td>
                    <div class="dropdown">
                      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                        <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('admin/passenger/update/' . $passenger->id) ?>"><i class="fa fa-edit"></i> Details</a></li>
                        <li><a href="<?php echo site_url('admin/booking?action=search&passenger_id=' . $passenger->id) ?>" target="_blank"><i class="fa fa-book"></i> View all booking </a></li>
                        <li><a href="<?php echo site_url('admin/booking?action=search&passenger_id=' . $passenger->id . '&status=new') ?>" target="_blank"><i class="fa fa-bars"></i> View current booking </a></li>
                        <li><a href="<?php echo site_url('admin/booking?action=search&passenger_id=' . $passenger->id . '&status=completed') ?>" target="_blank"><i class="fa fa-undo"></i> View booking history </a></li>
                        <li> <a href="<?php echo site_url('admin/passenger/delete/' . $passenger->id) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</a>
                        </li>
                      </ul>
                    </div>
                  </td>
                </tr>
            <?php endforeach;
            endif;
            ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
</div><!-- /.row -->

<script>
  $(function() {
    $('#dt-pages').dataTable();
  });
</script>