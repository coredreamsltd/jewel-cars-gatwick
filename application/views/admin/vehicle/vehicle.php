<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Vehicle Manager</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo site_url('admin/vehicle/add_update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" id="dt-pages">
                    <thead>
                        <tr>
                            <th style="width:3%"></th>
                            <th>INFO</th>
                            <th>VEHICLE</th>
                            <th style="width: 110px;">CAPACITY</th>
                            <th>PRICING</th>
                            <th width="12%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($vehicles)) :
                            foreach ($vehicles as $index => $vehicle) :
                                $driver = $this->driver_model->get(['vehicle_id' => $vehicle->id]);
                        ?>
                                <tr>
                                    <td><?php echo ++$index; ?></td>
                                    <td>
                                       <p class="p-bold"><strong> <i class="fa fa-car"></i> <?= $vehicle->plate_number ?></strong></p>
                                       <p>  <i class="fa fa-phone"></i> <?= $vehicle->mobile ?></p>
                                        <?php if ($driver) : ?>
                                           <p> <i class="fa fa-user-secret"></i> <?= $driver->name  ?> [<?= $driver->phone_cc . $driver->mobile  ?>]</p>
                                        <?php endif; ?>
                                    </td>
                                    <td> <i class="fa fa-car"></i> <?= $vehicle->make . ' ' . $vehicle->model ?> ( <?= ucfirst($vehicle->color) ?>)<br></td>
                                    <td>
                                        <i class="fa fa-users"> &nbsp;<?= $vehicle->passenger ?></i> &nbsp;&nbsp;&nbsp;
                                        <i class="fa fa-suitcase"> &nbsp;<?= $vehicle->luggage ?></i>&nbsp;&nbsp;&nbsp;
                                        <i class="fa fa-soccer-ball-o"> &nbsp;<?= $vehicle->sports_luggage ?></i>
                                    </td>
                                    <td>
                                        <?php if ($vehicle->pricing_scheme) : ?>
                                            <?= $vehicle->pricing_scheme ?><br>
                                            <?= $vehicle->pricing_scheme ?> pricing scheme
                                        <?php else:?>
                                            -
                                        <?php endif; ?>

                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-info" href="<?php echo site_url('admin/vehicle/add_update/' . $vehicle->id) ?>">Edit</a>
                                        <a class="btn btn-sm btn-danger" href="<?php echo site_url('admin/vehicle/delete/' . $vehicle->id) ?>" onclick="return confirm('Are you sure?')">Delete</a>
                                    </td>
                                </tr>
                        <?php endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(function() {
        $('#dt-pages').dataTable();
    });
</script>