<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="" method="post" class="forms" enctype="multipart/form-data">
                <div class="box-header">
                    <h3 class="box-title">Vehicle | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
                    <span class="pull-right add-new">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                    </span>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h4>BASIC INFO</h4>
                            <label>Plate number <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="plate_number" value="<?= !$isNew ? $vehicle->plate_number : '' ?>" required>
                            <label>Mobile <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="mobile" value="<?= !$isNew ? $vehicle->mobile : '' ?>">
                            <label>Pricing scheme <span class="text-danger">*</span></label>
                            <select class="form-control" name="pricing_scheme" required>
                                <option value="">---------</option>
                                <?php if (!empty($fleets)) : foreach ($fleets as $fleet) : ?>
                                        <option <?= !$isNew &&  $vehicle->pricing_scheme == $fleet->title ? 'selected' : '' ?>>
                                            <?= $fleet->title ?>
                                        </option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <h4>CAR DETAILS</h4>
                            <div class="is-relative auto-add">
                                <label for="car-manufacture-choice">Car manufacture <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" list="car-manufacture" id="car-manufacture-choice" name="make" value="<?= !$isNew ? $vehicle->make : '' ?>" required>
                                <span class="add-btn"></span>
                            </div>
                            <datalist id="car-manufacture">
                                <?php if (!empty($manufacturers)) : foreach ($manufacturers as $manufacturer) : ?>
                                        <option value="<?= $manufacturer->name ?>">
                                    <?php endforeach;
                                endif; ?>
                            </datalist>
                            <div class="is-relative auto-add">
                                <label for="car-model-choice">Car model <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" list="car-model" id="car-model-choice" name="model" value="<?= !$isNew ? $vehicle->model : '' ?>" required>
                                <span class="add-btn"></span>
                            </div>
                            <datalist id="car-model">
                            </datalist>

                            <label>Vehicle class <span class="text-danger">*</span></label>
                            <select class="form-control" name="class" required>
                                <option>---------</option>
                                <?php if (!empty($fleets)) : foreach ($fleets as $fleet) : ?>
                                        <option <?= !$isNew &&  $vehicle->class == $fleet->title ? 'selected' : '' ?>>
                                            <?= $fleet->title ?>
                                        </option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                            <label>Color <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="color" value="<?= !$isNew ? $vehicle->color : '' ?>" required>
                            <label>Others</label>
                            <textarea class="form-control" name="others"><?= !$isNew ? $vehicle->others : '' ?></textarea>
                        </div>
                        <div class="col-md-4">
                            <h4>CAR CAPACITY</h4>
                            <label>Seats(excluding driver's seat) <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="passenger" value="<?= !$isNew ? $vehicle->passenger : '' ?>" required>
                            <label>Luggage <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="luggage" value="<?= !$isNew ? $vehicle->luggage : '' ?>" required>
                            <label>Sport luggage <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="sports_luggage" value="<?= !$isNew ? $vehicle->sports_luggage : '' ?>" required>
                            <label>Child seat <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="child_seat" value="<?= !$isNew ? $vehicle->child_seat : '' ?>" required>
                            <label>Production year <span class="text-danger">*</span></label>
                            <select class="form-control" name="year" required>
                                <option>---------</option>
                                <?php for ($i = date('Y'); $i >= (date('Y') - 200); --$i) : ?>
                                    <option <?= !$isNew &&  $vehicle->year == $i ? 'selected' : '' ?>>
                                        <?= $i ?>
                                    </option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<input type="hidden" name="make_id">
<script>
    var SITE_URL = '<?= site_url() ?>';
    $(function() {
        getModel(($('[name=make]').val().trim()).replace(' ', '-').toLowerCase());
        $('[name=make]').change(function() {
            var that = $(this);
            var make = ($(this).val().trim()).replace(' ', '-').toLowerCase();
            $.ajax({
                url: SITE_URL + "admin/vehicle/ajaxCheckManufacture/" + make,
                dataType: 'json',
                success: function(response) {
                    getModel(make);
                    if (!response.status) {
                        var html = '<i class="fa fa-plus text-primary add-new-make"></i>';
                        that.next('span').html(html);
                        return;
                    }
                    $('.add-new-make').remove();
                }
            });
        });

        $('[name=model]').change(function() {
            var that = $(this);
            var model = ($(this).val().trim()).replace(' ', '-').toLowerCase();
            $.ajax({
                url: SITE_URL + "admin/vehicle/ajaxCheckModel/" + model,
                dataType: 'json',
                success: function(response) {
                    if (!response.status) {
                        var html = '<i class="fa fa-plus text-primary add-new-model"></i>';
                        that.next('span').html(html);
                        return;
                    }
                    $('.add-new-model').remove();
                }
            });
        });

        $(".box-body").on("click", ".add-new-make", function() {
            var that = $(this);
            var make = ($('[name=make]').val());
            $.ajax({
                url: SITE_URL + "admin/vehicle/ajaxAddManufacture/" + make,
                dataType: 'json',
                success: function(response) {
                    if (!response.status) {
                        alert(response.msg);
                        return;
                    }
                    getModel(make);
                    that.removeClass('add-new-make');
                    that.removeClass('text-primary');
                    that.removeClass('fa-plus');
                    that.addClass('fa-check text-success');
                    $('[name=make_id]').val(response.data);
                }
            });
        });

        $(".box-body").on("click", ".add-new-model", function() {
            var that = $(this);
            var model = ($('[name=model]').val());
            $.ajax({
                url: SITE_URL + "admin/vehicle/ajaxAddModel?model=" + model + '&manufacturer_id=' + $('[name=make_id]').val(),
                dataType: 'json',
                success: function(response) {
                    if (!response.status) {
                        alert(response.msg);
                        return;
                    }
                    that.removeClass('add-new-make');
                    that.removeClass('text-primary');
                    that.removeClass('fa-plus');
                    that.addClass('fa-check text-success');
                    that.attr('data-make-id', response.data);
                }
            });
        });
    });

    function getModel(make) {
        $.ajax({
            url: SITE_URL + "admin/vehicle/ajaxGetModel/" + make,
            dataType: 'json',
            success: function(response) {
                $('[name=make_id]').val(response.data.manufacturer_id);
                if (!response.status) {
                    var html = '<i class="fa fa-plus text-primary add-new-model"></i>';
                    $('#car-model-choice').next('span').html(html);
                    $('#car-model').html('');
                    // alert(response.msg);
                    return;
                }

                var html_model = '';
                $(response.data.models).each(function(index, value) {
                    html_model += '<option value="' + value.model + '">';
                });
                console.log(html_model);
                $('#car-model').html(html_model);
            }
        });
    }
</script>