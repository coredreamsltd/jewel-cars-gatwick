<style>
    div#map_geolocation img {
        max-width: none;
    }

    #map-canvas {
        height: 525px;
    }

    #map-canvas img {
        max-width: none !important;
    }

    /* #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 50%;
        margin-top: 11px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    } */

    .loader-map p {
        font-size: 24px;
        background: #f5f5f5c2;
    }

    .loader-map {
        position: absolute;
        top: 50%;
        left: 48%;
        margin-left: -50px;
        width: 480px;
        height: 133px;
        z-index: 3;
        text-align: center;
    }

    .result {
        font-size: 0.8em;
        margin: 5px;
        margin-top: 0px;
        padding: 4px 8px;
        border-radius: 2px;
        background: #F0F7FF;
        border: 2px solid #D7E7FF;
        cursor: pointer;
        min-height: 5em;
    }

    .result.active {
        background-color: #D9E7F7;
        border-color: #9DB9E4;
    }

    .result img {
        float: right;
    }
</style>
<?php flash() ?>
<div class="box">
    <form action="" method="post">
        <div class="box-header">
            <h3 class="box-title btn-block">
                Location | <?= segment(4) ? 'Edit' : 'Add' ?>
                <div class="pull-right">
                    <a href="<?= site_url('admin/fleet_manager/add_update/' . $_GET['fleet_id'] . '#tab7') ?>" class="btn btn-primary">
                        <i class="fa fa-bars"></i> All Locations
                    </a>

                </div>
            </h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Location</label>
                        <input id="map-start" type="text" class="form-control" name="title" value="<?= $isEdit ? $zone->title : '' ?>" placeholder="Enter location" required="">
                        <span class="startLatLng">
                            <input type="hidden" data-geo="lat" name="lat" value="<?= $isEdit ? $zone->lat : '' ?>">
                            <input type="hidden" data-geo="lng" name="lng" value="<?= $isEdit ? $zone->lng : '' ?>">
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Distance included</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="rate_distance" value="<?= $isEdit ? $zone->rate_distance : '0.00' ?>" required="">
                            <span class="input-group-addon"><?= DISTANCE ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <div class="input-group">
                            <span class="input-group-addon"><?= CURRENCY ?></span>
                            <input type="text" class="form-control" name="rate" value="<?= $isEdit ? $zone->rate : '0.00' ?>" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Radius</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="radius" value="<?= $isEdit ? $zone->radius : '0.20' ?>" required="">
                            <span class="input-group-addon"><?= DISTANCE ?></span>
                        </div>
                    </div>
                    <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Save</button>
                </div>

                <div class="col-md-9">
                    <div class="form-group">
                        <input type="hidden" name="coordinates" value="<?= $isEdit ? $zone->coordinates : '' ?>">
                        <label>Draw zone coverage area</label>
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </form>

</div><!-- /.box -->
<script src="<?= base_url('assets/Wicket/wicket.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/Wicket/wicket-gmap3.js') ?>" type="text/javascript"></script>
<script>
    var map;
    var contents;
    var circle = '';
    var polygon = '';
    var markers = [];
    var marker = '';
    var coordinates = <?= $isEdit ? "'" . $zone->coordinates . "'" : 'null' ?>;
    var lat = <?= $isEdit ? "'" . $zone->lat . "'" : 'null' ?>;
    var lng = <?= $isEdit ? "'" . $zone->lng . "'" : 'null' ?>;
    var start = <?= $isEdit ? "'" . $zone->title . "'" : 'null' ?>;

    $(window).load(function() {
        initMap();
        init_draw_tools(map);
        draw_polygon(map, coordinates);
    });

    $(function() {
        var start = $("#map-start"),
            end = $('#map-end');
        start.geocomplete({
            country: 'uk',
            details: ".startLatLng",
            detailsAttribute: "data-geo",
            types: ['geocode', 'establishment']
        }).bind("geocode:result", function(event, result) {
            lat = result.geometry.location.lat();
            lng = result.geometry.location.lng();
            start = result.formatted_address;

            drawRoute(start, lat, lng);

        });
        $('[name=radius]').change(function() {
            lat = $('[name=lat]').val();
            lng = $('[name=lng]').val();
            if (!lat || !lng) {
                return;
            }
            drawRoute($('[name=title]').val(), lat, lng);
        });
    });

    function initMap() {
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: {
                lat: parseFloat(<?= !empty($zone->lat) ?  $zone->lat : 51.5074 ?>),
                lng: parseFloat(<?= !empty($zone->lng) ?  $zone->lng  : 0.1278 ?>)
            },
            zoom: 10
        });
        if ('<?= $isEdit ?>') {
            marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(<?= !empty($zone->lat) ?  $zone->lat : 0 ?>),
                    lng: parseFloat(<?= !empty($zone->lng) ?  $zone->lng  : 0 ?>)
                },
                map: map
            });
        }

        // search_box(map);
    }

    function drawCircle(map, lat, lng, radius) {
        init_draw_tools(map);
        if (circle ? true : false) {
            // circle.setRadius(0);
        }

        circle = {
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            map: map,
            center: {
                lat: parseFloat(lat),
                lng: parseFloat(lng)
            },
            radius: parseFloat(radius)
        };
        map = new google.maps.Circle(circle)
        // map.bindTo('center', marker, 'position');
        google.maps.event.addListener(map, 'click', function(args) {
            console.log(args.latLng);
            // var wkt = new Wkt.Wkt();
            // wkt.fromObject(polygon);
            // console.log('9989898989988',wkt.write());
            // $('[name=coordinates]').val(wkt.write());
        });
    }

    function init_draw_tools(map) {
        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT,
                // drawingModes: ['']
                drawingModes: ['polygon', 'circle']
            }
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
            var wkt = new Wkt.Wkt();
            wkt.fromObject(polygon);
            // console.log(wkt.write());
            $('[name=coordinates]').val(wkt.write());
        });

        google.maps.event.addListener(drawingManager, 'circlecomplete', function(polygon) {
            var wkt = new Wkt.Wkt();
            wkt.fromObject(polygon);
            // console.log(wkt.write());
            $('[name=coordinates]').val(wkt.write());
        });

    }

    function draw_polygon(map, coordinates) {

        if (coordinates === null) {
            return;
        }
        var wkt = new Wkt.Wkt();
        wkt.read(coordinates);
        $('[name=coordinates]').val(coordinates);
        // Construct the polygon.
        polygon = wkt.toObject({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        });
        polygon.setMap(map);
    }


    function drawRoute(start, start_lat, start_lng) {
        if (start == '' || typeof start == 'undefined') {
            start_lat = null;
            start_lng = null;
        }

        if (start_lat && start_lng) {
            mapsApiMarker(document.getElementById('map-canvas'), start_lat, start_lng);
            drawCircle(map, start_lat, start_lng, (parseFloat($('[name=radius]').val() * 1609.34).toFixed(2)));
        }
        var coordinates = getPolygonCoordinates(lat, lng, (parseFloat($('[name=radius]').val()).toFixed(2)));
        $('[name=coordinates]').val(coordinates);
    }

    function mapsApiMarker(map_div, lat, lng) {
        var radius = parseInt($('[name=radius]').val());
        var myLatLng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        };
        var zoom = 12;
        if (radius >= 4 && radius < 7) {
            zoom = 10;
        } else if (radius >= 7 && radius < 25) {
            zoom = 8;
        } else if (radius >= 25 && radius < 55) {
            zoom = 6;
        } else if (radius >= 55 && radius < 400) {
            zoom = 5;
        } else if (radius >= 400) {
            zoom = 4;
        }
        map = new google.maps.Map(map_div, {
            center: myLatLng,
            zoom: zoom
        });
        marker = new google.maps.Marker({
            map: map,
            position: myLatLng
        });
    }

    function getPolygonCoordinates(lat, lng, radius) {
        var dir = 1;
        var lat = parseFloat(lat);
        var lng = parseFloat(lng);
        var point = new google.maps.LatLng(lat, lng);
        var d2r = Math.PI / 180; // degrees to radians 
        var r2d = 180 / Math.PI; // radians to degrees 
        var earthsradius = 3963; // 3963 is the radius of the earth in miles

        var points = 64;
        if (radius >= 100 && radius < 200) {
            points = 512;
        } else if (radius >= 200 && radius < 300) {
            points = 1024;
        } else if (radius >= 300 && radius < 400) {
            points = 2048;
        } else if (radius >= 400 && radius < 500) {
            points = 4096;
        } else if (radius > 500) {
            points = 8192;
        }
            

        // find the raidus in lat/lon 
        var rlat = (radius / earthsradius) * r2d;
        var rlng = rlat / Math.cos(point.lat() * d2r);

        var extp = new Array();
        if (dir == 1) {
            var start = 0;
            var end = points + 1; // one extra here makes sure we connect the path
        } else {
            var start = points + 1;
            var end = 0;
        }
        var POLYGON = 'POLYGON((';
        for (var i = start;
            (dir == 1 ? i < end : i > end); i = i + dir) {
            var theta = Math.PI * (i / (points / 2));
            ey = point.lng() + (rlng * Math.cos(theta)); // center a + radius x * cos(theta) 
            ex = point.lat() + (rlat * Math.sin(theta)); // center b + radius y * sin(theta) 
            POLYGON += ey + ' ' + ex + ',';
            // extp.push(new google.maps.LatLng(ex, ey));
        }
        POLYGON = POLYGON.slice(0, -1);
        POLYGON += '))';
        return POLYGON;
    }
</script>