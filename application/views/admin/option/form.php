<?php flash() ?>
<div class="row">
    <div class="col-md-12">
        <form action method="post" class="forms" enctype="multipart/form-data">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Setting Manager </h3>
                    <div class="pull-right">
                        <button class="btn btn-flat btn-primary" type="submit"> Save</button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row login-box-body">
                        <?php foreach ($options as $key => $option): ?>
                            <?php if($key =='logo'):?>
                                <div class="col-md-6">
                                <label><?= ucwords(str_replace('_', ' ', $key)) ?><span
                                            class="text-danger">*</span></label>
                                <input type="file" name="<?= $key ?>">

                                <?php if($option):?>
                                    <img src="<?=base_url('uploads/page/'.$option)?>" width='50px'>
                                <?php endif;?>
                                </div>

                            <?php elseif($key=='trip_script' || $key=='trip_html_code' || $key=='driver_guide'||
                            $key=='google_tracking_code'|| $key=='google_analytic_code'|| $key=='seo_tracking_code'|| $key=='we_chat_script'):?>
                                    <div class="col-md-6">
                                <label><?= ucwords(str_replace('_', ' ', $key)) ?><span
                                            class="text-danger">*</span></label>
                                <textarea class="form-control" rows="5" name="<?=$key?>"><?php echo $option ?></textarea>
                                    </div>
                            <?php else:?>
                                <div class="col-md-6">
                                    <label><?= ucwords(str_replace('_', ' ', $key)) ?><span
                                                class="text-danger">*</span></label>
                                    <input type="text" class="form-control"
                                           name="<?= $key ?>"
                                           value="<?php echo $option ?>"
                                    >
                                </div>

                            <?php endif;?>
                        <?php endforeach; ?>
                    </div>
                </div>
        </form>
    </div><!-- /.box -->
</div>
