
<?php $isNew = isset($seo) ? false : true; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">SEO Tools | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="" method="post" class="forms">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Page Name<span class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter page name" class="form-control required" name="name" value="<?php echo!$isNew ? $seo->name : '' ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Link<span class="text-danger">*</span></label>
                                            <input type="text" name="link" placeholder="Enter url for seo (enter '/' for home page for other eg: '/about')" class="form-control" value="<?php echo!$isNew ? $seo->link : '' ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Page Title<span class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter page title" class="form-control required" name="title" value="<?php echo!$isNew ? $seo->title : '' ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Description<span class="text-danger">*</span></label>
                                            <textarea name="description" placeholder="Enter page description" class="form-control required"><?php echo!$isNew ? $seo->description : '' ?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Keywords<span class="text-danger">*</span></label>
                                            <textarea name="keywords" placeholder="Enter keywords (comma separated)" class="form-control required"><?php echo!$isNew ? $seo->keywords : '' ?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input class="btn btn-primary" type="submit" value="Save">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div><!-- /.box-body -->
                </div>

            </div>
        </div>

    </div>
</div>
