<?php $count =1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash(); ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">SEO Manager</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/seo/seo_add_update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" id="dt-seo">
                    <thead>                           
                        <tr>
                            <th style="width: 70px">#</th>
                            <th>SEO Title</th>
                            <th>Link</th>
                            <th style="width: 150px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($seo)) {
                            foreach ($seo as $s) {
                                ?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo $s['name'] ?></td>
                                    <td><a href="<?php echo $s['link'] ? base_url($s['link']) : base_url() ?>" target="_blank"><?php echo $s['link'] ? $s['link'] : base_url() ?></a></td>
                                    <td>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('admin/seo/seo_add_update/' . $s['id']) ?>">Edit</a>
                                        <a class="btn btn-sm btn-danger" href="<?php echo base_url('admin/seo/seo_delete/' . $s['id']) ?>" onclick="return confirm('Are you sure?')">Delete</a>                                    
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(function () {
        $('#dt-seo').dataTable();
    });
</script>