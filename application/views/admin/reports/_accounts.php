<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Accounts</h3>
            </div><!-- /.box-header -->
            <?php flash() ?>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <form action="" method="post" class="forms">
                            <div class="form-group">
                                <label class="control-label">Generate Report For</label>
                                <select class="form-control" name="account_type">
                                    <option value="personal" <?= isset($_POST['account_type']) && $_POST['account_type'] == 'personal' ? 'selected' : '' ?>>Passenger Account</option>
                                    <option value="driver" <?= isset($_POST['account_type']) && $_POST['account_type'] == 'driver' ? 'selected' : '' ?>>Drivers Accounts</option>
                                </select>
                            </div>
                            <button class="btn btn-primary" type="submit"> Get Accounts</button>
                        </form>
                    </div>
                </div>
            </div><!-- /.box-body -->

            <div class="box-body">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title pull-right">
                            <a href="<?= base_url('admin/reports/export') ?>" class="btn btn-success" target="_blank" style="margin-right: 5px;">
                                <i class="fa fa-download"></i> Export Report File
                            </a>
                        </h3>
                    </div>
                    <div class="box-body">
                        <?php if (!empty($reports)) : ?>
                            <div class="table-responsive">
                                <table class="table" id="datatable">
                                    <thead>
                                        <tr>
                                            <?php foreach ($reports[0] as $k => $v) : ?>
                                                <th><?= $k ?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($reports as $index => $row) : ?>
                                            <tr>
                                                <?php foreach ($row as $k => $v) : ?>
                                                    <td><?= $v ?></td>
                                                <?php endforeach; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(function() {

        check_generate_report_for_selection();

        $('[name=report_for]').change(function() {
            check_generate_report_for_selection();
        });

    });

    function check_generate_report_for_selection() {
        var report_for = $('[name=report_for] :selected').val();

        $('.extra-info').hide();
        $('.extra-info input, .extra-info select').attr('disabled', '');

        $('#' + report_for).show();
        $('#' + report_for + ' input, #' + report_for + ' select').removeAttr('disabled');
    }
</script>