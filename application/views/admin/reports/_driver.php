<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Driver Reports</h3>
            </div><!-- /.box-header -->
            <div class="box-body">

                <?php flash() ?>


                <form action="" method="post">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" class="form-control datepicker" name="date_from" placeholder="From: dd/mm/yyyy" value="<?= $_POST ? $_POST['date_from'] : '' ?>" required="">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control datepicker" name="date_to" placeholder="To: dd/mm/yyyy" value="<?= $_POST ? $_POST['date_to'] : '' ?>" required="">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="driver_id">
                                <option value="">All drivers</option>
                                <?php if ($drivers): foreach ($drivers as $driver): ?>
                                        <option value="<?= $driver->driver_id ?>" <?= $_POST && $_POST['driver_id'] == $driver->driver_id ? 'selected' : '' ?>><?= $driver->name ?> [<?= $driver->nickname ?>]</option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="fleet_id">
                                <option value="">All vehicle types</option>
                                <?php if ($fleets): foreach ($fleets as $fleet): ?>
                                        <option value="<?= $fleet->id ?>" <?= $_POST && $_POST['fleet_id'] == $fleet->id ? 'selected' : '' ?>><?= $fleet->title ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-default">Generate</button>
                        </div>
                    </div>
                </form>

            </div>

            <div class="box-body">

                <?php if (isset($reports) && !empty($reports)): ?>
                    <a href="<?= base_url('admin/reports/export') ?>" target="_blank" class="btn btn-lg btn-danger">Export Report File</a><br><br>
                <?php endif; ?>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Driver</th>
                                <th>Nickname</th>
                                <th>Driver Fare</th>
                                <th>Commission</th>
                                <th>Driver Earning</th>
                                <th>Company Earning</th>
                                <th>Cash Payment</th>
                                <th>Card Payment</th>
                                <th>Outstanding Amount</th>
                            </tr>
                        </thead>
                        <?php if (isset($reports) && !empty($reports)): ?>
                            <tbody>
                                <?php foreach ($reports as $report): ?>
                                    <tr>
                                        <td><?= $report['name'] ?></td>
                                        <td><?= $report['nickname'] ?></td>
                                        <td><?= SITE_CURRENCY . $report['driver_fare_sum'] ?></td>
                                        <td><?= SITE_CURRENCY . $report['company_commission_sum'] ?></td>
                                        <td><?= SITE_CURRENCY . $report['driver_earning_sum'] ?></td>
                                        <td><?= SITE_CURRENCY . $report['company_earning_sum'] ?></td>
                                        <td><?= SITE_CURRENCY . $report['cash_payment_sum'] ?></td>
                                        <td><?= SITE_CURRENCY . $report['card_payment_sum'] ?></td>
                                        <td><?= SITE_CURRENCY . $report['outstanding_amount'] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2">Totals</th>
                                    <th><?= SITE_CURRENCY . $totals['driver_fare_sum'] ?></th>
                                    <th><?= SITE_CURRENCY . $totals['company_commission_sum'] ?></th>
                                    <th><?= SITE_CURRENCY . $totals['driver_earning_sum'] ?></th>
                                    <th><?= SITE_CURRENCY . $totals['company_earning_sum'] ?></th>
                                    <th><?= SITE_CURRENCY . $totals['cash_payment_sum'] ?></th>
                                    <th><?= SITE_CURRENCY . $totals['card_payment_sum'] ?></th>
                                    <th><?= SITE_CURRENCY . $totals['outstanding_amount'] ?></th>
                                </tr>
                            </tfoot>
                        <?php endif; ?>
                    </table>
                </div>

            </div>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(function () {

        check_generate_report_for_selection();

        $('.datepicker-from').datepicker({
            dateFormat: 'dd/mm/yy',
            onSelect: function (dateText, inst) {
                dateText = dateText.split("/");
                dateText = new Date(dateText[2], dateText[1] - 1, dateText[0]);
                $('.datepicker-to').datepicker({
                    minDate: new Date(dateText),
                    dateFormat: 'dd/mm/yy'
                });
            }
        });

        $('[name=report_for]').change(function () {
            check_generate_report_for_selection();
        });

    });

    function check_generate_report_for_selection() {
        var report_for = $('[name=report_for] :selected').val();

        $('.extra-info').hide();
        $('.extra-info input, .extra-info select').attr('disabled', '');

        $('#' + report_for).show();
        $('#' + report_for + ' input, #' + report_for + ' select').removeAttr('disabled');
    }
</script>