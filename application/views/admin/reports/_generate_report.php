<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Booking Report</h3>
            </div><!-- /.box-header -->
            <?php flash() ?>
            <div class="box-body">
                <div class="row">
                    <form action="" method="post" class="forms">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Generate Report For</label>
                                <select class="form-control" name="report_for">
                                    <option value="bookings" <?= isset($_POST['report_for']) && $_POST['report_for'] == 'bookings' ? 'selected' : '' ?>>Bookings</option>
                                    <!-- <option value="job_sheet" <?= isset($_POST['report_for']) && $_POST['report_for'] == 'job_sheet' ? 'selected' : '' ?>>Job Sheet</option>
                                        <option value="user_email" <?= isset($_POST['report_for']) && $_POST['report_for'] == 'user_email' ? 'selected' : '' ?>>User Email</option> -->
                                </select>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div id="bookings" class="extra-info">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">Date From</label>
                                            <input type="text" class="form-control datepicker-from" placeholder="From Date (dd/mm/yyyy)" name="date_from" value="<?= isset($_POST['date_from']) ? $_POST['date_from'] : '' ?>" required="">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Date To</label>
                                            <input type="text" class="form-control datepicker-to" placeholder="To Date (dd/mm/yyyy)" name="date_to" value="<?= isset($_POST['date_to']) ? $_POST['date_to'] : '' ?>" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit" style="float: right">Generate Report</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-body -->

            <div class="box-body">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title pull-right">
                            <a href="<?= base_url('admin/reports/export') ?>" class="btn btn-success" target="_blank" style="margin-right: 5px;">
                                <i class="fa fa-download"></i> Export Report File
                            </a>
                        </h3>
                        <h3 class="box-title pull-right">
                            <a href="<?= base_url('admin/reports/pdf') ?>" class="btn btn-danger" target="_blank" style="margin-right: 5px;">
                                <i class="fa fa-download"></i> Export PDF File
                            </a>
                        </h3>
                    </div>
                    <div class="box-body">
                        <?php if (!empty($reports)) : ?>
                            <div class="table-responsive">
                                <table class="table table-striped" id="dt-table">
                                    <thead>
                                        <tr>
                                            <?php foreach ($reports[0] as $k => $v) : ?>
                                                <th><?= $k ?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($reports as $index => $row) : ?>
                                            <tr>
                                                <?php foreach ($row as $k => $v) : ?>
                                                    <td><?= $v ?></td>
                                                <?php endforeach; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(document).ready(function() {
        $('#dt-table').dataTable();
    });
    
    $(function() {

        check_generate_report_for_selection();

        $('.datepicker-from').datepicker({
            dateFormat: 'dd/mm/yy',
        });

        $('.datepicker-to').datepicker({
            dateFormat: 'dd/mm/yy'
        });

        $('[name=report_for]').change(function() {
            check_generate_report_for_selection();
        });

    });

    function check_generate_report_for_selection() {
        var report_for = $('[name=report_for] :selected').val();

        $('.extra-info').hide();
        $('.extra-info input, .extra-info select').attr('disabled', '');

        $('#' + report_for).show();
        $('#' + report_for + ' input, #' + report_for + ' select').removeAttr('disabled');
    }
</script>