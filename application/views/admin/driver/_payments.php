<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title btn-block">
                    Payments - <?= $driver->name ?>
                
                <div class="pull-right">
                    <button data-toggle="modal" data-target="#modal-payment" class="btn btn-primary">Add Payment</button>
                </div>
                </h3>
                
            </div><!-- /.box-header -->

            <div class="box-body">

                <table class="table table-bordered border-color-fix dt-tables">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Payment Date</th>
                            <th>Payment Method</th>
                            <th>Paid Amount</th>
                            <th width="10%">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if ($payments) {
                            foreach ($payments as $index => $p) {
                                ?>
                                <tr>
                                    <td><?= ++$index ?></td>
                                    <td><?= DateTime::createFromFormat("Y-m-d", $p->payment_date)->format('d M, Y') ?></td>
                                    <td><?= $p->payment_method ?></td>
                                    <td><?= CURRENCY . $p->amount ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?= base_url('admin/driver/delete_payment/' . $p->id) ?>" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>

                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->


<div class="modal fade" id="modal-payment" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Payment</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <th>Amount</th>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon"><?= CURRENCY ?></span>
                                    <input type="text" class="form-control" name="amount" required="">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Payment Method</th>
                            <td>
                                <select class="form-control" name="payment_method">
                                    <option>Cash</option>
                                    <option>Bank Deposit</option>
                                    <option>Other</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Payment Date/Time</th>
                            <td><input type="text" name="payment_date" class="form-control all-datepicker" required=""></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Payment</button>
                </div>
            </form>
        </div>
    </div>
</div>