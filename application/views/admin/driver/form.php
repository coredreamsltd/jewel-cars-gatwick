<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="" method="post" class="forms" enctype="multipart/form-data">
                <div class="box-header">
                    <h3 class="box-title">Driver Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
                    <span class="pull-right add-new">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                </span>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="full_name"
                                   value="<?= !$isNew ? $passenger->full_name : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <label>Email <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" name="email"
                                   value="<?= !$isNew ? $passenger->email : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <label>Phone CC <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="phone_cc"
                                   value="<?= !$isNew ? $passenger->phone_cc : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <label>Phone No. <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="phone_no"
                                   value="<?= !$isNew ? $passenger->phone_no : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <label>Address <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="address"
                                   value="<?= !$isNew ? $passenger->address : '' ?>" required>
                        </div>
                        <div class="col-md-12">
                            <label>Image <span class="text-danger">*</span></label>
                            <input type="file" name="image" >
                            <img width="200px"
                                 src="<?php echo base_url('uploads/passenger/' . $passenger->image) ?>"
                                 class="img-responsive">
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->