<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title btn-block">Driver Job Sheets

                    <div class="pull-right">
                        <a href="<?=site_url('admin/driver/send_job_sheet/'.$driver->id)?>" class="btn btn-warning"><i class="fa fa-send"></i> Send Job Sheet</a>
                    </div>
                </h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <form action="" method="post" id="form-bulk-action">
                    <div class="table-responsive">
                        <table class="table table-bordered border-color-fix dt-tables">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sent Date</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Booking IDs</th>
                                    <!-- <th width="10%">Job Status</th> -->
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if (!empty($job_sheets)) {
                                    foreach ($job_sheets as $index => $js) {
                                        ?>
                                        <tr>
                                            <td><?= ++$index ?></td>
                                            <td><?= DateTime::createFromFormat("Y-m-d H:i:s", $js->date_created)->format('d M, Y') ?></td>
                                            <td><?= $js->from_date ?></td>
                                            <td><?= $js->to_date ?></td>
                                            <td><?= $js->booking_ids ?></td>
                                            <!-- <td>
                                                <?php
                                                if ($js->job_status == 'assigned') {
                                                    $label_js = 'label-default';
                                                } else if ($js->job_status == 'accepted') {
                                                    $label_js = 'label-primary';
                                                } else if ($js->job_status == 'rejected') {
                                                    $label_js = 'label-danger';
                                                }
                                                ?>
                                                <span class="label <?= $label_js ?>">
                                                    <?= $js->job_status ?>
                                                </span>

                                            </td> -->
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Actions <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="<?= base_url('admin/driver/preview_job_sheet_email/' . $js->id) ?>" target="_blank"><i class="fa fa-search-plus"></i> Preview</a></li>
                                                        <li>
                                                            <a href="<?= base_url('admin/driver/delete_job_sheet/' . $js->id) ?>" onclick="return confirm('Are you sure?');">
                                                                <i class="fa fa-trash"></i> Delete
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>

                        </table>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-reminder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Reminder</h4>
            </div>

            <form method="post">
                <input type="hidden" name="driver_id" value="<?= $driver->driver_id ?>">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>From Date</label>
                                <input type="text" class="form-control datepicker" name="from_date" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>To Date</label>
                                <input type="text" class="form-control datepicker" name="to_date" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Preview Reminder Email</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-preview-reminder">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" style="font-family: Times New Roman !important;">

            </div>
            <div class="modal-footer">
                <form action="<?= site_url('admin/driver/send_reminder') ?>" method="post">
                    
                    <input type="hidden" name="driver_id">
                    <input type="hidden" name="from_date">
                    <input type="hidden" name="to_date">
                    <input type="hidden" name="html">
                    
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Send Email</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(document).ready(function () {
        
        var modal_reminder = $('#modal-reminder');
        var modal_preview_reminder = $('#modal-preview-reminder');
        
        $('#btn-reminder').click(function () {
            modal_reminder.modal();
        });
        
        modal_reminder.find('form').submit(function(){
            
            $.ajax({
                url: SITE_URL + "admin/driver/ajax_preview_reminder",
                dataType: 'json',
                method: 'post',
                data: {
                    from_date: $(this).find('[name=from_date]').val(),
                    to_date: $(this).find('[name=to_date]').val(),
                    driver_id: $(this).find('[name=driver_id]').val()
                },
                success: function(response){
                    
                    modal_preview_reminder.find('.modal-body').html(response.data.html);
                    modal_preview_reminder.find('[name=html]').val(response.data.html);
                    
                    modal_preview_reminder.find('[name=driver_id]').val(response.data.request_data.driver_id);
                    modal_preview_reminder.find('[name=from_date]').val(response.data.request_data.from_date);
                    modal_preview_reminder.find('[name=to_date]').val(response.data.request_data.to_date);
                    
                    modal_preview_reminder.modal();
                    
                },
                error: function(){
                    alert('Server error occured.');
                }
            });
            
            return false;
            
        });
        
    });
</script>