<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header" style="padding: 10px 0;">
                <h3 class="box-title">Send Driver Statement</h3>
            </div><!-- /.box-header -->
            <?php flash() ?>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-send-job-sheet" method="post">
                            <input type="hidden" name="driver_id" value="<?= $driver->id ?>">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="control-label">Jobs From</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control all-datepicker" name="from_date" required="">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Jobs To</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control all-datepicker" name="to_date">
                                                    </div>  
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Subject</label>
                                            <div class="input-group col-md-12">
                                                <span class="input-group-addon"><i class="fa fa-pencil"></i> </span>
                                                <input class="form-control" name="mail_subject" value="[RefID:<?= $statement_ref_id ?>] Driver Work Statement (<?= date('d/m/Y') ?>) - <?= SITE_NAME ?>" required="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Email Message</label>
                                            <textarea class="form-control" name="mail_message" id="message" required="">
                                                
                                                <p class="white"> Your statement from {start_date} to {end_date}. The amount payable is (<?= CURRENCY ?> {total_driver_payable})</p>
                                            </textarea>
                                            <script type="text/javascript">
                                                var editor = CKEDITOR.replace('message',
                                                        {
                                                            customConfig: '<?php echo base_url('assets/admin-assets/js/plugins/ckeditor/my_config.js') ?>'
                                                        });
                                                CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin-assets/js/plugins/ckfinder/') ?>');
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-search"> </i> Preview Statement Email</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-send-job-sheet">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" style="font-family: Times New Roman !important;">

            </div>
            <div class="modal-footer">
                <form action="" id="form-invoice" name="form_invisible" method="post">
                    <input type="hidden" name="i_statement_ref_id" value="<?= $statement_ref_id ?>">
                    <input type="hidden" name="i_payable_amount_due">
                    <input type="hidden" name="i_driver_id" value="<?= $driver->id ?>">
                    <input type="hidden" name="i_from_date">
                    <input type="hidden" name="i_to_date">
                    <input type="hidden" name="i_mail_subject">
                    <input type="hidden" name="i_mail_template">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Email Driver Statement</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    $(function () {
        $('#form-send-job-sheet').submit(function () {
            if ($(this).valid()) {

                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();

                $.post(
                        '<?= base_url('admin/driver/ajax_preview_statement') ?>',
                        {'formdata': $(this).serialize()},
                function (response) {

                    response = jQuery.parseJSON(response);

                    if (response.status) {
                        $('[name=i_payable_amount_due]').val(response.data.payable_amount_due);
                        $('[name=i_from_date]').val(response.data.from_date);
                        $('[name=i_to_date]').val(response.data.to_date);
                        $('[name=i_mail_subject]').val(response.data.mail_subject);
                        $('[name=i_mail_template]').val(response.data.mail_template);

                        $('#modal-send-job-sheet .modal-body').html(response.data.mail_template);
                        $('#modal-send-job-sheet .modal-header h4').html("<strong>Mail Subject: </strong>" + response.data.mail_subject);
                        $('#modal-send-job-sheet').modal();
                    } else {
                        alert(response.msg);
                    }

                }
                );
                return false;

            } else {
                alert('Please make sure all the required fields are filled properly.');
            }
        });
    });
</script>   