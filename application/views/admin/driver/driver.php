<div class="row">
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-aqua">
      <span class="info-box-icon"><i class="fa fa-user-secret"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Drivers Today</span>
        <span class="info-box-number"><?= $count['today'] ?></span>
        <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
        </div>
        <span class="progress-description">
          100% <small class="pull-right"><?= date('d/m/Y') ?></small>
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-green">
      <span class="info-box-icon"><i class="fa fa-user-secret"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Drivers Monthly</span>
        <span class="info-box-number"><?= $count['month'] ?></span>

        <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
        </div>
        <span class="progress-description">
          100% <small class="pull-right"><?= date('F Y') ?></small>
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-blue">
      <span class="info-box-icon"><i class="fa fa-user-secret"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Drivers Annual</span>
        <span class="info-box-number"><?= $count['annual'] ?></span>

        <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
        </div>
        <span class="progress-description">
          100% <small class="pull-right"><?= date('Y') ?></small>
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
</div>
<div class="row">
  <div class="col-md-12">
    <?php flash() ?>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Driver List
        </h3>
        <?php if($this->session->userdata['account_type'] != 3) :?>
        <span class="pull-right add-new">
          <a href="<?= site_url('admin/driver/add') ?>" class="btn btn-success">Add New</a>
        </span>
        <?php endif; ?>

      </div><!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered" id="dt-table">
          <thead class="thead-dark">
            <tr>
              <th width="1%">#</th>
              <th>Image</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone No.</th>
              <!-- <th>Vehicle</th> -->
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (!empty($drivers)) :
              foreach ($drivers as $index => $driver) :
                // $vehicle = $this->vehicle_model->get(['id' => $driver->vehicle_id]);
            ?>
                <tr>
                  <td class="lboarder-<?= $driver->status ? 'success' : 'danger' ?>">
                    <?= ++$index ?>
                  </td>
                  <td>
                    <img src="<?= base_url('uploads/driver/' . ($driver->image ?: 'default.png')) ?>" class="circular-profile profile-img-zoom">
                  </td>
                  <td><?php echo $driver->name ?></td>
                  <td><?php echo $driver->email ?></td>
                  <td><?php echo $driver->phone_cc . $driver->mobile ?></td>
                  <!-- <td>
                    <?= $vehicle ? '<i class="fa fa-car"></i> ' . $vehicle->plate_number : '-' ?>
                  </td> -->
                  <td>
                    <?php echo $driver->status ? '<span class="badge bg-green">Enable</span>' : '<span class="badge bg-red">Disabled</span>' ?></td>
                  <td>
                    <div class="dropdown">
                      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                        <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                      <li> <a href="<?= site_url('admin/driver/details/' . $driver->id) ?>" title="View Details">
                            <i class="fa fa-eye"></i> View details
                          </a>
                        </li>
                        <li> <a href="<?= site_url('admin/booking?action=search&driver_id=' . $driver->id) ?>" target="_blank" title="View Job History">
                            <i class="fa fa-undo"></i> View job history
                          </a>
                        </li>
                        <li> <a href="<?= site_url('admin/booking?action=search&status=job_assigned&driver_id=' . $driver->id) ?>" target="_blank" title=" View assigned Job">
                            <i class="fa fa-book"></i> View assigned job
                          </a>
                        </li>
                        <!-- <li> <a href="<?= site_url('admin/driver/statements/' . $driver->id) ?>" title="Driver Statement">
                            <i class="fa fa-tasks"></i> Statement
                          </a>
                        </li>
                        <li> <a href="<?= site_url('admin/driver/job_sheets/' . $driver->id) ?>" title="Job Sheet">
                            <i class="fa fa-file-o"></i> Job Sheet
                          </a>
                        </li> -->
                        <?php if($this->session->userdata['account_type'] != 3) :?>
                          <li> <a href="<?= site_url('admin/driver/delete/' . $driver->id) ?>" title="Delete" onclick="return confirm('Are you sure?')">
                              <i class="fa fa-trash-o"></i> Delete
                            </a>
                          </li>
                        <?php endif; ?>
                      </ul>
                    </div>
                  </td>
                </tr>
            <?php endforeach;
            endif;
            ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
</div><!-- /.row -->
<!--add-vehicle modal-->
<div class="modal fade" id="add-vehicle-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title text-center" id="upload-dialog-title">Add Vehicle</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-striped">
          <?php
          if (!empty($vehicles)) :
            foreach ($vehicles as $vehicle) :
          ?>
              <tr>
                <td>
                  <i class="fa fa-car"></i> <?= $vehicle->plate_number ?><br>
                  <i class="fa fa-phone"></i> <?= $vehicle->mobile ?><br>
                </td>
                <td> <i class="fa fa-car"></i> <?= $vehicle->make . ' ' . $vehicle->model ?> ( <?= ucfirst($vehicle->color) ?>)<br></td>
                <td>
                  <i class="fa fa-users"> <?= $vehicle->passenger ?></i>
                  <i class="fa fa-suitcase"> <?= $vehicle->luggage ?></i>
                  <i class="fa fa-soccer-ball-o"> <?= $vehicle->sports_luggage ?></i>
                </td>
                <td>
                  <form action="<?= site_url('admin/driver/assignVehicle') ?>" method="post">
                    <input type="hidden" name="driver_id">
                    <input type="hidden" name="vehicle_id" value="<?= $vehicle->id ?>">
                    <button type="submit" class="btn btn-success">Add This</button>
                  </form>
                </td>
              </tr>
          <?php
            endforeach;
          endif;
          ?>
        </table>
      </div>
    </div>
  </div>
</div>
<!--add-vehicle modal end-->
<script>
  function addVehicle(driver_id) {
    $('[name=driver_id]').val(driver_id);
    $('#add-vehicle-dialog').modal('show');
  }
  $(function() {
    $('#dt-table').dataTable();
  });
</script>