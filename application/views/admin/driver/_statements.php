<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title btn-block">
                    Statements - <?= $driver->name ?>
                    <div class="pull-right">
                        <a href="<?= base_url('admin/driver/send_statement/' . $driver->id) ?>" class="btn btn-primary">Send New Statement</a>
                    </div>
                </h3>
            </div><!-- /.box-header -->

            <div class="box-body">

                <table class="table table-bordered border-color-fix dt-tables">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Statement Ref ID</th>
                            <th>Generated Date</th>
                            <th>Statement From/To Dates</th>
                            <th>Payable Amount Due</th>
                            <th width="10%">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if ($statements) :
                            foreach ($statements as $index => $s) :
                                ?>
                                <tr class="<?= $s->is_cancelled ? 'bg-danger' : '' ?>">
                                    <td><?= ++$index ?></td>
                                    <td><?= $s->statement_ref_id ?></td>
                                    <td><?= date('d/m/Y', strtotime($s->date_created)) ?></td>
                                    <td><?= $s->from_date . ' - ' . $s->to_date ?></td>
                                    <td><?= CURRENCY . $s->payable_amount_due ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?= base_url('admin/driver/preview_statement/' . $s->statement_ref_id) ?>" target="_blank"><i class="fa fa-search"></i> Preview</a></li>
                                                <?php if (!$s->is_cancelled): ?>
                                                    <li><a href="#!" onclick="cancel_statement('<?= $s->statement_ref_id ?>')"><i class="fa fa-times"></i> Cancel</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>

                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-cancel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cancel Statement</h4>
            </div>
            <form action="<?= base_url() ?>admin/driver/cancel_statement" method="post">
                <input type="hidden" name="statement_ref_id">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Cancel Message</label>
                        <textarea class="form-control" name="message" required=""></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Confirm Cancel</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    function cancel_statement(statement_ref_id){
        var modal = $('#modal-cancel');
        modal.find('[name=statement_ref_id]').val(statement_ref_id);
        modal.modal();
    }
</script>