<div class="row">
    <div class="col-md-12">
        <form method="post" enctype="multipart/form-data">
            <div class="box">
            <div class="box-header">
                <h3 class="box-title">Add Driver</h3>
            </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Name:</label>
                                <input type="text" name="name"  class="form-control ">
                            </div>
                            <div class="form-group">
                                <label>Email:</label>
                                <input type="text" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Phone CC:</label>
                                <input type="text" name="phone_cc" class="form-control ">
                            </div>
                            <div class="form-group">
                                <label>Mobile:</label>
                                <input type="text" name="mobile" class="form-control ">
                            </div>
                            <div class="form-group">
                                <label>Address:</label>
                                <input type="text" name="address" class="form-control ">
                            </div>
                            <div class="form-group">
                                <label>Status:</label><br>
                                <label>
                                    <input type="radio" name="status" value="1" checked> Enable
                                </label>
                                <label>
                                    <input type="radio" name="status" value="0"> Disabled
                                </label>
                            </div>
                            <button type="submit" class="btn btn-flat btn-info"> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>