<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Area List</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo site_url('admin/service-area/add_update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" id="dt-pages">
                    <thead>
                        <tr>
                            <th style="width:3%"></th>
                            <th>Name</th>
                            <th>Country</th>
                            <th width="12%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($areas)):
                            foreach ($areas as $index=>$area):
                                ?>
                                <tr>
                                    <td><?php echo ++$index; ?></td>
                                    <td><?php echo $area->custom_area_name ?></td>
                                    <td>United Kingdom</td>
                                    <td>
                                        <a class="btn btn-sm btn-info" href="<?php echo site_url('admin/service-area/add-update/' . $area->id) ?>">Edit</a>
                                        <a class="btn btn-sm btn-danger" href="<?php echo site_url('admin/service-area/delete/' . $area->id) ?>" onclick="return confirm('Are you sure?')">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(function(){
        $('#dt-pages').dataTable();
    });
</script>