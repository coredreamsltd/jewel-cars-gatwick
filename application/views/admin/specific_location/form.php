<style>
    div#map_geolocation img {
        max-width: none;
    }

    #map-canvas {
        height: 525px;
    }

    #map-canvas img {
        max-width: none !important;
    }

    /* 
    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 50%;
        margin-top: 11px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    } */

    .loader-map p {
        font-size: 24px;
        background: #f5f5f5c2;
    }

    .loader-map {
        position: absolute;
        top: 50%;
        left: 48%;
        margin-left: -50px;
        width: 480px;
        height: 133px;
        z-index: 3;
        text-align: center;
    }

    .result {
        font-size: 0.8em;
        margin: 5px;
        margin-top: 0px;
        padding: 4px 8px;
        border-radius: 2px;
        background: #F0F7FF;
        border: 2px solid #D7E7FF;
        cursor: pointer;
        min-height: 5em;
    }

    .result.active {
        background-color: #D9E7F7;
        border-color: #9DB9E4;
    }

    .result img {
        float: right;
    }
</style>
<?php flash() ?>
<div class="box">
    <div class="loader-map" style="display: none;">
        <p><i class="fa fa-cog fa-spin fa-2x"></i> <br>Loading.....</p>
    </div>
    <form action="" method="post">
        <div class="box-header">
            <h3 class="box-title btn-block">
                Specific Location (Airport) | <?php echo (!$isEdit) ? 'Add' : 'Update' ?>
            </h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <input type="hidden" name="lat" value="<?= $isEdit ? $area->lat : '' ?>">
            <input type="hidden" name="lng" value="<?= $isEdit ? $area->lng : '' ?>">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Search location</label>
                        <input id="pac-input" class="form-control" type="text" placeholder="Search Location">
                    </div>
                    <div class="form-group">
                        <label>Location name</label>
                        <input class="form-control" name="title" value="<?= $isEdit ? $area->title : '' ?>" type="text" placeholder="Search Location" required>
                    </div>
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" name="type" required>
                            <option name="airport" <?= $isEdit && $area->type=='airport' ? 'selected' : '' ?>>Airport</option>
                            <!-- <option name="seaport" <?= $isEdit && $area->type=='seaport' ? 'selected' : '' ?>>Seaport</option>
                            <option name="station" <?= $isEdit && $area->type=='station' ? 'selected' : '' ?>>Station</option> -->
                        </select>
                    </div>
                    <div class="form-group">
					<label>Meeting instructions</label>
                        <textarea class="form-control" name="instructions" rows="7"><?= $isEdit ? $area->instructions : '' ?></textarea>
					</div>
                 
                    <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Save</button>
                </div>

                <div class="col-md-3">
					   <div class="form-group">
                        <label>Choose Zone Location(s)</label>
                        <div class="search-result"></div>
                    </div>
				</div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="hidden" name="coordinates" value="<?= $isEdit ? $area->coordinates : '' ?>">
                        <label>Draw zone coverage area</label>
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>

        </div><!-- /.box-body -->
    </form>
</div><!-- /.box -->
<script src="<?= base_url('assets/Wicket/wicket.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/Wicket/wicket-gmap3.js') ?>" type="text/javascript"></script>
<script>
    var map;
    var contents;
    var polygon = {};
    var coordinates = <?= $isEdit ? "'" . $area->coordinates . "'" : 'null' ?>;

    $(window).load(function() {
        initMap();
        init_draw_tools(map);
        draw_polygon(map, coordinates);
    });

    $(function() {
        $('.search-result').on('click', '.result', function() {
            $('.result').removeClass('active');
            $(this).addClass('active');
            polygon.setMap(null);
            var content = contents[$(this).data('position')];

            draw_polygon(map, content.coordinate);
            var ne = new google.maps.LatLng(content.boundingbox[0], content.boundingbox[2]);
            var sw = new google.maps.LatLng(content.boundingbox[1], content.boundingbox[3]);
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(sw);
            bounds.extend(ne);
            map.fitBounds(bounds);
        });
    });

    function initMap() {
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: {
                lat: parseFloat(<?= $isEdit ? "'" . $area->lat . "'" : 51.5074 ?>),
                lng: parseFloat(<?= $isEdit ? "'" . $area->lng . "'" : 0.1278 ?>)
            },
            zoom: <?= $isEdit ?10:8?>
        });
        search_box(map);
    }

    function init_draw_tools(map) {
        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT,
                drawingModes: ['polygon']
            }
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {

            var wkt = new Wkt.Wkt();
            wkt.fromObject(polygon);
            $('[name=coordinates]').val(wkt.write());

        });
    }

    function draw_polygon(map, coordinates) {
        if (coordinates === null) {
            return;
        }
        var wkt = new Wkt.Wkt();
        wkt.read(coordinates);
        $('[name=coordinates]').val(coordinates);
        // Construct the polygon.
        polygon = wkt.toObject({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        });
        polygon.setMap(map);
    }

    function search_box(map) {
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            console.log(places);
            processMultiPolygonLine($('#pac-input').val(), map);
            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();

            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var location = place.geometry.location;
                var lat = location.lat();
                var lng = location.lng();
                $('[name=lat]').val(lat);
                $('[name=lng]').val(lng);
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });

    }

    function processMultiPolygonLine(location, map) {
        location = location.split(',')[0];
        $('.search-result').html('');
        $('.loader-map').show();
        var url = "https://nominatim.openstreetmap.org/search.php?polygon_geojson=1&format=json&q=" + location;
        $.ajax({
            url: url,
            dataType: 'json',
            success: function(response) {
                // polygon = {};
                if (response) {
                    contents = [];
                    $.each(response, function(i, v) {
                        if ((v.osm_type == 'relation' || v.osm_type == 'way') && (v.geojson.type == 'Polygon' || v.geojson.type == 'MultiPolygon') &&
                            (1 || v.type == 'administrative' || v.type == 'aerodrome' || v.type == 'state' || v.type == 'country' || v.type == 'city')) {
                            var lng_lat_string = '';
                            var polygon_points = (typeof response[i].geojson.coordinates[0][0] != 'undefined' && response[i].geojson.coordinates[0][0].length > 2) ?
                                response[i].geojson.coordinates[0][0] : response[i].geojson.coordinates[0];
                            $.each(polygon_points, function(i2, v2) {
                                lng_lat_string += v2[0] + ' ' + v2[1] + ',';
                            });
                            var content = {
                                'display_name': v.display_name,
                                'type': v.type,
                                'icon': v.icon,
                                'boundingbox': v.boundingbox,
                                'coordinate': "POLYGON((" + lng_lat_string.slice(0, -1) + "))",
                            };
                            contents.push(content);
                        }
                    });
                }
                if (contents.length > 0) {
                    var html = '';
                    if (isEmpty(polygon) == false) {
                        polygon.setMap(null);
                    }

                    $.each(contents, function(key, content) {

                        if (key == 0) {
                            draw_polygon(map, content.coordinate);
                            var ne = new google.maps.LatLng(content.boundingbox[0], content.boundingbox[2]);
                            var sw = new google.maps.LatLng(content.boundingbox[1], content.boundingbox[3]);
                            var bounds = new google.maps.LatLngBounds();
                            bounds.extend(sw);
                            bounds.extend(ne);
                            map.fitBounds(bounds);
                        }

                        html += '<div class="result ' + (key == 0 ? 'active' : '') + '" data-position="' + key + '">' +
                            '<span class="name">' + content.display_name + '</span>' +
                            '<span class="type">(' + content.type + ')</span>';
                        if (content.icon) {
                            html += '<img alt="icon" src="' + content.icon + '">';
                        }
                        html += ' </div>';
                    });
                    $('.search-result').html(html);
                }
                $('.loader-map').hide();

            },
            error: function() {
                $('.loader-map').hide();
                alert('Something went worng while fetching data. Please try again.');
            }
        });
    }

    function isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
</script>
