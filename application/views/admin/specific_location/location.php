<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Airport List</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo site_url('admin/specific-location/add-update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" id="dt-pages">
                    <thead>
                        <tr>
                            <th style="width:3%"></th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Instructions</th>
                            <th width="12%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($areas)):
                            foreach ($areas as $index=>$area):
                                ?>
                                <tr>
                                    <td><?= ++$index; ?></td>
                                    <td><?= $area->title ?></td>
                                    <td><?= strtoupper($area->type) ?></td>
                                    <td><?=$area->instructions ?></td>
                                
                                    <td>
                                        <a class="btn btn-sm btn-info" href="<?= site_url('admin/specific-location/add-update/' . $area->id) ?>">Edit</a>
                                        <a class="btn btn-sm btn-danger" href="<?= site_url('admin/specific-location/delete/' . $area->id) ?>" onclick="return confirm('Are you sure?')">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(function(){
        $('#dt-pages').dataTable();
    });
</script>
