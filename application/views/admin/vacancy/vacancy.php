<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Vacancy Manager</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" id="dt-pages">
                    <thead>
                    <tr>
                        <th style="width:3%"></th>
                        <th>Name</th>
                        <th>Contact Information</th>
                        <th>Other Information</th>
                        <th width="12%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($vacancies)):
                        foreach ($vacancies as $vacancy):
                            ?>
                            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $vacancy->fname . " " . $vacancy->lname ?></td>
                                <td>
                                    <strong>Email:</strong> <?php echo $vacancy->email ?><br>
                                    <strong>Phone:</strong> <?php echo $vacancy->phone ?><br>
                                    <strong>Postcode:</strong> <?php echo $vacancy->postcode ?>
                                </td>
                                <td>
                                    <strong>Vehicle Made:</strong> <?php echo $vacancy->vehicle_made ?><br>
                                    <strong>Vehicle Model:</strong> <?php echo $vacancy->vehicle_model ?><br>
                                    <strong>PCO License No.:</strong> <?php echo $vacancy->poc_license_no ?><br>
                                    <strong>MOT File:</strong> <a href="<?=base_url('uploads/vacancy/'.$vacancy->mot_file)?>" target="_blank"> Click Here</a><br>
                                    <strong>Insurance File:</strong> <a href="<?=base_url('uploads/vacancy/'.$vacancy->insurance_file)?>" target="_blank"> Click Here</a>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-danger"
                                       href="<?php echo site_url('admin/vacancy/delete/' . $vacancy->id) ?>"
                                       onclick="return confirm('Are you sure?')">Delete</a>
                                </td>
                            </tr>
                        <?php endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(function () {
        $('#dt-pages').dataTable();
    });
</script>