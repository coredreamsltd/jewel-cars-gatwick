<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Testimonial | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>

            </div><!-- /.box-header -->
            <div class="box-body">
                <form action method="post" class="forms" enctype="multipart/form-data">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>
                                <label>Post by<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Enter Name" class="form-control"
                                       name="post_by"
                                       value="<?php echo (!$isNew) ? $testimonial->post_by : ''; ?>" required=""/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Rating<span class="text-danger">*</span></label>
                                        <select class="form-control" name="rating" required>
                                            <option class="selected" value="">Select Rating</option>
                                            <option <?= (!$isNew) && $testimonial->rating == 1 ? 'selected' : '' ?>>1</option>
                                            <option <?= (!$isNew) && $testimonial->rating == 2 ? 'selected' : '' ?>>2</option>
                                            <option <?= (!$isNew) && $testimonial->rating == 3 ? 'selected' : '' ?>>3</option>
                                            <option <?= (!$isNew) && $testimonial->rating == 4 ? 'selected' : '' ?>>4</option>
                                            <option <?= (!$isNew) && $testimonial->rating == 5 ? 'selected' : '' ?>>5</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Status<span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" required>
                                            <option value="1" <?= (!$isNew) && $testimonial->status == 1 ? 'selected' : '' ?>>SHOW
                                            </option>
                                            <option value="0" <?= (!$isNew) && $testimonial->status == 0 ? 'selected' : '' ?>>HIDE
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Post Date<span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter Post Date" class="form-control all-datepicker"
                                               name="post_date" value="<?php echo (!$isNew && $testimonial->post_date) ? (date('d/m/Y',strtotime($testimonial->post_date))) : date('d/m/Y'); ?>">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Review Title<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Enter Title" class="form-control"
                                       name="title"
                                       value="<?php echo (!$isNew) ? $testimonial->title : ''; ?>" required=""/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Content<span class="text-danger">*</span></label>
                                <textarea rows="4" type="text" placeholder="Enter content" class="form-control"
                                          name="content"
                                          required><?php echo (!$isNew) ? $testimonial->content : ''; ?></textarea>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('content',
                                        {
                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                        });
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td><input id="has-ckeditor" class="btn btn-primary" type="submit" value="Save"></td>
                        </tr>
                        </tbody>
                    </table>
                    <!--  </form> -->
                </form>
            </div>

        </div><!-- /.box -->
    </div>