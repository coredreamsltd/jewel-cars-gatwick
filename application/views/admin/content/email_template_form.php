<?php flash() ?>
<div class="row">
    <div class="col-md-12">
        <form method="post" class="forms" enctype="multipart/form-data">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Email Templates</h3>
                    <div class="pull-right">
                        <button class="btn btn-flat btn-primary" type="submit"> Save</button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row login-box-body">
                        <?php if (!empty($templates)) : ?>
                            <?php foreach ($templates as $key => $template) : if($key =='booking_received_top'){continue;}?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?= ucwords(str_replace('_', ' ', $key)) ?><span class="text-danger">*</span></label>
                                        <textarea type="text" class="form-control" name="<?= $key ?>"><?= $template ?></textarea>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('<?= $key ?>', {
                                        customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                    });
                                </script>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
        </form>
    </div><!-- /.box -->
</div>
