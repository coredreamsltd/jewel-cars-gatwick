<?php
$isNew = true;
if (segment(4) != '') {
    $filename = $page->filename;
    $isNew = false;
    $name = $page->name;
    $excerpt = $page->excerpt;
    $desc = $page->desc;
    $desc_short = $page->desc_short;
    $desc_long = $page->desc_long;
    $extra_1 = $page->extra_1;
    $extra_2 = $page->extra_2;
    $extra_3 = $page->extra_3;
    $extra_4 = $page->extra_4;
    $name_rus = $page->name_rus;
    $desc_rus = $page->desc_rus;
    $desc_short_rus = $page->desc_short_rus;
    $is_active = $page->is_active;
    $template = $page->template;
}
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form method="post" class="forms" enctype="multipart/form-data">
                <div class="box-header">
                    <h3 class="box-title">Page Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-lg btn-flat btn-primary"> Save</button>
                    </div>
                </div><!-- /.box-header -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">English</a>
                    </li>
                    <li role="presentation">
                        <!--                        <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Russian</a>-->
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content ">
                    <div role="tabpanel" class="tab-pane active" id="tab1">
                        <div class="row english">
                            <div class="col-sm-12">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Page Name <span class="text-danger">*</span></label>
                                                    <input type="text" placeholder="Enter page name"
                                                           class="form-control required" name="name"
                                                           value="<?php echo !$isNew ? $name : '' ?>" required>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Page Template <span class="text-danger">*</span></label>
                                                    <select class="form-control" name="template" required>
                                                        <option value="normal_page" <?= !$isNew && $template == 'normal_page' ? 'selected' : ''; ?>>
                                                            Normal Page
                                                        </option>
                                                        <option value="business_page" <?= !$isNew && $template == 'business_page' ? 'selected' : ''; ?>>
                                                            Business Page
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Page Image <span class="text-danger">*</span></label>
                                                    <input type="file" name="page"
                                                           class="<?php echo ($isNew) ? 'required' : '' ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                <?php if (!$isNew && $filename): ?>
                                                        <img src="<?php echo base_url('uploads/page/' . $filename) ?>" class="img-responsive">
                                                    <?php endif; ?>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="type">
                                        <td>
                                            <label>Select Type <span class="text-danger">*</span></label><br>
                                            <label>
                                                <input type="radio" name="type"
                                                       value="cities" <?= !$isNew && $page->type == 'cities' ? 'checked' : '' ?>
                                                       required> CITIES
                                            </label>
                                            <label>
                                                <input type="radio" name="type"
                                                       value="airports" <?= !$isNew && $page->type == 'airports' ? 'checked' : '' ?>
                                                       required> AIRPORTS
                                            </label>
                                            <label>
                                                <input type="radio" name="type"
                                                       value="cruise_ports" <?= !$isNew && $page->type == 'cruise_ports' ? 'checked' : '' ?>
                                                       required> CRUISE PORTS
                                            </label>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <label>Excerpt <span class="text-danger">*</span></label>
                                <textarea name="excerpt" placeholder="Enter page excerpt"
                                          class="form-control"><?php echo !$isNew ? $excerpt : '' ?></textarea>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('excerpt',
                                        {
                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                        });
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                </script>
                            </div>
                            <div class="col-sm-6">
                                <label>Page Description <span class="text-danger">*</span></label>
                                <textarea name="desc" placeholder="Enter page description"
                                          class="form-control required"
                                          id="desc"><?php echo !$isNew ? $desc : '' ?></textarea>
                                <label for="desc" class="error" style="display:none;">This field is
                                    required</label>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('desc',
                                        {
                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                        });
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                </script>
                            </div>
                            <div class="col-sm-6">
                                <label>Page Short Description <span class="text-danger">*</span></label>
                                <textarea name="desc_short" placeholder="Enter page short description"
                                          class="form-control required"
                                          id="desc_short"><?php echo !$isNew ? $desc_short : '' ?></textarea>
                                <label for="desc_short" class="error" style="display:none;">This field is
                                    required</label>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('desc_short',
                                        {
                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                        });
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                </script>
                            </div>
                            <div class="col-sm-6">
                                <label>Page Long Description <span class="text-danger">*</span></label>
                                <textarea name="desc_long" placeholder="Enter page long description"
                                          class="form-control required"
                                          id="desc_short"><?php echo !$isNew ? $desc_long : '' ?></textarea>
                                <label for="desc_short" class="error" style="display:none;">This field is
                                    required</label>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('desc_long',
                                        {
                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                        });
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                </script>
                            </div>
                            <div class="col-sm-6">
                                <label>Extra Description 1<span class="text-danger">*</span></label>
                                <textarea name="extra_1" placeholder="Enter description" class="form-control required"><?php echo !$isNew ? $extra_1 : '' ?></textarea>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('extra_1',
                                        {
                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                        });
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                </script>
                            </div>
                            <div class="col-sm-6">
                                <label>Extra Description 2<span class="text-danger">*</span></label>
                                <textarea name="extra_2" placeholder="Enter description" class="form-control required"><?php echo !$isNew ? $extra_2 : '' ?></textarea>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('extra_2',
                                        {
                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                        });
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(document).ready(function () {
        var template = $('[name=template]').val();
        type(template);

        $('select[name="template"]').change(function () {
            var template = $(this).val();
            type(template);

        });
    });

    function type(template) {
        if (template === 'service_page') {
            $('.type').find('input[type=radio]').removeAttr('disabled');
            $('.type').show();
        } else {
            $('.type').find('input[type=radio]').attr('disabled', 'disabled');
            $('.type').hide();
        }
    }
</script>

<!--<script>-->
<!--    $(document).ready(function () {-->
<!--//        $('.russian').hide();-->
<!--        $('.language_1').click(function () {-->
<!--            $('.english').show();-->
<!--            $('.russian').hide();-->
<!--        });-->
<!--        $('.language_2').click(function () {-->
<!--            $('.english').hide();-->
<!--            $('.russian').show();-->
<!--        });-->
<!---->
<!--    });-->
<!--</script>-->