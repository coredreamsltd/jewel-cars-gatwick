<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<style>
    .no-img img {
        display: none !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Testimonial Manager</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo site_url('admin/content/add_update_testimonial') ?>" class="btn btn-success">Add
                        New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-striped" id="dt-pages" style="table-layout: fixed;   width: 100%;">
                    <thead>
                    <tr>
                        <th style="width:3px"></th>
                        <th style="width:15%">Post By</th>
                        <th style="width:45px">Rating</th>
                        <th style="width:25%">Title</th>
                        <th>Content</th>
                        <th>IP Address</th>
                        <th style="width: 40px">Status</th>
                        <th width="12%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($testimonials)):
                        foreach ($testimonials as $testimonial):
                            ?>
                            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $testimonial->post_by ?></td>
                                <td><?php echo $testimonial->rating ?> </td>
                                <td><?php echo $testimonial->title ?> </td>
                                <td class="no-img"
                                    style="overflow: hidden;  white-space: nowrap;   text-overflow: ellipsis; word-break: break-all; min-width: 382px !important;"><?php echo $testimonial->content ?></td>
                                <td>
                                    <?php $user_agent_data = json_decode($testimonial->user_agent_data);?>
                                    <?= !empty($user_agent_data->{'IP Address'}) ? $user_agent_data->{'IP Address'} : '-' ?><br>
                                    <?= !empty($user_agent_data->{'Browser'}) ? $user_agent_data->{'Browser'} : '' ?>
                                    <?= !empty($user_agent_data->{'OS'}) ? $user_agent_data->{'OS'} .'<br>': '' ?>
                                    <?= !empty($user_agent_data->{'Mobile Device'}) ? $user_agent_data->{'Mobile Device'} : '' ?>

                                </td>
                                <td><?= $testimonial->status ? '<label class="label label-success">SHOW</label>' : '<label class="label label-danger">HIDDEN</label>' ?></td>
                                <td>
                                    <a class="btn btn-sm btn-info"
                                       href="<?php echo site_url('admin/content/add_update_testimonial/' . $testimonial->id) ?>">Edit</a>
                                    <a class="btn btn-sm btn-danger"
                                       href="<?php echo site_url('admin/content/delete_testimonial/' . $testimonial->id) ?>"
                                       onclick="return confirm('Are you sure?')">Delete</a>
                                </td>
                            </tr>
                        <?php endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script>
    $(function () {
        $('#dt-pages').dataTable();
    });
</script>