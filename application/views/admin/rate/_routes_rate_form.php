<?php
$isNew = true;
if (segment(4) != '') {
    $location = $location[0];
    $isNew = false;
}
?>
<style>
    #locations-table {
        position: relative;
    }

    .loader {
        position: absolute;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.75);
        color: #fff;
        z-index: 99;
        text-align: center;
        padding-top: 10%;
    }
</style>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Routes Rate Manager</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php flash() ?>
                <form action="" method="post" class="forms">
                    <div class="form-horizontal">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>
                                        <label>From <span class="text-danger">*</span></label>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <select class="form-control" id="from-location-type" onchange="list_from_locations(this.value)">
                                                            <option value="">- Select From Location Type -</option>
                                                            <?php
                                                            if ($location_types) :
                                                                foreach ($location_types as $type) :
                                                            ?>
                                                                    <option value="<?= $type['id'] ?>"><?= $type['type'] ?></option>
                                                            <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <select name="from_location_id" class="form-control required">
                                                            <option value="">- Select From Location -</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <label>To <span class="text-danger">*</span></label>
                                        <select name="location_type_id" class="form-control required">
                                            <option value="">- Select Location Type -</option>
                                            <?php foreach ($location_types as $lt) : ?>
                                                <option value="<?= $lt['id'] ?>" <?php echo (!$isNew && $type_id == $lt['id']) ? 'selected' : '' ?>><?= $lt['type'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td style="vertical-align: bottom">
                                        <button id="has-ckeditor" class="btn btn-primary btn-block" type="button" onclick="manage_rates()"><i class="fa fa-cogs"></i>&nbsp; Manage Rates</button>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </form>
            </div><!-- /.box-body -->

            <div class="box-body" id="locations-table">

                <div class="row" style="display:none">
                    <div class="col-md-4 pull-left">
                        <h4>Bulk Rate Increment/Decrement</h4>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <select name="rate_change_method">
                                    <option value="increase">Increase By</option>
                                    <option value="decrease">Decrease By</option>
                                </select>
                            </span>
                            <span class="input-group-addon">
                                <label class="radio-inline">
                                    <input type="radio" name="increment_type" value="by_percent" checked> %
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="increment_type" value="by_amount"> <?= CURRENCY ?>
                                </label>
                            </span>
                            <input type="text" class="form-control" name="increment_rate" value="0">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick="increment_rate()">Apply</button>
                            </span>
                        </div><!-- /input-group -->
                    </div>
                </div>

                <form method="post" action="add_update_routes_rate">

                    <input type="hidden" name="from_location_id">
                    <input type="hidden" name="location_type_id">
                    <div id="table-ajax"></div>

                </form>

                <div id="loader-rr" class="loader">
                    <i class="fa fa-spinner fa-2x fa-spin"></i>
                    <h3>Loading route data...</h3>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script type="text/javascript">
    jQuery(function($) {

        $('#locations-table').hide();
        <?php if (isset($_GET['from']) && isset($_GET['to'])) { ?>

            $('select[name="location_type_id"]').val(<?= $_GET['to'] ?>);
            check_from_type_by_from_id(<?= $_GET['from'] ?>);

        <?php } ?>

    });

    function check_from_type_by_from_id(location_id) {
        $.ajax({
            type: 'GET',
            url: 'ajax_location_type_by_location_id/' + location_id,
            success: function(response) {
                response = $.parseJSON(response);
                if (response.status) {
                    $('#from-location-type').val(response.data.id);
                    list_from_locations(response.data.id);

                    //                    manage_rates();
                } else {
                    alert('Invalid location id.');
                }
            },
            failure: function(response) {
                alert('Something went wrong. Please try again.');
            }
        });
    }

    function list_from_locations(from_type_id) {
        $.ajax({
            type: 'GET',
            url: 'ajax_locations_by_type_id/' + from_type_id,
            success: function(response) {
                response = $.parseJSON(response);
                if (response.status) {
                    var from_location_html = '';
                    $.each(response.data, function(index, location) {
                        from_location_html += '<option value="' + location.id + '">' + location.name + '</option>';
                    });
                    $('[name=from_location_id]').html(from_location_html);

                    <?php if (isset($_GET['from']) && isset($_GET['to'])) : ?>
                        $('select[name="from_location_id"]').val(<?= $_GET['from'] ?>);
                        manage_rates();
                    <?php endif; ?>

                } else {
                    alert('No Locations found within this location type category. Try another.');
                }
            },
            failure: function(response) {
                alert('Something went wrong. Please try again.');
            }
        });
    }

    function manage_rates() {
        $('[name=increment_rate]').val(0);
        getLocationsWithRate();
    }

    function increment_rate() {
        getLocationsWithRate();
    }

    function getLocationsWithRate() {

        $('#loader-rr').show();
        var fromLocationId = $('select[name="from_location_id"] :selected').val();
        var locationTypeId = $('select[name="location_type_id"] :selected').val();

        $('#route-information').html($('select[name=location_type_id] :selected').text());
        $.ajax({
            type: "POST",
            url: "get_locations_with_rate",
            data: {
                'from_location_id': fromLocationId,
                'location_type_id': locationTypeId
            },
            dataType: "json",
            success: function(response) {

                if (response != 'No Data') {
                    var locationsTable = '';

                    locationsTable += '<table class="table table-striped table-hover" id="datatable-rr">' +
                        '<thead>' +
                        '<tr>' +
                        '<th width="5%">S.N.</th>' +
                        '<th width="50%"><strong class="box-title" id="route-information"></strong></th>' +
                        '<th width="15%">From<br><span class="route-information"></span></th>' +
                        '<th width="15%">To<br><span class="route-information"></span></th>' +
                        '<th width="15%">Discount</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>';


                    for (var i = 0; i < response.length; i++) {

                        var increment_type = $('[name=increment_type]:checked').val();
                        var increment_rate = parseInt($('[name=increment_rate]').val());
                        var rate_change_method = $('[name=rate_change_method]').find(":selected").val();

                        if (rate_change_method == 'increase') {
                            var from_to_rate = Math.round((increment_type == 'by_amount') ? parseInt(response[i].rate) + increment_rate : parseInt(response[i].rate) + (response[i].rate * increment_rate * 0.01));
                            var to_from_rate = Math.round((increment_type == 'by_amount') ? parseInt(response[i].reverse_rate) + increment_rate : parseInt(response[i].reverse_rate) + (parseInt(response[i].reverse_rate) * increment_rate * 0.01));
                        } else if (rate_change_method == 'decrease') {
                            var from_to_rate = Math.round((increment_type == 'by_amount') ? parseInt(response[i].rate) - increment_rate : parseInt(response[i].rate) - (response[i].rate * increment_rate * 0.01));
                            var to_from_rate = Math.round((increment_type == 'by_amount') ? parseInt(response[i].reverse_rate) - increment_rate : parseInt(response[i].reverse_rate) - (parseInt(response[i].reverse_rate) * increment_rate * 0.01));
                        }

                        from_to_rate = from_to_rate > 0 ? from_to_rate : 0;
                        to_from_rate = to_from_rate > 0 ? to_from_rate : 0;

                        locationsTable += '<tr>';
                        locationsTable += '<td>' + (i + 1) + '</td>';
                        locationsTable += '<td>' + response[i].name + '</td>';
                        locationsTable += '<td><div class="input-group"><span class="input-group-addon">&pound;</span><input type="text" class="form-control" name="' + fromLocationId + "-" + response[i].id + '" value="' + from_to_rate + '" onchange="copy_reverse_rate(this)"></div></td>';
                        locationsTable += '<td><div class="input-group"><span class="input-group-addon">&pound;</span><span></span><input type="text" class="form-control" name="' + response[i].id + "-" + fromLocationId + '" value="' + to_from_rate + '"></div></td>';
                        locationsTable += '<td><div class="input-group"><span class="input-group-addon">&pound;</span><span></span><input type="text" class="form-control" name="discount_' + fromLocationId + '-' + response[i].id + '_' + response[i].id + '-' + fromLocationId + '" value="' + response[i].discount + '"></div></td>';
                        locationsTable += '</tr>';
                    }

                    locationsTable += '</tbody>' +
                        '<tfoot>' +
                        '<tr>' +
                        '<td colspan="5"><button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save Rates</button></td>' +
                        '</tr>' +
                        '</tfoot>' +
                        '</table>';

                    var from = $('select[name="from_location_id"] option:selected').html();
                    var locationType = $('select[name="location_type_id"] option:selected').html();
                    $('.route-information').html(from);

                    $('input[name="from_location_id"]').val(fromLocationId);
                    $('input[name="location_type_id"]').val(locationTypeId);

                    $('#locations-table #table-ajax').html(locationsTable);

                    $('#locations-table').show();
                    $('#datatable-rr').dataTable();

                } else {
                    $('#locations-table').hide();
                }

                $('#loader-rr').hide();
            }
        });

    }

    function copy_reverse_rate(element) {
        var from_to_name = element.name.split('-');
        var to_from_name = $('[name=' + from_to_name[1] + '-' + from_to_name[0] + ']');

        if (to_from_name.val() == '0') {
            to_from_name.val(element.value);
        }

    }
</script>