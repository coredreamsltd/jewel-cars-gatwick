<?php
$isNew = true;
$data = $minimum[0];
$isNew = false;
$start = 0;
$end = $data['end'];
$id = $data['id'];
$rate = $data['rate'];
$is_min = $data['is_min'];
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Minimum Rate Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                    <form class="forms" action="<?php echo base_url('admin/rate/google_minimum/' . $id) ?>" method="post">
                        <table class="table-bordered table">
                            <tr>
                                <td>
                                    <label>Start <span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" disabled value="0">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>End <span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="<?php echo $end ?>" name="end">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Amount <span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="<?php echo $rate ?>" name="rate">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input class="btn btn-primary" type="submit" value="Save">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="col-md-6">

                    <div class="alert alert-warning" role="alert">
                        <h3><span class="label label-info">Information</span></h3>
                        EXAMPLE<br>
                        <table class="table">
                            <tr><th>Start: </th><td>0 Mile</td></tr>
                            <tr><th>End: </th><td>2 Mile</td></tr>
                            <tr><th>Amount: </th><td>&pound; 10</td></tr>
                        </table>  

                        <p>
                            <b>*Note:</b> This minimum charge will be added to the final fare.	
                        </p>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->