<?php
$isNew = true;
if(segment(4) != '') {
    $rate = $rate[0];
    $isNew = false;
}
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Breakdow Fare Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form class="forms" action="" method="post">
                    <table class="table-bordered table">
                        <tr>
                            <td>
                                <label>Start <span class="text-danger">*</span></label>
                                <div class="form-group">
                                    <div class="input-group small-input-box">
                                        <input class="form-control" type="text" name="start" value="<?php echo !$isNew ? $rate['start'] : '' ?>">
                                        <div class="input-group-addon">mile(s)</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>End <span class="text-danger">*</span></label>
                                <div class="form-group">
                                    <div class="input-group small-input-box">
                                        <input class="form-control" type="text" name="end" value="<?php echo !$isNew ? $rate['end'] : '' ?>">
                                        <div class="input-group-addon">mile(s)</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Distance Rate <span class="text-danger">*</span></label>
                                <div class="form-group">
                                    <div class="input-group small-input-box">
                                        <div class="input-group-addon">&pound;</div>
                                        <input class="form-control" type="text" name="rate" value="<?php echo !$isNew ? $rate['rate'] : '' ?>">
                                        <div class="input-group-addon">per mile</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="2">
                                <input class="btn btn-primary" type="submit" value="Save">
                            </td>
                        </tr>
                    </table>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->