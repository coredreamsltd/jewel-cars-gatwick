<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Fare Breakdown</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/rate/break-down-add-update') ?>" class="btn btn-success">Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-8">
                    <table class="table table-bordered" id="datatable-google-miles-rate">
                        <thead>
                            <tr>
                                <th>Start</th>
                                <th>End</th>
                                <th>Rate</th>
                                <th width="22%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($rates)) {
                                foreach ($rates as $p) {
                                    ?>
                                    <tr>
                                        <td><?php echo $p['start'] ?> mile(s)</td>
                                        <td><?php echo $p['end'] ?> mile(s)</td>
                                        <td><?php echo SITE_CURRENCY . $p['rate'] ?></td>
                                        <td>
                                            <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/rate/break-down-add-update/' . $p['id']) ?>"><i class="fa fa-edit"></i> Edit</a>
                                            <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/rate/break-down-delete/' . $p['id']) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(function () {
        $('#datatable-google-miles-rate').dataTable({
            bSort: false
        });
    });
</script>