<?php
$isNew = true;
if(segment(4) != '') {
    $isNew = false;
}
?>

<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Single Route Rate Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/rate/add-update-routes-rate') ?>" class="btn btn-success">Add Rates</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form action="update_single_route_rate" method="post" class="forms">
                	<input type="hidden" name="routes_rate_id" value="<?= $routes_rate['id'] ?>">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>
                                <label>From <span class="text-danger">*</span></label>
                                <input type="text" value="<?= $routes_rate['from_name'] ?>" class="form-control required" disabled>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>To <span class="text-danger">*</span></label>
                                <input type="text" value="<?= $routes_rate['to_name'] ?>" class="form-control required" disabled>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <label>Rate <span class="text-danger">*</span></label>
                                <input type="text" name="rate" value="<?= $routes_rate['rate']  ?>" class="form-control required">
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <input id="has-ckeditor" class="btn btn-primary" type="submit" value="Save">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div><!-- /.box-body -->
            
            <div class="box-header">
                <h3 class="box-title" id="route-information"></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
	            <form method="post" action="add_update_routes_rate">
	            	<input type="hidden" name="from_location_id">
	            	<table class="table table-bordered" id="locations-table"></table>
	            </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->