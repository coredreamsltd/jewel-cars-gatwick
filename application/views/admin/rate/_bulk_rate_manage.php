<?php flash() ?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Bulk Rate Manager</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <div class="col-md-4">
                <form action="" method="post">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    <label>Last updated:</label>
                                    <p class="form-control-static"><?= $add_charge->datetime_last_incremented ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Bulk Increment/Decrement<span class="text-danger">*</span></label>

                                    <div class="input-group">
                                        <div class="input-group-addon" style="padding: 5px 12px;">
                                            <select name="increase_or_decrease">
                                                <option value="increase" <?= $add_charge->increase_or_decrease == 'increase' ? 'checked' : '' ?>>Increase By</option>
                                                <option value="decrease" <?= $add_charge->increase_or_decrease == 'decrease' ? 'checked' : '' ?>>Decrease By</option>
                                            </select>
                                        </div>
                                        <input type="text" name="bulk_increment_decrement_rate" class="form-control" placeholder="0.00" value="<?= $add_charge->bulk_increment_decrement_rate ?>">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save Rates</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>

        </div>
    </div><!-- /.box -->
</div>
