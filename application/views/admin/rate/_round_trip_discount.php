<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Round Trip Discount Manager</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">

                    <div class="col-md-4">
                        <form action="" method="post">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Round Trip Discount<span class="text-danger">*</span></label>

                                            <div class="input-group">
                                                <input type="text" name="round_trip_discount" class="form-control" placeholder="Discount %" value="<?= $add_charge->round_trip_discount ?>">
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save Rates</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->