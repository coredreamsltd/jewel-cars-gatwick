<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Group Rate | <?= !$isEdit ? 'Add' : 'Edit' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php flash() ?>
                <form action="" method="post">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Group Rate Type</label>
                                <select name="group_rate_type" class="form-control">
                                    <option value="group-group" <?= $isEdit && $group_rate->to_group_id != null ? 'selected' : '' ?>>
                                        Group/Group Rate
                                    </option>
                                    <option value="group-location" <?= $isEdit && $group_rate->to_location_id != null ? 'selected' : '' ?>>
                                        Group/Location Rate
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>From</label>
                                <select class="form-control" name="from_group_id" required="">
                                    <option value="">- select group -</option>
                                    <?php
                                    if ($location_groups): foreach ($location_groups as $group):
                                        ?>
                                        <option value="<?= $group->id ?>" <?= $isEdit && $group_rate->from_group_id == $group->id ? 'selected' : '' ?>>
                                            <?= $group->group_name ?>
                                        </option>
                                        <?php
                                    endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>To <span class="text-danger">*</span></label>

                                <div id="to_options">
                                    <div id="to_location">
                                        <select class="form-control" id="to-location-type"
                                                onchange="list_from_locations(this.value)" required="">
                                            <option value="">- Select From Location Type -</option>
                                            <?php
                                            if ($location_types):
                                                foreach ($location_types as $type):
                                                    ?>
                                                    <option value="<?= $type->id ?>"><?= $type->type ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                        <select name="to_location_id" class="form-control" required="">
                                            <option value="">- Select From Location -</option>
                                        </select>
                                    </div>

                                    <div id="to_group">
                                        <select class="form-control" name="to_group_id" required="">
                                            <option value="">- select group -</option>
                                            <?php
                                            if ($location_groups): foreach ($location_groups as $group):
                                                ?>
                                                <option value="<?= $group->id ?>" <?= $isEdit && $group_rate->to_group_id == $group->id ? 'selected' : '' ?>>
                                                    <?= $group->group_name ?>
                                                </option>
                                                <?php
                                            endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>One way rate</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><?= CURRENCY ?></span>
                                            <input type="text" name="one_way_rate" class="form-control"
                                                   value="<?= $isEdit ? $group_rate->one_way_rate : '' ?>" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Return rate</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><?= CURRENCY ?></span>
                                            <input type="text" name="return_rate" class="form-control"
                                                   value="<?= $isEdit ? $group_rate->return_rate : '' ?>" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Discount</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><?= CURRENCY ?></span>
                                            <input type="text" name="discount" class="form-control"
                                                   value="<?= $isEdit ? $group_rate->discount : '' ?>" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-success" type="submit"> Save Rate</button>
                        </div>

                    </div>
                </form>
            </div><!-- /.box-body -->

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<script type="text/javascript">
    var SITE_URL = '<?=site_url()?>';
    jQuery(function ($) {

        check_group_rate_type();

        $('#locations-table').hide();
        <?php if ($isEdit && $group_rate->to_location_id != null) : ?>

        $('select[name="location_type_id"]').val(<?= $group_rate->to_location_id ?>);
        check_from_type_by_from_id(<?= $group_rate->to_location_id ?>);

        <?php endif; ?>

        $('[name=group_rate_type]').change(function () {
            check_group_rate_type();
        });

    });

    function check_from_type_by_from_id(location_id) {
        $.ajax({
            type: 'GET',
            url: SITE_URL + 'admin/rate/ajax_location_type_by_location_id/' + location_id,
            success: function (response) {
                response = $.parseJSON(response);
                if (response.status) {
                    $('#to-location-type').val(response.data.id);
                    list_from_locations(response.data.id);

                } else {
                    alert('Invalid location id.');
                }
            },
            failure: function (response) {
                alert('Something went wrong. Please try again.');
            }
        });
    }

    function list_from_locations(from_type_id) {
        $.ajax({
            type: 'GET',
            url: SITE_URL + 'admin/rate/ajax_locations_by_type_id/' + from_type_id,
            success: function (response) {
                response = $.parseJSON(response);
                if (response.status) {
                    var from_location_html = '';
                    $.each(response.data, function (index, location) {
                        from_location_html += '<option value="' + location.id + '">' + location.name + '</option>';
                    });
                    $('[name=to_location_id]').html(from_location_html);

                    <?php if ($isEdit && $group_rate->to_location_id != null) : ?>
                    $('select[name="to_location_id"]').val(<?= $group_rate->to_location_id ?>);
                    <?php endif; ?>

                } else {
                    alert('No Locations found within this location type category. Try another.');
                }
            },
            failure: function (response) {
                alert('Something went wrong. Please try again.');
            }
        });
    }

    function getLocationsWithRate() {

        $('#loader-rr').show();
        var fromLocationId = $('select[name="to_location_id"] :selected').val();
        var locationTypeId = $('select[name="location_type_id"] :selected').val();

        $('#route-information').html($('select[name=location_type_id] :selected').text());
        $.ajax({
            type: "POST",
            url: "get_locations_with_rate",
            data: {'to_location_id': fromLocationId, 'location_type_id': locationTypeId},
            dataType: "json",
            success: function (response) {

                if (response != 'No Data') {
                    var locationsTable = '';

                    locationsTable += '<table class="table table-striped table-hover" id="datatable-rr">'
                        + '<thead>'
                        + '<tr>'
                        + '<th width="5%">S.N.</th>'
                        + '<th width="50%"><strong class="box-title" id="route-information"></strong></th>'
                        + '<th width="15%">From<br><span class="route-information"></span></th>'
                        + '<th width="15%">To<br><span class="route-information"></span></th>'
                        + '<th width="15%">Discount</th>'
                        + '</tr>'
                        + '</thead>'
                        + '<tbody>';


                    for (var i = 0; i < response.length; i++) {

                        var increment_type = $('[name=increment_type]:checked').val();
                        var increment_rate = parseInt($('[name=increment_rate]').val());
                        var rate_change_method = $('[name=rate_change_method]').find(":selected").val();

                        if (rate_change_method == 'increase') {
                            var from_to_rate = Math.round((increment_type == 'by_amount') ? parseInt(response[i].rate) + increment_rate : parseInt(response[i].rate) + (response[i].rate * increment_rate * 0.01));
                            var to_from_rate = Math.round((increment_type == 'by_amount') ? parseInt(response[i].reverse_rate) + increment_rate : parseInt(response[i].reverse_rate) + (parseInt(response[i].reverse_rate) * increment_rate * 0.01));
                        } else if (rate_change_method == 'decrease') {
                            var from_to_rate = Math.round((increment_type == 'by_amount') ? parseInt(response[i].rate) - increment_rate : parseInt(response[i].rate) - (response[i].rate * increment_rate * 0.01));
                            var to_from_rate = Math.round((increment_type == 'by_amount') ? parseInt(response[i].reverse_rate) - increment_rate : parseInt(response[i].reverse_rate) - (parseInt(response[i].reverse_rate) * increment_rate * 0.01));
                        }

                        from_to_rate = from_to_rate > 0 ? from_to_rate : 0;
                        to_from_rate = to_from_rate > 0 ? to_from_rate : 0;

                        locationsTable += '<tr>';
                        locationsTable += '<td>' + (i + 1) + '</td>';
                        locationsTable += '<td>' + response[i].name + '</td>';
                        locationsTable += '<td><div class="input-group"><span class="input-group-addon">&pound;</span><input type="text" class="form-control" name="' + fromLocationId + "-" + response[i].id + '" value="' + from_to_rate + '" onchange="copy_reverse_rate(this)"></div></td>';
                        locationsTable += '<td><div class="input-group"><span class="input-group-addon">&pound;</span><span></span><input type="text" class="form-control" name="' + response[i].id + "-" + fromLocationId + '" value="' + to_from_rate + '"></div></td>';
                        locationsTable += '<td><div class="input-group"><span class="input-group-addon">&pound;</span><span></span><input type="text" class="form-control" name="discount_' + fromLocationId + '-' + response[i].id + '_' + response[i].id + '-' + fromLocationId + '" value="' + response[i].discount + '"></div></td>';
                        locationsTable += '</tr>';
                    }

                    locationsTable += '</tbody>'
                        + '<tfoot>'
                        + '<tr>'
                        + '<td colspan="5"><button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save Rates</button></td>'
                        + '</tr>'
                        + '</tfoot>'
                        + '</table>';

                    var from = $('select[name="to_location_id"] option:selected').html();
                    var locationType = $('select[name="location_type_id"] option:selected').html();
                    $('.route-information').html(from);

                    $('input[name="to_location_id"]').val(fromLocationId);
                    $('input[name="location_type_id"]').val(locationTypeId);

                    $('#locations-table #table-ajax').html(locationsTable);

                    $('#locations-table').show();
                    $('#datatable-rr').dataTable();

                } else {
                    $('#locations-table').hide();
                }

                $('#loader-rr').hide();
            }
        });

    }

    function copy_reverse_rate(element) {
        var from_to_name = element.name.split('-');
        var to_from_name = $('[name=' + from_to_name[1] + '-' + from_to_name[0] + ']');

        if (to_from_name.val() == '0') {
            to_from_name.val(element.value);
        }
    }

    function check_group_rate_type() {
        var group_rate_type = $('[name=group_rate_type] :selected').val();

        $('#to_options div').hide();

        if (group_rate_type == 'group-group') {
            $('#to_group select').removeAttr('disabled');
            $('#to_location select').attr('disabled', '');

            $('#to_group').show();
        } else if (group_rate_type == 'group-location') {
            $('#to_location select').removeAttr('disabled');
            $('#to_group select').attr('disabled', '');

            $('#to_location').show();
        }

    }
</script>