<div class="box">
    <div class="box-header">
        <h3 class="box-title btn-block">
            Group Rates
            <div class="pull-right">
                <a href="<?= site_url('admin/rate/add_edit_group_rate') ?>" class="btn btn-success">Add Group Rate</a>
            </div>
        </h3>
    </div>
    <div class="box-body">
        <?php flash() ?>
        <table class="table" id="dt-table">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>From Group</th>
                    <th>To Group</th>
                    <th>To Location</th>
                    <th>One Way Fare</th>
                    <th>Return Fare</th>
                    <th>Discount</th>
                    <th>Last Updated</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($group_rates):
                    foreach ($group_rates as $index => $rate):
                        ?>
                        <tr>
                            <td><?= ++$index ?></td>
                            <td><a href="<?= site_url("admin/location/add_edit_group/$rate->from_group_id") ?>"><?= $rate->from_group_name ?></a></td>
                            <td><a href="<?= site_url("admin/location/add_edit_group/$rate->to_group_id") ?>"><?= $rate->to_group_name ? $rate->to_group_name : '-' ?></a></td>
                            <td><?= $rate->to_location_name ? $rate->to_location_name : '-' ?></td>
                            <td><?= CURRENCY . $rate->one_way_rate ?></td>
                            <td><?= CURRENCY . $rate->return_rate ?></td>
                            <td><?= CURRENCY . $rate->discount ?></td>
                            <td><?= date('d M, Y h:i A', strtotime($rate->updated_at)) ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="<?= site_url('admin/rate/add_edit_group_rate/' . $rate->id) ?>"><i class="fa fa-edit"></i> Edit</a></li>
                                        <li><a href="<?= site_url('admin/rate/delete_group_rate/' . $rate->id) ?>" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i> Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#dt-group-rates').dataTable();
    });
</script>