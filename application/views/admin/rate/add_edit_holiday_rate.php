<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Holiday Rate | <?= !$isEdit ? 'Add' : 'Edit' ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php flash() ?>
                <form action="" method="post">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <br>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" <?= $isEdit && $holiday_rate->is_active ? "checked" : '' ?> value="1" required="">Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" value="0" <?= $isEdit && !$holiday_rate->is_active ? "checked" : '' ?>>Inactive
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Incr/Decr Rate (Use - to decrease)</label>
                                        <div class="input-group">
                                            <input type="text" name="incr_decr_rate" class="form-control" value="<?= $isEdit ? $holiday_rate->incr_decr_rate : '' ?>" required="">
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Starting Date</label>    
                                        <input class="form-control datepicker" name="starting_date" value="<?= isset($holiday_rate->starting_date) ? DateTime::createFromFormat('Y-m-d', $holiday_rate->starting_date)->format('d/m/Y') : '' ?>"  required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Starting Time<span class="text-danger">*</span></label>
                                        <input class="form-control timepicki" name="starting_time" value="<?= isset($holiday_rate->starting_time) ? DateTime::createFromFormat('H:i:s', $holiday_rate->starting_time)->format('H:i') : '' ?>"  required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Ending Date <span class="text-danger">*</span></label>
                                        <input class="form-control datepicker" name="ending_date" value="<?= isset($holiday_rate->ending_date) ? DateTime::createFromFormat('Y-m-d', $holiday_rate->ending_date)->format('d/m/Y') : '' ?>"  required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Ending Time</label>
                                        <input class="form-control timepicki" name="ending_time" value="<?= isset($holiday_rate->ending_time) ? DateTime::createFromFormat('H:i:s', $holiday_rate->ending_time)->format('H:i') : '' ?>"  required>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success">Save Rate</button>
                        </div>
                    </div>
                </form>
            </div><!-- /.box-body -->

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
