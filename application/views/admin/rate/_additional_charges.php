<?php $count = segment(4) + 1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Additional Charges Manager</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <?php /*
                    <div class="col-md-4">
                        <form action="" method="post" class="forms">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Wait & Return ( Two way )<span class="text-danger"></span></label>

                                            <div class="input-group" style="width: 200px">

                                                <input type="text" name="wait_and_return" class="form-control" placeholder="Amount" value="<?= $additional_charges[0]['wait_and_return'] ?>">
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Child Seat Charge (Per seat)<span class="text-danger"></span></label>
        
                                            <div class="input-group" style="width: 200px">
                                                <span class="input-group-addon">&pound;</span>
                                                <input type="text" name="child_seat" class="form-control" placeholder="Amount" value="<?= $additional_charges[0]['child_seat'] ?>">
        
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Pickup From Airport Charge<span class="text-danger"></span></label>
                                            <div class="input-group" style="width: 200px">
                                                <div class="input-group-addon">&pound;</div>
                                                <input type="text" name="from_airport_fee" class="form-control" placeholder="Amount" value="<?= $additional_charges[0]['from_airport_fee'] ?>">
                                            </div>
                                        </td>
                                    </tr>  
                                    <tr>
                                        <td>
                                            <label>Meet and Greet Charge <span class="text-danger"></span></label>
                                            <div class="input-group" style="width: 200px">
                                                <div class="input-group-addon">&pound;</div>
                                                <input type="text" name="meet_and_greet" class="form-control" placeholder="Amount" value="<?= $additional_charges[0]['meet_and_greet'] ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Paypal/Card Charge<span class="text-danger"></span></label>

                                            <div class="input-group" style="width: 200px">
                                                <span class="input-group-addon">&pound;</span>
                                                <input type="text" name="card_fee" class="form-control" placeholder="Amount" value="<?= $additional_charges[0]['card_fee'] ?>">

                                            </div>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save Rates</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    */?>
                    <div class="col-md-8">
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Airport Pickup Charges</a></li>
                                <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Seaport Pickup Charges</a></li>
                                <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Railway Station Pickup Charges</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab1">
                                    <form action="<?= base_url('admin/rate/save_pickup_meet_rates') ?>" method="post">
                                        <table class="table">
                                            <tr>
                                                <th>Airport Name</th>
                                                <th width="20%">Charge</th>
                                            </tr>
                                            <?php
                                            $pickup_rates = $this->common_model->run_query("SELECT l.id as l_id, l.name, pr.amount FROM tbl_locations as l LEFT JOIN tbl_add_rate_pickup_meet as pr ON l.id = pr.location_id WHERE l.location_type_id = 1");
                                            if ($pickup_rates):
                                                foreach ($pickup_rates as $pr):
                                                    ?>
                                                    <tr>
                                                        <td><?= $pr['name'] ?></td>
                                                        <td>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><?= CURRENCY ?></span>
                                                                <input type="text" class="form-control" name="<?= $pr['l_id'] ?>" value="<?= $pr['amount'] ? $pr['amount'] : 0 ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </table>
                                        <button type="submit" class="btn btn-primary">Save Airport Charges</button>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab2">
                                    <form action="<?= base_url('admin/rate/save_pickup_meet_rates') ?>" method="post">
                                        <table class="table">
                                            <tr>
                                                <th>Seaport Name</th>
                                                <th width="20%">Charge</th>
                                            </tr>
                                            <?php
                                            $pickup_rates = $this->common_model->run_query("SELECT l.id as l_id, l.name, pr.amount FROM tbl_locations as l LEFT JOIN tbl_add_rate_pickup_meet as pr ON l.id = pr.location_id WHERE l.location_type_id = 6");
                                            if ($pickup_rates):
                                                foreach ($pickup_rates as $pr):
                                                    ?>
                                                    <tr>
                                                        <td><?= $pr['name'] ?></td>
                                                        <td>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><?= CURRENCY ?></span>
                                                                <input type="text" class="form-control" name="<?= $pr['l_id'] ?>" value="<?= $pr['amount'] ? $pr['amount'] : 0 ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </table>
                                        <button type="submit" class="btn btn-primary">Save Seaport Charges</button>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab3">
                                    <form action="<?= base_url('admin/rate/save_pickup_meet_rates') ?>" method="post">
                                        <table class="table">
                                            <tr>
                                                <th>Railway Station Name</th>
                                                <th width="20%">Charge</th>
                                            </tr>
                                            <?php
                                            $pickup_rates = $this->common_model->run_query("SELECT l.id as l_id, l.name, pr.amount FROM tbl_locations as l LEFT JOIN tbl_add_rate_pickup_meet as pr ON l.id = pr.location_id WHERE l.location_type_id = 8");
                                            if ($pickup_rates):
                                                foreach ($pickup_rates as $pr):
                                                    ?>
                                                    <tr>
                                                        <td><?= $pr['name'] ?></td>
                                                        <td>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><?= CURRENCY ?></span>
                                                                <input type="text" class="form-control" name="<?= $pr['l_id'] ?>" value="<?= $pr['amount'] ? $pr['amount'] : 0 ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </table>
                                        <button type="submit" class="btn btn-primary">Save Railway Station Charges</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->