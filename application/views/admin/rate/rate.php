<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <?php flash() ?>
            <form method="post">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td colspan="2" class="bg-gray">
                                <h4>
                                    <span class="pull-right">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Save</button>
                                    </span>
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <th>Direction</th>
                            <td>
                                <select class=" form-control" type="text" name="direction" placeholder="Select Gender" required>
                                    <option value="">Select Direction</option>
                                    <option value="one_way" <?= (!empty($rate->direction) && $rate->direction  == 'one_way') ? 'selected' : ''; ?>>
                                        One Way
                                    </option>
                                    <option value="two_way" <?= (!empty($rate->direction) && $rate->direction  == 'two_way') ? 'selected' : ''; ?>>
                                        Two Way
                                    </option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th>From</th>
                            <td><input type="text" id="search_from" placeholder="" name="from" value="<?= !empty($rate->from) ? json_decode($rate->from) : ''  ?>" autocomplete="off" class="form-control" required /></td>
                            <td><input type="hidden" id="search_from_id" name="from_id" value="<?= !empty($rate->from_id) ? json_decode($rate->from_id) : ''  ?>"> </td>
                        </tr>
                        <tr>
                            <th>To</th>
                            <td><input type="text" id="search_to" placeholder="" name="to" value="<?= !empty($rate->to) ? json_decode($rate->to) : ''  ?>" autocomplete="off" class="form-control" required /></td>
                            <td><input type="hidden" id="search_to_id" name="to_id" autocomplete="off" value="<?= !empty($rate->to_id) ? json_decode($rate->to_id) : ''  ?>"> </td>
                        <tr>
                            <th>Base Price</th>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">£</span>
                                    <input type="text" class="form-control search-label-up" name="base_price" value="<?= !empty($rate->base_price) ? $rate->base_price : ''  ?>" autocomplete="off" required>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="bg-gray">
                                <h4>
                                    <strong>Action & Vehicle</strong>
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <th>Action</th>
                            <td>
                                <select class="form-control" type="text" name="action" required>
                                    <option value="">Select Action</option>
                                    <option value="multiply" <?= (!empty($rate->action) && $rate->action == 'multiply') ? 'selected' : ''; ?>>
                                        Multiply Base Price (*)
                                    </option>
                                </select>
                            </td>
                        </tr>

                        <?php $fleet_rates = (!empty($rate->fleet_rates)) ? json_decode($rate->fleet_rates, true) : ''; ?>
                        <?php if ($fleets) : foreach ($fleets as $key => $fleet) : ?>
                                <tr>
                                    <th><?= ucwords($fleet->title) ?></th>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon"><?= CURRENCY ?></span>
                                            <input type="text" class="form-control" name="fleet_rates[<?= $fleet->id ?>]" value="<?= !empty($fleet_rates[$fleet->id]) ? $fleet_rates[$fleet->id] : 0 ?>" required>
                                        </div>
                                    </td>
                                </tr>
                        <?php endforeach;
                        endif; ?>
                    </tbody>
                </table>
            </form>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(document).ready(function() {
        $('#search_from').tokenfield({
            autocomplete: {
                source: function(request, response) {
                    jQuery.get('<?= base_url('admin/rate/ajax_auto_complete') ?>', {
                        query: request.term
                    }, function(data) {
                        data = JSON.parse(data);
                        response(data);
                    });
                },
                select: function(event, ui) {
                    var ids = $("#search_from_id").val();
                    //    console.log('1---',ids);
                    if (ids === "") {
                        ids = ui.item.id;
                    } else {
                        ids += ',' + ui.item.id;
                    }
                    // console.log('2---',ids);
                    $("#search_from_id").val(ids);
                    from_checklocation(ui.item.id);
                },
            }
        });

        function from_checklocation(id) {
            var to = document.getElementById("search_to_id").value;
            if (to === '') {
                return;
            } else {
                $.ajax({
                    url: "<?= site_url('admin/rate/ajax_validate_location') ?>",
                    type: 'post',
                    data: {
                        f_id: id,
                        type: 'from',
                        t_id: to,
                    },
                    dataType: 'json',
                    success: function(response) {
                        if (!response.status) {
                            alert(response.msg);
                        }
                    }
                });
            }
        }

        $('#search_to').tokenfield({

            autocomplete: {
                source: function(request, response) {
                    jQuery.get('<?= base_url('admin/rate/ajax_auto_complete') ?>', {
                        query: request.term
                    }, function(data) {
                        data = JSON.parse(data);
                        response(data);
                    });
                },
                select: function(event, ui) {
                    var ids = $("#search_to_id").val();
                    //    console.log('1---',ids);
                    if (ids === "") {
                        ids = ui.item.id;
                    } else {
                        ids += ',' + ui.item.id;
                    }
                    // console.log('2---',ids);
                    $("#search_to_id").val(ids);
                    to_checklocation(ui.item.id);
                },
            }
        });

        function to_checklocation(id) {
            var to = document.getElementById("search_from_id").value;
            if (to === '') {
                return;
            } else {
                $.ajax({
                    url: "<?= site_url('admin/rate/ajax_validate_location') ?>",
                    type: 'post',
                    data: {
                        f_id: id,
                        type: 'to',
                        t_id: to,
                    },
                    dataType: 'json',
                    success: function(response) {
                        if (!response.status) {
                            alert(response.msg);
                        }
                    }
                });
            }
        }
    });
</script>