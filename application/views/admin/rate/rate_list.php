<?php $count = 1; ?>
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/rate/add-update') ?>" class="btn btn-success"><i class="fa fa-plus-square"> Add Rate</i></a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered dt-tables">
                        <thead>
                            <tr>
                                <th width="2%">#</th>
                                <th>From</th>
                                <th>To</th>
                                <th width="10%">Direction</th>
                                <th>Base Price</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($rates)) :
                                foreach ($rates as $index => $rate) :
                                    $rate->from = json_decode($rate->from);
                                    $rate->to = json_decode($rate->to);

                                    $rate->from = explode(',', $rate->from);
                                    $rate->to = explode(',', $rate->to);
                            ?>
                                    <tr>
                                        <td><?= $count++; ?></td>
                                        <td> <?= '<span class="badge">'.implode('</span>,<span class="badge">', $rate->from).'</span>' ?> </td>
                                        <td> <?= '<span class="badge">'.implode('</span>,<span class="badge">', $rate->to).'</span>' ?> </td>
                                        <td><?= ucwords(str_replace('_',' ',$rate->direction))?></td>
                                        <td><?= CURRENCY.$rate->base_price ?></td>
                                        <td>
                                            <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/rate/add-update/' . $rate->id) ?>"><i class="fa fa-trash text-primary"> Edit</i></a>
                                            <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/rate/delete/' . $rate->id) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash text-danger"> Delete</i></a>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                            <?php endforeach;
                            endif; ?>

                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->