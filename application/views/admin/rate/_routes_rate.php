<?php $count = segment(4)+1; ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/rate/add-update-routes-rate') ?>" class="btn btn-success">Add Rates</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th style="width: 70px">#</th>
                        <th>FROM</th>
                        <th>TO</th>
                        <th>Rate</th>
                        <th style="width: 150px">Action</th>
                    </tr>
                    <?php if(!empty($routes_rates)) {
                        foreach($routes_rates as $rr) { ?>
                            <tr>
                                <td><?php echo $count;$count++; ?></td>
                                <td><?php echo $rr['from_name'] ?></td>
                                <td><?php echo $rr['to_name'] ?></td>
                                <td><?=CURRENCY?><?php echo $rr['rate'] ?></td>
                                <td>
                                    <a class="btn btn-sm btn-info" href="<?php echo base_url('admin/rate/update_single_route_rate/'.$rr['id']) ?>">Edit</a>
                                    <a class="btn btn-sm btn-danger" href="<?php echo base_url('admin/rate/delete_single_route/'.$rr['id']) ?>" onclick="return confirm('Are you sure?')">Delete</a>
                                </td>
                            </tr>
                    <?php } } else { ?>
                        <tr>
                            <td colspan="5">- No Data Available -</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <?php if(!empty($routes_rates)) { ?>
                <div class="box-footer clearfix">
                    <?php echo $pagination ?>
                </div>
            <?php } ?>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
