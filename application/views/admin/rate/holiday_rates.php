<div class="box">
    <div class="box-header">
        <h3 class="box-title btn-block">
            Holiday Rates
            <div class="pull-right">
                <a href="<?= site_url('admin/rate/add_edit_holiday_rate') ?>" class="btn btn-success">Add Holiday Rate</a>
            </div>
        </h3>
    </div>
    <div class="box-body">
        <?php flash() ?>
        <table class="table table-bordered" id="dt-holiday-rates">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Start Date/Time</th>
                    <th>End Date/Time</th>
                    <th>Incr/Decr Rate</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($holiday_rates):
                    foreach ($holiday_rates as $index => $holiday):
                        ?>
                        <tr>
                            <td><?= ++$index ?></td>
                            <td><?= DateTime::createFromFormat('Y-m-d', $holiday->starting_date)->format('d/m/Y') . ' ' . DateTime::createFromFormat('H:i:s', $holiday->starting_time)->format('h:i A') ?></td>
                            <td><?= DateTime::createFromFormat('Y-m-d', $holiday->ending_date)->format('d/m/Y') . ' ' . DateTime::createFromFormat('H:i:s', $holiday->ending_time)->format('h:i A') ?></td>
                            <td><?= $holiday->incr_decr_rate ?>%</td>
                            <td>
                                <span class="label label-<?= $holiday->is_active ? 'success' : 'danger' ?>"><?= $holiday->is_active ? 'Active' : 'Disabled' ?></span>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="add_edit_holiday_rate/<?= $holiday->id ?>"><i class="fa fa-edit"></i> Edit</a></li>
                                        <li><a href="delete_holiday_rate/<?= $holiday->id ?>" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i> Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>

            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#dt-holiday-rates').dataTable();
    });
</script>