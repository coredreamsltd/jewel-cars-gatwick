<style>
    img {
        max-width: 218px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="collapse" id="collapseExample">
            <div class="card card-body">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Package</h3>
                    </div>
                    <form role="form" action="<?= site_url('admin/packages/addUpdate') ?>" method="post"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" name="title" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type='file' onchange="readURL(this);" name="image">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <img id="blah" src="<?= base_url('uploads/package/default.png') ?>" alt="image">
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Rate</label>
                                        <input type='text' class="form-control" name="rate" required>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Image Alt Tag</label>
                                        <input type='text' class="form-control" name="alt">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="excerpt" rows="6" required=""></textarea>
                                    </div>
                                </div>

                                <!--                            <div class="col-md-12">-->
                                <!--                                <div class="form-group">-->
                                <!--                                    <label>Content</label>-->
                                <!--                                    <textarea class="form-control" name="content" rows="6" required=""></textarea>-->
                                <!--                                </div>-->
                                <!--                            </div>-->
                                <!--<div class="col-md-12">
                                <legend>Add Rates</legend>
                                <div class="row">
                                    <?php /*if($fleets): foreach($fleets as $fleet):*/ ?>
                                    <div class="col-md-3">
                                        <label><? /*=$fleet->title*/ ?></label><br>
                                        <input type="number" name="data[<? /*=$fleet->id*/ ?>]" value="0" required>
                                    </div>
                                <?php /*endforeach; endif;*/ ?>
                                </div>
                            </div>-->
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Packages</h3>
                <span class="pull-right add-new">
                    <a class="btn btn-primary btn-flat" data-toggle="collapse" href="#collapseExample" role="button"
                       aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-plus"></i> Add New</a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">

                <table class="table table-bordered table-striped" id="dt-table">
                    <thead>
                    <tr>
                        <th width="3%">#</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Excerpt</th>
                        <th>Rate</th>
                        <th width="12%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($packages)):
                        foreach ($packages as $index => $package):
                            ?>
                            <tr>
                                <td><?= ++$index; ?></td>
                                <td>
                                    <img src="<?= base_url('uploads/package/' . $package->image) ?>" alt="<?=$package->alt?>" width="100px">
                                </td>
                                <td><?= $package->title ?></td>
                                <td style="word-break: break-all"><?= substr($package->excerpt,0,150) ?>......</td>
                                <td><?= CURRENCY.$package->rate ?></td>
                                <td>
                                    <a class="btn btn-sm btn-info" href="#!" onclick="edit('<?= $package->id ?>')">
                                        Edit
                                    </a>
                                    <a class="btn btn-sm btn-danger"
                                       href="<?php echo site_url('admin/packages/delete/' . $package->id) ?>"
                                       onclick="return confirm('Are you sure?')">Delete</a>
                                </td>
                            </tr>
                        <?php endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
<script>
    function edit(id) {
        $.ajax({
            url: '<?=site_url()?>' + "admin/packages/ajaxGetPackage/" + id,
            dataType: 'json',
            success: function (response) {
                if (!response.status) {
                    alert(response.message);
                    return;

                }
                var url = '<?= site_url('admin/packages/addUpdate/') ?>' + id;
                $('#collapseExample form').attr('action', url);
                $('#collapseExample [name=title]').val(response.data.title);
                $('#collapseExample [name=rate]').val(response.data.rate);
                $('#collapseExample [name=excerpt]').val(response.data.excerpt);
                $('#collapseExample [name=alt]').val(response.data.alt);
                $('#blah').attr('src', "<?= base_url('uploads/package/') ?>"+response.data.image);
                $('#collapseExample').toggle();

            }
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>