<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">

                <h3 class="box-title">Queries and Answers</h3>
                <span class="pull-right add-new">
                    <a href="<?php echo base_url('admin/help/add_update_qa?help_id='.segment('4')) ?>" class="btn btn-success btn-flat">
                        <i class="fa fa-plus-square"> Add New Query & Answer </i>
                    </a>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-stripped table-hover dt-tables">
                        <thead>
                            <tr>
                                <th width="2%">S.No</th>
                                <th width="28%">Title</th>
                                <th width="30%">Content</th>
                                <th width="25%">Post By</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($qas))
                                foreach ($qas as $count => $qa) {
                            ?>
                                <tr>
                                    <td><?= ++$count; ?></td>
                                    <td><?= $qa->title ?></td>
                                    <td>
                                        <?= substr($qa->content,0,100) ?>........
                                    </td>
                                    <td><?= $qa->post_by?:'Admin' ?></td>
                                    <td>
                                        <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/help/add_update_qa/' . $qa->id.'?help_id='.segment('4')) ?>"><i class="fa fa-edit text-primary"> Edit</i></a>
                                        <a class="btn btn-sm btn-default" href="<?php echo base_url('admin/help/delete_qa/' . $qa->id.'?help_id='.segment('4')) ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash text-danger"> Delete</i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.box-body -->

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Import Location Data</h4>
            </div>
            <form action="<?= site_url('admin/rate/importLocationWithRates') ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="file" name="file" required>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-flat"> Import</button>
                </div>
            </form>
        </div>

    </div>
</div>