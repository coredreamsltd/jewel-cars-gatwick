<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header"></div><!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" class="forms" enctype="multipart/form-data">
                    <input type="hidden" name="help_id" value="<?= !empty($_GET['help_id']) ? $_GET['help_id'] : '' ?>">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Title <span class="text-danger">*</span></label>
                                    <input type="text" placeholder="" class="form-control" name="title" value="<?= !empty($qa->title) ? $qa->title : '' ?>" required="">
                                </div>
                                <div class="col-md-6">
                                    <label>Post By <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="post_by" value="<?= !empty($qa->post_by) ? $qa->post_by : '' ?>">
                                </div>
                                <div class="col-md-12">
                                    <label>Content <span class="text-danger">*</span></label>
                                    <textarea class="form-control" rows="6" name="content"><?= !empty($qa->content) ? $qa->content : '' ?></textarea>
                                    <script type="text/javascript">
                                    var editor = CKEDITOR.replace('content',
                                        {
                                            customConfig: '<?php echo base_url('assets/admin/js/plugins/ckeditor/my_config.js') ?>'
                                        });
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url('assets/admin/js/plugins/ckfinder/') ?>');
                                </script>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Posted Image <span class="text-danger">*</span></label>
                            <input type="file" name="image">
                            <?php if (!empty($qa->image)) : ?>
                                <img src="<?= base_url('uploads/help/' . $qa->image) ?>">
                            <?php endif; ?>
                        </div>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-success"> Save</button>
                </form>
            </div>

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(function() {
        $('.fa-icon').iconpicker();
    });
</script>