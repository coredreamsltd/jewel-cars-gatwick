<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header"></div><!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" class="forms" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Title <span class="text-danger">*</span></label>
                                    <input type="text" placeholder="Enter title" class="form-control" name="title" value="<?= !empty($help->title) ? $help->title : '' ?>" required="">
                                </div>
                                <div class="col-md-6">
                                    <label>Title Icon <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control fa-icon" name="title_image" value="<?= !empty($help->title_image) ? $help->title_image : '' ?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Post By <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="post_by" value="<?= !empty($help->post_by) ? $help->post_by : '' ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Posted Image <span class="text-danger">*</span></label>
                            <input type="file" name="image">
                            <?php if(!empty($help->image)):?>
                                <img src="<?=base_url('uploads/help/'.$help->image)?>">
                            <?php endif;?>
                        </div>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-success"> Save</button>
                </form>
            </div>

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<script>
    $(function() {
        $('.fa-icon').iconpicker();
    });
</script>