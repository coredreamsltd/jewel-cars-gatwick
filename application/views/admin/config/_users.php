<div class="row">
    <div class="col-md-12">
        <?php flash() ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Admin User Manager</h3>
                <?php if ($_SESSION['current_user'] == 1) : ?>
                    <span class="pull-right">
                        <a href="<?= site_url('admin/config/add-update') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add new user</a>
                    </span>
                <?php endif; ?>
            </div><!-- /.box-header -->

            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 70px">#</th>
                            <th>Username</th>
                            <th style="width: 150px">Action</th>
                        </tr>
                        <?php if (!empty($admins)) :
                            foreach ($admins as $index => $admin) : ?>
                                    <tr>
                                        <td><?= ++$index; ?></td>
                                        <td><?= $admin->username ?></td>
                                        <td>
                                            <a class="btn btn-xs btn-info" href="<?= site_url('admin/config/add_update/' . $admin->id) ?>"><i class="fa fa-edit"></i> Edit</a>
                                            <?php if ($admin->id != 1 && $_SESSION['current_user'] ==1) : ?>
                                                <a class="btn btn-xs btn-danger" href="<?= site_url('admin/config/delete/' . $admin->id) ?>"><i class="fa fa-delete"></i> Delete</a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <tr>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->

        </div><!-- /.box -->
    </div>
</div><!-- /.row -->