<style>
    div#map_geolocation img {
        max-width: none;
    }

    #map-canvas {
        height: 525px;
    }

    #map-canvas img {
        max-width: none !important;
    }

    /* #pac-input-1 {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 50%;
        margin-top: 11px;
    }

    #pac-input-1:focus {
        border-color: #4d90fe;
    } */

    .loader-map p {
        font-size: 24px;
        background: #f5f5f5c2;
    }

    .loader-map {
        position: absolute;
        top: 50%;
        left: 48%;
        margin-left: -50px;
        width: 480px;
        height: 133px;
        z-index: 3;
        text-align: center;
    }

    .result {
        font-size: 0.8em;
        margin: 5px;
        margin-top: 0px;
        padding: 4px 8px;
        border-radius: 2px;
        background: #F0F7FF;
        border: 2px solid #D7E7FF;
        cursor: pointer;
        min-height: 5em;
    }

    .result.active {
        background-color: #D9E7F7;
        border-color: #9DB9E4;
    }

    .result img {
        float: right;
    }
</style>
<?php flash() ?>
<div class="box">
    <form action="" method="post">
        <div class="box-header">
            <h3 class="box-title btn-block">
                Route | <?= segment(4) ? 'Edit' : 'Add' ?>
                <div class="pull-right">
                    <a href="<?= site_url('admin/fleet_manager/add_update/' . $_GET['fleet_id'] . '#tab8') ?>" class="btn btn-primary">
                        <i class="fa fa-bars"></i> All Routes
                    </a>
                </div>
            </h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Start point</label>
                        <input id="map-start" type="text" class="form-control" name="title_1" value="<?= $isEdit ? $zone->title_1 : '' ?>" placeholder="Enter location" required="">
                        <span class="startLatLng">
                            <input type="hidden" data-geo="lat" name="lat_1" value="<?= $isEdit ? $zone->lat_1 : '' ?>">
                            <input type="hidden" data-geo="lng" name="lng_1" value="<?= $isEdit ? $zone->lng_1 : '' ?>">
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Start radius</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="radius_1" value="<?= $isEdit ? $zone->radius_1 : '0.50' ?>" required="">
                            <span class="input-group-addon"><?= DISTANCE ?></span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label>End point</label>
                        <input id="map-end" type="text" class="form-control" name="title_2" value="<?= $isEdit ? $zone->title_2 : '' ?>" placeholder="Enter location" required="">
                        <span class="endLatLng">
                            <input type="hidden" data-geo="lat" name="lat_2" value="<?= $isEdit ? $zone->lat_2 : '' ?>">
                            <input type="hidden" data-geo="lng" name="lng_2" value="<?= $isEdit ? $zone->lng_2 : '' ?>">
                        </span>
                    </div>
                    <div class="form-group">
                        <label>End radius</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="radius_2" value="<?= $isEdit ? $zone->radius_2 : '0.50' ?>" required="">
                            <span class="input-group-addon"><?= DISTANCE ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <div class="input-group">
                            <span class="input-group-addon"><?= CURRENCY ?></span>
                            <input type="text" class="form-control" name="rate" value="<?= $isEdit ? $zone->rate : '0.00' ?>" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Distance</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="rate_distance" value="<?= $isEdit ? $zone->rate_distance : '0.00' ?>" readonly="">
                            <span class="input-group-addon"><?= DISTANCE ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Valid for return</label>
                        <div class="input-group">
                            <input type="checkbox" name="type" value="two_way" <?= $isEdit && $zone->type == 'two_way' ? 'checked'  : '' ?>>
                        </div>
                    </div>
                    <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Save</button>
                </div>

                <div class="col-md-9">
                    <div class="form-group">
                        <input type="hidden" name="coordinates_1" value="<?= $isEdit ? $zone->coordinates_1 : '' ?>">
                        <input type="hidden" name="coordinates_2" value="<?= $isEdit ? $zone->coordinates_2 : '' ?>">
                        <label>Draw zone coverage area</label>
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </form>
</div><!-- /.box -->
<script src="<?= base_url('assets/Wicket/wicket.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/Wicket/wicket-gmap3.js') ?>" type="text/javascript"></script>
<script>
    var map;
    var circle = '';
    var marker = '';
    var coordinates_1 = <?= $isEdit ? "'" . $zone->coordinates_1 . "'" : 'null' ?>;

    var start_lat = <?= $isEdit ? "'" . $zone->lat_1 . "'" : 'null' ?>;
    var start_lng = <?= $isEdit ? "'" . $zone->lng_1 . "'" : 'null' ?>;
    var start_formatted_address = <?= $isEdit ? "'" . $zone->title_1 . "'" : 'null' ?>;
    var end_lat = <?= $isEdit ? "'" . $zone->lat_2 . "'" : 'null' ?>;
    var end_lng = <?= $isEdit ? "'" . $zone->lng_2 . "'" : 'null' ?>;
    var end_formatted_address = <?= $isEdit ? "'" . $zone->title_2 . "'" : 'null' ?>;

    $(function() {
        initMap();
        var start = $("#map-start"),
            end = $('#map-end');
        start.geocomplete({
            country: 'uk',
            details: ".startLatLng",
            detailsAttribute: "data-geo",
            types: ['geocode', 'establishment']
        }).bind("geocode:result", function(event, result) {
            start_lat = result.geometry.location.lat();
            start_lng = result.geometry.location.lng();
            start_formatted_address = result.formatted_address;

            drawRoute(start_formatted_address, start_lat, start_lng, end_formatted_address, end_lat, end_lng);

        });
        end.geocomplete({
            country: 'uk',
            details: ".endLatLng",
            detailsAttribute: "data-geo",
            types: ['geocode', 'establishment']
        }).bind("geocode:result", function(event, result) {
            end_lat = result.geometry.location.lat();
            end_lng = result.geometry.location.lng();
            end_formatted_address = result.formatted_address;
            drawRoute(start_formatted_address, start_lat, start_lng, end_formatted_address, end_lat, end_lng);
        });
        $('[name=radius_1],[name=radius_2]').change(function() {
            drawRoute(start_formatted_address, start_lat, start_lng, end_formatted_address, end_lat, end_lng);
        });
        if (<?= $isEdit ? 'true' : 'false' ?> === true) {
            drawRoute('<?= !empty($zone->title_1) ? $zone->title_1 : '' ?>', '<?= !empty($zone->lat_1) ? $zone->lat_1 : '' ?>', '<?= !empty($zone->lng_1) ? $zone->lng_1 : '' ?>', '<?= !empty($zone->title_2) ? $zone->title_2 : '' ?>', '<?= !empty($zone->lat_2) ? $zone->lat_2 : '' ?>', '<?= !empty($zone->lng_2) ? $zone->lng_2 : '' ?>');
        }
    });


    function initMap() {
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: {
                lat: 51.5074,
                lng: 0.1278
            },
            zoom: 6
        });
        // search_box(map);
    }

    function drawCircle(map, lat, lng, radius) {
        circle = {
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            map: map,
            center: {
                lat: parseFloat(lat),
                lng: parseFloat(lng)
            },
            radius: parseFloat(radius) // in meters
        };
        map = new google.maps.Circle(circle)
        // map.bindTo('center', marker, 'position');

        // google.maps.event.addListener(map, 'click', function(args) {
        //     console.log(args.latLng);
        // });
    }

    function drawRoute(start, start_lat, start_lng, end, end_lat, end_lng) {
        if (start == '' || typeof start == 'undefined') {
            start_lat = null;
            start_lng = null;
        }
        if (end == '' || typeof end == 'undefined') {
            end_lat = null;
            end_lng = null;
        }
        if (start_lat && start_lng && end_lat && end_lng) {
            if (marker) {
                marker.setMap(null);
            }
            mapsApiRoute(document.getElementById('map-canvas'), start, start_lat, start_lng, end, end_lat, end_lng);
            var coordinates = getPolygonCoordinates(start_lat, start_lng, (parseFloat($('[name=radius_1]').val()).toFixed(2)));
            $('[name=coordinates_1]').val(coordinates);
            var coordinates = getPolygonCoordinates(end_lat, end_lng, (parseFloat($('[name=radius_1]').val()).toFixed(2)));
            $('[name=coordinates_2]').val(coordinates);
        } else if (start_lat && start_lng || end_lat && end_lng) {
            if (start_lat && start_lng) {
                mapsApiMarker(document.getElementById('map-canvas'), start_lat, start_lng);
                drawCircle(map, start_lat, start_lng, (parseFloat($('[name=radius_1]').val() * 1609.34).toFixed(2)));
            } else if (end_lat && end_lat) {
                mapsApiMarker(document.getElementById('map-canvas'), end_lat, end_lng);
                drawCircle(map, end_lat, end_lng, (parseFloat($('[name=radius_2]').val() * 1609.34).toFixed(2)));
            }
        }
    }

    function mapsApiMarker(map_div, lat, lng) {

        var myLatLng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        };
        map = new google.maps.Map(map_div, {
            center: myLatLng,
            zoom: 10
        });
        marker = new google.maps.Marker({
            map: map,
            position: myLatLng
        });
    }

    function mapsApiRoute(map_div, start, start_lat, start_lng, end, end_lat, end_lng) {
        var pointA = new google.maps.LatLng(parseFloat(start_lat), parseFloat(start_lng)),
            pointB = new google.maps.LatLng(parseFloat(end_lat), parseFloat(end_lng)),
            myOptions = {
                zoom: 7,
                center: pointA
            },
            map = new google.maps.Map(map_div, myOptions),

            directionsService = new google.maps.DirectionsService,
            directionsDisplay = new google.maps.DirectionsRenderer({
                map: map
                // }),
                // markerA = new google.maps.Marker({
                //     position: pointA,
                //     title: start,
                //     label: "A",
                //     map: map
                // }),
                // markerB = new google.maps.Marker({
                //     position: pointB,
                //     title: end,
                //     label: "B",
                //     map: map
            });

        // get route from A to B
        calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB);
        drawCircle(map, start_lat, start_lng, (parseFloat($('[name=radius_1]').val() * 1609.34).toFixed(2)));
        drawCircle(map, end_lat, end_lng, (parseFloat($('[name=radius_2]').val() * 1609.34).toFixed(2)));
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
        directionsService.route({
            origin: pointA,
            destination: pointB,
            avoidTolls: true,
            avoidHighways: false,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var distance = parseFloat((response.routes[0].legs[0].distance.value) * 0.000621371).toFixed(2);
                console.log(distance);
                $('[name=rate_distance]').val(distance);
                directionsDisplay.setDirections(response);

            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }

    function getPolygonCoordinates(lat, lng, radius) {
        var dir = 1;
        var lat = parseFloat(lat);
        var lng = parseFloat(lng);
        var point = new google.maps.LatLng(lat, lng);
        var d2r = Math.PI / 180; // degrees to radians 
        var r2d = 180 / Math.PI; // radians to degrees 
        var earthsradius = 3963; // 3963 is the radius of the earth in miles

        var points = 64;
        if (radius >= 100 && radius < 200) {
            points = 512;
        } else if (radius >= 200 && radius < 300) {
            points = 1024;
        } else if (radius >= 300 && radius < 400) {
            points = 2048;
        } else if (radius >= 400 && radius < 500) {
            points = 4096;
        } else if (radius > 500) {
            points = 8192;
        }


        // find the raidus in lat/lon 
        var rlat = (radius / earthsradius) * r2d;
        var rlng = rlat / Math.cos(point.lat() * d2r);

        var extp = new Array();
        if (dir == 1) {
            var start = 0;
            var end = points + 1; // one extra here makes sure we connect the path
        } else {
            var start = points + 1;
            var end = 0;
        }
        var POLYGON = 'POLYGON((';
        for (var i = start;
            (dir == 1 ? i < end : i > end); i = i + dir) {
            var theta = Math.PI * (i / (points / 2));
            ey = point.lng() + (rlng * Math.cos(theta)); // center a + radius x * cos(theta) 
            ex = point.lat() + (rlat * Math.sin(theta)); // center b + radius y * sin(theta) 
            POLYGON += ey + ' ' + ex + ',';
            // extp.push(new google.maps.LatLng(ex, ey));
        }
        POLYGON = POLYGON.slice(0, -1);
        POLYGON += '))';
        return POLYGON;
    }
</script>