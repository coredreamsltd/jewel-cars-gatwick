<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Custom Rate Manager</h3>
            </div><!-- /.box-header -->
            <div class="box-body custom-rate-wrapper bg-purple">
                <?php flash() ?>
                <div class="row">
                    <div class="col-md-3">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active"><a href="#route" data-toggle="tab">Routes</a></li>
                                <li><a href="#add-route" data-toggle="tab">Add route</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="route">
                                    <h3>Your Routes</h3>

                                    <ul class="list-unstyled route-list">
                                        <li>
                                            <p><span>Buckingham Palace Road, London SW1W 9SH, UK</span> - <span>London SW11, UK</span></p>
                                            <a href="#!" class="btn btn-warning"> <i class="fa fa-pencil"> </i> Edit</a>
                                            <a href="#!" class="btn btn-danger"> <i class="fa fa-trash"></i> Delete</a>
                                        </li>
                                        <li>
                                            <p><span>Buckingham Palace Road, London SW1W 9SH, UK</span> - <span>London SW12, UK</span></p>
                                            <a href="#!" class="btn btn-warning"> <i class="fa fa-pencil"> </i> Edit</a>
                                            <a href="#!" class="btn btn-danger"> <i class="fa fa-trash"></i> Delete</a>
                                        </li>
                                        <li>
                                            <p><span>Buckingham Palace Road, London SW1W 9SH, UK</span> - <span>London SW13, UK</span></p>
                                            <a href="#!" class="btn btn-warning"> <i class="fa fa-pencil"> </i> Edit</a>
                                            <a href="#!" class="btn btn-danger"> <i class="fa fa-trash"></i> Delete</a>
                                        </li>
                                        <li>
                                            <p><span>Buckingham Palace Road, London SW1W 9SH, UK</span> - <span>London SW14, UK</span></p>
                                            <a href="#!" class="btn btn-warning"> <i class="fa fa-pencil"> </i> Edit</a>
                                            <a href="#!" class="btn btn-danger"> <i class="fa fa-trash"></i> Delete</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="add-route">
                                    <h3>Add route</h3>

                                    <form>
                                        <div class="form-group">
                                            <label>Start point</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Start radius</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>End point</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>End radius</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" value=""> Return Available</label>
                                        </div>
                                        <button class="btn btn-success btn-block">Save</button>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <div class="col-md-9">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d317715.7119369244!2d-0.38178583985784054!3d51.528735196048615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C%20UK!5e0!3m2!1sen!2snp!4v1587387272892!5m2!1sen!2snp" width="100%" height="700px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->