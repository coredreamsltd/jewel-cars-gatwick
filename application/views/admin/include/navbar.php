<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?php if (is_admin_menu_accessible(2)) : ?>
                <li class="treeview <?= in_array(segment(2), ['booking', 'option']) ? "active" : "" ?>">
                    <a href="#">
                        <i class="fa fa-book"></i> <span>Booking Manager</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo segment(3) == 'booking' ? "active" : "" ?>"><a href="<?php echo site_url('admin/booking') ?>"><i class="fa fa-file-o"></i>View Booking</a></li>

                        <li class="<?php echo segment(3) == 'add-booking' ? "active" : "" ?>"><a href="<?php echo site_url('admin/booking/quote') ?>"><i class="fa fa-plus"></i> Add Booking</a></li>

                        <li class="<?php echo segment(3) == 'yearly-booking' ? "active" : "" ?>"><a href="<?php echo site_url('admin/booking/yearly_booking') ?>"><i class="fa fa-eye"></i> Yearly Booking</a></li>

                    </ul>
                </li>
            <?php endif; ?>

            <?php if (is_admin_menu_accessible(3)) : ?>
            <li class="treeview <?= in_array(segment(2), ['content', 'option','faqs']) ? "active" : "" ?>">
                <a href="#">
                    <i class="fa fa-files-o"></i> <span>Content Manager</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo segment(3) == 'pages' ? "active" : "" ?>"><a href="<?php echo site_url('admin/content/pages') ?>"><i class="fa fa-file-o"></i>Page
                            Manager</a></li>
                    <!-- <li class="<?php echo segment(3) == 'banners' ? "active" : "" ?>"><a href="<?php echo site_url('admin/content/banners') ?>"><i class="fa fa-picture-o"></i>
                                Banner Manager</a></li> -->
                    <li class="<?php echo segment(2) == 'faqs' ? "active" : "" ?>"><a href="<?php echo site_url('admin/faqs') ?>"><i class="fa fa-question-circle"></i> FAQ Manager</a></li>
                    <li class="<?php echo segment(3) == 'testimonials' ? "active" : "" ?>"><a href="<?php echo site_url('admin/content/testimonials') ?>"><i class="fa fa-quote-right"></i> Testimonial Manager</a></li>
                    <li class="<?php echo segment(3) == 'email-template' ? "active" : "" ?>"><a href="<?php echo site_url('admin/content/email-template') ?>"><i class="fa fa-file-code-o"></i> Email Templates</a></li>

                </ul>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(4)) : ?>
            <li class="<?php echo segment(2) == 'fleet-manager' || segment(2) == 'fleet_manager' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/fleet-manager') ?>">
                    <i class="fa fa-car"></i> <span>Fleet Manager</span>
                </a>
            </li>
            <?php endif; ?>

            <?php if (is_admin_menu_accessible(5)) : ?>
            <li class="<?php echo segment(2) == 'rate' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/rate') ?>">
                    <i class="fa fa-gbp"></i> <span>Rate Manager</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(8)) : ?>
            <li class="<?php echo segment(2) == 'location' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/location/categories') ?>">
                    <i class="fa fa-globe"></i> <span>Location Manager</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(8)) : ?>
            <li class="<?php echo segment(2) == 'specific-location' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/specific-location') ?>">
                    <i class="fa fa-map-pin"></i> <span>Specific Location Manager</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(7)) : ?>
            <li class="<?php echo segment(2) == 'driver' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/driver') ?>"><i class="fa fa-user-secret"></i>
                    <span>Driver Manager</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(6)) : ?>
            <li class="<?php echo segment(2) == 'passenger' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/passenger') ?>">
                    <i class="fa fa-users"></i> <span>Passenger Manager</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(10)) : ?>
            <li class="<?php echo segment(2) == 'invoice' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/invoice') ?>">
                    <i class="fa fa-file-text"></i> <span>Invoice Manager</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(9)) : ?>
            <li class="<?php echo segment(2) == 'discounts' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/discounts/coupons') ?>">
                    <i class="fa fa-gift"></i> <span>Discount Manager</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(9)) : ?>
            <li class="<?php echo segment(2) == 'discounts' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/reports') ?>">
                    <i class="fa fa-truck"></i> <span>Booking Report</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(11)) : ?>
            <li class="<?php echo segment(2) == 'seo' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/seo') ?>"><i class="fa fa-bars"></i>
                    <span>SEO Manager</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(13)) : ?>
            <li class="<?= segment(2) == 'system-user' ? 'active' : '' ?>">
                <a href="<?= site_url('admin/system-user') ?>"><i class='fa fa-user'></i>
                    <span>System User</span>
                </a>
            </li>
            <?php endif; ?>
            <?php if (is_admin_menu_accessible(3)) : ?>
            <li class="<?php echo segment(2) == 'option' ? "active" : "" ?>">
                <a href="<?php echo site_url('admin/option') ?>">
                    <i class="fa fa-cogs"></i> <span>Settings</span>
                </a>
            </li>
            <?php endif; ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>