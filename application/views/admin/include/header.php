<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin|Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/bootstrap/css/bootstrap.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
      <!-- Jquery UI -->
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/bower_components/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/bower_components/fullcalendar-scheduler/scheduler.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/dist/css/AdminLTE.min.css">
    <!--    stylecss-->
    <link rel="stylesheet" href="<?= base_url('assets/admin/css/style.css') ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/dist/css/skins/_all-skins.min.css">
    <!-- jquery timepicker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/jt.timepicker/jquery.timepicker.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?= base_url('assets/admin') ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/bootstrap-sweetalert-master/dist/sweetalert.css') ?>">
    <!-- font awesome picker -->
    <link href="<?php echo base_url('assets/bower_components/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css') ?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/css/intlTelInput.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/noty.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/themes/bootstrap-v3.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/themes/light.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/themes/metroui.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/themes/mint.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/themes/nest.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/themes/relax.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/themes/semanticui.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/noty-master/lib/themes/sunset.css') ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/dark.css">
    <style>
        .intl-tel-input {
            width: 100%;
        }

        .intl-tel-input .country-list {
            z-index: 3;
        }
    </style>
    <!-- jQuery 2.2.3 -->
    <script src="<?= base_url('assets/admin') ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!--    <script src="-->
    <? //= base_url('assets/admin') 
    ?>
    <!--/bootstrap/js/bootstrap.min.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/ckfinder/ckfinder.js"></script>
    <script src="<?= base_url('assets/bower_components/noty-master/lib/noty.min.js') ?>"></script>
    <!--    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>-->

    <!--    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!--    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->


</head>

<body class="hold-transition skin-blue sidebar-mini <?=segment(2)=='booking'?'sidebar-collapse':''?>">
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">J</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">  <img src="<?= base_url('assets/images/logo.png') ?>" width="50"></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= base_url('assets/admin') ?>/dist/img/boxed-bg.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo ucfirst($username); ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= base_url('assets/admin') ?>/dist/img/boxed-bg.jpg" class="img-circle" alt="User Image">

                                    <p>
                                        Welcome <?php echo ucfirst($username); ?>
                                        <small><?php echo date('d M Y', time()) ?></small>
                                    </p>
                                </li>

                                <!-- Menu Body -->

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <?php if($this->session->userdata['account_type'] != 3) :?>
                                        <div class="pull-left">
                                            <a href="<?php echo site_url('admin/config') ?>" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                    <?php endif; ?>
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('admin/index/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>

                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <?php if($this->session->userdata['account_type'] == 1) :?>
                        <li>
                            <a href="<?= site_url('admin/option') ?>"><i class="fa fa-gears"></i></a>
                        </li>
                        <?php endif; ?>

                    </ul>
                </div>
            </nav>
        </header>

        <div class="wrapper row-offcanvas row-offcanvas-left">