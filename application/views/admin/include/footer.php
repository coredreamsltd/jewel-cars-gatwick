<footer class="main-footer">
    <div class="pull-right hidden-xs">

    </div>
    <strong>Copyright &copy; <?php echo date('Y') ?> .</strong> All rights reserved.
</footer>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<!-- <script src="<?= base_url('assets/admin') ?>/plugins/jQuery/jquery-2.2.3.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- <script src="<?= base_url() ?>assets/admin/bower_components/js-cookie/src/js.cookie.js" type="text/javascript"></script> -->
<script src="<?= base_url() ?>/assets/bower_components/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url('assets/admin') ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- Datatables -->
<link href="<?= base_url('assets/bower_components/datatables/media/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
<script src="<?= base_url('assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables/media/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- Sparkline -->
<script src="<?= base_url('assets/admin') ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= base_url('assets/admin') ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url('assets/admin') ?>/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= base_url('assets/admin') ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url('assets/admin') ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= base_url('assets/admin') ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/admin') ?>/plugins/fastclick/fastclick.js"></script>
<!-- jquery timepicker -->
<script src="<?= base_url() ?>assets/bower_components/jt.timepicker/jquery.timepicker.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/admin') ?>/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="<?= base_url('assets/admin') ?>/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/admin') ?>/dist/js/demo.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?= base_url('assets/admin') ?>/plugins/chartjs/Chart.min.js"></script>

<!--Geo Location-->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places,drawing,geometry&key=<?= MAP_API_KEY ?>"></script>
<script src="<?= base_url() ?>assets/bower_components/geocomplete/jquery.geocomplete.min.js"></script>
<!-- font awesome picker -->
<script src="<?= base_url() ?>assets/bower_components/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.js"></script>
<!-- Bower components -->
<!-- credit card validator -->
<script src="<?= base_url() ?>assets/admin/bower_components/jquery-creditcardvalidator/jquery.creditCardValidator.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/fullcalendar/lib/moment.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/fullcalendar/fullcalendar.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/fullcalendar-scheduler/scheduler.min.js"></script>
<script src="<?php echo base_url('assets/bower_components/bootstrap-sweetalert-master/dist/sweetalert.min.js') ?>"></script>
<script>
    $(document).ready(function() {
        $('#dt-table').dataTable();
        $('.dt-tables').dataTable();
        $('.datepicker').datepicker({
            startDate: new Date(),
            format: 'dd/mm/yyyy',
            todayHighlight: true

        });
        $('.all-datepicker').datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true

        });
        $('.timepicker').timepicker();
        var start = $("#start"),
            end = $('#end');
        start.geocomplete({
            country: 'uk',
            details: ".start",
            detailsAttribute: "data-geo",
            types: ['geocode', 'establishment']
        });
        end.geocomplete({
            country: 'uk',
            details: ".end",
            detailsAttribute: "data-geo",
            types: ['geocode', 'establishment']
        });

    });
</script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
</body>

</html>