<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= (isset($seo->title)) ? $seo->title : '' ?></title>
    <meta name="description" content="<?= (isset($seo->description)) ? $seo->description : '' ?>">
    <meta name="keywords" content="<?= (isset($seo->keywords)) ? $seo->keywords : '' ?>">


    <link href="<?= base_url() ?>assets/bower_components/md-bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/bower_components/md-bootstrap/css/mdb.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/fonts/font.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datetimepicker-master/jquery.datetimepicker.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/dark.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/jt.timepicker/jquery.timepicker.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/intl-tel-input-master/build/css/intlTelInput.css">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/bootstrap-sweetalert-master/dist/sweetalert.css') ?>">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/master.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css">

    <script src="<?= base_url() ?>assets/bower_components/md-bootstrap/js/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<body>
    <header>
        <div class="container" id="booking">
            <div class="row">
                <div class="col-md-3">
                    <figure class="logo">
                        <a href="<?= site_url() ?>"> <img src="<?= base_url() ?>assets/images/logo.png"></a>
                    </figure>
                </div>
                <div class="col-md-9">
                    <div class="nav-right">
                        <p class="account_option">
                            <?php if ($passenger) : ?>
                                <a href="<?= site_url('accounts/passenger/profile') ?>"><i class="fas fa-user-circle"></i> <strong>Hi! <?= $passenger->full_name ?></strong></a> &nbsp;&nbsp;
                            <?php else : ?>
                                <a href="<?= site_url('passenger/login') ?>"><i class="fas fa-user-circle"></i> <strong>Passenger login</strong></a> &nbsp;&nbsp;
                            <?php endif; ?>
                            <?php if ($driver) : ?>
                                <a href="<?= site_url('accounts/driver/profile') ?>"><i class="fas fa-user"></i> <strong>Hi! <?= $driver->name ?></strong></a> &nbsp;&nbsp;
                            <?php else : ?>
                                <a href="<?= site_url('driver/login') ?>"><i class="fas fa-user"></i> <strong>Driver login</strong></a> &nbsp;&nbsp;
                            <?php endif; ?>
                            <?= SITE_NAME ?>
                        </p>
                        <div class="cta">
                            <a href="tel:<?= SITE_NUMBER_1 ?>">
                                <img src="<?= base_url() ?>assets/images/phone.svg"> <?= SITE_NUMBER_1 ?>
                            </a>
                            <a href="#tel:<?= SITE_NUMBER ?>">
                                <img src="<?= base_url() ?>assets/images/phone2.svg"> <?= SITE_NUMBER ?>
                            </a>
                        </div>
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                 <ul class="navbar-nav mr-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= site_url('') ?>">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= site_url('school-transport#booking') ?>">School Transport</a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            Business
                                        </a>
                                        <a href="javascript:void(0)" class="cstm-drop">
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <?php if (!empty($business_template_data)) : foreach ($business_template_data as $business) : ?>
                                                    <a class="dropdown-item" href="<?= site_url($business->slug.'#booking') ?>"><?= $business->name ?></a>
                                            <?php endforeach;
                                            endif; ?>
                                        </div>
                                    </li>


                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= site_url('about#booking') ?>">About Us</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= site_url('faq#booking') ?>">FAQ's</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= site_url('reviews#booking') ?>">Reviews</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= site_url('work-with-us#booking') ?>">Work With Us</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= site_url('contact#booking') ?>">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="full-page-loader" style="display: none;">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="25 25 50 50">
                <circle cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke="#45d6b5" stroke-linecap="round" stroke-dashoffset="0" stroke-dasharray="100, 200">
                    <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 50 50" to="360 50 50" dur="2.5s" repeatCount="indefinite" />
                    <animate attributeName="stroke-dashoffset" values="0;-30;-124" dur="1.25s" repeatCount="indefinite" />
                    <animate attributeName="stroke-dasharray" values="0,200;110,200;110,200" dur="1.25s" repeatCount="indefinite" />
                </circle>
            </svg>
        </div>
    </header>

    <!-- <div class="head-banner" style="display: none;">
        <div class="container">
            <h4><i class="fa fa-info-circle"></i> COVID-19 Response </h4>
            <P>Jewel Cars gatwick continue to provide our professional transport services, with additional measures in place to ensure you get to your destination safely.</P>
            <p><a href="<?= site_url('coronavirus') ?>">More Information</a></p>
            <a href="#!" class="close" onclick="closeCovidCookies()"><i class="fa fa-times"></i></a>
        </div>
    </div> -->