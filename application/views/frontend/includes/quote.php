<div class="widget" id="quote-component">
    <h2>
		<span v-if="screen === 'quote-form'">Free Instant Online Quotes</span>
      	<span v-else>Book Online</span>
	</h2>
    <i class="fa fa-arrow-left text-white" title="Back" style="cursor: pointer;" v-if="screenType" v-on:click="editScreen(screenType)"></i>
    <div class="advice info d-none"></div>
    <div class="advice error d-none"></div>
    <div class="advice success d-none"></div>
    <?php $this->load->view('frontend/quote/quote-form') ?>
    <?php $this->load->view('frontend/quote/vehicle-selection') ?>
    <?php $this->load->view('frontend/quote/booking') ?>
    <?php $this->load->view('frontend/quote/booking-two-way') ?>
    <?php $this->load->view('frontend/quote/confirm') ?>
    <?php $this->load->view('frontend/quote/login-register') ?>
    <?php $this->load->view('frontend/quote/reset-password') ?>
</div>
<script type="text/javascript" src="https://cc-cdn.com/generic/scripts/v1/cc_c2a.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/maplace-js/0.2.10/maplace.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places,drawing,geometry&key=<?= MAP_API_KEY ?>"></script>
<script>
    var SITE_URL = '<?= site_url() ?>';
    var BASE_URL = '<?= base_url() ?>';
    var coupon_discount_code = '<?= getSession('discount')['code'] ?>';
    var coupon_discount_amount = '<?= getSession('discount')['amount'] ?>';
    var screen = '<?= getSession('screen') ?>';
    var journey = <?= getSession('journey') ?: 0 ?>;
    var journey_type = '<?= getSession('journey_type') ?>';
    var journeys = JSON.parse(JSON.stringify(<?= json_encode(getSession('journeys')) ?>));
    var additional_rate = JSON.parse('<?= $additional_rate ? json_encode($additional_rate) : 'null' ?>');
    var passenger = JSON.parse('<?= $passenger ? json_encode($passenger) : 'null' ?>');
    var meet_and_greet_page = JSON.parse(JSON.stringify(<?= json_encode(getSession('meet_and_greet') ? getSession('meet_and_greet') : 'null') ?>));
    // console.log('------', additional_rate, '------');
    $(function() {
        var new_vue = new Vue({
            el: "#quote-component",
            data: {
                bookings: (typeof(journeys) != "undefined" && journeys !== null) ? journeys : {
                    one_way: {
                        quote: {},
                        fleets: [],
                        selected_fleet: {},
                        booking_details: {},
                        grand_total_charge: {}
                    },
                    two_way: {
                        quote: {},
                        fleets: [],
                        selected_fleet: {},
                        booking_details: {},
                        grand_total_charge: {}
                    }
                },
                booking: {
                    // lat: this.booking[this.booking.journey_type].quote.start_lat,
                    one_way: {
                        quote: (journeys[journey]['one_way'].quote) ? journeys[journey]['one_way'].quote : {},
                        fleets: (journeys[journey]['one_way'].fleets) ? journeys[journey]['one_way'].fleets : [],
                        selected_fleet: (journeys[journey]['one_way'].selected_fleet) ? journeys[journey]['one_way'].selected_fleet : {},
                        booking_details: (journeys[journey]['one_way'].booking_details) ? journeys[journey]['one_way'].booking_details : {},
                        grand_total_charge: (journeys[journey]['one_way'].grand_total_charge) ? journeys[journey]['one_way'].grand_total_charge : {}
                    },
                    two_way: {
                        quote: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].quote : {},
                        fleets: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].fleets : [],
                        selected_fleet: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].selected_fleet : {},
                        booking_details: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].booking_details : {},
                        grand_total_charge: (journeys[journey]['two_way']) ? journeys[journey]['two_way'].grand_total_charge : {}
                    },
                    journey_type: (journeys[journey].journey_type) ? journeys[journey].journey_type : 'one_way',
                },
                // fleets: [],
                form_submit_status: false,
                journey: journey,
                journey_type: journey_type,
                screen: screen,
                additional_rate: additional_rate,
                grand_total: 0,
                coupon_discount_code: coupon_discount_code,
                coupon_discount_amount: coupon_discount_amount,
                grand_total_after_discount: 0,
                login: {
                    email: '',
                    password: '',
                },
                register: {
                    fname: '',
                    lname: '',
                    email: '',
                    tel_phone_cc: '',
                    tel_phone_no: '',
                    phone_cc: '',
                    phone_no: '',
                    address: '',
                    address_1: '',
                    city: '',
                    post_code: '',
                    password: '',
                },
                passenger: passenger,
                show_basket: false,
                accept_terms: false,
                isDisabledBookingForm: false,
                pay_method: 'card',
                baby_ages: ['0 to 8 Months', '9 to 15 Months', '16 to 24 Months', '25 to 36 Months', '37 to 40 Months', 'Over 40 months'],
                max_fields: 5, //maximum input boxes allowed
                x: 0,
                count: 0, //initlal text box count
                screenType: '',
                meet_and_greet: meet_and_greet_page,
                map_locations: [],
            },
            methods: {
                getQuote: function() {
                    var _this = this;
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    if (!$('#quote-form').valid()) {
                        return false;
                    }
                    var quote_data = objectifyForm($('#quote-form').serializeArray());
                    quote_data.journey = journey;
                    this.booking.journey_type.quote = quote_data;
                    // var is_valid = this.quoteFormValidate();
                    // console.log('is_valid'+is_valid);
                    // if (!is_valid) {
                    //     return false;
                    // }
                    this.form_submit_status = true;
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait, calculating fares...  <i class="fa fa-cog fa-spin pull-right"></i></a>');
                    $.ajax({
                        type: "POST",
                        url: SITE_URL + "/quote/ajaxGetQuote",
                        data: quote_data,
                        dataType: 'json',
                        success: function(response) {
                            if (!response.status) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            location.replace("<?= site_url('#booking') ?>");
                            location.reload();
                        }
                    });
                },
                selectVehicle: function() {
                    var _this = this;
                    var journey_type = $('[name=journey_type]:checked').val();
                    var fleet_id = $('[name=journey_type]:checked').data('fleet-id');
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    if (!journey_type || !fleet_id) {
                        $('.advice.error').html('<div class="alert alert-danger">Please at least select one vehicle.</div>');
                        $('.advice.error').removeClass('d-none');
                        return;
                    }
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait, calculating fares...  <i class="fa fa-cog fa-spin pull-right"></i></a>');
                    $.ajax({
                        url: '<?= site_url('quote/ajaxBooking') ?>',
                        method: 'POST',
                        data: {
                            fleet_id: fleet_id,
                            journey_type: journey_type
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status !== true) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            location.reload();
                        }
                    });
                },
                submitOneBooking: function() {
                    if (!$('#booking-info-form').valid()) {
                        return false;
                    }
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait.....  <i class="fa fa-cog fa-spin pull-right"></i></a>');
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    var booking_data = objectifyForm($('#booking-info-form').serializeArray());
                    booking_data.coupon_discount_code = this.coupon_discount_code;
                    $.ajax({
                        url: '<?= site_url('quote/oneWayBookingInfo') ?>',
                        method: 'POST',
                        data: booking_data,
                        dataType: 'json',
                        success: function(response) {
                            if (!response.status) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            location.reload();
                        }
                    });

                },
                submitTwoBooking: function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    if (!$('#booking-info-form-two').valid()) {
                        return false;
                    }
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait.....  <i class="fa fa-cog fa-spin pull-right"></i></a>');

                    var booking_data = objectifyForm($('#booking-info-form-two').serializeArray());
                    // alert('hell yhaa!');
                    $.ajax({
                        url: '<?= site_url('quote/twoWayBookingInfo') ?>',
                        method: 'POST',
                        data: booking_data,
                        dataType: 'json',
                        success: function(response) {
                            if (response.status !== true) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            location.reload();
                        }
                    });

                },
                payNow: function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    $('.full-page-loader').show();
                    var that = this;
                    var bookings = $.parseJSON(JSON.stringify(this.bookings));
                    var booking_data = [];
                    $.each(bookings, function(i, booking) {
                        delete booking.one_way.fleets;
                        delete booking.two_way.fleets;
                        // console.log('----------', booking, '-----------');
                        booking_data[i] = booking;
                    });


                    $.ajax({
                        url: '<?= site_url('quote/finish') ?>',
                        method: 'POST',
                        data: {
                            booking_data: booking_data,
                            grand_total: that.grand_total,
                            coupon_discount_code: that.coupon_discount_code,
                            coupon_discount_amount: that.coupon_discount_amount,
                            pay_method: that.pay_method,
                        },
                        dataType: 'json',
                        success: function(response) {
                            $('.full-page-loader').hide();
                            if (response.status !== true) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            // console.log(response);
                            // console.log(response.data.is_finish != undefined);

                            if (response.data.is_finish != undefined) {
                                location.href = response.data.redirect_url;
                            } else {
                                location.replace(response.data.redirect_url);
                                location.reload();
                            }
                        }
                    });

                },
                logIn: function() {
                    if (!$('#login-form').valid()) {
                        return false;
                    }
                    var email = this.login.email;
                    var password = this.login.password;

                    if (!email || !password) {
                        swal({
                            title: "Error",
                            text: "Email and password fields must no be empty.",
                            type: 'warning',
                            showConfirmButton: true,
                            timer: 3000
                        });
                        return;
                    }

                    if (this.isEmail(email) == false) {
                        swal({
                            title: "Error",
                            text: "Invalid email address format.",
                            type: 'warning',
                            showConfirmButton: true,
                            timer: 3000
                        });
                        return false;
                    }

                    $.ajax({
                        url: '<?= site_url('accounts/index/ajax_login') ?>',
                        method: 'POST',
                        data: {
                            email: email,
                            password: password
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status !== true) {
                                $('.advice.error').html('<p>Error: Details not recognised: Please ensure you have entered your credentials correctly. If you are using us for the first time, please fill in the fields under New Customer instead...</p>');
                                $('.advice.error').removeClass('d-none');
                                // swal({
                                //     title: "Error",
                                //     text: response.msg,
                                //     type: 'warning',
                                //     showConfirmButton: true,
                                //     timer: 3000
                                // });
                                return;
                            }
                            window.location.href = '#booking';
                            location.reload();
                        }
                    });
                },
				resetPassword: function() {
                    if (!$('#reset-password-form').valid()) {
                        return false;
                    }
                    var email = this.login.email;

                    if (!email) {
                        swal({
                            title: "Error",
                            text: "Email field must no be empty.",
                            type: 'warning',
                            showConfirmButton: true,
                            timer: 3000
                        });
                        return;
                    }

                    if (this.isEmail(email) == false) {
                        swal({
                            title: "Error",
                            text: "Invalid email address format.",
                            type: 'warning',
                            showConfirmButton: true,
                            timer: 3000
                        });
                        return false;
                    }

                    $.ajax({
                        url: '<?= site_url('accounts/index/ajax_reset_password') ?>',
                        method: 'POST',
                        data: {
                            email: email
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status !== true) {
                                $('.advice.error').html('<p>'+(response?.msg??'')+'</p>');
                                $('.advice.error').removeClass('d-none');
                                // swal({
                                //     title: "Error",
                                //     text: response.msg,
                                //     type: 'warning',
                                //     showConfirmButton: true,
                                //     timer: 3000
                                // });
                                return;
                            }
							 swal({
                                    title: "SUccess",
                                    text: response.msg,
                                    type: 'success',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                            window.location.href = '#booking';
                           setTimeout(function(){
							location.reload();
						   },1500);
                        }
                    });
                },
                newRegister: function() {
                    if (!$('#register-form').valid()) {
                        return false;
                    }
                    var that = this;
                    if (!$('#register-form').valid()) {
                        return;
                    }
                    // if (this.isEmail(register.email) == false) {
                    //     swal({
                    //         title: "Error",
                    //         text: "Invalid email address format.",
                    //         type: 'warning',
                    //         showConfirmButton: true,
                    //         timer: 3000
                    //     });
                    //     return false;
                    // }
                    this.register.tel_phone_cc = '+44';
                    this.register.phone_cc = '+44';
                    $.ajax({
                        url: '<?= site_url('accounts/index/ajax_register') ?>',
                        method: 'POST',
                        data: that.register,
                        dataType: 'json',
                        success: function(response) {
                            if (response.status !== true) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            location.reload();
                        }
                    });
                },
                editScreen: function(screen, journey = null, journey_type = 'one_way') {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                    $('.button-inline').html('<a href="javascript:void(0)" class="btn btn-main btn-block"> Please wait....  <i class="fa fa-cog fa-spin pull-right"></i></a>');
                    $.ajax({
                        type: "POST",
                        url: SITE_URL + "/quote/editScreen",
                        data: {
                            screen: screen,
                            journey: journey,
                            journey_type: journey_type,
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (!response.status) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            location.reload();
                        }
                    });
                },
                toggleIsBabySeat: function(journey_type, event) {

                    if (event.target.checked) {
                        this.booking[journey_type].booking_details.is_baby_seat = true;
                    } else {
                        this.booking[journey_type].booking_details.is_baby_seat = false;
                    }
                },
                toggleChildSeat: function(journey_type, type, event) {
                    if (event.target.checked) {
                        this.booking[journey_type].booking_details[type] = true;
                    } else {
                        this.booking[journey_type].booking_details[type] = false;
                    }
                    // console.log(this.booking[journey_type].booking_details);
                },
                isEmail: function(email) {
                    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (!regex.test(email)) {
                        return false;
                    } else {
                        return true;
                    }
                },
                quoteFormValidate: function() {
                    var quote = $.parseJSON(JSON.stringify(this.booking[this.journey_type].quote));
                    var quote_error = 0;
                    var req_params = ['start', 'start_lat', 'start_lng', 'start_post_code', 'end', 'end_lat', 'end_lng', 'end_post_code'];

                    $.each(req_params, function(r_key, req) {
                        if (quote[req] == '') {
                            $('[name=' + req + ']').next('.error').text('This field is required.');
                            ++quote_error;
                        } else {
                            $('[name=' + req + ']').next('.error').text('');
                        }
                    });

                    if (quote_error > 0) {
                        return false;
                    }

                    return true;
                },
                calculateTotalFare: function() {
                    var that = this;
                    if (this.bookings) {
                        $(this.bookings).each(function(i, booking) {
                            if (booking.one_way.selected_fleet) {
                                that.grand_total = parseInt(parseFloat(that.grand_total) + parseFloat(booking.one_way.selected_fleet.fare) + parseFloat(booking.journey_type == 'two_way' ? booking.two_way.selected_fleet.fare : 0));
                            }
                        });
                    }
                },
                applyDiscount: function() {
                    var that = this;
                    $.ajax({
                        url: '<?= site_url('quote/processDiscount') ?>',
                        method: 'POST',
                        data: {
                            discount_code: that.coupon_discount_code,
                            grand_total: that.grand_total
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (!response.status) {
                                that.coupon_discount_code = '';
                                that.coupon_discount_amount = 0;
                                that.grand_total_after_discount = that.grand_total;
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            that.coupon_discount_amount = response.data.discount_amount;
                            that.grand_total_after_discount = response.data.grand_total_after_discount;
                        }
                    });

                },
                way_point: function(x) {
                    $(".way_point_address" + x).autocomplete({
                        autoFocus: 1,
                        minLength: 1,
                        source: function(request, response) {
                            $.ajax({
                                url: SITE_URL + "/quote/ajax_get_locations",
                                data: {
                                    q: request.term
                                },
                                dataType: "json",
                                success: function(data) {
                                    if (data.status == 0) {
                                        loadFechify("way_point_address" + x, x);
                                        return;
                                    }
                                    response(data.data);
                                },
                            });
                        },
                        select: function(event, ui) {
                            if (ui.item.id <= 0) {
                                loadFechify("way_point_address" + x, x);
                                return false;
                            }

                            $(".way_point_address" + x).val(ui.item.label + ', ' + ui.item.postcode);
                            $('.way_point' + x).find('.lat').val();
                            $('.way_point' + x).find('.lng').val();
                            $('.way_point' + x).find('.postcode').val(ui.item.postcode);
                            $('.way_point' + x).find('.city').val('');
                            $('.way_point' + x).find('.district').val('');
                            $('.way_point' + x).find('.country').val('');
                            return false;
                        },
                    });
                },
                loadData: function() {
                    var vm = this;
                    this.show_basket = this.bookings[0]['one_way'].booking_details.name ? true : false;
                    $(".all-datepicker").flatpickr();
                    $(".datetime-picker").flatpickr({
                        minDate: "today",
                        enableTime: true,
                        altInput: true,
                        time_24hr: true,
                        altFormat: "l j F, Y H:i",
                        dateFormat: "Y-m-d H:i",
                        position: 'below',
                        close: true,
                        onChange: function(selectedDates, dateStr, instance, timezone_offset_get) {
                            var datetime = new Date(selectedDates);
                            var cHour = datetime.getHours();
                            var cMin = datetime.getMinutes();
                            if (cHour == 0 && cMin == 0) {
                                html = '<li class="validation condition warning"><i class="fas fa-info-circle"></i> To avoid confusion, please enter 23:55 (on the previous day) or 00:05 instead</li>';
                                $('.date-time-validation .validation').html(html);
                            } else {
                                $('.date-time-validation .validation').html('');
                            }

                        },
                        onOpen: function(selectedDates, dateStr, instance) {
                            $(instance.timeContainer).append('<button class="flatpickr-close">Apply</button>');
                            $(instance.timeContainer).find('.flatpickr-close').css({
                                "background-color": "#ffca09",
                                "color": "#000",
                                "border": "0",
                                "border-radius": "0 0 5px 0",
                                "padding": "0 15px 0 15px",
                                "font-weight": "700",
                                "cursor": "pointer"
                            });
                        },
                        onClose: function(selectedDates, dateStr, instance) {
                            $(instance.timeContainer).find('.flatpickr-close').remove();
                            var datetime = new Date(selectedDates);
                            month = datetime.getMonth() + 1;
                            date = datetime.getDate();
                            $.ajax({
                                url: SITE_URL + "/quote/ajaxDateTimeCheck",
                                method: 'post',
                                data: {
                                    date: datetime.getFullYear() + '-' + (month > 9 ? month : `0` + month) + '-' + (date > 9 ? date : `0` + date),
                                    time: datetime.getHours() + ':' + (datetime.getMinutes() > 9 ? datetime.getMinutes() : `0` + datetime.getMinutes()),
                                },
                                dataType: "json",
                                success: function(response) {
                                    if (response.status == false) {
                                        vm.isDisabledBookingForm=true;
                                        swal({
                                            title: "Warning",
                                            html: true,
                                            text: response?.message ?? '',
                                            type: 'warning',
                                            showConfirmButton: true
                                        });
                                    }else{
                                        vm.isDisabledBookingForm=false;
                                    }
                                },
                            });
                        }
                    });

                    // duplicating alert calenders on different places
                    // $('body').on('click', '.datetime-picker', function() {
                    //     $('.flatpickr-calendar').addClass('open');
                    // });
                    $('body').on('click', '.flatpickr-close', function() {
                        $('.flatpickr-calendar').removeClass('open');
                        $('.datetime-picker').removeClass('active');
                    });
                    $('[name=pickup_date]').change(function() {
                        var start_date = $(this).val();
                        var arr = start_date.split('/');

                        $('[name=return_date]').val('');
                        $('[name=return_date]').datepicker('destroy');
                        $('[name=return_date]').removeAttr('disabled', '');
                        $('[name=return_date]').datepicker({
                            dateFormat: 'dd/mm/yy',
                            yearRange: '1999:2050',
                            minDate: new Date(arr[2], arr[1] - 1, arr[0])
                        });
                    });

                    $('.timepicker').timepicker({
                        timeFormat: 'H:i',
                        step: 5,
                        disableTouchKeyboard: true,
                    });

                    $('.start').keyup(function() {
                        $('.startLatLng').find('input').val('');
                    });

                    $('.end').keyup(function() {
                        $('.endLatLng').find('input').val('');
                    });

                    $.widget("app.autocomplete", $.ui.autocomplete, {
                        _renderItem: function(ul, item) {
                            var result = this._super(ul, item);
                            result.find("div").html('<i class="fa ' + item.logo + '"></i> <span class="auto-item-query">' + item.label + '</span>');
                            return result;
                        },
                    });

                    $(".start,.end").click(function() {
                        // $("#cc_c2a").hide();
                    });

                    $(".start").autocomplete({
                        autoFocus: 1,
                        minLength: 1,
                        source: function(request, response) {
                            $.ajax({
                                url: SITE_URL + "/quote/ajax_get_locations",
                                data: {
                                    q: request.term
                                },
                                dataType: "json",
                                success: function(data) {
                                    $(".start-loader").html('');
                                    if (data.status == 0) {
                                        loadFechify('start');
                                        return;
                                    }
                                    // $('#cc_c2a').hide();
                                    response(data.data);
                                },
                            });
                        },
                        search: function(event, ui) {
                            $("#ui-id-1").css("display", "none");
                            $('#cc_c2a').hide();
                            $(".start-loader").html('<div><p><i class="fa fa-spin fa-cog"></i>Finding Locations.....</p></div>');
                        },
                        select: function(event, ui) {
                            if (ui.item.id <= 0) {
                                loadFechify('start');
                                return false;
                            }

                            $(".start").val(ui.item.label);
                            $("[name=start_lat]").val('');
                            $("[name=start_lng]").val('');
                            $("[name=start_post_code]").val(ui.item.postcode);
                            $("[name=start_town_or_city]").val('');
                            $("[name=start_district]").val('');
                            $("[name=start_country]").val('');

                            vm.booking[vm.journey_type].quote.start = ui.item.label;
                            vm.booking[vm.journey_type].quote.start_lat = '';
                            vm.booking[vm.journey_type].quote.start_lng = '';
                            vm.booking[vm.journey_type].quote.start_post_code = ui.item.postcode;
                            vm.booking[vm.journey_type].quote.start_town_or_city = '';
                            vm.booking[vm.journey_type].quote.start_district = '';
                            vm.booking[vm.journey_type].quote.start_country = '';
                            return false;
                        },
                    });

                    $(".end").autocomplete({
                        autoFocus: 1,
                        minLength: 1,
                        source: function(request, response) {
                            $.ajax({
                                url: SITE_URL + "/quote/ajax_get_locations",
                                data: {
                                    q: request.term
                                },
                                dataType: "json",
                                success: function(data) {
                                    $(".end-loader").html('');
                                    if (data.status == 0) {
                                        loadFechify('end');
                                        return;
                                    }
                                    response(data.data);
                                },
                            });
                        },
                        search: function(event, ui) {
                            $("#ui-id-2").css("display", "none");
                            $('#cc_c2a').hide();
                            $(".end-loader").html('<div><p><i class="fa fa-spin fa-cog"></i>Finding Locations.....</p></div>');
                        },
                        select: function(event, ui) {
                            if (ui.item.id <= 0) {
                                loadFechify('end');
                                return false;
                            }

                            $(".end").val(ui.item.label);
                            vm.booking[vm.journey_type].quote.end = ui.item.label;

                            $("[name=end_lat]").val('');
                            $("[name=end_lng]").val('');
                            $("[name=end_post_code]").val(ui.item.postcode);
                            $("[name=end_town_or_city]").val('');
                            $("[name=end_district]").val('');
                            $("[name=end_country]").val('');

                            vm.booking[vm.journey_type].quote.end_lat = '';
                            vm.booking[vm.journey_type].quote.end_lng = '';
                            vm.booking[vm.journey_type].quote.end_post_code = ui.item.postcode;
                            vm.booking[vm.journey_type].quote.end_town_or_city = '';
                            vm.booking[vm.journey_type].quote.end_district = '';
                            vm.booking[vm.journey_type].quote.end_country = '';
                            return false;
                        },
                    });

                    $("#quote-form").on("click", '.field-swap', function() {
                        let upperSolo, lowerSolo, upperBulk = [],
                            lowerBulk = [];
                        upperSolo = $(this).parent().prev().find("input.trg-solo").val();
                        lowerSolo = $(this).parent().next().find("input.trg-solo").val();

                        $(this).parent().prev().find(".trg-bulk").find('input').each(function(index, element) {
                            upperBulk.push($(this).val());
                        });
                        $(this).parent().next().find(".trg-bulk input").each(function(index, element) {
                            lowerBulk.push($(this).val());
                        });

                        $(this).parent().prev().find("input.trg-solo").val(lowerSolo);
                        $(this).parent().next().find("input.trg-solo").val(upperSolo);

                        $(this).parent().prev().find(".trg-bulk").find('input').each(function(index, element) {
                            $(element).val(lowerBulk[index]);

                        });
                        $(this).parent().next().find(".trg-bulk input").each(function(index, element) {
                            $(element).val(upperBulk[index]);
                        });
                    });

                    $('.show-via').click(function(e) { //on add input button click
                        e.preventDefault();
                        if (vm.x == vm.max_fields) { //max input box allowed
                            alert("You have reached the limit of adding " + vm.x + " Via Points");
                        } else {
                            var html = '<div class="form-group"> <label>Via</label> <span class="addon"><img src="./assets/images/location.svg"></span>' +
                                '<input type="text" class="form-control trg-solo way_point_address' + vm.x + '" placeholder="Eg: Gatwick Airport or RH6 ONP" name="stop_point[' + vm.count + ']" required autocomplete="off">' +
                                '<span class="trg-bulk way_point' + vm.count + '">' +
                                '<input type="hidden" class="lat" name="way_point_lat[' + vm.count + ']">' +
                                '<input type="hidden" class="lng" name="way_point_lng[' + vm.count + ']">' +
                                '<input type="hidden" class="postcode" name="way_point_post_code[' + vm.count + ']"> ' +
                                '<input type="hidden" class="city" name="way_point_city[' + vm.count + ']"> ' +
                                '<input type="hidden" class="district" name="way_point_district[' + vm.count + ']">' +
                                '<input type="hidden" class="country" name="way_point_country[' + vm.count + ']">' +
                                '</span>' +
                                '<a href="javascript:void(0)" class="remove_field">' +
                                '<i class="fa fa-times text-danger"></i>' +
                                '</a>' +
                                '</div>' +
                                '<div class="options"><a></a> <a href="javascript:void(0)" class="toggle field-swap"><img src="<?= base_url('assets/images') ?>/two-way.svg"></a> </div>';
                            $(html).insertBefore($(".input-fields-wrap"));
                            vm.way_point(vm.count);
                            vm.x++;
                            vm.count++;
                        }
                        e.preventDefault();
                    });
                    $('#quote-form').on("click", ".remove_field", function(e) { //user click on remove text
                        e.preventDefault();
                        $(this).parent('div').next('div').remove();
                        $(this).parent('div').remove();
                        --vm.x;
                    });

                    $('#register-form').validate({
                        errorPlacement: function(error, element) {
                            //   element.after(error); //default
                        }
                    });

                    $('#login-form').validate({
                        errorPlacement: function(error, element) {
                            //   element.after(error); //default
                        }
                    });

                    $('#quote-form').validate({
                        errorPlacement: function(error, element) {
                            // element.after(error); //default
                            error.appendTo(element.parent().find(".error"));
                        }
                    });

                    $('#booking-info-form').validate({
                        errorPlacement: function(error, element) {
                            error.appendTo(element.parent().find(".validation"));
                        },
                        invalidHandler: function(form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            // console.log(validator);
                            $('html, body').animate({
                                scrollTop: $(validator.errorList[0].element).offset().top - 160
                            }, 1000);

                        }
                    });

                    $('#booking-info-form-two').validate({
                        errorPlacement: function(error, element) {
                            error.appendTo(element.parent().find(".validation"));
                        },
                        invalidHandler: function(form, validator) {
                            if (!validator.numberOfInvalids())
                                return;

                            $('html, body').animate({
                                scrollTop: $(validator.errorList[0].element).offset().top - 160
                            }, 1000);

                        }
                    });

                    $('#booking-info-form [name=name],#booking-info-form-two [name=name]').on('keyup', function() {
                        var _this = $(this);
                        setTimeout(function() {
                            var html = '';
                            // _this.parent('div').addClass('focused');
                            if (!_this.val()) {
                                html = '<li class="validation condition invalid"><i class="fas fa-exclamation-triangle"></i> Must not be <em>empty</em></li>';
                            } else if (_this.val().split(' ', 2).length < 2 || (typeof(_this.val().split(' ', 2)[1]) == 'undefined' || _this.val().split(' ', 2)[1] == '' || _this.val().split(' ', 2)[1] == null)) {
                                html = '<li class="validation condition warning"><i class="fas fa-info-circle"></i> Should be a <em>name</em> and <em>initial</em>; for example <strong>Jane B</strong> or <strong>J Brookes</strong>. Separate multiple names with <em>commas</em>, where required</li>';
                            }
                            _this.parent('div').find('.validation').html(html);
                        }, 1000);
                    });

                    $('#booking-info-form [name=name],#booking-info-form-two [name=name]').hover(
                        function() {
                            $(this).parent('div').find('.validation li').show();
                        },
                        function() {
                            $(this).parent('div').find('.validation li').hide();
                        }
                    );

                    $('#booking-info-form [name=phone],#booking-info-form-two [name=phone]').on('keyup', function() {
                        var _this = $(this);
                        setTimeout(function() {
                            var html = '';
                            // _this.parent('div').addClass('focused');
                            if (!_this.val()) {
                                html = '<li class="validation condition invalid"><i class="fas fa-exclamation-triangle"></i> Must not be <em>empty</em></li>';
                            } else {
                                if (_this.val().length < 10) {
                                    html += '<li class="validation condition warning"><i class="fas fa-info-circle"></i> Should have <em>at least</em> 11 digits. Separate multiple numbers with <em>commas</em>, where required</li></li>';
                                }
                                // if (!['+', '0'].includes(_this.val().charAt(0))) {
                                //     html += '<li class="validation condition warning"><i class="fas fa-info-circle"></i> Should start with zero (0) or plus (+); for example <strong>0044...</strong> or <strong>+44...</strong></li>';
                                // }
                            }
                            _this.parent('div').find('.validation').html(html);
                        }, 1000);
                    });

                    $('#login-form [name=email],#register-form [name=email]').on('keyup', function() {
                        var _this = $(this);
                        setTimeout(function() {
                            var html = '';
                            // _this.parent('div').addClass('focused');
                            if (!_this.val()) {
                                html = '<li class="validation condition invalid"><i class="fas fa-exclamation-triangle"></i> Must not be <em>empty</em></li>';
                            } else if (!validateEmail(_this.val())) {
                                html = '<li class="validation condition warning" >Should start with a <em>user</em>; for example <strong>john.smith</strong>@</li>' +
                                    '<li class="validation condition warning" >Should end with a <em>domain</em>; for example @<strong>example.com</strong></li>' +
                                    '<li class="validation condition warning" >Should have a single @ character</li>';
                            }
                            _this.parent('div').find('.validation').html(html);
                        }, 1000);
                    });
                    $('#login-form [name=password],#register-form [name=password]').on('keyup', function() {
                        var _this = $(this);
                        setTimeout(function() {
                            var html = '';
                            // _this.parent('div').addClass('focused');
                            if (!_this.val()) {
                                html = '<li class="validation condition invalid"><i class="fas fa-exclamation-triangle"></i> Must not be <em>empty</em></li>';
                            }
                            _this.parent('div').find('.validation').html(html);
                        }, 1000);
                    });

                    function validateEmail(email) {
                        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        return re.test(String(email).toLowerCase());
                    }
                },
                geocode: function(locationType, countIndex = 0) {
                    console.log(locationType, this.booking[this.booking.journey_type].quote, 'location Type');
                    let geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                            address: locationType == 'stop_point' ? this.booking[this.booking.journey_type].quote[locationType][countIndex] : this.booking[this.booking.journey_type].quote[locationType],
                        })
                        .then((response) => {
                            let results = response.results;
                            if (locationType == "stop_point") {
                                this.booking[this.booking.journey_type].quote['way_point_lat'][countIndex] = results[0].geometry.location.lat();
                                this.booking[this.booking.journey_type].quote['way_point_lng'][countIndex] = results[0].geometry.location.lng();
                            } else {
                                this.booking[this.booking.journey_type].quote[locationType + '_lat'] = results[0].geometry.location.lat();
                                this.booking[this.booking.journey_type].quote[locationType + '_lng'] = results[0].geometry.location.lng();
                            }

                            if (((this.booking[this.booking.journey_type].quote.start_lat ? true : false) && (this.booking[this.booking.journey_type].quote.end_lat ? true : false))) {
                                this.drawMap();
                            }
                        })
                        .catch((e) => {
                            console.log("Geocode was not successful for the following reason: " + e);
                        });
                },
                drawMap: function() {
                    var via_points = this.booking[this.booking.journey_type].quote.stop_point ?? '';

                    this.map_locations.push({
                        lat: this.booking[this.booking.journey_type].quote.start_lat,
                        lon: this.booking[this.booking.journey_type].quote.start_lng,
                        type: 'marker',
                        title: this.booking[this.booking.journey_type].quote.start,
                        html: this.booking[this.booking.journey_type].quote.start
                    });
                    if (via_points) {
                        via_points.map((via, index) => {
                            this.map_locations.push({
                                lat: this.booking[this.booking.journey_type].quote.way_point_lat[index],
                                lon: this.booking[this.booking.journey_type].quote.way_point_lng[index],
                                type: 'marker',
                                title: this.booking[this.booking.journey_type].quote.stop_point[index],
                                html: this.booking[this.booking.journey_type].quote.stop_point[index]
                            });
                        });
                    }

                    this.map_locations.push({
                        lat: this.booking[this.booking.journey_type].quote.end_lat,
                        lon: this.booking[this.booking.journey_type].quote.end_lng,
                        type: 'marker',
                        title: this.booking[this.booking.journey_type].quote.end,
                        html: this.booking[this.booking.journey_type].quote.end
                    });

                    var locations = this.map_locations;
                    new Maplace({
                        locations,
                        map_div: '#map',
                        generate_controls: false,
                        type: 'directions',
                        draggable: false,
                    }).Load();
                },
                removeDiscount: function() {
                    var _this = this;
                    $.ajax({
                        url: '<?= site_url('quote/removeDiscount') ?>',
                        method: 'POST',
                        data: {
                            grand_total: _this.grand_total,
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (!response.status) {
                                swal({
                                    title: "Error",
                                    text: response.msg,
                                    type: 'warning',
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                return;
                            }
                            _this.coupon_discount_code = '';
                            _this.coupon_discount_amount = 0;
                            _this.grand_total_after_discount = _this.grand_total;
                        }
                    });
                }
            },
            mounted: function() {
                let map;
                var vm = this;
                // this.screenType = screen;
                // console.log(this.booking[this.booking.journey_type].quote);
                this.loadData();
                this.calculateTotalFare();
                this.grand_total_after_discount = parseFloat(parseFloat(this.grand_total) - parseFloat(this.coupon_discount_amount)).toFixed(2);
                if (screen == 'quote-form') {
                    if (this.booking[this.journey_type].quote.stop_point) {
                        var html = '';
                        $.each(this.booking[this.journey_type].quote.stop_point, function(i, v) {
                            html += '<div class="form-group"> <label>Via</label> <span class="addon"><img src="./assets/images/location.svg"></span>' +
                                '<input type="text" class="form-control trg-solo way_point_address' + vm.x + '" value="' + v + '" placeholder="Eg: Gatwick Airport or RH6 ONP" name="stop_point[' + vm.count + ']" required autocomplete="off">' +
                                '<span class="trg-bulk way_point' + vm.count + '">' +
                                '<input type="hidden" class="lat" name="way_point_lat[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_lat[i] + '">' +
                                '<input type="hidden" class="lng" name="way_point_lng[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_lng[i] + '">' +
                                '<input type="hidden" class="postcode" name="way_point_post_code[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_post_code[i] + '"> ' +
                                '<input type="hidden" class="city" name="way_point_city[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_city[i] + '"> ' +
                                '<input type="hidden" class="district" name="way_point_district[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_district[i] + '">' +
                                '<input type="hidden" class="country" name="way_point_country[' + vm.count + ']" value="' + vm.booking[vm.journey_type].quote.way_point_country[i] + '">' +
                                '</span>' +
                                '<a href="javascript:void(0)" class="remove_field">' +
                                '<i class="fa fa-times text-danger"></i>' +
                                '</a>' +
                                '</div>' +
                                '<div class="options"><a></a> <a href="javascript:void(0)" class="toggle field-swap"><img src="<?= base_url('assets/images') ?>/two-way.svg"></a> </div>';
                            vm.way_point(vm.count);
                            vm.x++;
                            vm.count++;
                        });
                        $(html).insertBefore($(".input-fields-wrap"));
                    }
                    this.screenType = "";
                } else if (screen == 'vehicle-selection') {
                    // if (this.booking.selected_fleet !== "undefined") {
                    //     $('input[value=' + this.booking.journey_type + '][data-fleet-id=' + this.booking.selected_fleet.id + ']').prop('checked', true);
                    // }
                    // console.log(this.booking[this.booking.journey_type].quote);
                    map = new google.maps.Map(document.getElementById("map"), {
                        zoom: 4,
                        center: {
                            lat: 52.8807629,
                            lng: -1.5661885
                        },
                        mapTypeControl: false,
                    });
                    this.geocode('start');
                    this.geocode('end');
                    var count = this.booking[this.booking.journey_type].quote.stop_point ? this.booking[this.booking.journey_type].quote.stop_point.length : 0;
                    if (count > 0) {
                        for (i = 0; i < count; i++) {
                            // console.log(this.booking[this.booking.journey_type].quote['stop_point'][i]);
                            this.geocode('stop_point', i);
                        }
                    }
                    this.screenType = "quote-form";

                } else if (screen == 'booking-form') {
                    this.meet_and_greet.excerpt = this.meet_and_greet.excerpt.replace("SERVICE_CHARGE", this.additional_rate.meet_and_greet);
                    this.meet_and_greet.desc = this.meet_and_greet.desc.replace("SERVICE_CHARGE", '0');
                    this.booking.one_way.selected_fleet.passengers = parseInt(this.booking.one_way.selected_fleet.passengers);
                    this.booking.one_way.selected_fleet.suitcases = parseInt(this.booking.one_way.selected_fleet.suitcases);
                    this.booking.one_way.selected_fleet.luggage = parseInt(this.booking.one_way.selected_fleet.luggage);
                    this.booking.one_way.selected_fleet.infant_seats = parseInt(this.booking.one_way.selected_fleet.infant_seats);
                    this.booking.one_way.selected_fleet.baby_seats = parseInt(this.booking.one_way.selected_fleet.baby_seats);
                    this.booking.one_way.selected_fleet.child_booster_seats = parseInt(this.booking.one_way.selected_fleet.child_booster_seats);
                    this.booking.one_way.selected_fleet.booster_seats = parseInt(this.booking.one_way.selected_fleet.booster_seats);
                    this.booking.one_way.booking_details.discount_code = this.booking.one_way.booking_details.discount_code ? this.booking.one_way.booking_details.discount_code : '';
                    this.screenType = "vehicle-selection";
                } else if (screen == 'booking-form-two') {
                    this.meet_and_greet.excerpt = this.meet_and_greet.excerpt.replace("SERVICE_CHARGE", this.additional_rate.meet_and_greet);
                    this.meet_and_greet.desc = this.meet_and_greet.desc.replace("SERVICE_CHARGE", '0');
                    this.booking.two_way.selected_fleet.passengers = parseInt(this.booking.two_way.selected_fleet.passengers);
                    this.booking.two_way.selected_fleet.suitcases = parseInt(this.booking.two_way.selected_fleet.suitcases);
                    this.booking.two_way.selected_fleet.luggage = parseInt(this.booking.two_way.selected_fleet.luggage);
                    this.booking.two_way.selected_fleet.baby_seats = parseInt(this.booking.two_way.selected_fleet.baby_seats);
                    this.booking.two_way.selected_fleet.infant_seats = parseInt(this.booking.two_way.selected_fleet.infant_seats);
                    this.booking.two_way.selected_fleet.child_booster_seats = parseInt(this.booking.two_way.selected_fleet.child_booster_seats);
                    this.booking.two_way.selected_fleet.booster_seats = parseInt(this.booking.two_way.selected_fleet.booster_seats);
                    this.booking.two_way.booking_details.name = this.booking.two_way.booking_details.name ? this.booking.two_way.booking_details.name : this.booking.one_way.booking_details.name;
                    this.booking.two_way.booking_details.phone = this.booking.two_way.booking_details.phone ? this.booking.two_way.booking_details.phone : this.booking.one_way.booking_details.phone;
                    this.screenType = "vehicle-selection";
                }

                var hash = window.location.hash;
                if (!hash) {
                    this.screen = 'quote-form';
                }

				$('.form-control-clear').click(function () {
					$(this).parent('div').find('input').val('');
					$(this).siblings('input[type="text"]').val('').trigger('propertychange').focus();
				});

            },
            computed: {
                isDisabled: function() {
                    return !this.accept_terms;
                }
            },
            updated: function() {
                // if (this.screen == 'quote-form') {
                //     this.quoteFormValidate();
                // }
                this.$nextTick(function() {
                    // this.loadData();
                });
            }
        });
    });

    var cc_c2a_obj = new clickToAddress({
        accessToken: '<?= FECHIFY_ACCESS_TOKEN ?>',
        gfxMode: 1,
        countryMatchWith: 'iso_3',
        enabledCountries: ['GBR'],
        style: {
            ambient: 'light',
            accent: 'default'
        },
        domMode: 'class'
    });

    function loadFechify(trgClass, trgKey = 0) {
        $(".ui-widget-content").hide();
        $('#cc_c2a').show();
        getCraftyData(trgClass, trgKey);
    }

    function getCraftyData(trgClass, trgKey) {
        let _this = this;
        this.trgClass = trgClass;
        cc_c2a_obj.attach({
            search: trgClass
        }, {
            onResultSelected: function(c2a, elements, address) {
                let res;
                if (address.line_1) {
                    res = address.line_1 + ', ' + address.line_2 + ', ' + address.locality + ', ' + address.postal_code;
                } else {
                    res = address.company_name + ', ' + address.locality + ', ' + address.postal_code;
                }

                $("." + _this.trgClass).val(res);
                if (_this.trgClass == 'start') {
                    $('[name=start_town_or_city]').val(address.line_1 + ', ' + address.line_2);
                    $('[name=start_post_code]').val(address.postal_code);
                    $('[name=start_country]').val(address.country_name);
                    $('[name=start_district]').val(address.locality);
                } else if (_this.trgClass == 'end') {
                    $('[name=end_town_or_city]').val(address.line_1 + ', ' + address.line_2);
                    $('[name=end_post_code]').val(address.postal_code);
                    $('[name=end_country]').val(address.country_name);
                    $('[name=end_district]').val(address.locality);
                } else {
                    $('.way_point' + trgKey + ' .city').val(address.line_1 + ', ' + address.line_2);
                    $('.way_point' + trgKey + ' .postcode').val(address.postal_code);
                    $('.way_point' + trgKey + ' .country').val(address.country_name);
                    $('.way_point' + trgKey + ' .district').val(address.locality);
                }
            }
        });
    }

    function initFechify() {
        var config = {
            accessToken: '<?= FECHIFY_ACCESS_TOKEN ?>', // Replace this with your access token
            gfxMode: 1,
            countryMatchWith: "text",
            enabledCountries: ["United Kingdom"],
            domMode: 'name',
            placeholders: false,
            onResultSelected: function(c2a, elements, address) {
                let trgClass;
                if ($(elements.search).hasClass('start')) {
                    trgClass = $('.start');
                } else if ($(elements.search).hasClass('end')) {
                    trgClass = $('.end');
                }
                if (address.line_1) {
                    trgClass.val(address.line_1 + ', ' + address.line_2 + ', ' + address.locality + ', ' + address.postal_code);
                } else {
                    trgClass.val(address.company_name + ', ' + address.locality + ', ' + address.postal_code);
                }
            },
        };
        cc = new clickToAddress(config);
        cc.attach({
            search: 'start',
            town: 'start_town_or_city',
            postcode: 'start_post_code',
            county: 'start_country',
            line_1: 'start_district'
        });
        cc.attach({
            search: 'end',
            town: 'end_town_or_city',
            postcode: 'end_post_code',
            county: 'end_country',
            line_1: 'end_district'
        });
    }

    function numberTwoDigit(n) {
        return n > 9 ? "" + n : "0" + n;
    }

    function objectifyForm(inp) {
        var rObject = {};
        for (var i = 0; i < inp.length; i++) {
            if (inp[i]['name'].substr(inp[i]['name'].length - 2) == "[]") {
                var tmp = inp[i]['name'].substr(0, inp[i]['name'].length - 2);
                if (Array.isArray(rObject[tmp])) {
                    rObject[tmp].push(inp[i]['value']);
                } else {
                    rObject[tmp] = [];
                    rObject[tmp].push(inp[i]['value']);
                }
            } else {
                rObject[inp[i]['name']] = inp[i]['value'];
            }
        }

        return rObject;
    }
</script>
