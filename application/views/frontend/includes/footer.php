<div class="footer">
    <div class="container">
        <ul class="list-inline">
            <li>&copy; <?php echo date('Y') ?> <?= SITE_NAME ?> </li>
            <li><a href="<?= site_url('coronavirus') ?>">Coronavirus</a></li>
            <li><a href="<?= site_url('gdpr') ?>">GDPR</a></li>
            <li><a href="<?= site_url('privacy-policy') ?>">Privacy Policy</a></li>
            <li><a href="<?= site_url('terms-and-conditions') ?>">Terms of Policy</a></li>
            <li><a href="<?= site_url('cookies-policy') ?>">Cookies Policy</a></li>
        </ul>
    </div>
    <a href="https://api.whatsapp.com/send?phone=4407756903081" target="_blank" style="position:fixed; bottom:95px; right:20px;">
        <img src="<?=base_url('assets/images/WhatsApp.svg.png')?>" style="width:60px;">
    </a>
</div>
<div class="cookies" id="cookies-wrapper" style="display: none;">
    <div class="container-fluid">
        <div class="notice-text">
            <p>By continuing to use the site you agree to our cookies policy. <a href="<?= site_url('privacy-policy') ?>">Read policy</a></p>
            <h4><a href="#!" onclick="closeCookies()">Accept</a></h4>
        </div>
    </div>
</div>
<!--quote login model-->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-left" id="cancellationLabel">Passenger Sign In</h4>
                <!--<p class="text-center">No problem – we can help you reset it.</p>-->
            </div>
            <div class="modal-body">
                <div class="register-group">
                    <div class="row cstm-col">
                        <div class="col-md-12">
                            <div class="register-content">
                                <form action="<?= site_url('accounts/passengers') ?>" method="post">
                                    <div class="form-group relative">
                                        <label>Email</label>
                                        <input class="form-control" type="email" name="email" required placeholder="Enter Email">
                                    </div>
                                    <div class="form-group relative">
                                        <label>Password</label>
                                        <input class="form-control" name="password" required type="password" placeholder="Enter Password">
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="checkbox">
                                            <input type="checkbox"> Remember me
                                        </div>
                                        <a href="#!" data-toggle="modal" class="modal-trigger forget" data-target="#forgot-password">Forgot your password?</a>
                                    </div>
                                    <div class="form-group clearfix">
                                        <button class="btn-ghost btn-block" type="submit">LOG IN</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--login end-->
<!--forgot password model-->
<div class="modal fade" id="forgot-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="cancellationLabel">Forgot Your Password?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body register-content">
                <div class="register-group">
                    <p><strong> Please provide email address – we can help you reset it.</strong></p>
                    <form action="<?= site_url('index/reset_password') ?>" method="post">
                        <div class="row cstm-col">

                            <div class="col-md-12">
                                <div class="form-group relative">
                                    <label>Email</label>
                                    <input class="form-control" type="email" name="email" required placeholder="Enter Email">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="hidden" name="type" value="<?= segment(2) == 'corporates' ? 'corporate' : (segment(2) == 'travel-agents' ? 'travel_agent' : (segment(2) == 'drivers' ? 'driver' : 'passenger')) ?>">
                                <button type="submit" class="btn btn-block btn-black">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/620cdcc6a34c245641268d8d/1fs13sqi1';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script src="<?= base_url() ?>assets/bower_components/md-bootstrap/js/popper.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/md-bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/md-bootstrap/js/mdb.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/intl-tel-input-master/build/js/intlTelInput.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/bower_components/jt.timepicker/jquery.timepicker.min.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?= base_url() ?>assets/js/bootbox.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<!-- CC Validator -->
<script src="<?= base_url('assets') ?>/bower_components/jquery-creditcardvalidator/jquery.creditCardValidator.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/bower_components/bootstrap-sweetalert-master/dist/sweetalert.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.8/vue.min.js"></script>
<script src="<?= base_url() ?>assets/js/custom.js"></script>
<!-- Start of LiveChat (www.livechatinc.com) code -->

<script>
    $(function() {
        if (getCookie('covid-cookie')) {
            $('.head-banner').hide();
        } else {
            $('.head-banner').show();
        }
        if (getCookie('term-cookie')) {
            $('#cookies-wrapper').hide();
        } else {
            $('#cookies-wrapper').show();
        }


        $('.form-validation').validate();
        $(".countrycode").intlTelInput({
            separateDialCode: true,
            initialCountry: "auto",
            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "us";
                    callback(countryCode);
                });
            },
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
        });

        $('[name=tel_phone_cc]').val($('.selected-dial-code').html());
        $('[name=phone_cc]').val($('.selected-dial-code').html());
        $("input").on("countrychange", function(e, countryData) {
            $('[name=tel_phone_cc]').val('+' + countryData.dialCode);
            $('[name=phone_cc]').val('+' + countryData.dialCode);
        });
    });

    $(".dropdown-menu").hide();
    $(".cstm-drop").click(function() {
        $(".dropdown-menu").toggle();
        $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
    });

    function closeCovidCookies() {
        setCookie('covid-cookie', 1, 90);
        $('.head-banner').slideUp(200);
    }

    function closeCookies() {
        setCookie('term-cookie', 1, 90);
        $('#cookies-wrapper').slideUp(200);
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
</script>
</body>

</html>
