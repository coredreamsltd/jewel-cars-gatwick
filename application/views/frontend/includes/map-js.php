<script>
    $(document).ready(function() {
        mapsApi_initMap();
        var stop_point = <?= (!empty($quote['stop_point'])) ? json_encode($quote['stop_point']) : "''" ?>;
        var way_point_lat = <?= !empty($quote['way_point_lat']) ? json_encode($quote['way_point_lat']) : "''" ?>;
        var way_point_lng = <?= !empty($quote['way_point_lng']) ? json_encode($quote['way_point_lng']) : "''" ?>;
        var start = "<?= $quote['start'] ?>";
        var start_lat = "<?= $quote['start_lat'] ?>";
        var start_lng = "<?= $quote['start_lng'] ?>";
        var end = "<?= $quote['end'] ?>";
        var end_lat = "<?= $quote['end_lat'] ?>";
        var end_lng = "<?= $quote['end_lng'] ?>";
        var polyline_data = <?= !empty($google_data['polyline_map_data']) ? $google_data['polyline_map_data'] : 'null' ?>;
        if (!polyline_data) {
            var myLatLng = {
                lat: parseFloat(start_lat),
                lng: parseFloat(start_lng)
            };
            map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                scrollwheel: true,
                zoom: 6
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: start
            });
            return;

        }

        mapsApi_route(document.getElementById('map'), start, start_lat, start_lng, end, end_lat, end_lng, polyline_data, stop_point, way_point_lat, way_point_lng);
    });

    function mapsApi_initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 38.1038486,
                lng: -9.3912887
            },
            scrollwheel: true,
            zoom: 2
        });
    }

    function mapsApi_route(map_div, start, start_lat, start_lng, end, end_lat, end_lng, polyline_map_data, stop_point, way_point_lat, way_point_lng) {
        var start_lat_lng = {
            lat: parseFloat(start_lat),
            lng: parseFloat(start_lng)
        };
        var end_lat_lng = {
            lat: parseFloat(end_lat),
            lng: parseFloat(end_lng)
        };
        var way_point_lat_lng = [];
        if (way_point_lat !== '') {

            $.each(way_point_lat, function(index, lat) {
                way_point_lat_lng[index] = {
                    lat: parseFloat(lat),
                    lng: parseFloat(way_point_lng[index])
                };
            });
        }
        
        // init map
        var map = new google.maps.Map(map_div);
        // Draw polyline
        var rideCoordinates = google.maps.geometry.encoding.decodePath(polyline_map_data.overview_polyline.points);
        var ridePath = new google.maps.Polyline({
            map: map,
            path: rideCoordinates,
            strokeColor: "#518191",
            strokeOpacity: 1,
        });
        // set map bounds
        var ne = new google.maps.LatLng(polyline_map_data.bounds.northeast.lat, polyline_map_data.bounds.northeast.lng);
        var sw = new google.maps.LatLng(polyline_map_data.bounds.southwest.lat, polyline_map_data.bounds.southwest.lng);
        map.fitBounds(new google.maps.LatLngBounds(sw, ne));
        var markerA = new google.maps.Marker({
            map: map,
            position: start_lat_lng,
            icon: '<?= base_url('assets/images/start.png'); ?>'
        });
        var infowindowA = new google.maps.InfoWindow({
            content: '<strong>FROM: </strong>' + start
        });
        markerA.addListener('click', function() {
            infowindowA.open(map, markerA);
        });
        var markerB = new google.maps.Marker({
            map: map,
            position: end_lat_lng,
            icon: '<?= base_url('assets/images/end.png'); ?>'
        });
        var infowindowB = new google.maps.InfoWindow({
            content: '<strong>TO: </strong>' + end
        });
        markerB.addListener('click', function() {
            infowindowB.open(map, markerB);
        });

        if (way_point_lat_lng !== '') {
            $.each(way_point_lat_lng, function(index, waypoint) {

                var marker = new google.maps.Marker({
                    map: map,
                    position: waypoint,
                    icon: '<?= base_url('assets/images/via.png'); ?>'
                });
                var infowindow = new google.maps.InfoWindow({
                    content: '<strong>VIA: </strong>' + stop_point[index]
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });

            });
        }


    }
</script>