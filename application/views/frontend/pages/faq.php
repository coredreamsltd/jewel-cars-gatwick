<?php $faq_data = get_page_by_slug('faq-page') ?>
<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-intro card">
                    <?= $faq_data->excerpt ?>
                    <div id="accordion">
                        <?php if (!empty($faqs)) : ?>
                            <?php foreach ($faqs as $index=>$faq) : ?>
                                <div class="collapse-item">
                                    <div class="card-header" id="heading-<?=$index?>">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-<?=$index?>" aria-expanded="true" aria-controls="collapse-<?=$index?>">
                                                <?=$faq->title?> <i class="fa fa-angle-down"></i>
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapse-<?=$index?>" class="collapse <?=!$index?'show':''?>" aria-labelledby="heading-<?=$index?>" data-parent="#accordion">
                                        <div class="card-body"><?=$faq->content?></div>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>