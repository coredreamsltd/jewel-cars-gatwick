<div class="inner-pg thank-you">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="main-intro contact card">
                    <?php if (!empty($_GET['status']) && $_GET['status'] == 'success') : ?>
                        <h1>Success!</h1>
                        <?php flash()?>
                    <?php elseif (!empty($_GET['status']) && $_GET['status'] == 'failed') : ?>
                        <h1>Failed!</h1>
                        <?php flash()?>
                    <?php else : ?>
                        <h1>Thank You!</h1>
                        <p>Your booking request has been submitted successfully. You'll soon receive an email or call for confirmation.</p>
                    <?php endif; ?>
                    <br>
                    <a href="<?= site_url() ?>" class="btn btn-main waves-effect waves-light">
                        <i class="fa fa-home"></i> Go to home page
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>