<?php $about_data = get_page_by_slug('about-page') ?>
<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
            <?php $this->load->view('frontend/includes/quote')?>
            </div>
            <div class="col-md-7">
                <div class="main-intro card text-left">
                   <?=$about_data->excerpt?>
                </div>
            </div>
        </div>
    </div>
</div>