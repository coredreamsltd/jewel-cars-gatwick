<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="main-intro contact card">
                    <h1>Reviews</h1>
                    <hr>
                    <div class="reviews-wrap">
                        <?php if ($testimonials) : foreach ($testimonials as $testimonial) : ?>
                                <div class="single-review">
                                    <div class="client-des">
                                        <h3><?= $testimonial->title ?></h3>
                                        <p><?= $testimonial->content ?></p>
                                    </div>
                                    <div class="client-info">
                                        <br>
                                        <ul class="list-inline">
                                            <?php
                                            for ($i = 1; $i <= $testimonial->rating; $i++) {
                                                echo "<li><i class='fa fa-star'></i></li>";
                                            }
                                            ?>
                                        </ul>
                                        <figure>
                                            <figcaption><?= $testimonial->post_by ?><br>
                                                <span><?= SMDCrateDateTimeFormat($testimonial->post_date, 'Y-m-d', 'M d, Y') ?></span>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                        <?php endforeach;
                        endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="widget">
                    <h2>Leave a Review</h2>
                    <div class="contact-form journey-details">
                        <?php flash() ?>
                        <form action="<?= site_url('mail/review') ?>" method="post">
                            <div class="form-group">
                                <label for="text">Full Name</label>
                                <input type="text" class="form-control" placeholder="Enter Full Name" name="post_by" required="">
                            </div>
                            <div class="form-group">
                                <label for="email">Email </label>
                                <input type="text" class="form-control" placeholder="Enter Email Address" name="email" required="">
                            </div>
                            <div class="form-group">
                                <label for="text">Title of Your review</label>
                                <input type="text" class="form-control" placeholder="Enter Title" name="title" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Rating</label><br>
                                <select class="form-control rating" name="rating">
                                    <option value="5" selected>★★★★★</option>
                                    <option value="4">★★★★</option>
                                    <option value="3">★★★</option>
                                    <option value="2">★★</option>
                                    <option value="1">★</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Write your Review</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" name="content" rows="3" placeholder="Your Review"></textarea>
                            </div>
                            <button type="submit" class="btn  btn-block btn-main">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>