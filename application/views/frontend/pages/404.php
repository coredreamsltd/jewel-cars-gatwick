<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="main-intro contact card">
                    <h1>4<span>0</span>4 Page Error</h1>
                    <p>We cannot find the page you're looking for.</p>
                    <br>
                    <a href="<?= site_url() ?>" class="btn btn-main waves-effect waves-light">
                        <i class="fa fa-home"></i> Go to home page
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>