<?php $school_data = get_page_by_slug('school-transport-page') ?>
<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <?php $this->load->view('frontend/includes/quote') ?>
            </div>
            <div class="col-md-7">
                <div class="main-intro card text-left">
                    <?= $school_data->excerpt ?>
                </div>
            </div>
        </div>
        <div class="area card">
            <?= $school_data->desc ?>
        </div>
    </div>
</div>