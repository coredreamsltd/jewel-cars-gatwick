<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <?php $this->load->view('frontend/includes/quote') ?>
            </div>
            <div class="col-md-7">
                <div class="main-intro card text-left">
                    <?= $template_data->excerpt ?>
                    <?= $template_data->desc ?>
                    <?= $template_data->desc_short ?>
                </div>
            </div>
        </div>
    </div>
</div>