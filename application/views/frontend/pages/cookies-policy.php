<?php $cookies_data = get_page_by_slug('cookies-policy-page') ?>
<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <?php $this->load->view('frontend/includes/quote') ?>
            </div>
            <div class="col-md-7">
                <div class="main-intro card">
                    <?= $cookies_data->desc ?>
                </div>
            </div>
        </div>
    </div>
</div>