<?php $work_data = get_page_by_slug('work-with-us-page') ?>
<div class="inner-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-intro card">
                    <?= $work_data->excerpt ?>
                </div>
            </div>
        </div>
    </div>
</div>