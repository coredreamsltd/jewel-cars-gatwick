<?php $home_data = get_page_by_slug('home-page') ?>
<div class="main_page">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <?php $this->load->view('frontend/includes/quote') ?>
                <div class="why_us">
                    <?= $home_data->desc ?>
                </div>
            </div>
            <div class="col-md-7">
                <div class="fleet_sec">
                    <?= $home_data->excerpt ?>
                    <?php if (!empty($fleets)) : foreach ($fleets as $index => $fleet) : ?>
                            <div class="fleet_item">
                                <img src="<?= base_url('uploads/fleet/'.$fleet->img_name) ?>">
                                <article>
                                    <h4><?=$fleet->title?></h4>
                                    <p><?=$fleet->desc?></p>
                                </article>
                            </div>
                    <?php endforeach;
                    endif; ?>
                </div>
                <div class="advantage card">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $home_data->desc_short ?>
                        </div>
                        <div class="col-md-6">
                            <?= $home_data->desc_long ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="intro card">
                    <?= $home_data->extra_1 ?>
                </div>
                <div class="intro card">
                    <?= $home_data->extra_2 ?>
                </div>
            </div>
        </div>
    </div>
</div>