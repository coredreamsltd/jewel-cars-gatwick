<div class="main_page">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <input class="form-control" id="start" name="search_field">
            </div>
        </div>
    </div>
</div>

<script src="https://cc-cdn.com/generic/scripts/v1/cc_c2a.min.js"></script>

<script>
    new clickToAddress({
        accessToken: '1ae47-842d5-1d82d-06799', // Replace this with your access token
        dom: {
            search: 'search_field', // 'search_field' is the name of the search box element
            line_1: 'addr_line1',
            line_2: 'addr_line2',
            company: 'company',
            town: 'addr_town',
            postcode: 'addr_postcode',
            county: 'addr_county',
            country: 'addr_country'
        },
        domMode: 'name', // Use names to find form elements
        showLogo: true, // Show Address Auto-Complete logo
        historyTools: true, // Show arrows
        defaultCountry: 'gbr', // Sets the default country to the UK
        getIpLocation: true, // Sets default country based on IP
        countrySelector: true, // Country list is enabled
        enabledCountries: ['gbr', 'usa', 'can'], // Enabled countries are the UK, USA and Canada
        countryMatchWith: 'iso_3', // enabledCountries uses ISO_3 codes
        countryLanguage: 'en', // Country list displayed in English
        disableAutoSearch: false, // Auto-search is enabled
        limitToMaxLength: {
            enabled: false,
            useEllipsis: false // Note: This feature may not be compatible with certain database systems
        },
        onResultSelected: function(c2a, elements, address) {
            // Perform any action here with the available data.
            // For example, you could reveal all the form fields when the fields are filled.
            console.log(c2a, elements, address);
            $('#start').val(address.line_1+' '+address.line_2+' '+address.postal_code);
        },
        onSetCounty: function(c2a, elements, county) {
            // Perform any action here with the available data.
            // You may want your own function to change the county, depending on your checkout.
        },
        onError: function(code, message) {
            // Perform any action here with the available data.
            // For example, you may want to reveal the form fields if there is an error.
        },
        onSearchFocus: function(c2a, elements) {
            // Perform any action here with the available data.
            // For example, you may want to  part of the form when the search box is selected.
        }

    });
</script>