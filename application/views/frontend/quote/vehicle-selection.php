<div class="vehicle-select" id="choose-vehicle" v-show="screen == 'vehicle-selection'" style="display: none;">
    <div class="journey-info">
        <div class="journey">
            <h2><span>Pick Up:</span></h2>
            <h2>{{booking[journey_type].quote.start}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form')"><i class="fa fa-edit"></i></a></h2>
        </div>
        <div v-if="booking[journey_type].quote.stop_point">
            <div class="journey" v-for="via in booking[journey_type].quote.stop_point">
                <h2><span>Via:</span></h2>
                <h2> {{via}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form')"><i class="fa fa-edit"></i></a></h2>
            </div>
        </div>
        <div class="journey">
            <h2><span>Drop Off:</span></h2>
            <h2> {{booking[journey_type].quote.end}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form')"><i class="fa fa-edit"></i></a></h2>
        </div>
    </div>
    <div class="my_card">
        <div id="map" class="map-canvas"></div>
    </div>
    <div class="my_card">
        <h4>Journey Fares</h4>
        <div class="info-text">
            <span class="info"><img src="<?= base_url('assets/images') ?>/info.svg"></span>
            <p class="hidden-text">Now that you've chosen your route, you can view the available fares and select a vehicle type.</p>
        </div>
        <article>
            <h5>Book a return journey now for an instant discount!</h5>
            <p>Pricing includes any applicate VAT & Credit Card Charges</p>
        </article>
    </div>

    <!-- {{booking.two_way.fleets}} -->
    <!-- {{bookings[journey].two_way.fleets[1]}} -->
    <div class="fleet_price" v-for="(fleet,index) in  (journey_type == 'one_way'?booking.one_way.fleets:booking.two_way.fleets)" v-if="bookings[journey].one_way.fleets[index] && (bookings[journey].one_way.fleets[index].fare || bookings[journey].two_way.fleets[index].fare) ">
        <div class="row" >
            <div class="col-md-7">
                <h5>{{fleet.title}}</h5>
                <img v-bind:src="'<?= base_url() ?>/uploads/fleet/' + fleet.img_name">
                <p>{{fleet.desc_1}}</p>
            </div>
            <div class="col-md-5">
                <label class="btn btn-main" v-if="bookings[journey].one_way.fleets[index].fare">Single <?= CURRENCY ?> {{bookings[journey].one_way.fleets[index].fare}}
                    <input type="radio" name="journey_type" value="one_way" :data-fleet-id="fleet.id" v-on:click="selectVehicle()">
                </label>
                <label class="btn btn-main" v-if="bookings[journey].two_way.fleets[index].fare">Return <?= CURRENCY ?> {{bookings[journey].one_way.fleets[index].fare + bookings[journey].two_way.fleets[index].fare}}
                    <input type="radio" name="journey_type" value="two_way" :data-fleet-id="fleet.id" v-on:click="selectVehicle()">
                </label>
            </div>
        </div>
    </div>

    <div class="button-inline">
        <button type="button" class="btn btn-default" v-on:click="editScreen('quote-form')">
            Edit Route <img src="<?= base_url('assets/images') ?>/edit.svg">
        </button>
        <button type="button" class="btn btn-main" v-on:click="selectVehicle()">
            Continue <img src="<?= base_url('assets/images') ?>/angle-right.svg">
        </button>
    </div>
</div>