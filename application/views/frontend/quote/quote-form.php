    <div class="quote" v-show="screen == 'quote-form'" style="display: none;">
        <form id="quote-form" method="post">
            <div class="info-text">
                <span class="info"><img src="<?= base_url('assets/images') ?>/info.svg"></span>
                <p class="hidden-text">Now that you've chosen your route, you can view the available fares and select a vehicle type.</p>
            </div>
            <div class="form-group">
                <label>Pick Up location</label>
                <span class="addon"><img src="<?= base_url('assets/images') ?>/location.svg"></span>
                <input type="text" class="form-control start trg-solo" placeholder="Eg: Gatwick Airport or RH6 ONP" name="start" :value="booking[journey_type].quote.start" required autocomplete="off">
				<span class="addon-right form-control-clear"><i class="fa fa-times"></i></span>
                <span class='error'></span>
                <span class="startLatLng trg-bulk">
                    <input type="hidden" data-geo="lat" name="start_lat" :value="journey_type=='one_way'?booking.one_way.quote.start_lat:booking[journey_type].quote.start_lat">
                    <input type="hidden" data-geo="lng" name="start_lng" :value="booking[journey_type].quote.start_lng">
                    <input type="hidden" name="start_post_code" :value="booking[journey_type].quote.start_post_code">
                    <input type="hidden" name="start_town_or_city" :value="booking[journey_type].quote.start_town_or_city">
                    <input type="hidden" name="start_district" :value="booking[journey_type].quote.start_district">
                    <input type="hidden" name="start_country" :value="booking[journey_type].quote.start_country">
                </span>
                <div class="location-suggestion-container start-loader"></div>
            </div>
            <div class="options">
                <a href="javascript:void(0)" class="add_via show-via"><img src="<?= base_url('assets/images') ?>/via.svg"> Add Via</a>
                <a href="javascript:void(0)" class="toggle field-swap"><img src="<?= base_url('assets/images') ?>/two-way.svg"></a>
            </div>

            <div class="form-group" v-if="booking[journey_type].quote.stop_point" v-for="(via,i) in booking[journey_type].quote.stop_point">
                <label class="active">Via</label>
                <span class="addon"><img src="./assets/images/location.svg"></span>
                <input type="text" class="form-control trg-solo {{'way_point_address'+i}}" :value="via" placeholder="Eg: Gatwick Airport or RH6 ONP" name="stop_point[{{i}}]" required="" autocomplete="off">
                <span class="trg-bulk {{'way_point'+i}}">
                    <input type="hidden" class="lat" name="way_point_lat[{{i}}]" :value="booking[journey_type].quote.way_point_lat[i]">
                    <input type="hidden" class="lng" name="way_point_lng[{{i}}]" :value="booking[journey_type].quote.way_point_lng[i]">
                    <input type="hidden" class="postcode" name="way_point_post_code[{{i}}]" :value="booking[journey_type].quote.way_point_post_code[i]" id="address-zip">
                    <input type="hidden" class="city" name="way_point_city[{{i}}]" :value="booking[journey_type].quote.way_point_city[i]" id="address-town">
                    <input type="hidden" class="district" name="way_point_district[{{i}}]" :value="booking[journey_type].quote.way_point_district[i]" id="address-province-name">
                    <input type="hidden" class="country" name="way_point_country[{{i}}]" :value="booking[journey_type].quote.way_point_country[i]" id="address-country">
                </span>
                <a href="javascript:void(0)" class="remove_field"><i class="fa fa-times text-danger"></i></a>
                <div class="options">
                    <a></a>
                    <a href="javascript:void(0)" class="toggle field-swap"><img src="<?= base_url('assets/images') ?>/two-way.svg"></a>
                </div>
            </div>

            <div class="form-group input-fields-wrap">
                <label>Drop Off location</label>
                <span class="addon"><img src="<?= base_url('assets/images') ?>/location.svg"></span>
                <input type="text" class="form-control end trg-solo search_keyword" placeholder="Eg: Gatwick Airport or RH6 ONP" name="end" :value="booking[journey_type].quote.end" required autocomplete="off">
                <span class="addon-right form-control-clear"><i class="fa fa-times"></i></span>
				<span class='error'></span>
                <span class="endLatLng trg-bulk">
                    <input type="hidden" data-geo="lat" name="end_lat" :value="booking[journey_type].quote.end_lat">
                    <input type="hidden" data-geo="lng" name="end_lng" :value="booking[journey_type].quote.end_lng">
                    <input type="hidden" name="end_post_code" :value="booking[journey_type].quote.end_post_code">
                    <input type="hidden" name="end_town_or_city" :value="booking[journey_type].quote.end_town_or_city">
                    <input type="hidden" name="end_district" :value="booking[journey_type].quote.end_district">
                    <input type="hidden" name="end_country" :value="booking[journey_type].quote.end_country">
                </span>
                <div class="location-suggestion-container end-loader"></div>
            </div>
            <div class="button-inline">
                <button v-if="show_basket" type="button" class="btn btn-default" v-on:click="editScreen('confirm')">
                    Basket <img src="<?= base_url('assets/images') ?>/basket.svg">
                </button>
                <!-- <a href="<?= site_url('passenger/login') ?>" class="btn btn-default">
                    Log In <img src="<?= base_url('assets/images') ?>/login.svg">
                </a> -->
                <button class="btn btn-default" data-toggle="modal" data-target="#exampleModal">
                    Log In <img src="<?= base_url('assets/images') ?>/login.svg">
                </button>
                <a href="javascript:void(0)" class="btn btn-main" v-on:click="getQuote">
                    Get Quote <img src="<?= base_url('assets/images') ?>/angle-right.svg">
                </a>
            </div> 
        </form>

            <!-- Login Modal -->
            <?php
                $this->load->view('/frontend/quote/login-modal');
            ?>

             <!-- Reset Password Modal -->
             <div class="modal fade" id="resetModel" tabindex="-1" role="dialog" aria-labelledby="resetModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="resetModalLabel">Reset Password</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" id="reset-password-form">
                                <div class="form-wrapper">
                                    <div class="row form-row">
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                <label>Email</label>
                                                <span class="addon"><img src="<?= base_url('assets/images') ?>/email.svg"></span>
                                                <input type="email" class="form-control" v-model="login.email" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-inline">
                                <button v-if="show_basket" type="button" class="btn btn-default" v-on:click="editScreen('confirm')">
                                        Basket <img src="<?= base_url('assets/images') ?>/basket.svg">
                                    </button>
                                    <button type="button" class="btn btn-main" v-on:click="editScreen('account')">
                                        Submit <i class="fa fa-sign-in-alt"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <hr>
        <div class="payment_card">
            <h3>We Accept</h3>
            <img src="<?= base_url('assets/images') ?>/payment_logo.png">
        </div>
    </div>
