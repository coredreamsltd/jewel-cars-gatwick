<div class="confirm-booking vehicle-select" v-show="screen == 'confirm'" style="display: none;">
	<div v-if="bookings[0].one_way.quote">
		<div class="split-journey" v-if="(bookings && booking.one_way.booking_details)" v-for="(booking, index) in bookings">
			<div class="btn_main"> Fare:
				<span v-if="booking.one_way.selected_fleet"><?= CURRENCY ?>
					{{booking.one_way.selected_fleet.fare+(booking.journey_type == 'two_way'?booking.two_way.selected_fleet.fare:0)}}
				</span>
			</div>
			<div class="journey-info">
				<div class="journey">
					<h2><span>Pick up:</span></h2>
					<h2> {{booking.one_way.quote.start}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form',index)" title="Edit start location">
							<i class="fa fa-edit"></i></a>
					</h2>
				</div>
				<div v-if="booking.one_way.quote.stop_point">
					<div class="journey" v-for="via in booking.one_way.quote.stop_point">
						<h2><span>Via:</span></h2>
						<h2> {{via}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form',index)"><i class="fa fa-edit"></i></a></h2>
					</div>
				</div>
				<div class="journey">
					<h2><span>Drop off:</span></h2>
					<h2> {{booking.one_way.quote.end}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form',index)" title="Edit end location"><i class="fa fa-edit"></i></a></h2>
				</div>
			</div>
			<div class="card-wrapper">
				<a href="javascript:void(0)" v-on:click.prevent="editScreen('booking-form',index)" class="confirm-edit-btn" title="Edit these details"><i class="fa fa-edit"></i></a>
				<a v-bind:href="'./?action=remove_journey&journey='+ index+'&type=one_way'" title="Remove this journey from basket" onclick="return confirm('Remove This Journey From Your Basket?')" class="confirm-edit-btn cancel"><i class="fa fa-trash-alt"></i></a>
				<p v-if="booking.one_way.quote.is_start_airport">
					Flight arriving from {{booking.one_way.booking_details.flight_arrive_from}}, {{booking.one_way.booking_details.flight_number}}
				</p>
				<p>Pickup at {{booking.one_way.booking_details.pickup_time}} on {{booking.one_way.booking_details.pickup_date}}</p>
				<p v-if="booking.one_way.selected_fleet">By {{booking.one_way.selected_fleet.title}}</p>
				<p>{{booking.one_way.booking_details.passenger}} Passenger with {{booking.one_way.booking_details.suitcase}} Luggage</p>
				<p>
					<span v-if="booking?.one_way?.booking_details?.infant_seat??false">{{booking.one_way.booking_details.infant_seat}} Infant Seat 0-12 months,</span>
					<span v-if="booking?.one_way?.booking_details?.child_seat??false">{{booking.one_way.booking_details.child_seat}} Child Seat 1-2 Years</span>
				</p>
				<p>
					<span v-if="booking?.one_way?.booking_details?.child_booster_seat??false">{{booking.one_way.booking_details.child_booster_seat}} Child Booster Seat 2-4 Years,</span>
					<span v-if="booking?.one_way?.booking_details?.booster_seat??false">{{booking.one_way.booking_details.booster_seat}} Booster Seat 4 Years +</span>
				</p>
			</div>

			<div v-if="booking.journey_type == 'two_way'">
				<br>
				<div class="journey-info">
					<div class="journey">
						<h2><span>Pick up:</span></h2>
						<h2> {{booking.two_way.quote.start}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form',index,'two_way')" title="Edit start location"><i class="fa fa-edit"></i></a></h2>
					</div>
					<div v-if="booking.two_way.quote.stop_point">
						<div class="journey" v-for="via in booking.two_way.quote.stop_point">
							<h2><span>Via:</span></h2>
							<h2> {{via}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form',index,'two_way')"><i class="fa fa-edit"></i></a></h2>
						</div>
					</div>
					<div class="journey">
						<h2><span>Drop off:</span></h2>
						<h2> {{booking.two_way.quote.end}} <a href="javascript:void(0)" v-on:click.prevent="editScreen('quote-form',index,'two_way')" title="Edit end location"><i class="fa fa-edit"></i></a></h2>
					</div>
				</div>

				<div class="card-wrapper" v-if="booking.two_way.booking_details">
					<a href="javascript:void(0)" v-on:click.prevent="editScreen('booking-form-two',index)" class="confirm-edit-btn" title="Edit these details"><i class="fa fa-edit"></i></a>
					<a v-bind:href="'./?action=remove_journey&journey='+ index+'&type=two_way'" title="Remove this journey from basket" onclick="return confirm('Remove This Journey From Your Basket?')" class="confirm-edit-btn cancel"><i class="fa fa-trash-alt"></i></a>
					<p v-if="booking.two_way.quote.is_start_airport">
						Flight arriving from {{booking.two_way.booking_details.flight_arrive_from}}, {{booking.two_way.booking_details.flight_number}}
					</p>
					<p>Pickup at {{booking.two_way.booking_details.pickup_time}} on {{booking.two_way.booking_details.pickup_date}}</p>
					<p>By {{booking.two_way.selected_fleet.title}}</p>
					<p>{{booking.two_way.booking_details.passenger}} Passenger with {{booking.two_way.booking_details.suitcase}} Luggage</p>
					<p>
						<span v-if="booking?.two_way?.booking_details?.infant_seat??false">{{booking.two_way.booking_details.infant_seat}} Infant Seat 0-12 months,</span>
						<span v-if="booking?.two_way?.booking_details?.child_seat??false">{{booking.two_way.booking_details.child_seat}} Child Seat 1-2 Years</span>
					</p>
					<p>
						<span v-if="booking?.two_way?.booking_details?.child_booster_seat??false">{{booking.two_way.booking_details.child_booster_seat}} Child Booster Seat 2-4 Years,</span>
						<span v-if="booking?.two_way?.booking_details?.booster_seat??false">{{booking.two_way.booking_details.booster_seat}} Booster Seat 4 Years +</span>
					</p>
				</div>
			</div>

			<div class="is_flex">
				<a v-bind:href="'./?action=add_return_journey&journey='+ index" class="btn btn-default" v-if="booking.journey_type == 'one_way'">
					Add Return <i class="fa fa-history"></i>
				</a>
				<a href="<?= site_url('?action=add_new_journey') ?>" v-if="bookings.length -1 == index" class="btn btn-default"> New Journey <i class="fa fa-calendar-plus"></i></a>
			</div>
		</div>
		<br>
		<div v-if="coupon_discount_amount == 0">
			<label>Add a promotional code (optional)</label>
			<div class="input-group mb-3">
				<input type="text" class="form-control" v-model="coupon_discount_code" required>
				<div class="input-group-append">
					<span class="input-group-text" id="apply-discount" @click="applyDiscount()">Apply</span>
				</div>
			</div>
		</div>

		<div v-else>
			<label>Remove discount</label> <button class="btn btn-danger" @click="removeDiscount()"><i class="fa fa-trash"></i></button>
		</div>
		<!-- <div class="btn_main">
            <br>
            <label>Payment Method</label>
            <input type="radio" name="pay_method" v-model="pay_method" value="cash"> Cash
            <input type="radio" name="pay_method" v-model="pay_method" value="card"> Card
        </div> -->
		<br>
		<div class="btn_main">
			Total Fare:
			<tag v-if="coupon_discount_amount>0"><small>(Inclusive of coupon discount <?= CURRENCY ?>{{coupon_discount_amount}})</small></tag>
			<span><?= CURRENCY ?>{{grand_total_after_discount>0?grand_total_after_discount:grand_total}}</span>
		</div>
		<div class="checkmark">
			<input type="checkbox" id="terms-and-conditions" name="is_terms" v-model="accept_terms">
			<label for="terms-and-conditions">I have read and agree to the <a href="<?= site_url('terms-and-conditions') ?>">terms and conditions</a> with <?= SITE_NAME ?></label>
		</div>
		<button @click="payNow()" class="btn btn-main" :disabled='isDisabled'>Pay Now</button>
	</div>
	<div v-else>
		<p class="text-white">Currently no journeys in your basket. Please click on "<a href="<?= site_url('?action=add_new_journey') ?>" class="text-primary">Add a new journey</a>" to begin the booking process.</p>
	</div>
</div>
