<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="advice error d-none"></div>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login To Jewel Cars Gatwick</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="login-form">
                    <div class="form-wrapper">
                        <div class="form-group">
                            <label>Email</label>
                            <span class="addon"><img src="<?= base_url('assets/images') ?>/email.svg"></span>
                            <input type="email" class="form-control" name="email" v-model="login.email" required>
                            <ul class="validation"></ul>
                        </div>
                        <div class="form-group">
                            <label>Password </label>
                            <span class="addon"><i class="fa fa-key"></i></span>
                            <input type="password" class="form-control" name="password" v-model="login.password" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>
                    <div class="button-inline">
                        <button type="button" class="btn btn-default" data-target="#resetModel" data-toggle="modal" data-dismiss="modal">
                            Reset Password <i class="fa fa-key"></i>
                        </button>
                        <button type="button" class="btn btn-main" v-on:click="logIn()">
                            Log In <i class="fa fa-sign-in-alt"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>