<div class="inner-pg">
	<div class="container">
		<?php if (empty($invoice) || !empty($invoice->is_cancelled) || !empty($invoice->payment_status)) : ?>
			<div class="main-intro contact card">
				<?php flash() ?>
			</div>
		<?php else : ?>
			<div class="row">
				<div class="col-md-5">
					<div class="widget">
						<h2>Make a Payment</h2>
						<div class="contact-form journey-details">
							<form action="<?= site_url("quote/payment_finish?invoice_id={$invoice->invoice_id}") ?>" method="post">
								<div class="form-group">
									<label>Credit holder name</label>
									<span class="addon"><img src="<?= base_url('assets/images') ?>/user.svg"></span>
									<input class="form-control auto-tab" type="text" name="card_holder_name" placeholder="Enter credit holder name" required>
								</div>
								<div class="form-group">
									<label>Credit card number</label>
									<span class="addon" id="card-image"><img src="<?= base_url('assets/images/cc/default.png') ?>"></span>
									<input class="form-control auto-tab" type="text" name="card_number" placeholder="Enter credit card number" maxlength="16" required>
								</div>
								<div class="row form-row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Expiry month</label>
											<span class="addon"><img src="<?= base_url('assets/images') ?>/calendar.svg"></span>
											<input class="form-control auto-tab" name="exp_month" placeholder="MM" required min="1" max="12" maxlength="2">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Expiry year</label>
											<span class="addon"><img src="<?= base_url('assets/images') ?>/calendar.svg"></span>
											<input class="form-control auto-tab" name="exp_year" placeholder="YY" required min="<?= date('y') ?>" max="99" maxlength="2">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>CVC</label>
											<span class="addon"><img src="<?= base_url('assets/images') ?>/lock.svg"></span>
											<input class="form-control auto-tab" name="card_cvc" placeholder="1234" maxlength="4" required>
										</div>
									</div>
								</div>

								<button type="submit" class="btn  btn-block btn-main">Submit</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-7">
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
<script>
	$(document).ready(function() {

		$('.auto-tab').keyup(function() {
			auto_tab($(this));
		});

		$('[name=card_number]').validateCreditCard(function(result) {
			if (result.card_type) {

				var cardLength, cvvLength;

				cardLength = result.card_type.valid_length[0];

				if (result.card_type.name === 'visa' || result.card_type.name === 'visa_electron' || result.card_type.name === 'mastercard') {
					cvvLength = 3;
				} else if (result.card_type.name === 'amex') {
					cvvLength = 4;
				} else {
					cvvLength = 4;
				}

				$('[name=card_number]').attr('maxLength', cardLength);
				$('[name=card_code]').attr('maxLength', cvvLength);

				auto_tab($(this));

				$('#card-image img').attr('src', '<?= base_url('assets/images/cc') ?>/' + result.card_type.name + '.png');
			} else {
				$('#card-image img').attr('src', '<?= base_url('assets/images/cc/default.png') ?>');
			}
		});
	});

	function auto_tab(element) {
		if (element.val().length == element.attr('maxLength')) {
			var inputs = element.closest('form').find('.auto-tab');
			inputs.eq(inputs.index(element) + 1).focus();
		}
	}
</script>
