<div class="journey-details vehicle-select" v-show="screen == 'booking-form'" style="display: none;">
    
    <h4>Passenger Details
    </h4>
    <div class="info-text">
        <span class="info" data-toggle="passenger-details"><img src="<?= base_url('assets/images') ?>/info.svg"></span>
    </div>
    <form method="post" id="booking-info-form">
        <div class="form-wrapper">
            <div class="form-group">
                <label>Head Passenger Full Name</label>
                <span class="addon"><img src="<?= base_url('assets/images') ?>/user.svg"></span>
                <input type="text" class="form-control" name="name" v-model="booking.one_way.booking_details.name" required>
                <ul class='validation'></ul>
            </div>
            <div class="form-group hidden-text" data-identifier="passenger-details">
                <div class="group group-info">
                    <div class="form-information-guidance">
                        <p class="guidance"><i class="fas fa-info-circle"></i> Our driver will need to know the <em>Head Passenger Name</em> and <em>Passenger Mobile</em> telephone number.</p>
                        <p class="guidance"><i class="fas fa-info-circle"></i> If you're booking for someone else and don't know this information yet, please use a dash <span style="display:inline-block;">( - )</span> instead.</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Mobile Number</label>
                <!--<span class="addon"><img src="<?= base_url('assets/images') ?>/phone2.svg"></span>-->
                <input type="hidden" name="phone_cc">
                <input type="number" class="form-control countrycode" name="phone" v-model="booking.one_way.booking_details.phone" required>
                <ul class='validation'></ul>
            </div>
            <!-- <div class="form-group">
                <label>Email</label>
                <span class="addon"><img src="<?= base_url('assets/images') ?>/email.svg"></span>
                <input type="email" class="form-control" name="email" v-model="booking.one_way.booking_details.email" required>
            </div> -->
            <div class="row form-row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Passengers </label>
                        <span class="addon"><img src="<?= base_url('assets/images') ?>/user.svg"></span>
                        <select class="form-control" name="passenger" required v-if="booking.one_way.selected_fleet != ''">
                            <option v-for="pax in booking.one_way.selected_fleet.passengers" :value="pax">
                                {{pax}} Person
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Suitcases</label>
                        <span class="addon"><img src="<?= base_url('assets/images') ?>/luggage.svg"></span>
                        <select class="form-control" name="suitcase" required v-if="booking.one_way.selected_fleet != ''">
                            <option value="0">0 Suitcase</option>
                            <option v-for="lug in booking.one_way.selected_fleet.suitcases" :value="lug">
                                {{lug}} Suitcases
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Hand Luggages</label>
                        <span class="addon"><img src="<?= base_url('assets/images') ?>/luggage.svg"></span>
                        <select class="form-control" name="luggage" required v-if="booking.one_way.selected_fleet != ''">
                            <option value="0">0 Hand Luggage</option>
                            <option v-for="lug in booking.one_way.selected_fleet.luggage" :value="lug">
                                {{lug}} Hand Luggages
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12" v-if="booking.one_way.selected_fleet && (booking.one_way.selected_fleet.infant_seats || booking.one_way.selected_fleet.baby_seats || booking.one_way.selected_fleet.child_booster_seats || booking.one_way.selected_fleet.booster_seats)">
                    <div class="form-group">
                        <label><input type="checkbox" name="is_baby_seat" @change="toggleIsBabySeat('one_way',$event)"> I require a child seat</label>
                    </div>
                    <div class="form-group" v-if="booking.one_way.booking_details.is_baby_seat">
                        <label v-if="booking.one_way.selected_fleet.infant_seats">
                            <input type="checkbox" name="is_infant_seat" @change="toggleChildSeat('one_way','is_infant_seat',$event)"> Infant Seat 0-12 months (<?= CURRENCY ?> {{additional_rate.baby_seater}})
                            <select name="infant_seat" required v-model="booking.one_way.booking_details.infant_seat" v-if="booking.one_way.booking_details.is_infant_seat" style="width:15%">
                                <option v-for="seat in booking.one_way.selected_fleet.infant_seats">
                                    {{seat}}
                                </option>
                            </select>
                        </label>
                        <label v-if="booking.one_way.selected_fleet.baby_seats">
                            <input type="checkbox" name="is_child_seat" @change="toggleChildSeat('one_way','is_child_seat',$event)"> Child Seat 1-2 Years (<?= CURRENCY ?> {{additional_rate.baby_seater}})
                            <select name="child_seat" required v-model="booking.one_way.booking_details.child_seat" v-if="booking.one_way.booking_details.is_child_seat" style="width:15%">
                                <option v-for="seat in booking.one_way.selected_fleet.baby_seats">
                                    {{seat}}
                                </option>
                            </select>
                        </label>
                        <label v-if="booking.one_way.selected_fleet.child_booster_seats">
                            <input type="checkbox" name="is_child_booster_seat" @change="toggleChildSeat('one_way','is_child_booster_seat',$event)"> Child Booster Seat 2-4 Years (<?= CURRENCY ?> {{additional_rate.baby_seater}})
                            <select name="child_booster_seat" required v-model="booking.one_way.booking_details.child_booster_seat" v-if="booking.one_way.booking_details.is_child_booster_seat" style="width:15%">
                                <option v-for="seat in booking.one_way.selected_fleet.child_booster_seats">
                                    {{seat}}
                                </option>
                            </select>
                        </label>
                        <label v-if="booking.one_way.selected_fleet.booster_seats">
                            <input type="checkbox" name="is_booster_seat" @change="toggleChildSeat('one_way','is_booster_seat',$event)"> Booster Seat 4 Years + (<?= CURRENCY ?> {{additional_rate.baby_seater}})
                            <select name="booster_seat" required v-model="booking.one_way.booking_details.booster_seat" v-if="booking.one_way.booking_details.is_booster_seat" style="width:15%">
                                <option v-for="seat in booking.one_way.selected_fleet.booster_seats">
                                    {{seat}}
                                </option>
                            </select>
                        </label>
                        <!-- <label>Baby's Age</label>
                        <select class="form-control" name="baby_age" required v-model="booking.one_way.booking_details.baby_age">
                            <option v-for="age in baby_ages">
                                {{age}}
                            </option>
                        </select> -->
                    </div>
                </div>
            </div>
            <div class="form-group hidden-text" data-identifier="passenger-details">
                <div class="group group-info">
                    <div class="form-information-guidance">
                        <p class="guidance">If the desired amount of passengers and luggage isn't available, you'll need to go <a href="#!" @click="editScreen('vehicle-selection')">back to your fares</a> and select a larger vehicle.</p>
                    </div>
                </div>
            </div>
        </div>
        <h4>Journey Details</h4>
        <div class="info-text">
            <span class="info" data-toggle="journey-details"><img src="<?= base_url('assets/images') ?>/info.svg"></span>
        </div>
        <div class="form-wrapper">
            <div v-if="booking.one_way.quote.is_start_airport">
                <div class="row form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Flight Arriving From</label>
                            <span class="addon"><img src="<?= base_url('assets/images') ?>/airport-terminal.svg"></span>
                            <input type="text" class="form-control" name="flight_arrive_from" v-model="booking.one_way.booking_details.flight_arrive_from" required>
                            <ul class='validation'></ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Flight Number</label>
                            <span class="addon"><img src="<?= base_url('assets/images') ?>/flight.svg"></span>
                            <input type="text" class="form-control" minlength="2" name="flight_number" v-model="booking.one_way.booking_details.flight_number" required>
                            <ul class='validation'></ul>
                        </div>
                    </div>
                </div>
                <div class="hidden-text" data-identifier="journey-details">
                    <div class="group group-info">
                        <div class="form-information-guidance">
                            <p class="guidance">Knowing the <em>Flight Number</em> and its <em>point of Origin</em> allows our driver to anticipate delays.</p>
                            <p class="guidance">Please use a dash ( - ) if these details are not yet known.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" v-if="booking.one_way.quote.is_start_airport">
                <label>Meet & Greet Service</label>
                <span class="addon"><img src="<?= base_url('assets/images') ?>/meet-greet.svg"></span>
                <select class="form-control" name="meet_and_greet" required>
                    <option value="0" v-html="meet_and_greet.desc"></option>
                    <option value="1" v-html="meet_and_greet.excerpt"></option>
                    <!-- <option value="0">No (+<?= CURRENCY . '5.00' ?>) I will call my driver</option>
                    <option value="1">Yes (+<?= CURRENCY ?> {{additional_rate.meet_and_greet}}) meet me on arrival</option> -->
                </select>
                <div class="hidden-text" data-identifier="journey-details">
                    <div class="group group-info">
                        <div class="form-information-guidance">
                            <p class="guidance">If you request the <em>Meet &amp; Greet Service</em>, we organise a convenient meeting point for you where your driver will be waiting, displaying a sign with your name on it.</p>
                            <p class="guidance">If not, the driver will wait nearby in their vehicle and expect a call from you when you're ready to depart.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-12">
                    <div class="form-group date-time-validation">
                        <label v-if="booking.one_way.quote.is_start_airport">Arrival Date & Time</label>
                        <label v-else>Pick-Up Date & Time</label>
                        <span class="addon"><img src="<?= base_url('assets/images') ?>/calendar.svg"></span>
                        <input type="text" class="form-control datetime-picker" name="pickup_date_time" v-model="booking.one_way.booking_details.pickup_date_time" required>
                        <ul class='validation'></ul>
                    </div>
                </div>
                <!-- <div class="col-md-12">
                        <label>Discount code</label>
                        <span class="addon"><i class="fa fa-gift"></i></span>
                        <input type="text" class="form-control" name="discount_code" v-model="booking.one_way.booking_details.discount_code">
                    </div> -->
            </div>

            <div class="hidden-text" data-identifier="journey-details">
                <div class="group group-info">
                    <div class="form-information-guidance">
                        <p class="guidance" v-if="booking.one_way.quote.is_start_airport"><em>Arrival Date</em> and <em>Arrival Time</em> should be the scheduled arrival time of your flight.</p>
                        <p class="guidance" v-else><em>Pick-Up Date</em> and <em>Pick-Up Time</em> is the date and time the driver needs to collect you.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-inline">
            <button type="button" class="btn btn-default" v-on:click.prevent="editScreen('vehicle-selection')">
                Back To Fares <img src="<?= base_url('assets/images') ?>/edit.svg">
            </button>
            <button type="button" class="btn btn-main" v-on:click="submitOneBooking()" :disabled='isDisabledBookingForm'>
                Continue <img src="<?= base_url('assets/images') ?>/angle-right.svg">
            </button>
        </div>
    </form>
</div>
