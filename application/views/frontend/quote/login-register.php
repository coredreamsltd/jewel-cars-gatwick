<div class="journey-details vehicle-select" v-show="screen == 'account'" style="display: none;">
    <section v-if="passenger">
        <h4><i class="fa fa-user"></i> Hi! {{passenger.full_name}}</h4>
        <div class="button-inline">
            <button type="button" class="btn btn-default" v-on:click="editScreen('confirm')">
                Basket <img src="<?= base_url('assets/images') ?>/basket.svg">
            </button>
            <a href="<?= site_url('accounts/passenger/profile') ?>" class="btn btn-default waves-effect waves-light">
                Profile <i class="fa fa-user"></i>
            </a>
            <a href="<?= site_url('accounts/passenger/logout') ?>" class="btn btn-default waves-effect waves-light">
                Logout <i class="fa fa-sign-out-alt"></i>
            </a>
        </div>
    </section>
    <section v-else>
        <h4>Existing Passenger</h4>
        <div class="info-text">
            <span class="info"><img src="<?= base_url('assets/images') ?>/info.svg"></span>
            <p class="hidden-text">Register passenger can log in using their personal password. If you cannot remember your password, please click the Reset Password button.</p>
        </div>
        <form method="post">
            <div class="form-wrapper">
                <div class="row form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <span class="addon"><img src="<?= base_url('assets/images') ?>/email.svg"></span>
                            <input type="email" class="form-control" name="email" v-model="login.email" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Password </label>
                            <span class="addon"><i class="fa fa-key"></i></span>
                            <input type="password" class="form-control" name="password" v-model="login.password" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-inline">
                <button type="button" class="btn btn-default" v-on:click.prevent="editScreen('reset-password')">
                    Reset Password <i class="fa fa-key"></i>
                </button>
                <button type="button" class="btn btn-main" v-on:click="logIn()">
                    Log In <i class="fa fa-sign-in-alt"></i>
                </button>
            </div>
        </form>
        <form method="post" id="register-form">
            <h4>New Passenger</h4>
            <div class="info-text">
                <span class="info"><img src="<?= base_url('assets/images') ?>/info.svg"></span>
                <p class="hidden-text">Please enter your name and contact information below to proceed, or log in above if you've used us before</p>
            </div>
            <div class="form-wrapper">
                <div class="row form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="fname" v-model="register.fname" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="lname" v-model="register.lname" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <span class="addon"><img src="<?= base_url('assets/images') ?>/email.svg"></span>
                    <input type="email" class="form-control" name="email" v-model="register.email" required>
                    <ul class="validation"></ul>
                </div>
                <div class="row form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Telephone Number</label>
                            <input type="hidden" v-model="register.tel_phone_cc" name="tel_phone_cc">
                            <input type="text" class="form-control countrycode" name="tel_phone_no" v-model="register.tel_phone_no" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mobile Number</label>
                            <input type="hidden" v-model="register.phone_cc" name="phone_cc">
                            <input type="text" class="form-control countrycode" name="phone_no" v-model="register.phone_no" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Billing Address, Line 1</label>
                    <span class="addon"><img src="<?= base_url() ?>assets/images/location.svg"></span>
                    <input type="text" class="form-control" name="address" v-model="register.address" required>
                    <ul class="validation"></ul>
                </div>
                <div class="form-group">
                    <label>Billing Address, Line 2 (Optional)</label>
                    <span class="addon"><img src="<?= base_url() ?>assets/images/location.svg"></span>
                    <input type="text" class="form-control" v-model="register.address_1">
                </div>
                <div class="row form-row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label>City</label>
                            <span class="addon"><img src="<?= base_url() ?>assets/images/location.svg"></span>
                            <input type="text" class="form-control" name="city" v-model="register.city" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Postcode</label>
                            <span class="addon"><img src="<?= base_url() ?>assets/images/location.svg"></span>
                            <input type="text" class="form-control" name="post_code" v-model="register.post_code" required>
                            <ul class="validation"></ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <span class="addon"><img src="<?= base_url() ?>assets/images/lock.svg"></span>
                    <input type="password" class="form-control" name="password" v-model="register.password" required>
                    <ul class="validation"></ul>
                </div>
            </div>
            <div class="button-inline">
                <!-- <button v-if="show_basket" type="button" class="btn btn-default" v-on:click="editScreen('confirm')">
                    Basket <img src="<?= base_url('assets/images') ?>/basket.svg">
                </button> -->
                <button type="button" class="btn btn-main" v-on:click="newRegister()">
                    Continue to Basket <i class="fa fa-angle-right"></i>
                </button>
            </div>
        </form>
    </section>
</div>