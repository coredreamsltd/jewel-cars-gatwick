<div class="journey-details vehicle-select" v-show="screen == 'reset-password'" style="display: none;">
    <h4>Reset password</h4>
    <div class="info-text">
        <span class="info"><img src="<?= base_url('assets/images') ?>/info.svg"></span>
        <p class="hidden-text">Register passenger can log in using their personal password. If you cannot remember your password, please click the Reset Password button.</p>
    </div>
    <form method="post" id="reset-password-form">
        <div class="form-wrapper">
            <div class="row form-row">
                <div class="col-md-12">
                        <div class="form-group">
                        <label>Email</label>
                        <span class="addon"><img src="<?= base_url('assets/images') ?>/email.svg"></span>
                        <input type="email" class="form-control" v-model="login.email" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-inline">
        <button v-if="show_basket" type="button" class="btn btn-default" v-on:click="editScreen('confirm')">
                Basket <img src="<?= base_url('assets/images') ?>/basket.svg">
            </button>
            <button type="button" class="btn btn-main" v-on:click="resetPassword()">
                Submit <i class="fa fa-sign-in-alt"></i>
            </button>
        </div>
    </form>
    
</div>
