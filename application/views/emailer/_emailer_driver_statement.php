<?php $this->load->view('emailer/include/header') ?>
<div style="background: #f2f2f2; font-family: 'Nunito', sans-serif;">
    <table style="margin: 0 auto;">
        <tr>
            <td>
                <table class="container" style="width: 750px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
                    <tr>
                        <td class="section">
                            <div class="title">
                                <table class="row">
                                    <tr>
                                        <td class="small-12 large-12 first columns">
                                            <p class="white"><span>Dear <?= $data['driver_name'] ?></span>
                                                <?php echo strtr($data['mail_message'], $replace_data) ?></p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="custom-table">
                                <table>
                                    <thead>
                                        <tr>
                                            <th width="2%">S.N.</th>
                                            <th width="7%">Ref</th>
                                            <th width="15%">Pax. Name</th>
                                            <th width="25%">Journey Route</th>
                                            <th width="10%">Pickup Date/Time</th>
                                            <!-- <th width="10%">Fare</th> -->
                                            <th width="10%">Cash Collected by Driver</th>
                                            <!-- <th width="10%">Company Commission</th> -->
                                            <th width="10%">Driver Earning</th>
                                            <th width="10%">Payable</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7">Statement summary till
                                                (<?= DateTime::createFromFormat('d/m/Y', $data['from_date'])->modify('-1 day')->format('d/m/Y') ?>
                                                )
                                            </td>
                                            <td><?= CURRENCY . TM_number_format($statement['last_amount']) ?></td>
                                        </tr>
                                        <?php
                                        if ($data['jobs']) :
                                            foreach ($data['jobs'] as $index => $j) :
                                        ?>
                                                <tr>
                                                    <td><?= ++$index . '.' ?></td>
                                                    <td><?= $j['booking_ref_id'] ?></td>
                                                    <td><?= $j['client_name'] ?></td>
                                                    <td><?= $j['pickup_address'] . ' - ' . $j['dropoff_address'] ?></td>
                                                    <td><?= $j['pickup_date'] . ', ' . $j['pickup_time'] ?></td>
                                                    <!-- <td><?= CURRENCY . TM_number_format($j['driver_fare']) ?></td> -->
                                                    <td><?= CURRENCY . TM_number_format($j['cash_collected_by_driver']) ?></td>
                                                    <!-- <td><?= CURRENCY . $j['company_commission'] ?></td> -->
                                                    <td><?= CURRENCY . TM_number_format($j['driver_fare'] - $j['company_commission']) ?></td>
                                                    <td><?= CURRENCY . TM_number_format($j['driver_fare'] - $j['company_commission'] - $j['cash_collected_by_driver']) ?></td>
                                                </tr>
                                            <?php endforeach; ?>

                                            <tr>
                                                <th colspan="5">Totals:</th>
                                                <!-- <td><?= CURRENCY . TM_number_format($replace_data['{total_final_fare}']) ?></td> -->
                                                <td><?= CURRENCY . TM_number_format($replace_data['{total_cash_collected_by_driver}']) ?></td>
                                                <?php /* <td><?= CURRENCY . $replace_data['{total_company_earning}'] ?></td> */ ?>
                                                <td><?= CURRENCY . TM_number_format($replace_data['{total_driver_earning}']) ?></td>
                                                <td><?= CURRENCY . TM_number_format($statement['current_amount']) ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8"><br></td>
                                            </tr>
                                            <tr>
                                                <th colspan="8">Payments (<?= $data['from_date'] . ' - ' . date('d/m/Y') ?>)</th>
                                            </tr>
                                            <?php
                                            if ($data['payments']) :
                                                foreach ($data['payments'] as $p) :
                                            ?>
                                                    <tr>
                                                        <td colspan="7"><?= $p->payment_date ?> (<?= $p->payment_method ?>)</td>
                                                        <td><?= CURRENCY . $p->amount ?></td>
                                                    </tr>
                                                <?php
                                                endforeach;
                                            else :
                                                ?>
                                                <tr>
                                                    <td colspan="8">- No payments done -</td>
                                                </tr>
                                            <?php endif; ?>

                                            <tr>
                                                <th colspan="5"></th>
                                                <td colspan="2">
                                                    <h4>Total Payable:</h4>
                                                </td>
                                                <td>
                                                    <h4><?= CURRENCY . $statement['total_payable'] ?></h4>
                                                </td>
                                            </tr>

                                        <?php else : ?>
                                            <tr>
                                                <td colspan="8">- No work statement -</td>
                                            </tr>
                                        <?php endif; ?>

                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php $this->load->view('emailer/include/footer') ?>