<?php $this->load->view('emailer/include/header'); ?>
<div style="width: 850px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
    <table style="padding:20px; width:100%">
        <tr>
            <td width="70%">
                <strong>TO</strong><br>
                <?= $data['client_name'] ?>
            </td>
            <td width="30%">
                Date: <?= date('d/m/Y') ?><br>
                Invoice Id: <?= $data['invoice_id'] ?>
            </td>
        </tr>
    </table>

    <div style="padding:20px">
        <div id="mail-message" contenteditable="true">
            <?= $data['mail_message'] ?>
        </div>
        <!-- <p style="margin-bottom:10px">Payment Link: </p> -->
        <!-- <a href="<?= $data['payment_url'] ?>" target="_blank" style="background:#ffc107 ;display:inline-block; font-weight: 800; color: #000;padding:12px 32px">Click to Pay</a> -->
        <!-- Payment Link: <a href="<?= $data['payment_url'] ?>" target="_blank"><?= $data['payment_url'] ?></a> -->
        <br>
        <table style="margin-top:20px">
            <thead>
                <tr style="border-bottom:1px solid #313131">
                    <th style="background: #2c67b1;  font-size:12px; padding:10px 0; color:#fff">BK. ID</th>
                    <th style="background: #2c67b1;  font-size:12px; padding:10px 0; color:#fff">Pickup Date</th>
                    <th style="background: #2c67b1;  font-size:12px; padding:10px 0; color:#fff">Name</th>
                    <th style="background: #2c67b1; font-size:12px;  padding:10px 0; color:#fff">Journey Route</th>
                    <th style="background: #2c67b1; font-size:12px;  padding:10px 0; color:#fff">Vehicle</th>
                    <th style="background: #2c67b1; font-size:12px;  padding:10px 0; color:#fff">Fare</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $booking_total = 0;
                foreach ($data['booking_ids'] as $bId) :
                    $b = $this->common_model->get_where('tbl_booking_infos', array('booking_ref_id' => $bId));
                    $booking_total += $b[0]['total_fare'];
                ?>
                    <tr style="background-color:#dfe5ed">
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= $b[0]['booking_ref_id'] ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= DateTime::createFromFormat('Y-m-d', $b[0]['pickup_date'])->format('D d M,Y') ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= $b[0]['client_name'] ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= $b[0]['pickup_address'] . ' - ' . $b[0]['dropoff_address'] ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= $b[0]['vehicle_name'] ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= CURRENCY . TM_number_format($b[0]['total_fare']) ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>

                    <th style="background: #ffc107 ; padding:10px; color:#111" colspan="4">Total Fare</th>
                    <th style="background: #ffc107 ; padding:10px; color:#111" colspan="2"><?= CURRENCY . TM_number_format($booking_total) ?></th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="padding:20px; ">
        <div style="width:60%; float:left;">&nbsp;</div>
        <table style="width:40%">
            <tr>
                <td colspan="5"></td>
            </tr>

            <tr>
                <td colspan="3"></td>
                <th style="background: #2c67b1; color:#fff; padding:10px; font-size:14px; text-align:left">Total Fare</th>
                <td style="background: #dfe5ed; font-size:16px; font-weight:600; padding:10px; font-size:14px;"><?= CURRENCY . TM_number_format($booking_total) ?></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <th style="background: #2c67b1; color:#fff; padding:10px; font-size:14px; text-align:left">Invoice Amount</th>
                <td style="background: #dfe5ed; font-size:16px; font-weight:600; padding:10px; font-size:14px;"><?= CURRENCY . TM_number_format($data['invoice_amount']) ?></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <th style="background: #2c67b1; color:#fff; padding:10px; font-size:14px; text-align:left">Total Due</th>
                <td style="background: #dfe5ed; font-size:16px; font-weight:600; padding:10px; font-size:14px;"><?= CURRENCY . $this->invoice_model->get_due_amount_bulk($data['booking_ids']) ?></td>
            </tr>

        </table>
    </div>

</div>
<?php $this->load->view('emailer/include/footer'); ?>
