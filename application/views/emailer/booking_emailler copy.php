<?php $this->load->view('emailer/include/header'); ?>
<div style="background: #f2f2f2; font-family: 'Nunito', sans-serif;">
    <table style="margin: 0 auto;">
        <tr>
            <td>
                <table style="width: 850px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
                    <tr>
                        <td class="section" style="padding: 10px 12px 0 12px;">
                            <?php if ($emailer_to == 'admin') : ?>
                                Dear Admin, You receive booking request.
                            <?php elseif ($emailer_to == 'client') : ?>
                                Dear <?= $data['client_name'] ?>, <?= $data['mail_message'] ?? '' ?>
                            <?php elseif ($emailer_to == 'account_client') : ?>
                                Dear <?= $passenger->full_name ?>, <?= $data['mail_message'] ?? '' ?>
                            <?php elseif ($emailer_to == 'confirm_admin') : ?>
                                Dear Admin, <?= $data['mail_message'] ?? '' ?>
                            <?php elseif ($emailer_to == 'confirm_client') : ?>
                                Dear <?= $data['client_name'] ?>, <br>
                                Jewel Cars Gatwick would love your feedback. Post a review to our profile please.
                                <a href="https://g.page/r/CXOzxfKWEstXEA0/review"><?= SITE_NAME ?></a><br>
                                Thank you for using our service.
                                <!-- <?= $data['mail_message'] ?>. Please provide you valuable feedback <a href="https://g.page/r/CXOzxfKWEstXEA0/review">https://g.page/r/CXOzxfKWEstXEA0/review</a> -->
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="section" style="padding: 2px 12px 0 12px; border-top:1px solid #e5e5e5;">
                            <p>Reservation ID: <strong><?= $data['booking_ref_id'] ?></strong></p>
                            <p>Pick Up Date: <strong><?= DateTime::createFromFormat('Y-m-d', $data['pickup_date'])->format('l d M, Y') ?></strong></p>
                            <p>Pick Up Time : <strong><?= date('h:i A', strtotime($data['pickup_time'])) ?></strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="section" style="padding: 2px 12px 0 12px; border-top:1px solid #e5e5e5;">
                            <p>Passengers Name: <strong><?= $data['client_name'] ?></strong></p>
                            <p>Phone Number: <strong><?= $data['phone_cc'] . $data['client_phone'] ?></strong></p>
                            <p>Payment Method: <strong><?= ucwords(str_replace('-', ' ', $data['pay_method'])) ?></strong></p>
                        </td>
                    </tr>
                    <?php if (isAirport($data)) : ?>
                        <tr>
                            <td class="section" style="padding: 2px 12px 0 12px; border-top:1px solid #e5e5e5;">
                                <p>Flight No: <strong><?= $data['flight_number'] ?></strong></p>
                                <p>Arriving From: <strong><?= $data['flight_arrive_from'] ?: '-' ?></strong></p>
                                <p>Pickup Instruction: <strong>
                                        <?php if ($data['meet_and_greet'] && strpos(strtolower($data['pickup_address']), "gatwick airport") !== false) : ?>
                                            The driver will meet you in arrival hall (by Costa Coffee)
                                        <?php elseif (!$data['meet_and_greet'] && strpos(strtolower($data['pickup_address']), "gatwick airport north") !== false) : ?>
                                            Driver will meet you Opposite Hotel Premier inn
                                        <?php elseif (!$data['meet_and_greet'] && strpos(strtolower($data['pickup_address']), "gatwick airport south") !== false) : ?>
                                            Driver will meet you at Short Stay Orange Car Park, Level 0 (Outside lifts)
                                        <?php endif; ?>
                                    </strong></p>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td class="section" style="padding: 2px 12px 0 12px; border-top:1px solid #e5e5e5;">
                            <p>Pick Up Location : <strong><?= $data['pickup_address'] ?></strong></p>
                            <?php if ($data['via_point']) : ?>
                                <p>
                                    Via :<strong>
                                        <?php foreach (json_decode($data['via_point']) as $index => $via_point) : ?>
                                            (<?= ++$index ?>) <?= $via_point ?>,
                                        <?php endforeach; ?>
                                    </strong>
                                </p>
                            <?php endif; ?>
                            <p>Drop Off Location: <strong><?= $data['dropoff_address'] ?></strong></p>
                            <p>Vehicle: <strong><?= $data['vehicle_name'] ?></strong></p>
                            <p>Passengers: <strong><?= $data['client_passanger_no'] ?></strong></p>
                            <p>Suitcases: <strong><?= $data['client_luggage'] ?: 0 ?></strong> Carry-on: <strong><?= $data['client_hand_luggage'] ?: 0 ?></strong></p>
                            <?php if ($data['client_baby_no']) : ?>
                                <?php if ($data['infant_seat']) : ?>
                                    <p>Infant Seat 0-12 months: <strong><?= $data['infant_seat'] ?></strong></p>
                                <?php endif; ?>
                                <?php if ($data['child_seat']) : ?>
                                    <p>Child Seat 1-2 Years: <strong><?= $data['child_seat'] ?></strong></p>
                                <?php endif; ?>
                                <?php if ($data['child_booster_seat']) : ?>
                                    <p>Child Booster Seat 2-4 Years: <strong><?= $data['child_booster_seat'] ?></strong></p>
                                <?php endif; ?>
                                <?php if ($data['booster_seat']) : ?>
                                    <p>Booster Seat 4 Years +: <strong><?= $data['booster_seat'] ?></strong></p>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="section" style="padding: 2px 12px 0 12px; border-top:1px solid #e5e5e5; border-bottom:1px solid #e5e5e5;">
                            <p>Date/Time Reserved : <strong><?= date('dS F Y H:i (h:i a)', strtotime($data['created_at'])) ?></strong></p>
                            <p>Email: <strong><?= $data['client_email'] ?></strong></p>
                            <p>Summary: <strong><?= $data['message'] ?: '-' ?></strong></p>
                            <p>Journey Type: <strong><?= ucwords(str_replace('_', ' ', $data['journey_type'])) ?></strong></p>
                            <p>Total Price: <strong><?= CURRENCY ?><?= $data['total_fare'] ?></strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="section" style="padding: 2px 12px 0 12px;">
                            <p><strong>Our Contact Details</strong></p>
                            <p><strong>Always keep our phone numbers and call us if you can't make contact with the driver for whatever reason.</strong></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php $this->load->view('emailer/include/footer'); ?>