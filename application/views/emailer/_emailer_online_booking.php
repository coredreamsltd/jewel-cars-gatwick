<?php $this->load->view('emailer/include/header'); ?>
<table class="bg-main">
    <tr>
        <td>
            <table class="container">     
                <tr>
                    <td>
                        <?php if ($email_to == 'admin'): ?>
                            Dear Admin, You receive Online Reservation request.
                              <p> &nbsp;</p>
                        <?php elseif ($email_to == 'client'): ?>
                            Dear <?= $data['name'] ?>, Your online reservation details are as follows.
                              <p> &nbsp;</p>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="section">
                        <table class="table-details">
                            <tbody>
                                <tr>
                                    <th colspan="2" class="table-details-title">Personal Details</th>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td><?= $data['name'] ?></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?= $data['email'] ?></td>
                                </tr>
                                <tr>
                                    <th>Contact Number</th>
                                    <td><?= $data['phone'] ?></td>
                                </tr>                                                             
                            </tbody>
                            <tbody>
                                <tr>
                                    <th colspan="2" class="table-details-title">Journey Details</th>
                                </tr>
                                <tr>
                                    <th>Pickup Point</th>
                                    <td><?= $data['start'] ?></td>
                                </tr>                             
                                <tr>
                                    <th>Drop Off Point</th>
                                    <td><?= $data['end'] ?></td>
                                </tr>
                                <tr>
                                    <th>Pick Up Date/Time</th>
                                    <td><?= $data['date'] ?> <?= $data['time'] ?></td>
                                </tr>                           
                                <tr>
                                    <th>Vehicle</th>
                                    <td><?= $data['vehicle'] ?></td>
                                </tr>
                                <tr>
                                    <th>Service Type</th>
                                    <td><?= $data['service_type'] ?></td>
                                </tr>
                            </tbody>
<!--                            <tbody>-->
<!--                                <tr>-->
<!--                                    <th colspan="2" class="table-details-title">Special Instruction</th>-->
<!--                                </tr>                                                         -->
<!--                                <tr>-->
<!--                                    <th>Message</th>-->
<!--                                    <td>--><?php //echo $data['message'] ?><!--</td>-->
<!--                                </tr>                                                                                   -->
<!--                            </tbody>-->
                            <tbody>
                                <tr>
                                    <th colspan="2" class="table-details-title">Payment Type</th>
                                </tr>
                                <tr>
                                    <th>Payment Method</th>
                                    <td><?= ucfirst(str_replace('-',' ',$data['payment_type'])) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php $this->load->view('emailer/include/footer'); ?>