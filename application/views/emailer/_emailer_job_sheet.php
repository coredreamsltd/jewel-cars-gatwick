<?php $this->load->view('emailer/include/header') ?>
<div style="background: #f2f2f2; font-family: 'Nunito', sans-serif;">
    <table style="margin: 0 auto;">
        <tr>
            <td>
                <table class="container" style="width: 750px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
                    <tr>
                        <td class="section">
                            <?php if (!empty($email_to_admin)) : ?>
                                <p><strong>Dear Admin,</strong></p>
                                Following are the job sheet details send to driver [<?= $data['driver']->name ?>].
                            <?php else : ?>
                                <p><strong>Dear <?= $data['driver']->name ?>,</strong></p>
                                <?= $data['mail_message'] ?>
                            <?php endif; ?>

                            <div class="table" style="margin: 20px 0;">
                                <h2>Job Sheet:</h2>
                                <table style="width: 100%; border:1px solid #eeeeee; margin-bottom: 20px; vertical-align: top;">
                                    <tbody>
                                        <?php
                                        if ($data['jobs']) :
                                            foreach ($data['jobs'] as $job) :
                                        ?>
                                                <tr style="background-color: #50d9fc;">
                                                    <td colspan="3" style="padding: 5px;"><strong>Ref:</strong> <?= $job['booking_ref_id'] ?> | <strong>Pax.
                                                            Name:</strong> <?= $job['client_name'] ?> | <strong>Pax.
                                                            Mob.:</strong> <?= $job['client_phone'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px;">
                                                        <strong>Pickup Date/Time</strong><br>
                                                        <?= DateTime::createFromFormat('d/m/Y', $job['pickup_date'])->format('D d/m/Y') . ', ' . $job['pickup_time'] ?>
                                                    </td>
                                                    <td>
                                                        <strong>From:</strong><br>
                                                        <?= $job['pickup_address'] ?>
                                                    </td>

                                                    <!-- Payment Related -->
                                                    <td rowspan="2">
                                                        <strong>Journey Fare:</strong> <?= CURRENCY . $job['driver_fare'] ?><br>
                                                        <strong>Payment Method:</strong> <?= strtoupper($job['pay_method']) ?><br>

                                                        <?php
                                                        if ($job['pay_method'] == 'cash') :
                                                            $due = $this->invoice_model->get_due_amount($job['booking_ref_id']);
                                                        ?>
                                                            <strong>Collect Due:</strong> <?= CURRENCY . ($due) ?><br>
                                                        <?php endif; ?>
                                                        <strong>Special Instruction:</strong> <?= $job['message'] ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px;">
                                                        <strong>Vehicle Details</strong><br>
                                                        <?= $job['vehicle_name'] ?><br>
                                                        <strong>No. of. Pax.:</strong><?= $job['client_passanger_no'] ?><br>
                                                        <strong>Luggage:</strong><?= $job['client_luggage'] ?>
                                                    </td>
                                                    <td>
                                                        <!-- Airport/Seaport/Station Details -->

                                                        <!-- <strong>Flight arrival
                                    date/time:</strong><?= $job['flight_arrival_date'] . ', ' . $job['flight_arrival_time'] ?>
                                <br>
                                <strong>Flight No.:</strong><?= $job['flight_no'] ?><br> -->


                                                        <strong>To:</strong><br>
                                                        <?= $job['dropoff_address'] ?><br>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <hr>
                                                    </td>
                                                </tr>
                                            <?php
                                            endforeach;
                                        else :
                                            ?>
                                            <tr>
                                                <td colspan="3">No Jobs due.</td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php $this->load->view('emailer/include/footer') ?>