<?php $this->load->view('emailer/include/header'); ?>
<div style="background: #f2f2f2; font-family: 'Nunito', sans-serif;">
    <table style="margin: 0 auto;">
        <tr>
            <td>
                <table class="container" style="width: 750px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
                    <tr><br>
                        <td class="section">
                            <table class="table-details" style="margin-top: -45px; width: 100%; border-spacing: 0; border-collapse: collapse;">
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="table-details-title" style="background: #00D262; padding: 12px; color: #fff; text-align: left;">Job Reject</th>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Message</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;">Job rejected by driver <strong> <?= $driver->name, '(' . $driver->email . ')' ?></strong> for BK.ID
                                            [<?= $booking->booking_ref_id ?>]
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php $this->load->view('emailer/include/footer'); ?>