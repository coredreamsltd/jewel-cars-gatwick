<?php $this->load->view('emailer/include/header'); ?>
<div style="background: #f2f2f2; font-family: 'Nunito', sans-serif;">
    <table style="margin: 0 auto;">
        <tr>
            <td>
                <table class="container" style="width: 750px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
                    <tr>
                        <td>
                            <strong>Dear <?= $driver->name ?>,</strong><br>
                            <p>You've been assigned a new job. Please login to your dashboard and view job details.</p>
                            <strong>Following are the details of <?= $booking->client_name ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="section"><br><br><br>
                            <table class="table-details" style="margin-top: -45px; width: 100%; border-spacing: 0; border-collapse: collapse;">
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="table-details-title" style="background: #00D262; padding: 12px; color: #fff; text-align: left;">Personal Details (BK.ID:<?= $booking->booking_ref_id ?>)</th>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Name</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->client_name ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Email</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->client_email ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Phone No.</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->client_phone ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">No. of Passengers</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->client_passanger_no ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">No. of Baby Seats</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->client_baby_no ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Check-in Luggage/ Hand Luggage.</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->client_luggage ?></td>
                                    </tr>
                                </tbody>
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="table-details-title" style="background: #00D262; padding: 12px; color: #fff; text-align: left;">Journey Details</th>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Pickup Point</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->pickup_address ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Drop Off Point</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->dropoff_address ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Pick Up Date/Time</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->pickup_date ?> <?= $booking->pickup_time ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Vehicle</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->vehicle_name ?></td>
                                    </tr>
                                </tbody>
                                <?php if (isAirport((array)$booking)) : ?>
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="table-details-title" style="background: #00D262; padding: 12px; color: #fff; text-align: left;">Flight Arrival Details</th>
                                        </tr>
                                        <tr>
                                            <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Arrival From</th>
                                            <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->flight_arrive_from ?></td>
                                        </tr>
                                        <tr>
                                            <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Flight No.</th>
                                            <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->flight_number ?></td>
                                        </tr>
                                        <tr>
                                            <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Pick-Up Time After Landing</th>
                                            <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->after_landing_time ?></td>
                                        </tr>
                                        <tr>
                                            <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Display Name</th>
                                            <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $booking->display_name ?></td>
                                        </tr>
                                        <tr>
                                            <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Travelling Class</th>
                                            <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= ucwords(str_replace('-', ' ', $booking->class_type)) ?></td>
                                        </tr>
                                        <tr>
                                            <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Passport Type</th>
                                            <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= ucwords(str_replace('-', ' ', $booking->password_type)) ?></td>

                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="table-details-title" style="background: #00D262; padding: 12px; color: #fff; text-align: left;">Fare Details</th>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Driver Fare</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= CURRENCY . $booking->driver_fare ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php $this->load->view('emailer/include/footer'); ?>