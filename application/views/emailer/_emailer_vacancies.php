<?php $this->load->view('emailer/include/header'); ?>
    <table class="bg-main">
        <tr>
            <td>
                <table class="container">
                    <tr>
                        <td>
                            <?php if ($email_to = 'admin'): ?>
                                Dear Admin, You receive vacancy request.
                            <?php elseif ($email_to = 'client'): ?>
                                Dear Client, Your vacancy details are as follows.
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="section">
                            <table class="table-details">
                                <tbody>
                                <tr>
                                    <th colspan="2" class="table-details-title">Personal Details</th>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td><?= $data['fname']." ".$data['lname'] ?></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?= $data['email'] ?></td>
                                </tr>
                                <tr>
                                    <th>Contact Number</th>
                                    <td><?= $data['phone'] ?></td>
                                </tr>
                                <tr>
                                    <th>Postcode</th>
                                    <td><?= $data['postcode'] ?></td>
                                </tr>
                                <tr>
                                    <th>PCO License No.</th>
                                    <td><?= $data['poc_license_no'] ?></td>
                                </tr>
                                <tr>
                                    <th>Vehicle Made</th>
                                    <td><?= $data['vehicle_made'] ?></td>
                                </tr>
                                <tr>
                                    <th>Vehicle Model</th>
                                    <td><?= $data['vehicle_model'] ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php $this->load->view('emailer/include/footer'); ?>