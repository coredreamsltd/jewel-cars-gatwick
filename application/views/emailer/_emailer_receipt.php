<?php
$bIds = isset($data['booking_ids']) ? explode(',', $data['booking_ids']) : array($data['booking_id']);
$journey_total_fare = 0;
$journey_total_discount = 0;
$receipt_amount_paid = $data['invoice_amount'];
$amount_due = 0;

// journey total fare
foreach ($bIds as $bId) {
    $b_detail = $this->booking_info_model->get(['booking_ref_id' => $bId]);
    if ($b_detail) {
        $journey_total_fare += $b_detail->total_fare;
        $journey_total_discount += $b_detail->discount_fare;
    }
    $amount_due += $this->invoice_model->get_due_amount($bId, $is_manual_receipt ? $data['invoice_amount'] : 0);
}
?>

<?php $this->load->view('emailer/include/header'); ?>
<div style="width: 850px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
    <table style=" padding: 20px; width: 100%;">
        <tbody style="padding: 20px; display: table-cell;">
            <tr style="width: 100%; display: table;">
                <td width="50%">
                    <strong>BILLED TO</strong><br>
                    <?= $data['client_name'] ?> <br>
                    DATE OF ISSUE: <?= $data['date_of_payment'] ?><br>
                    INVOICE ID: <?= $data['invoice_id'] ?><br>
                </td>
                <td width="50%">
                    INVOICE: <?= ($amount_due <= 0) ? 'Paid' : 'Not Paid' ?><br>
                    <?= SITE_NAME ?><br>
                    <a href="mailto:<?= SITE_EMAIL ?>" class="pl-2"><?= SITE_EMAIL ?></a>
                </td>
            </tr>
        </tbody>
    </table>

    <div style="padding:20px">
        <div id="mail-message" contenteditable="true">
            <?= $data['mail_message'] ?>
        </div>
        <br>
        <table style="margin-top:20px">
            <thead>
                <tr style="border-bottom:1px solid #313131">
                    <th style="background: #2c67b1;  font-size:12px; padding:10px 0; color:#fff">BK. ID</th>
                    <th style="background: #2c67b1;  font-size:12px; padding:10px 0; color:#fff">Pickup Date</th>
                    <th style="background: #2c67b1;  font-size:12px; padding:10px 0; color:#fff">Name</th>
                    <th style="background: #2c67b1; font-size:12px;  padding:10px 0; color:#fff">Journey Route</th>
                    <th style="background: #2c67b1; font-size:12px;  padding:10px 0; color:#fff">Vehicle</th>
                    <th style="background: #2c67b1; font-size:12px;  padding:10px 0; color:#fff">Fare</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $booking_total = 0;
                foreach ($bIds as $bId) :
                    $b = $this->common_model->get_where('tbl_booking_infos', array('booking_ref_id' => $bId));
                    $booking_total += $b[0]['total_fare'];
                ?>
                    <tr style="background-color:#dfe5ed">
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= $b[0]['booking_ref_id'] ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= DateTime::createFromFormat('Y-m-d', $b[0]['pickup_date'])->format('D d M,Y') ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= $b[0]['client_name'] ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= $b[0]['pickup_address'] . ' - ' . $b[0]['dropoff_address'] ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= $b[0]['vehicle_name'] ?></td>
                        <td style="font-size:13px; padding: 5px; line-height:24px;"><?= CURRENCY . TM_number_format($b[0]['total_fare']) ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <th style="background: #ffc107 ; padding:10px; color:#111" colspan="4">Total Fare</th>
                    <th style="background: #ffc107 ; padding:10px; color:#111" colspan="2"><?= CURRENCY . TM_number_format($booking_total) ?></th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="padding:20px; ">
        <div style="width:60%; float:left;">&nbsp;</div>
        <table style="width:40%">
            <tr>
                <td colspan="5"></td>
            </tr>

            <tr>
                <td colspan="3"></td>
                <th style="background: #2c67b1; color:#fff; padding:10px; font-size:14px; text-align:left">SUBTOTAL</th>
                <td style="background: #dfe5ed; font-size:16px; font-weight:600; padding:10px; font-size:14px;"><?= CURRENCY ?> <?= TM_number_format($journey_total_fare - $journey_total_discount) ?></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <th style="background: #2c67b1; color:#fff; padding:10px; font-size:14px; text-align:left">DISCOUNT</th>
                <td style="background: #dfe5ed; font-size:16px; font-weight:600; padding:10px; font-size:14px;"><?= CURRENCY . TM_number_format($journey_total_discount) ?></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <th style="background: #2c67b1; color:#fff; padding:10px; font-size:14px; text-align:left">TOTAL</th>
                <td style="background: #dfe5ed; font-size:16px; font-weight:600; padding:10px; font-size:14px;"><?= CURRENCY . TM_number_format($journey_total_fare) ?></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <th style="background: #2c67b1; color:#fff; padding:10px; font-size:14px; text-align:left">DUE AMOUNT</th>
                <td style="background: #dfe5ed; font-size:16px; font-weight:600; padding:10px; font-size:14px;"><?= CURRENCY . TM_number_format($amount_due) ?></td>
            </tr>

        </table>
    </div>
</div>
<?php $this->load->view('emailer/include/footer'); ?>