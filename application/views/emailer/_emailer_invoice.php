<?php $this->load->view('emailer/include/header'); ?>
<div style="width: 850px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
    <table style="padding:20px; width:100%">
        <tr>
            <td style="padding: 10px;">
                <strong>INVOICE NUMBER</strong><br>
                <p><?= $data['invoice_id'] ?></p>
            </td>
            <td style="padding: 10px;">
                <strong>DATE OF ISSUE</strong><br>
                <p><?= date('d/m/Y') ?></p>
            </td>
            <td style="padding: 10px;">
                <strong>BILLED TO </strong><br>
                <p><?= $data['client_name'] ?></p>
            </td>
            <td style="padding: 10px;">
                <strong>PAYMENT</strong><br>
                <p>Amount Due: <?php echo CURRENCY . $this->invoice_model->get_due_amount($data['booking_id'], 0) ?></p>
            </td>
        </tr>
    </table>

    <table style="padding:20px; width:100%">
        <tr>
            <td>
                <table>

                    <tr>
                        <td style="padding: 10px;">
                            <div style="clear: both"></div>
                            <table>
                                <thead>
                                    <tr>
                                        <th style="color: #fff; padding: 10px; width: 75%;background: #2c67b1;">
                                            Description
                                        </th>
                                        <th style="color: #fff; padding: 10px; width: 25%;background: #2c67b1;text-align: right;">
                                            Amount
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="margin-bottom:10px;">

                                    <tr>
                                        <td style="border-bottom: 1px solid #8c8888; padding: 10px; width: 15%">
                                            Transfer on <b><?= date('D d M,Y', strtotime($data['pickup_date'])) . ' @ ' . date('h:i A', strtotime($data['pickup_time'])) ?></b>
                                            From <b><?= $data['pickup_address'] ?></b> to <b> <?= $data['dropoff_address'] ?></b>
                                        </td>
                                        <td style="border-bottom: 1px solid #8c8888; padding: 10px; width: 15%;  text-align: right;">
                                            <?= CURRENCY . $data['total_fare'] ?>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <!-- <td>
                                            <p style="margin-bottom:10px">Payment Link: </p>
                                            <a href="<?= $data['payment_url'] ?>" target="_blank" style="background:#ffc107 ;display:inline-block; font-weight: 800; color: #000;padding:12px 32px">Click to Pay</a>
                                        </td> -->
                                        <td>
                                            <p style="font-size: 14px"> <strong class="fare-break-down">SUBTOTAL</strong> <span style="float:right; display:inline-block"><?= CURRENCY ?> <?= round($data['total_fare'] - $data['discount_fare'], 2) ?></span></p>
                                            <p style="font-size: 14px"><strong class="fare-break-down">DISCOUNT</strong> <span style="float:right; display:inline-block"><?= CURRENCY . $data['discount_fare'] ?></span></p>
                                            <p style="font-size: 14px"><strong class="fare-break-down">TOTAL</strong> <span style="float:right; display:inline-block"><?= CURRENCY . $data['total_fare'] ?></span></p>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php $this->load->view('emailer/include/footer'); ?>
