<?php $this->load->view('emailer/include/header'); ?>
    <table class="bg-main">
        <tr>
            <td>
                <table class="container">

                    <tr>
                        <td class="section">
                            <?php if ($emailer_to == 'admin'): ?>
                                Dear Admin, You receive quote request.
                                <p> &nbsp;</p>
                            <?php elseif ($emailer_to == 'client'): ?>
                                Dear <?= $data['name'] ?>, Your quote request details are as follows. We will get back to you as soon as possible.
                                 <p> &nbsp;</p>
                            <?php endif; ?>
                           
                            <table class="table-details">
                                <tbody>
                                <tr>
                                    <th colspan="2" class="table-details-title">Personal Details</th>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td> <?= $data['name'] ?></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?= $data['email'] ?></td>
                                </tr>
                                <tr>
                                    <th>Contact No.</th>
                                    <td><?= $data['phone_cc'] . $data['phone'] ?></td>
                                </tr>
                                </tbody>
                                <?php if (!empty($data['package_id'])): ?>
                                    <tbody>
                                    <tr>
                                        <th colspan="2" class="table-details-title">Package Details</th>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td><?= $data['package_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Rate</th>
                                        <td><?= CURRENCY . $data['package_rate'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Image</th>
                                        <td>
                                            <img src="<?= base_url('uploads/package/' . $data['package_img']) ?>"
                                                 alt="pack-logo">
                                        </td>
                                    </tr>
                                    </tbody>
                                <?php endif; ?>
                                <tbody>
                                <tr>
                                    <th colspan="2" class="table-details-title">Booking Details</th>
                                </tr>
                                <?php if (!empty($data['booking_type'])): ?>
                                <tr>
                                    <th>Booking Type</th>
                                    <td><?= $data['booking_type'] ?></td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Pickup Location</th>
                                    <td><?= $data['start'] ?></td>
                                </tr>
                                <tr>
                                    <th>Drop-Off Location</th>
                                    <td><?= $data['end'] ?></td>
                                </tr>
                                <tr>
                                    <th>Pickup Date</th>
                                    <td><?= $data['date'] ?></td>
                                </tr>
                                <tr>
                                    <th>Pickup Time</th>
                                    <td><?= $data['time'] ?>  (<?= date('h:i A', strtotime($data['time'])) ?>)</td>
                                </tr>
                                <?php if (!empty($data['fleet'])): ?>
                                    <tr>
                                        <th>Fleet</th>
                                        <td><?= $data['fleet'] ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th> No . Of Passenger(Including babies)</th>
                                    <td><?= $data['passeger_no'] ?></td>
                                </tr>
                                <tr>
                                    <th>Checked-in Luggage/Hand Luggage</th>
                                    <td><?= $data['luggage'] . '/' . $data['hand_luggage'] ?></td>
                                </tr>
                                <tr>
                                    <th>Child Seat</th>
                                    <td>
                                        <?= $data['rear_facing_seats'] ?>-Rear facing seat (0-12 months/10KG)<br>
                                        <?= $data['forward_facing_seats'] ?>-Forward facing (1-4 years/18 KG)<br>
                                        <?= $data['booster_facing_seats'] ?>-Booster seat (4-11 years/25 KG)
                                    </td>
                                </tr>
                                </tbody>
                                <?php if ($data['is_airport']==1): ?>
                                    <tbody>
                                    <tr>
                                        <th colspan="2" class="table-details-title">Flight Details</th>
                                    </tr>
                                    <tr>
                                        <th>Arriving From</th>
                                        <td><?= $data['flight_arrive_from'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Flight No.</th>
                                        <td><?= $data['flight_no'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Landing Time</th>
                                        <td><?= $data['landing_time'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Display Name</th>
                                        <td><?= $data['display_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Travelling Class</th>
                                        <td><?= ucwords(str_replace('-',' ',$data['class_type'])) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Passport Type</th>
                                        <td><?= ucwords(str_replace('-',' ',$data['password_type'])) ?></td>
                                    </tr>
                                    </tbody>
                                <?php endif; ?>
                                <tbody>
                                <tr>
                                    <th colspan="2" class="table-details-title">Special Instruction</th>
                                </tr>
                                <tr>
                                    <th>Payment Type</th>
                                    <td><?= strtoupper($data['pay_method']) ?></td>
                                </tr>
                                <tr>
                                    <th>Additional Information</th>
                                    <td style="word-break: break-all;"><?= $data['message'] ?></td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php $this->load->view('emailer/include/footer'); ?>