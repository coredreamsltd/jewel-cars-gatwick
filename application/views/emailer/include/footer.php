<div class="container" style="width: 850px; margin: 0 auto; background: #dfe5ed;">
    <div class="footer" style="padding: 2px 0; background: #dfe5ed;">
        <div style="text-align: left; padding-top: 10px;">
            <p><strong><?= SITE_NAME ?></strong></p>
            <p>T:
                <a href="tel:<?= only_numbers(SITE_NUMBER_1) ?>"><?= SITE_NUMBER_1 ?></a>
                or
                <a href="https://api.whatsapp.com/send?phone=<?= only_numbers(SITE_NUMBER) ?>" target="_blank">
                    <?= SITE_NUMBER ?>
                    <img src="<?= base_url('assets/images/WhatsApp.svg.png') ?>" style="width:25px; padding-bottom: 3px;">
                </a>


            </p>
            <p>Email: <a href="mailto:<?= SITE_EMAIL ?>"><?= SITE_EMAIL ?></a></p>
            <p>Web: <a href="//<?= WEBSITE_URL ?>"><?= WEBSITE_URL ?></a></p>
        </div>
    </div>
</div>
</div>
</body>
</html>