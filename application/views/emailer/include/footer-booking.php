<div class="container" style="width: 100%; margin: 0 auto; background: #fff;">
	<div class="footer" style="padding: 2px 0; background: #fff;">
		<div style="text-align: left; padding-top: 10px;">
			<p><strong style="margin-bottom: 10px;display:block">Our Contact Details</strong></p>
			<div style="border: 1px solid #000; padding:5px">
				<p>Always keep our phone numbers and call us if you can't make contact with the driver for whatever reason.</p>
				<p>You can leave your feedback <a href="<?= site_url('reviews') ?>" target="_blank">here</a></p>
				<p><strong><?= SITE_NAME ?></strong></p>
				<p>T:
					<a href="tel:<?= only_numbers(SITE_NUMBER_1) ?>"><?= SITE_NUMBER_1 ?></a>
					or
					<a href="https://api.whatsapp.com/send?phone=<?= only_numbers(SITE_NUMBER) ?>" target="_blank">
						<?= SITE_NUMBER ?>
						<img src="<?= base_url('assets/images/WhatsApp.svg.png') ?>" style="width:25px; padding-bottom: 3px;">
					</a>
				</p>
				<p>Email: <a href="mailto:<?= SITE_EMAIL ?>"><?= SITE_EMAIL ?></a></p>
				<p>Web: <a href="//<?= WEBSITE_URL ?>"><?= WEBSITE_URL ?></a></p>
				<p>Bookings made directly with the driver are illegal and in case off accident you will not be cover by the insurance.</p>
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>
