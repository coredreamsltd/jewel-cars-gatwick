<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        @import url(https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,400;0,600;0,700;1,400&display=swap);
        .w-50{
                width:48% !important;
            }
        @media only screen and (max-width: 575px){
            td {
              
                padding-left: 5px !important; 
            }
            .w-50{
                width:100% !important;
                    float: none !important;
            }
        }
    
    </style>
</head>
<body class="background: #fff; color: #333; font-family: 'Nunito', sans-serif; font-weight: 400; font-size: 15px; margin: 0 auto !important;width:600px">
    <div class="main-wrapper" style="background: #fff; padding: 24px 0;width:600px; margin: 0 auto !important;">
        <div class="bg-main" style="width: 100%;">
            <div class="container" style="width: 600px; margin: 0 auto; background: #fff;">
                <div class="section header" style="background:#ecececdd !important;  font-family: 'Nunito', sans-serif; font-size: 14px; color: #212121; padding: 0; padding-bottom: 0; border-bottom: 0; margin-top: 10px; outline: none;">
                    <div class="main-head" style="padding: 8px 16px; background: #fff; border-bottom:2px solid #fff">
                        <div class="w-50" style="float:left;">
                            <img src="<?=base_url('assets/images/logo.png')?>" alt="logo" style="width: 100px;">
                        </div>
                       <div class="w-50" style="float:right;">
                            <h4 style="text-align: left; padding-left: 30px !important; padding-bottom: 0; line-height: 24px;">
								Your Booking Details<br>
                                <span style="font-size: 14px; text-decoration: none; color: #184178; font-weight: 600;">Service Provided by <b><?=SITE_NAME?></b></span><br>
                                <a style="font-size: 14px; text-decoration: none; color: #184178; font-weight: 600;" href="tel:<?=SITE_NUMBER_1?>">Tel :  <?=SITE_NUMBER_1?></a><br>
                                <a style="font-size: 14px; text-decoration: none; color: #184178; font-weight: 600;" target="_blank" href="https://api.whatsapp.com/send?phone=<?= only_numbers(SITE_NUMBER) ?>" >Tel :  <?=SITE_NUMBER?>  <img src="<?= base_url('assets/images/WhatsApp.svg.png') ?>" style="width:25px; padding-bottom: 3px;"></a><br>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
