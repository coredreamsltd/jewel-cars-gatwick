<?php $this->load->view('emailer/include/header'); ?>
<div style="background: #f2f2f2; font-family: 'Nunito', sans-serif;">
    <table style="margin: 0 auto;">
        <tr>
            <td>
                <table class="container" style="width: 750px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
                    <tr>
                        <td>
                            <strong>Dear <?= $data['full_name'] ?>,</strong><br>
                            <p>You're now registered with <?= SITE_NAME ?>. Please use your credentials for login</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="section">
                            <table class="table-details" style="margin-top: -45px; width: 100%; border-spacing: 0; border-collapse: collapse;">
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="table-details-title" style="background: #00D262; padding: 12px; color: #fff; text-align: left;">Account Information</th>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Name</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $data['full_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Phone No.</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $data['phone_cc'].$data['phone_no'] ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Telephone No.</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $data['tel_phone_cc'].$data['tel_phone_no'] ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Address</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $data['address'].', '.$data['address_1'].', '.$data['city'].', '.$data['post_code'] ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Email</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;"><?= $data['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <th style="padding: 10px 12px; text-align: left; border-bottom: 1px solid #ddd; font-weight: 400; width: 50%;">Password</th>
                                        <td style="padding: 10px 5px; border-bottom: 1px solid #ddd;">********</td>
                                    </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php $this->load->view('emailer/include/footer'); ?>