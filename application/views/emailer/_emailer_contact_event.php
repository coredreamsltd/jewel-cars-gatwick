<?php $this->load->view('emailer/include/header'); ?>
<table class="bg-main">
    <tr>
        <td>
            <table class="container">
                <tr>
                    <td class="section">

                        <table class="table-details">
                            <tbody>
                                <tr>
                                    <th colspan="2" class="table-details-title">Event Information</th>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td><?= $title . $fname . ' ' . $lname ?></td>
                                </tr>
                                <tr>
                                    <th>Work Email</th>
                                    <td><?= $email ?></td>
                                </tr>
                                <tr>
                                    <th>Mobile Number</th>
                                    <td><?= $mobile ?: '-' ?></td>
                                </tr>
                                <tr>
                                    <th>Company Name</th>
                                    <td><?= $company_name ?: '-' ?></td>
                                </tr>
                                <tr>
                                    <th>Event Location</th>
                                    <td><?= $event_location ?: '-' ?></td>
                                </tr>
                                <tr>
                                    <th>Event Start & End Date</th>
                                    <td><?= $start_date . ' - ' . $end_date ?></td>
                                </tr>
                                <tr>
                                    <th>Number of Passengers</th>
                                    <td><?= $no_of_passenger ?: '-' ?></td>
                                </tr>
                                <tr>
                                    <th>Vehicles</th>
                                    <td><?= $fleet_title?implode(', ',$fleet_title):'' ?></td>
                                </tr>
                                <tr>
                                    <th>Describe your Event</th>
                                    <td style="word-break: break-all;"><?= $message ?: '-' ?></td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php $this->load->view('emailer/include/footer'); ?>