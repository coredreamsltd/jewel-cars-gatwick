<?php $this->load->view('emailer/include/header-booking'); ?>
<div style="background: #fff; font-family: 'Nunito', sans-serif;">
	<table style="margin: 0 auto; background: #fff;">
		<tr>
			<td>
				<table style="width: 600px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
					<tr>
						<td class="section" style="padding: 10px 12px 0 12px;">
							<?php if ($emailer_to == 'admin') : ?>
								Dear Admin, You receive booking request.
							<?php elseif ($emailer_to == 'client') : ?>

							<?php elseif ($emailer_to == 'account_client') : ?>

							<?php elseif ($emailer_to == 'confirm_admin') : ?>
								Dear Admin, <?= $mail_message ?? '' ?>
							<?php elseif ($emailer_to == 'admin_booking_update') : ?>
								Dear Admin, <?= $mail_message ?? '' ?>
							<?php elseif ($emailer_to == 'client_booking_update') : ?>

							<?php elseif ($emailer_to == 'confirm_client') : ?>
								Dear <?= $data['client_name'] ?>, <br>
								Jewel Cars Gatwick would love your feedback. Post a review to our profile please.
								<a href="https://g.page/r/CXOzxfKWEstXEA0/review"><?= SITE_NAME ?></a><br>
								Thank you for using our service.
								<!-- <?= $data['mail_message'] ?>. Please provide you valuable feedback <a href="https://g.page/r/CXOzxfKWEstXEA0/review">https://g.page/r/CXOzxfKWEstXEA0/review</a> -->
							<?php endif; ?>
						</td>
					</tr>
				</table>
				<div class="w-50" style="float:left;">
					<div style=" padding:10px">
						<strong style="margin-bottom: 10px;display:block">Journey Details</strong>
						<table style="border: 1px solid #ddd; border-collapse: collapse; width:100%;font-family: 'Nunito',sans-serif; border:1;  color: #333; font-weight: 400; background: #fff;">

							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Pickup Location</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse;"><?= $data['pickup_address'] ?></td>
							</tr>
							<?php if ($data['via_point']) : ?>
								<tr>
									<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Via Location</td>
									<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">
										<?php foreach (json_decode($data['via_point']) as $index => $via_point) : ?>
											(<?= ++$index ?>) <?= $via_point ?>,
										<?php endforeach; ?>
									</td>
								</tr>
							<?php endif; ?>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Drop Off Location</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px"><?= $data['dropoff_address'] ?></td>
							</tr>

							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Pickup Date</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px"><?= DateTime::createFromFormat('Y-m-d', $data['pickup_date'])->format('l d M, Y') ?></td>
							</tr>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Pickup Time</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px"><?= DateTime::createFromFormat('H:i:s', $data['pickup_time'])->format('H:i (h:i A)') ?></td>
							</tr>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Vehicle</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px"><?= $data['vehicle_name'] ?></td>
							</tr>
							<?php if (isAirport($data)) : ?>
								<tr>
									<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px; color:#44c955;">Pickup Instruction</td>
									<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px; color:#44c955;">
										<?= meetingInstruction(isAirport($data)) ?>
									</td>
								</tr>
							<?php endif; ?>
						</table>
					</div>
					<div style=" padding:10px">
						<strong style="margin-bottom: 10px;display:block">Your Detail</strong>
						<table style="width: 100%; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; background: #fff;">
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Name</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px"><?= $data['client_name'] ?></td>
							</tr>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Email</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px; max-width:250px; word-break:break-all;"><?= $data['client_email'] ?></td>
							</tr>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Phone Number</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px"><?= $data['phone_cc'] . $data['client_phone'] ?></td>
							</tr>
						</table>
					</div>
					<div style=" padding:10px">
						<strong style="margin-bottom: 10px;display:block">Passengers</strong>
						<table style="width: 100%; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; background: #fff;">
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Passengers</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= $data['client_passanger_no'] ?></td>
							</tr>

							<?php if ($data['client_baby_no']) : ?>
								<?php if ($data['infant_seat']) : ?>
									<tr>
										<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Infant Seat 0-12 months</td>
										<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= $data['infant_seat'] ?></td>
									</tr>
								<?php endif; ?>
								<?php if ($data['child_seat']) : ?>
									<tr>
										<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Child Seat 1-2 Years</td>
										<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= $data['child_seat'] ?></td>
									</tr>
								<?php endif; ?>
								<?php if ($data['child_booster_seat']) : ?>
									<tr>
										<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Child Booster Seat 2-4 Years</td>
										<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= $data['child_booster_seat'] ?></td>
									</tr>
								<?php endif; ?>
								<?php if ($data['booster_seat']) : ?>
									<tr>
										<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Booster Seat 4 Years +</td>
										<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= $data['booster_seat'] ?></td>
									</tr>
								<?php endif; ?>
							<?php endif; ?>

						</table>
					</div>
					<div style=" padding:10px">
						<strong style="margin-bottom: 10px;display:block">Luggage Requirement</strong>
						<table style="width: 100%; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; background: #fff;">
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Luggage</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= $data['client_luggage'] ?? 0 ?></td>
							</tr>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Carry-on</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= $data['client_hand_luggage'] ?? 0 ?></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="w-50" style="float:right;">
					<div style=" padding:10px">
						<strong style="margin-bottom: 10px;display:block">Reservation Detail</strong>
						<table style="width: 100%; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; background: #fff;">
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Reservation ID</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px"><?= $data['booking_ref_id'] ?></td>
							</tr>

							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Date/Time Reserved</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px"><?= DateTime::createFromFormat('Y-m-d H:i:s', $data['pickup_date'] . ' ' . $data['pickup_time'])->format('l d M, Y H:i (h:i A)') ?></td>
							</tr>

							<?php $charge_details = json_decode($data['selected_fleet'])->charge_details; ?>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">Summary</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px">
									<p>Fare: <?= CURRENCY ?><?= number_format($data['total_fare'] - ($charge_details->increment->baby_seater_charge ?? 0) - ($charge_details->increment->airport_charge ?? 0) - ($charge_details->increment->meet_and_greet_charge ?? 0), 2); ?></p>
									<p>Baby Seat: <?= CURRENCY ?><?= number_format($charge_details->increment->baby_seater_charge ?? 0, 2); ?></p>
									<p>Airport: <?= CURRENCY ?><?= number_format($charge_details->increment->airport_charge ?? 0, 2); ?></p>
									<p>Meet & Greet: <?= CURRENCY ?><?= number_format($charge_details->increment->meet_and_greet_charge ?? 0, 2); ?></p>
									<p>Total: <?= CURRENCY ?><?= number_format($data['total_fare'], 2); ?></p>
								</td>
							</tr>
						</table>
					</div>
					<div style=" padding:10px">
						<strong style="margin-bottom: 10px;display:block">General Booking Details</strong>
						<table style="width: 100%; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; background: #fff;">
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Journey Type</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= ucwords(str_replace('_', ' ', $data['journey_type'])) ?></td>
							</tr>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Payment Method</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= ucwords(str_replace('-', ' ', $data['pay_method'])) ?></td>
							</tr>
							<tr>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;">Total Price</td>
								<td class="section" style="border: 1px solid #ddd; border-collapse: collapse; padding:5px;width:48%;"><?= CURRENCY ?><?= number_format($data['total_fare'],2) ?></td>
							</tr>
						</table>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
<?php $this->load->view('emailer/include/footer-booking'); ?>
