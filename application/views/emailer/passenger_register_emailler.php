<?php $this->load->view('emailer/include/header'); ?>
<div style="background: #f2f2f2; font-family: 'Nunito', sans-serif;">
    <table style="margin: 0 auto;">
        <tr>
            <td>
                <table style="width: 850px; font-family: 'Nunito', sans-serif; color: #333; font-weight: 400; margin: 0 auto; background: #fff;">
                    <tr>
                        <td class="section" style="padding: 10px 12px 0 12px;">
                            <?php if ($emailer_to == 'admin') : ?>
                                Dear Admin, New passenger has been registered.
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="section" style="padding: 2px 12px 0 12px; border-top:1px solid #e5e5e5;">
                            <p>Passengers Name: <strong><?= $data['full_name'] ?></strong></p>
                            <p>Phone Number: <strong><?= $data['phone_cc'] ?> <?= $data['phone_no'] ?></strong></p>
                        </td>
                    </tr>
                   
                    <tr>
                        <td class="section" style="padding: 2px 12px 0 12px; border-top:1px solid #e5e5e5; border-bottom:1px solid #e5e5e5;">
                            <p>Date/Time Registered : <strong><?= date('dS F Y H:i (h:i a)', strtotime($data['created_at'])) ?></strong></p>
                            <p>Email: <strong><?= $data['email'] ?></strong></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php $this->load->view('emailer/include/footer'); ?>