<?php
class Teams_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'teams';
        $this->primary_key = 'id';
    }
}
