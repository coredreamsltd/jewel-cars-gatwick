<?php

class Temp_api_quote_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'temp_api_quotes';
        $this->primary_key = 'id';
    }
}
