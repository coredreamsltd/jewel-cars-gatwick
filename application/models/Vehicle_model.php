<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 10:30 AM
 */

class Vehicle_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'vehicles';
        $this->primary_key = 'id';
    }
}
