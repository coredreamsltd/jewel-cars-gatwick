<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 10:30 AM
 */

class Blog_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_blog';
        $this->primary_key = 'id';
    }
}