<?php

class Pages_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_page';
        $this->primary_key = 'id';
    }

    function getPageBySlugNameByJoin($slug_name, $language_id = null)
    {
        $this->db->select("p.*");
        if ($language_id == 2) {
            $this->db->select("m.name as name,m.desc as desc,m.desc_short as desc_short");
        }
        $this->db->from('tbl_page p')
            ->join('tbl_multilanguage_data m', 'p.id = m.pages_id', 'left')
            ->where('p.slug', $slug_name);
        return $this->db->get()->row();
    }

    function addPage($data) {

        $page_data = array(
            'is_active' => $data['is_active'],
            'template' => $data['template'] ,
            'name' => $data['name'],
            'slug' => $data['slug'],
            'excerpt' => $data['excerpt'],
            'desc' => $data['desc'],
            'desc_short' => $data['desc_short'],
            'desc_long' => $data['desc_long'],
            'filename' => $data['filename'],
            'type' => !empty($data['type'])?$data['type']:'',
        );
        if ($this->db->insert($this->table, $page_data)) {
            $insert_id = $this->db->insert_id();
//            $multi_data = array(
//                'pages_id' => $insert_id,
//                'name' => $data['name_rus'],
//                'desc' => $data['desc_rus'],
//                'desc_short' => $data['desc_short_rus'],
//                'short_code' => 'rus'
//            );
//            if ($this->db->insert('tbl_multilanguage_data', $multi_data)) {
//                return true;
//            }
        return true;
        }
        return false;
    }

    function updatePage($id, $data) {

        $page_data = array(
            'is_active' => $data['is_active'],
            'template' => $data['template'] ,
            'name' => $data['name'],
            'excerpt' => $data['excerpt'],
            'desc' => $data['desc'],
            'desc_short' => $data['desc_short'],
            'desc_long' => $data['desc_long'],
            'type' => !empty($data['type'])?$data['type']:'',
        );
        if (isset($data['filename'])) {
            $page_data['filename'] = $data['filename'];
        }
//        $multi_data = array(
//            'name' => $data['name_rus'],
//            'desc' => $data['desc_rus'],
//            'desc_short' => $data['desc_short_rus'],
//        );
        $this->db->where('id', $id);
        if ($this->db->update($this->table, $page_data)) {
//            $this->db->where('pages_id', $id);
//            if ($this->db->update('tbl_multilanguage_data', $multi_data)) {
//                return true;
//            }
                return true;
        }
        return false;
    }
    function getPageByPageIdJoin($id) {
        $this->db
            ->select("p.*,m.name as name_rus,m.desc as desc_rus,m.desc_short as desc_short_rus")
            ->from('tbl_page p')
            ->join('tbl_multilanguage_data m', 'p.id = m.pages_id', 'left')
            ->where('p.id', $id);
        return $this->db->get()->row();
    }
    function get_all_language() {
        $query = $this->db->get('tbl_languages');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }
}
