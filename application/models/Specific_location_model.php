<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 10:30 AM
 */

class Specific_location_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'specific_locations';
        $this->primary_key = 'id';
    }

    public function getAllArea() {
        $this->db->select('*, ST_AsText(coordinates) coordinates')
            ->from($this->table);

        $result = $this->db->get()->result();
        return $result;
    }

    public function getArea($id=null)
    {

        $this->db->select('*, ST_AsText(coordinates) coordinates')
            ->from($this->table)
            ->where('id', $id);

        $row = $this->db->get()->row();
        return $row;
    }

    public function insertArea($zone_data) {

        $this->db->set('coordinates', "ST_GeomFromText('{$zone_data['coordinates']}')", false);
        $this->db->set('title', $zone_data['title']);
        $this->db->set('type', $zone_data['type']);
        $this->db->set('lat', $zone_data['lat']);
        $this->db->set('lng', $zone_data['lng']);
        $this->db->set('instructions', $zone_data['instructions']);
        $this->db->insert($this->table);
        return $this->db->insert_id();
    }
	
    public function updateArea($zone_id, $zone_data) {
		
		$this->db->set('coordinates', "ST_GeomFromText('{$zone_data['coordinates']}')", false);
        $this->db->set('title', $zone_data['title']);
        $this->db->set('type', $zone_data['type']);
        $this->db->set('lat', $zone_data['lat']);
        $this->db->set('lng', $zone_data['lng']);
		$this->db->set('instructions', $zone_data['instructions']);
        $this->db->where('id', $zone_id);
        $this->db->update($this->table);

        return $this->db->affected_rows();
    }
}
