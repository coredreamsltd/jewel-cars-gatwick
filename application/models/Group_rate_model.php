<?php

class Group_rate_model extends MY_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_group_rates';
        $this->primary_key = 'id';
    }

    public function insert_batch($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    public function getAllWithNames()
    {

        return $this->db
            ->select('from_lg.id from_group_id, from_lg.group_name from_group_name, to_lg.id to_group_id, to_lg.group_name to_group_name, to_l.name to_location_name')
            ->select('gr.*')
            ->from($this->table . ' gr')
            ->join('tbl_location_group from_lg', 'from_lg.id = gr.from_group_id', 'left')
            ->join('tbl_location_group to_lg', 'to_lg.id = gr.to_group_id', 'left')
            ->join('tbl_locations to_l', 'to_l.id = gr.to_location_id', 'left')
            ->order_by('gr.id', 'desc')
            ->get()
            ->result();
    }

    public function getByGroupId($group_id)
    {
        $this->db
            ->select('from_lg.id from_group_id, from_lg.group_name from_group_name, to_lg.id to_group_id, to_lg.group_name to_group_name, to_l.name to_location_name')
            ->select('gr.*')
            ->from($this->table . ' gr')
            ->join('tbl_location_group from_lg', 'from_lg.id = gr.from_group_id', 'left')
            ->join('tbl_location_group to_lg', 'to_lg.id = gr.to_group_id', 'left')
            ->join('tbl_locations to_l', 'to_l.id = gr.to_location_id', 'left')
            ->where('gr.from_group_id', $group_id)
            ->or_where('gr.to_group_id', $group_id);

        $result = $this->db->get()->result();

        return $result;
    }

}