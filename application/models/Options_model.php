<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 9:58 AM
 */

class Options_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'options';
        $this->primary_key = 'id';
    }
}
