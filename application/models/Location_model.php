<?php

class Location_model extends MY_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_locations';
        $this->primary_key = 'id';
    }

    public function get_locations_by_group_id($location_group_id)
    {
        return $this->db
            ->select('l.*')
            ->from($this->table . ' l')
            ->join('tbl_location_group_reln lgr', 'lgr.location_id = l.id')
            ->where('lgr.location_group_id', $location_group_id)
            ->get()
            ->result();
    }

    public function get_all_with_group()
    {
        return $this->db
            ->select('l.*, lg.id lg_id, lg.group_name lg_group_name')
            ->from($this->table . ' l')
            ->join('tbl_location_group_reln lgr', 'lgr.location_id = l.id', 'left')
            ->join('tbl_location_group lg', 'lg.id = lgr.location_group_id', 'left')
            ->get()
            ->result();
    }

    public function location_search($keywords)
    {
        $this->db->select("*");    
        $this->db->like('name', $keywords);
        $this->db->or_like('postcode', $keywords);
        $query = $this->db->get("tbl_locations"); 
        return $query->result();
    }



    public function search($keywords)
    {
        $keywords_arr = explode(' ', $keywords);

        $location_type = $this->db
            ->from('tbl_location_type')
            ->where('type', $keywords_arr[0])
            ->get()->row();



        $sql = "SELECT l.* FROM $this->table l";

        $sql .= " WHERE";

        if ($location_type) {
            array_shift($query_arr);
            $sql .= " location_type_id = $location_type->id";
            $sql .= " AND name LIKE '%" . implode(' ', $keywords_arr) . "%'";
        } else {
            $sql .= " name LIKE '%" . implode(' ', $keywords_arr) . "%'";
        }
        
        $sql .= " OR postcode LIKE '%" . implode(' ', $keywords_arr) . "%'";
        $sql .= " OR address LIKE '%" . implode(' ', $keywords_arr) . "%'";
        $sql .= " OR search_text LIKE '%" . implode(' ', $keywords_arr) . "%'";
        $sql .= " ORDER BY " . TM_order_special_string("name", $keywords);

        $result = $this->db->query($sql)->result();

        return $result;
    }

    public function get_by_location_type_id($location_type_id)
    {
        return $this->db
            ->get_where('tbl_locations', array('location_type_id' => $location_type_id))
            ->result();
    }
}
