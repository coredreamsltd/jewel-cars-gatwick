<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Fleets_route_rates_model
 *
 * @author coredreams
 */
class Fleets_route_rates_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'fleets_route_rates';
        $this->primary_key = 'id';
    }

    public function join_route_rate($from_id, $to_id, $fleet_id) {
        echo $from_id . '<br>';
        echo $to_id . '<br>';
        $query = $this->db->select('*')
                        ->from('fleets_route_rates frr')
                        ->join('routes_rate rr', 'rr.id = frr.route_rate_id', 'left')
                        ->where(['rr.from_id', $from_id])
                        ->where(['rr.to_id', $to_id])
                        ->where(['frr.to_id', $fleet_id])
                        ->get()->result();

        debug($query);
    }

    public function get_fleets_by_route_rate($route_rate_id) {
        $query = $this->db
                ->select('frr.rate')
                ->select('f.*')
                        ->from('tbl_fleets f')
                        ->join('fleets_route_rates frr', 'f.fleet_groups_id=frr.fleet_group_id ', 'left')
                        ->where('frr.route_rate_id', $route_rate_id)
                        ->where('frr.rate <> ', '0.00')
                        ->get()->result();

        return $query;
    }

}
