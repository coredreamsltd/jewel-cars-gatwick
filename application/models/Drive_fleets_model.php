<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 5:17 PM
 */

class Drive_fleets_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "driver_fleets";
        $this->primary_key = 'id';
    }
}