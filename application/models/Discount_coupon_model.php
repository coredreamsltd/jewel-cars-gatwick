<?php

class Discount_coupon_model extends MY_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_discount_coupons';
        $this->primary_key = 'id';
        $this->load->model('discount_coupon_list_model');
    }

    public function discountProcess($discount_coupon_no=null)
    {
        
        $data = [
            'status' => 0,
            'discount' => 0,
            'message' => 'Discount coupon code does not matched',
        ];

        $this->discount_coupon_list_model->update(array('is_redeemed' => 1), array('coupon_no' => $discount_coupon_no));
        $coupon = $this->discount_coupon_list_model->get(array('coupon_no' => $discount_coupon_no));

        if ($coupon) {
            // $this->db->query("UPDATE tbl_discount_coupons SET no_of_coupons_redeemed = no_of_coupons_redeemed+1 WHERE id = {$coupon->discount_coupon_id}");
            $discountCoupon = $this->get(array('id' => $coupon->discount_coupon_id));

            $data = [
                'status' => 1,
                'message' => 'Coupon found',
                'discount' => $discountCoupon->discount,
                'discount_type' => $discountCoupon->discount_type,
            ];
        }

        return $data;

    }

}