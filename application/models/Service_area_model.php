<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 10:30 AM
 */

class Service_area_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'service_areas';
        $this->primary_key = 'id';
    }

    public function getAllArea() {
        $this->db->select('*, ST_AsText(coordinates) coordinates')
            ->from($this->table);

        $result = $this->db->get()->result();
        return $result;
    }

    public function getArea($id=null)
    {

        $this->db->select('*, ST_AsText(coordinates) coordinates')
            ->from($this->table)
            ->where('id', $id);

        $row = $this->db->get()->row();
        return $row;
    }

    public function insertArea($zone_data) {

        $this->db->set('coordinates', "ST_GeomFromText('{$zone_data['coordinates']}')", false);
        $this->db->set('area_name', $zone_data['area_name']);
        $this->db->set('custom_area_name', $zone_data['custom_area_name']);
        $this->db->set('lat', $zone_data['lat']);
        $this->db->set('lng', $zone_data['lng']);
        $this->db->insert($this->table);
        return $this->db->insert_id();
    }

    public function updateArea($zone_id, $zone_data) {

        $this->db->set('coordinates', "ST_GeomFromText('{$zone_data['coordinates']}')", false);
        $this->db->set('area_name', $zone_data['area_name']);
        $this->db->set('custom_area_name', $zone_data['custom_area_name']);
        $this->db->set('lat', $zone_data['lat']);
        $this->db->set('lng', $zone_data['lng']);
        $this->db->where('id', $zone_id);
        $this->db->update($this->table);

        return $this->db->affected_rows();
    }
}
