<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 12:13 PM
 */

class File_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->table = "file";
        $this->primary_key = 'id';
    }

}