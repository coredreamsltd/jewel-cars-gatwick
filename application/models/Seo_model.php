<?php

class Seo_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_seo';
        $this->primary_key = 'id';
    }

    public function get_seo()
    {

        $seo = $this->db->get_where('tbl_seo', array('link' => '/' . uri_string()))->row();
        return $seo ? $seo : false;
    }
}
