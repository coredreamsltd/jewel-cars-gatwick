<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Booking_info_model
 *
 * @author Sujendra
 */
class Booking_info_model extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_booking_infos';
		$this->load->model('invoice_model');
		$this->load->model('passenger_model');
	}

	public function generate_ref_id()
	{
		$booking_ref_id = rand(11111, 99999);

		$booking = $this->db
			->select('*')
			->from($this->table)
			->where('booking_ref_id', $booking_ref_id)
			->get()
			->row();

		if (!$booking) {
			return $booking_ref_id;
		} else {
			$this->generate_ref_id();
		}
	}

	public function insert_batch($data = [])
	{
		return $this->db->insert_batch($this->table, $data);
	}

	public function send_auto_receipt_email($invoice_id)
	{

		$invoice = $this->db->get_where('tbl_invoice', array('invoice_id' => $invoice_id))->result_array();
		if (!$invoice[0]['booking_id'] && !$invoice[0]['cart_id']) {
			$bulk_invoice_bids = $this->db->get_where('tbl_invoice_bulk', array('invoice_id' => $invoice_id))->result_array();
			$booking_id = $bulk_invoice_bids[0]['booking_id'];
		} else {
			$booking_id = $invoice[0]['booking_id'];
		}

		$booking = $this->db->get_where('tbl_booking_infos', array('booking_ref_id' => $booking_id))->result_array();

		if (!isset($booking[0]) || !isset($invoice[0])) {
			return false;
		}
		$data = [
			'mail_subject' => "Payment Receipt for Invoice ID: {$invoice_id}",
			'mail_message' => "Thank you! Payment of " . CURRENCY . $invoice[0]['invoice_amount'] . " has been received.",
		];

		if (!$invoice[0]['booking_id'] && !$invoice[0]['cart_id']) {
			$booking_ids = array();
			foreach ($bulk_invoice_bids as $b_ids) {
				array_push($booking_ids, $b_ids['booking_id']);
			}
			$data['booking_ids'] = implode(',', $booking_ids);
		}

		$data = $booking[0] + $invoice[0] + $data;
		$ids = isset($data['booking_ids']) ? explode(',', $data['booking_ids']) : array($data['booking_id']);
		$data['prev_receipts'] = $this->invoice_model->getAllPaidReceipts($ids);

		$invoice_mail_template = $this->load->view('emailer/_emailer_receipt', array('data' => $data, 'is_manual_receipt' => false), true);

		$receiptData = [
			'mail_subject_receipt' => $data['mail_subject'],
			'mail_template_receipt' => $invoice_mail_template,
			'receipt_mailed_datetime' => date('Y-m-d H:i:s'),
		];

		$this->invoice_model->update($receiptData, array('invoice_id' => $invoice_id));

		$passenger = $this->passenger_model->get(array('id' => $booking[0]['passenger_id']));
		$recipients = [$passenger->email];
		email_help($recipients, $receiptData['mail_subject_receipt'], $receiptData['mail_template_receipt'], [SITE_EMAIL => SITE_NAME]);
		
		$admin_emailer_template = common_emogrifier($this->load->view('emailer/booking_emailler', array('data' => $booking[0], 'emailer_to' => 'admin'), true));
		email_help([ADMIN_EMAIL], "Booking Received - " . SITE_NAME . " [{$booking[0]['booking_ref_id']}]", $admin_emailer_template, [SITE_EMAIL => SITE_NAME]);

		if (count($ids)) {
			$bookings = $this->db->where_in('booking_ref_id', $ids)->get('tbl_booking_infos')->result();
			foreach ($bookings as $booking) {
				$passenger = $this->passenger_model->get(array('id' => $booking->passenger_id));
				$recipients = [$passenger->email];
				email_help($recipients, $booking->mail_subject_confirm, $booking->mail_template_confirm, [SITE_EMAIL => SITE_NAME]);
			}
		}

	}
}
