
<?php
class Packages_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'packages';
        $this->primary_key = 'id';
    }
}
