<?php

class Job_sheet_model extends MY_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->table = "tbl_job_sheet";
        $this->primary_key = 'id';
    }

    public function get_by_id($job_sheet_id)
    {
        return $this->db->get_where('tbl_job_sheet', array('id' => $job_sheet_id))->row();
    }

    public function update_job_status($status, $job_sheet_id, $driver_id)
    {

        $cond = array(
            'id' => $job_sheet_id,
            'driver_id' => $driver_id
        );
        $this->db->update('tbl_job_sheet', array('job_status' => $status, 'updated_at' => date('Y-m-d H:i:s')), $cond);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    function last_job_sheet_by_booking_id($booking_id)
    {
        return $this->db->like('booking_ids', $booking_id)
            ->order_by('id', 'DESC')
            ->get('tbl_job_sheet')
            ->row();
    }
}
