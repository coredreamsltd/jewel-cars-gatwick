<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/1/2017
 * Time: 4:35 PM
 */

class Vacancy_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'tbl_vacancies';
        $this->primary_key='id';

    }

}