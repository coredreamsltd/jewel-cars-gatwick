<?php

class Discount_account_based_model extends MY_Model {
    

    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_discount_account_based';
        $this->primary_key = 'id';
    }
        
}