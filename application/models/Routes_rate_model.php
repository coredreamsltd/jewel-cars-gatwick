<?php

class Routes_rate_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_routes_rate';
        $this->primary_key = 'id';
    }

    function calculate_route_fares($start_id, $end_id)
    {
        $from_to_fare = $this->db->get_where('tbl_routes_rate', array('from_id' => $start_id, 'to_id' => $end_id))->row();
        $to_from_fare = $this->db->get_where('tbl_routes_rate', array('to_id' => $start_id, 'from_id' => $end_id))->row();


        $fares = array(
            'one_way_fare' => $from_to_fare->rate,
            'two_way_fare' => $from_to_fare->rate + $to_from_fare->rate
        );

        return $fares;
    }

    function get_route_rate($start_id, $end_id)
    {
        $rate = $this->db->get_where('tbl_routes_rate', array('from_id' => $start_id, 'to_id' => $end_id))->row();
        if ($rate) {
            return $rate;
        }
    }

    function get_discounted_rate($start_id, $end_id)
    {
        $rate = $this->db->get_where('tbl_routes_rate', array('from_id' => $start_id, 'to_id' => $end_id))->row();
        return $rate ? $rate->rate - $this->get_route_discount($start_id, $end_id) : 0;
    }

    function get_route_discount($start_id, $end_id)
    {
        $rate = $this->db->get_where('tbl_routes_rate', array('from_id' => $start_id, 'to_id' => $end_id))->row();

        if ($rate) {
            if ($rate->discounttype == 'by_amount') {
                return $rate->discount;
            } else if ($rate->discounttype == 'by_percent') {
                return round($rate->rate * $rate->discount * 0.01);
            }
        }

        return 0;
    }

    function additional_charges_manual($start_id, $end_id)
    {

        $add_charge = array(
            'charges_one_way' => array(
                'airport_pickup_charge' => 0,
                'seaport_pickup_charge' => 0,
                'railway_station_pickup_charge' => 0
            ),
            'charges_return' => array(
                'return_airport_pickup_charge' => 0,
                'return_seaport_pickup_charge' => 0,
                'return_railway_station_pickup_charge' => 0
            ),
            'total_charge_one_way' => 0,
            'total_charge_return' => 0
        );

        $pickup_charge = $this->db->get_where('tbl_add_rate_pickup_meet', array('location_id' => $start_id))->row();
        if ($pickup_charge) {
            if (isAirport($start_id)) {
                $add_charge['charges_one_way']['airport_pickup_charge'] = $pickup_charge->amount;
            } elseif (isSeaport($start_id)) {
                $add_charge['charges_one_way']['seaport_pickup_charge'] = $pickup_charge->amount;
            } elseif (isStation($start_id)) {
                $add_charge['charges_one_way']['railway_station_pickup_charge'] = $pickup_charge->amount;
            }
        }

        $return_pickup_charge = $this->db->get_where('tbl_add_rate_pickup_meet', array('location_id' => $end_id))->row();
        if ($return_pickup_charge) {
            if (isAirport($end_id)) {
                $add_charge['charges_return']['return_airport_pickup_charge'] = $return_pickup_charge->amount;
            } else if (isSeaport($end_id)) {
                $add_charge['charges_return']['return_seaport_pickup_charge'] = $return_pickup_charge->amount;
            } else if (isStation($end_id)) {
                $add_charge['charges_return']['return_railway_station_pickup_charge'] = $return_pickup_charge->amount;
            }
        }

        foreach ($add_charge['charges_one_way'] as $k => $v) {
            $add_charge['total_charge_one_way'] += $v;
        }

        foreach ($add_charge['charges_return'] as $k => $v) {
            $add_charge['total_charge_return'] += $v;
        }

        return $add_charge;
    }

    function additional_charges_google()
    {

        $add_charge = array(
            'charges_one_way' => array(
                'airport_pickup_charge' => 0,
                'seaport_pickup_charge' => 0
            ),
            'charges_return' => array(
                'return_airport_pickup_charge' => 0,
                'return_seaport_pickup_charge' => 0
            ),
            'total_charge_one_way' => 0,
            'total_charge_return' => 0
        );

        return $add_charge;
    }

    /**
     *
     * @param float $fare
     * @param array () $add_charge
     * @param array () $fleet
     * @param string $fare_type
     * @param string $triptype
     * @param string $x_pickup_date
     * @param Holiday_rate_model $Holiday_rate_model
     * @return array()
     */
    function calculate_final_fare($fare, $add_charge, $fleet, $fare_type, $triptype, $x_pickup_date, $Holiday_rate_model, $account, $booking_info = null)
    {

        $final_fare = $fare;

        // Fleet Charge
        $fleet_charge = round($fleet->charge_type == 'by_amount' ? $fleet->fleet_charge : $final_fare * $fleet->fleet_charge * 0.01);
        $final_fare += $fleet_charge;

        // TA Additional Commission
        $ta_additional_commission = 0;
        if ($account && $account['account_type'] == 'travel_agent') {
            $ta_additional_commission = round($final_fare * $account['additional_commission_rate'] * 0.01);
            $final_fare += $ta_additional_commission;
        }

        // Holiday rate Incr/Decr
        $discount['holiday_incr_decr'] = 0;
        if (is_valid_date($x_pickup_date)) {
            $holiday_rate = $Holiday_rate_model->get_holiday_rate(DateTime::createFromFormat('d/m/Y', $x_pickup_date));
            if ($holiday_rate && $holiday_rate->is_active) {
                // one way
                $discount['holiday_incr_decr'] = round($final_fare * $holiday_rate->incr_decr_rate * 0.01);
                $final_fare += $discount['holiday_incr_decr'];
            }
        }

        // Round Trip Discount
        $discount['round_trip'] = 0;
        if ($triptype == 'two_way') {
            $add_rates = $this->db->get('tbl_additional_rate')->row();
            $discount['round_trip'] = round($final_fare * $add_rates->round_trip_discount * 0.01);
            $final_fare -= $discount['round_trip'];
        }

        // Additional Charges
        if ($fare_type == 'one_way_fare') {
            $final_fare += $add_charge['total_charge_one_way'];
        } else if ($fare_type == 'return_fare') {
            $final_fare += $add_charge['total_charge_return'];
        }

        $return_data = array(
            'final_fare' => $final_fare,
            'discount' => $discount,
            'ta_additional_commission' => $ta_additional_commission
        );

        return $return_data;
    }

    function process_discount_coupon($discount_coupon_no, $final_fare = 0)
    {
        $msg_discount = array(
            'status' => 0,
            'msg' => '',
            'amount' => 0,
            'coupon_no' => $discount_coupon_no,
            'discount_coupon' => null
        );

        if (!$discount_coupon_no) {
            $msg_discount['msg'] = 'You did not enter any discount coupons.';
            return $msg_discount;
        }

        $coupon = $this->db->where(array('coupon_no' => $discount_coupon_no))->get('tbl_discount_coupon_list')->row();
        if (!$coupon) {
            $msg_discount['msg'] = 'Invalid coupon (' . $discount_coupon_no . ')';
            return $msg_discount;
        }

        $dc = $this->db->where(array('id' => $coupon->discount_coupon_id))->get('tbl_discount_coupons')->row();

        $today = date_create();
        $valid_from = date_create_from_format('d/m/Y', $dc->valid_from);
        $valid_to = date_create_from_format('d/m/Y', $dc->valid_to);

        if (!($today >= $valid_from && $today <= $valid_to)) { // date check
            $msg_discount['msg'] = 'Your coupon (' . $discount_coupon_no . ') is valid but out of date.';
            return $msg_discount;
        }

        if (!$dc->is_active) {
            $msg_discount['msg'] = 'This coupon (' . $discount_coupon_no . ') is currently disabled.';
            return $msg_discount;
        }

        if ($coupon->is_redeemed) {

            if (!$dc->is_reusable) {
                $msg_discount['msg'] = 'This coupon (' . $discount_coupon_no . ') is already redeemed.';
                return $msg_discount;
            }

            if ($dc->max_redeem_limit != 0 && $dc->max_redeem_limit == $dc->no_of_coupons_redeemed) {
                $msg_discount['msg'] = 'This coupon (' . $discount_coupon_no . ') has exceeded its max usage limit.';
                return $msg_discount;
            }

            $msg_discount['status'] = 1;
            $msg_discount['amount'] = round($dc->discount_type == 'by_percent' ? $final_fare * $dc->discount * 0.01 : $dc->discount);
            $msg_discount['msg'] = $dc->coupon_title . ' (' . ($dc->discount_type == 'by_percent' ? $dc->discount . '%' : SITE_CURRENCY . $dc->discount) . ')';
            $msg_discount['discount_coupon'] = $dc;
            return $msg_discount;
        }

        $msg_discount['status'] = 1;
        $msg_discount['amount'] = round($dc->discount_type == 'by_percent' ? $final_fare * $dc->discount * 0.01 : $dc->discount);
        $msg_discount['msg'] = $dc->coupon_title . ' (' . ($dc->discount_type == 'by_percent' ? $dc->discount . '%' : SITE_CURRENCY . $dc->discount) . ')';
        $msg_discount['discount_coupon'] = $dc;
        return $msg_discount;

    }

    public function insert_batch($set)
    {
        return $this->db
            ->insert_batch($this->table, $set);
    }

    public function update_batch($set, $index)
    {
        return $this->db
            ->update_batch($this->table, $set, $index);
    }

}
