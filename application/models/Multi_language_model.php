<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/22/2018
 * Time: 10:53 AM
 */

class Multi_language_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_multilanguage_data';
        $this->primary_key = 'id';
    }

}