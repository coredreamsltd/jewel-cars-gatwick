<?php

class Discount_coupon_list_model extends MY_Model {
    

    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_discount_coupon_list';
        $this->primary_key = 'id';
    }
        
}