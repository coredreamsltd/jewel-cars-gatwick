<?php

class Invoice_bulk_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "tbl_invoice_bulk";
        $this->primary_key = 'id';
    }
}
