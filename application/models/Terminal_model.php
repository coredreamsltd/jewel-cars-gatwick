<?php

class Terminal_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'tbl_terminals';
        $this->primary_key = 'id';
    }
}
