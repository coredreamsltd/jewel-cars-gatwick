<?php

class Location_group_reln_model extends MY_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_location_group_reln';
        $this->primary_key = 'id';
    }

    public function insert_batch($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

}