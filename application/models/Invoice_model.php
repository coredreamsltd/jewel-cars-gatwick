<?php

require APPPATH . '/vendor/autoload.php';
use Pelago\Emogrifier\CssInliner;

class Invoice_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "tbl_invoice";
        $this->primary_key = 'id';
    }

    public function get_by_id($invoice_id)
    {
        return $this->db->get_where('tbl_invoice', array('id' => $invoice_id))->row();
    }

    //    public function get_due_amount($booking_id, $new_invoice_amount) {
    //
    //        $booking = $this->db->get_where('tbl_bookings', array('booking_id' => $booking_id))->row();
    //        $total_amount_paid = $this->db->select_sum('invoice_amount')
    //                        ->get_where('tbl_invoice', array('booking_id' => $booking_id, 'payment_status' => 1, 'is_cancelled' => 0))->row();
    //
    //        if ($total_amount_paid->invoice_amount) {
    //            return round($booking->final_fare - $total_amount_paid->invoice_amount - $new_invoice_amount);
    //        } else {
    //            return round($booking->final_fare - $new_invoice_amount);
    //        }
    //    }

    public function get_due_amount($booking_id, $new_invoice_amount = null)
    {

        $booking = $this->booking_info_model->get(['booking_ref_id' => $booking_id]);

        $invoices = $this->db
            ->select('*, i.invoice_id i_invoice_id')
            ->from('tbl_invoice i')
            ->join('tbl_invoice_bulk ib', 'i.invoice_id = ib.invoice_id', 'left')
            ->where("(ib.booking_id = '{$booking->booking_ref_id}' OR i.booking_id = '{$booking->booking_ref_id}')")
            ->where('payment_status', 1)
            ->get()
            ->result();

        $amount_due = $booking->total_fare;

        if ($invoices) {
            foreach ($invoices as $i) {
                $amount_due -= $i->partial_amount > 0 ? $i->partial_amount : $i->invoice_amount;
            }
        }

        if ($new_invoice_amount) {
            $amount_due -= $new_invoice_amount;
        }

        return $amount_due;
    }

    /**
     *
     * @param array $booking_ids
     * @param float $new_invoice_amount
     */
    public function get_due_amount_bulk($booking_ids, $new_invoice_amount = null)
    {

        $booking_ids = implode(',', $booking_ids);
        //        $invoices = $this->db
        //            ->select('*, i.invoice_id i_invoice_id')
        //            ->from('tbl_invoice i')
        //            ->join('tbl_invoice_bulk ib', 'i.invoice_id = ib.invoice_id', 'left')
        //            ->where("(ib.booking_id IN ({$booking_ids}) OR i.booking_id IN ({$booking_ids}))")
        //            ->where('payment_status', 1)
        //            ->get()
        //            ->result();

        $bookings_total = $this->db
            ->select_sum('total_fare', 'sum_final_fare')
            ->where_in('booking_ref_id', explode(',', $booking_ids))
            ->from('tbl_booking_infos')
            ->get()->row();

        $amount_due = $bookings_total->sum_final_fare;


        //        if ($invoices) {
        //            foreach ($invoices as $i) {
        //                $amount_due -= $i->partial_amount > 0 ? $i->partial_amount : $i->invoice_amount;
        //            }
        //        }

        if ($new_invoice_amount) {
            $amount_due -= $new_invoice_amount;
        }

        return $amount_due;
    }

    public function create_invoice($quote, $booking, $final_fare, $booking_id)
    {

        // Generate Invoice
        $invoice_id = uniqueid('INV', 5, 'tbl_invoice', 'invoice_id');
        $invoice_amount = $booking['pay_method'] == 'cash' ? round($final_fare * 0.25) : $booking['total_fare'];

        $fleet = $this->fleet_model->get($booking['selected_fleet_id']);
        $booking['invoice_id'] = $invoice_id;
        $booking['booking_id'] = $booking_id;
        $booking['fleet_title'] = $fleet->title;
        $booking['invoice_amount'] = $invoice_amount;
        $booking['customer_name'] = check_user('account_id') ? get_userdata('name') : $booking['client_name'];
        $booking['mail_message'] = 'We have received your booking. However, no payment has been made. Please use the link at the bottom to make the payment so we can confirm your booking. If we do not receive payment within the next two days, we will presume that you are no longer interested in this booking and proceed to cancel.';
        $booking['payment_url'] = site_url('quote/payment?invoice_id=' . $invoice_id);

        $invoice_mail_template = ($this->load->view('emailer/_emailer_invoice', array('data' => $quote + $booking), true));


        $invoice_data = array(
            'invoice_id' => $invoice_id,
            'booking_id' => $booking_id,
            'invoice_amount' => $invoice_amount,
            'invoice_payment_method' => $booking['pay_method'],
            'customer_email' => check_user('account_id') ? get_userdata('email') : $booking['client_name'],
            'mail_subject' => 'Payment Confirmation for Invoice Id: ' . $invoice_id,
            'mail_template' => $invoice_mail_template,
            'generated_date' => date(DateTime::W3C),
            'created_at' => date(DateTime::W3C),
        );

        return $invoice_mail_template;
    }

    /**
     * create_invoice_bulk function is built pretty complicated due to various changes in feature. Sorry for this one.
     *
     * @param type $booking_ids
     * @param type $booking
     * @param type $invoice_amount
     * @param type $invoice_payment_method
     * @param boolean $return_template
     * @param string $temp_invoice_id
     * @param array $invoice_data
     * @return type
     */
    public function create_invoice_bulk($booking_ids, $booking, $invoice_amount, $invoice_payment_method = null, $return_template = false, $temp_invoice_id = null, $invoice_data = null)
    {

        $invoice_id = $temp_invoice_id !== null ? $temp_invoice_id : uniqueid('INV', 5, 'tbl_invoice', 'invoice_id');
        $invoice_total = 0;

        if ($invoice_payment_method !== null) {
            if ($booking['pay_method'] == 'cash') {
                foreach ($booking_ids as $b_id) {
                    $due_amount = $this->get_due_amount($b_id);
                    $invoice_total += round($due_amount * 0.25);
                }
            } else {
                $invoice_total = $invoice_amount;
            }
        } else {
            $invoice_total = $invoice_amount;
        }

        if (!isset($invoice_data['mail_subject'])) {
            $invoice_data['mail_subject'] = 'Payment Confirmation for Invoice Id: ' . $invoice_id;
        }

        if (empty($invoice_data['mail_message'])) {
            $invoice_data['mail_message'] = '';
            //            $template = $this->common_model->get_all('tbl_email_templates');
            //            $invoice_data['mail_message'] = $template[0]['invoice_template_top'];
        }
        if (empty($invoice_data['payment_url'])) {
            $invoice_data['payment_url'] = site_url('quote/payment?invoice_id=' . $invoice_id);
            //            $template = $this->common_model->get_all('tbl_email_templates');
            //            $invoice_data['mail_message'] = $template[0]['invoice_template_top'];
        }

        $invoice_emailer_template_data = array(
            'invoice_id' => $invoice_id,
            'booking_ids' => $booking_ids,
            'mail_message' => $invoice_data['mail_message'],
            'payment_url' => $invoice_data['payment_url'],
            'invoice_amount' => $invoice_total
        );

        $invoice_emailer_template_data['client_name'] = $booking['client_name'];
        $invoice_mail_template = $this->load->view('emailer/_emailer_invoice_bulk', array('data' => $invoice_emailer_template_data), true);
        if ($return_template) {
            return $invoice_mail_template;
        }

        $invoice_data = array(
            'invoice_id' => $invoice_id,
            'invoice_amount' => $invoice_total,
            'invoice_payment_method' => $invoice_payment_method ? $invoice_payment_method : 'card',
            'customer_email' => $booking['client_email'],
            'mail_subject' => $invoice_data['mail_subject'],
            'mail_template' => $invoice_mail_template,
            'generated_date' => date(DateTime::W3C)
        );

        if ($this->insert($invoice_data)) {

            if ($invoice_payment_method == 'free_credit') {
                $partial_amount_array = array();
                $remainder = $invoice_total % count($booking_ids);

                foreach ($booking_ids as $index => $b_id) {
                    $partial_amount = ($invoice_total - $remainder) / count($booking_ids);
                    $partial_amount_array[] = (count($booking_ids) <= $index + 1) ? $partial_amount : ($partial_amount + $remainder);
                }
            }

            foreach ($booking_ids as $index => $b_id) {

                if (isset($partial_amount_array)) {
                    $partial_amount = $partial_amount_array[$index];
                } else {
                    $due_amount = $this->get_due_amount($b_id);
                    $partial_amount = $booking['pay_method'] == 'cash' ? round($due_amount * 0.25) : $due_amount;
                }

                $invoice_bulk_data = array(
                    'booking_id' => $b_id,
                    'invoice_id' => $invoice_id,
                    'partial_amount' => $partial_amount,
                    'created_at' => date(DateTime::W3C)
                );
                $this->db->insert('tbl_invoice_bulk', $invoice_bulk_data);
                $this->db->update('tbl_booking_infos', array('is_bulk_invoice_raised' => 1), array('booking_ref_id' => $b_id));
            }

            // return $invoice_id;
            return $invoice_mail_template;
        }
    }

    public function create_invoice_cart($cart, $cart_data)
    {

        $invoice_id = uniqueid('INV', 5, 'tbl_invoice', 'invoice_id');
        $booking_ids = explode(',', $cart_data['booking_ids']);

        $invoice_total = null;
        if ($cart['payment_type'] == 'cash') {
            foreach ($booking_ids as $b_id) {
                $b_detail = $this->booking_model->get_by_booking_id($b_id);
                $invoice_total += round($b_detail->final_fare * 0.25);
            }
        } elseif ($cart['payment_type'] == 'card') {
            $invoice_total = $cart_data['cart_amount']; // Full Amount
        }

        $emailer_data = array(
            'invoice_id' => $invoice_id,
            'invoice_amount' => $invoice_total,
            'customer_name' => get_userdata('name'),
            'mail_message' => 'We have received your booking. However, no payment has been made. Please use the link at the below to make the payment so we can confirm your booking. If we do not receive payment within the next two days, we will presume that you are no longer interested in this booking and proceed to cancel.',
            'payment_url' => base_url('book/payment?invoice_id=' . $invoice_id),
            'booking_ids' => $booking_ids
        );

        $invoice_mail_template = $this->load->view('emailer/_emailer_invoice_cart', array(
            'data' => $emailer_data,
            'account' => $this->account_model->get(get_userdata('account_id'))
        ), true);

        $invoice_data = array(
            'invoice_id' => $invoice_id,
            'cart_id' => $cart_data['cart_id'],
            'invoice_amount' => $invoice_total,
            'customer_email' => get_userdata('email'),
            'mail_subject' => 'Invoice for cart id: ' . $cart_data['cart_id'],
            'mail_template' => $invoice_mail_template,
            'generated_date' => date(DateTime::W3C)
        );

        if ($this->insert($invoice_data)) {
            foreach ($booking_ids as $b_id) {
                $b_detail = $this->booking_model->get_by_booking_id($b_id);
                $this->common_model->insert('tbl_invoice_bulk', array(
                    'booking_id' => $b_id,
                    'invoice_id' => $invoice_id,
                    'partial_amount' => $cart['payment_type'] == 'cash' ? round($b_detail->final_fare * 0.25) : $b_detail->final_fare,
                    'created_at' => date(DateTime::W3C)
                ));
                $this->common_model->update('tbl_bookings', array('is_cart_invoice_raised' => 1), array('booking_id' => $b_id));
            }
            return $invoice_id;
        }
    }

    public function get_by_ref_id($invoice_ref_id)
    {
        return $this->db->get_where('tbl_invoice', array('invoice_id' => $invoice_ref_id))->row();
    }

    function get_all_by_booking_id($booking_id)
    {
        $this->db
            ->select('ib.partial_amount')
            ->select('i.*')
            ->from($this->table . ' i')
            ->join('tbl_invoice_bulk ib', 'ib.invoice_id = i.invoice_id', 'left')
            ->where("(ib.booking_id = '$booking_id' OR i.booking_id = '$booking_id')")
            ->order_by('i.id desc');

        $all_invoices = $this->db->get()->result();

        return $all_invoices;
    }

    public function getAllRelatedToACartId($cartId)
    {
        $this->db
            ->select('ib.partial_amount')
            ->select('i.*')
            ->from($this->table . ' i')
            ->join('tbl_invoice_bulk ib', 'ib.invoice_id = i.invoice_id')
            ->join('tbl_bookings b', 'b.booking_id = ib.booking_id')
            ->where('b.cart_id', $cartId)
            ->group_by('i.invoice_id')
            ->order_by('i.id desc');

        $all_invoices = $this->db->get()->result();

        return $all_invoices;
    }

    public function generate_invoice_id()
    {
        return uniqueid('INV', 5, 'tbl_invoice', 'invoice_id');
    }

    public function get_by_invoice_id($invoice_id)
    {
        return $this->db->get_where('tbl_invoice', array('invoice_id' => $invoice_id))->row();
    }

    public function getAllPaidReceipts($booking_ids)
    {

        $this->db
            ->select('i.*')
            ->from($this->table . ' i')
            ->join('tbl_invoice_bulk ib', 'ib.invoice_id = i.invoice_id')
            ->where_in('ib.booking_id', $booking_ids)
            ->where('i.payment_status', 1)
            ->order_by('i.generated_date', 'asc')
            ->group_by('i.invoice_id');
        $result = $this->db->get()->result();
        return $result;
    }

    public function getAllPaidReceiptsOfSingleBooking($booking_id)
    {
        $this->db
            ->select('ib.partial_amount')
            ->select('i.*')
            ->from($this->table . ' i')
            ->join('tbl_invoice_bulk ib', 'ib.invoice_id = i.invoice_id')
            ->where('ib.booking_id', $booking_id)
            ->where('i.payment_status', 1)
            ->order_by('i.generated_date', 'asc')
            ->group_by('i.invoice_id');

        $result = $this->db->get()->result();
        return $result;
    }

    public function search($q, $is_count = false)
    {

        $this->db->select('i.*');

        if (isset($q['search']) && $q['search']) {
            $columns = ['invoice_id', 'booking_id', 'invoice_amount', 'customer_email', 'invoice_payment_method'];

            foreach ($columns as $index => $column) {
                $columns[$index] = "COALESCE(`$column`, '')";
            }

            $concat_string = "CONCAT(" . implode(',', $columns) . ")";
            $this->db->select($concat_string . ' custom_search_data', false);
        }

        $this->db->from('tbl_invoice i');

        if (isset($q['account_id']) && $q['account_id']) {
            $this->db->join('tbl_invoice_bulk as ib', 'ib.invoice_id = i.invoice_id')
                ->join('tbl_booking_infos as b', 'b.booking_ref_id = ib.booking_id')
                ->where(array('b.passenger_id' => $q['account_id']))
                ->group_by('i.id');
        }


        if (isset($q['per_page_limit']) && isset($q['per_page'])) {
            $this->db->limit($q['per_page_limit'], $q['per_page']);
        } else if (isset($q['per_page_limit']) && !isset($q['per_page'])) {
            $this->db->limit($q['per_page_limit']);
        }


        if (isset($q['search']) && $q['search']) {
            $keywords = explode(' ', $q['search']);
            $having_query = [];
            foreach ($keywords as $index => $keyword) {
                if (in_array($keyword, ['not', 'paid'])) continue;
                $having_query[] = "custom_search_data LIKE '%$keyword%'";
            }

            if ($having_query) {
                $this->db->having('(' . implode(' OR ', $having_query) . ')');
            }

            // custom extra conditions
            if (stripos($q['search'], 'not') !== false || stripos($q['search'], 'not paid') !== false) {
                $this->db->where('payment_status', 0);
                $this->db->where('(is_cancelled IS NULL OR is_cancelled = 0)');
            }

            if (stripos($q['search'], 'paid') !== false && !(stripos($q['search'], 'not') !== false)) {
                $this->db->where('payment_status', 1);
                $this->db->where('(is_cancelled IS NULL OR is_cancelled = 0)');
            }
        }

        $this->db->order_by('i.id', 'DESC');
        $query = $this->db->get();

        if ($is_count) {
            return $query->num_rows();
        }

        $result = $query->result_array();
        return $result;
    }

    function getAllInvoicesByContractorId($contractor_id)
    {
        $this->db->select();
        $this->db->from('tbl_invoice as invoice');
        $this->db->join('tbl_contractor_booking_relation as relation', 'relation.booking_id=invoice.booking_id', 'left');
        $this->db->join('tbl_bookings as booking', 'booking.booking_id=relation.booking_id', 'left');
        $this->db->join('tbl_sub_contractor as contractor', 'contractor.contractor_id=relation.contractor_id', 'left');
        $this->db->where('relation.contractor_id', $contractor_id);
        $this->db->group_by('invoice.invoice_id');
        $result['single_invoices'] = $this->db->get()->result_array();

        $this->db->select();
        $this->db->from('tbl_invoice_bulk as bulk');
        $this->db->join('tbl_invoice as invoice', 'invoice.invoice_id=bulk.invoice_id', 'left');
        $this->db->join('tbl_contractor_booking_relation as relation', 'relation.booking_id=bulk.booking_id', 'left');
        $this->db->join('tbl_bookings as booking', 'booking.booking_id=relation.booking_id', 'left');
        $this->db->join('tbl_sub_contractor as contractor', 'contractor.contractor_id=relation.contractor_id', 'left');
        $this->db->where('relation.contractor_id', $contractor_id);
        $this->db->group_by('invoice.invoice_id');
        $result['bulk'] = $this->db->get()->result_array();
        return $result;
    }
}
