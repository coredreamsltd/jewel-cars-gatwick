<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Yearly_model
 */
class Yearly_booking_model extends MY_Model {

    public function __construct() {
        $this->has_one['driver'] = array('Driver_model','id','driver_id');
        $this->has_one['passenger'] = array('Passenger_model','id','passenger_id');
        $this->has_one['fleet'] = array('Fleet_model','id','fleet_id');
        parent::__construct();
        $this->table = 'yearly_bookings';
    }

}
