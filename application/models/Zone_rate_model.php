<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/14/2018
 * Time: 10:42 AM
 */

class Zone_rate_model extends MY_Model
{
    public function __construct() {
        parent::__construct();
        $this->table = 'zones_rate';
        $this->primary_key = 'id';
    }
}