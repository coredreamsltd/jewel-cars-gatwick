<?php

class Testimonials_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'testimonials';
        $this->primary_key = 'id';
    }

}
