<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 10:49 AM
 */

class Driver_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "driver";
        $this->primary_key = 'id';
    }

    public function allJobs($driver_id)
    {
        $sql = "SELECT * FROM tbl_booking_infos as b"
            . " JOIN tbl_assigned_job as aj ON b.id = aj.booking_id"
            . " WHERE aj.driver_id = $driver_id";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function get_statements($driver_id)
    {
        return $this->db->order_by('id DESC')->get_where('tbl_statements', array('driver_id' => $driver_id))->result();
    }

    public function isStatementRaised($from_date, $driver_id)
    {
        $count = $this->db
            ->where("STR_TO_DATE(to_date, '%d/%m/%Y') >= '" . DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d') . "'")
            ->where('driver_id', $driver_id)
            ->where('is_cancelled', 0)
            ->count_all_results('tbl_statements');

        return $count > 0 ? true : false;
    }

    public function getBookings($driver_id, $from_date = null, $to_date = null)
    {
        $sql = "SELECT * FROM tbl_booking_infos";
        $sql .= " WHERE 1";

        if ($from_date && $to_date) {
            $sql .= " AND (STR_TO_DATE(pickup_date, '%m/%d/%Y') BETWEEN STR_TO_DATE('$from_date', '%m/%d/%Y') AND STR_TO_DATE('$to_date', '%m/%d/%Y'))";
        }
        $sql .= " AND driver_id = '$driver_id'";
        $sql .= " AND is_cancelled = 0";
        $sql .= " ORDER BY pickup_date ASC";

        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function getPayments($driver_id, $from_date = null, $to_date = null)
    {

        $this->db
            ->from('tbl_driver_payments')
            ->where('driver_id', $driver_id);

        if ($from_date && $to_date) {
            $from_date = DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d');
            $to_date = DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d');
            $this->db->where("(payment_date BETWEEN '$from_date' AND '$to_date')");
        }

        $result = $this->db->get()->result();
        return $result;
    }

    public function getTotalPayment($driver_id, $from_date = null, $to_date = null)
    {
        $this->db
            ->select_sum('amount')
            ->from('tbl_driver_payments')
            ->where('driver_id', $driver_id);

        if ($from_date && $to_date) {
            $from_date = DateTime::createFromFormat('d/m/Y', $from_date)->format('Y-m-d');
            $to_date = DateTime::createFromFormat('d/m/Y', $to_date)->format('Y-m-d');
            $this->db->where("(payment_date BETWEEN '$from_date' AND '$to_date')");
        }

        $row = $this->db->get()->row();
        return $row->amount;
    }

    public function statement_amount($job_start_date, $job_end_date, $payment_start_date, $payment_end_date, $driver_id, $is_deduct_paid)
    {

        $statement_amount = 0;

        $bookings = $this->getBookings($driver_id, $job_start_date, $job_end_date);

        foreach ($bookings as $b) {
            $b = (object) $b;
            $b->company_commission=round($b->driver_fare*0.01*COMPANY_COMMISSION,2);
            $cash_to_driver = $this->calculateCashToDriver($b->booking_ref_id);
            $statement_amount += $b->driver_fare - $b->company_commission - $cash_to_driver;
        }

        if ($is_deduct_paid) {
            $total_payment = $this->getTotalPayment($driver_id, $payment_start_date, $payment_end_date);
            return round($statement_amount - $total_payment,2);
        }

        return round($statement_amount,2);
    }

    public function calculateCashToDriver($booking_id)
    {
//        $row = $this->db
//            ->select_sum('partial_amount')
//            ->from('tbl_invoice i ')
//            ->join('tbl_invoice_bulk ib', 'ib.invoice_id = i.invoice_id')
//            ->where([
//                'ib.booking_id' => $booking_id,
////                'i.invoice_payment_method' => 'cash',
//                'i.is_cancelled' => 0
//            ])
//            ->get()->row();
//        return $row->partial_amount;
        $data = $this->db
            ->select('*')
            ->from('tbl_invoice i')
            ->where([
                'i.booking_id' => $booking_id,
                'i.invoice_payment_method' => 'cash',
                'i.is_cancelled' => 0
            ])
            ->get()->row();

        return (!empty($data->invoice_amount)?$data->invoice_amount:0);
    }

    public function get_payments($driver_id)
    {
        return $this->db->order_by('id DESC')->get_where('tbl_driver_payments', array('driver_id' => $driver_id))->result();
    }

    public function add_payment($data, $driver_id)
    {

        $db_data = array(
            'driver_id' => $driver_id,
            'amount' => $data['amount'],
            'payment_method' => $data['payment_method'],
            'payment_date' => DateTime::createFromFormat('d/m/Y', $data['payment_date'])->format('Y-m-d'),
            'created_at' =>date('Y-m-d H:i:s')
        );

        if ($this->db->insert('tbl_driver_payments', $db_data)) {
            return $this->db->insert_id();
        }
    }

    public function delete_payment($payment_id)
    {
        $this->db->delete('tbl_driver_payments', array('id' => $payment_id));
        return $this->db->affected_rows() > 0 ? true : false;
    }

    public function get_job_sheets($driver_id)
    {
        return $this->db->order_by('id DESC')->get_where('tbl_job_sheet', array('driver_id' => $driver_id))->result();
    }
}
