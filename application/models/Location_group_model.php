<?php

class Location_group_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_location_group';
        $this->primary_key = 'id';
    }
}