<?php

class Route_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'routes';
        $this->primary_key = 'id';
    }

    public function getAllZone()
    {
        $this->db->select('*, ST_AsText(coordinates_1) coordinates_1, ST_AsText(coordinates_2) coordinates_2')
            ->from($this->table);

        $result = $this->db->get()->result();
        return $result;
    }

    public function getZone($zone_id)
    {

        $this->db->select('*, ST_AsText(coordinates_1) coordinates_1, ST_AsText(coordinates_2) coordinates_2')
        ->from($this->table)
        ->where('id', $zone_id);
        
        $row = $this->db->get()->row();
        return $row;
    }
    
    public function getWhere($conditions = null)
    {
        
        $this->db->select('*, ST_AsText(coordinates_1) coordinates_1, ST_AsText(coordinates_2) coordinates_2')
            ->from($this->table)
            ->where($conditions);

        $row = $this->db->get()->result();
        return $row;
    }

    public function insertZone($zone_data)
    {

        $this->db->set('coordinates_1', "ST_GeomFromText('{$zone_data['coordinates_1']}')", false);
        $this->db->set('coordinates_2', "ST_GeomFromText('{$zone_data['coordinates_2']}')", false);
        $this->db->set('title_1', $zone_data['title_1']);
        $this->db->set('lat_1', $zone_data['lat_1']);
        $this->db->set('lng_1', $zone_data['lng_1']);
        $this->db->set('radius_1', $zone_data['radius_1']);
        $this->db->set('title_2', $zone_data['title_2']);
        $this->db->set('lat_2', $zone_data['lat_2']);
        $this->db->set('lng_2', $zone_data['lng_2']);
        $this->db->set('radius_2', $zone_data['radius_2']);
        $this->db->set('rate', $zone_data['rate']);
        $this->db->set('rate_distance', $zone_data['rate_distance']);
        $this->db->set('fleet_id', $zone_data['fleet_id']);
        $this->db->set('service_area_id', $zone_data['service_area_id']);
        $this->db->set('type', $zone_data['type']);
        $this->db->insert($this->table);
        return $this->db->insert_id();
    }

    public function updateZone($zone_id, $zone_data)
    {

        $this->db->set('coordinates_1', "ST_GeomFromText('{$zone_data['coordinates_1']}')", false);
        $this->db->set('coordinates_2', "ST_GeomFromText('{$zone_data['coordinates_2']}')", false);
        $this->db->set('title_1', $zone_data['title_1']);
        $this->db->set('lat_1', $zone_data['lat_1']);
        $this->db->set('lng_1', $zone_data['lng_1']);
        $this->db->set('radius_1', $zone_data['radius_1']);
        $this->db->set('title_2', $zone_data['title_2']);
        $this->db->set('lat_2', $zone_data['lat_2']);
        $this->db->set('lng_2', $zone_data['lng_2']);
        $this->db->set('radius_2', $zone_data['radius_2']);
        $this->db->set('rate', $zone_data['rate']);
        $this->db->set('rate_distance', $zone_data['rate_distance']);
        $this->db->set('fleet_id', $zone_data['fleet_id']);
        $this->db->set('service_area_id', $zone_data['service_area_id']);
        $this->db->set('type', $zone_data['type']);
        $this->db->where('id', $zone_id);
        $this->db->update($this->table);

        return $this->db->affected_rows();
    }
}
