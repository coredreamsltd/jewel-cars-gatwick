<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_Controller
 *
 * @author Sujendra
 */
class Admin_Controller extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('current_user')) {
            redirect('admin/index');
        }
        // $this->load->model('service_area_model');
        // $this->data['service_area_id'] = getSession('service_area_id');
        // $this->data['service_areas'] = $this->service_area_model->getAllArea();
        $this->data['username'] = getSession('username');
    }
}
