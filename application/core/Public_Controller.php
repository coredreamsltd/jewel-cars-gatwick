<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_Controller
 *
 * @author Binay
 */
class Public_Controller extends MY_Controller
{
    public $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('seo_model');
        $this->load->model('passenger_model');
        $this->load->model('driver_model');
        $this->load->model('pages_model');
        $this->load->model('fleet_model');

        $this->load->model('yearly_booking_model');
        $this->load->model('yearly_time_schedule_model');
        $this->load->model('booking_info_model');



        $this->data['seo'] = $this->seo_model->get_seo();
        $this->data['passenger'] = $this->passenger_model->get(array('id' => $this->session->userdata('logged_in_passenger')));
        $this->data['driver'] = $this->driver_model->get(array('id' => $this->session->userdata('logged_in_driver')));
        $this->data['business_template_data'] = $this->pages_model->get_all(['template' => 'business_page']);


        // QUOTE
        // session_destroy();

        if (empty(getSession('journey'))) {
            setSession('journey', 0);
        }
        if (empty(getSession('journey_type'))) {
            setSession('journey_type', 'one_way');
        }
        if (empty(getSession('screen'))) {
            setSession('screen', 'quote-form');
        }
        if (empty(getSession('discount'))) {
            setSession('discount', ['code' => '', 'amount' => 0]);
        }
        // setSession('journey_type', 'two_way');
        // setSession('journey_type', 'one_way');
        // setSession('screen', 'quote-form');
        // setSession('screen', 'vehicle-selection');
        if (!empty($_GET['action'])) {
            if ($_GET['action'] == 'add_new_journey') {
                if (!empty($_SESSION['journeys'][getSession('journey')]['one_way']['quote'])) {
                    setSession('journey', count($_SESSION['journeys']));
                }
                setSession('screen', 'quote-form');
            } elseif ($_GET['action'] == 'add_return_journey' && isset($_GET['journey'])) {
                $_SESSION['journeys'][getSession('journey')]['journey_type'] = 'two_way';
                setSession('journey_type', 'two_way');
                setSession('journey', $_GET['journey']);
                setSession('screen', 'booking-form-two');
                redirect(site_url('#booking'));
            } elseif ($_GET['action'] == 'remove_journey' && isset($_GET['journey']) && isset($_GET['type'])) {
                $_SESSION['journeys'][getSession('journey')]['journey_type'] = 'one_way';
                setSession('journey_type', 'one_way');
                setSession('journey', getSession('journey'));
                setSession('screen', 'confirm');

                if ($_GET['type'] == 'one_way') {
                    $one_way = $_SESSION['journeys'][getSession('journey')]['one_way'];
                    $two_way = $_SESSION['journeys'][getSession('journey')]['two_way'];
                    $_SESSION['journeys'][getSession('journey')]['one_way'] = $two_way;
                    $_SESSION['journeys'][getSession('journey')]['two_way'] = $one_way;
                }

                if ($_SESSION['journeys'][getSession('journey')]['journey_type'] == 'one_way') {
                    unset($_SESSION['journeys'][getSession('journey')]);
                }
            }
            redirect(site_url());
        }

        $journey = getSession('journey');
        $journeys = getSession('journeys');

        if (empty($journeys[$journey]['one_way'])) {
            $_SESSION['journeys'][$journey]['one_way'] = ['quote' => '', 'fleets' => '', 'selected_fleet' => '', 'booking_details' => '', 'grand_total_charge' => ''];
            setSession('discount', ['code' => '', 'amount' => 0]);
        }
        // if (empty($journeys[$journey]['two_way'])) {
        //     $_SESSION['journeys'][$journey]['two_way']=['quote'=>'','fleets'=>'','selected_fleet'=>'','booking_details'=>'','grand_total_charge'=>''];
        // }

        $this->data['fleets'] = $this->fleet_model->order_by('sort')->get_all(['status' => 1]);
        $this->data['additional_rate'] = ['meet_and_greet' => 0, 'baby_seater' => 0];
        if (!empty($_SESSION['journeys'][getSession('journey')][getSession('journey_type')]['selected_fleet']->id)) {
            $conditions = ['fleet_id' => $_SESSION['journeys'][getSession('journey')][getSession('journey_type')]['selected_fleet']->id];
            $this->data['additional_rate'] =  $this->db->get_where('tbl_additional_rate', $conditions)->row();
            if ($this->data['additional_rate']) {
                $this->data['additional_rate']->multi_airport = json_decode($this->data['additional_rate']->multi_airport);
                $this->data['additional_rate']->multi_seaport = json_decode($this->data['additional_rate']->multi_seaport);
                $this->data['additional_rate']->multi_station = json_decode($this->data['additional_rate']->multi_station);
                $this->data['additional_rate']->multi_airport_dropoff = json_decode($this->data['additional_rate']->multi_airport_dropoff);
                $this->data['additional_rate']->multi_seaport_dropoff = json_decode($this->data['additional_rate']->multi_seaport_dropoff);
                $this->data['additional_rate']->multi_station_dropoff = json_decode($this->data['additional_rate']->multi_station_dropoff);
            }
        }
    }

    function notification() {
        if($this->input->is_cli_request())
        {
            $date = new DateTime();
            $current_date = date('Y-m-d', strtotime($date->format('Y-m-d')));
            $current_time = date('H:i:s', strtotime($date->format('H:i:s')));
            $bookings  = $this->yearly_booking_model->with('driver')->fields(['id','start_date','end_date','home_pickup_time','school_pickup_time'])->get_all();
            foreach($bookings as $booking)
            {
                if($current_date >= $booking->start_date && $current_date <= $booking->end_date && ($current_time == $booking->home_pickup_time || $current_time == $booking->school_pickup_time))
                {
                    $data = [
                        'booking' => $booking,
                        'driver' => $booking->driver,
                        'mail_message' => "Today your job is at ".date('h:i a', strtotime($booking->home_pickup_time)),
                    ];
                    $emailer_template = $this->load->view('emailer/emailer_yearly_job_remainder', $data, true);
                    $sent_email = email_help([$booking->driver->email], "Yearly job reminder" . SITE_NAME, $emailer_template, [SITE_EMAIL => SITE_NAME]);
    
                    $post = [
                        'yearly_booking_id' => $booking->id,
                        'home_pickup_time' => $booking->home_pickup_time,
                        'school_pickup_time' => $booking->school_pickup_time,
                        'pickup_date' => $current_date,
                        'is_email_sent' => $sent_email ? 1 : 0,
                    ];
    
                    $this->yearly_time_schedule_model->insert($post);
                }
            }
        }
    }
}
