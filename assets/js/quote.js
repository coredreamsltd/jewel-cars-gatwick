var GET_ADDRESS_URL = "https://api.getAddress.io/";
var API_KEY = "-ZTt5_mWHEG1srrzErhyBg24212";
$(document).ready(function() {

    var max_fields = 5; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".show-via"); //Add button ID

    var x = 0; //initlal text box count
    $('.show-via').click(function(e) { //on add input button click
        e.preventDefault();
        if (x == max_fields) { //max input box allowed
            alert("You have reached the limit of adding " + x + " Via Points");
        } else {
            var html = '<div class="form-group"> <label>Via</label> <span class="addon"><img src="./assets/images/location.svg"></span>' +
                '<input type="text" class="form-control way_point_address' + x + '" placeholder="Eg: Gatwick Airport or RH6 ONP" name="stop_point[]" required>' +
                '<span class="way_point' + x + '">' +
                '<input type="hidden" class="lat" name="way_point_lat[]">' +
                '<input type="hidden" class="lng" name="way_point_lng[]">' +
                '<input type="hidden" class="postcode" name="way_point_post_code[]"> ' +
                '<input type="hidden" class="city" name="way_point_city[]"> ' +
                '<input type="hidden" class="district" name="way_point_district[]">' +
                '<input type="hidden" class="country" name="way_point_country[]">' +
                '</span>' +
                '<a href="#!" class="remove_field">' +
                '<i class="fa fa-times text-danger"></i>' +
                '</a>' +
                '</div>'
            $(wrapper).append(html); //add input box
            way_point(x);
            x++; //text box increment
        }
        e.preventDefault();
    });
    $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        --x;
    });


});
//way point function
function way_point(x) {

    $(".way_point_address" + x).autocomplete({
        source: function(request, response) {
            $.ajax({
                url: GET_ADDRESS_URL + "autocomplete/" + request.term,
                data: {
                    "api-key": API_KEY,
                },
                dataType: "json",
                success: function(data) {
                    response(data.suggestions);
                },
            });
        },
        select: function(event, ui) {
            $(".way_point_address" + x).val(ui.item.address);
            $.ajax({
                url: GET_ADDRESS_URL + "get/" + ui.item.id,
                data: {
                    "api-key": API_KEY,
                },
                dataType: "json",
                success: function(response1) {


                    $('.way_point' + x).find('.lat').val(response1.latitude);
                    $('.way_point' + x).find('.lng').val(response1.longitude);
                    $('.way_point' + x).find('.postcode').val(response1.postcode);
                    $('.way_point' + x).find('.city').val(response1.town_or_city);
                    $('.way_point' + x).find('.district').val(response1.district);
                    $('.way_point' + x).find('.country').val(response1.country);
                },
            });
            return false;
        },
    });
}